<?php 
/*
* jdavila 
* 08/09/2015
*/
session_start();
include("../conectar.php");
include("../JSON.php");
$compcod=$_REQUEST["comp"];
$aprcod=$_REQUEST["cod"];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="sinhead">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>editar tipo inventario</title>
<link href="../<?php echo $css; ?>" rel="stylesheet" type="text/css" media="screen" />
<script language="JavaScript" src="../javascript/jquery.js"></script>
<script language="JavaScript" src="../javascript/javascript.js"></script>
<script language="JavaScript" src="javascript.js"></script>
</head>
<body class="sinbody" bgcolor="#FFFFFF" onload="javascript:editar('<?php echo $compcod;?>','<?php echo $aprcod;?>')">

<div id="aediv" class="white_content">

        <form id="form" name="form" method="post" action="" class="form">
		        <input name="compcod" id="compcod" type="hidden" value="<?php echo $compcod;?>">
                <table width="100%"  border="0">
                  <tr>
                    <th width="20%" scope="col"><label>Serial</label></th>
                    <th width="36%" id="wsaprcod" align="left" scope="col"></th>
                    <th colspan="2" id="erreaprcod" class="Estilo5" scope="col">&nbsp;
                    <input name="aprcod" id="aprcod" type="hidden"></th>
                  </tr>
                  <tr>
                    <th scope="col"><label>Descripción</label></th>
                    <th scope="col"><div align="left">
                    <input name="aprdes" id="aprdes" type="text" size="50" maxlength="100"></input>
                    </div></th>
                    <th colspan="2" id="erreaprdes" class="Estilo5" scope="col">&nbsp;</th>
                  </tr>
                  <tr>
                    <th scope="col"><label>Modelo</label></th>
                    <th scope="col"><div align="left">
                      <input name="aprmod" id="aprmod" type="text" size="50" maxlength="50"></input>
                    </div></th>
                    <th colspan="2" id="erreamcnoi" class="Estilo5" scope="col">&nbsp;</th>
                  </tr>
                  
                   <tr>
                    <th scope="col"><label>Ubicación</label></th>
                    <th scope="col"><div align="left">                     
                        <select name="aprubi" id="aprubi">
                            <option value= " " >*** Selecciones ***</option>
								<?php $sql="SELECT UBIC09, DESM09 FROM \"SF.APDATA\".RH009L3 ORDER BY DESM09";
                                        $result1=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111")); 
                                        while(odbc_fetch_row($result1)){			
                                                                $cod=trim(odbc_result($result1,"UBIC09"));
                                                                $des=odbc_result($result1,"DESM09");
								?>
                            <option value= "<?php echo $cod; ?>" ><?php echo $des; ?></option>
                           	 <?php } ?>
                        </select>
                    </div></th>
                    <th colspan="2" id="erramcrec" class="Estilo5" scope="col">&nbsp;</th>
                  </tr>
                  
                  <tr>
                    <th scope="col"><label>Cartucho</label></th>
                    <th scope="col"><div align="left">
                      <input name="aprcar" id="aprcar" type="text" size="50" maxlength="50"></input>
                    </div></th>
                    <th colspan="2" id="erreaprcar" class="Estilo5" scope="col">&nbsp;</th>
                  </tr>
                  
                  <tr>
                    <th scope="col"><label>Usuario</label></th>
                    <th scope="col"><div align="left">
                      <select name="aprusr" id="aprusr">
                          <option value= " " >*** No tiene supervisor ***</option>
                          <?php $sql="Select auscod, ausap1, ausap2, ausno1, ausno2 from mb20fp t1 order by ausno1 asc";
                                    $result1=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111")); 
                                    while(odbc_fetch_row($result1)){			
                                                            $auscod=trim(odbc_result($result1,1));
                                                            $nombre=odbc_result($result1,5)." ".odbc_result($result1,4).", ".odbc_result($result1,3)." ".odbc_result($result1,2);?>
                          <option value= "<?php echo $auscod; ?>" ><?php echo $nombre ?></option>
                          <?php } ?>
                        </select>
                    </div></th>
                    <th colspan="2" id="erreaprusr" class="Estilo5" scope="col">&nbsp;</th>
                  </tr>
                  
                  <tr>
                    <th scope="col">&nbsp;</th>
                    <th scope="col"></th>
                    <!--<th width="23%"  scope="col"><p onclick="marcaseditarcerrar()" class="subir" align="right">salir</p></th>
                    <th width="21%"  scope="col"><p align="right" class="subir" onclick="marcaseditarvalidar()">Grabar</p></th>-->
    				<th width="23%"  scope="col"><input name="cancelar" type="button" onClick="parent.location.reload();parent.Shadowbox.close();"  value="Salir" id="cancelar"></th>
	                <th width="21%"  scope="col"><input type="button" name="submit" id="submit" value="Grabar" onClick="editarvalidar()" /></th>
                  </tr>
                </table>
    	</form>
</div>

<div align="center" id="aftsav">
  <br />
  <br />
  <br />
  <br />
  <br />
  <br />
  Registro Editado con exito !
  <br />
  <br />
  <br />
  <input name="Submit3" type="button" onClick="parent.location.reload();parent.Shadowbox.close();"" " value="Cerrar"></div>
</body>
</html>