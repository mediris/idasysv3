/*
* jdavila 
* 08/09/2015
*/
function agregar()
{
	//$("#compcod").val('');
	// $("#aprcod ").html('');
	$('input[name="aprcod"]').val('');
	$("#wsaprcod").html('');
	$("#aprdes").val('');
	$("#aprmod").val('');
	$("#aprubi").val('');
	$("#aprcar").val('');
	$("#aprusr").val('');
	$("#aftsav").hide(1);
}

/*
jDavila
26/03/12
*/
function agregarvalidar(){	
	var contenido= "";
	var elemento;
	document.body.style.cursor="wait";

	$.ajax({
	   	async: false,
        type: "POST",
		dataType: "JSON",
        url: "prtagregarvalidarphp.php",
        data: $("#form").serialize(),
        success: function(datos){
			vdata = parseJSONx(datos);}							
	});			
	sierror='N';
	for(i in vdata)	{
		if(vdata[i].campo) 	{
			document.getElementById(vdata[i].campo).innerHTML=vdata[i].msg;
			sierror='S';
		}
	}
	document.body.style.cursor="default";
	if(sierror=='N') 		
	{
		$("#aediv").hide(1000);
		$("#aftsav").show(1000);
	}
}

/*
jDavila
26/03/12
*/
function editar(comp,cod) {

	var param = [];
	param['compcod']=comp;
	param['aprcod']=cod;
	ejecutasqlp("prtinformacionphp.php",param);

	for(i in gdata)
	{
		$("#compcod").val(gdata[i].ACICOD);
//		$("#aprcod ").html(gdata[i].APRCOD);
		$('input[name="aprcod"]').val(gdata[i].APRCOD);
		$("#wsaprcod").html(gdata[i].APRCOD);
		$("#aprdes").val(gdata[i].APRDES);
		$("#aprmod").val(gdata[i].APRMOD);
		$("#aprubi").val(gdata[i].APRUBI);
		$("#aprcar").val(gdata[i].APRCAR);
		$("#aprusr").val(gdata[i].APRUSR);
	};
	$("#aftsav").hide(1);
}

/*
jDavila
26/03/12
*/
function editarvalidar(){	
	var contenido= "";
	var elemento;
	document.body.style.cursor="wait";
	$.ajax({
			async: false,
			type: "POST",
			dataType: "JSON",
			url: "prteditarvalidarphp.php",
			data: $("#form").serialize(),
			success: function(datos){
						vdata = parseJSONx(datos);}							
	});			
	var sierror='N';
	for(i in vdata)	{
		if(vdata[i].campo) 	{
			document.getElementById(vdata[i].campo).innerHTML=vdata[i].msg;
			sierror='S';
		}
	}
	document.body.style.cursor="default";
	if(sierror=='N') 		
	{
		$("#form").hide(1000);
		$("#aftsav").show(1000);	
	}

}

/*
jDavila
26/03/12
*/
function eliminar(comp,cod) {

	
	if (confirm('Seguro que desea borrar la Impresora ' + cod + '?'))
	{
		var param = [];
		param['compcod']=comp;
		param['aprcod']=cod;
		ejecutasqld("prteliminar.php",param);
		location.reload();
	}
}


