/*
jDavila
28/03/12
*/
function agregar()
{
	document.form.ammcod.value="";
	document.form.ammdes.value="";
	document.form.ammtip.value="";
	$("#aftsav").hide(1);
}

/*
jDavila
28/03/12
*/
function agregarvalidar(){	
	var contenido= "";
	var elemento;
	document.body.style.cursor="wait";

	$.ajax({
	   	async: false,
        type: "POST",
		dataType: "JSON",
        url: "multimediaagregarvalidarphp.php",
        data: $("#form").serialize(),
        success: function(datos){
			vdata = parseJSONx(datos);}							
	});			
	sierror='N';
	for(i in vdata)	{
		if(vdata[i].campo) 	{
			document.getElementById(vdata[i].campo).innerHTML=vdata[i].msg;
			sierror='S';
		}
	}
	document.body.style.cursor="default";
	if(sierror=='N') 		
	{
		$("#aediv").hide(1000);
		$("#aftsav").show(1000);
	}
}

/*
jDavila
28/03/12
*/
function editar(tipo) {

	var param = [];
	param['ammcod']=tipo;
	ejecutasqlp("multimediainformacionphp.php",param);

	for(i in gdata)
	{
		document.form.ammcod.value=gdata[i].AMMCOD;
		document.getElementById("wsammcod").innerHTML=gdata[i].AMMCOD; //codigo del indice
		document.form.ammdes.value=gdata[i].AMMDES;
		document.form.ammtip.value=gdata[i].AMMTIP;
	};
	$("#aftsav").hide(1);
}

/*
jDavila
28/03/12
*/
function editarvalidar(){	
	var contenido= "";
	var elemento;
	document.body.style.cursor="wait";
	$.ajax({
			async: false,
			type: "POST",
			dataType: "JSON",
			url: "multimediaeditarvalidarphp.php",
			data: $("#form").serialize(),
			success: function(datos){
						vdata = parseJSONx(datos);}							
	});			
	var sierror='N';
	for(i in vdata)	{
		if(vdata[i].campo) 	{
			document.getElementById(vdata[i].campo).innerHTML=vdata[i].msg;
			sierror='S';
		}
	}
	document.body.style.cursor="default";
	if(sierror=='N') 		
	{
		$("#form").hide(1000);
		$("#aftsav").show(1000);	
	}

}

/*
jDavila
28/03/12
*/
function eliminar(tipo) {
	if (confirm('Seguro que desea borrar el multimedia ' + tipo + '?'))
	{
		var param = [];
		param['ammcod']=tipo;
		ejecutasqld("multimediaeliminar.php",param);
		location.reload();
	}
}


