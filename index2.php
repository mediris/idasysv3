<?php 
	session_start();
	include("conectar.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>Idasys V3</title>
    <link href="<?php echo $css; ?>" rel="stylesheet" type="text/css" media="screen" />
    <script language="JavaScript" src="javascript/javascript.js"></script>
    <script language="JavaScript" src="javascript/jquery.js"></script>
    
    <script language="JavaScript" type="text/JavaScript">
    	$(window).scroll(function(){actualizascroll();})
    </script>
    
    <script language="JavaScript" type="text/JavaScript">
		<!--
		function MM_reloadPage(init) {  //reloads the window if Nav4 resized
			if (init==true) with (navigator) {
				if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
					document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; 
				}
			}
			else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
		}
		MM_reloadPage(true);
		//-->
    </script>
    <style>
	#loginform {
	  width: 800px;
	  font-weight: 300;
	  color: #000;
	  text-shadow: 1px 1px 2px rgba(0, 0, 0, 0.9);
	}
	#loginform1{
		border:1;
		alignment-adjust: central;
		width: 350px;
		background-color:#E6E6E6;
		background: rgba(0, 0, 0, 0.15);
		border: 0;
		border-radius: 5px;
	}
	input, label {
	  font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
	  font-size: 15px;
	  font-weight: 300;
	  -webkit-box-sizing: border-box;
	  -moz-box-sizing: border-box;
	  box-sizing: border-box;
	}
	
	input[type=text], input[type=password] {
	  padding: 0 10px;
	  width: 150px;
	  height: 30px;
	  text-shadow: 1px 1px 1px black;
	  border: 0;
	  border-radius: 5px;
	  -webkit-box-shadow: inset 0 1px 4px rgba(0, 0, 0, 0.3), 0 1px rgba(255, 255, 255, 0.06);
	  box-shadow: inset 0 1px 4px rgba(0, 0, 0, 0.3), 0 1px rgba(255, 255, 255, 0.06);
	}
	input[type=text]:focus, input[type=password]:focus {
	  color: white;
	  background: rgba(0, 0, 0, 0.11);
	  outline: 0;
	   text-shadow: 1px 1px 1px black;
	}
	</style>
</head>
<body class="login">
<div id="headerlogin">
    <div id="hlogo">
        &nbsp;
    </div>
</div>
<!-- end header -->

<div style="width:100%; margin-top:50px">
    
            <table width="58%" border="0" valign="middle" align="center">
                <tr valign="middle" align="center">
                	<td>
                        <form name="loginform" id="loginform" method="post" action="javascript:procesarlogin()">
                            <fieldset id="loginform1">
								<?php if ($Usuario=="") { ?>
                                        <table border="0">
                                            <tr>
                                                <td width="280px">
                                                    <div align="right"> 
                                                         <label>Usuario..:</label>
                                                         <input name="usuario" type="text" id="usuario2" size="10" maxlength="10" onkeyup="loginform.usuario.value=loginform.usuario.value.toUpperCase()" />
                                                    </div>
                                                    <div align="right"> 
                                                         <label>Contrase&ntilde;a..:</label>
                                                         <input name="contrasena" type="password" id="contrasena" size="10" maxlength="10" onkeyup="loginform.contrasena.value=loginform.contrasena.value.toUpperCase()" />
                                                    </div>
                                                </td>
                                                <td >
                                                    <div id="botonentrar" align="left">
                                                        <input type="submit" name="entrar" id="entrar" value="Entrar" />
                                                        <input name="accion" type="hidden" id="accion" value="*on" />
                                                    </div>
                                                    <div id="esperardiv" align="left" style="display:none;"> <img src="images/Espera.gif" width="20" height="20" /></div>
                                                 </td>
                                             </tr>
                                         </table>
                                         
                                <?php } else { ?>
                                        <script>
                                            top.location.href = "/idasysv3/index.php"; 
                                        </script>
                                <?php } ?>
                            </fieldset>
                        </form>
                    </td>
                </tr> 
                
                <tr valign="bottom">
                    <td  colspan="2">
                        <div id="errusuario" class="Estilo5" align="center"></div>
                    </td>
                </tr>   
                <tr valign="middle" align="left">
                    <td colspan="2">
                    &nbsp;
                     <!--<br /><br /> <br />
                        <table align="center">
                        	<tr valign="middle" >
                                <td align="center" valign="middle" >
                                    &nbsp;<img src="images/MEDITRON_logo_rif.png" height="40" alt="MEDITRON" />&nbsp;
                                    <br /><br />
                                </td>
                            </tr>
                            <tr valign="middle">
                               <td align="center" valign="middle" >
                                    &nbsp;<img src="images/IDACA_Logo_rif.png" height="60" alt="IDACA" />&nbsp;
                                    <br /><br />
                                </td>
                            </tr>
                            <tr valign="middle">
                                <td align="center" valign="middle" >
                                    &nbsp;<img src="images/SEFIMEDCA_logo_rif.png" height="90" alt="SEFIMEDCA" />&nbsp;
                                    <br /><br />
                                </td>
                            </tr>
                            
                        </table>-->
                    </td>
                </tr>   
            </table>
        
</div>
    
</body>