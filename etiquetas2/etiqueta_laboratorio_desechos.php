<?php
    include_once('../conectar.php');
?>
<page backtop="1mm">
    <table style="border-style: dotted; height: 75mm; width: 100mm; vertical-align: middle;" border="0.5px" cellspacing="0" cellpadding="0" align="center">
        <tr>
            <td style="width: 50%; font-size: 9px; text-align: center; vertical-align: top; padding: 6px 0px 6px 0px;">
                <img height="22" src="logomeditronnuevo.png" alt="Meditron" />
                <br/>
                <strong><?php echo $Companiarif; ?></strong>
            </td>
            <td style="width: 38%; font-size: 30px; text-align: center; vertical-align: middle">
                <img height="22" src="logo_material_radioactivo.png" alt="Material Radioactivo" />
            </td>
        </tr>
        <tr>
            <td colspan="2" style="font-size: 11px; text-align: center; vertical-align: central; padding: 6px 0px 6px 3px;">
                <?php if($aalcod == '0004'){ ?>
                    <strong>Desechos Peligrosos Iodo (I-131)</strong>
                <?php }else{ ?>
                    <strong>Desechos Peligrosos Tecnecio (Tc-99m)</strong>
                <?php } ?>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="font-size: 11px; text-align: left; vertical-align: central; padding: 5px 0px 5px 3px;">
                <strong>Desecho: </strong><?php echo $ardes != '' ? wordwrap(utf8_encode($ardes),40,"<br>") : '--'; ?> (<?php echo $arcod; ?>)
            </td>
            <!-- <?php if($cant == 1){ ?>
                <td colspan="2" style="font-size: 11px; text-align: left; vertical-align: central; padding: 5px 0px 5px 3px;">
                    <strong>Desecho: </strong><?php echo $ardes != '' ? wordwrap(utf8_encode($ardes),40,"<br>") : '--'; ?> (<?php echo $arcod; ?>)
                </td>
            <?php }else{ ?>
                <td colspan="2" style="font-size: 11px; text-align: left; vertical-align: central; padding: 5px 0px 5px 3px;">
                <strong>Desecho: </strong>Agujas Descartables (Aguja (espinal) 22G x 1", Aguja 16G x 3", Jeringa Qosina, Puntas Pipetas Blanca)
            </td>
            <?php } ?> -->
        </tr>
        <tr>
            <?php if($tipo == '0001'){ ?>
                <td style="font-size: 11px; text-align: left; vertical-align: central; padding: 5px 0px 5px 3px;">
                    <strong>Lote N°: </strong><?php echo $atrlot != '' ? trim($atrlot) : '--'; ?>
                </td>
                <td style="font-size: 11px; text-align: left; vertical-align: central; padding: 5px 0px 5px 3px;">
                    <strong>N° Producción: </strong><?php echo $nropro != '' ? trim($nropro) : '--'; ?>
                </td>
            <?php }else if($tipo == '0002'){ ?>
                <td colspan="2" style="font-size: 11px; text-align: left; vertical-align: central; padding: 5px 0px 5px 3px;">
                    <strong>N° Producción: </strong><?php echo $nropro != '' ? trim($nropro) : '--'; ?>
                </td>
            <?php }else{ ?>
                <td colspan="2" style="font-size: 11px; text-align: left; vertical-align: central; padding: 5px 0px 5px 3px;">
                    <strong>Lote N°: </strong><?php echo $atrlot != '' ? trim($atrlot) : '--'; ?>
                </td>
            <?php } ?>
        </tr>
        <tr>
            <td colspan="2" style="font-size: 11px; text-align: left; vertical-align: central; padding: 5px 0px 5px 3px;">
                <strong>Fecha de ingreso: </strong><?php echo $fecing != '' ? trim($fecing) : '--'; ?>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="font-size: 11px; text-align: left; vertical-align: central; padding: 5px 0px 5px 3px;">
                <strong>Tasa de dosis a la fecha de ingreso: </strong><?php echo $acting != '' ? utf8_encode(trim($acting)) : '--'; ?> µSv/hr
            </td>
        </tr>
        <tr>
            <td colspan="2" style="font-size: 11px; text-align: left; vertical-align: central; padding: 5px 0px 5px 3px;">
                <strong>Fecha de salida: </strong><?php echo $fecsal != '' ? trim($fecsal) : '--'; ?>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="font-size: 11px; text-align: left; vertical-align: central; padding: 5px 0px 5px 3px;">
                <strong>Tasa de dosis a la fecha de salida: </strong> 0 µSv/hr
            </td>
        </tr>
    </table>
    <div style="position: relative; left: 23px; top: 5px;">
        <strong style="font-size: 9px; vertical-align: central; padding: 5px 0px 3px 8px;">F-LAB-045-06-21</strong>
    </div>
</page>