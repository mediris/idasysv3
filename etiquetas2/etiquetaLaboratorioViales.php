<?php
include_once('../conectar.php');

?>
<style>

    .etiqueta{
        border-style: dotted; 
        height: 75mm; 
        width: 100mm;
    }

    .margin-top{
        margin-top: 20px;
    }

    .logo{
        width:45%; 
        padding: 5px 0px 5px 2px;
        text-align: center;
    }

    .titulo-material-radiactivo{
        width:45%;
        font-size:11px;
        padding: 5px 0px 7px 2px;
    }

    .texto-etiqueta-1{
        font-size:11px; 
        padding: 4px 3px 4px 3px;
    }

    .height-10{
        height: 10px;
    }

</style>
<page backtop="1mm">
    <table class="etiqueta margin-top" border="0.5px" cellspacing="0" cellpadding="0" align="center" >
        <tr>
            <td class="logo">
                <img height="15" src="logomeditronnuevo.png" alt="MEDITRON" />
            </td>
            <td class="titulo-material-radiactivo">
            	<strong>MATERIAL RADIACTIVO</strong>
                <img height="18" src="logo_material_radioactivo.png" alt="MEDITRON" />
            </td>
        </tr>
        <tr>
            <td colspan="2" class="texto-etiqueta-1 height-10">
                <strong>Institución:</strong> <?php echo utf8_encode(wordwrap(trim($data[0]), 35, "<br>")); ?>
            </td>
        </tr>
        <tr>
            <td colspan="2" class="texto-etiqueta-1 height-10">
                <strong>Condiciones: </strong> TRASLADAR EN BLINDAJE PLOMADO
            </td>
        </tr>
    </table>
    <table class="etiqueta margin-top" border="0.5px" cellspacing="0" cellpadding="0" align="center" >
        <tr>
            <td class="logo">
                <img height="15" src="logomeditronnuevo.png" alt="MEDITRON" />
            </td>
        </tr>
        <tr>
            <td class="titulo-material-radiactivo" style="text-align: center;">
                <strong>MATERIAL RADIACTIVO</strong>
                <img height="18" src="logo_material_radioactivo.png" alt="MEDITRON" />
            </td>
        </tr>
        <tr>
            <td colspan="2" class="texto-etiqueta-1 height-10">
                Solución intravenosa de Tecnecio-99m
            </td>
        </tr>
    </table>
</page>