<?php
include_once('../conectar.php');

?>
<page backtop="1mm">
<table style="border-style:dotted;height:75mm;width:100mm;vertical-align:middle;" border="0.5px" cellspacing="0" cellpadding="0" align="center" >
  <tr>
    <td style="width:50%;font-size:9px;text-align:center;vertical-align:top" >
            <img height="20" src="logomeditronnuevo.png" alt="MEDITRON" />&nbsp;&nbsp;&nbsp;
            <img height="10" src="logo_medix.png" alt="MEDIX" />
            <!--<img height="10" src="etiqueta_clip_image001.png" alt="Meditron" />-->
            <br/>            
            <strong><?php echo 'CONTRATO No. 039-2015<br />ADDENDUM 001-2016'; ?></strong>
    </td>
    <td style="width:38%;font-size:15px;text-align:center;vertical-align:middle">
        	<strong>
            <?php 

				if($aticod=='0001'){
					echo "EQ";
				}else if($aticod=='0002'){
					echo "CNS";
				}else if($aticod=='0003'){
					echo "RPT";
				}else {
					echo "-";
				}
				if($aalcod=='0001')	echo "<strong>-BI</strong>";
				if($aalcod=='0002')	echo "<strong>-NV</strong>";
				if($aalcod=='0003')	echo "<strong>-RP</strong>";
				
				if($asicod==1)		echo "<strong>-MEC</strong>";
				if($asicod==2)		echo "<strong>-MED</strong>";
				if($asicod==3)		echo "<strong>-RX</strong>";
			?>
            </strong>
    </td>
  </tr>
  <tr>
    <td colspan="2" style="font-size:9px;text-align:left;vertical-align:central;">
		No. Parte: <strong style="font-size:15px"><?php echo $arcod; ?></strong>
    </td>
  </tr>
  <tr style="border-right:none;border-left:none;">
    <td colspan="2" style="height:20mm;text-align:center;vertical-align:middle;font-size:9px;border-right:none;border-left:none;">
        	<barcode type="C39" value="<?php echo $arcod; ?>" style="width:70mm; height:15mm"></barcode>
			<!--<img src="etiqueta_clip_image005.jpg" alt="barra" width="161" height="32"/>-->
    </td>
  </tr>
  <tr style="border-right:none;border-left:none;">
  	<td colspan="2" style="height:20mm;text-align:left;vertical-align:top;font-size:9px;border-right:none;border-left:none;" >
        	<div style="width:330px;">
                DESCRIPCIÓN: 
                <!--17 caracteres en mayuscula-->
                <?php 
                $font='font-size:12px';
                //if($ardeslar!=''){$font = ""; $long='40'; }else{ $font='font-size:15px'; $long='30';} 
                ?>
                <!--<strong style=" <?php echo $font; ?> "><?php echo utf8_encode( wordwrap($ardeslar!=''?$ardeslar:$ardes,$long,'<br />', true)); ?></strong>-->
                <strong style=" <?php echo $font; ?>;">
                    <?php 
                        $desetq = '';
                        echo utf8_encode(trim($ardes));
                    ?>
                </strong>
                
                <?php 
                        $desetq .=@ (odbc_result($result,'AMCDES')!='' && trim(odbc_result($result,'AMCDES'))!='SIN MARCA')?" / Marca: ".trim(odbc_result($result,'AMCDES')):'';
                        $desetq .=@ odbc_result($result,'MODELOMEDIX')!=''?" / Modelo: ".trim(odbc_result($result,'MODELOMEDIX')):'';
                        $desetq .=@ !empty($atrlot)?" / <strong>Serial: ".trim($atrlot).'</strong>':'';
                        echo utf8_encode($desetq); 
                ?>
            </div>
            
    </td>
  </tr>
  <tr>
    <td style="font-size:9px;text-align:left;vertical-align:middle" >
		UBICACIÓN: <strong style="font-size:9px"><?php $list = list_ubiart($cid, $Compania, $alma, $arcod,3); echo $list!=''?$list:"Sin Ubicación";?></strong>
    </td>
    <td style="font-size:9px;text-align:left;vertical-align:middle" >
        No. TRANSACCION: <strong style="font-size:9px"><?php echo $nrotra;?></strong>
    </td>
  </tr>
</table>
</page>
