<?php
    include_once('../conectar.php');
?>
<page backtop="1mm">
    <table style="border-style: dotted; height: 75mm; width: 100mm; vertical-align: middle;" border="0.5px" cellspacing="0" cellpadding="0" align="center">
        <tr>
            <td style="width: 50%; font-size: 9px; text-align: center; vertical-align: top; padding: 3px 0px 3px 0px;">
                <img height="22" src="logomeditronnuevo.png" alt="Meditron" />
                <!-- <img height="15" src="etiqueta_clip_image003.png" alt="INVAP" /> -->
                <br/>            
                <!-- <strong><?php echo 'CONTRATO No. 122-2014<br />INVAP-MPPS'; ?></strong> -->
                <strong>LABORATORIO</strong>
            </td>
            <td style="width: 38%; font-size: 30px; text-align: center; vertical-align: middle">
                <strong>
                <?php 
				    if($aticod == '0001'){
					   echo "MP";
				    }else if($aticod == '0002'){
					   echo "CON";
				    }else if($aticod == '0003'){
                        echo "MO";
                    }else if($aticod == '0004'){
				       echo "PF";
				    }else {
					   echo "-";
				    }
                ?>
                </strong>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="font-size: 11px; text-align: left; vertical-align: central; padding: 3px 0px 3px 3px;">
                N° lote transacción: <strong style="font-size: 11px"><?php echo $atrlot != '' ? trim($atrlot) : '--'; ?></strong>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="font-size: 11px; text-align: left; vertical-align: central; padding: 3px 0px 3px 3px;">
                Código del artículo: <strong style="font-size: 11px"><?php echo $arcod != '' ? trim($arcod) : '--'; ?></strong>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="height: 20mm; text-align: center; vertical-align: middle; font-size: 9px;">
        	   <barcode type="C39" value="<?php echo $arcod; ?>" style="width: 55mm; height: 10mm"></barcode>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="height: 15mm; text-align: left; vertical-align: top; font-size: 9px;">
                DESCRIPCION: 
                <!--17 caracteres en mayuscula-->
                <?php 
                    $long='40'; 
                    $font='font-size:12px';
                    //if($ardeslar!=''){$font = ""; $long='40'; }else{ $font='font-size:15px'; $long='30';} 
                ?>
      		    <!--<strong style=" <?php echo $font; ?> "><?php echo utf8_encode( wordwrap($ardeslar!=''?$ardeslar:$ardes,$long,'<br />', true)); ?></strong>-->
                <strong style=" <?php echo $font; ?> ">
				<?php 
					$desetq = trim($ardes);
					
					$desetq .=@ (odbc_result($result,'AMCDES')!='' && trim(odbc_result($result,'AMCDES'))!='SIN MARCA')?" / Marca: ".trim(odbc_result($result,'AMCDES')):'';
					$desetq .=@ odbc_result($result,'MODELO')!=''?" / Modelo: ".trim(odbc_result($result,'MODELO')):'';
					$desetq .=@ odbc_result($result,'SERIAL')!=''?" / Serial: ".trim(odbc_result($result,'SERIAL')):'';
					echo utf8_encode( wordwrap($desetq,$long,'<br />', true)); ?>
                </strong>
            </td>
        </tr>
        <tr>
            <td style="font-size: 9px; vertical-align: middle; padding: 3px 0px 3px 5px;">
                UBICACIÓN: <strong style="font-size: 9px"><?php $list = list_ubiart($cid, $Compania, $alma, $arcod,3); echo $list!=''?$list:"Sin Ubicación";?></strong>
            </td>
            <td style="font-size: 9px; vertical-align: middle; padding: 3px 0px 3px 5px;">
                N°. TRANSACCION: <strong style="font-size:9px"><?php echo $nrotra;?></strong>
            </td>
        </tr>
    </table>
</page>