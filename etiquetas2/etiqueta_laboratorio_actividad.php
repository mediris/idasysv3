<?php
include_once('../conectar.php');

?>
<style>

    .etiqueta{
        border-style: dotted; 
        height: 75mm; 
        width: 100mm;
    }

    .margin-top{
        margin-top: 20px;
    }

    .logo{
        width:45%; 
        padding: 5px 0px 5px 2px;
        text-align: center;
    }

    .titulo-material-radiactivo{
        width:45%;
        font-size:11px;
        padding: 5px 0px 7px 2px;
    }

    .texto-etiqueta-1{
        font-size:11px; 
        padding: 4px 3px 4px 3px;
    }
</style>
<page backtop="1mm">
    <table class="etiqueta margin-top" border="0.5px" cellspacing="0" cellpadding="0" align="center">
        <tr>
            <td class="logo">
                <img height="15" src="logomeditronnuevo.png" alt="MEDITRON" />
            </td>
            <td class="titulo-material-radiactivo">
                <strong>MATERIAL RADIACTIVO</strong>
                <img height="18" src="logo_material_radioactivo.png" alt="MEDITRON" />
            </td>
        </tr>
        <tr>
            <td class="texto-etiqueta-1">
               <strong>Actividad: </strong><?php echo utf8_encode(trim($data[0])); ?> <?php echo utf8_encode(trim($uniMedida));?>
            </td>
            <td class="texto-etiqueta-1">
               <strong>N° nota de entrega: </strong><?php echo add_ceros($numdot,6); ?>
            </td>
        </tr>
        <tr>
            <td colspan="2" class="texto-etiqueta-1">
               <strong>Fecha y hora de calibración: </strong><?php echo utf8_encode(trim($data[3])); ?>
            </td>
        </tr>
        <tr>
            <td colspan="2" class="texto-etiqueta-1">
                <strong>Fecha de caducidad: </strong><?php echo utf8_encode(trim($data[2])); ?>
            </td>
        </tr>
    </table>

    <table class="etiqueta margin-top" border="0.5px" cellspacing="0" cellpadding="0" align="center">
        <tr>
            <td class="logo">
                <img height="15" src="logomeditronnuevo.png" alt="MEDITRON" />
            </td>
            <td class="titulo-material-radiactivo">
                <strong>MATERIAL RADIACTIVO</strong>
                <img height="18" src="logo_material_radioactivo.png" alt="MEDITRON" />
            </td>
        </tr>
        <tr>
            <td class="texto-etiqueta-1">
               <strong>Actividad: </strong><?php echo utf8_encode(trim($data[0])); ?> <?php echo utf8_encode(trim($uniMedida));?>
            </td>
            <td class="texto-etiqueta-1">
               <strong>N° nota de entrega: </strong><?php echo add_ceros($numdot,6); ?>
            </td>
        </tr>
        <tr>
            <td colspan="2" class="texto-etiqueta-1">
               <strong>Fecha y hora de calibración: </strong><?php echo utf8_encode(trim($data[3])); ?>
            </td>
        </tr>
        <tr>
            <td colspan="2" class="texto-etiqueta-1">
                <strong>Fecha de caducidad: </strong><?php echo utf8_encode(trim($data[2])); ?>
            </td>
        </tr>
    </table>
</page>