<?php session_start();
	
	include_once("../conectar.php");
	require_once('html2pdf/html2pdf.class.php');

	$numdot = trim($_GET['num']);
	$atrcod = trim($_GET['atrcod']);

	/* Select para consultar los paramatros adicionales de la dotacion */
	$sql = "SELECT T1.ACICOD, T1.ATRCOD, T1.AMDCOD, T1.APDCOD, T2.APDDES, T2.APDSEC, T2.APDLON, T2.APDDCR, T2.APDTIP, T2.APDVAL, T2.ATACOD, T2.ASBCOD, T2.APDSTS, T2.APDLND, T3.ASBDES, T3.ASBSTS, T4.APACOD, T4.ADSNRO, T4.AAPVLA, T4.AAPDES, T4.AAPLON, T4.AAPLND, T4.AAPSEC, T4.AAPTIP, T4.ATSCOD FROM MB10FP T3 INNER JOIN MB03FP T2 ON( T2.ATACOD=T3.ATACOD AND T2.ASBCOD=T3.ASBCOD) INNER JOIN IV37FP T1 ON ( T1.APDCOD=T2.APDCOD AND T1.AMDCOD=T2.AMDCOD and T1.ATRCOD=(SELECT T7.ATRSAL FROM is15fp T7 WHERE T7.ACICOD=T1.ACICOD and T7.ATSCOD='".$atrcod."')) LEFT JOIN IV46FP T4 ON ( T1.ACICOD=T4.ACICOD AND T4.ADSNRO='".$numdot."' AND T1.APDCOD=T4.APACOD and T4.ATSCOD='".$atrcod."') WHERE T1.ACICOD='".$Compania."' AND T1.AMDCOD='".$modulo."' ORDER BY T2.ASBCOD, T2.APDSEC, T1.APDCOD";

	// $sql2 = "SELECT T2.AUMDES FROM IV36FP T1 INNER JOIN IV13FP T2 ON(T1.ACICOD = T2.ACICOD AND T2.AUMCOD = T1.ATRUMB) WHERE T1.ACICOD = '".$Compania."' AND T1.ADSNRO = '".$numdot."'";

	$sql2 = "SELECT T1.AARCOD, T2.AARDES, T3.ALTCOD, T4.AUMDES FROM IV36FP T1  INNER JOIN IV05FP T2 ON(T2.AARCOD = T1.AARCOD) INNER JOIN IV48FP T3 ON(T3.AARCOD = T1.AARCOD) INNER JOIN IV13FP T4 ON(T4.ACICOD = T1.ACICOD AND T4.AUMCOD = T1.ATRUMB)  WHERE T1.ACICOD = '".$Compania."' AND T1.ADSNRO = '".$numdot."'";

	$result=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111"));
	$result2=odbc_exec($cid,$sql2)or die(exit("Error en odbc_exec 11111"));

	while(odbc_fetch_row($result2)){
		$codigo = odbc_result($result2, "AARCOD");
		$descripcion = odbc_result($result2, "AARDES");
		$loteArt = odbc_result($result2, "ALTCOD");

		if(trim(odbc_result($result2, "AUMDES")) != 'Pieza'){
			$uniMedida = odbc_result($result2, "AUMDES");
		}
	}
	
	ob_start();
	$data = [];
	while(odbc_fetch_row($result)){
		array_push($data, odbc_result($result, "AAPVLA"));
	}
	
	if(trim($_GET['atrcod']) == '02'){
		include('etiqueta_laboratorio_actividad.php');
	}else if(trim($_GET['atrcod']) == '04'){
		include('etiqueta_laboratorio_viales.php');
	}

	$content = ob_get_clean();
	$html2pdf = new HTML2PDF('landscape', array(65,100), 'es',array(0, 0, 0, 0), 'UTF-8', 0);
	$html2pdf->WriteHTML($content);
	$html2pdf->Output('etiqueta.pdf');

?>