<?php
include_once('../conectar.php');

?>
<style>

    .etiqueta{
        border-style: dotted; 
        height: 75mm; 
        width: 100mm;
    }

    .margin-top{
        margin-top: 20px;
    }

    .logo{
        width:45%; 
        padding: 5px 0px 5px 2px;
        text-align: center;
    }

    .titulo-material-radiactivo{
        width:45%;
        font-size:10px;
        padding: 5px 0px 7px 5px;
    }

    .texto-etiqueta-1{
        font-size:11px; 
        padding: 4px 3px 4px 3px;
    }

    .height-10{
        height: 10px;
    }

</style>
<page backtop="1mm">
    <table class="etiqueta margin-top" border="0.5px" cellspacing="0" cellpadding="0" align="center" >
        <tr>
            <td class="logo">
                <img height="12" src="logomeditronnuevo.png" alt="MEDITRON" />
            </td>
            <td class="titulo-material-radiactivo">
                <strong>MATERIAL RADIACTIVO</strong>
                <img height="12" src="logo_material_radioactivo.png" alt="MEDITRON" />
            </td>
        </tr>
        <tr>
            <td colspan="2" class="texto-etiqueta-1 height-10">
                <strong>Nombre del medicamento:</strong> <?php echo utf8_encode(wordwrap(trim($descripcion), 35, "<br>")); ?> <strong>(<?php echo utf8_encode(substr(trim($codigo),0,20)); ?>)</strong>. <strong>Lote: </strong> <?php $lote = !empty(trim($loteArt)) ? trim($loteArt) : "-------"; echo $lote; ?>
            </td>
        </tr>
        <tr>
            <td class="texto-etiqueta-1">
               <strong>N° nota de entrega: </strong><?php echo add_ceros($numdot,6); ?>
            </td>
            <td class="texto-etiqueta-1">
                <!-- <strong>Lote: </strong> <?php $lote = !empty(trim($loteArt)) ? trim($loteArt) : "-------"; echo $lote; ?> -->
                <strong>F/H Calibración: </strong><?php echo utf8_encode(trim($data[1])); ?>
            </td>
        </tr>
        <tr>
            <td class="texto-etiqueta-1">
                <strong>Actividad (mCi): </strong><?php echo utf8_encode(trim($data[0])); ?>
            </td>
            <td class="texto-etiqueta-1">
                <!-- <strong>F/H Calibración: </strong><?php echo utf8_encode(trim($data[1])); ?> -->
                <strong>Fecha Vencimiento: </strong> <?php echo utf8_encode(trim($data[4])); ?>
            </td>
            
        </tr>
        <tr>
            <td class="texto-etiqueta-1">
                <strong>Volumen (ml): </strong><?php echo utf8_encode(trim($data[5])); ?>
            </td>
            <td class="texto-etiqueta-1">
                <<strong>Vía admin: </strong>Intravenoso
            </td>
        </tr>
    </table>
</page>