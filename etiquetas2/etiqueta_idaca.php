<!-- <?php
    include_once('../conectar.php');
?>
<page backtop="3mm">
    <table style="border-style:dotted;height:75mm;width:100mm;vertical-align:middle;" border="0.5px" cellspacing="0" cellpadding="0" align="center" >
        <tr>
            <td style="height:10mm;font-size:9px;text-align:center;vertical-align:middle; padding: 5px">
                <img height="30" src="../images/logoidacadef2005.png" alt="IDACA" />
            </td>
        </tr>
        <?php if($nrotra != '-'){ ?>
            <tr>
                <td colspan="2" style="font-size:10px;text-align:left;vertical-align:central; padding: 5px">
                    No. TRANSACCION: <strong style="font-size:10px"><?php echo $nrotra;?> (<?php echo $atrdes;?>)</strong>
                </td>
            </tr>
        <?php } ?>
        <tr>
            <td colspan="2" style="height:20mm;text-align:left;vertical-align:top;font-size:14px;" >
                DESCRIPCION: 
                <?php 
                $long='40'; 
                $font='font-size:14px';
                ?>
                <strong style=" <?php echo $font; ?> ">
                    <?php 
                    $desetq = trim($ardes) . " (" . $arcod . ")";
                    echo utf8_encode(wordwrap($desetq,$long,'<br />', true)); ?>
                </strong>
            </td>
        </tr>
        <tr>
            <td style="font-size:10px;text-align:left;vertical-align:middle; padding: 5px" >
                UBICACIÓN: <strong style="font-size:10px"><?php $list = list_ubiart($cid, $Compania, $alma, $arcod,3); echo $list!=''?$list:"Sin Ubicación";?></strong>
            </td>
        </tr> 
    </table>
</page> -->
<?php
    include_once('../conectar.php');
?>
<page backtop="3mm">
    <table border="0.5px" cellspacing="0" cellpadding="0" align="center">
        <tr>
            <td style="height:10mm; width:39.3mm; font-size:16px; text-align:center; vertical-align:middle; padding: 4px">
                <?php echo trim(utf8_encode($arcod)); ?>
            </td>
            <td style="height:10mm; width:39.3mm; font-size:16px; text-align:center; vertical-align:middle; padding: 4px">
                <?php $list = list_ubiart($cid, $Compania, $alma, $arcod,3); echo $list != '' ? $list : "Sin Ubicación";?>
            </td>
        </tr> 
    </table>
    <table style="margin-top: 2px" border="0.5px" cellspacing="0" cellpadding="0" align="center">
        <tr>
            <td style="text-align:center; vertical-align:middle; padding: 4px; width:30mm; font-size: 16px">
                <?php echo trim(utf8_encode($arcod)); ?> 
            </td>
            <td style="text-align:center; vertical-align:middle; padding: 4px; width:30mm; font-size: 16px">
                <?php $list = list_ubiart($cid, $Compania, $alma, $arcod,3); echo $list != '' ? $list : "Sin Ubicación";?> 
            </td>
        </tr>
        <tr>
            <td colspan="2" style="height:16mm; width:85mm; font-size:16px; text-align:center; vertical-align:middle; padding: 4px">
                <?php echo trim(utf8_encode($ardes)); ?>
            </td>
        </tr> 
    </table>
</page>
