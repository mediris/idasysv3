<?php
include_once('../conectar.php');

?>
<style>

    .etiqueta{
        border-style: dotted; 
        height: 75mm; 
        width: 100mm;
    }

    .margin-top{
        margin-top: 20px;
    }

    .logo{
        width:45%; 
        padding: 5px 0px 5px 2px;
        text-align: center;
    }

    .titulo-material-radiactivo{
        width:45%;
        font-size:11px;
        padding: 5px 0px 7px 2px;
    }

    .texto-etiqueta-1{
        font-size:11px; 
        padding: 4px 3px 4px 3px;
    }

    .height-10{
        height: 10px;
    }

</style>
<page backtop="1mm">
    <table class="etiqueta" border="0.5px" cellspacing="0" cellpadding="0" align="center" >
        <tr>
            <td class="logo">
                <img height="15" src="logomeditronnuevo.png" alt="MEDITRON" />
            </td>
            <td class="titulo-material-radiactivo">
            	<strong>MATERIAL RADIACTIVO</strong>
                <img height="18" src="logo_material_radioactivo.png" alt="MEDITRON" />
            </td>
        </tr>
        <tr>
            <td colspan="2" class="texto-etiqueta-1 height-10">
                <strong>Nombre del medicamento:</strong> <?php echo utf8_encode(wordwrap(trim($nombreArticulo), 35, "<br>")); ?>
            </td>
        </tr>
        <tr>
            <td class="texto-etiqueta-1">
                <strong>Código: </strong><?php echo utf8_encode(substr(trim($codArticulo),0,25)); ?>
            </td>
            <td class="texto-etiqueta-1">
                <strong>Lote: </strong> <?php $lote = !empty(trim($loteArticulo)) ? trim($loteArticulo) : "-------"; echo $lote; ?>
            </td>
        </tr>
        <tr>
            <td colspan="2" class="texto-etiqueta-1">
                <strong>Nombre de la empresa: </strong>MEDITRON C.A
            </td>
        </tr>
        <tr>
            <td colspan="2" class="texto-etiqueta-1">
                <strong>Institución: </strong> <?php echo utf8_encode(trim($data[0])); ?>
            </td>
        </tr>
        <tr>
            <td colspan="2" class="texto-etiqueta-1">
                <strong>Paciente: </strong> <?php echo utf8_encode(trim($data[1])); ?> / <strong>C.I.: </strong><?php echo utf8_encode(trim($data[2])); ?>
            </td>
        </tr>
        <tr>
            <td class="texto-etiqueta-1">
                <strong>Cantidad de cápsula: </strong> <?php echo intval(trim($cantidad)); ?>
            </td>
            <td class="texto-etiqueta-1">
                <strong>Vía de administración: </strong>Oral
            </td>
        </tr>
        <tr>
            <td colspan="2" class="texto-etiqueta-1">
               <strong>Fecha y hora de calibración: </strong><?php echo utf8_encode(trim($data[4])); ?>
            </td>
        </tr>
        <tr>
            <td colspan="2" class="texto-etiqueta-1">
                <strong>Fecha de caducidad: </strong><?php echo utf8_encode(trim($data[5])); ?>
            </td>
        </tr>
        <tr>
            <td colspan="2" class="texto-etiqueta-1 height-10">
                <!-- <strong>Conds: </strong> <?php echo utf8_encode(trim($data[3])); ?> -->
				<strong>Conds: </strong> TRASLADAR EN BLINDAJE PLOMADO
            </td>
        </tr>
    </table>
</page>