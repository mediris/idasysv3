<?php
include_once('../conectar.php');

?>
<page backtop="1mm">
<table style="border-style:dotted;height:75mm;width:100mm;vertical-align:middle;" border="0.5px" cellspacing="0" cellpadding="0" align="center" >
  <tr>
    <td style="width:50%;font-size:9px;text-align:center;vertical-align:top" >
            <img height="22" src="logomeditronnuevo.png" alt="INVAP" />&nbsp;&nbsp;&nbsp;
            <img height="15" src="etiqueta_clip_image003.png" alt="INVAP" />
            <!--<img height="10" src="etiqueta_clip_image001.png" alt="Meditron" />-->
            <br/>            
            <strong><?php echo 'CONTRATO No. 122-2014<br />INVAP-MPPS'; ?></strong>
    </td>
    <td style="width:38%;font-size:30px;text-align:center;vertical-align:middle">
        	<strong>
            <?php 
				if($aticod=='0001' || $aticod=='0002'){
					echo "RT";
				}else if($aticod=='0003' || $aticod=='0007'){
					echo "AT";
				}else if($aticod=='0004' || $aticod=='0008'){
					echo "ELM";
				}else if($aticod=='0005' || $aticod=='0009'){
					echo "GTEC";
				}else if($aticod=='0006'){
					echo "GEN";
				}else if($aticod=='0010' || $aticod=='0011'){
					echo "OCP";
				}else {
					echo "-";
				}
				if($aalcod=='0006')
				echo "<strong>-D</strong>";
				
				if($aalcod=='0007')
				echo "<strong>-DM</strong>";
			?>
            </strong>
    </td>
  </tr>
  <tr>
    <td colspan="2" style="font-size:9px;text-align:left;vertical-align:central;">
		No. Parte: <strong style="font-size:15px"><?php echo $arcod; ?></strong>
    </td>
  </tr>
  <tr style="border-right:none;border-left:none;">
    <td colspan="2" style="height:20mm;text-align:center;vertical-align:middle;font-size:9px;border-right:none;border-left:none;">
        	<barcode type="C39" value="<?php echo $arcod; ?>" style="width:70mm; height:15mm"></barcode>
			<!--<img src="etiqueta_clip_image005.jpg" alt="barra" width="161" height="32"/>-->
    </td>
  </tr>
  <tr style="border-right:none;border-left:none;">
  	<td colspan="2" style="height:20mm;text-align:left;vertical-align:top;font-size:9px;border-right:none;border-left:none;" >
        	DESCRIPCION: 
            <!--17 caracteres en mayuscula-->
            <?php 
			$long='40'; 
			$font='font-size:12px';
			//if($ardeslar!=''){$font = ""; $long='40'; }else{ $font='font-size:15px'; $long='30';} 
			?>
      		<!--<strong style=" <?php echo $font; ?> "><?php echo utf8_encode( wordwrap($ardeslar!=''?$ardeslar:$ardes,$long,'<br />', true)); ?></strong>-->
            <strong style=" <?php echo $font; ?> ">
				<?php 
					$desetq = trim($ardes);
					
					$desetq .=@ (odbc_result($result,'AMCDES')!='' && trim(odbc_result($result,'AMCDES'))!='SIN MARCA')?" / Marca: ".trim(odbc_result($result,'AMCDES')):'';
					$desetq .=@ odbc_result($result,'MODELO')!=''?" / Modelo: ".trim(odbc_result($result,'MODELO')):'';
					$desetq .=@ odbc_result($result,'SERIAL')!=''?" / Serial: ".trim(odbc_result($result,'SERIAL')):'';
					echo utf8_encode( wordwrap($desetq,$long,'<br />', true)); ?>
            </strong>
    </td>
  </tr>
  <tr>
    <td style="font-size:9px;text-align:left;vertical-align:middle" >
		UBICACIÓN: <strong style="font-size:9px"><?php $list = list_ubiart($cid, $Compania, $alma, $arcod,3); echo $list!=''?$list:"Sin Ubicación";?></strong>
    </td>
    <td style="font-size:9px;text-align:left;vertical-align:middle" >
        No. TRANSACCION: <strong style="font-size:9px"><?php echo $nrotra;?></strong>
    </td>
  </tr>
</table>
</page>
