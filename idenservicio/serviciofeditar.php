<?php 
/*
* jdavila 
* 16/03/2012
*/
session_start();
include("../conectar.php");
include("../JSON.php");

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="sinhead">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>editar tipo inventario</title>
<link href="../<?php echo $css; ?>" rel="stylesheet" type="text/css" media="screen" />
<script language="JavaScript" src="../javascript/jquery.js"></script>
<script language="JavaScript" src="../javascript/javascript.js"></script>
<script language="JavaScript" src="javascript.js"></script>
</head>
<body class="sinbody" bgcolor="#FFFFFF" onload="javascript:editar('<?php echo $aiscod;?>')">

<div id="aediv" class="white_content">

        <form id="form" name="form" method="post" action="" class="form">
                <table width="100%"  border="0">
                  <tr>
                    <th width="20%" scope="col"><label>Servicio</label></th>
                    <th width="36%" id="wsaiscod" align="left" scope="col">
                    <th colspan="2" id="erreaiscod" class="Estilo5" scope="col">&nbsp;
                    <input name="aiscod" id="aiscod" type="hidden"></th>
                  </tr>
                  <tr>
                    <th scope="col"><label>Descripción</label></th>
                    <th scope="col"><div align="left">
                    <input name="aisdes" id="aisdes" type="text" size="40" maxlength="40"></input>
                    </div></th>
                    <th colspan="2" id="erreaisdes" class="Estilo5" scope="col">&nbsp;</th>
                  </tr>
                  <tr>
                    <th scope="col"><label>Observaciones</label></th>
                    <th scope="col">
                        <div align="left">
                            <label id="caracteres"></label><br />
                   			<textarea name="aisobs" id="aisobs" cols="25" rows="2" onKeyDown="valida_longitud();" onKeyUp="valida_longitud();" ></textarea>
                        </div>
                    </th>
                    <th colspan="2" id="erraisobs" class="Estilo5" scope="col">&nbsp;</th>
                  </tr>
                  <tr>
                    <th scope="col">&nbsp;</th>
                    <th scope="col"></th>
                    <!--<th width="23%"  scope="col"><p onclick="marcaseditarcerrar()" class="subir" align="right">salir</p></th>
                    <th width="21%"  scope="col"><p align="right" class="subir" onclick="marcaseditarvalidar()">Grabar</p></th>-->
    				<th width="23%"  scope="col"><input name="cancelar" type="button" onClick="parent.location.reload();parent.Shadowbox.close();"  value="Salir" id="cancelar"></th>
	                <th width="21%"  scope="col"><input type="button" name="submit" id="submit" value="Grabar" onClick="editarvalidar()" /></th>
                  </tr>
                </table>
    	</form>
</div>

<div align="center" id="aftsav">
  <br />
  <br />
  <br />
  <br />
  <br />
  <br />
  Registro Agregado con exito !
  <br />
  <br />
  <br />
  <input name="Submit3" type="button" onClick="parent.location.reload();parent.Shadowbox.close();"" " value="Cerrar"></div>
</body>
</html>