<?php 
/*
 *jDavila
 *30/03/2012
 */
session_start();
include("conectar.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
<title>Idasys V3</title>
<link href="<?php echo $css; ?>" rel="stylesheet" type="text/css" media="screen" />
<script language="JavaScript" src="javascript/javascript.js"></script>
<script language="JavaScript" src="javascript/jquery.js"></script>

<style type="text/css" title="currentStyle">
			@import "DataTables-1.9.4/media/css/<?php echo $demo_page;?>";
			@import "DataTables-1.9.4/media/css/<?php echo $demo_table;?>";
</style>
<script type="text/javascript" language="javascript" src="DataTables-1.9.4/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf-8">
			
			 $(document).ready(function() 
			 {
				  document.getElementById('info').style.display ="";
			      $('#info').dataTable( 
				  {
					"bStateSave": true,
					"oLanguage": 
					{
			          "sLengthMenu": "Mostrar _MENU_ registros por Pag.",
						"sZeroRecords": "No Existen Registros",
						"sInfo": "Mostrando desde _START_ hasta _END_ de _TOTAL_ registros",
						"sInfoEmpty": "Mostrando desde 0 hasta 0 de 0 registros",
						"sInfoFiltered": "(filtrando de _MAX_ de registros)",
						"sSearch": "Buscar",
					  "oPaginate": 
					  {
			            "sNext": "Siguiente",
						"sPrevious": "Anterior"
			          }
			        }
			      });
			  });
	</script>

</head>
<body>
<div id="wrapper">
  <?php 
  		include("superior.php");
  		?>
  <div id="page">
      <?php include("validar.php");  		?>

<!--<div id="sidebar">
	    <ul>
	      <li>
	        <h2>Estad&iacute;sticas</h2>
	        <p>Aqu&iacute; se reflejar&aacute;n estad&iacute;sticas particulares seg&uacute;n en la opci&oacute;n del sistema que se encuentre</p>
          </li>
	      <li>
	        <h2>Categories</h2>
	        <ul>
	          <li><a href="#">Aliquam libero</a></li>
	          <li><a href="#">Consectetuer adipiscing elit</a></li>
	          <li><a href="#">Metus aliquam pellentesque</a></li>
	          <li><a href="#">Suspendisse iaculis mauris</a></li>
	          <li><a href="#">Urnanet non molestie semper</a></li>
	          <li><a href="#">Proin gravida orci porttitor</a></li>
            </ul>
          </li>
	      <li>
	        <h2>Blogroll</h2>
	        <ul>
	          <li><a href="#">Aliquam libero</a></li>
	          <li><a href="#">Consectetuer adipiscing elit</a></li>
	          <li><a href="#">Metus aliquam pellentesque</a></li>
	          <li><a href="#">Suspendisse iaculis mauris</a></li>
	          <li><a href="#">Urnanet non molestie semper</a></li>
	          <li><a href="#">Proin gravida orci porttitor</a></li>
            </ul>
          </li>
	      <li>
	        <h2>Archives</h2>
	        <ul>
	          <li><a href="#">Aliquam libero</a></li>
	          <li><a href="#">Consectetuer adipiscing elit</a></li>
	          <li><a href="#">Metus aliquam pellentesque</a></li>
	          <li><a href="#">Suspendisse iaculis mauris</a></li>
	          <li><a href="#">Urnanet non molestie semper</a></li>
	          <li><a href="#">Proin gravida orci porttitor</a></li>
            </ul>
          </li>
      </ul>
	    </div>-->
<!--	  <div id="content" style="background-image: url(fondoimagen.png);">-->
   	  
 		
		<div id="content3" >
        	<table width="100%"   border="0">
              <tr>
                <td width="84%" scope="col"><h1 align="center" class="title">Calculos de Movimientos y Reservas</h1> </td>
              </tr>
            </table>
         <div id="container">
            <div id="demo">
            <table width="100%" id="info" style="display:">
                
                    <thead>
                        <tr>
                        	<th >Calculo de Movimiento</th>
                            <th >Cambios de Unidades de medidas movimientos</th>
                            <th >Calculo de reservas</th>

                        </tr> 
                    </thead>
                    <tbody >
                            <tr align="center">
                                <td scope="col"><a id="movimiento" href="calculomovimiento.php" target="_new" style="font-size:18px;">Calculo de Movimiento</a></td>
                                <td scope="col"><a id="unimedida" href="ajuasteumm.php" target="_new" style="font-size:18px;">Cambio Uni Medida</a></td>
                                <td scope="col"><a id="reserva" href="calculoreservas.php" target="_new" style="font-size:18px;">Calculo Reserva</a></td>
                            </tr>
                            <tr>
                                <td scope="col">Se utiliza si existe variación entre los valores de totales del listado y el detalle del mismo </td>
                                <td scope="col">Se utilizar si se duplican las Unidades de medida de un artículo al momento de visualizar la existencia o reservas</td>
                                <td scope="col">Se utiliza si existe una diferencia entre reservas y el detalle de las mismas</td>
                            </tr>
                    </tbody>
                </table>
            </div>
	  </div>
	</div>
		<!-- end #content -->
		<!-- end #sidebar -->
	<div style="clear: both;">&nbsp;</div>
  </div>
	<!-- end #page -->
</div>
	<div id="footer">
		<div id="opcionesmasusadas"><?php include("opcionesmasusadasphp.php");?></div>
	</div>
	<!-- end #footer -->
</body>
</html>
