<?php 
/*
 *jDavila
 *02/03/2012
 */
session_start();
include("../conectar.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
<title>Idasys V3</title>
<link href="../<?php echo $css; ?>" rel="stylesheet" type="text/css" media="screen" />
<script language="JavaScript" src="../javascript/javascript.js"></script>
<script language="JavaScript" src="javascript.js"></script>
<script language="JavaScript" src="../javascript/jquery.js"></script>
<link href="../shadowbox.css"  rel="stylesheet" type="text/css"/>
<script src="../shadowbox.js" type="text/javascript"/> </script>
<script language="JavaScript" type="text/JavaScript">
Shadowbox.init({overlayOpacity: "0.5"});
</script>
<style type="text/css" title="currentStyle">
			@import "../DataTables-1.9.4/media/css/<?php echo $demo_page;?>";
			@import "../DataTables-1.9.4/media/css/<?php echo $demo_table;?>";
</style>
<script type="text/javascript" language="javascript" src="../DataTables-1.9.4/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf-8">
			
			 $(document).ready(function() 
			 {
			      document.getElementById('info').style.display="";
				  $('#info').dataTable( 
				  {
			        "bStateSave": true,
					"oLanguage": 
					{
			          	"sLengthMenu": "Mostrar _MENU_ registros por Pag.",
						"sZeroRecords": "No Existen Registros",
						"sInfo": "Mostrando desde _START_ hasta _END_ de _TOTAL_ registros",
						"sInfoEmpty": "Mostrando desde 0 hasta 0 de 0 registros",
						"sInfoFiltered": "(filtrando de _MAX_ de registros)",
						"sSearch": "Buscar",
						"oPaginate": 
						{
							"sNext": "Siguiente",
							"sPrevious": "Anterior"
						}
			        }
			      } );
			  } );
</script>
</head>
<body>
<div id="wrapper">
  <?php 
  		include("../superior.php");
  		?>
  <div id="page">
      <?php include("../validar.php");  		?>

<!--<div id="sidebar">
	    <ul>
	      <li>
	        <h2>Estad&iacute;sticas</h2>
	        <p>Aqu&iacute; se reflejar&aacute;n estad&iacute;sticas particulares seg&uacute;n en la opci&oacute;n del sistema que se encuentre</p>
          </li>
	      <li>
	        <h2>Categories</h2>
	        <ul>
	          <li><a href="#">Aliquam libero</a></li>
	          <li><a href="#">Consectetuer adipiscing elit</a></li>
	          <li><a href="#">Metus aliquam pellentesque</a></li>
	          <li><a href="#">Suspendisse iaculis mauris</a></li>
	          <li><a href="#">Urnanet non molestie semper</a></li>
	          <li><a href="#">Proin gravida orci porttitor</a></li>
            </ul>
          </li>
	      <li>
	        <h2>Blogroll</h2>
	        <ul>
	          <li><a href="#">Aliquam libero</a></li>
	          <li><a href="#">Consectetuer adipiscing elit</a></li>
	          <li><a href="#">Metus aliquam pellentesque</a></li>
	          <li><a href="#">Suspendisse iaculis mauris</a></li>
	          <li><a href="#">Urnanet non molestie semper</a></li>
	          <li><a href="#">Proin gravida orci porttitor</a></li>
            </ul>
          </li>
	      <li>
	        <h2>Archives</h2>
	        <ul>
	          <li><a href="#">Aliquam libero</a></li>
	          <li><a href="#">Consectetuer adipiscing elit</a></li>
	          <li><a href="#">Metus aliquam pellentesque</a></li>
	          <li><a href="#">Suspendisse iaculis mauris</a></li>
	          <li><a href="#">Urnanet non molestie semper</a></li>
	          <li><a href="#">Proin gravida orci porttitor</a></li>
            </ul>
          </li>
      </ul>
	    </div>-->
<!--	  <div id="content" style="background-image: url(fondoimagen.png);">-->
   	  
 		<?php 
				/*seccion de carga de informacion*/
		 		$wsolicitud=0;
				
				$sql="SELECT aticod, atides, substring(T1.ATIECA, 1 , 2) || '-' || substring(T1.ATIECA, 3 , 2) || '-' || substring(T1.ATIECA, 5 , 2) || '-' || substring(T1.ATIECA, 7 , 2) as ATIECA, atid1n, atid2n, atid3n, atid4n, atitco, atists, substring(T1.ATINIV, 1 , 2) || '-' || substring(T1.ATINIV, 3 , 2) || '-' || substring(T1.ATINIV, 5 , 2) || '-' || substring(T1.ATINIV, 7 , 2) as ATINIV FROM iv01fp t1 WHERE ACICOD ='$Compania' ORDER BY aticod";
				$resultt=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111"));
				$z=0;
				$lin=1;
				$limitep=$_SESSION['solicitudlineasporpaginat'];
				$pag=1;
				$primero='S';
				
				while(odbc_fetch_row($resultt))
				{
					$jml = odbc_num_fields($resultt);
					$row[$z]["pagina"] =  $pag;
					for($i=1;$i<=$jml;$i++)
					{
						$row[$z][odbc_field_name($resultt,$i)] =  odbc_result($resultt,$i);
					}
					$z++;
					if ($lin>=$limitep) 
					{
						$limitep+=$_SESSION['solicitudlineasporpaginat'];
						$pag++;
					}
					$lin++;
				}
				/*guarla informacion en un arreglo*/
				$_SESSION['solicitudarreglo']=$row;
				
				/*permite mostrar pag seleccionada*/
				if ($solicitudpagina==0) 	
				{
					$totsol=($lin-1);
					$_SESSION['totalsolicitudes']=$totsol;
					$solicitudpagina=1;
					$_SESSION['solicitudpaginas']=$pag;
				}
				
			?>    
        <div id="content3" >  
		
        <table width="100%"   border="0">
          <tr>
            <td width="84%" ><h1 align="center" class="title">TIPOS DE INVENTARIOS</h1><hr /></td>
            <td width="16%" ><div align="left">
              <table width="100%"  border="0">
                <tr>
                  <th width="36%" ><a rel="shadowbox;width=650;height=345" title="Agregar Tipo de Inventario" href="tiposinventariosfagregar.php"><img src="../images/agregar.gif" alt="Agregar" width="25" height="25" border="0" /></a></th>
                  <th width="30%" ><img src="../images/excel.jpg" alt="" width="25" height="25" /></th>
                  <th width="16%" ><img src="../images/pdf.jpg" alt="" width="25" height="25" /></th>
                  <th width="18%" ><img src="../images/impresora.gif" alt="" width="25" height="25" /></th>
                </tr>
              </table>
            </div></td>
          </tr>
          
        </table>
        
        <div id="container">
            <div id="demo">
            <table width="100%" id="info" style="display:none">
            
                <thead>
                    <tr>
                        <th width="8%" >Tipo</th>
                        <th width="17%" >Descripci�n</th>
                        <th width="10%" >Niveles</th>
                        <th width="11%" >Estructura Art�culo</th>
                        <th width="18%" >Descripciones <br>Niveles</th>
                        <th width="9%" >Tipo de Costo</th>
                        <th width="10%" >Status</th>
                        <th width="17%" >opciones</th>
                    </tr> 
                </thead>
                <tbody >
                    <?php 
                        $astfld="";
                                                            /*se muestra la cantidad de elementos segun la solicitada en $solicitudpagina*/
                        $paginat=$_SESSION['solicitudarreglo'];
                        
                        $pagact=$solicitudpagina;
                        for($g=0; $g < (count($paginat)); $g++)
                        {
                            //if ($paginat[$g]["pagina"] < $pagact) continue;
                            //if ($paginat[$g]["pagina"] > $pagact) break; 
                           
                    ?>
                             <!--<tr>
                                <td colspan="6"><?php print_r($paginat[$g]) ?></td>
                             </tr>-->
                            <tr>
                                <td ><div align="center"><strong><?php echo $paginat[$g]["ATICOD"];?></strong></div></td>                
                                <td ><div align="left"><?php echo $paginat[$g]["ATIDES"];?></div></td>
                                <td ><div align="left"><?php echo $paginat[$g]["ATINIV"];?></div></td>
                                <td ><div align="left"><?php echo $paginat[$g]["ATIECA"];?></div></td>
                                <td ><div align="left"><?php echo trim($paginat[$g]["ATID1N"])."<br />".trim($paginat[$g]["ATID2N"])."<br />".trim($paginat[$g]["ATID3N"])."<br />".trim($paginat[$g]["ATID4N"])."<br />";?></div></td>
                                <td ><div align="center"><?php if($paginat[$g]["ATITCO"]==1)echo "Promedio"; else if ($paginat[$g]["ATITCO"]==2)echo "Fijo";else if ($paginat[$g]["ATITCO"]==3)echo "&Uacute;ltimo Costo";?></div></td>
                                <td ><div align="center"><?php echo status('ATISTS',$paginat[$g]["ATISTS"]);?></div></td>
                                <?php /* seccion de opciones */ ?>
                                <td >
                                    <ul id="opciones">
                                        <li><a title="Editar Tipo de Inventario" rel="shadowbox;width=650;height=345" href="tiposinventariosfeditar.php?tipo=<?php echo trim($paginat[$g]["ATICOD"]);?>"><img src="../images/editar.gif" alt="Editar" width="15" height="15" border="0" /></a></li>
                                        <li><a title="Eliminar" href="javascript:eliminar('<?php echo trim($paginat[$g]["ATICOD"]);?>')"><img src="../images/eliminar.gif" alt="Eliminar" width="15" height="15" border="0" /></a></li>
                                        <li><a title="Sub Tipo de Inventario" href="subtiposin/subtipoinindex.php?tinventario=<?php echo trim($paginat[$g]["ATICOD"]);?>"><img src="../images/subtipoinventario.png" alt="Sub Tipo Inventario" width="15" height="15" border="0" /></a></li>
                                        <li><a title="Art&iacute;culos" href="articulos/articuloinindex.php?tinventario=<?php echo trim($paginat[$g]["ATICOD"]);?>"><img src="../images/articulos.png" alt="Art&iacute;culos" width="15" height="15" border="0" /></a></li>
                                    </ul>
                                </td>             
                            </tr>
                    <?php } ?> 
                
                </tbody>
            
            </table>
        </div>
       </div>
	</div>
		<!-- end #content -->
		<!-- end #sidebar -->
	<div style="clear: both;">&nbsp;</div>
  </div>
	<!-- end #page -->
</div>
	<div id="footer">
		<div id="opcionesmasusadas"><?php include("../opcionesmasusadasphp.php");?></div>
	</div>
	<!-- end #footer -->
</body>
</html>
