<?php 
/*
 *jDavila
 *12/03/2012
 */
session_start();
include("../../conectar.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
<title>Idasys V3</title>
<link href="../../<?php echo $css; ?>" rel="stylesheet" type="text/css" media="screen" />
<script language="JavaScript" src="../../javascript/javascript.js"></script>
<script language="JavaScript" src="javascript.js"></script>
<script language="JavaScript" src="../../javascript/jquery.js"></script>
<link href="../../shadowbox.css"  rel="stylesheet" type="text/css"/>
<script src="../../shadowbox.js" type="text/javascript"/> </script>
<script language="JavaScript" type="text/JavaScript">
Shadowbox.init({overlayOpacity: "0.5"});
</script>
<style type="text/css" title="currentStyle">
			@import "../../DataTables-1.9.4/media/css/<?php echo $demo_page;?>";
			@import "../../DataTables-1.9.4/media/css/<?php echo $demo_table;?>";
</style>
<script type="text/javascript" language="javascript" src="../../DataTables-1.9.4/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf-8">
			
			 $(document).ready(function() 
			 {
				  document.getElementById('info').style.display ="";
			      $('#info').dataTable( 
				  {
			        //inicio seccion de agrupacion
					"fnDrawCallback": function ( oSettings ) {
						if ( oSettings.aiDisplay.length == 0 )
						{
							return;
						}
				 
						var nTrs = $('#info tbody tr');
						var iColspan = nTrs[0].getElementsByTagName('td').length;
						var sLastGroup = "";
						for ( var i=0 ; i<nTrs.length ; i++ )
						{
							var iDisplayIndex = oSettings._iDisplayStart + i;
							var sGroup = oSettings.aoData[ oSettings.aiDisplay[iDisplayIndex] ]._aData[0];
							if ( sGroup != sLastGroup )
							{
								var nGroup = document.createElement( 'tr' );
								var nCell = document.createElement( 'td' );
								nCell.colSpan = iColspan;
								nCell.className = "ASICOD";//el nombre de la comlumna (debe estar de primera..)
								nCell.innerHTML = sGroup;
								nGroup.appendChild( nCell );
								nTrs[i].parentNode.insertBefore( nGroup, nTrs[i] );
								sLastGroup = sGroup;
							}
						}
					},
					"aoColumnDefs": [
						{ "bVisible": false, "aTargets": [ 0 ] }
					],
					"aaSortingFixed": [[ 0, 'asc' ]],
					"aaSorting": [[ 1, 'asc' ]],
					"sDom": 'lfr<"giveHeight"t>ip',//fin seccion de agrupacion
					
					"bStateSave": true,
					"oLanguage": 
					{
			          "sLengthMenu": "Mostrar _MENU_ registros por Pag.",
						"sZeroRecords": "No Existen Registros",
						"sInfo": "Mostrando desde _START_ hasta _END_ de _TOTAL_ registros",
						"sInfoEmpty": "Mostrando desde 0 hasta 0 de 0 registros",
						"sInfoFiltered": "(filtrando de _MAX_ de registros)",
						"sSearch": "Buscar",
					  "oPaginate": 
					  {
			            "sNext": "Siguiente",
						"sPrevious": "Anterior"
			          },
					  
			        }
			      } );
			  } );
</script>
</head>
<body>
<div id="wrapper">
  <?php 
  		include("../../superior.php");
  		?>
  <div id="page">
      <?php include("../../validar.php");  		?>

<!--<div id="sidebar">
	    <ul>
	      <li>
	        <h2>Estad&iacute;sticas</h2>
	        <p>Aqu&iacute; se reflejar&aacute;n estad&iacute;sticas particulares seg&uacute;n en la opci&oacute;n del sistema que se encuentre</p>
          </li>
	      <li>
	        <h2>Categories</h2>
	        <ul>
	          <li><a href="#">Aliquam libero</a></li>
	          <li><a href="#">Consectetuer adipiscing elit</a></li>
	          <li><a href="#">Metus aliquam pellentesque</a></li>
	          <li><a href="#">Suspendisse iaculis mauris</a></li>
	          <li><a href="#">Urnanet non molestie semper</a></li>
	          <li><a href="#">Proin gravida orci porttitor</a></li>
            </ul>
          </li>
	      <li>
	        <h2>Blogroll</h2>
	        <ul>
	          <li><a href="#">Aliquam libero</a></li>
	          <li><a href="#">Consectetuer adipiscing elit</a></li>
	          <li><a href="#">Metus aliquam pellentesque</a></li>
	          <li><a href="#">Suspendisse iaculis mauris</a></li>
	          <li><a href="#">Urnanet non molestie semper</a></li>
	          <li><a href="#">Proin gravida orci porttitor</a></li>
            </ul>
          </li>
	      <li>
	        <h2>Archives</h2>
	        <ul>
	          <li><a href="#">Aliquam libero</a></li>
	          <li><a href="#">Consectetuer adipiscing elit</a></li>
	          <li><a href="#">Metus aliquam pellentesque</a></li>
	          <li><a href="#">Suspendisse iaculis mauris</a></li>
	          <li><a href="#">Urnanet non molestie semper</a></li>
	          <li><a href="#">Proin gravida orci porttitor</a></li>
            </ul>
          </li>
      </ul>
	    </div>-->
<!--	  <div id="content" style="background-image: url(fondoimagen.png);">-->
   	  
		  <?php 
		  	$iu = new inf_tinventario($Compania, $tinventario); 
            $desc=$iu->batides;
            $niv=$iu->batinivn;
          ?>
 		<?php 
				/*seccion de carga de informacion*/
		 		$wsolicitud=0;
				
				$sql="SELECT T2.ASICOD, T2.ASIDES, T1.AARCOD, T1.AARNIV, T1.AARDES, T1.AARTDT, T1.AARSTS FROM iv05fp t1, iv03fp t2 WHERE T1.ACICOD= T2.ACICOD and T1.ATICOD= T2.ATICOD and T1.ASICOD= T2.ASICOD and t1.ACICOD ='$Compania' and t1.aticod='$tinventario' ORDER BY t1.asicod, t1.aarcod";
				$resultt=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111"));
				$z=0;
				$lin=1;
				$limitep=$_SESSION['solicitudlineasporpaginat'];
				$pag=1;
				$primero='S';
				
				while(odbc_fetch_row($resultt))
				{
					$jml = odbc_num_fields($resultt);
					$row[$z]["pagina"] =  $pag;
					for($i=1;$i<=$jml;$i++)
					{
						$row[$z][odbc_field_name($resultt,$i)] =  odbc_result($resultt,$i);
					}
					
					$z++;
					if ($lin>=$limitep) 
					{
						$limitep+=$_SESSION['solicitudlineasporpaginat'];
						$pag++;
					}
					$lin++;
					
					
				}
				/*guarla informacion en un arreglo*/
				$_SESSION['solicitudarreglo']=$row;
				
				/*permite mostrar pag seleccionada*/
				if ($solicitudpagina==0) 	
				{
					$totsol=($lin-1);
					$_SESSION['totalsolicitudes']=$totsol;
					$solicitudpagina=1;
					$_SESSION['solicitudpaginas']=$pag;
				}
				
			?>      
		<div id="content3" >
        	<table width="100%"   border="0">
              <tr>
                <td width="84%" ><h1 align="center" class="title"> ART&Iacute;CULOS</h1><div align="center"><a href="../tiposinventariosindex.php">Tipo de Inventario</a>: <?php echo $tinventario."-".$desc;?>
                <input type="hidden" name="tinventario" id="tinventario" value="<?php echo $tinventario;?>"></div><hr /></td>
                <td width="16%" ><div align="left">
                  <table width="100%"  border="0">
                    <tr>
                      <th width="36%" ><a rel="shadowbox;width=650;height=345" title="Agregar Art&iacute;culo" href="articuloinfagregar.php?tinventario=<?php echo $tinventario;?>"><img src="../../images/agregar.gif" alt="Agregar" width="25" height="25" border="0" /></a></th>
                      <th width="30%" ><img src="../../images/excel.jpg" alt="" width="25" height="25" /></th>
                      <th width="16%" ><img src="../../images/pdf.jpg" alt="" width="25" height="25" /></th>
                      <th width="18%" ><img src="../../images/impresora.gif" alt="" width="25" height="25" /></th>
                    </tr>
                  </table>
                </div></td>
              </tr>
            </table>
        <div id="container">
            <div id="demo">
                <table width="100%" id="info" style="display:none">
                
                    <thead>
                        <tr>
                        	<th>ASICOD</th>
                            <th width="8%" >Foto</th>
                            <th width="8%" >C&oacute;digo</th>
                            <th width="17%" >Descripción</th>
                            <th width="10%" >Nivel</th>
                            <th width="11%" >Tipo</th>
                            <th width="10%" >Status</th>
                            <th width="17%" >opciones</th>
                        </tr> 
                    </thead>
                    <tbody >
                        <?php 
                            $astfld="";
                            /*se muestra la cantidad de elementos segun la solicitada en $solicitudpagina*/
                            $paginat=$_SESSION['solicitudarreglo'];
                            
                            $pagact=$solicitudpagina;
                            for($g=0; $g < (count($paginat)); $g++)
                            {
                        ?>
                    			 <!--<tr>
                                    <td colspan="6"><?php print_r($paginat[$g]) ?></td>
                                 </tr>-->
                                <tr>
                                	<td ><div align="left"><strong><?php echo trim($paginat[$g]["ASICOD"])." -> ".trim($paginat[$g]["ASIDES"]);?></strong></div></td>                
                                    
                                    <?php $sql="SELECT AMDCOD, AMMCOD, AMMDES, AMMTIP, AMMSTS FROM MB09FP WHERE AMDCOD='$modulo' AND AMMTIP='F' AND AMMCOD='1' ORDER BY AMMCOD";
                                    $resultt2=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111")); ?>
                                    <td >
                                        <div align="left">
                                            <!--<a target='_blank' href='http://<?php echo $Direccionip; ?>/idasysv3/fotosarticulos/<?php echo "A_".$paginat[$g]["AARCOD"]."_". odbc_result($resultt2,2);?>.jpg'>-->
                                            <?php 
                                                $mostrar = 0;
                                                $foto = "../../fotosarticulos/A_".trim($paginat[$g]["AARCOD"])."_". odbc_result($resultt2,2).'.jpg';
                                                if(!file_exists($foto))
                                                {$foto ='../../images/nodisponible.png'; $mostrar = 0;}else{$mostrar = 1;}
                                                    if($mostrar)
                                                    {															
                                            ?>
                                            
                                            <a target='_blank' href='<?php echo trim($foto); ?>'>
                                                    <?php }?>
                                            <!--<img src='../../fotosarticulos/<?php echo "A_".$paginat[$g]["AARCOD"]."_". odbc_result($resultt2,2);?>.jpg' width="50" height="50" border='0' onerror="javascript:this.onerror=null;this.src='../../images/nodisponible.png';"/>-->
                                            <img src='<?php echo trim($foto); ?>' width="50" height="50" border='0'/>
                                            <?php 
                                                    if(file_exists($foto))
                                                    {															
                                            ?>
                                            </a>
                                            <?php } ?>
                                        </div>
                                    </td> 
                                    <td ><div align="center"><?php echo $paginat[$g]["AARCOD"];?></div></td>
                                    <td ><div align="center"><?php echo $paginat[$g]["AARDES"];?></div></td>
                                    <td ><div align="center"><?php echo $paginat[$g]["AARNIV"];?></div></td>
                                    <td ><div align="center"><?php echo $paginat[$g]["AARTDT"];?></div></td>
                                    <td ><div align="center"><?php echo status('AARSTS',$paginat[$g]["AARSTS"]);?></div></td>   
                                  <td >
                                        <ul id="opciones">
                                            <li><a title="Editar Art&iacute;culo" rel="shadowbox;width=650;height=345"  href="articuloinfeditar.php?articulo=<?php echo trim($paginat[$g]["AARCOD"]);?>&tinventario=<?php echo $tinventario;?>"><img src="../../images/editar.gif" alt="Editar" width="15" height="15" border="0" /></a></li>
                                            <?PHP if(validarutilizado(trim($paginat[$g]["AARCOD"]), $cid, $Compania) == 1){ ?>
                                            	<li>&nbsp;<img src="../../images/eliminar none.gif" alt="Eliminar" width="15" height="15" border="0" />&nbsp;</li>
                                            <?php }else{ ?>
	                                            <li><a title="Eliminar" href="javascript:eliminar('<?php echo trim($paginat[$g]["AARCOD"]);?>','<?php echo trim($tinventario);?>','<?php echo trim($paginat[$g]["ASICOD"]);?>')"><img src="../../images/eliminar.gif" alt="Eliminar" width="15" height="15" border="0" /></a></li>
                                            <?php } ?>
                                            <!--<li><a title="Foto Art&iacute;culo" rel="shadowbox;width=650;height=345"  href="articuloinfeditar.php?tipo='<?php echo $paginat[$g]["AARCOD"];?>'"><img src="../../images/editar.gif" alt="Editar" width="15" height="15" border="0" /></a></li>-->
                                            <!--<li><a href="javascript:articulosfotos('<?php echo trim($paginat[$g]["AARCOD"]);?>','<?php echo trim($tinventario);?>','<?php echo trim($paginat[$g]["ASICOD"]);?>')"><img src="../../images/fotos.gif" alt="Fotos" width="28" height="28" border="0"></a></li>-->
                                            <li><a title="Carga de Fotos" rel="shadowbox;width=700;height=600" href="articulosfotos.php?&codigo=<?php echo trim($paginat[$g]["AARCOD"]);?>&tinventario=<?php echo trim($tinventario);?>&stinventario=<?php echo trim($paginat[$g]["ASICOD"]);?>)"><img src="../../images/fotos.gif" alt="Fotos" width="28" height="28" border="0"></a></li>
                                            <li><a title="Factor de conversión" href="../factcconversion/factconverindex.php?&aarcod=<?php echo trim($paginat[$g]["AARCOD"]);?>&tinventario=<?php echo trim($tinventario);?>"><img src="../../images/conversion.png" alt="Conversion" width="28" height="28" border="0"></a></li> 
                                            <?php if($paginat[$g]["AARSTS"] == '02') {?>
	                                            <li><a title="Activar" href="#" onclick="JavaScript:activar('<?php echo trim($paginat[$g]["AARCOD"]);?>','<?php echo trim($paginat[$g]["AARSTS"]);?>');"><img src="../../images/activar.png" alt="Activar" width="28" height="28" border="0"></a></li> 
                                            <?php }else if($paginat[$g]["AARSTS"] == '01') {?>
                                            	<li><a title="Desactivar" href="#" onclick="JavaScript:activar('<?php echo trim($paginat[$g]["AARCOD"]);?>','<?php echo trim($paginat[$g]["AARSTS"]);?>');"><img src="../../images/desactivar.png" alt="Desactivar" width="28" height="28" border="0"></a></li> 
                                            <?php }?>
                                            <li><a rel="shadowbox;width=650;height=345" title="Copiar Art&iacute;culo" href="articuloinfagregarcopia.php?&aarcod=<?php echo trim($paginat[$g]["AARCOD"]);?>&tinventario=<?php echo $tinventario;?>"><img src="../../images/copia.png" alt="Copiar Art&iacute;culo" width="28" height="28" border="0"></a></li> 
                                        </ul>
                                    </td>             																			
                                </tr>
                        <?php } ?> 
                    
                    </tbody>
                
                </table>
            </div>
	  </div>
	</div><!-- end #content -->
		
		
	<div style="clear: both;">&nbsp;</div>
  </div>
	<!-- end #page -->
</div>
	<div id="footer">
		<div id="opcionesmasusadas"><?php include("../../opcionesmasusadasphp.php");?></div>
	</div>
	<!-- end #footer -->
</body>
</html>
