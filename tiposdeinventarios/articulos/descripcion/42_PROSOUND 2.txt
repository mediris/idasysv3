INCLUYE:
*Transductor Electrónico Convexo Tecnología Ex PHD, con Harmónica de tejido, multifrecuencial (2.14, 2.5/3.75/5.0/6.0 Hz), uso abdomen en general, OG/GYN, código UST-9137. Serial No. XXXX
*Transductor electrónico microconvexo de súper alta densidad multifrecuencial (3.75/5.0/6.0/7.5 MHz), con harmónica de tejido (2.14/2.5/3.0) para exploraciones endocavitarias (Transvaginal- Transrectal) de 136º/7mmR, Modelo: UST-9145. Serial No. XXXX 
*Carro de Transporte, Modelo RMT-PS2
*Video impresora multiformato Blanco/Negro Digital, Marca: Sony, Modelo: UP-X898MD. Serial No. XXXX
*Unidad UPS True on Line de 1KVA.