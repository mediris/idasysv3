- Grupo Generador C450D6 
- Potencia Standby :550kVA/440kW
- Potencia Prime 495kVA/396kW
- Tensión de operación 3x208Vac + N 60HZ
- Motor Marca Cummins
- Combustible: Diesel
- Alternador Marca Standford Sin escobillas
El mismo incluye:
* Silenciador de escape
* Flexible de escape de acero inoxidable
* Cargador de batería de arranque
* Baterías de arranque libre de mantenimiento
* Interruptor termomagnético de protección
* Precalentador de block
* Regulador electrónico de velocidad
* Regulador de tensión electrónico