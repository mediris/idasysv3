COMPUESTO POR: 
2	Pinza para Pólipos Gross-Maier, Curva, 26.5 cm
4	Pinza de Campo Backhaus, 11 cm
4	Pinza de Campo para Papeles, 11.5 cm
2	Mango de Bisturí No. 3, 12.0 cm
1	Mango de Bisturí No. 7, 13.5 cm - 5 3/8 "
1	TC-Tijera para Disección, Metzenbaum, Curva, 11.5 cm
1	TC-Tijera para Disección Delicada, Metzenbaum - Fino, Curva, 14.5 cm
1	TC- Tijera Toennis, Curva, 17.5 cm
1	Tijera para Disección Jameson, Curva, 15.5 cm
1	TC-Tijera para ligadura, Curva, dentada ,   18.0 cm
1	Tijera Quirúrgica, Aguda/Roma, Recta, 14 .5 cm
1	Mini-Pinza de Disección Adson, 12 cm
1	Pinza de Disección Mini-Adson, 12 cm,          1 x 2D
2	Pinza de Disección, 1 x 2 D, Mediana, 14.5 cm
2	Pinza de Disección Potts-Smith, 21 cm
2	Pinza Vascular De Bakey, 1.5 mm, 16 cm
2	Pinza Vascular De Bakey, 1.5 mm, 20 cm
6	Pinza Hemostática Halsted-Mosquito, Curva, 12.5 cm
6	Pinza Hemostática Halsted-Mosquito, Curva, 14.5 cm
2	Pinza Hemostática Kocher, Dientes 1 x 2, Recta, 14 cm
2	Pinza Hemostática Kocher, Dientes 1 x 2, Recta, 18.5 cm
2	Pinza Hemostática Halsted-Mosquito, Curva, 18.5 cm
2	Pinza para Tapones Förster, Curva, 18 cm
4	Pinza para Peritoneo Mikulicz-Baby, 14.5 cm
2	Pinza para Peritoneo Mikulicz, 18.5 cm
2	Pinza Hemostática Mixter-Baby, Curva, 14.0 cm
2	Pinza Hemostática Baby-Mixter, Curva, 18.5 cm
1	TC Porta-Aguja Halsey, 13 cm
1	TC Porta-Aguja Crile-Wood, 15 cm
1	TC De Bakey Porta-Aguja, 18 cm
1	TC Porta-Aguja Mayo-Hegar, 16 cm
1	Sonda Abotonada, Ø1.0 mm, 14.5 cm
2	Separador Senn-Green, 10 x 6 mm, 16 cm
2	Separador Langenbeck, 30 x 11 mm, 22 cm
2	Separador Langenbeck, 40 x 11 mm, 22 cm
2	Separador Desmarres, 12 mm, 16 cm
2	Separador doble Baby-Roux, 12.5 cm
2	Retractor Roux, FIG. 1, 14.5 cm
2	Retractor Fritsch, 33 X 40 mm, 24 cm
2	Ganchito, Agudo, 2 Garfios, 16. 5 cm
2	Separador Fino, Agudo, 4 Garfios, 16.5 cm
2	Separador Volkmann, Semi-Agudo, 4 Garfios, 22.5 cm
1	Espátula Martin, Maleable, 16 mm, 20 cm
1	Separador Adson, Romo, 3 x 4D, 14.0 cm
1	Separador Abdominal Balfour-Baby, 12.5 cm
1	Pinza Intestinal Allis-Baby, 4 x 5D, 13 cm
1	Pinza Intestinal At-Allis, 15.5 cm
1	Pinza Intestinal At-Allis, 20.0 cm
2	Pinza Intestinal At-Baby-Doyen, Curva, 13.5 cm
2	Pinza Intestinal At-Baby-Doyen, Curva, 17.0 cm
2	Pinza Anastomosis Derra, FIG 2, 17 cm
1	Caja Interior Perforada Edelstahl 18/8 martinit
1	Cubeta Redonda, Acero Inoxidable, 40 mm, Ø 80 mm, 0.14 L
1	Cubeta Redonda, Acero Inoxidable, 55 mm, Ø 128 mm, 0.35L
1	Riñonera, 250 X 140 X 40 mm
1	Aguja Guía, Redon, Tipo cuchillo, 8 CH
1	Aguja Guía, Redon, Tipo cuchillo, 10 CH
1	marSafe, gris, perf., 60 x 30 x 16 cm
2	Plaquita, marSafe, con texto
2	marTray, DIN, 477 x 251 x 44 mm