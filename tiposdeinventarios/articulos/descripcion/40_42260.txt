Con Ventilador microprocesado controlado por volumen, presión y pantalla LCD monocromática que incluye:
*-Carro para equipo de anestesia
*-Bloque de flujómetros 2x 02-N20-Air (O.R.C.)
*-Canister, absorbente  CO2
*-Sistema de circuito-paciente (reusable, libre de latex)
*-Kit de mangueras (Manguera PVC O2, PVC N2O, PVC Aire, PVC Vacío) 
*-Brazo soporte para circuito
*-Válvula APL 
*-2 cajones integrados
*-Bolsa interna de  2L - Libre de latex
*-Caja de cal sodada, 4.5kg. 
*-Cable de alimentación NORTH AM. 2M 3 PIN
*- Un (1) Vaporizador Drager tipo DR para Isoflurano S/N.: ASHC-1235
*- Un (1) Vaporizador Drager tipo DR para Sevoflurano S/N.: ASHD-0342
*-PBU (Módulo de Pacientes) S/N: 45168