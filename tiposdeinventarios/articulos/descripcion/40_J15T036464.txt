- Grupo Generador C250D6 
- Potencia Standby :330kVA/264kW 
- Potencia Prime 300kVA/240kW 
- Tensión de operación 3x208Vac + N 60HZ 
- Motor Marca Cummins 
- Combustible: Diesel 
- Alternador Marca Standford Sin escobillas 
El mismo incluye: 
* Cabina de Insonorización 
* Silenciador de escape 
* Flexible de escape de acero inoxidable 
* Cargador de batería de arranque 
* Baterías de arranque libre de mantenimiento 
* Interruptor termomagnético de protección 
* Precalentador de block 
* Regulador electrónico de velocidad 
* Regulador de tensión electrónico