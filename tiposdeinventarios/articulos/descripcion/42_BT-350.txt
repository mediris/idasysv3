ACCESORIOS:
* Dos (2) transductores para doppler fetal.
* Un (1) transductor para actividad uterina
* Un (1) marcador de eventos remotos
* Un (1) cable de alimentación
* Un (1) gel acústico
* Tres (3) correas sujetadoras.
* Dos (2) paquetes de papel de impresión
* Manual de operación
* Batería Recargable