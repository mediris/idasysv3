Tablero de transferencia Automática OTPC 1600
Características técnicas:
- Accionamiento Lineal
- Intensidad nominal 1600A
- Tensión nominal 3x208Vac
- Frecuencia nominal 60Hz
- Polos 4
- Sensado por sobre/bajo tensión en ambas fuentes
- Sensado de frecuencia en ambas fuentes
- Detección de pérdida de una fase