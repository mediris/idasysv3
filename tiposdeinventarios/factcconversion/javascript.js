/*
jDavila
21/03/12
*/
function agregar()
{
	//document.form.aarcod.value="";
	document.form.aumori.value="";
	document.form.aumfin.value="";
	document.form.aumfac.value="";
	$("#aftsav").hide(1);
}

/*
jDavila
21/03/12
*/
function agregarvalidar(){	
	var contenido= "";
	var elemento;
	document.body.style.cursor="wait";

	$.ajax({
	   	async: false,
        type: "POST",
		dataType: "JSON",
        url: "factconveragregarvalidarphp.php",
        data: $("#form").serialize(),
        success: function(datos){
			vdata = parseJSONx(datos);}							
	});			
	sierror='N';
	for(i in vdata)	{
		if(vdata[i].campo) 	{
			document.getElementById(vdata[i].campo).innerHTML=vdata[i].msg;
			sierror='S';
		}
	}
	document.body.style.cursor="default";
	if(sierror=='N') 		
	{
		$("#aediv").hide(1000);
		$("#aftsav").show(1000);
	}
}

/*
jDavila
20/03/12
*/
function editar(tipo, ori, fin){

	var param = [];
	param['aarcod']=tipo;
	param['aumori']=ori;
	param['aumfin']=fin;
	ejecutasqlp("factconverinformacionphp.php",param);

	for(i in gdata)
	{
		document.form.haarcod.value=gdata[i].AARCOD;
		document.getElementById("wsaarcod").innerHTML=gdata[i].AARDES + ' (' + gdata[i].AARCOD + ')';

		document.form.aumori.value=gdata[i].AUMORI;
		document.getElementById("wsaumori").innerHTML=gdata[i].AUMORIDES;

		document.form.aumfin.value=gdata[i].AUMFIN;
		document.getElementById("wsaumfin").innerHTML=gdata[i].AUMFINDES;
		
		document.form.aumfac.value=gdata[i].AUMFAC;
		
		//carga el valor formateado
		document.getElementById("valor").innerHTML=NumFormat(gdata[i].AUMFAC, '9', '3', '.', ',')

	};
	$("#aftsav").hide(1);
}

/*
jDavila
20/03/12
*/
function editarvalidar(){	
	var contenido= "";
	var elemento;
	document.body.style.cursor="wait";
	$.ajax({
			async: false,
			type: "POST",
			dataType: "JSON",
			url: "factconvereditarvalidarphp.php",
			data: $("#form").serialize(),
			success: function(datos){
						vdata = parseJSONx(datos);}							
	});			
	var sierror='N';
	for(i in vdata)	{
		if(vdata[i].campo) 	{
			document.getElementById(vdata[i].campo).innerHTML=vdata[i].msg;
			sierror='S';
		}
	}
	document.body.style.cursor="default";
	if(sierror=='N') 		
	{
		$("#form").hide(1000);
		$("#aftsav").show(1000);	
	}

}

/*
jDavila
20/03/12
*/
function eliminar(tipo, ori, fin) {
	if (confirm('Seguro que desea borrar factor de conversion' + tipo + '?'))
	{
		var param = [];
		param['aarcod']=tipo;
		param['aumori']=ori;
		param['aumfin']=fin;
		
		ejecutasqld("factconvereliminar.php",param);
		location.reload();
	}
}


/**/

/*
 * @jDavila
 * @obj: se manda el elemnet select para evaluar la opcion seleccionada
 * se utiliza el metodo para cargar las opciones posibles en el select de unidad
 * final quitando la seleccionada en unidad origen
 */
function combo_unifinal(obj)
{
	var value = $('#'+obj.id).val();
	if (value==0){
		value = 99999999;
	}
	$.ajax({
		async: false,
		type: 'POST',
		url: 'cargarcombofinal.php',
		beforeSend:function(){
			$('#s_unifinal').html('<span class="input-notification attention png_bg">Cargando...</span>');
		},
		data: 'uni='+value,
		success: function(html){                
			$('#s_unifinal').html(html);
		}
	});
}
/*
 * @jDavila
 * @numero: Es el número que se desea formatear.
 * @decimales: número de decimales en valor regresado.
 * @miles: número de digitos en la separación de miles.
 * @decSep: separador de decimales. El valor predeterminado es el ‘punto’.
 * @milSep: separador de miles. El valor predeterminado para el separador de miles es la ‘coma’.
 * formate el numero con las caracteristicas dadas
 */
function NumFormat(numero, decimales, miles, decSep, milSep)
{
	if(typeof(miles)=="undefined"){miles=9999;}
	if (typeof(decSep)=="undefined"){decSep = ".";}
	if (typeof(milSep)=="undefined"){milSep=",";}
 	var str = numero.toString();
 	var pos = str.indexOf(decSep);
 	if (pos==-1) str = str + decSep;
 	pos = str.indexOf(decSep)
 	var arr =  str.split("");
 	var ret  = "";
 	var i = 0;
 	k = pos-1;
 	m = 0
	while (k >= 0)
	{
		if (m==miles){
	   		ret = milSep + ret;
	   		m = 0;
	  	}
	  	++m;
	  	ret = arr[k--] +   ret;
	}
    if (decimales==0) return ret;  
 	ret = ret +  decSep;
 	var j=0;
 	i = pos + 1;
	while (i < str.length && j < decimales){
		ret += arr[i++];
	   	++j;
	}
 	while ( j<decimales){
  		ret += "0";
		++j;
 	}
	return ret;
}

