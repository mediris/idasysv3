<?php session_start();
include("../conectar.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="sinhead">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>agregar usuario</title>
<link href="../<?php echo $css; ?>" rel="stylesheet" type="text/css" media="screen" />
<script language="JavaScript" src="../javascript/jquery.js"></script>
<script language="JavaScript" src="../javascript/javascript.js"></script>
<script language="JavaScript" src="javascript.js"></script>
<script language="JavaScript" src="../calendario/javascripts.js"></script>
</head>
<body class="sinbody" bgcolor="#FFFFFF" onload="javascript:usuarioagregar();cargarparametros('');">
<div id="agregarusuariodiv">
<form action="" method="post" name="form" class="form" id="form">
    <table width="100%"  border="0">
      <tr>
        <th width="20%" scope="col"><label>Usuario</label></th>
        <th width="36%" scope="col"><div align="left">
          <input name="auscod" type="text" id="auscod" size="10" maxlength="10" onKeyUp="form.auscod.value=form.auscod.value.toUpperCase()">
        </div></th>
        <th colspan="2" id="errauscod"  scope="col">&nbsp;</th>
      </tr>
      <tr>
        <th scope="col"><label>Primer Apellid. </label></th>
        <th scope="col"><div align="left">
          <input name="ausap1" type="text" id="ausap1" size="25" maxlength="25">
        </div></th>
        <th colspan="2" id="errausap1" scope="col">&nbsp;</th>
      </tr>
      <tr>
        <th scope="col"><label>Segundo Apellido </label></th>
        <th scope="col"><div align="left">
          <input name="ausap2" type="text" id="ausap2" size="25" maxlength="25">
        </div></th>
        <th colspan="2" id="errausap2"  scope="col">&nbsp;</th>
      </tr>
      <tr>
        <th scope="col"><label>Primer Nombre </label></th>
        <th scope="col"><div align="left">
          <input name="ausno1" type="text" id="ausno1" size="25" maxlength="25">
        </div></th>
        <th colspan="2" id="errausno1"  scope="col">&nbsp;</th>
      </tr>
      <tr>
        <th scope="col"><label>Segundo Nombre </label></th>
        <th scope="col"><div align="left">
          <input name="ausno2" type="text" id="ausno2" size="25" maxlength="25">
        </div></th>
        <th colspan="2" id="errausno2"  scope="col">&nbsp;</th>
      </tr>
      <tr>
        <th scope="col"><label>Fecha Nacimiento </label></th>
        <th scope="col">

          <div align="left">
   <?php echo escribe_formulario_fecha_vacio("ausfen","usuarioagregarform","01.01.1900"); ?>
        </div>
          </th>
        <th colspan="2" scope="col"><div align="left">(DD.MM.AAAA)</div></th>
      </tr>
      <tr>
        <th scope="col"><label>Fecha de Ingreso </label></th>
        <th scope="col">          <div align="left">
    <?php echo escribe_formulario_fecha_vacio("ausfei","usuarioagregarform","01.01.1900"); ?>
   </div>
</th>
        <th colspan="2" scope="col"><div align="left">(DD.MM.AAAA)</div></th>
      </tr>
      <tr>
        <th scope="col"><label>Ficha en N&oacute;mina</label></th>
        <th scope="col"><div align="left">
          <input name="ausfic" type="text" id="ausfic" size="10" maxlength="10">
        </div></th>
        <th colspan="2" id="errausfic"  scope="col">&nbsp;</th>
      </tr>
      <tr>
        <th scope="col"><label>Sexo</label></th>
        <th scope="col"><div align="left">
          <p>
          
            <input name="aussex" type="radio" value="M" checked>
  Masculino

           
            <input type="radio" name="aussex" value="F">
  Femenino
            <br>
          </p>
</div></th>
        <th colspan="2" id="erraussex"  scope="col">&nbsp;</th>
      </tr>
	  <tr>
        <th scope="col"><label>Usuario Supervisor </label></th>
        <th scope="col"><div align="left">
          <input type="checkbox" name="aussup" id="aussup"value="S">
</div></th>
        <th colspan="2" id="erraussup"  scope="col">&nbsp;</th>
      </tr>
	  <tr>
        <th scope="col"><label>Tel&eacute;fonos</label></th>
        <th scope="col"><div align="left">
          <input name="austls" type="text" id="austls" size="25" maxlength="25">
        </div></th>
        <th colspan="2" id="erraustls"  scope="col">&nbsp;</th>
      </tr>
      <tr>
        <th scope="col"><label>Supervisor</label></th>
        <th scope="col"><div align="left"><select name="aususr" id="select">
      <option value= " " >*** No tiene supervisor ***</option>
      <?php $sql="Select auscod, ausap1, ausap2, ausno1, ausno2 from mb20fp t1 where aussup='S' order by ausap1, ausap2 , ausno1";
				$result1=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111")); 
  				while(odbc_fetch_row($result1)){			
										$auscod=trim(odbc_result($result1,1));
										$nombre=odbc_result($result1,5)." ".odbc_result($result1,4).", ".odbc_result($result1,3)." ".odbc_result($result1,2);?>
      <option value= "<?php echo $auscod; ?>" ><?php echo $nombre ?></option>
      <?php } ?>
    </select></div></th>
        <th colspan="2" id="erraususr"  scope="col">&nbsp;</th>
      </tr>
	  <tr>
        <th scope="col"><label>Email</label></th>
        <th scope="col"><div align="left">
          <input name="aemmai" type="text" id="austls" size="50" maxlength="50">
        </div></th>
        <th colspan="2" id="erraemmai"  scope="col">&nbsp;</th>
      </tr>
      <tr>
            <td colspan="4">
                <table width="100%"  border="0" >
                  <tr>
                    <td width="100%" colspan="3" scope="col"><h3>Parámetros Adicionales: </h3>
                      <span class="header">
                      </span></td>
                    </tr>
                    <tr>
                        <td>
                        <div id="parametros">
                        </div>
                        </td>
                    </tr>
                  </table>
            </td>
      </tr>
      <tr>
        <th scope="col">&nbsp;</th>
        <th scope="col"></th>
        <th width="23%" id="erraususr" scope="col"><input name="cancelar" type="button" onClick="parent.location.reload();parent.Shadowbox.close();"" " value="Salir" id="cancelar"></th>
        <th width="21%" id="erraususr" scope="col"><input type="button" name="submit" id="submit" value="Grabar" onClick="usuarioagregarvalidar()" /></th>
      </tr>
    </table>
  </form>
  </div>
<div align="center" id="usuarioagregaraftsav">
  <br />
  <br />
  <br />
  <br />
  <br />
  <br />
  Registro Agregado con exito !
  <br />
  <br />
  <br />
  <input name="Submit3" type="button" onClick="parent.location.reload();parent.Shadowbox.close();"" " value="Cerrar"></div>
</body>
</html>