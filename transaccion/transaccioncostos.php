<?php session_start();
include("../conectar.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="sinhead">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Ver Transacci&oacute;n</title>
<link href="../<?php echo $css; ?>" rel="stylesheet" type="text/css" media="screen" />
<script language="JavaScript" src="../javascript/jquery.js"></script>
<script language="JavaScript" src="../javascript/javascript.js"></script>
<script language="JavaScript" src="javascript.js"></script>
<script language="JavaScript" src="../calendario/javascripts.js"></script>
</head>
<?php
	
	//Se modifica consulta para MEDIX por los LOTES/SERIALES
	 if($Compania=='42'){
	 	/*
		sql ="SELECT T1.ACICOD, T1.AALCOD, T1.ATRCOD, T1.ATRNUM, T1.ATRDES, T1.ATROBS, T2.ATRART, SUM(T2.ATRCAN) AS ATRCAN, T2.ATRUMH, T2.ATRCUT, T3.ATRDES AS ATRTDES
	 	FROM IV15FP T1 
	 		INNER JOIN iv16fp T2 ON (T1.AALCOD= T2.AALCOD AND T1.ATRNUM=T2.ATRNUM AND T1.ATRCOD=T2.ATRCOD AND T1.ACICOD=T2.ACICOD) 
	 		INNER JOIN IV12FP T3 ON (T1.ACICOD=T3.ACICOD AND T1.ATRCOD=T3.ATRCOD) 
	 	WHERE T1.ACICOD='$Compania' AND T1.ATRCOD='$atrcod' AND T1.ATRNUM=$atrnum AND T1.AALCOD='$almacen' 
	 	GROUP BY T1.ACICOD, T1.AALCOD, T1.ATRCOD, T1.ATRNUM, T1.ATRDES, T1.ATROBS, T2.ATRART, T2.ATRUMH, T2.ATRCUT, T3.ATRDES";
		*/
		$sql ="SELECT T2.ATRSEC, T1.ACICOD, T1.AALCOD, T1.ATRCOD, T1.ATRNUM, T1.ATRDES, T1.ATROBS, T2.ATRART, SUM(T2.ATRCAN) AS ATRCAN, T2.ATRUMH, T2.ATRCUT, T1.ATRFLT, T3.ATRDES AS ATRTDES
	 	FROM IV15FP T1 
	 		INNER JOIN iv16fp T2 ON (T1.AALCOD= T2.AALCOD AND T1.ATRNUM=T2.ATRNUM AND T1.ATRCOD=T2.ATRCOD AND T1.ACICOD=T2.ACICOD) 
	 		INNER JOIN IV12FP T3 ON (T1.ACICOD=T3.ACICOD AND T1.ATRCOD=T3.ATRCOD) 
	 	WHERE T1.ACICOD='$Compania' AND T1.ATRCOD='$atrcod' AND T1.ATRNUM=$atrnum AND T1.AALCOD='$almacen' 
	 	GROUP BY T2.ATRSEC, T1.ACICOD, T1.AALCOD, T1.ATRCOD, T1.ATRNUM, T1.ATRDES, T1.ATROBS, T2.ATRART, T2.ATRUMH, T2.ATRCUT, T1.ATRFLT, T3.ATRDES
		ORDER BY T2.ATRSEC ";
	 } else {
	  $sql ="SELECT T1.ACICOD, T1.AALCOD, T1.ADPCOD, T1.ATRCOD, T1.ATRNUM, T1.ATRDES, T1.ATRFEC, T1.ATRHOR, T1.ATRMOR, T1.AUSCOD, T1.ATRSTS,
			 T2.ADPCOD, T2.ATRSEC,T2.ATRART, T2.ATRLOT, T2.ATRSER, T2.ATRCAN, T2.ATRUMB, T2.ATRUMH,T2.ATRFAC, T2.ATRCUT, T2.ATRCUS, T2.ATREAC,
			 T2.ATREAA, T2.ATREAL, T2.ATDSTS, T3.ATRDES AS ATRTDES, T1.ATROBS, T2.ATRCUT, T1.ATRFLT
		FROM IV15FP T1
			left join iv16fp T2 on (T1.AALCOD= T2.AALCOD AND T1.ATRNUM=T2.ATRNUM AND T1.ATRCOD=T2.ATRCOD AND T1.ACICOD=T2.ACICOD), 
			IV12FP T3 
		WHERE T1.ACICOD='$Compania' AND T1.ATRCOD=$atrcod AND T1.ATRNUM=$atrnum AND 
 			  T1.AALCOD='$almacen' AND T1.ATRCOD=T3.ATRCOD AND T1.ACICOD=T3.ACICOD ";//T1.AUSCOD='$Usuario' AND
		}

	$result = odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111")); 
	$result2 = odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111")); 	

?>

<body class="sinbody" bgcolor="#FFFFFF" >
<div id="ocultarform">
	<form name="costoform" id="costoform" method="post" class="form" action="">
	<table class="tabla1">
		<tr>
			<td width="29%"  scope="col"><label>Almac&eacute;n</label></td>
			<td width="32%" scope="col">
				<div align="left" id="aalcod"><strong><?php echo alamcen(trim(odbc_result($result, 'AALCOD')),$Compania);?></strong></div>
                <input type="hidden" name="aalcod" id="aalcod" value="<?php echo odbc_result($result, 'AALCOD'); ?>"  />
			</td>
			<td colspan="2" id="erraalcod"  scope="col">&nbsp;</td>
		</tr>
		<tr>
			<td width="29%"  scope="col"><label>N&uacute;mero</label></td>
			<td width="32%" scope="col">
				<div align="left" id="atrnum"><strong><?php echo trim(odbc_result($result, 'ATRNUM'));?></strong></div>
                <input type="hidden" name="atrnum" id="atrnum" value="<?php echo odbc_result($result, 'ATRNUM'); ?>"  />
			</td>
			<td colspan="2" id="erratrnum"  scope="col">&nbsp;</td>
		</tr>
		<tr>
			<td  scope="col">Transacci&oacute;n:</td>
			<td width="32%" scope="col">
				<div align="left" id="atrcod"><strong><?php echo trim(odbc_result($result, 'ATRTDES'))."(".trim(odbc_result($result, 'ATRCOD')).")";?></strong></div>
                <input type="hidden" name="atrcod" id="atrcod" value="<?php echo odbc_result($result, 'ATRCOD'); ?>"  />
			</td>
			<td colspan="2" id="erratrcod"  scope="col">&nbsp;</td>
		</tr>
		<tr>
			<td scope="col"><label>Descripci&oacute;n</label></td>
			<td scope="col">
				<div align="left" id="atrdes"><strong><?php echo trim(odbc_result($result, 'ATRDES'));?></strong></div>
			</td>
			<td colspan="2" id="erratrdes" scope="col">&nbsp;</td>
		</tr>
        <tr>
			<td scope="col"><label>Observaci&oacute;n</label></td>
			<td scope="col" colspan="3">
				<div align="left" id="atrdes"><strong><?php echo trim(odbc_result($result, 'ATROBS'));?></strong></div>
			</td>
		</tr>
		<tr>
			<td colspan="4">
				<table width="100%"  border="0" >
					<tr>
						<td width="100%" colspan="3" scope="col"><h3>Par&aacute;metros Adicionales: </h3>
						  <span class="header">
						  </span>
						</td>
					</tr>
					<tr>
						<td>
							<div id="parametros">
                            <?php 
								$sql = "SELECT T1.ACICOD, T1.ATRCOD, T1.AMDCOD, T1.APDCOD,
												T2.APDDES, T2.APDSEC, T2.APDLON, T2.APDDCR, T2.APDTIP, T2.APDVAL, T2.ATACOD, 
												T2.ASBCOD, T2.APDSTS, T2.APDLND, T3.ASBDES, T3.ASBSTS, T4.APACOD, 
												T4.ATRNUM, T4.AAPVLA, T4.AAPDES, T4.AAPLON, T4.AAPLND, T4.AAPSEC, 
												T4.AAPTIP 
											FROM mb10fp T3, MB03FP T2, iv37FP T1                    
												LEFT JOIN IV38FP T4 ON ( T1.ACICOD=T4.ACICOD AND T4.ATRNUM=".trim(odbc_result($result, 'ATRNUM'))." AND T1.ATRCOD=T4.ATRCOD AND T1.APDCOD=T4.APACOD)                                          
											WHERE T1.ACICOD='$Compania' AND T1.APDCOD=T2.APDCOD AND                  
												T1.AMDCOD=T2.AMDCOD AND T1.AMDCOD='$modulo' AND T2.ATACOD=T3.ATACOD AND 
												T2.ASBCOD=T3.ASBCOD AND T1.ATRCOD='".trim(odbc_result($result, 'ATRCOD'))."' 
											ORDER BY T2.ASBCOD, T2.APDSEC, T1.APDCOD";
								echo fparam_adi_add_ver_transaccion('agregarform','detalle',$sql,'1');
							?>
                            
							</div>
						</td>
					</tr>
				 </table>
			</td>
		</tr>
        <tr>
        	<td colspan="4">
            	<table  id="grilla" width="100%"  border="0" >
                    <thead>
                    	<?php if($Compania == '40'){?>
                    		<tr>
	                            <th colspan="6" align="center" scope="col"><h3>Detalle de Transacci&oacute;n: </h3></th>
	                        </tr>
	                        <tr>	
	                            <th >Art&iacute;culo</th>
	                            <th >Cantidad</th>
	                            <th >Unid/Med </th>
	                            <th >Costo por Unidad</th>
	                            <th >Fletey Seguro</th>
	                            <th >&nbsp;</th>
	                        </tr>
                    	<?php }else{?>
	                        <tr>
	                            <th colspan="5" align="center" scope="col"><h3>Detalle de Transacci&oacute;n: </h3></th>
	                        </tr>
	                        <tr>	
	                            <th >Art&iacute;culo</th>
	                            <th >Cantidad</th>
	                            <th >Unid/Med </th>
	                            <th >Costo por Unidad</th>
	                            <th >&nbsp;</th>
	                        </tr>
                    	<?php }?>
                    </thead>
                    <tbody>
                    	
                        <?php 
                        $cont = 1; 
                        while(odbc_fetch_row($result2)){

								$aar  = new inf_articulo($Compania, trim(odbc_result($result2,'ATRART')) );
							?>
                            <tr>
                            
                                <td><div align="left" id="aarcod"><?php echo $aar->baardes."(<strong>".trim(odbc_result($result2,'ATRART'))."</strong>)";?></div></td>


                                <?php if($Compania == '40'){?>
                                
                                	<td>
	                                	<div id="div_aslcan">
	                                    	<input type="text" name="aslcan[<?php echo trim(odbc_result($result2,'ATRART'));?>]" id="aslcan[<?php echo trim(odbc_result($result2,'ATRART'));?>]" value="<?php echo number_format(trim(odbc_result($result2,'ATRCAN')),2,".","") ?>"  />
	                                 	</div>
	                                </td>


                                <?php }else{?>

                                	<td>
                                		<div id="aslcan" align="right">
                                			<strong><?php echo @number_format(trim(odbc_result($result2,'ATRCAN')),2,".","");?></strong>
                                		</div>
                                	</td>

                                <?php }?>
                                


                                <td><div id="div_aarnum"><strong><?php echo unidad_medidad(trim(odbc_result($result2,'ATRUMH')),$Compania,trim(odbc_result($result2,'ATRCAN')));?></strong></div></td>
                                <td>
                                	<div id="div_atrcut">
                                    	<input type="text" name="atrcut[<?php echo trim(odbc_result($result2,'ATRART'));?>]" id="atrcut[<?php echo trim(odbc_result($result2,'ATRART'));?>]" value="<?php echo odbc_result($result2,'ATRCUT')!=''?number_format(odbc_result($result2,'ATRCUT'),2,'.',''):number_format(0,2); ?>"  />
                                 	</div>
                                </td>



                                <?php if($Compania == '40' && $cont == 1){?>
                                	<td>
	                                	<div id="div_atrflet">
	                                    	<input type="text" name="atrflt" id="atrflt" value="<?php echo odbc_result($result2,'ATRFLT')!=''?number_format(odbc_result($result2,'ATRFLT'),2,'.',''):number_format(0,2); ?>"  />
	                                 	</div>
	                                </td>
                                <?php }?>



                                <td><div id="erraarcod_<?php echo trim(odbc_result($result2,'ATRART'));?>"></div></td>
                            </tr>

                        <?php 
                        $cont++; 
                    	} ?>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
			<td width="21%" scope="col" colspan="4">
                <div align="center">
                    <input name="Submit4" type="button" onClick="editarvalidarcosto();"" " value="Enviar">&nbsp;
                    <input name="Submit3" type="button" onClick="window.close();"" " value="Cerrar">
                </div>
            </td>
		</tr>
	</table>
    </form>	
</div>

<div align="center" id="agregaraftsav" style="display:none">
  <br />
  <br />
  <br />
  <br />
  <br />
  <br />
  Registro Agregado con exito !
  <br />
  <br />
  <br />
  <input name="Submit3" type="button" onClick="window.close();"" " value="Cerrar">
</div>	
</body>
</html>