<?php
echo '<style>
		.table {
				font:0.8em/145% \'Trebuchet MS\',helvetica,arial,verdana;
				width: 700px;
				border : auto;
				
		}
		
		.tableinfo{
			width: 650px;
		}
		td, th {
				padding:5px;
		}
		
		caption {
				padding: 0 0 .5em 0;
				font-size: 1.4em;
				font-weight: bold;
				background: transparent;
		}

		</style>
<page>
  <page_footer>
  <div align="right">
    Pag. [[page_cu]]/[[page_nb]]
  </div>
  </page_footer>';

	 /*$sql ="SELECT T1.ACICOD, T1.AALCOD, T1.ADPCOD, T1.ATRCOD, T1.ATRNUM,
			 T1.ATRDES, T1.ATRFEC, T1.ATRHOR, T1.ATRMOR, T1.AUSCOD, T1.ATRSTS,
			 T2.ADPCOD, T2.ATRSEC,T2.ATRART, T2.ATRLOT, T2.ATRSER, T2.ATRCAN,
			 T2.ATRUMB, T2.ATRUMH,T2.ATRFAC, T2.ATRCUT, T2.ATRCUS, T2.ATREAC,
			 T2.ATREAA, T2.ATREAL, T2.ATDSTS, T3.ATRDES AS ATRTDES, T1.ATROBS
			FROM IV15FP T1 
			 left join iv16fp T2 on (T1.AALCOD= T2.AALCOD AND T1.ATRNUM=T2.ATRNUM AND T1.ATRCOD=T2.ATRCOD AND T1.ACICOD=T2.ACICOD), 
			 IV12FP T3 
			WHERE
			 T1.ACICOD='$Compania' AND T1.ATRCOD=$atrcod AND T1.ATRNUM=$atrnum AND
			 T1.AALCOD='$almacen' AND T1.ATRCOD=T3.ATRCOD AND 
			 T1.ACICOD=T3.ACICOD ";//T1.AUSCOD='$Usuario' AND
	*/

	if($Compania == '43'){
		$sql = "SELECT T1.ACICOD, T1.AALCOD, T1.ADPCOD, T1.ATRCOD, T1.ATRNUM, T1.ATRDES, T1.ATRFEC, T1.ATRHOR, T1.ATRMOR, T1.AUSCOD, T1.ATRSTS, T2.ADPCOD, T2.ATRSEC,T2.ATRART, T7.ALTCOD AS ATRLOT, T2.ATRSER, T2.ATRCAN, T2.ATRUMB, T2.ATRUMH,T2.ATRFAC, T2.ATRCUT, T2.ATRCUS, T2.ATREAC, T2.ATREAA, T2.ATREAL, T2.ATDSTS, T3.ATRDES AS ATRTDES, T1.ATROBS, T2.ATRLOT, T4.AMCCOD, T5.AMCDES, (SELECT T6.AAPVLA FROM IV17FP T6 WHERE T6.ACICOD=T1.ACICOD AND T6.AARCOD=T2.ATRART AND T6.APACOD IN ('0112') ) AS SERIAL, (SELECT T6.AAPVLA FROM IV17FP T6 WHERE T6.ACICOD=T1.ACICOD AND T6.AARCOD=T2.ATRART AND T6.APACOD IN ('0116') ) AS MODELO, (SELECT T6.AAPVLA FROM IV17FP T6 WHERE T6.ACICOD=T1.ACICOD AND T6.AARCOD=T2.ATRART AND T6.APACOD IN ('0111') ) AS DESLAR

		FROM IV15FP T1 
		LEFT JOIN IV16fp T2 on (T1.AALCOD=T2.AALCOD AND T1.ATRNUM=T2.ATRNUM AND T1.ATRCOD=T2.ATRCOD AND T1.ACICOD=T2.ACICOD)
		LEFT JOIN IV06FP T4 ON (T1.ACICOD=T4.ACICOD AND T2.ATRART=T4.AARCOD) 
		LEFT JOIN IV04FP T5 ON (T1.ACICOD=T5.ACICOD AND T4.AMCCOD=T5.AMCCOD)
		LEFT JOIN iv48fp t7 on (T2.ATRART = T7.AARCOD and T7.ACICOD = T2.ACICOD), IV12FP T3

		WHERE T1.ACICOD='$Compania' AND T1.ATRCOD=$atrcod AND T1.ATRNUM=$atrnum AND T1.AALCOD='$almacen' AND T1.ATRCOD=T3.ATRCOD AND T1.ACICOD=T3.ACICOD";
	}else{
		$sql ="SELECT T1.ACICOD, T1.AALCOD, T1.ADPCOD, T1.ATRCOD, T1.ATRNUM, T1.ATRDES, T1.ATRFEC, T1.ATRHOR, T1.ATRMOR, T1.AUSCOD, T1.ATRSTS, T2.ADPCOD, T2.ATRSEC,T2.ATRART, T2.ATRLOT, T2.ATRSER, T2.ATRCAN, T2.ATRUMB, T2.ATRUMH,T2.ATRFAC, T2.ATRCUT, T2.ATRCUS, T2.ATREAC, T2.ATREAA, T2.ATREAL, T2.ATDSTS, T3.ATRDES AS ATRTDES, T1.ATROBS, T2.ATRLOT, T4.AMCCOD, T5.AMCDES, (SELECT T6.AAPVLA FROM IV17FP T6 WHERE T6.ACICOD=T1.ACICOD AND T6.AARCOD=T2.ATRART AND T6.APACOD IN ('0112') ) AS SERIAL, (SELECT T6.AAPVLA FROM IV17FP T6 WHERE T6.ACICOD=T1.ACICOD AND T6.AARCOD=T2.ATRART AND T6.APACOD IN ('0116') ) AS MODELO, (SELECT T6.AAPVLA FROM IV17FP T6 WHERE T6.ACICOD=T1.ACICOD AND T6.AARCOD=T2.ATRART AND T6.APACOD IN ('0111') ) AS DESLAR
		
		FROM IV15FP T1 
		LEFT JOIN IV16fp T2 on (T1.AALCOD=T2.AALCOD AND T1.ATRNUM=T2.ATRNUM AND T1.ATRCOD=T2.ATRCOD AND T1.ACICOD=T2.ACICOD)
		LEFT JOIN IV06FP T4 ON (T1.ACICOD=T4.ACICOD AND T2.ATRART=T4.AARCOD) 
		LEFT JOIN IV04FP T5 ON (T1.ACICOD=T5.ACICOD AND T4.AMCCOD=T5.AMCCOD), IV12FP T3 
		WHERE T1.ACICOD='$Compania' AND T1.ATRCOD=$atrcod AND T1.ATRNUM=$atrnum AND T1.AALCOD='$almacen' AND T1.ATRCOD=T3.ATRCOD AND T1.ACICOD=T3.ACICOD ";//T1.AUSCOD='$Usuario' AND
	}

	//SELECT T6.AAPVLA FROM iv17fp T6 WHERE T6.ACICOD='41' and T6.AARCOD='' AND T6.APACOD IN ('0142')
	
	$result = odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111")); 
	$result2 = odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111")); 	
	
	if($Compania =='14'){$img = 'logoidacadef20052.png'; $rif= '';}
	else if($Compania =='01'){$img = 'logomeditronnuevo2.png';$rif= '';}
	else{$img = 'logomeditronnuevo2.png';$rif= '';}
echo 
	'<table class="table">
		<thead>
			<tr>
				<th valign="middle" width="325px"><img src="../images/'.$img.'"/><h5>R.I.F.: '.$Companiarif.'</h5></th>
				<th valign="middle" scope="col" align="center" colspan="2">
					<!--<table>
						<tr>
							<th>
								<h3>'.wordwrap(utf8_encode(trim(odbc_result($result, 'ATRTDES'))),10,"<br>\n").'</h3>
							</th>
							<th>
								<h3>Nro '.$atrnum.'</h3>
							</th>
						</tr>
					</table>
					-->
						<table >
						<tr>
							<td>Fecha:</td>
							<td>
								'.odbc_result($result, 'ATRFEC').'
							</td>
						</tr>
					</table>
					<table >
						<tr>
							<td>Almac&eacute;n</td>
							<td>
								'.wordwrap(trim(utf8_encode(alamcen(trim(odbc_result($result, 'AALCOD')),$Compania))),15,"<br>\n").'
							</td>
						</tr>
					</table>
					 
				</th>
			</tr>
			<tr >
				<td colspan="3">
					<table class="tableinfo" style="border:1">
						<tr>
							<!--
							<td><strong>Almac&eacute;n</strong></td>
							<td>
								'.utf8_encode(alamcen(trim(odbc_result($result, 'AALCOD')),$Compania)).'
							</td>
							-->
							<td><strong>Transacci&oacute;n:</strong></td>
							<td >
								<!--'.utf8_encode(wordwrap(trim(odbc_result($result, 'ATRTDES'))." (".trim(odbc_result($result, 'ATRCOD')),30,'<br />')).')-->
								'.utf8_encode( wordwrap(trim( odbc_result($result, 'ATRTDES') ) ,30,'<br />' ) ).'
							</td>
							<td align="right"><strong>N&uacute;mero: </strong></td>
							<td>
								'.trim(odbc_result($result, 'ATRNUM')).'
							</td>
						</tr>
						<tr valign="middle">
							<td><strong>Descripci&oacute;n: </strong></td>
							<td colspan="4">
								<div align="left" id="atrdes" >'.utf8_encode(wordwrap(trim(odbc_result($result, 'ATRDES')),74,'<br />')).'</div>
							</td>
						</tr>
						<tr>
							<td><strong>Observaci&oacute;n: </strong></td>
							<td  colspan="4">
								'.utf8_encode(wordwrap(trim(odbc_result($result, 'ATROBS')),74,'<br />')).'
							</td>
						</tr>
						
					</table>
				</td>
			</tr>
		</thead>
		<tr>
			<td colspan="3">
				<hr />
			</td>
		</tr>
		<tr>
			<td colspan="3">
				<table class="tableinter">
					<tr>
						<td colspan="3" scope="col">'; 
							if($Compania != '43'){
						 		echo '<h3>Par&aacute;metros Adicionales: </h3>';
							}
							echo'
					<!--		<span class="header"></span>
						</td>
					</tr>
					<tr>
						<td>-->
							';

								$sql = "SELECT T1.ACICOD, T1.ATRCOD, T1.AMDCOD, T1.APDCOD, T2.APDDES,     
									T2.APDSEC, T2.APDLON, T2.APDDCR, T2.APDTIP, T2.APDVAL, T2.ATACOD, 
									T2.ASBCOD, T2.APDSTS, T2.APDLND, T3.ASBDES, T3.ASBSTS, T4.APACOD, 
									T4.ATRNUM, T4.AAPVLA, T4.AAPDES, T4.AAPLON, T4.AAPLND, T4.AAPSEC, 
									T4.AAPTIP FROM mb10fp T3, MB03FP T2, iv37FP T1                    
									LEFT JOIN IV38FP T4                                               
									ON ( T1.ACICOD=T4.ACICOD AND T4.ATRNUM=".trim(odbc_result($result, 'ATRNUM'))." AND T1.ATRCOD=T4.ATRCOD  
									AND T1.APDCOD=T4.APACOD)                                          
									WHERE T1.ACICOD='$Compania' AND T1.APDCOD=T2.APDCOD AND                  
									T1.AMDCOD=T2.AMDCOD AND T1.AMDCOD='$modulo' AND T2.ATACOD=T3.ATACOD  
									AND T2.ASBCOD=T3.ASBCOD AND T1.ATRCOD='".trim(odbc_result($result, 'ATRCOD'))."' ORDER BY T2.ASBCOD, 
									T2.APDSEC, T1.APDCOD";
								echo utf8_encode(fparam_adi_add_ver_transaccion('agregarform','',$sql,'1'));

echo                             
							'<div id="parametros"></div>
						</td>
					</tr>
					
				 </table>
			</td>
		</tr>
		<tr>
			<td colspan="3">
				<hr />
			</td>
		</tr>
		<tr >
			<th >Art&iacute;culo</th>
			<th >Cantidad</th>
			<th >Unid/Med </th>
		</tr>
		<tbody>';
									while(odbc_fetch_row($result2)){
					$aar  = new inf_articulo($Compania, trim(odbc_result($result2,'ATRART')) );
					$lote = trim(odbc_result($result2, 'ATRLOT'));
					if($lote!=''){$loteinf = " - <b>LOTE:</b> ".odbc_result($result2, 'ATRLOT');}else{$loteinf='';}
				echo 
				'<tr>
					<td>
						'.wordwrap(utf8_encode(trim($aar->baardes).'('.trim(odbc_result($result2,'ATRART'))),45,'<br />').') '.$loteinf;
						if($Compania =='40'){
							$lista  = '<br />';
							(!empty(odbc_result($result2,'AMCDES')) && trim(odbc_result($result2,'AMCDES')) != 'SIN MARCA')? $lsit.='&nbsp;<strong>Marca</strong>: '.utf8_encode(odbc_result($result2,'AMCDES')).'<br />':'';
							!empty(odbc_result($result2,'MODELO'))?$lista.='&nbsp;<strong>Modelo</strong>: '.utf8_encode(odbc_result($result2,'MODELO')).'<br />':'';
							!empty(odbc_result($result2,'SERIAL'))?$lista.='&nbsp;<strong>Serial</strong>: '.utf8_encode(odbc_result($result2,'SERIAL')).'':'';							
							echo $lista;					
						}
					echo '	
					</td>
					<td>'.@number_format(trim(odbc_result($result2,'ATRCAN')),2,",",".").'</td>
					<td>'.utf8_encode(unidad_medidad(trim(odbc_result($result2,'ATRUMH')),$Compania,trim(odbc_result($result2,'ATRCAN')))).'</td>
				</tr>';
			}
		echo 
		'</tbody>
	</table>
</page>';
