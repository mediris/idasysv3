/*
jDavila
03/04/12
*/
function agregar()
{
	document.agregarform.atrcod.value="";
	document.agregarform.atrdes.value="";

	$("#aftsav").hide(1);
}

/*
jDavila
03/04/12
*/
function agregarvalidar(){	

	document.getElementById('erraalcod').innerHTML = "";
	document.getElementById('erratrcod').innerHTML = "";
	document.getElementById('erratrdes').innerHTML = "";
	document.getElementById('erratrobs').innerHTML = "";
	
	
	var contenido= "";
	var elemento;
	var nodetalle= "";
	document.body.style.cursor="wait";

	$.ajax({
	   	async: false,
        type: "POST",
		dataType: "JSON",
        url: "transaccionesagregarvalidarphp.php",
        data: $("#agregarform").serialize(),
        success: function(datos){
			vdata = parseJSONx(datos);}							
	});			
	sierror='N';
	for(i in vdata)	{
		if(vdata[i].campo){
			document.getElementById(vdata[i].campo).innerHTML=vdata[i].msg;
			sierror='S';		
		}
		if(vdata[i].ATRNUM){
			document.solicitudes.atrnum.value=vdata[i].ATRNUM;
			document.getElementById('divatrnum').innerHTML = vdata[i].ATRNUM;
		}
		if(vdata[i].AALCOD){
			document.agregarform.aalcod.value=vdata[i].AALCOD;
			document.solicitudes.aalcod.value=vdata[i].AALCOD;
			document.getElementById('divaaldes').innerHTML = vdata[i].AALDES;
		}
		if(vdata[i].ATRCOD){
			document.agregarform.atrcod.value=vdata[i].ATRCOD;
			document.solicitudes.atrcod.value=vdata[i].ATRCOD;
			document.getElementById('divatrdes1').innerHTML = vdata[i].ATRDES1;
		}
		if(vdata[i].ATRDES){
			$('#divatrdes').html(vdata[i].ATRDES);
			//document.getElementById('divatrdes').innerHTML = vdata[i].ATRDES;
		}
		
	}
	
	document.body.style.cursor="default";
	if(sierror=='N')
	{
		if(nodetalle=="S"){
			finalizarreq();
		}else{
			capturararticulo(' ');
			document.getElementById('ocultarform').style.display="none";
			document.getElementById('reqst06fp').style.display="";
		}//fin del else
	}
}

/*
jDavila
30/03/12
*/
function editar(cod, num, alm) {

	
	var param = [];
	param['atrcod']=cod;
	param['atrnum']=num;
	param['aalcod']=alm;
	ejecutasqlp("transaccionesinformacionphp.php",param);

	for(i in gdata)
	{
		document.agregarform.atrnum.value=gdata[i].ATRNUM;
		document.getElementById("wsatrnum").innerHTML=gdata[i].ATRNUM;
		document.agregarform.atrcod.value=gdata[i].ATRCOD;
		document.getElementById("wsatrcod").innerHTML=gdata[i].ATRTDES +" ("+ gdata[i].ATRCOD +")" ;
		document.agregarform.atrdes.value=gdata[i].ATRDES;
		document.agregarform.atrobs.value=gdata[i].ATROBS;
	};
	$("#aftsav").hide(1);
}

/*
jDavila
30/03/12
*/
function editarvalidar(){	
	var contenido= "";
	var elemento;
	var nodetalle= "";
	var alma = document.getElementById('aalcod').value;
	document.body.style.cursor="wait";
	$.ajax({
			async: false,
			type: "POST",
			dataType: "JSON",
			url: "transaccioneseditarvalidarphp.php",
			data: $("#agregarform").serialize(),
			success: function(datos){
						vdata = parseJSONx(datos);
					}
	});			
	var sierror='N';
	for(i in vdata)	{
		if(vdata[i].campo) 	{
			document.getElementById(vdata[i].campo).innerHTML=vdata[i].msg;
			sierror='S';
		}
		if(vdata[i].ATRNUM){
			document.solicitudes.atrnum.value=vdata[i].ATRNUM;
			document.getElementById('divatrnum').innerHTML = vdata[i].ATRNUM;
		}
		if(vdata[i].AALCOD){
			document.agregarform.aalcod.value=vdata[i].AALCOD;
			document.solicitudes.aalcod.value=vdata[i].AALCOD;
			document.getElementById('divaaldes').innerHTML = vdata[i].AALDES;
		}
		if(vdata[i].ATRCOD){
			document.agregarform.atrcod.value=vdata[i].ATRCOD;
			document.solicitudes.atrcod.value=vdata[i].ATRCOD;
			document.getElementById('divatrdes1').innerHTML = vdata[i].ATRTDES;
		}
		if(vdata[i].ATRDES){
			document.getElementById('divatrdes').innerHTML = vdata[i].ATRDES;
		}
		/*seccion para cargar los articulos*/
		//alert(vdata[i].ATRART);
		//AND vdata[i].ATRART!="undefined"
		if (vdata[i].ATRART!='' )
		{
			var value4 = vdata[i].ATRART+'@'+vdata[i].ATRUMB;
			var texto4 = vdata[i].AARDES;
			var cantidad =  NumFormat(vdata[i].ATRCAN, '2', '10', '.', '');
			//var texto5 = vdata[i].ATRUMH;
			var texto5 = vdata[i].AUMDES;
			var value6 = vdata[i].ATRLOT;
			var texto6 = vdata[i].ATRLOT==0?"N/A":vdata[i].ATRLOT;

			cadena = "<tr>";
			cadena = cadena + "<td>" + texto4 + "</td>";
			cadena = cadena + "<td ><div align='right'>" + NumFormat(Number(cantidad),'2', '10', '.', '') + "</div></td>";
			cadena = cadena + "<td aling='center'>" + texto6 + "</td>";
			cadena = cadena + "<td>" + texto5 + "</td>";
			cadena = cadena + "<td><a class='elimina'  id='"+value4+"' name='"+value4+"'   href='javascript:fn_dar_eliminar(\"" + value4 + "\" , "+vdata[i].ATRNUM+",\"" + vdata[i].ATRCOD + "\", \"" + value6 + "\" );'><img src='../images/eliminar.gif' alt='Eliminar' width='15' height='15' border='0'></a></td>";
			cadena =  cadena + "</tr>";
			$("#grilla").append(cadena);
			
		};
	}
	document.body.style.cursor="default";
	if(sierror=='N')
	{
		if(nodetalle=="S"){
			finalizarreq();
		}else{
			capturararticulo(' ');
			document.getElementById('ocultarform').style.display="none";
			document.getElementById('reqst06fp').style.display="";
		}//fin del else
	}

}

/*
jDavila
30/03/12
*/
function eliminar(tran, num) {
	if (confirm('Seguro que desea borrar la transacion ' + num + '-' + tran + '?'))
	{
		var param = [];
		param['atrcod']=tran;
		param['atrnum']=num;
		ejecutasqld("transaccioneseliminar.php",param);
		location.reload();
	}
}


/**/
function finalizarreq()
{		
	$("#agregaraftsav").hide(1000);		
	parent.location.reload();
	parent.Shadowbox.close();
}	

function capturararticulo(code)
{
	
	$.get ("buscararticulo.php", { code: code },function(resultado)
	{
			if(resultado == false)
			{
				alert("No existe ningun art\u00edculo para tipo de requisici\u00f3n");
				document.getElementById("aarcod").value=0;
				$("#aarcod").attr("disabled",true);
			}
			else
			{
				$("#aarcod").attr("disabled",false);
				document.getElementById("aarcod").options.length=1;
				$('#aarcod').append(resultado);
			}
		}

	);
}

function cargarparametros(obj,num)
{
	//if(num=='')num=0;
	var param = [];
	var atrnum = num;
	var value = $('#'+obj.id).val();
	if (value==0){
		value = 99999999;
	}
	param['code']=value;
	param['num']=atrnum;
	$.ajax({
		async: false,
		type: 'POST',
		url: 'cargarparametros.php',
		beforeSend:function(){
			$('#parametros').html('<span class="input-notification attention png_bg">Cargando...</span>');
		},
		data: convertToJson(param),
		success: function(html){                
			$('#parametros').html(html);
		}
	});
}


function choiceart(targ,selObj,restore,e){ //v3.0
	dat = selObj.options[selObj.selectedIndex].value;
	var datos = dat.split("@");
	/*document.getElementById("aarumb").value=datos[1];*/
	var nro = document.getElementById('atrnum').value;
	var tran = document.getElementById('atrcod').value;
	combo_lote_trans(datos[0],nro, tran);
	combo_uni(datos[0], datos[1]);
	//alert(document.getElementById("aarumb").value + "-" +datos[1]);
	activaimagengrabar(1);
}


function activaimagengrabar(num){ 
	//alert("*-*"+document.getElementById("aslcan").value+"**");
	document.getElementById("aslcan").focus();
	if ((document.getElementById("aslcan").value !==0) && (document.solicitudes.aarcod.selectedIndex!=='')) {
	//if ((document.getElementById("aslcan").value !='') && (document.solicitudes.aarcod.selectedIndex!=='')) {
		document.getElementById("imagengrabar").style.display='block';}
	if ((document.getElementById("aslcan").value == 0) || (document.solicitudes.aarcod.selectedIndex=='')) {
	//if ((document.getElementById("aslcan").value == '') || (document.solicitudes.aarcod.selectedIndex=='')) {
		document.getElementById("imagengrabar").style.display='none'}
}
function esenter(e) {
	var tecla;
	if (window.event) {tecla = window.event.keyCode;}
	else if (e) tecla = e.which;
	else return false;
	if (tecla == 13)   	{
		if ((document.getElementById("aslcan").value !=0) && (document.solicitudes.aarcod.selectedIndex!='')) {
		//alert("***"+document.getElementById("aslcan").value+"**");
		//if (((document.getElementById("aslcan").value !="")||(document.getElementById("aslcan").value !=" ")) && (document.solicitudes.aarcod.selectedIndex!='')) {
			grabardetallereq(); 	
			return false;
		}
	}
	else{return false;  }

}


//Requerimiento grabar en iv16fp
	function grabardetallereq(){
				var indice6 = document.solicitudes.atlcod.selectedIndex;
    			var texto6 = document.solicitudes.atlcod.options[indice6].text;
				var value6 = document.solicitudes.atlcod.options[indice6].value ;

				var indice4 = document.solicitudes.aarcod.selectedIndex;
    			var texto4 = document.solicitudes.aarcod.options[indice4].text;
				var value4 = document.solicitudes.aarcod.options[indice4].value;
    			//var texto5 = document.solicitudes.aarumb.value;
				var indice5 = document.solicitudes.aarumb.selectedIndex;
    			var texto5 = document.solicitudes.aarumb.options[indice5].text;
				var value5 = document.solicitudes.aarumb.options[indice5].value;
				var tran =  document.solicitudes.atrcod.value;
				if(value6 !=-1){
				value6 = value6<0?"":value6 ;
                cadena = "<tr>";
				cadena = cadena + "<td>" + texto4 + "</td>";
                cadena = cadena + "<td> <div align='right'>" + NumFormat($("#aslcan").val() , '2', '10', '.', ',') + "</div></td>";
                cadena = cadena + "<td>" + texto6 + "</td>";
                cadena = cadena + "<td>" + texto5 + "</td>";
                cadena = cadena + "<td><a class='elimina'  id='"+value4+"' name='"+value4+"'   href='javascript:fn_dar_eliminar(\"" + value4 + "\" , this,\"" + tran + "\", \"" + value6 + "\" );'><img src='../images/eliminar.gif' alt='Eliminar' width='15' height='15' border='0'></a></td>";
				
				$.ajax({
	   			async: false,
					type: "POST",
					dataType: "JSON",
					url: "guardardetalles.php",
					data: $("#solicitudes").serialize(),
					success: function(datos){
						vdata = parseJSONx(datos);}							
				});			
                sierror='N';
				for(i in vdata)	{
					if(vdata[i].campo) 	{
						sierror='S';
					}
				}
												
				if (sierror=='S')	{
									alert("Producto ya se encuentra en la transaccion");
									//return false; 
									}								
				else				{
									$("#grilla").append(cadena);
									document.getElementById("aarcod").value="";
									document.getElementById("aslcan").value="";
									document.getElementById("aarumb").value="";
									combo_lote('','');
									combo_uni(document.getElementById("aarcod").value, document.getElementById("aarumb").value);
									activaimagengrabar();
									document.getElementById("aarcod").focus();
									}
				}else{
					alert('Continue con la Creacion del Lote para el Articulo Seleccionado.');
					window.open('../lotes/lotesfagregar.php?articulo='+document.getElementById("aarcod").value+'', '', 'width=800,height=400');
				}	
            };

		function fn_dar_eliminar(cod, hcd, tran, lot){
                $("a.elimina").click(function(){
					hcd = document.solicitudes.atrnum.value;
                    id = $(this).parents("tr").find("td").eq(0).html();
                    respuesta = confirm("Desea eliminar el Art\u00edculo: " + id);
                    if (respuesta){									
                            $(this).parents("#grilla tr").remove();
							var param = [];
							param['atrnum']=hcd;
							param['aarcod']=this.id;
							param['atrcod']=tran;
							param['atlcod']=lot;
							$.ajax({
	   						async: false,
        					type: "POST",
							dataType: "JSON",
        					url: "eliminardetalle.php",
        					data: convertToJson(param),
        					success: function(datos){
								//alert("Art\u00f3culo " + id + " eliminado")
							}							
                        })
                    }
                });
            };
			
			
/*
 * @jDavila
 *13/04/2012
 */
function combo_uni(art, medi)
{
	var param = [];
	param['medi']=medi;
	param['art']=art;
	$.ajax({
		async: false,
		type: 'POST',
		url: 'cargarunimedida.php',
		beforeSend:function(){
			$('#div_aarnum').html('<span class="input-notification attention png_bg">Cargando...</span>');
		},
		data: convertToJson(param),
		success: function(html){                
			$('#div_aarnum').html(html);
		}
	});
}

function transaccionver(atrcod, atrnum, aalcod) 
{
	self.name = "main"; // names current window as "main"
	var windowprops ="toolbar=0,location=0,directories=0,status=0,"+"menubar=0,scrollbars=1,resizable=0,width=990,height=800";
	var ventana="transaccionesver.php?&tipo="+atrcod+"&num="+atrnum+"&almacen="+aalcod+"";
	window.open(ventana,"", windowprops); // opens remote control
}

function transaccioncostos(atrcod, atrnum, aalcod)
{
	self.name = "main"; // names current window as "main"
	var windowprops ="toolbar=0,location=0,directories=0,status=0,"+"menubar=0,scrollbars=1,resizable=0,width=990,height=800";
	var ventana="transaccioncostos.php?&tipo="+atrcod+"&num="+atrnum+"&almacen="+aalcod+""; 
	window.open(ventana,"", windowprops); // opens remote control
}

/*
 * @jDavila
 * @numero: Es el número que se desea formatear.
 * @decimales: número de decimales en valor regresado.
 * @miles: número de digitos en la separación de miles.
 * @decSep: separador de decimales. El valor predeterminado es el ‘punto’.
 * @milSep: separador de miles. El valor predeterminado para el separador de miles es la ‘coma’.
 * formate el numero con las caracteristicas dadas
 */
function NumFormat(numero, decimales, miles, decSep, milSep)
{
	if(typeof(miles)=="undefined"){miles=9999;}
	if (typeof(decSep)=="undefined"){decSep = ".";}
	if (typeof(milSep)=="undefined"){milSep=",";}
 	var str = numero.toString();
 	var pos = str.indexOf(decSep);
 	if (pos==-1) str = str + decSep;
 	pos = str.indexOf(decSep)
 	var arr =  str.split("");
 	var ret  = "";
 	var i = 0;
 	k = pos-1;
 	m = 0
	while (k >= 0)
	{
		if (m==miles){
	   		ret = milSep + ret;
	   		m = 0;
	  	}
	  	++m;
	  	ret = arr[k--] +   ret;
	}
    if (decimales==0) return ret;  
 	ret = ret +  decSep;
 	var j=0;
 	i = pos + 1;
	while (i < str.length && j < decimales){
		ret += arr[i++];
	   	++j;
	}
 	while ( j<decimales){
  		ret += "0";
		++j;
 	}
	return ret;
}
/*
 * @jDavila
 * @tipo: Tipo Transaccion.
 * @atrcod: número de transaccion.
 * @alma: codigo almacen.
 */
function requerimimprimir(tipo, atrcod, alma)
{
	var windowprops = "toolbar=0,location=0,directories=0,status=0, " + "menubar=0,scrollbars=0,resizable=0,width=600,height=300";
	OpenWindow = window.open("exportarapdf.php?&atrnum="+tipo+"&aalcod="+alma+"&atrcod="+atrcod, "enviodecorreo", windowprops);
}
/*
 * @jDavila
 */
function editarvalidarcosto()
{
	$.ajax({
	async: false,
		type: "POST",
		dataType: "JSON",
		url: "guardardetallescosto.php",
		data: $("#costoform").serialize(),
		success: function(datos){
			vdata = parseJSONx(datos);
			}							
	});			
	sierror='N';
	var msj = '';
	for(i in vdata)	{
		if(vdata[i].campo) 	{
			sierror='S';
			msj = vdata[i].msg;
			campo = vdata[i].campo;
		}
	}
									
	if (sierror=='S')	{
		$("#"+campo).html(msj);
		alert("Falla en carga de Costo.");	
		return false;
	}								
	else
	{
		$("#agregaraftsav").show(1000);
		$("#ocultarform").hide(1000);
	}
	
};


/*
 * @jDavila
 * @tipo: carga informacion param adicio.
 * @cod: codigo de transaccion.
 * @num: número de transaccion.
 * @alm: codigo almacen.
 */
function cargarinfopa(cod, num, alm) {

	
	var param = [];
	param['atrcod']=cod;
	param['atrnum']=num;
	param['aalcod']=alm;
	ejecutasqlp("transaccionesinformacionphp.php",param);

	for(i in gdata)
	{
		$("#atrnum").val(gdata[i].ATRNUM);
		$("#wsatrnum").html(gdata[i].ATRNUM);
		
		$("#atrcod").val(gdata[i].ATRCOD);
		$("#wsatrcod").html(gdata[i].ATRTDES +" ("+ gdata[i].ATRCOD +")");

		$("#atrdes").val(gdata[i].ATRDES);
		$("#wsatrdes").html(gdata[i].ATRDES);
		
		$("#atrobs").val(gdata[i].ATROBS);
		$("#wsatrobs").html(gdata[i].ATROBS);
	};
	$("#aftsav").hide(1);
}

/*
jDavila
22/07/2015
*/
function editarvalidarpa(){	
	var contenido= "";
	var elemento;
	var nodetalle= "";
	var param = [];

	param['aalcod']=$("#aalcod").val();
	param['atrcod']=$("#atrcod").val();
	param['atrnum']=$("#atrnum").val();
	param['atrdes']=$("#atrdes").val();
	param['atrobs']=$("#atrobs").val();
	param['paradi']=$("#paradi").val();
	
	document.body.style.cursor="wait";
	$.ajax({
			async: false,
			type: "POST",
			dataType: "JSON",
			url: "transaccioneseditarvalidarphp.php",
			data: $("#agregarformpa").serialize(),
			success: function(datos){
						vdata = parseJSONx(datos);
					}
	});			
	var sierror='N';
	
	document.body.style.cursor="default";
	if(sierror=='N')
	{
		if(nodetalle=="S"){
			finalizarreq();
		}else{
			document.getElementById('ocultarform').style.display="none";
			document.getElementById('agregaraftsav').style.display="";
		}//fin del else
	}

}