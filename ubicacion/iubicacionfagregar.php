<?php 
/*
* jdavila 
* 02/03/2012
*/
session_start();
include("../conectar.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="sinhead">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>agregar ubicacion</title>
<link href="../<?php echo $css; ?>" rel="stylesheet" type="text/css" media="screen" />
<script language="JavaScript" src="../javascript/jquery.js"></script>
<script language="JavaScript" src="../javascript/javascript.js"></script>
<script language="JavaScript" src="javascript.js"></script>
</head>
<body class="sinbody" bgcolor="#FFFFFF" onload="javascript:agregar()">

<div id="aediv" class="white_content">
        <form id="form" name="form" method="post" action="" class="form">
            <table width="100%"  border="0">
              <tr>
                <th width="20%" scope="col"><label>Indice de Ubicaci&oacute;n</label></th>
                <th width="36%" scope="col"><div align="left">
                  <input name="aiucod" type="text" id="aiucod" size="04" maxlength="04" >
                </div></th>
                <th colspan="2" id="erraiucod" class="Estilo5" scope="col">&nbsp;</th>
              </tr>
              <tr>
                <th scope="col"><label>Descripción</label></th>
                <th scope="col"><div align="left">
                <textarea name="aiudes" cols="50" rows="2" id="aiudes" ></textarea>
                </div></th>
                <th colspan="2" id="erraiudes" class="Estilo5" scope="col">&nbsp;</th>
              </tr>
              <tr>
                <th scope="col"><label>Niveles</label></th>
                <th scope="col"><div align="left">
                  <input name="aiuniv1" type="text" id="aiuniv1" size="2" maxlength="2" onKeyPress="return Tecla(event, this);" onchange="javascript:validatotalcaracteres(this)">
                - <input name="aiuniv2" type="text" id="aiuniv2" size="2" maxlength="2" onKeyPress="return Tecla(event, this);" onchange="javascript:validatotalcaracteres(this)">
                - <input name="aiuniv3" type="text" id="aiuniv3" size="2" maxlength="2" onKeyPress="return Tecla(event, this);" onchange="javascript:validatotalcaracteres(this)">
                - <input name="aiuniv4" type="text" id="aiuniv4" size="2" maxlength="2" onKeyPress="return Tecla(event, this);" onchange="javascript:validatotalcaracteres(this)">
                Longitud Saldo: 
                <input name="aiunivs" type="text" disabled="disabled" id="aiunivs" value="15" size="2" maxlength="2">
                </div></th>
                <th colspan="2" id="erraiuniv" class="Estilo5" scope="col">&nbsp;</th>
              </tr> 
              <tr>
                <th scope="col">&nbsp;</th>
                <th scope="col"></th>
                <!--
                <th width="23%"  scope="col"><p onclick="iubicacionesagregarcerrar()" class="subir" align="right">salir</p></th>
                <th width="21%"  scope="col"><p align="right" class="subir" onclick="iubicacionesagregarvalidar()">Grabar</p></th>
                -->
                <th width="23%"  scope="col"><input name="cancelar" type="button" onClick="parent.location.reload();parent.Shadowbox.close();"  value="Salir" id="cancelar"></th>
		        <th width="21%"  scope="col"><input type="button" name="submit" id="submit" value="Grabar" onClick="agregarvalidar()" /></th>
              </tr>
            </table>
    	</form>
</div>
<div align="center" id="aftsav">
  <br />
  <br />
  <br />
  <br />
  <br />
  <br />
  Registro Agregado con exito !
  <br />
  <br />
  <br />
  <input name="Submit3" type="button" onClick="parent.location.reload();parent.Shadowbox.close();"" " value="Cerrar"></div>
</body>
</html>