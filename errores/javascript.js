/*
jDavila
02/03/12
*/
function agregar()
{
	document.agregarform.aidcod.value="";
	document.agregarform.aercod.value="";
	document.agregarform.aerd1n.value="";
	document.agregarform.aerd2n.value="";
	document.agregarform.aerres.value="";
	document.agregarform.aergra.value="";
	$("#agregaraftsav").hide(1);
}

/*
jDavila
02/03/12
*/
function agregarvalidar(){	
	var contenido= "";
	var elemento;
	document.body.style.cursor="wait";

	$.ajax({
	   	async: false,
        type: "POST",
		dataType: "JSON",
        url: "erroragregarvalidarphp.php",
        data: $("#agregarform").serialize(),
        success: function(datos){
			vdata = parseJSONx(datos);}							
	});			
	sierror='N';
	for(i in vdata)	{
		if(vdata[i].campo) 	{
			document.getElementById(vdata[i].campo).innerHTML=vdata[i].msg;
			sierror='S';
		}
	}
	document.body.style.cursor="default";
	if(sierror=='N') 		
	{
		$("#agregardiv").hide(1000);
		$("#agregaraftsav").show(1000);
	}
}

/*
jDavila
02/03/12
*/
function editar(error, idm) {

	var param = [];
	param['aercod']=error;
	param['aidcod']=idm;
	ejecutasqlp("errorinformacionphp.php",param);

	for(i in gdata)
	{
		document.editarform.aidcod.value=gdata[i].AIDCOD;
		document.editarform.aercod.value=gdata[i].AERCOD;
		document.getElementById("wsaercod").innerHTML=gdata[i].AERCOD;
		document.getElementById("wsaidcod").innerHTML=gdata[i].AIDCOD+' '+gdata[i].AIDDES;
		document.editarform.aerd1n.value=gdata[i].AERD1N;
		document.editarform.aerd2n.value=gdata[i].AERD2N;
		document.editarform.aerres.value=gdata[i].AERRES;
		document.editarform.aergra.value=gdata[i].AERGRA;
	};
	$("#agregaraftsav").hide(1);
}

/*
jDavila
02/03/12
*/
function editarvalidar(){	
	var contenido= "";
	var elemento;
	document.body.style.cursor="wait";
	$.ajax({
			async: false,
			type: "POST",
			dataType: "JSON",
			url: "erroreditarvalidarphp.php",
			data: $("#editarform").serialize(),
			success: function(datos){
						vdata = parseJSONx(datos);}							
	});			
	var sierror='N';
	for(i in vdata)	{
		if(vdata[i].campo) 	{
			document.getElementById(vdata[i].campo).innerHTML=vdata[i].msg;
			sierror='S';
		}
	}
	document.body.style.cursor="default";
	if(sierror=='N') 		
	{
		$("#editarform").hide(1000);
		$("#agregaraftsav").show(1000);	
	}

}

/*
jDavila
05/03/12
*/
function eliminar(err, idm) {
	if (confirm('Seguro que desea borrar el error' + idm + '-' + err + '?'))
	{
		var param = [];
		param['error']=err;
		param['idioma']=idm;
		ejecutasqld("erroreliminar.php",param);
		location.reload();
	}
}
