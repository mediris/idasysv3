/*
jDavila
26/03/12
*/
function agregar()
{
	document.form.amccod.value="";
	document.form.amcdes.value="";
	document.form.amcnoi[1].checked=true
	document.form.amcrec[0].checked=true
	$("#aftsav").hide(1);
}

/*
jDavila
26/03/12
*/
function agregarvalidar(){	
	var contenido= "";
	var elemento;
	document.body.style.cursor="wait";

	$.ajax({
	   	async: false,
        type: "POST",
		dataType: "JSON",
        url: "marcasagregarvalidarphp.php",
        data: $("#form").serialize(),
        success: function(datos){
			vdata = parseJSONx(datos);}							
	});			
	sierror='N';
	for(i in vdata)	{
		if(vdata[i].campo) 	{
			document.getElementById(vdata[i].campo).innerHTML=vdata[i].msg;
			sierror='S';
		}
	}
	document.body.style.cursor="default";
	if(sierror=='N') 		
	{
		$("#aediv").hide(1000);
		$("#aftsav").show(1000);
	}
}

/*
jDavila
26/03/12
*/
function editar(tipo) {

	var param = [];
	param['amccod']=tipo;
	ejecutasqlp("marcasinformacionphp.php",param);

	for(i in gdata)
	{
		document.form.amccod.value=gdata[i].AMCCOD;
		document.getElementById("wsamccod").innerHTML=gdata[i].AMCCOD;
		document.form.amcdes.value=gdata[i].AMCDES;
		if(gdata[i].AMCNOI=='I') {document.form.amcnoi[1].checked=true;}
		if(gdata[i].AMCNOI=='N') {document.form.amcnoi[0].checked=true;}
		if(gdata[i].AMCREC=='A') {document.form.amcrec[0].checked=true;}
		if(gdata[i].AMCREC=='B') {document.form.amcrec[1].checked=true;}
		if(gdata[i].AMCREC=='C') {document.form.amcrec[2].checked=true;}
	};
	$("#aftsav").hide(1);
}

/*
jDavila
26/03/12
*/
function editarvalidar(){	
	var contenido= "";
	var elemento;
	document.body.style.cursor="wait";
	$.ajax({
			async: false,
			type: "POST",
			dataType: "JSON",
			url: "marcaseditarvalidarphp.php",
			data: $("#form").serialize(),
			success: function(datos){
						vdata = parseJSONx(datos);}							
	});			
	var sierror='N';
	for(i in vdata)	{
		if(vdata[i].campo) 	{
			document.getElementById(vdata[i].campo).innerHTML=vdata[i].msg;
			sierror='S';
		}
	}
	document.body.style.cursor="default";
	if(sierror=='N') 		
	{
		$("#form").hide(1000);
		$("#aftsav").show(1000);	
	}

}

/*
jDavila
26/03/12
*/
function eliminar(tipo) {
	if (confirm('Seguro que desea borrar la marca ' + tipo + '?'))
	{
		var param = [];
		param['amccod']=tipo;
		ejecutasqld("marcaseliminar.php",param);
		location.reload();
	}
}


