<?php session_start();
include("../conectar.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="sinhead">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Ver Solicitud</title>
<link href="../<?php echo $css; ?>" rel="stylesheet" type="text/css" media="screen" />
<script language="JavaScript" src="../javascript/jquery.js"></script>
<script language="JavaScript" src="../javascript/javascript.js"></script>
<script language="JavaScript" src="javascript.js"></script>
<script language="JavaScript" src="../calendario/javascripts.js"></script>
</head>
<?php

	$sql = "SELECT T1.ACICOD, T1.ATLCOD, T3.ATLDES, T1.AUNCOD, T2.AUNDES, T1.ASONRO, T1.ASOFEC, T1.ASOOBS, T1.AUSCOD, T1.ASOSUP, T1.ASOSTS, T4.AAPVLA, T4.APACOD, T4.ASONRO
				FROM sl03fp T1, IS01FP T2, SL01FP T3, SL04FP T4
				WHERE T1.ACICOD = T2.ACICOD AND T1.ACICOD = T3.ACICOD AND 
					  T1.ATLCOD = T3.ATLCOD AND T1.AUNCOD = T2.AUNCOD AND 
					  T1.ACICOD = '$Compania' and T1.ATLCOD = '$atlcod' AND 
					  T1.AUNCOD = '$auncod' AND T1.ASONRO = 0$asonro AND
					  T4.ASONRO = 0$asonro AND T4.APACOD='1140'";

	$sqlden="SELECT AAPVLA FROM SL04FP WHERE ATLCOD='$atlcod' AND ASONRO=0$asonro AND APACOD='1141' AND ACICOD='$Compania'";
		
	$result = odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111")); 

	$resultden = odbc_exec($cid,$sqlden)or die(exit("Error en odbc_exec 11111")); 

// T1.ATLCOD, T3.ATLDES, T1.AUNCOD, T2.AUNDES, T1.ASONRO, 
// T1.ASOFEC, T1.ASOOBS, T1.AUSCOD, T1.ASOSUP, T1.ASOSTS
?>

<body class="sinbody" bgcolor="#FFFFFF" >
	<table class="tabla1">
		<tr>
			<td width="29%"  scope="col"><label>Tipo de Solicitud</label></td>
			<td width="32%" scope="col">
				<div align="left" id="aalcod"><strong><?php echo odbc_result($result, 'ATLDES');?></strong></div>
			</td>
		</tr>
        <tr>
			<td width="29%"  scope="col"><label>Numero de Solicitud</label></td>
			<td width="32%" scope="col">
				<div align="left" id="aalcod"><strong><?php echo odbc_result($result, 'ASONRO');?></strong></div>
			</td>
		</tr>
        <tr>
			<td width="29%"  scope="col"><label>Fecha de Solicitud</label></td>
			<td width="32%" scope="col">
				<div align="left" id="aalcod"><strong><?php echo odbc_result($result, 'ASOFEC');?></strong></div>
			</td>
		</tr>
        <tr>
			<td width="29%"  scope="col"><label>Servicio</label></td>
			<td width="32%" scope="col">
				<div align="left" id="aalcod"><strong><?php echo odbc_result($result, 'AUNDES');?></strong></div>
			</td>
		</tr>
        <tr>
			<td width="29%"  scope="col"><label>Elaborado </label></td>
			<td width="32%" scope="col">
				<div align="left" id="aalcod"><strong><?php echo odbc_result($result, 'AUSCOD');?></strong></div>
			</td>
		</tr>
        <tr>
			<td width="29%"  scope="col"><label>Autorizado </label></td>
			<td width="32%" scope="col">
				<div align="left" id="aalcod"><strong><?php echo odbc_result($result, 'ASOSUP');?></strong></div>
			</td>
		</tr>
		<tr>
			<td scope="col"><label>Descripci&oacute;n</label></td>
			<td scope="col">
				<div align="left" id="atrdes"><strong><?php echo odbc_result($result, 'ASOOBS');?></strong></div>
			</td>
		</tr>
		<?php 
			if(odbc_result($resultden, 'AAPVLA')!='') {
				?>
		<tr>
			<td scope="col"><label>Rechazada Por:</label></td>
			<td scope="col">
				<div align="left" id="atrdes"><strong><?php echo odbc_result($resultden, 'AAPVLA');?></strong></div>
			</td>
		</tr>
		<?php } ?>
		<tr>
			<td colspan="2">
				<table width="100%"  border="0" >
					<tr>
						<td width="100%" colspan="3" scope="col"><h3>Par&aacute;metros Adicionales: </h3>
						  <span class="header">
						  </span>
						</td>
					</tr>
                    <form id="agregarform" name="agregarform" method="post" class="form" action="">
					<tr>
						<td>
							<div id="parametros">
                            <?php 
								$sql ="SELECT T3.ACICOD, T4.ATLCOD, T2.APDDES, T2.APDCOD, T2.APDSTS, T2.APDLON, T2.APDLND, 
											T2.APDVAL, T1.ASBCOD, T2.APDDCR, T2.APDSEC, T3.AAPVLA, T2.APDTIP, T1.ASBDES 
										FROM SL02FP T4, MB10FP T1, MB03FP T2 
												LEFT JOIN SL04FP T3 ON (T3.ACICOD='$Compania' AND T2.APDCOD=T3.APACOD AND T3.ATLCOD='$atlcod' and t3.asonro='0$asonro') 
										WHERE T4.AMDCOD=T1.AMDCOD and T1.AMDCOD=T2.AMDCOD AND T4.AMDCOD='$modulo' AND T1.ATACOD='11' AND  T1.ATACOD=T2.ATACOD AND T1.ASBCOD=T2.ASBCOD 
										AND T4.ACICOD = '$Compania' AND T4.ATLCOD='$atlcod'  and t4.apdcod=t2.apdcod
										ORDER BY T1.ASBCOD, T2.APDSEC";
								//echo fparam_adi_add_ver_transaccion('agregarform','detalle',$sql,'1');
								echo fparam_adi_add_ver_usr_soli('agregarform','detalle',$sql, 1);
							?>
							</div>
						</td>
					</tr>
                    </form>
				 </table>
			</td>
		</tr>
        <tr>
			<td width="21%" scope="col" colspan="4"><div align="center"><input name="Submit3" type="button" onClick="window.close();"" " value="Cerrar"></div></td>
		</tr>
	</table>	
</body>
</html>