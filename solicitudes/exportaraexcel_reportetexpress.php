<?php session_start();
include("../conectar.php");
$arqnro = trim($_GET["num"]);
$tinva = unserialize( urldecode( $_GET['tinvan'] ) ) ;

//header("Pragma: ");
header("Pragma: no-cache");
header('Cache-control: ');
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Content-type: application/vnd.ms-excel");
//header("Content-type: application/octet-stream");
header("Content-disposition: attachment; filename=Reporte_Transporte_Express_".formatDate($Fechaactual,'dd.mm.aaa','aaaa_mm_dd').".xls");

?>
 

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Reporte Transporte Express</title>
</head>
<style>

h1, h2, h3, h4, h5 {
	margin: 0;
	padding: 0;
	font-weight: normal;
	color: #32639A;
}

h1 {
	font-size: 2em;
}

h2 {
	font-size: 2.4em;	
}

h3 {
	font-size: 1.6em;
	font-style: italic;
}
h4 {
	font-size: 1.6em;
	font-style: italic;
	color: #FFF;
}
h5 {
	font-size: 1.0em;
	font-style: italic;
	color: #666;
}

#background-image
{
	font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
	font-size: 10px;
	margin: 0px;
	width: 100%;
	text-align: left;
	border-collapse: collapse;
}
#background-image th
{
	padding: 12px;
	font-weight: normal;
	font-size: 12px;
	color: #339;
	border-bottom-style: solid;
	border-left-style: none;
	text-align: center;
}
#background-image td
{
	color: #669;
	border-top: 1px solid #fff;
	padding-right: 4px;
	padding-left: 4px;
}
#background-image tfoot td
{
	font-size: 9px;
}
#background-image tbody 
{

	background-repeat: no-repeat;
	background-position: left top;
}
#background-image tbody td
{
	background-image: url(images/backn.png);
}
* html #background-image tbody td
{
	/* 
	   ----------------------------
		PUT THIS ON IE6 ONLY STYLE 
		AS THE RULE INVALIDATES
		YOUR STYLESHEET
	   ----------------------------
	*/
	filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='images/backn.png',sizingMethod='crop');
	background: none;
}	
</style>
<body>
<?php 
				
				
		 		$wsolicitud=0;
				if ($solicitudpagina==0) 	
				{
						
						$z=0;
						$tothon=0;
						$totest=0;
						$lin=1;
						$limitep=$_SESSION['solicitudlineasporpaginat'];
						$pag=1;
						$primero='S';
						
						$sql ="SELECT T1.ACICOD, T1.ATLCOD, 
	 					 T3.ATLDES, T1.AUNCOD, 
						 T2.AUNDES, T1.ASONRO, 
						 T1.ASOFEC, T1.ASOOBS, 
						 T1.AUSCOD, T1.ASOSUP, 
						 T1.ASOSTS,
							(SELECT AAPVLA FROM sl04fp WHERE ACICOD=t1.acicod and ATLCOD=T1.ATLCOD and ASONRO=T1.ASONRO and APACOD='1128') as a1128,
							(SELECT AAPVLA FROM sl04fp WHERE ACICOD=t1.acicod and ATLCOD=T1.ATLCOD and ASONRO=T1.ASONRO and APACOD='1129') as a1129,
							(SELECT AAPVLA FROM sl04fp WHERE ACICOD=t1.acicod and ATLCOD=T1.ATLCOD and ASONRO=T1.ASONRO and APACOD='1130') as a1130,
							(SELECT T5.AUNDES FROM sl04fp t4 inner join is01fp t5 on (t4.acicod=t5.acicod and T4.AAPVLA= T5.AUNCOD ) WHERE t4.ACICOD=t1.acicod and t4.ATLCOD=T1.ATLCOD and t4.ASONRO=T1.ASONRO and t4.APACOD='1131' ) as a1131,
							(SELECT AAPVLA FROM sl04fp WHERE ACICOD=t1.acicod and ATLCOD=T1.ATLCOD and ASONRO=T1.ASONRO and APACOD='1132') as a1132,
							(SELECT AAPVLA FROM sl04fp WHERE ACICOD=t1.acicod and ATLCOD=T1.ATLCOD and ASONRO=T1.ASONRO and APACOD='1133') as a1133,
							(SELECT AAPVLA FROM sl04fp WHERE ACICOD=t1.acicod and ATLCOD=T1.ATLCOD and ASONRO=T1.ASONRO and APACOD='1134') as a1134,
							(SELECT AAPVLA FROM sl04fp WHERE ACICOD=t1.acicod and ATLCOD=T1.ATLCOD and ASONRO=T1.ASONRO and APACOD='1135') as a1135,
							(SELECT AAPVLA FROM sl04fp WHERE ACICOD=t1.acicod and ATLCOD=T1.ATLCOD and ASONRO=T1.ASONRO and APACOD='1136') as a1136,
							(SELECT AAPVLA FROM sl04fp WHERE ACICOD=t1.acicod and ATLCOD=T1.ATLCOD and ASONRO=T1.ASONRO and APACOD='1137') as a1137,
							(SELECT AAPVLA FROM sl04fp WHERE ACICOD=t1.acicod and ATLCOD=T1.ATLCOD and ASONRO=T1.ASONRO and APACOD='1138') as a1138,
							(SELECT AAPVLA FROM sl04fp WHERE ACICOD=t1.acicod and ATLCOD=T1.ATLCOD and ASONRO=T1.ASONRO and APACOD='1139') as a1139,
							(SELECT AAPVLA FROM sl04fp WHERE ACICOD=t1.acicod and ATLCOD=T1.ATLCOD and ASONRO=T1.ASONRO and APACOD='1140') as a1140
						FROM sl03fp T1 
							inner join IS01FP T2 on (T1.ACICOD = T2.ACICOD AND T1.AUNCOD = T2.AUNCOD)
							inner join SL01FP T3 on (T1.ACICOD = T3.ACICOD AND T1.ATLCOD = T3.ATLCOD)
						WHERE T1.ACICOD = '01' and T1.ATLCOD = '01' AND T1.ASOSTS IN ('03','04') AND T1.ASOFEC BETWEEN '$desde' AND '$hasta'
								ORDER BY T1.ASOFEC";
						
						//echo $sql."<br/><br/>";
						$resultt=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111"));
						
							while(odbc_fetch_row($resultt))
							{
								$jml = odbc_num_fields($resultt);
								$row[$z]["pagina"] =  $pag;
								for($i=1;$i<=$jml;$i++)
								{	
									$row[$z][odbc_field_name($resultt,$i)] =  odbc_result($resultt,$i);
								}
								$z++;
								if ($lin>=$limitep) 
								{
									$limitep+=$_SESSION['solicitudlineasporpaginat'];
									$pag++;
								}
								$lin++;
							}

						$totsol=($lin-1);
						$_SESSION['totalsolicitudes']=$totsol;
						$_SESSION['solicitudarreglo']=$row;
						$solicitudpagina=1;
						$_SESSION['solicitudpaginas']=$pag;
					}//fin de solicitudpagina
					/*se muestra la cantidad de elementos segun la solicitada en $solicitudpagina*/
					$paginat=$_SESSION['solicitudarreglo'];
			?>      
<table width="100%" border="0">
<tr>
<td height="89"><h1>
<?php if($Compania=='14'){?>
	<img src="http://<?php echo $Direccionip; ?>/idasysv3/images/logoidacadef2005.png" width="280" height="68" />
<?php }else if($Compania=='40'){?>
	<img src="http://<?php echo $Direccionip; ?>/idasysv3/images/MEDITRON_logo_rif.png" width="300" />
<?php }else{ ?>
	<img src="http://<?php echo $Direccionip; ?>/idasysv3/images/logomeditronnuevo.png" width="280" height="68" />
<?php } ?>
   </h1>
  <h5>RIF:  <?php echo $Companiarif; ?></h5></td>
</tr>
<tr>
<td>

<table width="100%" id="background-image" >
	
  <thead>
  	<tr>
        <th colspan="9" scope="col"><h1>Reporte de Servicio Transporte Express</h1></th>
    </tr>
    <tr>
        <th colspan="9" scope="col"><strong>Desde:</strong> <?php echo $desde;?> - <strong>Hasta:</strong> <?php echo $hasta; ?></th>
    </tr>
    <tr>
        <th colspan="9" scope="col">Elaborado el <?php echo $Fechaactual; ?> a las <?php echo $Horaactual2; ?></th>
    </tr>
    <tr>
        <th scope="col" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;solid;background-color:rgb(204,204,204);"><strong>Nro Solicitud</strong></th>
        <th scope="col" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;solid;background-color:rgb(204,204,204);"><strong>Fecha de Elaboraci&oacute;n</strong></th>
        <th scope="col" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;solid;background-color:rgb(204,204,204);"><strong>Nombre del Trabajador</strong></th>
        <th scope="col" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;solid;background-color:rgb(204,204,204);"><strong>Unidad Organizativa</strong></th>
        
        <th scope="col" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;solid;background-color:rgb(204,204,204);"><strong>Fecha de Servicio</strong></th>
        <th scope="col" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;solid;background-color:rgb(204,204,204);"><strong>Hora</strong></th>
        <th scope="col" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;solid;background-color:rgb(204,204,204);"><strong>Instituci&oacute;n a Visitar</strong></th>
        <th scope="col" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;solid;background-color:rgb(204,204,204);"><strong>Con Retorno</strong></th>
        <th scope="col" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;solid;background-color:rgb(204,204,204);"><strong>Tiempo de Espera</strong></th>
        <th scope="col" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;border-right:solid;solid;background-color:rgb(204,204,204);"><strong>Observaci&oacute;n</strong></th>
        <th scope="col" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;border-right:solid;solid;background-color:rgb(204,204,204);"><strong>STATUS</strong></th>
    </tr>
  </thead>
    				<tbody>
        						<?php
									//print_r($paginat);
									$pagact=$solicitudpagina;
									$part= 1;
									$color='';
									for($g=0; $g < (count($paginat)); $g++)
									{
											//Agrega letra a Parametro de Retorno
										if($paginat[$g]['A1138'] == 'S') {
											$paginat[$g]['A1138'] = $paginat[$g]['A1138'].'i';
										} elseif($paginat[$g]['A1138'] == 'N') {
											$paginat[$g]['A1138'] = $paginat[$g]['A1138'].'o';
										}

										$datetrans = new DateTime($paginat[$g]["A1135"]);
										$datesol = new DateTime($paginat[$g]["ASOFEC"]);

										//Status Solicitud
										if($paginat[$g]["ASOSTS"]=='03') {
											$status = "<strong>APROBADO</strong>";
										} elseif($paginat[$g]["ASOSTS"]=='04') {
											$status = "<strong>NO APROBADO</strong>";
									    }
										
									$html.='	 
                                        <td style="border-width:thin;border-bottom:solid;border-left:solid;'.$color.'">'.$paginat[$g]["ASONRO"].'</td>
                                        <td style="border-width:thin;border-bottom:solid;border-left:solid;'.$color.'">'.$paginat[$g]["ASOFEC"].'</td>
                                        
                                        <td style="border-width:thin;border-bottom:solid;border-left:solid;'.$color.'">'.$paginat[$g]["A1128"].'</td>
                                        <td style="border-width:thin;border-bottom:solid;border-left:solid;'.$color.'">'.$paginat[$g]["A1131"].'</td>
                                        <td style="border-width:thin;border-bottom:solid;border-left:solid;'.$color.'">'.$datetrans->format('d-m-Y').'</td>
                                        <td style="border-width:thin;border-bottom:solid;border-left:solid;'.$color.'">'.$paginat[$g]["A1136"].'</td>
                                        <td style="border-width:thin;border-bottom:solid;border-left:solid;'.$color.'">'.$paginat[$g]["A1134"].'</td>
                                        <td style="border-width:thin;border-bottom:solid;border-left:solid;'.$color.'">'.$paginat[$g]["A1138"].'</td>
                                        <td style="border-width:thin;border-bottom:solid;border-left:solid;'.$color.'">'.$paginat[$g]["A1139"].'</td>
                                        <td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;'.$color.'">'.$paginat[$g]["A1140"].'</td>
                                        <td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;'.$color.'">'.$status.'</td>
                                    </tr>
									';

									
									
									}
									echo $html;
									
								?>                                            
                  </tbody>
</table>
</td>
</tr>
</table>
</body>
</html>
