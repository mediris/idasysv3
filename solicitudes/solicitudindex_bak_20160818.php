<?php 
/*
 *jDavila
 *30/03/2012
 */
session_start();
include("../conectar.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
<title>Idasys V3</title>
<link href="../<?php echo $css; ?>" rel="stylesheet" type="text/css" media="screen" />
<script language="JavaScript" src="../javascript/javascript.js"></script>
<script language="JavaScript" src="javascript.js"></script>
<script language="JavaScript" src="../javascript/jquery.js"></script>
<link href="../shadowbox.css"  rel="stylesheet" type="text/css"/>
<script src="../shadowbox.js" type="text/javascript"/> </script>
<script language="JavaScript" type="text/JavaScript">
Shadowbox.init({overlayOpacity: "0.5"});
</script>
<style type="text/css" title="currentStyle">
			@import "../DataTables-1.9.4/media/css/<?php echo $demo_page;?>";
			@import "../DataTables-1.9.4/media/css/<?php echo $demo_table;?>";
</style>
<script type="text/javascript" language="javascript" src="../DataTables-1.9.4/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf-8">
			
			 $(document).ready(function() 
			 {
				  document.getElementById('info').style.display ="";
			      $('#info').dataTable( 
				  {
			        //inicio seccion de agrupacion
					"fnDrawCallback": function ( oSettings ) {
						if ( oSettings.aiDisplay.length == 0 )
						{
							return;
						}
				 
						var nTrs = $('#info tbody tr');
						var iColspan = nTrs[0].getElementsByTagName('td').length;
						var sLastGroup = "";
						for ( var i=0 ; i<nTrs.length ; i++ )
						{
							var iDisplayIndex = oSettings._iDisplayStart + i;
							var sGroup = oSettings.aoData[ oSettings.aiDisplay[iDisplayIndex] ]._aData[0];
							if ( sGroup != sLastGroup )
							{
								var nGroup = document.createElement( 'tr' );
								var nCell = document.createElement( 'td' );
								nCell.colSpan = iColspan;
								nCell.className = "ATLCOD";//el nombre de la comlumna (debe estar de primera..)
								nCell.innerHTML = sGroup;
								nGroup.appendChild( nCell );
								nTrs[i].parentNode.insertBefore( nGroup, nTrs[i] );
								sLastGroup = sGroup;
							}
						}
					},
					"aoColumnDefs": [
						{ "bVisible": false, "aTargets": [ 0 ] }
					],
					"aaSortingFixed": [[ 0, 'asc' ]],
					"aaSorting": [[ 1, 'asc' ]],
					"sDom": 'lfr<"giveHeight"t>ip',//fin seccion de agrupacion
					
					"bStateSave": true,
					"oLanguage": 
					{
			          "sLengthMenu": "Mostrar _MENU_ registros por Pag.",
						"sZeroRecords": "No Existen Registros",
						"sInfo": "Mostrando desde _START_ hasta _END_ de _TOTAL_ registros",
						"sInfoEmpty": "Mostrando desde 0 hasta 0 de 0 registros",
						"sInfoFiltered": "(filtrando de _MAX_ de registros)",
						"sSearch": "Buscar",
					  "oPaginate": 
					  {
			            "sNext": "Siguiente",
						"sPrevious": "Anterior"
			          },
					  
			        }
			      } );
			  } );
</script>
</head>
<body>
<div id="wrapper">
  <?php 
  		include("../superior.php");
  		?>
  <div id="page">
      <?php include("../validar.php");?>

<!--<div id="sidebar">
	    <ul>
	      <li>
	        <h2>Estad&iacute;sticas</h2>
	        <p>Aqu&iacute; se reflejar&aacute;n estad&iacute;sticas particulares seg&uacute;n en la opci&oacute;n del sistema que se encuentre</p>
          </li>
	      <li>
	        <h2>Categories</h2>
	        <ul>
	          <li><a href="#">Aliquam libero</a></li>
	          <li><a href="#">Consectetuer adipiscing elit</a></li>
	          <li><a href="#">Metus aliquam pellentesque</a></li>
	          <li><a href="#">Suspendisse iaculis mauris</a></li>
	          <li><a href="#">Urnanet non molestie semper</a></li>
	          <li><a href="#">Proin gravida orci porttitor</a></li>
            </ul>
          </li>
	      <li>
	        <h2>Blogroll</h2>
	        <ul>
	          <li><a href="#">Aliquam libero</a></li>
	          <li><a href="#">Consectetuer adipiscing elit</a></li>
	          <li><a href="#">Metus aliquam pellentesque</a></li>
	          <li><a href="#">Suspendisse iaculis mauris</a></li>
	          <li><a href="#">Urnanet non molestie semper</a></li>
	          <li><a href="#">Proin gravida orci porttitor</a></li>
            </ul>
          </li>
	      <li>
	        <h2>Archives</h2>
	        <ul>
	          <li><a href="#">Aliquam libero</a></li>
	          <li><a href="#">Consectetuer adipiscing elit</a></li>
	          <li><a href="#">Metus aliquam pellentesque</a></li>
	          <li><a href="#">Suspendisse iaculis mauris</a></li>
	          <li><a href="#">Urnanet non molestie semper</a></li>
	          <li><a href="#">Proin gravida orci porttitor</a></li>
            </ul>
          </li>
      </ul>
	    </div>-->
<!--	  <div id="content" style="background-image: url(fondoimagen.png);">-->
   	  
 		<?php 
				$sql="SELECT AUNCOD FROM IS09FP WHERE ACICOD='$Compania' and AUSCOD='$Usuario'";
				$resultec=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111"));
				//echo "<br><br><br><br><br><br>";
				$coddpto=trim(odbc_result($resultec,1));
				if($coddpto == ''){
					echo "<br><br><h1><center>Ud no est� asociado a alguna unidad solicitante</center></h1><br><center><img src='../images/alert.png' alt='Alerta' width='40' height='40' border='0'></center>";
					echo "<br><br><center>Por favor comuniquese con el administrador del sistema</center>";
					echo "<br><h3><center><a href=http://".$Direccionip."/idasysv3/index.php>REGRESAR</a></center></h3>";
					exit;
				}
				else
				{
					$sql="SELECT AUSCOD, AUSSUP FROM MB20FP WHERE AUSCOD='$Usuario' AND AUSSUP='S'";
					$result9=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111"));
					$super=trim(odbc_result($result9,2));
					if($super == 'S')
					{
						$auncod ='';
						$auncod = trim($_GET['auncod']);
						
						if(!empty($auncod))
						{	
							$sql="SELECT T1.ACICOD, T1.ATLCOD, T3.ATLDES, T1.AUNCOD, T2.AUNDES ,T1.ASONRO, T1.ASOFEC, T1.ASOOBS, T1.AUSCOD, T1.ASOSUP, T1.ASOSTS 
							FROM sl03fp t1, is01fp t2 , sl01fp t3 
							WHERE t1.acicod = t3.acicod and t1.atlcod=t3.atlcod and t1.acicod=t2.acicod and t1.auncod=t2.auncod and 
							t1.ACICOD='$Compania' AND T1.AUNCOD='$auncod' ORDER BY t1.ATLCOD desc, t1.AUNCOD";
						}else{
							$sql="SELECT T1.ACICOD, T1.ATLCOD, T3.ATLDES, T1.AUNCOD, T2.AUNDES ,T1.ASONRO, T1.ASOFEC, T1.ASOOBS, T1.AUSCOD, T1.ASOSUP, T1.ASOSTS 
							FROM sl03fp t1, is01fp t2 , sl01fp t3 
							WHERE t1.acicod = t3.acicod and t1.atlcod=t3.atlcod and t1.acicod=t2.acicod and t1.auncod=t2.auncod and 
							t1.ACICOD='$Compania' AND t1.AUNCOD='$coddpto' ORDER BY t1.ATLCOD desc, t1.AUNCOD";
						}
					}
					// colocar en despues de '$Compania'  AND t1.AUNCOD='$coddpto'
					else
					{
						$sql3="SELECT AAPVLA FROM IV43FP WHERE ACICOD ='$Compania' AND AUSCOD = '$Usuario' ";
						$result3=odbc_exec($cid,$sql3)or die(exit("Error en odbc_exec 11111"));
						$permiso = 0;
						$TEM =@ odbc_result($result3, 1);
						if(trim($TEM) == 'N') $permiso = 0;
						else $permiso = 1;
						if($permiso>0)
						{
							$sql="SELECT T1.ACICOD, T1.ATLCOD, T3.ATLDES, T1.AUNCOD, T2.AUNDES ,T1.ASONRO, T1.ASOFEC, T1.ASOOBS, T1.AUSCOD, T1.ASOSUP, T1.ASOSTS 
							FROM sl03fp t1, is01fp t2 , sl01fp t3 
							WHERE t1.acicod = t3.acicod and t1.atlcod=t3.atlcod and t1.acicod=t2.acicod and t1.auncod=t2.auncod and 
							t1.ACICOD='$Compania' AND t1.AUNCOD='$coddpto' ORDER BY t1.ATLCOD desc, t1.AUNCOD";
						}
						else
						{
							$sql="SELECT T1.ACICOD, T1.ATLCOD, T3.ATLDES, T1.AUNCOD, T2.AUNDES ,T1.ASONRO, T1.ASOFEC, T1.ASOOBS, T1.AUSCOD, T1.ASOSUP, T1.ASOSTS 
							FROM sl03fp t1, is01fp t2 , sl01fp t3 
							WHERE t1.acicod = t3.acicod and t1.atlcod=t3.atlcod and t1.acicod=t2.acicod and t1.auncod=t2.auncod and 
							t1.ACICOD='$Compania' AND T1.AUSCOD='$Usuario' ORDER BY t1.ATLCOD desc, t1.AUNCOD";
						}
					}
				}	
				//echo $sql;
				$resultt=@odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111"));
				$z=0;
				$lin=1;
				$limitep=$_SESSION['solicitudlineasporpaginat'];
				$pag=1;
				$primero='S';
				
				while(odbc_fetch_row($resultt))
				{
					$jml = odbc_num_fields($resultt);
					$row[$z]["pagina"] =  $pag;
					for($i=1;$i<=$jml;$i++)
					{
						$row[$z][odbc_field_name($resultt,$i)] =  odbc_result($resultt,$i);
					}
					$z++;
					if ($lin>=$limitep) 
					{
						$limitep+=$_SESSION['solicitudlineasporpaginat'];
						$pag++;
					}
					$lin++;
				}
				/*guarla informacion en un arreglo*/
				$_SESSION['solicitudarreglo']=$row;
				
				/*permite mostrar pag seleccionada*/
				if ($solicitudpagina==0) 	
				{
					$totsol=($lin-1);
					$_SESSION['totalsolicitudes']=$totsol;
					$solicitudpagina=1;
					$_SESSION['solicitudpaginas']=$pag;
				}
				
				
			?>      
		<div id="content3" >
        	<table width="100%"   border="0">
              <tr>
                <td width="84%" scope="col"><h1 align="center" class="title">Solicitudes:</h1> 
                    <div align="center"><h3><form>Unidad Solicitante:     
				<?PHP 
					$sqlsup = "SELECT AUSCIA, AUSCOD, AUSSUP, AUSUSR FROM mb20fp WHERE AUSCOD ='".$Usuario."' AND AUSCIA='".$Compania."'";
					$resultsup =@odbc_exec($cid,$sqlsup)or die(exit("Error en odbc_exec 11111"));
					$supervisor = odbc_result($resultsup, 'AUSSUP');
                    if($supervisor =='S') 
                    {
						?>
                        
                        <select name="auncod" id="auncod" onchange="submit();">
                        <?php 
						$sql="SELECT AUNCOD, AUNDES FROM IS01FP WHERE ACICOD='$Compania' order by aundes";
                        $result1=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111")); 
                        $select = '';
                        while(odbc_fetch_row($result1)){

                            $cod=trim(odbc_result($result1,1));
                            $des=trim(odbc_result($result1,2));
                            if(!empty($auncod))
                            {
                                if($cod == $auncod){
                                    $select = ' selected="selected" ';
                                }
                                else{
                                    $select = '';
                                }
                            }
							else
							{
								if($cod == $coddpto){
                                    $select = ' selected="selected" ';
                                }
                                else{
                                    $select = '';
                                }
							}
                        ?>
                            <option value= "<?php echo $cod; ?>" <?php echo $select?> ><?php echo $des." ( ".$cod." )"; ?></option>
                        <?php } ?>
                    </select>
                    <?php 	
                    }
                    else
                    {
                        $sql="SELECT AUNDES FROM IS01FP WHERE ACICOD='$Compania' AND AUNCOD='$coddpto'";
						$result20=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111"));
						echo $descrp=trim(odbc_result($result20,1));
                    }
                ?>
                </form></h3></div><hr />
                    </td>
                <td width="16%" scope="col"><div align="left">
                  <table width="100%"  border="0">
                    <tr>
                      <th width="36%" scope="col">
                   		<?php 
							if(!empty($auncod))
							{
						?>
	                          <th width="36%" scope="col"><a rel="shadowbox;width=800;height=500" title="Agregar Solicitud" href="solicitudfagregar.php?&tipo=<?php echo trim($auncod);?>"><img src="../images/agregar.gif" title="Agregar" width="29" height="30" border="0" /></a></th>
                        <?php 
							}else if(!empty($coddpto)){
						?>
    	                   	  <th width="36%" scope="col"><a rel="shadowbox;width=800;height=500" title="Agregar Solicitud" href="solicitudfagregar.php?&tipo=<?php echo trim($coddpto);?>"><img src="../images/agregar.gif" title="Agregar" width="29" height="30" border="0" /></a></th>
                        <?php 
							}
						?>
                      </th>
                      <th width="30%" scope="col"><img src="../images/excel.jpg" alt="" width="25" height="25" /></th>
                      <th width="16%" scope="col"><img src="../images/pdf.jpg" alt="" width="25" height="25" /></th>
                      <th width="18%" scope="col"><img src="../images/impresora.gif" alt="" width="25" height="25" /></th>
                    </tr>
                  </table>
                </div></td>
              </tr>
            </table>
         <div id="container">
            <div id="demo">
            <table width="100%" id="info" style="display:none">
                
                    <thead>
                        <tr>
                        	<th >ATLCOD</th>
                            <th >N� Solicitud</th>
                            <th >Uni. Solicitante</th>
                            <th >Fecha</th>
                            <th >Observaci�n</th>
                            <th >Usuario</th>
                            <th >Supervisor</th>
                            <th >Status</th>
                            <th >Opciones</th>
                        </tr> 
                    </thead>
                    <tbody >
                        <?php 
                            $astfld="";
                            /*se muestra la cantidad de elementos segun la solicitada en $solicitudpagina*/
                            $paginat=$_SESSION['solicitudarreglo'];
                            
                            $pagact=$solicitudpagina;
                            for($g=0; $g < (count($paginat)); $g++)
                            {                                
                        ?>
                                <tr>
                                	<td ><div ><strong><?php echo $paginat[$g]["ATLDES"];?></strong></div></td>
                                    <td ><div align="center"><strong><?php echo $paginat[$g]["ASONRO"];?></strong></div></td>
                                    <td ><div align="center"><strong><?php echo $paginat[$g]["AUNCOD"];?></strong></div></td>                
                                    <td ><div align="left"><?php echo $paginat[$g]["ASOFEC"];?></div></td>
                                    <td ><div align="left"><?php echo wordwrap($paginat[$g]["ASOOBS"],50,"<br />",true);?></div></td>
                                    <td ><div align="center"><?php echo $paginat[$g]["AUSCOD"]."-".$paginat[$g]["ATRHOR"];?></div></td>
                                    <td ><div align="center"><?php echo $paginat[$g]["ASOSUP"];?></div></td>                
                                    <td ><div align="center"><?php echo status('ASOSTS',$paginat[$g]["ASOSTS"]);//."**".$paginat[$g]["ASOSTS"]."**" ?></div></td>
    
                                    <?php /* seccion de opciones */ ?>
                                    <td >
                                     <ul id="opciones">
                                    <?php 	
											//echo "*".$paginat[$g]["AUSCOD"]."*-*".$Usuario."*<br />";
											
											if( trim($paginat[$g]["AUSCOD"])==trim($Usuario) || $paginat[$g]["ASOSUP"]==$paginat[$g]["AUSCOD"])
											{
									?>
                                               		<?php if($paginat[$g]["ASOSTS"]=='01'){?>
                                                    	<li><a title="Editar Solicitud" rel="shadowbox;width=850;height=450"  href="solicitudfeditar.php?atlcod=<?php echo trim($paginat[$g]["ATLCOD"]);?>&num=<?php echo trim($paginat[$g]["ASONRO"]);?>&uni=<?php echo trim($paginat[$g]["AUNCOD"]); ?>"><img src="../images/editar.gif" alt="Editar" width="15" height="15" border="0" /></a></li>
	                                                    <li><a title="Eliminar" href="javascript:eliminar('<?php echo trim($paginat[$g]["ATLCOD"]);?>','<?php echo trim($paginat[$g]["ASONRO"]);?>','<?php echo trim($paginat[$g]["AUNCOD"]);?>')"><img src="../images/eliminar.gif" alt="Eliminar" width="15" height="15" border="0" /></a></li>
                                                    <?php } ?>
                                                    <?php if($paginat[$g]["ASOSTS"]=='01'){?>
                                                        <!--<li><a title="Confirmar Solicitud" rel="shadowbox;width=650;height=345"  href="confirmar.php?tipo=<?php echo trim($paginat[$g]["ATLCOD"]);?>&num=<?php echo trim($paginat[$g]["ATRNUM"]);?>&tipo=<?php echo trim($paginat[$g]["AALCOD"]); ?>"><img src="../images/check.gif" alt="Editar" width="15" height="15" border="0" /></a></li>-->
                                                        <li><a href="javascript:enviarsolicitud('<?php echo trim($paginat[$g]["ATLCOD"]);?>','<?php echo trim($paginat[$g]["ASONRO"]);?>','<?php echo trim($paginat[$g]["AUNCOD"]);?>')"><img src="../images/enviaremail.png" title="Enviar a aprobar" width="36" height="28" border="0" /></a></li>
                                                   <?php }
												   		 if($paginat[$g]["ASOSTS"]=='03' || $paginat[$g]["ASOSTS"]=='04'){
															 ?>
														 <li><a href="javascript:solicitudimprimir('<?php echo trim($paginat[$g]["ATLCOD"]);?>','<?php echo trim($paginat[$g]["ASONRO"]);?>','<?php echo trim($paginat[$g]["AUNCOD"]); ?>')"><img src="../images/impresora.gif" title="Imprimir" width="31" height="28" border="0"></a></li>
                                                         <?php
														 }
											} 
													?>
                                    	<li><a href="javascript:solicitudver('<?php echo trim($paginat[$g]["ATLCOD"]);?>','<?php echo trim($paginat[$g]["ASONRO"]);?>','<?php echo trim($paginat[$g]["AUNCOD"]); ?>')"><img src="../images/ver.png" title="Ver Detalle" width="31" height="28" border="0"></a></li>
                                    </ul>
                                    </td>             
                                </tr>
                        <?php } ?> 
                    </tbody>
                </table>
            </div>
	  </div>
	</div>
		<!-- end #content -->
		<!-- end #sidebar -->
	<div style="clear: both;">&nbsp;</div>
  </div>
	<!-- end #page -->
</div>
	<div id="footer">
		<div id="opcionesmasusadas"><?php include("../opcionesmasusadasphp.php");?></div>
	</div>
	<!-- end #footer -->
</body>
</html>
