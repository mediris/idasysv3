<?php 
/*
 * jDavila
 * 23/02/2015
 */
session_start();
include("../conectar.php");
$tinva = $_GET['tinvan'];
//print_r($tinva);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
<title>Idasys V3</title>
<link href="../<?php echo $css; ?>" rel="stylesheet" type="text/css" media="screen" />
<script language="JavaScript" src="../javascript/javascript.js"></script>
<script language="JavaScript" src="../calendario/javascripts.js"></script>
<script language="JavaScript" src="javascript.js"></script>
<script language="JavaScript" src="../javascript/jquery.js"></script>
<link href="../shadowbox.css"  rel="stylesheet" type="text/css"/>
<script src="../shadowbox.js" type="text/javascript"/> </script>
<script language="JavaScript" type="text/JavaScript">
Shadowbox.init({overlayOpacity: "0.5"});
</script>
<style type="text/css" title="currentStyle">
			@import "../DataTables-1.9.4/media/css/<?php echo $demo_page;?>";
			@import "../DataTables-1.9.4/media/css/<?php echo $demo_table;?>";
</style>
<script type="text/javascript" language="javascript" src="../DataTables-1.9.4/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf-8">
		
			
			
			 $(document).ready(function() 
			 {
			      document.getElementById('info').style.display="";

				 $('#info').dataTable( 
				  {
					"bStateSave": true,
			        "oLanguage": 
					{
			          	"sLengthMenu": "Mostrar _MENU_ registros por Pag.",
						"sZeroRecords": "No Existen Registros",
						"sInfo": "Mostrando desde _START_ hasta _END_ de _TOTAL_ registros",
						"sInfoEmpty": "Mostrando desde 0 hasta 0 de 0 registros",
						"sInfoFiltered": "(filtrando de _MAX_ de registros)",
						"sSearch": "Buscar",
						"oPaginate": 
						{
							"sNext": "Siguiente",
							"sPrevious": "Anterior"
						}
			        }
			      } );
			  } );
</script>

</head> 
<body background="../images/fondo idaca.jpg" >
<div id="wrapper">
  <?php include("../superior.php");?>
  <div id="page">
     <?php  include("../validar.php");?>
 		<?php 
				
				if ($bandera==1) 
				{

						$sql ="SELECT T1.ACICOD, T1.ATLCOD, 
	 					 T3.ATLDES, T1.AUNCOD, 
						 T2.AUNDES, T1.ASONRO, 
						 T1.ASOFEC, T1.ASOOBS, 
						 T1.AUSCOD, T1.ASOSUP, 
						 T1.ASOSTS,
							(SELECT AAPVLA FROM sl04fp WHERE ACICOD=t1.acicod and ATLCOD=T1.ATLCOD and ASONRO=T1.ASONRO and APACOD='1128') as a1128,
							(SELECT AAPVLA FROM sl04fp WHERE ACICOD=t1.acicod and ATLCOD=T1.ATLCOD and ASONRO=T1.ASONRO and APACOD='1129') as a1129,
							(SELECT AAPVLA FROM sl04fp WHERE ACICOD=t1.acicod and ATLCOD=T1.ATLCOD and ASONRO=T1.ASONRO and APACOD='1130') as a1130,
							(SELECT T5.AUNDES FROM sl04fp t4 inner join is01fp t5 on (t4.acicod=t5.acicod and T4.AAPVLA= T5.AUNCOD ) WHERE t4.ACICOD=t1.acicod and t4.ATLCOD=T1.ATLCOD and t4.ASONRO=T1.ASONRO and t4.APACOD='1131' ) as a1131,
							(SELECT AAPVLA FROM sl04fp WHERE ACICOD=t1.acicod and ATLCOD=T1.ATLCOD and ASONRO=T1.ASONRO and APACOD='1132') as a1132,
							(SELECT AAPVLA FROM sl04fp WHERE ACICOD=t1.acicod and ATLCOD=T1.ATLCOD and ASONRO=T1.ASONRO and APACOD='1133') as a1133,
							(SELECT AAPVLA FROM sl04fp WHERE ACICOD=t1.acicod and ATLCOD=T1.ATLCOD and ASONRO=T1.ASONRO and APACOD='1134') as a1134,
							(SELECT AAPVLA FROM sl04fp WHERE ACICOD=t1.acicod and ATLCOD=T1.ATLCOD and ASONRO=T1.ASONRO and APACOD='1135') as a1135,
							(SELECT AAPVLA FROM sl04fp WHERE ACICOD=t1.acicod and ATLCOD=T1.ATLCOD and ASONRO=T1.ASONRO and APACOD='1136') as a1136,
							(SELECT AAPVLA FROM sl04fp WHERE ACICOD=t1.acicod and ATLCOD=T1.ATLCOD and ASONRO=T1.ASONRO and APACOD='1137') as a1137,
							(SELECT AAPVLA FROM sl04fp WHERE ACICOD=t1.acicod and ATLCOD=T1.ATLCOD and ASONRO=T1.ASONRO and APACOD='1138') as a1138,
							(SELECT AAPVLA FROM sl04fp WHERE ACICOD=t1.acicod and ATLCOD=T1.ATLCOD and ASONRO=T1.ASONRO and APACOD='1139') as a1139,
							(SELECT AAPVLA FROM sl04fp WHERE ACICOD=t1.acicod and ATLCOD=T1.ATLCOD and ASONRO=T1.ASONRO and APACOD='1140') as a1140
						FROM sl03fp T1 
							inner join IS01FP T2 on (T1.ACICOD = T2.ACICOD AND T1.AUNCOD = T2.AUNCOD)
							inner join SL01FP T3 on (T1.ACICOD = T3.ACICOD AND T1.ATLCOD = T3.ATLCOD)
						WHERE T1.ACICOD = '01' and T1.ATLCOD = '01' AND T1.ASOSTS IN ('03','04') AND T1.ASOFEC BETWEEN '$desde' AND '$hasta'
								ORDER BY T1.ASOFEC";
						
						$resultt=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111"));
						
						$z=0;
						
						while(odbc_fetch_row($resultt))
						{
							
							$jml = odbc_num_fields($resultt);
							for($i=1;$i<=$jml;$i++)
							{	

								$row[$z][odbc_field_name($resultt,$i)] =  odbc_result($resultt,$i);
							}
							$z++;
						}
						$_SESSION['solicitudarreglo']=$row;
						$paginat=$_SESSION['solicitudarreglo'];
				}
				
			?>
        <div id="content2" >   
        	<table width="100%"   border="0">
              <tr>
                <td width="65%" scope="col" colspan="2"><h1  class="title">Reportes de Transporte Express</h1><hr /></td>
                <td width="13%" scope="col"><div align="left">
                  <table width="100%"  border="0">
                    <tr>
                      <th width="30%" scope="col" >
                      <div class="flechas">
                      	<a href="exportaraexcel_reportetexpress.php?&desde=<?php echo $desde;?>&hasta=<? echo $hasta; ?>" target="_blank">
                        	<img src="../images/excel.jpg" alt="" width="30" height="30" title="Exportar a Excel Detalle"/>
                        </a>
                      </div></th>
                    </tr>
                  </table>
                </div></td>
              </tr>
              <tr>
              	<td colspan="3">
                	<form name="form" id="form" method="post" on>
                    <input type="hidden" name="bandera" id="bandera" value="<?php echo $bandera;?>" />
                	<table>
                    	<tr>
                        	<td>&nbsp;Desde:</td>
                            <td>&nbsp;<?php echo  escribe_formulario_fecha_vacio('desde','form',$desde); ?></td>
                            <td>&nbsp;</td>
                            <td>&nbsp;Hasta:</td>
                            <td>&nbsp;<?php echo  escribe_formulario_fecha_vacio('hasta','form',$hasta); ?></td>
                            <td rowspan="3"><a href="javascript:busqueda2();"><img src="../images/precesarconsulta.png" width="50" height="50" alt="Realizar Consulta" style="border:none;"></a></td>
                        </tr>
                    </table>
                    </form>
            </table>   
		<div id="container">
            <div id="demo">
                <table width="100%" id="info"  border="0" style="display:none;">
                  <thead>
                    <tr>
                    	<th scope="col" colspan="11" style="background-color:rgb(204,204,204)">Reportes de Transporte Express</th>
                    <tr>
                        <th scope="col" style="background-color:rgb(204,204,204)"><strong>Nro Solicitud</strong></th>
                        <th scope="col" style="background-color:rgb(204,204,204)"><strong>Fecha de Elaboraci&oacute;n</strong></th>
                        <th scope="col" style="background-color:rgb(204,204,204)"><strong>Nombre del Trabajador</strong></th>
                        <th scope="col" style="background-color:rgb(204,204,204)"><strong>Unidad Organizativa</strong></th>
                        <th scope="col" style="background-color:rgb(204,204,204)"><strong>Fecha de Servicio</strong></th>
                        <th scope="col" style="background-color:rgb(204,204,204)"><strong>Hora</strong></th>
                        <th scope="col" style="background-color:rgb(204,204,204)"><strong>Instituci&oacute;n a Visitar</strong></th>
                        <th scope="col" style="background-color:rgb(204,204,204)"><strong>Con Retorno</strong></th>
                        <th scope="col" style="background-color:rgb(204,204,204)"><strong>Tiempo de Espera</strong></th>
                        <th scope="col" style="background-color:rgb(204,204,204)"><strong>Observaci&oacute;n</strong></th>
                        <th scope="col" style="background-color:rgb(204,204,204)"><strong>STATUS</strong></th>
                    </tr>
                 </thead> 
                 <tbody>
        						<?php
									//print_r($paginat);
									$pagact=$solicitudpagina;
									$part= 1;

									if(count($paginat)>0){
										for($g=0; $g < (count($paginat)); $g++)
										{

											//Agrega letra a Parametro de Retorno
										if($paginat[$g]['A1138'] == 'S') {
											$paginat[$g]['A1138'] = $paginat[$g]['A1138'].'i';
										} elseif($paginat[$g]['A1138'] == 'N') {
											$paginat[$g]['A1138'] = $paginat[$g]['A1138'].'o';
										}

										$datetrans = new DateTime($paginat[$g]["A1135"]);
										$datesol = new DateTime($paginat[$g]["ASOFEC"]);				
								?>
                                            	<tr >
	                                                <td style="text-align:center;vertical-align:middle;" rowspan="<?php //echo $numRows; ?>"><?php echo $paginat[$g]["ASONRO"]; ?></td>
                                                    <td style="text-align:center;vertical-align:middle;" rowspan="<?php //echo $numRows; ?>"><?php echo $datesol->format('d-m-Y'); ?></td>
                                                    
                                                    
                                                    <td style="text-align:center;vertical-align:middle;"><?php echo $paginat[$g]["A1128"]; ?></td>
                                                    <td style="text-align:center;vertical-align:middle;"><?php echo $paginat[$g]["A1131"]; ?></td>
                                                    <td style="text-align:left;vertical-align:middle;" rowspan="<?php //echo $numRows; ?>"><?php echo $datetrans->format('d-m-Y'); ?></td>
                                                    <td style="text-align:center;vertical-align:middle;"><?php echo trim($paginat[$g]["A1136"]); ?></td>
                                                    
                                                    <td style="text-align:center;vertical-align:middle;"><?php echo $paginat[$g]["A1134"]; ?></td>
                                                    <td style="text-align:center;vertical-align:middle;"><?php echo $paginat[$g]["A1138"];  ?></td>
                                                    <td style="text-align:center;vertical-align:middle;"><?php echo $paginat[$g]["A1139"]; ?></td>
                                                    <td style="text-align:center;vertical-align:middle;"><?php echo $paginat[$g]["A1140"]; ?></td>
                                                    <td style="text-align:center;vertical-align:middle;"><?php 
                                                    if($paginat[$g]["ASOSTS"]=='03'){
                                                    	echo "<strong>APROBADO</strong>";
                                                    } elseif($paginat[$g]["ASOSTS"]=='04'){
                                                    	echo "<strong>NO APROBADO</strong>";
                                                    } ?></td>
                                                </tr>
								<?php 		
										}
									}else{
								?>  
                                				<tr >
	                                                <td style="text-align:left;vertical-align:middle;" rowspan="<?php //echo $numRows; ?>">--</td>
                                                    <td style="text-align:left;vertical-align:middle;" rowspan="<?php //echo $numRows; ?>">--</td>
                                                    
                                                    
                                                    <td style="text-align:center;vertical-align:middle;">--</td>
                                                    <td style="text-align:center;vertical-align:middle;">--</td>
                                                    <td style="text-align:center;vertical-align:middle;">--</td>
                                                    
                                                    <td style="text-align:center;vertical-align:middle;">--</td>
                                                    <td style="text-align:center;vertical-align:middle;">--</td>
                                                    <td style="text-align:center;vertical-align:middle;">--</td>
                                                    <td style="text-align:center;vertical-align:middle;">--</td>
                                                    <td style="text-align:center;vertical-align:middle;">--</td>
                                                    <td style="text-align:center;vertical-align:middle;">--</td>
                                                </tr>
                                <?php 
									}
								?>   
                       </tbody>                                         
                </table>
</div>
		  </div>
	  </div>
	</div>
		<!-- end #content -->
		<!-- end #sidebar -->
		<div style="clear: both;">&nbsp;</div>
	</div>
	<!-- end #page -->

	<div id="footer">
		<div id="opcionesmasusadas"><?php include("../opcionesmasusadasphp.php"); ?></div>
	</div>
 	<?php 
		echo "<script>
				var list = [];
				var pos = 1;
				var lista = \"".$tipoInve."\";
				$('input[name=\"tinvan[]\"]').each(function() { 
					//alert($(this).val() );
					if( lista.indexOf($(this).val()) != -1 ){
						//alert('paso '+$(this).val()) ;
						 $(this).attr('checked', true); 
					}
					pos++;
				});
			  </script>";
	?>
	<!-- end #footer -->
</body>
</html>
