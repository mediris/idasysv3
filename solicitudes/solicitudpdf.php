<?php 
/*
23-08-16
Luis Ramos
*/
session_start();
include("../conectar.php");
require_once('../tcpdf/config/lang/eng.php');
require_once('../tcpdf/tcpdf.php');

$numsol = $_GET['num']; //numero de solicitud
$uni = $_GET['uni']; //unidad solicitante
$atlcod = $_GET['atlcod']; // tipo de solicitud
if($Compania == '01' && $atlcod == '01' ){
	include("html_trans_express.php");
}else{
	include("html_generico.php");
}
//define ('K_PATH_MAIN', '/images/');

// Extend the TCPDF class to create custom Header and Footer
class MYPDF extends TCPDF {
    // Page footer
    //Variable para obtener footer
    public $header_temp;
    public $footer_temp;

    //Metodos utilizado para insertar y obtener la variable
    public function setHeaderTemp($h_val) {
    	$this->header_temp = $h_val;
    }

    public function getHeaderTemp() {
    	return $this->header_temp;
    }

    public function setFooterTemp($f_val) {
    	$this->footer_temp = $f_val;
    }

    public function getFooterTemp() {
    	return $this->footer_temp;
    }

    public function Header() {
        // Logo
         //$image_file = '../images/'.'logomeditronnuevo.png';
        //$this->Image($image_file, 10, 10, 50, '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
        // Set font
        $this->SetFont('helvetica', '', 10);
        // Title
        //$this->Cell(111, 25, 'asd', 0, false, 'C', 0, '', 0, false, 'M', 'M');
        //$this->Cell(111,25,'ALGÚN TÍTULO DE ALGÚN LUGAR',0,0,'C',20,12,20);

        $this->writeHTMLCell(0, 0, '', '', $this->header_temp, 0, 0, false, "L", true);
    }

    public function Footer() {
		$this->setY(-45);
		//Se asigna la variable temporal al metodo Footer
		$this->writeHTMLCell(0, 0, '', '', $this->footer_temp,/*.'<br>'.'Página '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(),*/ 0, 0, false, "L", true);
        //$this->Cell(0, 10, $html, 0, false, 'C', 0, '', 0, false, 'T', 'M');
		// Page number
        //$this->Cell(0, 10, 'Page '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
    }
}

// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, false, 'ISO-8859-1', false);

//Luego de crear la instancia, se llama el metodo setTemporal y se llama la variable
$pdf->setHeaderTemp($headerpdf);
$pdf->setFooterTemp($footerpdf);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('IDASYS WEB');
$pdf->SetTitle('Solicitud '.$nrosol.'');
$pdf->SetSubject('Solicitud '.$nrosol.'');
//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, 37, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(15);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

//$pdf->SetHeaderData($logo, PDF_HEADER_LOGO_WIDTH, $aundes, 'Tipo de Solicitud: '.$atldes.' - Nro. '.$asonro);		

// add a page
$pdf->AddPage();
// set font
$pdf->SetFont('helvetica', '', 10);

$pdf->writeHTML($html, true, false, true, false, '');
$pdf->Output('Solicitud_Nro_'.$nrosol.'.pdf', 'I');
	
?>