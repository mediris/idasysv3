<?php 
session_start();
include("../conectar.php");
require_once('../tcpdf/config/lang/eng.php');
require_once('../tcpdf/tcpdf.php');

include("html_generico.php");

// Extend the TCPDF class to create custom Header and Footer
class MYPDF extends TCPDF {
    // Page footer
    //Variable para obtener footer
    public $temporal;

    //Metodos utilizado para insertar y obtener la variable
    public function setTemporal($val) {
        $this->temporal = $val;
    }

    public function getTemporal() {
        return $this->temporal;
    }

    public function Footer() {
        $this->setY(-35);
        //Se asigna la variable temporal al metodo Footer
        $this->writeHTMLCell(0, 0, '', '', $this->temporal.'<br>'.'Página '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, 0, false, "L", true);
        //$this->Cell(0, 10, $html, 0, false, 'C', 0, '', 0, false, 'T', 'M');
        // Page number
        //$this->Cell(0, 10, 'Page '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
    }
}

// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

//Luego de crear la instancia, se llama el metodo setTemporal y se llama la variable
$pdf->setTemporal($footerpdf);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Johan Davila');
$pdf->SetTitle('Solicitud '.$arqnro.'');
$pdf->SetSubject('Solicitud '.$arqnro.'');
//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

$pdf->SetHeaderData($logo, PDF_HEADER_LOGO_WIDTH, $aundes, 'Tipo de Solicitud: '.$atldes.' - Nro. '.$asonro);       

// add a page
$pdf->AddPage();
// set font
$pdf->SetFont('helvetica', '', 10);

$pdf->writeHTML($html, true, false, false, false, '');
$pdf->Output('Despacho_Pedido_'.$arqnro.'.pdf', 'I');
    
?>