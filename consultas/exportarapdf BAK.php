<?php 
session_start();

include("../conectar.php");
/*header("Pragma: ");
header('Cache-control: ');
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Content-type: application/vnd.ms-excel");
header("Content-disposition: attachment; filename=listadeestudios.xls");*/
$html = '
<page>
<html><head>
<title>Listado de Estudios</title>
<style>

h1, h2, h3, h4, h5 {
	margin: 0;
	padding: 0;
	font-weight: normal;
	color: #32639A;
}

h1 {
	font-size: 2em;
}

h2 {
	font-size: 2.4em;	
}

h3 {
	font-size: 1.6em;
	font-style: italic;
}
h4 {
	font-size: 1.6em;
	font-style: italic;
	color: #FFF;
}
h5 {
	font-size: 1.0em;
	font-style: italic;
	color: #666;
}

#background-image
{
	/*font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;*/
	font-size: 8px;
	margin: 0px;
	width: 100%;
	text-align: left;
	border-collapse: collapse;
}
#background-image th
{
	padding: 12px;
	font-weight: normal;
	font-size: 9px;
	color: #339;
	border-bottom-style: solid;
	border-left-style: none;
	text-align: center;
}
#background-image td
{
	color: #669;
	border-top: 1px solid #fff;
	padding-right: 4px;
	padding-left: 4px;
}
#background-image tfoot td
{
	font-size: 8px;
}
#background-image tbody 
{

	background-repeat: no-repeat;
	background-position: left top;
}
#background-image tbody td
{
	background-image: url(images/backn.png);
}
* html #background-image tbody td
{
	/* 
	   ----------------------------
		PUT THIS ON IE6 ONLY STYLE 
		AS THE RULE INVALIDATES
		YOUR STYLESHEET
	   ----------------------------
	*/
	filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src=\'images/backn.png\',sizingMethod=\'crop\');
	background: none;
}	
</style>
</head><body>';
				
				if (!$_SESSION['fecsel']) {	$_SESSION['fecsel']=$Fechaactual;}
				$fecsel=$_SESSION['fecsel'];
		 		$wsolicitud=0;
				if ($solicitudpagina==0) 	
				{
							$sql="SELECT T2.Addlof, T2.ALCSED,T2.ADDFEF, T2.ACCTRA,T2.ADDNRF,T2.ADDNRR, T1.ARGCOD, T3.AESDES ,T1.ARGTTE, T2.ADDPNM,T2.ADDPNI,T1.ARGTTM, T2.ACOPTP, T2.APCDES, T2.ACOPTR, T2.ACODDG, T2.ADDNRP FROM id72fl94 t2,id76fp t1, id19fp t3 WHERE T1.ACCCIA= T2.ACCCIA and T1.ACCTRA=T2.ACCTRA and T1.ACCTIP= T2.ACCTIP and T1.ACCNRO= T2.ACCNRO and T1.ACCGRC= T2.ACCGRC and T1.ACCCLI= T2.ACCCLI and T1.ADDNRP=  T2.ADDNRP and T1.ADDTPF= T2.ADDTPF and T1.ADDNRF= T2.ADDNRF and T1.ADDNEF= T2.ADDNEF and T1.ADDLOF= t2.ADDLOF and T1.ACCCIA=  T3.ACOCOD and T1.ARGCOD = T3.AESCOD and t2.addsbs='F' and t1.acccia='14' and t1.addlof='11' and t2.addsts>'01' and t2.addfef= '$fecsel'";
							
							$resultt=odbc_exec($cidv2,$sql)or die(exit("Error en odbc_exec 11111"));
							$z=0;
							$lin=1;
							$limitep=1000000;
							$pag=1;
							$primero='S';
							
  							while(odbc_fetch_row($resultt))
							{
								$jml = odbc_num_fields($resultt);
								$row[$z]["pagina"] =  $pag;
   			 					for($i=1;$i<=$jml;$i++)
        						{
									$row[$z][odbc_field_name($resultt,$i)] =  odbc_result($resultt,$i);
								}
								$z++;
								if ($lin>=$limitep) 
								{
									$limitep+=$_SESSION['solicitudlineasporpaginat'];
									$pag++;
								}
								$lin++;
							}
							$totsol=($lin-1);
							$_SESSION['totalsolicitudes']=$totsol;
							$_SESSION['solicitudarreglo']=$row;
							$solicitudpagina=1;
							$_SESSION['solicitudpaginas']=$pag;
				}//fin de solicitudpagina
				/*se muestra la cantidad de elementos segun la solicitada en $solicitudpagina*/
									$paginat=$_SESSION['solicitudarreglo'];
$html.='
<table id="background-image">
<thead>
<tr>
<td colspan="9"><h1><img src="http://'.$Direccionip.'/idasysv3/images/logoidacadef2005.png" width="280" height="68" /></h1>
<h5>RIF:  J-30229023-6</h5></td>
</tr>
  	<tr>
    	<th width="7%" scope="col">Fecha</th>
    	<th width="10%" scope="col" align="center">Cod y Nro <br />Documento</th>
    	<th width="16%" scope="col">Estudio</th>
    	<th width="7%" scope="col" align="center">Monto <br />Bs.</th>
    	<th width="16%" scope="col">Paciente</th>
        <th width="7%" scope="col">C&eacute;dula</th>
		<th width="10%" scope="col" align="center">Honorarios <br />M&eacute;dicos <br />Bs.</th>
        <th width="13%" scope="col">Tipo de Cliente</th>
        <th width="6%" scope="col">Tarifa</th>
  	</tr></thead>';

									//print_r($paginat);
									$pagact=$solicitudpagina;
									for($g=0; $g < (count($paginat)); $g++)
									{
										if ($paginat[$g]["pagina"] < $pagact) continue;
										if ($paginat[$g]["pagina"] > $pagact) break; 
                                        /*$html.='<tr>
                                            <td  scope="col"><div>'.fecdma($paginat[$g]["ADDFEF"],'amd','.').'</div></td>                
                               				<td scope="col"><div><strong>'.$paginat[$g]["ACCTRA"].'-'.$paginat[$g]["ADDNRF"].'</strong></div></td>
                                            <td scope="col"><div  >'.$paginat[$g]["AESDES"].'</div></td>
                                            <td scope="col"><div  align="right" >'.number_format($paginat[$g]["ARGTTE"],2,",",".").'</div></td>
                                            <td scope="col"><div  >'.$paginat[$g]["ADDPNM"].'</div></td>
                                            <td scope="col"> <div align="right">'.$paginat[$g]["ADDPNI"].'</div></td>';
											*/
										$html.='<tr>
                                            <td scope="col">'.fecdma($paginat[$g]["ADDFEF"],'amd','.').'</td>
                               				<td scope="col"><strong>'.$paginat[$g]["ACCTRA"].'-'.$paginat[$g]["ADDNRF"].'</strong></td>
                                            <td scope="col">'.$paginat[$g]["AESDES"].'</td>
											<td scope="col" align="right" >'.number_format($paginat[$g]["ARGTTE"],2,",",".").'</td>
                                            <td scope="col">'.$paginat[$g]["ADDPNM"].'</td>
                                            <td scope="col" align="right"> '.$paginat[$g]["ADDPNI"].'</td>';
											
                                            /* seccion de opciones */ 
										/*$html.='
                                            <td scope="col"><div align="right">'.number_format($paginat[$g]["ARGTTM"],2,",",".").'</div>  </td>
                                            <td scope="col"><div>'.$paginat[$g]["APCDES"].'</div>  </td>
                                            <td scope="col"><div align="center">'.$paginat[$g]["ACOPTR"].'</div>  </td>
                                        </tr>';
										*/
										$html.='
                                            <td scope="col" align="right">'.number_format($paginat[$g]["ARGTTM"],2,",",".").'</td>
                                            <td scope="col">'.$paginat[$g]["APCDES"].'</td>
                                            <td scope="col" align="center">'.$paginat[$g]["ACOPTR"].'</td>
                                        </tr>';
									} 
									
/* $html.='<page_footer> 
 			<tr>
              <td colspan="9">FOOT pag [[page_cu]]/[[page_nb]]</td>
            </tr> 
        </page_footer>';*/
 $html.='</table>
</body>
</html>
</page>';

//echo $html;
    require_once(dirname(__FILE__).'/html2pdf/html2pdf.class.php');

    //$html2pdf = new HTML2PDF('landscape','letter','es');
	$html2pdf = new HTML2PDF('landscape','letter','es',array(0, 0, 0, 0));
	//$html2pdf = new HTML2PDF('landscape','A4','es',array(0, 0, 0, 0));
	//$html2pdf = new HTML2PDF('landscape',array(200,360),'es',array(0, 0, 0, 0));

    $html2pdf->WriteHTML($html);
    $html2pdf->Output('Listado de Estudios.pdf');
	

?>