<?php 
/*
 *jDavila
 *16/02/2012
 */
session_start();
include("../conectar.php");
include("../JSON.php");
$astcod=$_REQUEST["tiporeq"];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="sinhead">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Editar Tipo de Dotaci&oacute;n</title>
<link href="../<?php echo $css; ?>" rel="stylesheet" type="text/css" media="screen" />
<script language="JavaScript" src="../javascript/jquery.js" type="text/javascript"></script>
<script language="JavaScript" src="../javascript/javascript.js"></script>
<script language="JavaScript" src="javascript.js"></script>
</head>
<body class="sinbody" bgcolor="#FFFFFF" onload="javascript:editar('<?php echo $astcod;?>');">

<div id="editardiv" class="white_content">

        <form id="editarform" name="editarform" method="post" action="" class="form">

            <table width="60%"  border="0" >
                <tr>
                    <th width="29%" scope="col"><label>Tipo de Dotac&oacute;n</label></th>
                    <th width="30%" id="wsatscod" align="left" scope="col">&nbsp;</th>
                    <th colspan="2" id="errhatscod" class="Estilo5" scope="col">&nbsp;</th>
                    <input name="hatscod" id="hatscod" type="hidden">
                <tr>
                    <th scope="col"><label>Descripcion del Dotaci&oacute;n</label></th>
			        <th scope="col"><div align="left"><input name="atsdes" type="text" id="atsdes" size="40" maxlength="60" onKeyUp="editarform.atsdes.value=editarform.atsdes.value.toUpperCase()"/></div></th>
			        <th colspan="2" id="erratsdes" class="Estilo5" scope="col">&nbsp;</th>
                </tr>
                <tr>
                    <th scope="col"><label>Tipo Inventario</label></th>
                    <th scope="col">
                    	<div align="left">
                            <table border="0">
                            <input type="hidden" name="listaticod" id="listaticod" value="<?php echo $listaticod; ?>"  />
                            <?php 
                                $sql="SELECT ATICOD, ATIDES FROM IV01FP WHERE ACICOD='$Compania' ORDER BY ATICOD";
                                $result1=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111")); 
                                while(odbc_fetch_row($result1)){			
                                    $aticod=trim(odbc_result($result1,1));
                                    $atides=trim(odbc_result($result1,2));
									$checked = '';
									if($aticod == $listaticod)
									{
										$checked = "checked='checked'";
									}
                            ?>
                                    <tr>
                                    	<td>
                                        	<label name="aticod<?php echo "[".$aticod."]"; ?>"><?php echo $atides; ?></label>
                                        </td>
                                        <td>
                                        	<input name="aticod<?php echo "[".$aticod."]"; ?>" id="aticod<?php echo "[".$aticod."]"; ?>" type="checkbox" value="<?php echo $aticod; ?>" <?php echo $checked; ?> /><br />
                                        </td>
                                    
                            <?php } ?>
                            </table>
                        </div>
                    </th>
                    <th colspan="2" id="erraticod" class="Estilo5" scope="col">&nbsp;</th>
                </tr> 
                <tr>
                    <th scope="col"><label>Transacción Inventario</label></th>
                    <th scope="col"><div align="left"><select name="atrsal" id="atrsal">
						<?php 
							//$sql="SELECT atrcod, atrdes FROM iv12fp WHERE ACICOD='$Compania' and ATRSIG = '-' ORDER BY atrcod";
							$sql="SELECT atrcod, atrdes FROM iv12fp WHERE ACICOD='$Compania' ORDER BY atrcod";
							$result1=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111")); 
							while(odbc_fetch_row($result1)){			
								$atrcod=trim(odbc_result($result1,1));
								$atrdes=odbc_result($result1,2);
						?>
								<option value= "<?php echo $atrcod; ?>" ><?php echo $atrdes ?></option>
                        <?php } ?>
                        </select></div>
                    </th>
                    <th scope="col"><label>Transacción Inventario Reverso</label></th>
                    <th scope="col"><div align="left"><select name="atrent" id="atrent">
						<?php 
							//$sql="SELECT atrcod, atrdes FROM iv12fp WHERE ACICOD='$Compania' and ATRSIG = '-' ORDER BY atrcod";
							$sql="SELECT atrcod, atrdes FROM iv12fp WHERE ACICOD='$Compania' ORDER BY atrcod";
							$result1=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111")); 
							while(odbc_fetch_row($result1)){			
								$atrent=trim(odbc_result($result1,1));
								$atrdes=odbc_result($result1,2);
						?>
								<option value= "<?php echo $atrent; ?>" ><?php echo $atrdes ?></option>
                        <?php } ?>
                        </select></div>
                    </th>
                </tr>
                 <tr>
                    <th scope="col"><label>Requiere aprobaci&oacute;n</label></th>
                    <th scope="col"><div align="left">
                    		<input name="atrspr" id="atrspr" type="radio"  value="S" />Si
							<input name="atrspr" id="atrspr" type="radio"  value="N" />No
						</div>
                    </th>
                    <th colspan="2">&nbsp;</th>
                </tr>
                <tr >
                    <th scope="col">&nbsp;</th>
                    <th scope="col"></th>
                    <th width="23%"  scope="col"><input name="cancelar" type="button" onClick="parent.location.reload();parent.Shadowbox.close();"  value="Salir" id="cancelar"></th>
				    <th width="21%"  scope="col"><input type="button" name="submit" id="submit" value="Grabar" onClick="editarvalidar()" /></th>
                </tr>
            </table>
        </form>
</div>

<div align="center" id="editaraftsav" style="display:none">
  <br />
  <br />
  <br />
  <br />
  <br />
  <br />
  Registro Agregado con exito !
  <br />
  <br />
  <br />
  <input name="Submit3" type="button" onClick="parent.location.reload();parent.Shadowbox.close();"" " value="Cerrar"></div>
</body>
</html>