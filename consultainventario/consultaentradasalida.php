<?php 
/*
 * jDavila
 * 23/05/2012 
 * modificado: 24/05/2012
 */
session_start();
include("../conectar.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
<title>Idasys V3</title>
<link href="../<?php echo $css; ?>" rel="stylesheet" type="text/css" media="screen" />
<script language="JavaScript" src="../javascript/javascript.js"></script>
<script language="JavaScript" src="../calendario/javascripts.js"></script>
<script language="JavaScript" src="javascript.js"></script>
<script language="JavaScript" src="../javascript/jquery.js"></script>
<link href="../shadowbox.css"  rel="stylesheet" type="text/css"/>
<script src="../shadowbox.js" type="text/javascript"/> </script>
<script language="JavaScript" type="text/JavaScript">
Shadowbox.init({overlayOpacity: "0.5"});
</script>
<style type="text/css" title="currentStyle">
			@import "../DataTables-1.9.4/media/css/<?php echo $demo_page;?>";
			@import "../DataTables-1.9.4/media/css/<?php echo $demo_table;?>";
</style>
<script type="text/javascript" language="javascript" src="../DataTables-1.9.4/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf-8">
			
			 $(document).ready(function() 
			 {
			      document.getElementById('info').style.display="";
				  $('#info').dataTable( 
				  {
					"bStateSave": true,
			        "oLanguage": 
					{
			          	"sLengthMenu": "Mostrar _MENU_ registros por Pag.",
						"sZeroRecords": "No Existen Registros",
						"sInfo": "Mostrando desde _START_ hasta _END_ de _TOTAL_ registros",
						"sInfoEmpty": "Mostrando desde 0 hasta 0 de 0 registros",
						"sInfoFiltered": "(filtrando de _MAX_ de registros)",
						"sSearch": "Buscar",
						"oPaginate": 
						{
							"sNext": "Siguiente",
							"sPrevious": "Anterior"
						}
			        },
					"type" :
					[ [6,'INTEGER'] ]
					/*,
					"aoColumnDefs": [
						{ "bVisible": false, "aTargets": [ 0 ] }
					]*/
			      } );
			  } );
</script>

</head>
<body background="../images/fondo idaca.jpg" >
<div id="wrapper">
  <?php include("../superior.php");?>
  <div id="page">
     <?php include("../validar.php");?>

	  
 		<?php 
				//$anio = date("Y");
				$fecha = date("d-m-".$anio);
				
				if ($bandera==1) 
				{	
					$wsolicitud=0;
					if ($solicitudpagina==0) 	
					{
						$sql="SELECT T3.AARUMB,T4.AUMDES, T1.ACICOD, T1.AARCOD, T1.AARNIV, T1.ATICOD, T1.ASICOD, T1.AARDES, T1.AARTDT, T1.AARSTS 
								FROM IV05FP T1, IV06FP T3, IV13FP T4 
								WHERE T1.ACICOD='".$Compania."' AND T1.ACICOD=T3.ACICOD AND T1.AARCOD=T3.AARCOD AND T1.ACICOD=T4.ACICOD AND T3.AARUMB=T4.AUMCOD AND 
								( T1.AARCOD IN (SELECT T2.AARCOD FROM IV40FP T2 WHERE T2.ACICOD=T1.ACICOD AND T2.AALCOD='".$aalcod."' GROUP BY   
								T2.AARCOD ORDER BY T2.AARCOD ) ) AND T1.AARSTS = '01'";
							//T1.AARDES like ('A%') and 
						$resultt=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111"));
						//echo $sql."<br/>";
						$z=0;
						$tothon=0;
						$totest=0;
						$lin=1;
						$limitep=$_SESSION['solicitudlineasporpaginat'];
						$pag=1;
						$primero='S';
						
						
						set_time_limit(-1);
						while(odbc_fetch_row($resultt))
						{
							$artcod = odbc_result($resultt,'AARCOD');
							$atrdes = odbc_result($resultt,'AARDES');
							$aumdes = odbc_result($resultt,'AUMDES');

							$sql2="SELECT ('".$artcod."') as AARCOD, ('".$atrdes."') as AARDES, ('".$aumdes."') as AUMDES, ";
							$canmese = '';
							//for($k=(int)($mes2); $k<=(int)$mes1; $k++){ 
							for($k=0; $k<3; $k++){
								if( date('m',strtotime('0 months',strtotime(date($fecha)))) < date('m',strtotime('-'.$k.' months',strtotime(date($fecha)))) ){
									$anio2 = "".date('Y',strtotime('-1 year',strtotime(date($fecha))))."";
								}else{
									$anio2 = "".date('Y',strtotime('0 year',strtotime(date($fecha))))."";
								}
								$canmese .= "(SELECT SUM(T3.ATRCAN) AS ATRCAN 
											FROM IV16FP T3 
											INNER JOIN IV15FP T4 ON ( T3.ACICOD=T4.ACICOD AND T3.AALCOD=T4.AALCOD AND T3.ATRCOD=T4.ATRCOD AND T3.ATRNUM=T4.ATRNUM ) 
											INNER JOIN IV12FP T5 ON ( T4.ACICOD=T5.ACICOD AND T4.ATRCOD=T5.ATRCOD AND T5.ATRSIG='+') 
											WHERE  T4.ACICOD='".$Compania."' AND T4.AALCOD='".$aalcod."' AND month(T4.ATRFEC)='".date("m",strtotime("-".$k." month", strtotime($fecha)))."' AND YEAR(T4.ATRFEC)= '".$anio2."' AND T3.ATRART=T6.ATRART 
											GROUP BY T3.ATRART ORDER BY T3.ATRART ) AS ENT".date("m",strtotime("-".$k." month", strtotime($fecha)))." ,
											 (SELECT SUM(T3.ATRCAN) AS ATRCAN 
											FROM IV16FP T3 
											INNER JOIN IV15FP T4 ON ( T3.ACICOD=T4.ACICOD AND T3.AALCOD=T4.AALCOD AND T3.ATRCOD=T4.ATRCOD AND T3.ATRNUM=T4.ATRNUM ) 
											INNER JOIN IV12FP T5 ON ( T4.ACICOD=T5.ACICOD AND T4.ATRCOD=T5.ATRCOD AND T5.ATRSIG='-') 
											WHERE  T4.ACICOD='".$Compania."' AND T4.AALCOD='".$aalcod."' AND month(T4.ATRFEC)='".date("m",strtotime("-".$k." month", strtotime($fecha)))."' AND YEAR(T4.ATRFEC)= '".$anio2."' AND T3.ATRART=T6.ATRART 
											GROUP BY T3.ATRART ORDER BY T3.ATRART )AS SAL".date("m",strtotime("-".$k." month", strtotime($fecha)))." ,";
							}
							$canmese = substr($canmese,0, (strripos($canmese,",")));
							$canmese = $canmese." ";
									 
							$sql2.= $canmese." FROM IV16FP T6 
								  WHERE T6.ACICOD='".$Compania."' AND T6.AALCOD='".$aalcod."' AND T6.ATRART='".$artcod."'  
								  GROUP BY T6.ATRART ";
							//echo $sql2."<br/><br/>";
							
							$resultt2=odbc_exec($cid,$sql2)or die(exit("Error en odbc_exec 11111 - ".odbc_error($resultt2) ));
							while(odbc_fetch_row($resultt2))
							{
								$jml = odbc_num_fields($resultt2);
								$row[$z]["pagina"] =  $pag;
								for($i=1;$i<=$jml;$i++)
								{	if (odbc_field_name($resultt2,$i)=='ARGTTE') {$totest+=odbc_result($resultt2,$i);}
									if (odbc_field_name($resultt2,$i)=='ARGTTM') {$tothon+=odbc_result($resultt2,$i);}
									$row[$z][odbc_field_name($resultt2,$i)] =  odbc_result($resultt2,$i);
								}
								$z++;
								if ($lin>=$limitep) 
								{
									$limitep+=$_SESSION['solicitudlineasporpaginat'];
									$pag++;
								}
								$lin++;
							}
						}
						$totsol=($lin-1);
						$_SESSION['totalsolicitudes']=$totsol;
						$_SESSION['solicitudarreglo']=$row;
						$solicitudpagina=1;
						$_SESSION['solicitudpaginas']=$pag;
					}//fin de solicitudpagina
					/*se muestra la cantidad de elementos segun la solicitada en $solicitudpagina*/
					$paginat=$_SESSION['solicitudarreglo'];
				}
			?>
        <div id="content3" >   
        	<table width="100%"   border="0">
              <tr>
                <td width="65%" scope="col" colspan="2"><h1  class="title">Reporte de Entradas y Salidas</h1><hr /></td>
                <td width="13%" scope="col"><div align="left">
                  <table width="100%"  border="0">
                    <tr>
                      <th width="30%" scope="col" >
                      <div class="flechas">
                      	<a href="exportaraexcelentradasalida.php?&aalcod=<?php echo $aalcod; ?>&anio=<?php echo $anio; ?>" target="_blank">
                                <img src="../images/excel2.jpg" alt="" width="30" height="30" title="Exportar a Excel Entradas y Salidas"/>
                        </a>
                      </div></th>
                    </tr>
                  </table>
                </div></td>
              </tr>
              <tr>
              	<td colspan="3">
                	<form name="form" id="form" method="post">
                    <input type="hidden" name="bandera" id="bandera" value="<?php echo $bandera;?>" />
                	<table>
                    	<tr>
                        	<td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td rowspan="2"><a href="javascript:busquedaentrada();"><img src="../images/precesarconsulta.png" width="50" height="50" alt="Realizar Consulta" style="border:none;"></a></td>
                        </tr>
                        <tr>
                        	<td>&nbsp;Almac�n:</td>
                            <td>&nbsp;<?php //echo $aalcod; ?>
                            	<select name="aalcod" id="aalcod">
									<?php 
                                    //$sql="SELECT AALCOD, AALDES FROM iv07fp where acicod ='$Compania' order by AALDES ";
									if(accesotodasunisol_alma('4')=='S')
									{
										$sql="SELECT AALCOD, AALDES FROM iv07fp where acicod ='$Compania'  order by AALDES ";//and AALCOD <> '0017'
									}
									else
									{
										$sql = "SELECT T1.AALCOD, T2.AALDES FROM iv44fp T1, IV07FP T2 WHERE T1.acicod ='$Compania' AND T1.ACICOD=T2.ACICOD AND T1.auscod='$Usuario' AND T1.AALCOD= T2.AALCOD order by t2.AALDES";
									}
                                    $result1=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111")); 
									$select = '';
                                    while(odbc_fetch_row($result1)){
                                        $cod=trim(odbc_result($result1,1));
                                        $des=trim(odbc_result($result1,2));
										if(!empty($aalcod))
										{
											if($cod == $aalcod){
												$select = ' selected="selected" ';
											}
											else{
												$select = '';
											}
										}											
                                    ?>
                                        <option value= "<?php echo $cod; ?>" <?php echo $select?> ><?php echo $des."(".$cod.")"; ?></option>
                                    <?php } ?>
                                </select>
                            </td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                        	<td>&nbsp;A�o:</td>
                            <td>&nbsp;<?php //echo $anio2; ?>
                            	<select name="anio" id="anio">
									<?php 
									$sql3 ="SELECT YEAR(T4.ATRFEC) AS ANIO 
												FROM IV16FP T3 
												INNER JOIN IV15FP T4 ON ( T3.ACICOD=T4.ACICOD AND T3.AALCOD=T4.AALCOD AND T3.ATRCOD=T4.ATRCOD AND T3.ATRNUM=T4.ATRNUM ) 
												INNER JOIN IV12FP T5 ON ( T4.ACICOD=T5.ACICOD AND T4.ATRCOD=T5.ATRCOD AND T5.ATRSIG='-') 
												WHERE  T4.ACICOD='$Compania' 
												GROUP BY YEAR(T4.ATRFEC)                               
												ORDER BY YEAR(T4.ATRFEC) DESC";
									
                                    $result3=odbc_exec($cid,$sql3)or die(exit("Error en odbc_exec 11111")); 
									$select3 = '';
                                    //if(odbc_num_rows($result3) != 0){
									if(1==1){
										while(odbc_fetch_row($result3)){
											$ani=trim(odbc_result($result3,'ANIO'));
											if(!empty($ani))
											{
												if($ani == $anio){
													$select3 = ' selected="selected" ';
												}else{
													$select3 = '';
												}
											}											
										?>
											<option value= "<?php echo $ani; ?>" <?php echo $select3?> ><?php echo $ani; ?></option>
										 <?php 		} 
								   }else{
									   $ani= date('Y');
							?>
									   <option value= "<?php echo $ani; ?>" <?php echo $select3?> ><?php echo $ani; ?></option>
                             <?php
								   }
							 ?>
                                </select>
                            </td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                    </form>
                    <hr />
                </td>
                
              </tr>
              <!--<tr>
                <td colspan="2" scope="col"><h5 align="left"><?php echo $paginat[0]["ALCSED"];?>wwww</h5></td>
                <td scope="col"> Pagina: <?php if (!$solicitudpagina) {$paginaarriba=1;} else {$paginaarriba=$solicitudpagina;}; echo $paginaarriba."/".$_SESSION['solicitudpaginas']; ?></td>
              </tr>-->
            </table>   
		<div id="container">
            <div id="demo">
                <table width="100%" id="info" style="display:none">
                  <thead>
                    <tr>
                        <th scope="col" rowspan="2">Cod. Art�culo</th>
                        <th scope="col" rowspan="2">Art�culo</th>
                        <th scope="col" rowspan="2">Unidad de Medida</th>
                        <?php //for($k=(int)($mes2); $k<=(int)$mes1; $k++){ 
							  //for($k=0; $k<3; $k++){
							  for($k=2; $k!=-1; $k--){
						?>
                            <th scope="col" colspan="2"><?php echo mesescrito(date("m",strtotime("-".$k." month", strtotime($fecha)))); ?></th>
                        <?php } ?>
                    </tr>
                    <tr>
                        <?php //for($k=(int)($mes2); $k<=(int)$mes1; $k++){ 
							//for($k=0; $k<3; $k++){
							  for($k=2; $k!=-1; $k--){
						?>
                            <th scope="col">Entradas</th>
                            <th scope="col">Salidas</th>
                        <?php } ?>
                    </tr>
                  </thead>
    				<tbody>
        						<?php 
									$show = false;
									//print_r($paginat);
									$pagact=$solicitudpagina;
									for($g=0; $g < (count($paginat)); $g++)
									{
                                ?>
                                
                                        <tr>
                                        	<td scope="col"><div><?php echo $paginat[$g]["AARCOD"];?></div></td>
                                            <td scope="col"><div><?php echo $paginat[$g]["AARDES"];?></div></td>
                                            <td scope="col"><div align="center"><?php echo $paginat[$g]["AUMDES"];?></div></td>
                                            <?php //for($h=(int)($mes2); $h<=(int)$mes1; $h++){ 
												 //for($k=0; $k<3; $k++){
							  					for($k=2; $k!=-1; $k--){
												$valent= "ENT".date("m",strtotime("-".$k." month", strtotime($fecha))); 
												$valsal= "SAL".date("m",strtotime("-".$k." month", strtotime($fecha))); 
											?>
                                                <td scope="col"><div align="right"><?php echo number_format($paginat[$g][$valent],2,",",".");?></div></td>
                                                <td scope="col"><div align="right"><?php echo number_format($paginat[$g][$valsal],2,",",".");?></div></td>
                                            <?php } ?>
                                        </tr>
                            	<?php }?>      
                  </tbody>
                </table>
</div>
		  </div>
	  </div>
	</div>
		<!-- end #content -->
		<!-- end #sidebar -->
		<div style="clear: both;">&nbsp;</div>
	</div>
	<!-- end #page -->

	<div id="footer">
		<div id="opcionesmasusadas"><?php include("../opcionesmasusadasphp.php"); ?></div>
	</div>
 
	<!-- end #footer -->
</body>
</html>
