<?php 
/*
 * jDavila
 * 23/02/2015
 */
session_start();
include("../conectar.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
<title>Idasys V3</title>
<link href="../<?php echo $css; ?>" rel="stylesheet" type="text/css" media="screen" />
<script language="JavaScript" src="../javascript/javascript.js"></script>
<script language="JavaScript" src="../calendario/javascripts.js"></script>
<script language="JavaScript" src="javascript.js"></script>
<script language="JavaScript" src="../javascript/jquery.js"></script>
<link href="../shadowbox.css"  rel="stylesheet" type="text/css"/>
<script src="../shadowbox.js" type="text/javascript"/> </script>
<script language="JavaScript" type="text/JavaScript">
Shadowbox.init({overlayOpacity: "0.5"});
</script>
<style type="text/css" title="currentStyle">
			@import "../DataTables-1.9.4/media/css/<?php echo $demo_page;?>";
			@import "../DataTables-1.9.4/media/css/<?php echo $demo_table;?>";
</style>
<script type="text/javascript" language="javascript" src="../DataTables-1.9.4/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf-8">
		
			
			
			 $(document).ready(function() 
			 {
			      document.getElementById('info').style.display="";

				 $('#info').dataTable( 
				  {
					"bStateSave": true,
			        "oLanguage": 
					{
			          	"sLengthMenu": "Mostrar _MENU_ registros por Pag.",
						"sZeroRecords": "No Existen Registros",
						"sInfo": "Mostrando desde _START_ hasta _END_ de _TOTAL_ registros",
						"sInfoEmpty": "Mostrando desde 0 hasta 0 de 0 registros",
						"sInfoFiltered": "(filtrando de _MAX_ de registros)",
						"sSearch": "Buscar",
						"oPaginate": 
						{
							"sNext": "Siguiente",
							"sPrevious": "Anterior"
						}
			        }
			      } );
			  } );
</script>

</head>
<body background="../images/fondo idaca.jpg" >
<div id="wrapper">
  <?php include("../superior.php");?>
  <div id="page">
     <?php  include("../validar.php");?>
 		<?php 
				
				if ($bandera==1) 
				{	
					$wsolicitud=0;
					if ($solicitudpagina==0) 	
					{

						$sql="SELECT T5.AAPVLA, T5.APACOD, T3.AARUMB, T4.AUMDES, T1.ACICOD, T1.AARCOD, T1.AARNIV, T1.ATICOD, T1.ASICOD, T1.AARDES, T1.AARTDT, T1.AARSTS 
							FROM IV05FP T1 
								INNER JOIN IV06FP T3 ON ( T1.ACICOD=T3.ACICOD AND T1.AARCOD=T3.AARCOD ) 
								INNER JOIN IV13FP T4 ON ( T1.ACICOD=T4.ACICOD AND T3.AARUMB=T4.AUMCOD ) 
								LEFT JOIN IV17FP T5 ON ( T1.ACICOD=T5.ACICOD and T1.AARCOD=T5.AARCOD AND T5.APACOD = '0135' ) 
							WHERE T1.ACICOD='".$Compania."' AND 
								( T1.AARCOD IN (SELECT T2.AARCOD  FROM IV40FP T2 WHERE T2.ACICOD=T1.ACICOD AND T2.AALCOD='".$aalcod."' GROUP BY T2.AARCOD ORDER BY T2.AARCOD ) ) 
							ORDER BY T1.AARCOD";
						

						//echo $sql."<br/><br/>";
						$resultt=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111"));
						
						$z=0;
						$tothon=0;
						$totest=0;
						$lin=1;
						$limitep=$_SESSION['solicitudlineasporpaginat'];
						$pag=1;
						$primero='S';
						
						
						while(odbc_fetch_row($resultt))
						{
							$artcod = odbc_result($resultt,'AARCOD');
							$atrdes = odbc_result($resultt,'AARDES');
							$exp = substr($atrdes ,(strripos($atrdes,"(")) );
							$aumdes = odbc_result($resultt,'AUMDES');
							
							$listapacod = array('0133','0134','0135','0136','0137','0138','0139','0140','0141','0142','0143','0144','0145','0146','0147','0148','0149','0159','0160','0161','0162','0163','0164');
							//$listapacod = explode(',', $listapacod);
							$sql2="";
							$sql3="";
							$sql2="
								SELECT T4.ACICOD, T4.AALCOD, T4.ADPCOD, T4.ATRCOD, (T5.ATRDES) as ATRNOM, T5.ATRSIG, T4.ATRNUM, T4.ATRDES, 
										T4.ATRFEC, T4.AUSCOD, T4.ATROBS, T3.ATRSEC, T3.ATRART,('".$atrdes."') AS AARDES, T3.ATRCAN, T3.ATRUMB, ('".$aumdes."') AS AUMDES, ";
							foreach($listapacod as $key=>$value){			
								$sql3.="
										(SELECT T6.AAPVLA FROM iv17fp T6 WHERE T6.ACICOD=T3.ACICOD and T6.AARCOD=T3.ATRART AND T6.APACOD IN (".$value.") ) AS N".$value." , ";
							}
							$sql2.= substr($sql3,0, (strripos($sql3,",")));
							$sql2.="	
								FROM IV16FP T3 
									INNER JOIN IV15FP T4 ON ( T3.ACICOD=T4.ACICOD AND T3.AALCOD=T4.AALCOD AND T3.ATRCOD=T4.ATRCOD AND T3.ATRNUM=T4.ATRNUM )                                                                   
									INNER JOIN IV12FP T5 ON ( T4.ACICOD=T5.ACICOD AND T4.ATRCOD=T5.ATRCOD AND T5.ATRSIG='+')
								WHERE T4.ACICOD='".$Compania."' AND T4.AALCOD='".$aalcod."' AND T3.ATRART = '".$artcod."' AND T4.ATRCOD IN ('01') 
								ORDER BY T3.ATRART
								";
							//echo $sql2."<br/><br/>";
							//DIE();
							$resultt2=odbc_exec($cid,$sql2)or die(exit("Error en odbc_exec 11111 - ".odbc_error($resultt2) ));
							while(odbc_fetch_row($resultt2))
							{
								$jml = odbc_num_fields($resultt2);
								$row[$z]["pagina"] =  $pag;
								for($i=1;$i<=$jml;$i++)
								{	
									$row[$z][odbc_field_name($resultt2,$i)] =  odbc_result($resultt2,$i);
								}
								$z++;
								if ($lin>=$limitep) 
								{
									$limitep+=$_SESSION['solicitudlineasporpaginat'];
									$pag++;
								}
								$lin++;
							}
						}
						$totsol=($lin-1);
						$_SESSION['totalsolicitudes']=$totsol;
						$_SESSION['solicitudarreglo']=$row;
						$solicitudpagina=1;
						$_SESSION['solicitudpaginas']=$pag;
					}//fin de solicitudpagina
					/*se muestra la cantidad de elementos segun la solicitada en $solicitudpagina*/
					$paginat=$_SESSION['solicitudarreglo'];
				}
			?>
        <div id="content3" >   
        	<table width="100%"   border="0">
              <tr>
                <td width="65%" scope="col" colspan="2"><h1  class="title">Reporte de Meditron-CENTIS</h1><hr /></td>
                <td width="13%" scope="col"><div align="left">
                  <table width="100%"  border="0">
                    <tr>
                      <th width="30%" scope="col" >
                      <div class="flechas">
                      	<a href="exportaraexcel_centis.php?&aalcod=<?php echo $aalcod; ?>" target="_blank">
                        	<img src="../images/excel.jpg" alt="" width="30" height="30" title="Exportar a Excel Salida"/>
                        </a>
                      </div></th>
                    </tr>
                  </table>
                </div></td>
              </tr>
              <tr>
              	<td colspan="3">
                	<form name="form" id="form" method="post">
                    <input type="hidden" name="bandera" id="bandera" value="<?php echo $bandera;?>" />
                	<table>
                    	<tr>
                        	<td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td rowspan="2"><a href="javascript:busquedacv();"><img src="../images/precesarconsulta.png" width="50" height="50" alt="Realizar Consulta" style="border:none;"></a></td>
                        </tr>
                        <tr>
                        	<td>&nbsp;Almac�n:</td>
                            <td>&nbsp;<?php //echo $aalcod; ?>
                            	<select name="aalcod" id="aalcod">
									<?php 
                                    //$sql="SELECT AALCOD, AALDES FROM iv07fp where acicod ='$Compania' order by AALDES ";
									if(accesotodasunisol_alma('4')=='S')
									{
										$sql="SELECT AALCOD, AALDES FROM iv07fp where acicod ='$Compania'  order by AALDES ";//and AALCOD <> '0017'
									}
									else
									{
										$sql = "SELECT T1.AALCOD, T2.AALDES FROM iv44fp T1, IV07FP T2 WHERE T1.acicod ='$Compania' AND T1.ACICOD=T2.ACICOD AND T1.auscod='$Usuario' AND T1.AALCOD= T2.AALCOD order by t2.AALDES";
									}
                                    $result1=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111")); 
									$select = '';
                                    while(odbc_fetch_row($result1)){
                                        $cod=trim(odbc_result($result1,1));
                                        $des=trim(odbc_result($result1,2));
										if(!empty($aalcod))
										{
											if($cod == $aalcod){
												$select = ' selected="selected" ';
											}
											else{
												$select = '';
											}
										}											
                                    ?>
                                        <option value= "<?php echo $cod; ?>" <?php echo $select?> ><?php echo $des."(".$cod.")"; ?></option>
                                    <?php } ?>
                                </select>
                            </td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                    </form>
                    <hr />
                </td>
                
              </tr>
            </table>   
		<div id="container">
            <div id="demo">
                <table width="100%" id="info" style="display:">
                  <thead>
                    <tr>
	                   	<!--<th scope="col">Nro Transacci�n </th>-->
                    	<th scope="col">Fecha Llegada a Meditron</th>
                        <th scope="col">Nro de env�o - C�digo Art.</th>
                        <th scope="col">Art�culo</th>
                        <!--<th scope="col">Descripci�n Larga</th>-->
                        <!--<th scope="col">Nro Contenedor</th>-->
                        <!--<th scope="col">Nro Lote</th>-->
                        <th scope="col">Gu�a</th>
                        <th scope="col">Fecha Gu�a</th>
                        <th scope="col">Cantidad</th>
                        <th scope="col">Unidad</th>
                        <!--<th scope="col">Nro Factura</th>
                        <th scope="col">Fecha Factura</th>-->
                        <th scope="col">Nro Empaque</th>
                        <th scope="col">Fecha Empaque</th>
                        <th scope="col">Fecha Llegada al Pa�s</th>
                        <!--<th scope="col">Dimensiones</th>-->
                        <!--<th scope="col">Peso Neto</th>-->
                        <!--<th scope="col">Peso Bruto</th>-->
                    </tr>
                  </thead>
    				<tbody>
        						<?php
									//print_r($paginat);
									$pagact=$solicitudpagina;
									$part= 1;
									for($g=0; $g < (count($paginat)); $g++)
									{
										if(strripos($paginat[$g]["AARDES"],"(") > 0){
											$exp = substr($paginat[$g]["AARDES"] ,(strripos($paginat[$g]["AARDES"],"(")+1), (strripos($paginat[$g]["AARDES"],")")-strripos($paginat[$g]["AARDES"],"(")-1)  );
										}else{
											$exp = '-';
										}
                                 ?>           
                                                <tr>
                                                    <!--<td scope="col" valign="middle"><div><?php //echo $paginat[$g]["ATRNUM"]!=''?$paginat[$g]["ATRNUM"]:"--";?></div></td>-->
                                                    <td scope="col" valign="middle"><div><?php echo $paginat[$g]["N0140"]!=''?formatDate($paginat[$g]["N0140"], 'dd.mm.aaaa', 'aaaa/mm/dd'):"--";?></div></td>
                                                    <td scope="col" valign="middle"><div><?php echo $paginat[$g]["ATRART"]!=''?$paginat[$g]["ATRART"]:"--";?></div></td>
                                                    <td scope="col" valign="middle"><div><?php echo $paginat[$g]["AARDES"]!=''?$paginat[$g]["AARDES"]:"--";?></div></td>
                                                    <!--<td scope="col" valign="middle"><div><?php //echo $paginat[$g]["N0142"]!=''?$paginat[$g]["N0142"]:"--";?></div></td>-->
                                                    <!--<td scope="col" valign="middle"><div><?php //echo $paginat[$g]["N0133"]!=''?$paginat[$g]["N0133"]:"--";?></div></td>-->
                                                    <!--<td scope="col" valign="middle"><div><?php //echo $paginat[$g]["N0134"]!=''?$paginat[$g]["N0134"]:"--";?></div></td>-->
                                                    <td scope="col" valign="middle"><div><?php echo $paginat[$g]["N0137"]!=''?$paginat[$g]["N0137"]:"--";?></div></td>
                                                    <td scope="col" valign="middle"><div><?php echo $paginat[$g]["N0138"]!=''?formatDate($paginat[$g]["N0138"], 'dd.mm.aaaa', 'aaaa/mm/dd'):"--";?></div></td>
                                                    <td scope="col" valign="middle"><div><?php echo $paginat[$g]["ATRCAN"]!=''?number_format($paginat[$g]["ATRCAN"],2,",","."):number_format(0,2,",",".");?></div></td>
                                                    <td scope="col" valign="middle"><div><?php echo $paginat[$g]["AUMDES"]!=''?$paginat[$g]["AUMDES"]:"--";?></div></td>
                                                    <!--<td scope="col" valign="middle"><div><?php //echo $paginat[$g]["N0135"]!=''?$paginat[$g]["N0135"]:"--";?></div></td>
                                                    <td scope="col" valign="middle"><div><?php //echo $paginat[$g]["N0136"]!=''?formatDate($paginat[$g]["N0136"], 'dd.mm.aaaa', 'aaaa/mm/dd'):"--";?></div></td>-->
                                                    <td scope="col" valign="middle"><div><?php echo $paginat[$g]["N0164"]!=''?$paginat[$g]["N0164"]:"--";?></div></td>
                                                    <td scope="col" valign="middle"><div><?php echo $paginat[$g]["N0141"]!=''?formatDate($paginat[$g]["N0141"], 'dd.mm.aaaa', 'aaaa/mm/dd'):"--";?></div></td>
                                                    <td scope="col" valign="middle"><div><?php echo $paginat[$g]["N0139"]!=''?formatDate($paginat[$g]["N0139"], 'dd.mm.aaaa', 'aaaa/mm/dd'):"--";?></div></td>
                                                    <!--<td scope="col" valign="middle"><div><?php //echo $paginat[$g]["N0143"]!=''?$paginat[$g]["N0143"]:"--";?></div></td>-->
                                                    <!--<td scope="col" valign="middle"><div><?php //echo $paginat[$g]["N0144"]!=''?$paginat[$g]["N0144"]." Kg.":"--";?></div></td>-->
                                                    <!--<td scope="col" valign="middle"><div><?php //echo $paginat[$g]["N0145"]!=''?$paginat[$g]["N0145"]." Kg.":"--";?></div></td>-->                                   				
                                            </tr>
                                <?php 
									}
								?>      
                  </tbody>
                </table>
</div>
		  </div>
	  </div>
	</div>
		<!-- end #content -->
		<!-- end #sidebar -->
		<div style="clear: both;">&nbsp;</div>
	</div>
	<!-- end #page -->

	<div id="footer">
		<div id="opcionesmasusadas"><?php include("../opcionesmasusadasphp.php"); ?></div>
	</div>
 
	<!-- end #footer -->
</body>
</html>
