<?php 
/*
 * Luis Ramos (lRamos)
 * 10/11/2016 
 * Johan Davila
 * 28/03/2017
 */
session_start();
include("../../conectar.php");
$tinva = $_GET['tinvan'];
$aticod = $_GET['aticod'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
<title>Idasys V3</title>
<link href="../../<?php echo $css; ?>" rel="stylesheet" type="text/css" media="screen" />
<script language="JavaScript" src="../../javascript/javascript.js"></script>
<script language="JavaScript" src="../../calendario/javascripts.js"></script>
<script language="JavaScript" src="../javascript.js"></script>
<script language="JavaScript" src="../../javascript/jquery.js"></script>
<link href="../../shadowbox.css"  rel="stylesheet" type="text/css"/>
<script src="../../shadowbox.js" type="text/javascript"/> </script>
<script language="JavaScript" type="text/JavaScript">
Shadowbox.init({overlayOpacity: "0.5"});
</script>
<style type="text/css" title="currentStyle">
			@import "../../DataTables-1.9.4/media/css/<?php echo $demo_page;?>";
			@import "../../DataTables-1.9.4/media/css/<?php echo $demo_table;?>";
</style>
<script type="text/javascript" language="javascript" src="../../DataTables-1.9.4/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf-8">
			
			 $(document).ready(function() 
			 {
			      document.getElementById('info').style.display="";
				  $('#info').dataTable( 
				  {
					"bStateSave": true,
			        "oLanguage": 
					{
			          	"sLengthMenu": "Mostrar _MENU_ registros por Pag.",
						"sZeroRecords": "No Existen Registros",
						"sInfo": "Mostrando desde _START_ hasta _END_ de _TOTAL_ registros",
						"sInfoEmpty": "Mostrando desde 0 hasta 0 de 0 registros",
						"sInfoFiltered": "(filtrando de _MAX_ de registros)",
						"sSearch": "Buscar",
						"oPaginate": 
						{
							"sNext": "Siguiente",
							"sPrevious": "Anterior"
						}
			        }
					/*,
					"aoColumnDefs": [
						{ "bVisible": false, "aTargets": [ 0 ] }
					]*/
			      } );
			  } );
</script>
</head>
<body background="../../images/fondo idaca.jpg" >
<div id="wrapper">
  <?php include("../../superior.php");?>
  <div id="page">
     <?php include("../../validar.php");?>

	  
 		<?php 
				
				if ($bandera==1) 
				{	
					$tipoAlm = '';
					$tipoInve ='';
					if(!empty($aticod)){
						if($aticod=='01'){
							$tipoInve = '0001'; //EQUIPOS
							$tipoAlm = '0002'; //ALMACEN EQUIPOS
							$tipoSal = '0203'; //TIPO DE SALIDA
							$tipoDevT ='0219'; //TIPO DE DEVOLUCION TRANSACCION
							$tipoDevD ='11'; //TIPO DE DEVOLUCION DOTACION
						} elseif($aticod=='02'){
							$tipoInve = '0002'; //CONSUMIBLES
							$tipoAlm = '0001'; //ALMACEN CONSUMIBLE BI
							$tipoSal = '0209'; //TIPO DE SALIDA
							$tipoDevT ='0217'; //TIPO DE DEVOLUCION TRANSACCION
							$tipoDevD ='09'; //TIPO DE DEVOLUCION DOTACION
						} elseif($aticod=='03'){
							$tipoInve = '0002'; //CONSUMIBLES
							$tipoAlm = '0002'; //ALMACEN CONSUMIBLE NV
							$tipoSal = '0206'; //TIPO DE SALIDA
							$tipoDevT ='0218'; //TIPO DE DEVOLUCION TRANSACCION
							$tipoDevD ='10'; //TIPO DE DEVOLUCION DOTACION
						} elseif($aticod=='04'){
							$tipoInve = '0003'; //REPUESTOS
							$tipoAlm = '0003'; //ALMACEN REPUESTOS
							$tipoSal = '0213'; //TIPO DE SALIDA
							$tipoDevT ='0220'; //TIPO DE DEVOLUCION TRANSACCION
							$tipoDevD ='12'; //TIPO DE DEVOLUCION DOTACION
						}
					}

					/*
					$sql="SELECT T1.AARCOD, T1.AARDES,
							(SELECT SUM(T2.ATRCAN) FROM IV16FP T2 WHERE T2.ACICOD='42' AND T2.AALCOD IN ('$tipoAlm') AND T2.ATRCOD IN ( '0101', '0102') AND T2.ATRART=T1.AARCOD) as ENTRADAS,
							(SELECT SUM(T3.ATRCAN) FROM IV16FP T3 WHERE T3.ACICOD='42' AND T3.AALCOD IN ('$tipoAlm','0004', '0005', '0006') AND T3.ATRCOD IN ('$tipoSal','0103') AND T3.ATRART=T1.AARCOD) as SALIDAS, 
							(SELECT MAX(T4.ATRCUT) FROM IV16FP T4 WHERE T4.ACICOD='42' AND T4.AALCOD IN ('$tipoAlm') AND T4.ATRCOD='0101' AND T4.ATRART=T1.AARCOD) as COSTO,
							(SELECT SUM(T3.ATRCAN) FROM IV16FP T3 WHERE T3.ACICOD='42' AND T3.AALCOD IN ('$tipoAlm','0004', '0005', '0006') AND T3.ATRCOD='$tipoDevT' AND T3.ATRART=T1.AARCOD) as DEVOLUCIONES
							FROM IV05FP T1 WHERE T1.ACICOD='42' AND (SELECT SUM(T2.ATRCAN) FROM IV16FP T2 WHERE T2.ACICOD='42' AND T2.AALCOD IN ('$tipoAlm','0004', '0005', '0006') AND T2.ATRCOD='0101' AND T2.ATRART=T1.AARCOD) > 0";
					if(!empty($tipoInve)){
						$sql.="  AND T1.ATICOD IN (".$tipoInve.")";
					}
					*/
					$sql ="SELECT T1.AARCOD, T1.AARDES, 
							(
							 (SELECT SUM(T2.ATRCAN) FROM IDASYSW.IV16FP T2 WHERE T2.ACICOD='42' AND T2.AALCOD IN ('$tipoAlm') AND T2.ATRCOD IN ( '0101' ) AND T2.ATRART=T1.AARCOD) - 
							 CASE 
								WHEN (SELECT SUM(T5.ATRCAN) FROM IDASYSW.IV16FP T5 WHERE T5.ACICOD='42' AND T5.AALCOD IN ('$tipoAlm','0004', '0005', '0006') AND T5.ATRCOD IN ( '0103' ) AND T5.ATRART=T1.AARCOD )>0 
								THEN (SELECT SUM(T5.ATRCAN) FROM IDASYSW.IV16FP T5 WHERE T5.ACICOD='42' AND T5.AALCOD IN ('$tipoAlm','0004', '0005', '0006') AND T5.ATRCOD IN ( '0103' ) AND T5.ATRART=T1.AARCOD )
								ELSE 0
							  END +
							   CASE 
								WHEN (SELECT SUM(T5.ATRCAN) FROM IDASYSW.IV16FP T5 WHERE T5.ACICOD='42' AND T5.AALCOD IN ('$tipoAlm','0004', '0005', '0006') AND T5.ATRCOD IN ( '0102' ) AND T5.ATRART=T1.AARCOD )>0 
								THEN (SELECT SUM(T5.ATRCAN) FROM IDASYSW.IV16FP T5 WHERE T5.ACICOD='42' AND T5.AALCOD IN ('$tipoAlm','0004', '0005', '0006') AND T5.ATRCOD IN ( '0102' ) AND T5.ATRART=T1.AARCOD )
								ELSE 0
							  END
							) as ENTRADAS,
							
							(
							 (SELECT SUM(T3.ATRCAN) FROM IDASYSW.IV16FP T3 WHERE T3.ACICOD='42' AND T3.AALCOD IN ('$tipoAlm','0004', '0005', '0006') AND T3.ATRCOD IN ('$tipoSal','0103') AND T3.ATRART=T1.AARCOD) -
							 CASE 
								WHEN (SELECT SUM(T5.ATRCAN) FROM IDASYSW.IV16FP T5 WHERE T5.ACICOD='42' AND T5.AALCOD IN ('$tipoAlm','0004', '0005', '0006') AND T5.ATRCOD IN ( '0103' ) AND T5.ATRART=T1.AARCOD )>0 
								THEN (SELECT SUM(T5.ATRCAN) FROM IDASYSW.IV16FP T5 WHERE T5.ACICOD='42' AND T5.AALCOD IN ('$tipoAlm','0004', '0005', '0006') AND T5.ATRCOD IN ( '0103' ) AND T5.ATRART=T1.AARCOD )
								ELSE 0
							  END +
							   CASE 
								WHEN (SELECT SUM(T5.ATRCAN) FROM IDASYSW.IV16FP T5 WHERE T5.ACICOD='42' AND T5.AALCOD IN ('$tipoAlm','0004', '0005', '0006') AND T5.ATRCOD IN ( '0102' ) AND T5.ATRART=T1.AARCOD )>0 
								THEN (SELECT SUM(T5.ATRCAN) FROM IDASYSW.IV16FP T5 WHERE T5.ACICOD='42' AND T5.AALCOD IN ('$tipoAlm','0004', '0005', '0006') AND T5.ATRCOD IN ( '0102' ) AND T5.ATRART=T1.AARCOD )
								ELSE 0
							  END
							) as SALIDAS, 
							
							(SELECT MAX(T4.ATRCUT) FROM IDASYSW.IV16FP T4 WHERE T4.ACICOD='42' AND T4.AALCOD IN ('$tipoAlm') AND T4.ATRCOD='0101' AND T4.ATRART=T1.AARCOD) as COSTO, 
							(SELECT SUM(T3.ATRCAN) FROM IDASYSW.IV16FP T3 WHERE T3.ACICOD='42' AND T3.AALCOD IN ('$tipoAlm','0004', '0005', '0006') AND T3.ATRCOD='$tipoDevT' AND T3.ATRART=T1.AARCOD) as DEVOLUCIONES 
							FROM IDASYSW.IV05FP T1 
							WHERE T1.ACICOD='42' AND 
							(SELECT SUM(T2.ATRCAN) FROM IDASYSW.IV16FP T2 WHERE T2.ACICOD='42' AND T2.AALCOD IN ('$tipoAlm','0004', '0005', '0006') AND T2.ATRCOD='0101' AND T2.ATRART=T1.AARCOD) > 0 AND 
							T1.ATICOD IN ('$tipoInve') 
							ORDER BY T1.AARDES";

					//echo $sql;

					$result=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111"));
				}
			?>
        <div id="content3" >   
        	<table width="100%"   border="0">
              <tr>
                <td width="65%" scope="col" colspan="2"><h1  class="title">Reporte de Inventario con Costos de Entrada y Salida - MEDIX</h1>
                  <hr /></td>
                <td width="13%" scope="col"><div align="left">
                  <table width="100%"  border="0">
                    <tr>
                      <th width="30%" scope="col" >
                      <div class="flechas">
                      	<a href="exportaraexcel_medix_costo_ent_sal.php?&aticod=<?php echo $aticod; ?>&stm=<?php echo $stm; ?>&tinvan=<?php echo $url222 = urlencode( serialize($tinva) );?>" target="_blank">
                        	<img src="../../images/excel.jpg" alt="" width="30" height="30" title="Exportar a Excel"/>
                        </a>
                      </div></th>
                    </tr>
                  </table>
                </div></td>
              </tr>
              <tr>
              	<td colspan="3">
                	<form name="form" id="form" method="post" on>
                    <input type="hidden" name="bandera" id="bandera" value="<?php echo $bandera;?>" />
                    <input type="hidden" name="tipoAlm" id="tipoAlm" value="<?php echo $tipoAlm;?>" />
                    <input type="hidden" name="tipoInve" id="tipoInve" value="<?php echo $tipoInve;?>" />
                   <!-- <input type="hidden" name="aalcod" id="aalcod" value="0004" /> -->                    
                	<table>
                	<tr>
                        	<td>&nbsp;Tipo de Inventario:</td>
                            <td>&nbsp;<?php //echo $aticod; ?>
                            	<input type="radio" name="aticod" id="aticod0" value="01" <?php if($aticod=='01'){echo 'checked="checked"';} ?>> EQUIPOS (0001)&nbsp;
                            	<input type="radio" name="aticod" id="aticod1" value="02" <?php if($aticod=='02'){echo 'checked="checked"';} ?>> CONSUMIBLES BI (0002)&nbsp;
                            	<input type="radio" name="aticod" id="aticod2" value="03" <?php if($aticod=='03'){echo 'checked="checked"';} ?>> CONSUMIBLES NV (0002)&nbsp;
                            	<input type="radio" name="aticod" id="aticod3" value="04" <?php if($aticod=='04'){echo 'checked="checked"';} ?>> REPUESTOS (0003)&nbsp;
                            </td>
                            <td >&nbsp;<a href="javascript:busqueda6();"><img src="../../images/precesarconsulta.png" width="50" height="50" alt="Realizar Consulta" style="border:none;"></a></td>
                        </tr>
                    </table>
                    </form>
                </td>
              </tr>
            </table>   
		<div id="container">
            <div id="demo">
                <table width="100%" id="info"  border="0" style="display:none;">
                  <thead>
                    <tr>
                    	<th scope="col" rowspan="2" style="background-color:rgb(204,204,204)">C&oacute;digo</th>
                   		<th scope="col" rowspan="2" style="background-color:rgb(204,204,204)">Art&iacute;culo</th>
                        <th scope="col" rowspan="2" style="background-color:rgb(204,204,204)">Costo</th>
                    	<th scope="col" colspan="2" style="background-color:rgb(204,204,204)">Entradas</th>
                    	<th scope="col" colspan="2" style="background-color:rgb(204,204,204)">Salidas</th>
                    	<th scope="col" colspan="2" style="background-color:rgb(204,204,204)">Saldo Final</th>
                    </tr>
                    <tr>
                        <th scope="col" style="background-color:rgb(204,204,204)"><strong>Cantidad</strong></th>
                        <th scope="col" style="background-color:rgb(204,204,204)"><strong>Costo Total $</strong></th>
                        <th scope="col" style="background-color:rgb(204,204,204)"><strong>Cantidad</strong></th>
                        <th scope="col" style="background-color:rgb(204,204,204)"><strong>Costo Total $</strong></th>
                        <th scope="col" style="background-color:rgb(204,204,204)"><strong>Existencia</strong></th>
                        <th scope="col" style="background-color:rgb(204,204,204)"><strong>Costo Total $</strong></th>
                    </tr>
                 </thead> 
                 <tbody>
        						<?php
        						if($bandera==1){
									while(odbc_fetch_row($result))
									{	
											?>
                                            <tr >
	                                                <td style="text-align:left;vertical-align:middle;"><?php echo trim(odbc_result($result, 'AARCOD')); ?></td>
                                                    <td style="text-align:left;vertical-align:middle;"><?php echo trim(odbc_result($result, 'AARDES')); ?></td>
                                                    <td style="text-align:center;vertical-align:middle;"><?php echo number_format(odbc_result($result, 'COSTO'),3,',','');?></td>	
                                                    <?php 
                                                    //Costo de Entradas
                                                    $costoEn =  odbc_result($result, 'COSTO')*odbc_result($result, 'ENTRADAS');

                                                    //Salidas restando devoluciones
                                                    $salidas = odbc_result($result, 'SALIDAS') - odbc_result($result, 'DEVOLUCIONES');

                                                    //Costo de Salidas
                                                    $costoSal = round(odbc_result($result, 'COSTO'))*$salidas;
                                                    ?>
                                                    <td style="text-align:center;vertical-align:text-top;">&nbsp;<?php echo number_format(odbc_result($result, 'ENTRADAS'),2,',','');?></td>
                                                    <td style="text-align:center;vertical-align:text-top;">&nbsp;<?php echo number_format($costoEn,3,',','');?></td>
                                                    <td style="text-align:center;vertical-align:text-top;">&nbsp;<?php echo number_format($salidas,2,',','');?></td>
                                                    <td style="text-align:center;vertical-align:text-top;">&nbsp;<?php echo number_format($costoSal,3,',','');?></td>
                                                    <?php 
                                                     //Existencia Actual
                                                     $exist = round(odbc_result($result, 'ENTRADAS')) - $salidas;

                                                     //Costo de Existencia Actual
                                                     $costoFin = $exist*number_format(odbc_result($result, 'COSTO'),2,',','');		
                                                    ?>
                                                    <td style="text-align:center;vertical-align:text-top;">&nbsp;<?php echo number_format($exist,2,',','');?></td>
                                                    <td style="text-align:center;vertical-align:text-top;">&nbsp;<?php echo number_format($costoFin,3,',','');?></td>
                                            </tr>
								<?php 
									}//fin while
								}//fin if
							    ?>
                       </tbody>                                         
                </table>
</div>

		  </div>
	  </div>
	</div>
		<!-- end #content -->
		<!-- end #sidebar -->
		<div style="clear: both;">&nbsp;</div>
	</div>
	<!-- end #page -->

	<div id="footer">
		<div id="opcionesmasusadas"><?php include("../../opcionesmasusadasphp.php"); ?></div>
	</div>
 	<?php 
		echo "<script>
				var list = [];
				var pos = 1;
				var lista = \"".$tipoInve."\";
				$('input[name=\"tinvan[]\"]').each(function() { 
					//alert($(this).val() );
					if( lista.indexOf($(this).val()) != -1 ){
						//alert('paso '+$(this).val()) ;
						 $(this).attr('checked', true); 
					}
					pos++;
				});
			  </script>";
	?>
	<!-- end #footer -->
</body>
</html>
