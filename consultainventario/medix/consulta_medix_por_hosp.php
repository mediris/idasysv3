<?php 
/*
 * Luis Ramos (lRamos)
 * 02/11/2016 
 */
session_start();
include("../../conectar.php");
$tinva = $_GET['tinvan'];
$aticod = $_GET['aticod'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
<title>Idasys V3</title>
<link href="../../<?php echo $css; ?>" rel="stylesheet" type="text/css" media="screen" />
<script language="JavaScript" src="../../javascript/javascript.js"></script>
<script language="JavaScript" src="../../calendario/javascripts.js"></script>
<script language="JavaScript" src="../javascript.js"></script>
<script language="JavaScript" src="../../javascript/jquery.js"></script>
<link href="../../shadowbox.css"  rel="stylesheet" type="text/css"/>
<script src="../../shadowbox.js" type="text/javascript"/> </script>
<script language="JavaScript" type="text/JavaScript">
Shadowbox.init({overlayOpacity: "0.5"});
</script>
<style type="text/css" title="currentStyle">
			@import "../../DataTables-1.9.4/media/css/<?php echo $demo_page;?>";
			@import "../../DataTables-1.9.4/media/css/<?php echo $demo_table;?>";
</style>
<script type="text/javascript" language="javascript" src="../../DataTables-1.9.4/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf-8">
			
			 $(document).ready(function() 
			 {
			      document.getElementById('info').style.display="";
				  $('#info').dataTable( 
				  {
					"bStateSave": true,
			        "oLanguage": 
					{
			          	"sLengthMenu": "Mostrar _MENU_ registros por Pag.",
						"sZeroRecords": "No Existen Registros",
						"sInfo": "Mostrando desde _START_ hasta _END_ de _TOTAL_ registros",
						"sInfoEmpty": "Mostrando desde 0 hasta 0 de 0 registros",
						"sInfoFiltered": "(filtrando de _MAX_ de registros)",
						"sSearch": "Buscar",
						"oPaginate": 
						{
							"sNext": "Siguiente",
							"sPrevious": "Anterior"
						}
			        }
					/*,
					"aoColumnDefs": [
						{ "bVisible": false, "aTargets": [ 0 ] }
					]*/
			      } );
			  } );
</script>

</head>
<body background="../../images/fondo idaca.jpg" >
<div id="wrapper">
  <?php include("../../superior.php");?>
  <div id="page">
     <?php include("../../validar.php");?>

	  
 		<?php 
				
				if ($bandera==1) 
				{	
					$tipoAlm = '';
					$tipoInve ='';
					if(!empty($aticod)){
						if($aticod=='01'){
							$tipoInve = '0001'; //EQUIPOS
							$tipoAlm = '0002'; //ALMACEN EQUIPOS
						} elseif($aticod=='02'){
							$tipoInve = '0002'; //CONSUMIBLES
							$tipoAlm = '0001'; //ALMACEN CONSUMIBLE BI
						} elseif($aticod=='03'){
							$tipoInve = '0002'; //CONSUMIBLES
							$tipoAlm = '0002'; //ALMACEN CONSUMIBLE NV
						} elseif($aticod=='04'){
							$tipoInve = '0003'; //REPUESTOS
							$tipoAlm = '0003'; //ALMACEN REPUESTOS
						}
					}

					$sql="SELECT T1.AARCOD, T6.AARDES, T2.ADSNRO, T3.ATSCOD, T3.ATSDES, T2.ATRFEC, (SUM(T1.ATRCAN)) AS ATRCAN, T2.ATROBS, T4.AISCOD, T4.AISDES, T5.AUNDES, T1.ACICOD, T2.ADSNRO, T1.ATSCOD 
						FROM IV36FP T1 INNER JOIN IV35FP T2 ON ( T1.ACICOD = T2.ACICOD AND T1.AALCOD = T2.AALCOD AND T1.ADPCOD = T2.ADPCOD AND T1.ATSCOD = T2.ATSCOD AND T1.ADSNRO = T2.ADSNRO ) 
							INNER JOIN IS15FP T3 ON (T1.ACICOD=T3.ACICOD AND T2.ATSCOD=T3.ATSCOD) 
							INNER JOIN IV42FP T4 ON (T2.ACICOD=T4.ACICOD AND T2.AISCOD=T4.AISCOD)
							INNER JOIN IS01FP T5 ON (T1.ACICOD=T5.ACICOD AND T1.AALCOD=T5.AALCOD) 
							INNER JOIN IV05FP T6 ON (T1.ACICOD=T6.ACICOD AND T1.AARCOD=T6.AARCOD)
						WHERE T1.ACICOD='42' AND T1.AALCOD in ('0004', '0005', '0006') AND T3.ATSCOD in ('01','02','03','04') AND T1.AARCOD IN ( SELECT T7.AARCOD FROM IV05FP T7 WHERE T7.ACICOD=T1.ACICOD AND ( T7.AARCOD IN (SELECT T8.AARCOD FROM IV40FP T8 WHERE T8.ACICOD=T1.ACICOD AND T8.AALCOD in ('$tipoAlm') GROUP BY T8.AARCOD ) )";
					if(!empty($tipoInve)){
						$sql.=" AND T7.ATICOD IN ('$tipoInve'))";
					}
					$sql.="  
						GROUP BY T1.AARCOD, T6.AARDES, T2.ADSNRO, T3.ATSCOD, T3.ATSDES, T2.ATRFEC, T2.ATROBS, T4.AISCOD, T4.AISDES, T5.AUNDES, T1.ACICOD, T2.ADSNRO, T1.ATSCOD
						ORDER BY T4.AISDES, T2.ADSNRO ";

					$result=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111"));
				}
			?>
        <div id="content3" >   
        	<table width="100%"   border="0">
              <tr>
                <td width="65%" scope="col" colspan="2"><h1  class="title">Reporte de Salidas por Hospital - MEDIX</h1>
                  <hr /></td>
                <td width="13%" scope="col"><div align="left">
                  <table width="100%"  border="0">
                    <tr>
                      <th width="30%" scope="col" >
                      <div class="flechas">
                      	<a href="exportaraexcel_medix_por_hosp_<?php echo $aticod; ?>.php?&aticod=<?php echo $aticod; ?>&stm=<?php echo $stm; ?>&tinvan=<?php echo $url222 = urlencode( serialize($tinva) );?>" target="_blank">
                        	<img src="../../images/excel.jpg" alt="" width="30" height="30" title="Exportar a Excel"/>
                        </a>
                      </div></th>
                    </tr>
                  </table>
                </div></td>
              </tr>
              <tr>
              	<td colspan="3">
                	<form name="form" id="form" method="post" on>
                    <input type="hidden" name="bandera" id="bandera" value="<?php echo $bandera;?>" />
                    <input type="hidden" name="tipoAlm" id="tipoAlm" value="<?php echo $tipoAlm;?>" />
                   <!-- <input type="hidden" name="aalcod" id="aalcod" value="0004" /> -->                    
                	<table>
                	<tr>
                        	<td>&nbsp;Tipo de Inventario:</td>
                            <td>&nbsp;<?php //echo $aticod; ?>
                            	<input type="radio" name="aticod" id="aticod0" value="01" <?php if($aticod=='01'){echo 'checked="checked"';} ?>> EQUIPOS (0001)&nbsp;
                            	<input type="radio" name="aticod" id="aticod1" value="02" <?php if($aticod=='02'){echo 'checked="checked"';} ?>> CONSUMIBLES BI (0002)&nbsp;
                            	<input type="radio" name="aticod" id="aticod2" value="03" <?php if($aticod=='03'){echo 'checked="checked"';} ?>> CONSUMIBLES NV (0002)&nbsp;
                            	<input type="radio" name="aticod" id="aticod3" value="04" <?php if($aticod=='04'){echo 'checked="checked"';} ?>> REPUESTOS (0003)&nbsp;
                            </td>
                            <td >&nbsp;<a href="javascript:busqueda6();"><img src="../../images/precesarconsulta.png" width="50" height="50" alt="Realizar Consulta" style="border:none;"></a></td>
                        </tr>
                    </table>
                    </form>
                </td>
              </tr>
            </table>   
		<div id="container">
            <div id="demo">
                <table width="100%" id="info"  border="0" style="display:none;">
                  <thead>
                    <tr>
                    	<th scope="col" style="background-color:rgb(204,204,204)"><strong>Hospital</strong></th>
                        <th scope="col" style="background-color:rgb(204,204,204)"><strong>Nro. de Entrada</strong></th>
                        <th scope="col" style="background-color:rgb(204,204,204)"><strong>Fecha</strong></th>
                        <th scope="col" style="background-color:rgb(204,204,204)"><strong>Cantidad</strong></th>
                        <th scope="col" style="background-color:rgb(204,204,204)"><strong>C&oacute;digo</strong></th>
                        <th scope="col" style="background-color:rgb(204,204,204)"><strong>Art&iacute;culo</strong></th>
                    </tr>
                 </thead> 
                 <tbody>
        						<?php
        						if($bandera==1){
									while(odbc_fetch_row($result))
									{

											?>
                                            <tr >
                                                    <td style="text-align:center;vertical-align:middle;"><?php echo odbc_result($result, 'AISDES')!=''?odbc_result($result, 'AISDES'):'&nbsp;';?></td> 
                                                    <td style="text-align:center;vertical-align:middle;"><?php echo odbc_result($result, 'ADSNRO')!=''?add_ceros(odbc_result($result, 'ADSNRO'),6):'&nbsp;'; echo "(S)";?></td>
                                                    <td style="text-align:center;vertical-align:middle;"><?php echo odbc_result($result, 'ATRFEC')!=''?formatDate(odbc_result($result, 'ATRFEC'),'aaaa-mm-dd','dd/mm/aaaa' ):'&nbsp;';?></td>
                                                    <td style="text-align:center;vertical-align:middle;"><?php echo odbc_result($result, 'ATRCAN')!=''?number_format(odbc_result($result, 'ATRCAN'),2,',','.'):'&nbsp;';?></td>
                                                    <td style="text-align:left;vertical-align:middle;"><?php echo trim(odbc_result($result, 'AARCOD')); ?></td>
                                                    <td style="text-align:left;vertical-align:middle;"><?php echo trim(odbc_result($result, 'AARDES')); ?></td>
                                            </tr>
                             <?php  }//fin while
								} ?>
                       </tbody>                                         
                </table>
</div>
		  </div>
	  </div>
	</div>
		<!-- end #content -->
		<!-- end #sidebar -->
		<div style="clear: both;">&nbsp;</div>
	</div>
	<!-- end #page -->

	<div id="footer">
		<div id="opcionesmasusadas"><?php include("../../opcionesmasusadasphp.php"); ?></div>
	</div>
 	<?php 
		echo "<script>
				var list = [];
				var pos = 1;
				var lista = \"".$tipoInve."\";
				$('input[name=\"tinvan[]\"]').each(function() { 
					//alert($(this).val() );
					if( lista.indexOf($(this).val()) != -1 ){
						//alert('paso '+$(this).val()) ;
						 $(this).attr('checked', true); 
					}
					pos++;
				});
			  </script>";
	?>
	<!-- end #footer -->
</body>
</html>
