<?php
/*
 * Luis Ramos (lRamos)
 * 10/11/2016 
 */ 
session_start();
include("../../conectar.php");
include("../../PHPExcel/PHPExcel.php");
$arqnro = trim($_GET["num"]);
$tinva = unserialize( urldecode( $_GET['tinvan'] ) ) ;
$aticod = $_GET['aticod'];
$tipoAlm = '';
$tipoInve ='';
if(!empty($aticod)){
	if($aticod=='01'){
		$tipoInve = '0001'; //EQUIPOS
		$tipoAlm = '0002'; //ALMACEN EQUIPOS
		$filename = 'Reporte_MEDIX_Costos_EQ-'.formatDate($Fechaactual,'dd.mm.aaa','aaaa_mm_dd').".xlsx";
	} elseif($aticod=='02'){
		$tipoInve = '0002'; //CONSUMIBLES
		$tipoAlm = '0001'; //ALMACEN CONSUMIBLE BI
		$filename = 'Reporte_MEDIX_Costos_CONBI-'.formatDate($Fechaactual,'dd.mm.aaa','aaaa_mm_dd').".xlsx";
	} elseif($aticod=='03'){
		$tipoInve = '0002'; //CONSUMIBLES
		$tipoAlm = '0002'; //ALMACEN CONSUMIBLE NV
		$filename = 'Reporte_MEDIX_Costos_CONNV-'.formatDate($Fechaactual,'dd.mm.aaa','aaaa_mm_dd').".xlsx";
	} elseif($aticod=='04'){
		$tipoInve = '0003'; //REPUESTOS
		$tipoAlm = '0003'; //ALMACEN REPUESTOS
		$filename = 'Reporte_MEDIX_Costos_REP-'.formatDate($Fechaactual,'dd.mm.aaa','aaaa_mm_dd').".xlsx";
	}
}
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header("Content-disposition: attachment; filename=Reporte_Costos_MEDIX_".formatDate($Fechaactual,'dd.mm.aaa','aaaa_mm_dd').".xlsx");
header('Cache-Control: max-age=0');
?>
<?php 

					$sql="SELECT T1.ATRART, T1.ATRNUM, T1.ATRCOD, T1.ATRCUT, (SUM(T1.ATRCAN)) AS ATRCAN, T2.ATRDES, T3.ATRFEC, T3.ATRDES AS T3ARTDES, T3.ATROBS, T5.AARCOD, T5.AARDES, T7.AMCDES, T8.ATIDES
						FROM IV16FP T1
							INNER JOIN IV12FP T2 ON (T1.ACICOD=T2.ACICOD AND T1.ATRCOD=T2.ATRCOD)
							INNER JOIN IV15FP T3 ON (T1.ACICOD=T3.ACICOD AND T1.ATRCOD=T3.ATRCOD AND T1.ATRNUM=T3.ATRNUM AND T3.ATRSTS='02')
							LEFT JOIN IV35FP T4 ON (T1.ACICOD=T4.ACICOD AND T1.AALCOD=T4.AALCOD AND T1.ATRCOD=T4.ATRCOD AND T1.ATRNUM=T4.ATRNUM)
							INNER JOIN IV05FP T5 ON (T1.ACICOD=T5.ACICOD AND T1.ATRART=T5.AARCOD)
							INNER JOIN IV06FP T6 ON (T1.ACICOD=T6.ACICOD AND T5.AARCOD=T6.AARCOD)
							INNER JOIN IV04FP T7 ON (T1.ACICOD=T7.ACICOD AND T6.AMCCOD=T7.AMCCOD)
							INNER JOIN IV01FP T8 ON (T5.ACICOD=T8.ACICOD AND T5.ATICOD=T8.ATICOD)
						WHERE T1.ACICOD='$Compania' AND T1.AALCOD IN ('$tipoAlm') AND T1.ATRCOD='0101' AND T5.ATICOD='$tipoInve'
						GROUP BY T1.ATRART, T1.ATRNUM, T1.ATRCOD, T1.ATRCUT, T2.ATRDES, T3.ATRFEC, T3.ATRDES, T3.ATROBS, T4.ADSNRO, T4.ATSCOD, T5.AARCOD, T5.AARDES, T7.AMCDES, T8.ATIDES
						ORDER BY T5.AARDES, T1.ATRNUM, T1.ATRART";

					//echo $sql.'<br>';
					$result = odbc_exec($cid, $sql)or die(exit("Error en odbc_exec 11111"));
					$resultrow = odbc_exec($cid, $sql)or die(exit("Error en odbc_exec 11111"));

					$excel = new PHPExcel();

						//Propiedades del Excel
						$excel->getProperties()
						   ->setCreator('IDASYS WEB')
						   ->setTitle('Reporte MEDIX / Entrada con Costos')
						   ->setLastModifiedBy('IDASYS WEB')
						   ->setDescription('A demo to show how to use PHPExcel to manipulate an Excel file')
						   ->setSubject('Reporte de Entradas con Costos')
						   ->setKeywords('excel php office phpexcel')
						   ->setCategory('inventario')
						   ;

						$exceldata = $excel->getSheet(0);
						$exceldata->setTitle('Reporte_Costos__MEDIX');

						//Titulo
						$styleArray = array(
							    'font'  => array(
							        'bold'  => true
							    ),
							    'fill' => array(
					            'type' => PHPExcel_Style_Fill::FILL_SOLID,
					            'color' => array('rgb' => 'BDBDBD')
					            ),
					            'alignment' => array(
	            				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
	            				),
	            				'borders' => array(
					            'allborders' => array(
					                'style' => PHPExcel_Style_Border::BORDER_THIN,
					            	)
					        	)
						    );

						//Head Logo
					    $objDrawing = new PHPExcel_Worksheet_Drawing();
				        $objDrawing->setName('Logo');
				        $objDrawing->setDescription('Logo');
				        if($Compania == '14'){ 
					        $objDrawing->setPath('../../images/logoidacadef2005.png');
					        $objDrawing->setHeight(70);
					        $objDrawing->setWidth(200);
					    } else {
					    	$objDrawing->setPath('../../images/MEDITRON_logo_rif.png');
					        $objDrawing->setHeight(70);
				        	$objDrawing->setWidth(330);
					    }

				        $objDrawing->setCoordinates('A1');
				        $objDrawing->setWorksheet($excel->getActiveSheet());
				        //

						$excel->getActiveSheet()->getCell('A4')->setValue('RIF: '.$Companiarif);

						//Titulo 
						$excel->getActiveSheet()->getCell('A7')->setValue('REPORTE MEDIX');
						$exceldata->mergeCells('A7:K7');
						$excel->getActiveSheet()->getStyle('A7')->getFont()->setBold(true);
						$excel->getActiveSheet()->getStyle('A7')->getFont()->setSize(25);
						$excel->getActiveSheet()->getStyle('A7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
						//

						//Tipo de Inventario	
						$sql2="SELECT T6.ATIDES
						FROM IV01FP T6 
						WHERE T6.ACICOD='$Compania'  AND T6.ATICOD in (".$tipoInve.") ";
							
						$result2=odbc_exec($cid,$sql2)or die(exit("Error en odbc_exec 11111"));
						$tInventario = ''; 
						while(odbc_fetch_row($result2))
						{
							$tInventario .= trim(odbc_result($result2,'ATIDES'));
						}

						if(empty($tInventario)){$tInventario='N/A';}

						//Sub titulo 
						$excel->getActiveSheet()->getCell('A8')->setValue('Inventario con Costos de Entrada');
						$exceldata->mergeCells('A8:K8');
						$excel->getActiveSheet()->getStyle('A8')->getFont()->setBold(true);
						$excel->getActiveSheet()->getStyle('A8')->getFont()->setSize(20);
						$excel->getActiveSheet()->getStyle('A8')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

						$excel->getActiveSheet()->getCell('A9')->setValue('Elaborado el '.$Fechaactual.' a las '.$Horaactual2);
						$exceldata->mergeCells('A9:K9');
						$excel->getActiveSheet()->getStyle('A9')->getFont()->setBold(true);
						$excel->getActiveSheet()->getStyle('A9')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

						$excel->getActiveSheet()->getCell('A10')->setValue('Almacén Principal: MEDIX');
						$exceldata->mergeCells('A10:K10');
						$excel->getActiveSheet()->getStyle('A4')->getFont()->setBold(true);
						$excel->getActiveSheet()->getStyle('A10')->getFont()->setBold(true);
						$excel->getActiveSheet()->getStyle('A10')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

						$excel->getActiveSheet()->getCell('A11')->setValue('Filtrado por: '.$tInventario);
						$exceldata->mergeCells('A11:K11');
						$excel->getActiveSheet()->getStyle('A11')->getFont()->setBold(true);
						$excel->getActiveSheet()->getStyle('A11')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

						$exceldata->setCellValue('A12', 'Código');
						$exceldata->setCellValue('B12', 'Artículo');
						$exceldata->setCellValue('C12', 'Marca');
						$exceldata->setCellValue('D12', 'Tipo Inventario');
						$exceldata->setCellValue('E12', 'Ubicación');
						$exceldata->setCellValue('F12', 'Número Referencia Entrada');
						$exceldata->mergeCells('A12:A13');
						$exceldata->mergeCells('B12:B13');
						$exceldata->mergeCells('C12:C13');
						$exceldata->mergeCells('D12:D13');
						$exceldata->mergeCells('E12:E13');
						$exceldata->mergeCells('F12:F13');
						$excel->getActiveSheet()->getStyle('A12:K12')->applyFromArray($styleArray);

						//Entradas
						$exceldata->setCellValue('G12', 'Entradas'); // Sets cell 'a1' to value 'ID
						$exceldata->mergeCells('G12:J12'); 
					    $exceldata->setCellValue('G13', 'Fecha');
					    $exceldata->setCellValue('H13', 'Cantidad');
					    //$exceldata->setCellValue('I13', 'Observaciones');
						$exceldata->setCellValue('I13', 'Descripcion');
					    $exceldata->setCellValue('J13', 'Costo Unitario $');
					    $exceldata->setCellValue('K13', 'Costo Total $');
					    $excel->getActiveSheet()->getStyle('A13:K13')->applyFromArray($styleArray);
					    				   		
					    //Genera Data
					    $pos = 14;
					    $startline = 14;
					    $count = 1;
					    $rowCount = 0;
					   	while(odbc_fetch_row($resultrow)){
					   		$rowCount++;
					    }

					    while(odbc_fetch_row($result))
						{
							//Codigo
							$exceldata->setCellValue('A' . $pos, trim(odbc_result($result, 'AARCOD'))); 

							//Articulo
							$exceldata->setCellValue('B' . $pos, utf8_encode(trim(odbc_result($result, 'AARDES'))));

							//Marca
							$cellc = odbc_result($result, 'AMCDES')!=''?odbc_result($result, 'AMCDES'):'N/A';
							$exceldata->setCellValue('C' . $pos, trim($cellc)); 

							//Tipo Inventario
							$exceldata->setCellValue('D' . $pos, trim(odbc_result($result, 'ATIDES')));

							//Ubicacion 
	                        $list = list_ubiart($cid, $Compania, '$tipoAlm', odbc_result($result, 'AARCOD'),3);
	                                            
	                        if(trim($list)!=''){
	                            $exceldata->setCellValue('E' . $pos, trim($list)); 
	                        } else { 
	                            $exceldata->setCellValue('E' . $pos, ' - Sin Ubicación'); 
	                        }

	                        //Merge Celdas a la cantidad de NumRows
	                        if($prevcod==odbc_result($result, 'AARCOD') || is_null($prevcod)){
	                        	$mergepos = $pos;
	                        } else {
	                        	$exceldata->mergeCells('A'.$startline.':A'.$mergepos);
						    	$exceldata->mergeCells('B'.$startline.':B'.$mergepos);
						    	$exceldata->mergeCells('C'.$startline.':C'.$mergepos);
						    	$exceldata->mergeCells('D'.$startline.':D'.$mergepos);
						    	$exceldata->mergeCells('E'.$startline.':E'.$mergepos);
						    	$startline = $mergepos+1;
						    	$mergepos++;
	                        }

	                        //IF Especifico para hacer merge en caso de que en la ultima linea del result sea $prevcod==odbc_result($result, 'AARCOD')
	                        if($count >= $rowCount){
	                        	$exceldata->mergeCells('A'.$startline.':A'.$mergepos);
						    	$exceldata->mergeCells('B'.$startline.':B'.$mergepos);
						    	$exceldata->mergeCells('C'.$startline.':C'.$mergepos);
						    	$exceldata->mergeCells('D'.$startline.':D'.$mergepos);
						    	$exceldata->mergeCells('E'.$startline.':E'.$mergepos);
						    	$startline = $mergepos+1;
						    	$mergepos++;
	                        }
			
							//Numero Referencia Entrada
							if(odbc_result($result, 'ATRCOD')=='0210') { 
								$cellf = odbc_result($result, 'ADSNRO')!=''?add_ceros(odbc_result($result, 'ADSNRO'),6):' ';
								$exceldata->setCellValue('F' . $pos, $cellf.'(D)');
							}else{
	                           $cellf = odbc_result($result, 'ATRNUM')!=''?add_ceros(odbc_result($result, 'ATRNUM'),6):'N/A';
	                           $exceldata->setCellValue('F' . $pos, $cellf.'(E)');
							}	

							//Fecha
							$cellg = odbc_result($result, 'ATRFEC')!=''?formatDate(odbc_result($result, 'ATRFEC'),'aaaa-mm-dd','dd/mm/aaaa' ):'--/--/----';
							$exceldata->setCellValue('G' . $pos, $cellg);

							//Cantidad
							$cellh = odbc_result($result, 'ATRCAN')!=''?odbc_result($result, 'ATRCAN'):' ';
							$exceldata->setCellValue('H' . $pos, $cellh);

							/*
							//Observaciones
							$celli = odbc_result($result, 'ATROBS')!=''?odbc_result($result, 'ATROBS'):' ';
							$exceldata->setCellValue('I' . $pos, utf8_encode($celli));
							*/
							//DESCRIPCION T3ARTDES
							$celli = odbc_result($result, 'T3ARTDES')!=''?odbc_result($result, 'T3ARTDES'):' ';
							$exceldata->setCellValue('I' . $pos, utf8_encode($celli));

							//Costo Unitario $
							$cellj = odbc_result($result, 'ATRCUT')!=''?number_format(odbc_result($result, 'ATRCUT'),2,',',''):' ';
							$exceldata->setCellValue('J' . $pos, $cellj);

							//Costo Total $
							$cellk = odbc_result($result, 'ATRCUT')!=''?number_format((odbc_result($result, 'ATRCUT')*odbc_result($result, 'ATRCAN')),2,',',''):' ';
							$exceldata->setCellValue('K' . $pos, $cellk);	

							$prevcod = odbc_result($result, 'AARCOD');
               				$pos++;
               				$count++;
						}//fin while                        

						//Obtiene la cantidad de rows del excel (-13 rows del titulo) 			
						$rowCount = $excel->getActiveSheet()->getHighestRow();
						$rowCount = $rowCount-13;

						//Bordes y ajuste de anchura para la data generada
						$lin = 14;
				   		$prevpos = $lin-2;
						$highestColumn = $excel->getActiveSheet()->getHighestColumn();
						for($col = 'A'; $col <= $highestColumn; $col++)
					    {
					    	$value = $excel->getActiveSheet()->getCell($col.$prevpos)->getValue();
				   			$width = mb_strwidth($value);
				   			if($width == 0){
				   				$prevpos = $lin-1;
				   				$value = $excel->getActiveSheet()->getCell($col.$prevpos)->getValue();
				   				$width = mb_strwidth($value);
				   			}
					    	for($i = 0; $i < $rowCount; $i++){
					    		$value1 = $excel->getActiveSheet()->getCell($col.$lin)->getValue();
				   				$width1 = mb_strwidth($value1);
								if($width1 > $width){
									$width = $width1;
								}
								$excel->getActiveSheet()->getStyle($col.$lin)->applyFromArray(array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN,))));
								$excel->getActiveSheet()->getStyle($col.$lin)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
								//Texto centrado a Columnas especificas
								$excel->getActiveSheet()->getStyle('A'.$lin)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
								$excel->getActiveSheet()->getStyle('B'.$lin)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
								$excel->getActiveSheet()->getStyle('C'.$lin)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
								$excel->getActiveSheet()->getStyle('D'.$lin)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
								$excel->getActiveSheet()->getStyle('E'.$lin)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
								$excel->getActiveSheet()->getStyle($col.$lin)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
								//Background color de celdas
								if($col == 'J' || $col == 'K' ){ 
									if($col == 'J'){
										$excel->getActiveSheet()->getStyle($col.$lin)->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => '3399FF'))));
									} elseif($col == 'K') {
										$excel->getActiveSheet()->getStyle($col.$lin)->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => 'FFFF99'))));
									}/*elseif($col == 'W') {
										$excel->getActiveSheet()->getStyle($col.$lin)->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => '00FF00'))));
									}*/
								}
								//Wrap Text para la Columna 'D'
								$excel->getActiveSheet()->getStyle($col.$lin)->getAlignment()->setWrapText(true);
								$lin++;
								$prevpos++;
							}
							$width = $width+2;
							// IF para colocar un width estatico a la Columna y se aplique el WrapText
							if($col == 'A' || $col == 'B' || $col == 'I'){
								$width = 45;
								$excel->getActiveSheet()->getColumnDimension($col)->setAutoSize(false);
								$excel->getActiveSheet()->getColumnDimension($col)->setWidth($width);
							} else {
								$excel->getActiveSheet()->getColumnDimension($col)->setAutoSize(false);
								$excel->getActiveSheet()->getColumnDimension($col)->setWidth($width);
							}
							$width = 0;
							$lin = 14;
							$prevpos = $lin-2;
						}
						//Ocultar Columnas
						/*$excel->getActiveSheet()->getColumnDimension('D')->setVisible(false);
						$excel->getActiveSheet()->getColumnDimension('K')->setVisible(false);
						$excel->getActiveSheet()->getColumnDimension('U')->setVisible(false);*/

						//
						//Se genera el excel
				        $writer = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
						$writer->save('php://output');
						exit();
						?>