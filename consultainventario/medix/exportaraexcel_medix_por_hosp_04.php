<?php
/*
 * Luis Ramos (lRamos)
 * 21/11/2016 
 */ 
session_start();
include("../../conectar.php");
include("../../PHPExcel/PHPExcel.php");
$arqnro = trim($_GET["num"]);
$tinva = unserialize( urldecode( $_GET['tinvan'] ) ) ;
$aticod = $_GET['aticod'];
$tipoAlm = '';
$tipoInve ='';
if(!empty($aticod)){
	if($aticod=='01'){
		$tipoInve = '0001'; //EQUIPOS
		$tipoAlm = '0002'; //ALMACEN EQUIPOS
		$filename = 'Reporte_MEDIX_Hosp_EQ-'.formatDate($Fechaactual,'dd.mm.aaa','aaaa_mm_dd').".xlsx";
	} elseif($aticod=='02'){
		$tipoInve = '0002'; //CONSUMIBLES
		$tipoAlm = '0001'; //ALMACEN CONSUMIBLE BI
		$filename = 'Reporte_MEDIX_Hosp_CONS-BI-'.formatDate($Fechaactual,'dd.mm.aaa','aaaa_mm_dd').".xlsx";
	} elseif($aticod=='03'){
		$tipoInve = '0002'; //CONSUMIBLES
		$tipoAlm = '0002'; //ALMACEN CONSUMIBLE NV
		$filename = 'Reporte_MEDIX_Hosp_CONS-NV-'.formatDate($Fechaactual,'dd.mm.aaa','aaaa_mm_dd').".xlsx";
	} elseif($aticod=='04'){
		$tipoInve = '0003'; //REPUESTOS
		$tipoAlm = '0003'; //ALMACEN REPUESTOS
		$filename = 'Reporte_MEDIX_Hosp_REP-'.formatDate($Fechaactual,'dd.mm.aaa','aaaa_mm_dd').".xlsx";
	}
}
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header("Content-disposition: attachment; filename=".$filename."");
header('Cache-Control: max-age=0');
?>
<?php 
			$sql = "SELECT T1.AARCOD, T6.AARDES, T2.ADSNRO, T3.ATSCOD, T3.ATSDES, T2.ATRFEC, (SUM(T1.ATRCAN)) AS ATRCAN, T2.ATROBS, T4.AISCOD, T4.AISDES, T5.AUNDES, T1.ACICOD, T2.ADSNRO, T1.ATSCOD, 
				(SELECT T6.AAPVLA FROM IV46FP T6 WHERE T6.ACICOD=T1.ACICOD and T6.ADSNRO=T2.ADSNRO AND T6.ATSCOD=T1.ATSCOD AND T6.APACOD IN ('2002') ) AS N2002,
				(SELECT T6.AAPVLA FROM IV46FP T6 WHERE T6.ACICOD=T1.ACICOD and T6.ADSNRO=T2.ADSNRO AND T6.ATSCOD=T1.ATSCOD AND T6.APACOD IN ('2003') ) AS N2003,
				(SELECT T6.AAPVLA FROM IV46FP T6 WHERE T6.ACICOD=T1.ACICOD and T6.ADSNRO=T2.ADSNRO AND T6.ATSCOD=T1.ATSCOD AND T6.APACOD IN ('2004') ) AS N2004,
				(SELECT T6.AAPVLA FROM IV46FP T6 WHERE T6.ACICOD=T1.ACICOD and T6.ADSNRO=T2.ADSNRO AND T6.ATSCOD=T1.ATSCOD AND T6.APACOD IN ('2005') ) AS N2005,
				(SELECT T6.AAPVLA FROM IV46FP T6 WHERE T6.ACICOD=T1.ACICOD and T6.ADSNRO=T2.ADSNRO AND T6.ATSCOD=T1.ATSCOD AND T6.APACOD IN ('2006') ) AS N2006,
				(SELECT T6.AAPVLA FROM IV46FP T6 WHERE T6.ACICOD=T1.ACICOD and T6.ADSNRO=T2.ADSNRO AND T6.ATSCOD=T1.ATSCOD AND T6.APACOD IN ('2007') ) AS N2007,
				(SELECT T6.AAPVLA FROM IV46FP T6 WHERE T6.ACICOD=T1.ACICOD and T6.ADSNRO=T2.ADSNRO AND T6.ATSCOD=T1.ATSCOD AND T6.APACOD IN ('2008') ) AS N2008,
				(SELECT T6.AAPVLA FROM IV46FP T6 WHERE T6.ACICOD=T1.ACICOD and T6.ADSNRO=T2.ADSNRO AND T6.ATSCOD=T1.ATSCOD AND T6.APACOD IN ('2009') ) AS N2009,
				(SELECT T6.AAPVLA FROM IV46FP T6 WHERE T6.ACICOD=T1.ACICOD and T6.ADSNRO=T2.ADSNRO AND T6.ATSCOD=T1.ATSCOD AND T6.APACOD IN ('2010') ) AS N2010,
				(SELECT T6.AAPVLA FROM IV46FP T6 WHERE T6.ACICOD=T1.ACICOD and T6.ADSNRO=T2.ADSNRO AND T6.ATSCOD=T1.ATSCOD AND T6.APACOD IN ('2011') ) AS N2011,
				( SELECT TRIM(PRAPIN) || ' ' || TRIM(SEAPIN) || ' ' || TRIM(NOMBIN) FROM INTRAMED.INEMP WHERE CIASIN='01' AND CEDUIN=(SELECT T6.AAPVLA FROM IV46FP T6 WHERE T6.ACICOD=T1.ACICOD and T6.ADSNRO=T2.ADSNRO AND T6.ATSCOD=T1.ATSCOD AND T6.APACOD IN (2012)) and STTRIN ='A' ) AS N2012, 
				(SELECT T6.AAPVLA FROM IV46FP T6 WHERE T6.ACICOD=T1.ACICOD and T6.ADSNRO=T2.ADSNRO AND T6.ATSCOD=T1.ATSCOD AND T6.APACOD IN ('2016') ) AS N2016,
				( SELECT TRIM(APRDES) FROM IS21FP WHERE ACICOD='42' AND APRCOD=( SELECT T6.AAPVLA FROM IV46FP T6 WHERE T6.ACICOD=T1.ACICOD and T6.ADSNRO=T2.ADSNRO AND T6.ATSCOD=T1.ATSCOD AND T6.APACOD='2017') ) AS N2017,
				(SELECT T6.AAPVLA FROM IV46FP T6 WHERE T6.ACICOD=T1.ACICOD and T6.ADSNRO=T2.ADSNRO AND T6.ATSCOD=T1.ATSCOD AND T6.APACOD IN ('2018') ) AS N2018,
				(SELECT T6.AAPVLA FROM IV46FP T6 WHERE T6.ACICOD=T1.ACICOD and T6.ADSNRO=T2.ADSNRO AND T6.ATSCOD=T1.ATSCOD AND T6.APACOD IN ('2020') ) AS N2020,
				(SELECT T6.AAPVLA FROM IV46FP T6 WHERE T6.ACICOD=T1.ACICOD and T6.ADSNRO=T2.ADSNRO AND T6.ATSCOD=T1.ATSCOD AND T6.APACOD IN ('2021') ) AS N2021,
				(SELECT T6.AAPVLA FROM IV46FP T6 WHERE T6.ACICOD=T1.ACICOD and T6.ADSNRO=T2.ADSNRO AND T6.ATSCOD=T1.ATSCOD AND T6.APACOD IN ('2022') ) AS N2022,
				( SELECT AITCOD FROM IS22FP WHERE ACICOD='42' AND AITCOD=(SELECT T6.AAPVLA FROM IV17FP T6 WHERE T6.ACICOD=42 AND T6.AARCOD=T1.AARCOD AND T6.APACOD='1204') ) AS N2023, 
				( SELECT AITDES FROM IS22FP WHERE ACICOD='42' AND AITCOD=(SELECT T6.AAPVLA FROM IV17FP T6 WHERE T6.ACICOD=42 AND T6.AARCOD=T1.AARCOD AND T6.APACOD='1204') ) AS N2024
				 FROM IV36FP T1 INNER JOIN IV35FP T2 ON ( T1.ACICOD = T2.ACICOD AND T1.AALCOD = T2.AALCOD AND T1.ADPCOD = T2.ADPCOD AND T1.ATSCOD = T2.ATSCOD AND T1.ADSNRO = T2.ADSNRO ) 
					INNER JOIN IS15FP T3 ON (T1.ACICOD=T3.ACICOD AND T2.ATSCOD=T3.ATSCOD) 
					INNER JOIN IV42FP T4 ON (T2.ACICOD=T4.ACICOD AND T2.AISCOD=T4.AISCOD)
					INNER JOIN IS01FP T5 ON (T1.ACICOD=T5.ACICOD AND T1.AALCOD=T5.AALCOD) 
					INNER JOIN IV05FP T6 ON (T1.ACICOD=T6.ACICOD AND T1.AARCOD=T6.AARCOD)
					WHERE T1.ACICOD='42' AND T1.AALCOD in ('0004', '0005', '0006') AND T3.ATSCOD in ('01','02','03','04') AND T1.AARCOD IN ( SELECT T7.AARCOD FROM IV05FP T7 WHERE T7.ACICOD=T1.ACICOD AND ( T7.AARCOD IN (SELECT T8.AARCOD FROM IV40FP T8 WHERE T8.ACICOD=T1.ACICOD AND T8.AALCOD in ('$tipoAlm') GROUP BY T8.AARCOD ) ) AND T7.ATICOD IN ('$tipoInve')
					)GROUP BY T1.AARCOD, T6.AARDES, T2.ADSNRO, T3.ATSCOD, T3.ATSDES, T2.ATRFEC, T2.ATROBS, T4.AISCOD, T4.AISDES, T5.AUNDES, T1.ACICOD, T2.ADSNRO, T1.ATSCOD
					ORDER BY T4.AISDES, T2.ADSNRO";

					//echo $sql.'<br>';

					$result = odbc_exec($cid, $sql)or die(exit("Error en odbc_exec 11111"));

					$excel = new PHPExcel();

						//Propiedades del Excel
						$excel->getProperties()
						   ->setCreator('IDASYS WEB')
						   ->setTitle('Reporte MEDIX / Entrada y Salida')
						   ->setLastModifiedBy('IDASYS WEB')
						   ->setDescription('A demo to show how to use PHPExcel to manipulate an Excel file')
						   ->setSubject('Reporte de Entrada y Salida MEDIX')
						   ->setKeywords('excel php office phpexcel')
						   ->setCategory('inventario')
						   ;

						$exceldata = $excel->getSheet(0);
						$exceldata->setTitle('Reporte_MEDIX_Hosp_REPUESTOS');

						//Titulo
						$styleArray = array(
							    'font'  => array(
							        'bold'  => true
							    ),
							    'fill' => array(
					            'type' => PHPExcel_Style_Fill::FILL_SOLID,
					            'color' => array('rgb' => 'BDBDBD')
					            ),
					            'alignment' => array(
	            				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
	            				),
	            				'borders' => array(
					            'allborders' => array(
					                'style' => PHPExcel_Style_Border::BORDER_THIN,
					            	)
					        	)
						    );

						//Head Logo
					    $objDrawing = new PHPExcel_Worksheet_Drawing();
				        $objDrawing->setName('Logo');
				        $objDrawing->setDescription('Logo');
				        if($Compania == '14'){ 
					        $objDrawing->setPath('../../images/logoidacadef2005.png');
					        $objDrawing->setHeight(70);
					        $objDrawing->setWidth(200);
					    } else {
					    	$objDrawing->setPath('../../images/MEDITRON_logo_rif.png');
					        $objDrawing->setHeight(70);
				        	$objDrawing->setWidth(330);
					    }

				        $objDrawing->setCoordinates('A1');
				        $objDrawing->setWorksheet($excel->getActiveSheet());
				        //

						$excel->getActiveSheet()->getCell('A4')->setValue('RIF: '.$Companiarif);

						//Titulo 
						$excel->getActiveSheet()->getCell('A7')->setValue('REPORTE MEDIX');
						$exceldata->mergeCells('A7:M7');
						$excel->getActiveSheet()->getStyle('A7')->getFont()->setBold(true);
						$excel->getActiveSheet()->getStyle('A7')->getFont()->setSize(25);
						$excel->getActiveSheet()->getStyle('A7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
						//

						//Tipo de Inventario	
						$sql2="SELECT T6.ATIDES
						FROM IV01FP T6 
						WHERE T6.ACICOD='$Compania'  AND T6.ATICOD in (".$tipoInve.") ";
							
						$result2=odbc_exec($cid,$sql2)or die(exit("Error en odbc_exec 11111"));
						$tInventario = ''; 
						while(odbc_fetch_row($result2))
						{
							$tInventario .= trim(odbc_result($result2,'ATIDES'));
						}

						if(empty($tInventario)){$tInventario='N/A';}

						//Sub titulo 
						$excel->getActiveSheet()->getCell('A8')->setValue('Salidas por Hospitales');
						$exceldata->mergeCells('A8:M8');
						$excel->getActiveSheet()->getStyle('A8')->getFont()->setBold(true);
						$excel->getActiveSheet()->getStyle('A8')->getFont()->setSize(20);
						$excel->getActiveSheet()->getStyle('A8')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

						$excel->getActiveSheet()->getCell('A9')->setValue('Elaborado el '.$Fechaactual.' a las '.$Horaactual2);
						$exceldata->mergeCells('A9:M9');
						$excel->getActiveSheet()->getStyle('A9')->getFont()->setBold(true);
						$excel->getActiveSheet()->getStyle('A9')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

						$excel->getActiveSheet()->getCell('A10')->setValue('Almacén Principal: MEDIX');
						$exceldata->mergeCells('A10:M10');
						$excel->getActiveSheet()->getStyle('A4')->getFont()->setBold(true);
						$excel->getActiveSheet()->getStyle('A10')->getFont()->setBold(true);
						$excel->getActiveSheet()->getStyle('A10')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

						$excel->getActiveSheet()->getCell('A11')->setValue('Filtrado por: '.$tInventario);
						$exceldata->mergeCells('A11:M11');
						$excel->getActiveSheet()->getStyle('A11')->getFont()->setBold(true);
						$excel->getActiveSheet()->getStyle('A11')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

						//Salidas
						$exceldata->setCellValue('A12', 'Hospital');
						$exceldata->setCellValue('B12', 'Fecha');
						$exceldata->setCellValue('C12', 'Número Referencia de Salida');
					    $exceldata->setCellValue('D12', 'Cantidad');
					    $exceldata->setCellValue('E12', 'Código Artículo');
					    $exceldata->setCellValue('F12', 'Artículo');
					    $exceldata->setCellValue('G12', 'Instalado en el Equipo');
					    $exceldata->setCellValue('H12', 'Serial');
					    $exceldata->setCellValue('I12', 'Técnico Responsable');
					    $exceldata->setCellValue('J12', 'Número de SISCON-MED');
					    $exceldata->setCellValue('K12', 'Hoja de Servicio');
					    $exceldata->setCellValue('L12', 'Fecha Hoja de Servicio');
					    $exceldata->setCellValue('M12', 'Nro. Pedido');
						$excel->getActiveSheet()->getStyle('A12:M12')->applyFromArray($styleArray);
					    			
					    //Genera Data
					    $pos = 13;
					    while(odbc_fetch_row($result))
									{
											//Codigo
											/*$exceldata->setCellValue('A' . $pos, trim($paginat[$g]["AARCOD"])); 

											//Articulo
											$exceldata->setCellValue('B' . $pos, utf8_encode(trim($paginat[$g]["AARDES"])));

											//Marca
											$cellc = $paginat[$g]['AMCDES']!=''?$paginat[$g]['AMCDES']:'N/A';
											$exceldata->setCellValue('C' . $pos, trim($cellc)); 

											//Tipo Inventario
											$exceldata->setCellValue('D' . $pos, trim($paginat[$g]["ATIDES"]));

											//Ubicacion 
                                            $list = list_ubiart($cid, $Compania, '0002', $paginat[$g]["AARCOD"],3);
                                                                
                                            if(trim($list)!=''){
                                                $exceldata->setCellValue('E' . $pos, trim($list)); 
                                            } else { 
                                                $exceldata->setCellValue('E' . $pos, ' - Sin Ubicación'); 
                                            }

                                            //Merge Celdas a la cantidad de NumRows
                                           if($numRows > 1)
											{
												$lineas = $pos+$numRows-1;
												$exceldata->mergeCells('A'.$pos.':A'.$lineas);
										    	$exceldata->mergeCells('B'.$pos.':B'.$lineas);
										    	$exceldata->mergeCells('C'.$pos.':C'.$lineas);
										    	$exceldata->mergeCells('D'.$pos.':D'.$lineas);
										    	$exceldata->mergeCells('E'.$pos.':E'.$lineas);
										    	$exceldata->mergeCells('V'.$pos.':V'.$lineas);
											}*/

											
													//Numero Referencia Entrada
													/*if(odbc_result($result2, 'ATRCOD')=='0210') { 
														$cellf = odbc_result($result2, 'ADSNRO')!=''?add_ceros(odbc_result($result2, 'ADSNRO'),6):' ';
														$exceldata->setCellValue('F' . $pos, $cellf.'(D)');
													}else{
                                                       $cellf = odbc_result($result2, 'ATRNUM')!=''?add_ceros(odbc_result($result2, 'ATRNUM'),6):'N/A';
                                                       $exceldata->setCellValue('F' . $pos, $cellf.'(E)');
													}	

														//Fecha
														$cellg = odbc_result($result2, 'ATRFEC')!=''?formatDate(odbc_result($result2, 'ATRFEC'),'aaaa-mm-dd','dd/mm/aaaa' ):'--/--/----';
														$exceldata->setCellValue('G' . $pos, $cellg);

														//Cantidad
														$cellh = odbc_result($result2, 'ATRCAN')!=''?odbc_result($result2, 'ATRCAN'):' ';
														$exceldata->setCellValue('H' . $pos, $cellh);

														//Observaciones
														$celli = odbc_result($result2, 'ATROBS')!=''?odbc_result($result2, 'ATROBS'):'';
														$exceldata->setCellValue('I' . $pos, utf8_encode($celli));	*/

                                                    //Hospital
                                                   $exceldata->setCellValue('A' . $pos, 'HOSPITAL '.utf8_encode(trim(odbc_result($result, 'AISDES'))));

                                                   //Fecha
                                                   $cellb = odbc_result($result, 'ATRFEC')!=''?formatDate(odbc_result($result, 'ATRFEC'),'aaaa-mm-dd','dd/mm/aaaa' ):'&nbsp;';
                                                   $exceldata->setCellValue('B' . $pos, $cellb);

                                                   //Numero Referencia Salidas         
                                                   $cellc = odbc_result($result, 'ADSNRO')!=''?add_ceros(odbc_result($result, 'ADSNRO'),6):' ';
                                                   $exceldata->setCellValue('C' . $pos, $cellc.'(S)');

                                                   //Cantidad
                                                   $celld = odbc_result($result, 'ATRCAN')!=''?odbc_result($result, 'ATRCAN'):' ';
                                                   $exceldata->setCellValue('D' . $pos, $celld);

                                                   /*//Tipo de Salida       
                                                   $exceldata->setCellValue('K' . $pos, trim(odbc_result($result, 'ATSDES')));

                                                   //Almacen         
                                                   $exceldata->setCellValue('L' . $pos, utf8_encode(trim(odbc_result($result, 'AUNDES'))));*/

                                                   //Codigo
													$exceldata->setCellValue('E' . $pos, trim(odbc_result($result, 'AARCOD'))); 

													//Articulo
													$exceldata->setCellValue('F' . $pos, utf8_encode(trim(odbc_result($result, 'AARDES'))));

                                                   //Instalado en el Equipo
                                                   /*$cellq =  odbc_result($result, 'N2017')!=''?odbc_result($result, 'N2017'):'';
                                                   $exceldata->setCellValue('Q' . $pos, utf8_encode($cellq));

                                                   //Serial
                                                   $cellr =  odbc_result($result, 'N2018')!=''?odbc_result($result, 'N2003'):'';
                                                   $exceldata->setCellValue('R' . $pos, utf8_encode($cellr));*/

                                                   //Instalado en el Equipo
                                                   $cellg =  odbc_result($result, 'N2017')!=''?odbc_result($result, 'N2017'):'';
                                                   $exceldata->setCellValue('G' . $pos, utf8_encode($cellg));

                                                   //Serial
                                                   $cellh =  odbc_result($result, 'N2018')!=''?odbc_result($result, 'N2018'):'';
                                                   $exceldata->setCellValue('H' . $pos, utf8_encode($cellh));

                                                   //Tecnico Responsable
                                                   $celli =  odbc_result($result, 'N2012')!=''?odbc_result($result, 'N2012'):'';
                                                   $exceldata->setCellValue('I' . $pos, utf8_encode($celli)); 

                                                   //Numero de SISCON-MED
                                                   $cellj =  odbc_result($result, 'N2016')!=''?odbc_result($result, 'N2016'):'';
                                                   $exceldata->setCellValue('J' . $pos, utf8_encode($cellj));

                                                   //Hoja de Servicio
                                                   $cellk =  odbc_result($result, 'N2020')!=''?odbc_result($result, 'N2020'):'';
                                                   $exceldata->setCellValue('K' . $pos, utf8_encode($cellk));

                                                   //Fecha Hoja de Servicio
                                                   $fecHojaServ = new DateTime(odbc_result($result, 'N2021'));
                                                   $celll =  $fecHojaServ!=''?$fecHojaServ->format('d/m/Y'):'';
                                                   $exceldata->setCellValue('L' . $pos, utf8_encode($celll));

                                                   //Nro. Pedido
                                                   $cellm =  odbc_result($result, 'N2022')!=''?trim(odbc_result($result, 'N2022')):'';
                                                   $exceldata->setCellValue('M' . $pos, utf8_encode($cellm));
	                               				$pos++;
											}//fin while

						//Obtiene la cantidad de rows del excel (-13 rows del titulo) 			
						$rowCount = $excel->getActiveSheet()->getHighestRow();
						$rowCount = $rowCount-12;

						//Bordes y ajuste de anchura para la data generada
						$lin = 13;
				   		$prevpos = $lin-1;
						$highestColumn = $excel->getActiveSheet()->getHighestColumn();
						for($col = 'A'; $col <= $highestColumn; $col++)
					    {
					    	$value = $excel->getActiveSheet()->getCell($col.$prevpos)->getValue();
				   			$width = mb_strwidth($value);
				   			if($width == 0){
				   				$prevpos = $lin-1;
				   				$value = $excel->getActiveSheet()->getCell($col.$prevpos)->getValue();
				   				$width = mb_strwidth($value);
				   			}
					    	for($i = 0; $i < $rowCount; $i++){
					    		$value1 = $excel->getActiveSheet()->getCell($col.$lin)->getValue();
				   				$width1 = mb_strwidth($value1);
								if($width1 > $width){
									$width = $width1;
								}
								$excel->getActiveSheet()->getStyle($col.$lin)->applyFromArray(array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN,))));
								$excel->getActiveSheet()->getStyle($col.$lin)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
								//Texto centrado a Columnas especificas
								$excel->getActiveSheet()->getStyle('A'.$lin)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
								$excel->getActiveSheet()->getStyle('E'.$lin)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
								$excel->getActiveSheet()->getStyle('F'.$lin)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
								$excel->getActiveSheet()->getStyle($col.$lin)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
								//Background color de celdas
								/*if($col == 'H' || $col == 'N' || $col == 'V'){ 
									if($col == 'H'){
										$excel->getActiveSheet()->getStyle($col.$lin)->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => '3399FF'))));
									} elseif($col == 'N') {
										$excel->getActiveSheet()->getStyle($col.$lin)->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => 'FFFF99'))));
									} elseif($col == 'V') {
										$excel->getActiveSheet()->getStyle($col.$lin)->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => '00FF00'))));
									}
								}*/
								//Wrap Text para la Columna 'D'
								$excel->getActiveSheet()->getStyle($col.$lin)->getAlignment()->setWrapText(true);
								$lin++;
								$prevpos++;
							}
							$width = $width+2;
							// IF para colocar un width estatico a la Columna y se aplique el WrapText
							if($col == 'F' || $col == 'H' || $col == 'I' || $col == 'K'){
								$width = 45;
								$excel->getActiveSheet()->getColumnDimension($col)->setAutoSize(false);
								$excel->getActiveSheet()->getColumnDimension($col)->setWidth($width);
							} else {
								$excel->getActiveSheet()->getColumnDimension($col)->setAutoSize(false);
								$excel->getActiveSheet()->getColumnDimension($col)->setWidth($width);
							}
							$width = 0;
							$lin = 13;
							$prevpos = $lin-1;
						}

						//Ocultar Columnas
						/*$excel->getActiveSheet()->getColumnDimension('D')->setVisible(false);
						$excel->getActiveSheet()->getColumnDimension('K')->setVisible(false);
						$excel->getActiveSheet()->getColumnDimension('S')->setVisible(false);*/

						//
						//Se genera el excel
				        $writer = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
						$writer->save('php://output');
						exit();
						?>