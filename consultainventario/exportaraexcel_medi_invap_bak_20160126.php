<?php session_start();
include("../conectar.php");
$arqnro = trim($_GET["num"]);
$tinva = unserialize( urldecode( $_GET['tinvan'] ) ) ;
//header("Pragma: ");
header("Pragma: no-cache");
header('Cache-control: ');
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Content-type: application/vnd.ms-excel");
//header("Content-type: application/octet-stream");
header("Content-disposition: attachment; filename=Reporte_MEDI_INVAP_".formatDate($Fechaactual,'dd.mm.aaa','aaaa_mm_dd').".xls");

?>
 

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Reporte Cv</title>
</head>
<style>

h1, h2, h3, h4, h5 {
	margin: 0;
	padding: 0;
	font-weight: normal;
	color: #32639A;
}

h1 {
	font-size: 2em;
}

h2 {
	font-size: 2.4em;	
}

h3 {
	font-size: 1.6em;
	font-style: italic;
}
h4 {
	font-size: 1.6em;
	font-style: italic;
	color: #FFF;
}
h5 {
	font-size: 1.0em;
	font-style: italic;
	color: #666;
}

#background-image
{
	font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
	font-size: 10px;
	margin: 0px;
	width: 100%;
	text-align: left;
	border-collapse: collapse;
}
#background-image th
{
	padding: 12px;
	font-weight: normal;
	font-size: 12px;
	color: #339;
	border-bottom-style: solid;
	border-left-style: none;
	text-align: center;
}
#background-image td
{
	color: #669;
	border-top: 1px solid #fff;
	padding-right: 4px;
	padding-left: 4px;
}
#background-image tfoot td
{
	font-size: 9px;
}
#background-image tbody 
{

	background-repeat: no-repeat;
	background-position: left top;
}
#background-image tbody td
{
	background-image: url(images/backn.png);
}
* html #background-image tbody td
{
	/* 
	   ----------------------------
		PUT THIS ON IE6 ONLY STYLE 
		AS THE RULE INVALIDATES
		YOUR STYLESHEET
	   ----------------------------
	*/
	filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='images/backn.png',sizingMethod='crop');
	background: none;
}	
</style>
<body>
<?php 
				
				
		 		$wsolicitud=0;
				if ($solicitudpagina==0) 	
				{
						
						$z=0;
						$tothon=0;
						$totest=0;
						$lin=1;
						$limitep=$_SESSION['solicitudlineasporpaginat'];
						$pag=1;
						$primero='S';
						
						$tipoInve ='';
						if(!empty($tinva)){
							foreach($tinva as $key => $value){
								$tipoInve .= "'".$value."',";
							}
							$tipoInve = substr($tipoInve,0, (strripos($tipoInve,",")));
						}
						
						$sql="SELECT T3.AARUMB, T4.AUMDES, T1.ACICOD, T1.AARCOD, T1.AARNIV, T1.ATICOD, T1.ASICOD, T1.AARDES, T1.AARTDT, T1.AARSTS, T1.ATICOD, T6.ATIDES,
						(SELECT T6.ASLSAF FROM IV40FP T6 WHERE T6.ACICOD=T1.ACICOD AND T6.AARCOD=T1.AARCOD AND T6.AALCOD='0001' ORDER BY T6.ASLFEF DESC FETCH FIRST 1 ROWS ONLY) AS ASLSAF
							FROM IV05FP T1 
								INNER JOIN IV06FP T3 ON ( T1.ACICOD=T3.ACICOD AND T1.AARCOD=T3.AARCOD ) 
								INNER JOIN IV13FP T4 ON ( T1.ACICOD=T4.ACICOD AND T3.AARUMB=T4.AUMCOD ) 
								INNER JOIN IV01FP T6 ON (T1.ACICOD=T6.ACICOD  AND T1.ATICOD=T6.ATICOD)
							WHERE T1.ACICOD='$Compania' AND 
								( T1.AARCOD IN (SELECT T2.AARCOD FROM IV40FP T2 WHERE T2.ACICOD=T1.ACICOD AND T2.AALCOD='0001' GROUP BY T2.AARCOD ORDER BY T2.AARCOD ) ) 
								 ";
						if(!empty($tipoInve)){
							$sql.="  AND T1.ATICOD IN (".$tipoInve.")";
						}
						$sql.="  
							ORDER BY T1.AARDES";
						
						//echo $sql."<br/><br/>";
						$resultt=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111"));
						
							while(odbc_fetch_row($resultt))
							{
								$jml = odbc_num_fields($resultt);
								$row[$z]["pagina"] =  $pag;
								for($i=1;$i<=$jml;$i++)
								{	
									$row[$z][odbc_field_name($resultt,$i)] =  odbc_result($resultt,$i);
								}
								$z++;
								if ($lin>=$limitep) 
								{
									$limitep+=$_SESSION['solicitudlineasporpaginat'];
									$pag++;
								}
								$lin++;
							}

						$totsol=($lin-1);
						$_SESSION['totalsolicitudes']=$totsol;
						$_SESSION['solicitudarreglo']=$row;
						$solicitudpagina=1;
						$_SESSION['solicitudpaginas']=$pag;
					}//fin de solicitudpagina
					/*se muestra la cantidad de elementos segun la solicitada en $solicitudpagina*/
					$paginat=$_SESSION['solicitudarreglo'];
			?>      
<table width="100%" border="0">
<tr>
<td height="89"><h1>
<?php if($Compania=='14'){?>
	<img src="http://<?php echo $Direccionip; ?>/idasysv3/images/logoidacadef2005.png" width="280" height="68" />
<?php }else if($Compania=='40'){?>
	<img src="http://<?php echo $Direccionip; ?>/idasysv3/images/MEDITRON_logo_rif.png" width="300" />
<?php }else{ ?>
	<img src="http://<?php echo $Direccionip; ?>/idasysv3/images/logomeditronnuevo.png" width="280" height="68" />
<?php } ?>
   </h1>
  <h5>RIF:  <?php echo $Companiarif; ?></h5></td>
</tr>
<tr>
<td>

<table width="100%" id="background-image" >
	
  <thead>
  	<tr>
        <th colspan="17" scope="col"><h1>REPORTE - MEDI / INVAP</h1></th>
    </tr>
  	<tr>
        <th colspan="17" scope="col"><h2>Entrada y Salida de Repuesto y Equipos</h2></th>
    </tr>
    <tr>
        <th colspan="17" scope="col">Elaborado el <?php echo $Fechaactual; ?> a las <?php echo $Horaactual2; ?></th>
    </tr>
  	<tr>
        <th colspan="17" scope="col">Almac�n Principal: INVAP</th>
    </tr>
    <?php if(!empty($tinva)){ ?>
        <tr>
            <th colspan="17" scope="col">
                <strong>Filtrado por:</strong>
                <?php 
							$tipoInve ='';
							if(!empty($tinva)){
								foreach($tinva as $key => $value){
									$tipoInve .= "'".$value."',";
								}
								$tipoInve = substr($tipoInve,0, (strripos($tipoInve,",")));
							}
							
							$sql2="SELECT T6.ATIDES
							FROM IV01FP T6 
							WHERE T6.ACICOD='$Compania'  AND T6.ATICOD in (".$tipoInve.") ";
							
							$result2=odbc_exec($cid,$sql2)or die(exit("Error en odbc_exec 11111")); 
							while(odbc_fetch_row($result2))
							{
								echo odbc_result($result2,'ATIDES').", ";
							}
				
				;?>
            </th>
        </tr>
    <?php } ?>

    				<tr style="border-bottom:solid;">
                    	<th scope="col" rowspan="2" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)">C�digo</th>
                   		<th scope="col" rowspan="2" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)">Art�culo</th>
                        <th scope="col" rowspan="2" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)">Tipo Inventario</th>
                        <th scope="col" rowspan="2" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)">Ubicaci�n</th>
                        <th scope="col" rowspan="2" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)"><strong>N�mero Referencia Entrada</strong></th>
                        <th scope="col" rowspan="2" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)"><strong>N�mero Gu�a</strong></th>
                    	<th scope="col" colspan="3" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204); font-size:medium;">Entradas</th>
                        <th scope="col" rowspan="2" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)"><strong>N�mero Referencia Salidas</strong></th>
                        <th scope="col" colspan="10" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204); font-size:medium;">Salidas</th>
                        <th scope="col" rowspan="2" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;border-right:solid;background-color:rgb(204,204,204)">Existencia Actual</th>
                    </tr>
                    <tr>
                        <!--<th scope="col" style="border-width:thin;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)"><strong>Nro. de Entrada</strong></th>-->
                        <!--<th scope="col" style="border-bottom:solid;"><strong>Tipo</strong></th>-->
                        <th scope="col" style="border-width:thin;border-bottom:solid;border-left:solid;solid;background-color:rgb(204,204,204)"><strong>Fecha</strong></th>
                        <th scope="col" style="border-width:thin;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)"><strong>Cantidad</strong></th>
                        <!--<th scope="col" style="border-width:thin;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)"><strong>Descripci�n</strong></th>-->
                        <th scope="col" style="border-width:thin;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)"><strong>Observaciones</strong></th>
                        
                        <!--<th scope="col" style="border-width:thin;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)"><strong>Nro. de Salida</strong></th>-->
                        <!--<th scope="col" style="border-bottom:solid;"><strong>Tipo</strong></th>-->
                        <th scope="col" style="border-width:thin;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)"><strong>Fecha</strong></th>
                        <th scope="col" style="border-width:thin;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)"><strong>Cantidad</strong></th>
                        <th scope="col" style="border-width:thin;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)"><strong>Destino</strong></th>
                        <th scope="col" style="border-width:thin;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)"><strong>Observaciones</strong></th>
                        <th scope="col" style="border-width:thin;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)"><strong>T�cnico</strong></th>
                        <th scope="col" style="border-width:thin;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)"><strong>Nro. Hoja de Servicio</strong></th>
                        
                        <th scope="col" style="border-width:thin;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)"><strong>Fecha de Recepci�n</strong></th>
                        <th scope="col" style="border-width:thin;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)"><strong>Recibido por</strong></th>
                        <th scope="col" style="border-width:thin;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)"><strong>Nro. ITEM</strong></th>
                        <th scope="col" style="border-width:thin;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)"><strong>ITEM</strong></th>
                    </tr>
                  </thead>
    				<tbody>
        						<?php
									//print_r($paginat);
									$pagact=$solicitudpagina;
									$part= 1;
									for($g=0; $g < (count($paginat)); $g++)
									{
											//echo "//**".$paginat[$g]["AARCOD"]."<br>";
											/*entradas*/
											/* $sql2="SELECT T3.ATRNUM, T3.ATRCOD, T2.ATRDES, T3.ATRFEC, T1.ATRCAN, (T3.ATRDES) AS DESATR, T3.ATROBS 
														FROM IV16FP T1 
															INNER JOIN IV15FP T3 ON (T1.ACICOD=T3.ACICOD AND T1.ATRCOD=T3.ATRCOD AND T1.ATRNUM=T3.ATRNUM ) 
															INNER JOIN iv12fp T2 ON(T1.ACICOD= T2.ACICOD AND T1.ATRCOD=T2.ATRCOD) 
														WHERE T1.ACICOD='$Compania' AND T1.AALCOD='0001' AND T1.ATRCOD in ( '0001', '0003', '0004', '0006', '0101' ) AND  T1.ATRART='".$paginat[$g]["AARCOD"]."'
														ORDER BY T3.ATRFEC ";
											*/
											/*entradas*/
											 $sql2="SELECT T3.ATRNUM, T3.ATRCOD, T2.ATRDES, T3.ATRFEC, T1.ATRCAN, (T3.ATRDES) AS DESATR, T3.ATROBS, T5.ADSNRO, T5.ATSCOD,
													 		(SELECT T6.AAPVLA FROM IV38FP T6 WHERE T6.ACICOD=T1.ACICOD and T6.ATRNUM=T3.ATRNUM and T6.ATRCOD=T3.ATRCOD AND T6.APACOD IN ('1506') ) AS NROGIA
													FROM IV16FP T1 
														INNER JOIN IV15FP T3 ON (T1.ACICOD=T3.ACICOD AND T1.ATRCOD=T3.ATRCOD AND T1.ATRNUM=T3.ATRNUM AND T1.ADPCOD=T3.ADPCOD ) 
														INNER JOIN iv12fp T2 ON(T1.ACICOD= T2.ACICOD AND T1.ATRCOD=T2.ATRCOD) 
														LEFT JOIN IV35FP T5 ON (T1.ACICOD=T5.ACICOD AND T1.AALCOD=T5.AALCOD AND T3.ADPCOD=T5.ADPCOD AND T1.ATRCOD=T5.ATRCOD AND T1.ATRNUM=T5.ATRNUM)
													WHERE T1.ACICOD='$Compania' AND T1.AALCOD='0001' AND T1.ATRCOD in ( '0001', '0003', '0004', '0006', '0101' ) AND  T1.ATRART='".$paginat[$g]["AARCOD"]."' 
													ORDER BY T3.ATRFEC";
											//echo $sql2."<br/><br/>";
											$result2=odbc_exec($cid,$sql2)or die(exit("Error en odbc_exec 11111"));
											$result22=odbc_exec($cid,$sql2)or die(exit("Error en odbc_exec 11111"));
											$canEnt=0;
											//$canEnt = odbc_num_rows($result22);
											while(odbc_fetch_row($result22)){
												$canEnt++;
											}
											//echo "<br>"."*".$canEnt;
											
											/*salidas*/
											$sql3="SELECT T2.ADSNRO, T3.ATSCOD, T3.ATSDES, T2.ATRFEC, T1.ATRCAN, T2.ATROBS, T2.ATRDES, T4.AISCOD, T4.AISDES,
													(SELECT T6.AAPVLA FROM IV46FP T6 WHERE T6.ACICOD=T1.ACICOD and T6.ADSNRO=T2.ADSNRO AND T6.APACOD IN ('1501') ) AS N1501,
													(SELECT T6.AAPVLA FROM IV46FP T6 WHERE T6.ACICOD=T1.ACICOD and T6.ADSNRO=T2.ADSNRO AND T6.APACOD IN ('1504') ) AS N1504,
													(SELECT T6.AAPVLA FROM IV46FP T6 WHERE T6.ACICOD=T1.ACICOD and T6.ADSNRO=T2.ADSNRO AND T6.APACOD IN ('1505') ) AS N1505,
													(SELECT T6.AAPVLA FROM IV46FP T6 WHERE T6.ACICOD=T1.ACICOD and T6.ADSNRO=T2.ADSNRO AND T6.APACOD IN ('1510') ) AS N1510,
													(SELECT T6.AAPVLA FROM IV46FP T6 WHERE T6.ACICOD=T1.ACICOD and T6.ADSNRO=T2.ADSNRO AND T6.APACOD IN ('1509') ) AS N1509
														FROM IV36FP T1 
															INNER JOIN IV35FP T2 ON ( T1.ACICOD = T2.ACICOD AND 
																					  T1.AALCOD = T2.AALCOD AND 
																					  T1.ADPCOD = T2.ADPCOD AND 
																					  T1.ATSCOD = T2.ATSCOD AND 
																					  T1.ADSNRO = T2.ADSNRO AND 
																					  T2.ADSSTS NOT IN ('01', '02', '04', '05')  
																					) 
															INNER JOIN IS15FP T3 ON (T1.ACICOD=T3.ACICOD AND T2.ATSCOD=T3.ATSCOD) 
															INNER JOIN IV42FP T4 ON (T2.ACICOD=T4.ACICOD AND T2.AISCOD=T4.AISCOD) 
														WHERE T1.ACICOD='$Compania' AND T1.AARCOD= '".$paginat[$g]["AARCOD"]."' AND T1.AALCOD not in ('0006') AND T3.ATSCOD in ('01','04') ORDER BY T2.ATRFEC ";
											
											//echo $sql3."<br/><br/>";
											$result3=odbc_exec($cid,$sql3)or die(exit("Error en odbc_exec 11111"));
											$result33=odbc_exec($cid,$sql3)or die(exit("Error en odbc_exec 11111"));
											//echo "<br>"."+".$canSal = odbc_num_rows($result3);
											$canSal=0;
											//$canEnt = odbc_num_rows($result22);
											while(odbc_fetch_row($result33)){
												$canSal++;
											}
											//echo "<br>"."*".$canSal;
											
											
											$resultES = '';
											$numRows = '';
											if($canSal>$canEnt){
												$resultES = $result3;
												$numRows = $canSal;
												//echo "<br>"."* salida";
											}else if($canSal<=$canEnt){
												$resultES = $result2;
												$numRows = $canEnt;
												//echo "<br>"."* entrada";
											}
											$rowpos = 0;
											//echo "numRows:".$numRows;
											//echo "rowpos:".$rowpos;				
                                 ?>           
                                                <tr >
	                                                <td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:left;vertical-align:middle;" valign="middle" rowspan="<?php echo $numRows; ?>"><div>&nbsp;<?php echo $paginat[$g]["AARCOD"]; ?></div></td>
                                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:left;vertical-align:middle;" valign="middle" rowspan="<?php echo $numRows; ?>"><div><?php echo $paginat[$g]["AARDES"]; ?></div></td>
                                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:left;vertical-align:middle;" valign="middle" rowspan="<?php echo $numRows; ?>"><div><?php echo $paginat[$g]["ATIDES"]; ?></div></td>
                                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:left;vertical-align:middle;" valign="middle" rowspan="<?php echo $numRows; ?>">
                                                    	<div>
															<?php 
                                                                $list = list_ubiart($cid, $Compania, '0001', $paginat[$g]["AARCOD"],2);
                                                                //echo "*".trim($paginat[$g]["AUBCOD"])."*";
                                                                //echo '<a rel="shadowbox;width=400;height=300" href="editarfubicacion.php?&aarcod='.trim($paginat[$g]["AARCOD"]).'&aalcod='.$aalcod.'" title="Editar" >';
                                                                
                                                                if(trim($list)!=''){
                                                                    echo $list; } 
                                                                else { 
                                                                    echo ' - Sin Ubicaci�n';
                                                                }
                                                                //echo '</a>';
                                                             ?>
                                                        </div>
                                                    </td>
                                 <?php
                                             //while(odbc_fetch_row($resultES)){   
											 if($numRows>$rowpos){
											 while($numRows>$rowpos){   
											 
											//echo "numRows:".$numRows;
											//echo "rowpos:".$rowpos;
											          $rowpos++;       
													  odbc_fetch_row($result2);
													  odbc_fetch_row($result3);                                     
												if($rowpos<=$canEnt){
													?>
                                                <!-- entradas -->
                                                <?php 
													if(odbc_result($result2, 'ATRCOD')=='0003'){
														?>
                                                        <td style="border-width:thin;border-bottom:solid;border-left:solid;text-align:center;vertical-align:middle;" valign="middle">&nbsp;<?php echo odbc_result($result2, 'ADSNRO')!=''?add_ceros(odbc_result($result2, 'ADSNRO'),6):'&nbsp;'; echo "<strong>(D)</strong>";?></td>
                                                        <?php
													}else{
														?>
                                                        <td style="border-width:thin;border-bottom:solid;border-left:solid;text-align:center;vertical-align:middle;" valign="middle">&nbsp;<?php echo odbc_result($result2, 'ATRNUM')!=''?add_ceros(odbc_result($result2, 'ATRNUM'),6):'&nbsp;'; echo '(E)';?></td>
                                                        <?php
													}	
													
													if(odbc_result($result2, 'ATRCOD')=='0003'){
														?>
                                                        <td style="border-width:thin;border-bottom:solid;border-left:solid;text-align:center;vertical-align:middle;" valign="middle">&nbsp;<?php echo odbc_result($result2, 'NROGIA')!=''?odbc_result($result2, 'NROGIA'):'N/A';?></td>
                                                        <?php
													}else{
														?>
                                                        <td style="border-width:thin;border-bottom:solid;border-left:solid;text-align:center;vertical-align:middle;" valign="middle">&nbsp;<?php echo odbc_result($result2, 'NROGIA')!=''?odbc_result($result2, 'NROGIA'):'---&nbsp;';?></td>
                                                        <?php
													}
								?>
                                                    
                                                    <!--<td style="border-width:thin;border-bottom:solid;border-left:solid;">&nbsp;<?php echo odbc_result($result2, 'ATRDES')!=''?odbc_result($result2, 'ATRDES'):'&nbsp;';?></td>-->
                                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;text-align:center;vertical-align:middle;" valign="middle"><?php echo odbc_result($result2, 'ATRFEC')!=''?formatDate(odbc_result($result2, 'ATRFEC'),'aaaa-mm-dd','dd/mm/aaaa' ):'&nbsp;';?></td>
                                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;text-align:center;vertical-align:middle; background-color:rgb(0,153,255)" valign="middle"><?php echo odbc_result($result2, 'ATRCAN')!=''?number_format(odbc_result($result2, 'ATRCAN'),0,',','.'):'&nbsp;';?></td>
                                                    <!--<td style="border-width:thin;border-bottom:solid;border-left:solid;text-align:center;vertical-align:text-top;">&nbsp;<?php echo odbc_result($result2, 'DESATR')!=''?odbc_result($result2, 'DESATR'):'&nbsp;';?></td>-->
                                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;text-align:center;vertical-align:middle;" valign="middle"><?php echo odbc_result($result2, 'ATROBS')!=''?odbc_result($result2, 'ATROBS'):'&nbsp;';?></td>
                                                    	
                                                      
                                 <?php 
								 				
                                             	}else{
                                            // while(odbc_fetch_row($resultES)){                     
								?>
                                					<td style="border-width:thin;border-bottom:solid;border-left:solid;border-left:solid;">&nbsp;</td>
                                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;border-left:solid;">&nbsp;</td>
                                                    <!--<td style="border-width:thin;border-bottom:solid;border-left:solid;">&nbsp;</td>-->
                                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;">&nbsp;</td>
                                                    <td style="border-width:thin;border-bottom:solid;border-left:solid; background-color:rgb(0,153,255)">&nbsp;</td>
                                                    <!--<td style="border-width:thin;border-bottom:solid;border-left:solid;">&nbsp;</td>-->
                                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;">&nbsp;</td>
                              <?php 
                                            	 }
											 
											 	//if($rowpos <= $canSal){
													//if($rowpos<=$canEnt)
											 	//if(  ($rowpos <= $canEnt) && (number_format(odbc_result($result3, 'ATRCAN'),0,',','.') >= 0)  ){
												if(  ($rowpos <= $canSal) && (number_format(odbc_result($result3, 'ATRCAN'),0,',','.') >= 0)  ){
                                            
								?>                      
                                                    <!-- salida -->
                                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;text-align:center;vertical-align:middle;" valign="middle">&nbsp;<?php echo odbc_result($result3, 'ADSNRO')!=''?add_ceros(odbc_result($result3, 'ADSNRO'),6):'&nbsp;'; echo "(S)";?></td>
                                                    <!--<td style="border-width:thin;border-bottom:solid;border-left:solid;">&nbsp;<?php echo odbc_result($result3, 'ATSDES')!=''?odbc_result($result3, 'ATSDES'):'&nbsp;';?></td>-->
                                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;text-align:center;vertical-align:middle;" valign="middle"><?php echo odbc_result($result3, 'ATRFEC')!=''?formatDate(odbc_result($result3, 'ATRFEC'),'aaaa-mm-dd','dd/mm/aaaa' ):'&nbsp;';?></td>
                                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;text-align:center;vertical-align:middle;background-color:rgb(255,255,153)" valign="middle"><?php echo odbc_result($result3, 'ATRCAN')!=''?number_format(odbc_result($result3, 'ATRCAN'),0,',','.'):'&nbsp;';?></td>
                                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;text-align:center;vertical-align:middle;" valign="middle"><?php echo odbc_result($result3, 'AISDES')!=''?odbc_result($result3, 'AISDES'):'&nbsp;';?></td>
                                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;text-align:center;vertical-align:middle;" valign="middle"><?php echo odbc_result($result3, 'ATROBS')!=''?odbc_result($result3, 'ATROBS'):'&nbsp;';?></td>
                                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;text-align:center;vertical-align:middle;" valign="middle"><?php echo odbc_result($result3, 'ATROBS')!=''?odbc_result($result3, 'ATRDES'):'&nbsp;';?></td>
                                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;text-align:center;vertical-align:middle;" valign="middle"><?php echo odbc_result($result3, 'N1501')!=''?odbc_result($result3, 'N1501'):'--';?></td>
                                                    
                                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;text-align:center;vertical-align:middle;" valign="middle"><?php echo odbc_result($result3, 'N1504')!=''?odbc_result($result3, 'N1504'):'--';?></td>
                                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;text-align:center;vertical-align:middle;" valign="middle"><?php echo odbc_result($result3, 'N1505')!=''?odbc_result($result3, 'N1505'):'--';?></td>
                                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;text-align:center;vertical-align:middle;" valign="middle"><?php echo odbc_result($result3, 'N1510')!=''?odbc_result($result3, 'N1510'):'--';?></td>
                                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;text-align:center;vertical-align:middle;" valign="middle"><?php echo odbc_result($result3, 'N1509')!=''?odbc_result($result3, 'N1509'):'--';?></td>
                                <?php 
												}else{
								?>
                                					<td style="border-width:thin;border-bottom:solid;border-left:solid;">&nbsp;</td>
                                                    <!--<td style="border-width:thin;border-bottom:solid;">&nbsp;</td>-->
                                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;">&nbsp;</td>
                                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;background-color:rgb(255,255,153)">&nbsp;</td>
                                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;">&nbsp;</td>
                                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;">&nbsp;</td>
                                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;">&nbsp;</td>
                                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;">&nbsp;</td>
                                                    
                                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;">&nbsp;</td>
                                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;">&nbsp;</td>
                                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;">&nbsp;</td>
                                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;">&nbsp;</td>
                              <?php 
                                             	}
											 
                                            	if($rowpos == 1){
								?> 
                                                   <td align="right" style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:center;vertical-align:middle;background-color:rgb(0,255,0)" valign="middle" rowspan="<?php echo $numRows; ?>"><div><?php echo $paginat[$g]["ASLSAF"]!=''?number_format($paginat[$g]["ASLSAF"],0,',','.'):"--";?></div></td>
								<?php 
                                            	}//fin if  if($rowpos == 1)
								?>
                                            </tr>
                               <?php				
											}//fin while(odbc_fetch_row($resultES))
											}else{
								?> 
                                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;">&nbsp;</td>
                                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;">&nbsp;</td>
                                                    <!--<td style="border-width:thin;border-bottom:solid;">&nbsp;</td>-->
                                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;">&nbsp;</td>
                                                    <td style="border-width:thin;border-bottom:solid;border-left:solid; background-color:rgb(0,153,255)">&nbsp;</td>
                                                    <!--<td style="border-width:thin;border-bottom:solid;border-left:solid;">&nbsp;</td>-->
                                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;">&nbsp;</td>
                                                    
                                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;">&nbsp;</td>
                                                    <!--<td style="border-width:thin;border-bottom:solid;">&nbsp;</td>-->
                                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;">&nbsp;</td>
                                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;background-color:rgb(255,255,153)">&nbsp;</td>
                                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;">&nbsp;</td>
                                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;">&nbsp;</td>
                                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;">&nbsp;</td>
                                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;">&nbsp;</td>
                                                    
                                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;">&nbsp;</td>
                                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;">&nbsp;</td>
                                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;">&nbsp;</td>
                                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;">&nbsp;</td>
                                                    
                                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;background-color:rgb(0,255,0)" rowspan="<?php echo $numRows; ?>">&nbsp;</td>
                                                </tr>
								<?php 				
												
											}
																				
									}
								?>                                            
                  </tbody>

</table>
</td>
</tr>
</table>
</body>
</html>
