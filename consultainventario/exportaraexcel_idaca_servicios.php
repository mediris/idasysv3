<?php session_start();
include("../conectar.php");
$arqnro = trim($_GET["num"]);
$desde = $_GET['desde'];
$hasta = $_GET['hasta'];
$aalcod = $_GET['aalcod'];
//header("Pragma: ");
header("Pragma: no-cache");
header('Cache-control: ');
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Content-type: application/vnd.ms-excel");
//header("Content-type: application/octet-stream");
header("Content-disposition: attachment; filename=Reporte_dotaciones_servicios_IDACA.xls");

?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<title>Reporte de Dotaciones por Servicios IDACA</title>
	</head>
	<style>

		h1, h2, h3, h4, h5{
			margin: 0;
			padding: 0;
			font-weight: normal;
			color: #32639A;
		}

		h1{
			font-size: 2em;
		}

		h2{
			font-size: 2.4em;	
		}

		h3{
			font-size: 1.6em;
			font-style: italic;
		}

		h4{
			font-size: 1.6em;
			font-style: italic;
			color: #FFF;
		}

		h5{
			font-size: 1.0em;
			font-style: italic;
			color: #666;
		}

		#background-image{
			font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
			font-size: 10px;
			margin: 0px;
			width: 100%;
			text-align: left;
			border-collapse: collapse;
		}

		#background-image th{
			padding: 12px;
			font-weight: normal;
			font-size: 12px;
			color: #339;
			border-bottom-style: solid;
			border-left-style: none;
			text-align: center;
		}

		#background-image td{
			color: #669;
			border-top: 1px solid #fff;
			padding-right: 4px;
			padding-left: 4px;
		}

		#background-image tfoot td{
			font-size: 9px;
		}

		#background-image tbody{
			background-repeat: no-repeat;
			background-position: left top;
		}

		#background-image tbody td{
			background-image: url(images/backn.png);
		}

		* html #background-image tbody td{
			/* 
	   		----------------------------
			PUT THIS ON IE6 ONLY STYLE 
			AS THE RULE INVALIDATES
			YOUR STYLESHEET
	   		----------------------------
			*/
			filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='images/backn.png',sizingMethod='crop');
			background: none;
		}	
	</style>
	<body>
		<?php 

	 		$wsolicitud = 0;
			if($solicitudpagina == 0){

				$sql = "SELECT T2.AISCOD, T1.AARCOD, T3.AARDES, T4.AARUMB, T5.AUMDES, T6.AISDES, T2.ADSNRO, T2.ATROBS, SUM(T1.ATRCAN) AS ATRCAN, (SUBSTR(CHAR(T2.ATRFEC), 1, 4) || SUBSTR(CHAR(T2.ATRFEC), 6, 2)) AS ANOMES

				FROM IV36FP T1
				JOIN IV35FP T2 ON(T2.ACICOD = T1.ACICOD AND T2.ADSNRO = T1.ADSNRO AND T2.ATRFEC BETWEEN '".$desde."' AND '".$hasta."')
				JOIN IV05FP T3 ON(T3.ACICOD = T1.ACICOD AND T3.AARCOD = T1.AARCOD)
				JOIN IV06FP T4 ON(T4.ACICOD = T1.ACICOD AND T4.AARCOD = T1.AARCOD)
				JOIN IV13FP T5 ON(T5.ACICOD = T1.ACICOD AND T5.AUMCOD = T4.AARUMB)
				JOIN IV42FP T6 ON(T6.ACICOD = T1.ACICOD AND T6.AISCOD = T2.AISCOD)

				WHERE T1.ACICOD = '".$Compania."' AND T1.AALCOD = '".$aalcod."' AND T1.ATSCOD IN('01', '02', '03', '04', '05', '06', '07')

				GROUP BY T2.AISCOD, T1.AARCOD, T3.AARDES, T4.AARUMB, T5.AUMDES, T6.AISDES, T2.ADSNRO, T2.ATROBS, (SUBSTR(CHAR(T2.ATRFEC), 1, 4) || SUBSTR(CHAR(T2.ATRFEC), 6, 2))

				ORDER BY T2.AISCOD, T1.AARCOD, T3.AARDES, T4.AARUMB, T5.AUMDES, T6.AISDES, T2.ADSNRO, T2.ATROBS, (SUBSTR(CHAR(T2.ATRFEC), 1, 4) || SUBSTR(CHAR(T2.ATRFEC), 6, 2))";

				$resultt = odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111"));
				
				$z = 0;
				$lin = 1;
				$limitep = $_SESSION['solicitudlineasporpaginat'];
				$pag = 1;

				while(odbc_fetch_row($resultt)){ 

					$jml = odbc_num_fields($resultt);
					$row[$z]["pagina"] =  $pag;
					for($i=1;$i<=$jml;$i++){	
						$row[$z][odbc_field_name($resultt,$i)] =  odbc_result($resultt,$i);
					}
					$z++;
					if ($lin>=$limitep) 
					{
						$limitep+=$_SESSION['solicitudlineasporpaginat'];
						$pag++;
					}
					$lin++;
				}

				$totsol = ($lin-1);
				$_SESSION['totalsolicitudes'] = $totsol;
				$_SESSION['solicitudarreglo'] = $row;
				$solicitudpagina = 1;
				$_SESSION['solicitudpaginas'] = $pag;
			}//fin de solicitudpagina
			/*se muestra la cantidad de elementos segun la solicitada en $solicitudpagina*/
			$paginat = $_SESSION['solicitudarreglo'];
		?>

		<table width="100%" border="0">
			<tr>
				<td height="89">
					<h1>
						<?php if($Compania=='14'){?>
							<img src="http://<?php echo $Direccionip; ?>/idasysv3/images/logoidacadef2005.png" width="280" height="68" />
						<?php }else{ ?>
							<img src="http://<?php echo $Direccionip; ?>/idasysv3/images/logomeditronnuevo.png" width="280" height="68" />
						<?php } ?>
	   				</h1>
	  				<h5>RIF:  <?php echo $Companiarif; ?></h5>
	  			</td>
			</tr>
			<tr>
				<td>
					<table width="100%" id="background-image" >
	  					<thead>
	  						<tr>
	        					<th colspan="14" scope="col">
	        						<h3>Reporte Dotaciones por Servicios IDACA</h3>
	        					</th>
	    					</tr>
	    					<tr>
	        					<th colspan="14" scope="col"><h4>Elaborado el <?php echo $Fechaactual; ?> a las <?php echo $Horaactual2; ?></h4></th>
	    					</tr>

	  						<tr>
	        					<th colspan="14" scope="col"><h3>Almac&eacute;n: <?php echo alamcen($aalcod, $Compania);?></h3></th>
	    					</tr>
	  						<tr style="border-bottom:solid;">

						        <th scope="col" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)">C&oacute;digo Art.</th>
						        <th scope="col" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)">Art&iacute;culo</th>
						        <th scope="col" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)">Unidad de medida</th>
						        <th scope="col" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)">Servicio</th>	
						        <th scope="col" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)">Cantidad</th>
						        <th scope="col" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)">Dotaci&oacute;n</th>
						        <th scope="col" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)">Observaci&oacute;n</th>
						        <th scope="col" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;border-right:solid;background-color:rgb(204,204,204)">Fecha</th>
	    					</tr>
	  					</thead>
	   					<tbody>
							<?php 

							$pagact = $solicitudpagina;
							$part = 1;

							for($g = 0; $g < (count($paginat)); $g++){
								
								// echo "//**".$paginat[$g]["AARCOD"]."<br>";

								$ano = substr($paginat[$g]["ANOMES"],0, 4);
								$mes = substr($paginat[$g]["ANOMES"],4, 2);

								if($mes == '01'){
									$nombre = 'Enero';
								}else if($mes == '02'){
									$nombre = 'Febrero';
								}else if($mes == '03'){
									$nombre = 'Marzo';
								}else if($mes == '04'){
									$nombre = 'Abril';
								}else if($mes == '05'){
									$nombre = 'Mayo';
								}else if($mes == '06'){
									$nombre = 'Junio';
								}else if($mes == '07'){
									$nombre = 'Julio';
								}else if($mes == '08'){
									$nombre = 'Agosto';
								}else if($mes == '09'){
									$nombre = 'Septiembre';
								}else if($mes == '10'){
									$nombre = 'Octubre';
								}else if($mes == '11'){
									$nombre = 'Noviembre';
								}else if($mes == '12'){
									$nombre = 'Diciembre';
								} ?>

                            	<tr>

                             		<!-- Código del artículo -->
                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:center;vertical-align:middle;" valign="middle">
                                    	<?php echo $paginat[$g]["AARCOD"] != '' ? $paginat[$g]["AARCOD"] : '--';?>
                                    </td>

                                    <!-- Artículo -->
                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:center;vertical-align:middle;" valign="middle">
                                    	<?php echo $paginat[$g]["AARDES"] != '' ? $paginat[$g]["AARDES"] : '--';?>
                                    </td>

                                    <!-- Unidad de medida -->
                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:center;vertical-align:middle;" valign="middle">
                                    	<?php echo $paginat[$g]["AUMDES"] != '' ? $paginat[$g]["AUMDES"] : '--';?>
                                    </td>

                                    <!-- Servicio -->
                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:center;vertical-align:middle;" valign="middle">
                                    	<?php echo $paginat[$g]["AISDES"] != '' ? $paginat[$g]["AISDES"] : '--';?>
                                    </td>

                                    <!-- Cantidad -->
                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:center;vertical-align:middle;" valign="middle">
                                    	<?php echo $paginat[$g]["ATRCAN"] != '' ?  number_format($paginat[$g]["ATRCAN"],2,',','.') : '--';?>

                                    </td> 

                                    <!-- Dotacion -->
                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:center;vertical-align:middle;" valign="middle">
                                    	<?php echo $paginat[$g]["ADSNRO"] != '' ? $paginat[$g]["ADSNRO"] : '--';?>

                                    </td>

                                    <!-- Observacion -->
                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:center;vertical-align:middle;" valign="middle">
                                    	<?php echo $paginat[$g]["ATROBS"] != '' ? $paginat[$g]["ATROBS"] : '--';?>

                                    </td>

                                    <!-- Fecha -->
                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:center;vertical-align:middle;" valign="middle">
                                    	<?php echo $paginat[$g]["ANOMES"] != '' ? $nombre . " - " . $ano : '--';?>
                                    </td>

                                </tr>

	               				<?php 
	        				}?>      
	    				</tbody>
					</table>
				</td>
			</tr>
		</table>
	</body>
</html>
