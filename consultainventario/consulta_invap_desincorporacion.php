<?php 
/*
 * jDavila
 * 23/02/2015
 */
session_start();
include("../conectar.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
<title>Idasys V3</title>
<link href="../<?php echo $css; ?>" rel="stylesheet" type="text/css" media="screen" />
<script language="JavaScript" src="../javascript/javascript.js"></script>
<script language="JavaScript" src="../calendario/javascripts.js"></script>
<script language="JavaScript" src="javascript.js"></script>
<script language="JavaScript" src="../javascript/jquery.js"></script>
<link href="../shadowbox.css"  rel="stylesheet" type="text/css"/>
<script src="../shadowbox.js" type="text/javascript"/> </script>
<script language="JavaScript" type="text/JavaScript">
Shadowbox.init({overlayOpacity: "0.5"});
</script>
<style type="text/css" title="currentStyle">
			@import "../DataTables-1.9.4/media/css/<?php echo $demo_page;?>";
			@import "../DataTables-1.9.4/media/css/<?php echo $demo_table;?>";
</style>
<script type="text/javascript" language="javascript" src="../DataTables-1.9.4/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf-8">
		
			
			
			 $(document).ready(function() 
			 {
			      document.getElementById('info').style.display="";

				 $('#info').dataTable( 
				  {
					"bStateSave": true,
			        "oLanguage": 
					{
			          	"sLengthMenu": "Mostrar _MENU_ registros por Pag.",
						"sZeroRecords": "No Existen Registros",
						"sInfo": "Mostrando desde _START_ hasta _END_ de _TOTAL_ registros",
						"sInfoEmpty": "Mostrando desde 0 hasta 0 de 0 registros",
						"sInfoFiltered": "(filtrando de _MAX_ de registros)",
						"sSearch": "Buscar",
						"oPaginate": 
						{
							"sNext": "Siguiente",
							"sPrevious": "Anterior"
						}
			        }
			      } );
			  } );
</script>

</head> 
<body background="../images/fondo idaca.jpg" >
<div id="wrapper">
  <?php include("../superior.php");?>
  <div id="page">
     <?php  include("../validar.php");?>
 		<?php 
				
					
					$wsolicitud=0;
					if ($solicitudpagina==0) 	
					{

						$sql="SELECT T3.AARUMB, T4.AUMDES, T1.ACICOD, T1.AARCOD, T1.AARNIV, T1.ATICOD, T1.ASICOD, T1.AARDES, T1.AARTDT, T1.AARSTS,
						(SELECT T6.ASLSAF FROM IV40FP T6 WHERE T6.ACICOD=T1.ACICOD AND T6.AARCOD=T1.AARCOD AND T6.AALCOD='0006' ORDER BY T6.ASLFEF DESC FETCH FIRST 1 ROWS ONLY) AS ASLSAF
							FROM IV05FP T1 
								INNER JOIN IV06FP T3 ON ( T1.ACICOD=T3.ACICOD AND T1.AARCOD=T3.AARCOD ) 
								INNER JOIN IV13FP T4 ON ( T1.ACICOD=T4.ACICOD AND T3.AARUMB=T4.AUMCOD ) 
							WHERE T1.ACICOD='$Compania' AND ( T1.AARCOD IN (SELECT T2.AARCOD FROM IV40FP T2 WHERE T2.ACICOD=T1.ACICOD AND T2.AALCOD='0006' GROUP BY T2.AARCOD ORDER BY T2.AARCOD ) ) 
							ORDER BY T1.AARDES";
						
						//echo $sql."<br/><br/>";
						$resultt=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111"));
						
						$z=0;
						$tothon=0;
						$totest=0;
						$lin=1;
						$limitep=$_SESSION['solicitudlineasporpaginat'];
						$pag=1;
						$primero='S';
						
						
						while(odbc_fetch_row($resultt))
						{
							
							$jml = odbc_num_fields($resultt);
							$row[$z]["pagina"] =  $pag;
							for($i=1;$i<=$jml;$i++)
							{	
								$row[$z][odbc_field_name($resultt,$i)] =  odbc_result($resultt,$i);
							}
							$z++;
							if ($lin>=$limitep) 
							{
								$limitep+=$_SESSION['solicitudlineasporpaginat'];
								$pag++;
							}
							$lin++;
						}
						$totsol=($lin-1);
						$_SESSION['totalsolicitudes']=$totsol;
						$_SESSION['solicitudarreglo']=$row;
						$solicitudpagina=1;
						$_SESSION['solicitudpaginas']=$pag;
					}//fin de solicitudpagina
					/*se muestra la cantidad de elementos segun la solicitada en $solicitudpagina*/
					$paginat=$_SESSION['solicitudarreglo'];
				
			?>
        <div id="content2" >   
        	<table width="100%"   border="0">
              <tr>
                <td width="65%" scope="col" colspan="2"><h1  class="title">REPORTE DE REPUESTOS DA�ADOS - INVAP</h1>
                  <hr /></td>
                <td width="13%" scope="col"><div align="left">
                  <table width="100%"  border="0">
                    <tr>
                      <th width="30%" scope="col" >
                      <div class="flechas">
                      	<a href="exportaraexcel_invap_desincorporacion.php" target="_blank">
                        	<img src="../images/excel_invap.jpg" alt="" width="30" height="30" title="Exportar a Excel Reporte INVAP"/>
                        </a>
                        <a href="exportaraexcel_medi_invap_desincorporacion.php" target="_blank">
                        	<img src="../images/excel.jpg" alt="" width="30" height="30" title="Exportar a Excel Reporte MEDI-INVAP"/>
                        </a>
                      </div></th>
                    </tr>
                  </table>
                </div></td>
              </tr>
            </table>   
		<div id="container">
            <div id="demo">
                <table width="100%" id="info" >
                  <thead>
                    <tr>
                    	<th scope="col" rowspan="2" style="background-color:rgb(204,204,204)">C�digo</th>
                   		<th scope="col" rowspan="2" style="background-color:rgb(204,204,204)">Art�culo</th>
                        <th scope="col" rowspan="2" style="background-color:rgb(204,204,204)">Ubicaci�n</th>
                    	<th scope="col" colspan="3" style="background-color:rgb(204,204,204)">Entradas</th>
                        <th scope="col" colspan="3" style="background-color:rgb(204,204,204)">Salidas</th>
                        <th scope="col" rowspan="2" style="background-color:rgb(204,204,204)">Existencia Actual</th>
                    </tr>
                    <tr>
                        <th scope="col" style="background-color:rgb(204,204,204)"><strong>Nro. de Entrada</strong></th>
                        <!--<th scope="col" ><strong>Tipo</strong></th>-->
                        <th scope="col" style="background-color:rgb(204,204,204)"><strong>Fecha</strong></th>
                        <th scope="col" style="background-color:rgb(204,204,204)"><strong>Cantidad</strong></th>
                        <!--<th scope="col" style="background-color:rgb(204,204,204)"><strong>Descripci�n</strong></th>-->
                        <!--<th scope="col" style="background-color:rgb(204,204,204)"><strong>Observaciones</strong></th>-->
                        
                        <th scope="col" style="background-color:rgb(204,204,204)"><strong>Nro. de Salida</strong></th>
                        <!--<th scope="col" ><strong>Tipo</strong></th>-->
                        <th scope="col" style="background-color:rgb(204,204,204)"><strong>Fecha</strong></th>
                        <th scope="col" style="background-color:rgb(204,204,204)"><strong>Cantidad</strong></th>
                        <!--<th scope="col" style="background-color:rgb(204,204,204)"><strong>Destino</strong></th>
                        <th scope="col" style="background-color:rgb(204,204,204)"><strong>Observaciones</strong></th>-->
                    </tr>
                  </thead>
    				<tbody>
        						<?php
									//print_r($paginat);
									$pagact=$solicitudpagina;
									$part= 1;
									for($g=0; $g < (count($paginat)); $g++)
									{
											//echo "//**".$paginat[$g]["AARCOD"]."<br>";
											/*entradas*/
											 $sql2="SELECT T3.ATRNUM, T3.ATRCOD, T2.ATRDES, T3.ATRFEC, T1.ATRCAN, (T3.ATRDES) AS DESATR, T3.ATROBS 
														FROM IV16FP T1 
															INNER JOIN IV15FP T3 ON (T1.ACICOD=T3.ACICOD AND T1.ATRCOD=T3.ATRCOD AND T1.ATRNUM=T3.ATRNUM ) 
															INNER JOIN iv12fp T2 ON(T1.ACICOD= T2.ACICOD AND T1.ATRCOD=T2.ATRCOD) 
														WHERE T1.ACICOD='$Compania' AND T1.AALCOD='0006' AND T1.ATRCOD not in ( '0002', '0005', '0007', 0008) AND  T1.ATRART='".$paginat[$g]["AARCOD"]."' ORDER BY T3.ATRNUM";
											//echo $sql2."<br/><br/>";
											$result2=odbc_exec($cid,$sql2)or die(exit("Error en odbc_exec 11111"));
											$result22=odbc_exec($cid,$sql2)or die(exit("Error en odbc_exec 11111"));
											$canEnt=0;
											//$canEnt = odbc_num_rows($result22);
											while(odbc_fetch_row($result22)){
												$canEnt++;
											}
											//echo "<br>"."*".$canEnt;
											
											/*salidas*/
											$sql3="SELECT T2.ADSNRO, T3.ATSCOD, T3.ATSDES, T2.ATRFEC, T1.ATRCAN, T2.ATROBS, T4.AISCOD, T4.AISDES
														FROM IV36FP T1 
															INNER JOIN IV35FP T2 ON (T1.ACICOD=T2.ACICOD AND T1.AALCOD= T2.AALCOD AND T1.ADPCOD=T2.ADPCOD AND T1.ATSCOD= T2.ATSCOD AND T1.ADSNRO=T2.ADSNRO) 
															INNER JOIN IS15FP T3 ON (T1.ACICOD=T3.ACICOD AND T2.ATSCOD=T3.ATSCOD) 
															INNER JOIN IV42FP T4 ON (T2.ACICOD=T4.ACICOD AND T2.AISCOD=T4.AISCOD) 
														WHERE T1.ACICOD='$Compania' AND T1.AARCOD= '".$paginat[$g]["AARCOD"]."' AND T1.AALCOD='0006' ORDER BY T1.ADSNRO";
											
											//echo $sql3."<br/><br/>";
											$result3=odbc_exec($cid,$sql3)or die(exit("Error en odbc_exec 11111"));
											$result33=odbc_exec($cid,$sql3)or die(exit("Error en odbc_exec 11111"));
											//echo "<br>"."+".$canSal = odbc_num_rows($result3);
											$canSal=0;
											//$canEnt = odbc_num_rows($result22);
											while(odbc_fetch_row($result33)){
												$canSal++;
											}
											//echo "<br>"."*".$canSal;
											
											
											$resultES = '';
											$numRows = '';
											if($canSal>$canEnt){
												$resultES = $result3;
												$numRows = $canSal;
												//echo "<br>"."* salida";
											}else if($canSal<=$canEnt){
												$resultES = $result2;
												$numRows = $canEnt;
												//echo "<br>"."* entrada";
											}
											$rowpos = 0;
											//echo "numRows:".$numRows;
											//echo "rowpos:".$rowpos;				
                                 ?>           
                                                <!--<tr >
	                                                <td valign="middle" style="text-align:left;vertical-align:text-top;" rowspan="<?php echo $numRows; ?>"><div>&nbsp;<?php echo $paginat[$g]["AARCOD"]; ?></div></td>
                                                    <td valign="middle" style="text-align:left;vertical-align:text-top;" rowspan="<?php echo $numRows; ?>"><div><?php echo $paginat[$g]["AARDES"]; ?></div></td>
                                                 -->
                                 <?php
                                             //while(odbc_fetch_row($resultES)){   
											 if($numRows>$rowpos){
											 while($numRows>$rowpos){   
											 ?>
                                             <tr >
	                                                <td valign="middle" style="text-align:left;vertical-align:text-top;" rowspan="<?php //echo $numRows; ?>"><div>&nbsp;<?php echo $paginat[$g]["AARCOD"]; ?></div></td>
                                                    <td valign="middle" style="text-align:left;vertical-align:text-top;" rowspan="<?php //echo $numRows; ?>"><div><?php echo $paginat[$g]["AARDES"]; ?></div></td>
                                                    <td valign="middle" style="text-align:left;vertical-align:text-top;" rowspan="<?php //echo $numRows; ?>">
                                                    	<div>
															<?php 
                                                                $list = list_ubiart($cid, $Compania, '0006', $paginat[$g]["AARCOD"],2);
                                                                //echo "*".trim($paginat[$g]["AUBCOD"])."*";
                                                                //echo '<a rel="shadowbox;width=400;height=300" href="editarfubicacion.php?&aarcod='.trim($paginat[$g]["AARCOD"]).'&aalcod='.$aalcod.'" title="Editar" >';
                                                                
                                                                if(trim($list)!=''){
                                                                    echo $list; } 
                                                                else { 
                                                                    echo ' - Sin Ubicaci�n';
                                                                }
                                                                //echo '</a>';
                                                             ?>
                                                        </div>
                                                    </td>
                                             <?PHP
											//echo "numRows:".$numRows;
											//echo "rowpos:".$rowpos;
											          $rowpos++;       
													  odbc_fetch_row($result2);
													  odbc_fetch_row($result3);                                     
												if($rowpos<=$canEnt){	
									?>
                                                    <!-- entradas -->
                                                    <td style="text-align:center;vertical-align:text-top;">&nbsp;<?php echo odbc_result($result2, 'ATRNUM')!=''?odbc_result($result2, 'ATRNUM'):'&nbsp;';?></td>
                                                    <!--<td >&nbsp;<?php echo odbc_result($result2, 'ATRDES')!=''?odbc_result($result2, 'ATRDES'):'&nbsp;';?></td>-->
                                                    <td style="text-align:center;vertical-align:text-top;">&nbsp;<?php echo odbc_result($result2, 'ATRFEC')!=''?formatDate(odbc_result($result2, 'ATRFEC'),'aaaa-mm-dd','dd/mm/aaaa' ):'&nbsp;';?></td>
                                                    <td style="text-align:center;vertical-align:text-top;">&nbsp;<?php echo odbc_result($result2, 'ATRCAN')!=''?number_format(odbc_result($result2, 'ATRCAN'),2,',','.'):'&nbsp;';?></td>
                                                    <!--<td style="text-align:center;vertical-align:text-top;">&nbsp;<?php echo odbc_result($result2, 'DESATR')!=''?odbc_result($result2, 'DESATR'):'&nbsp;';?></td>-->
                                                    <!--<td style="text-align:center;vertical-align:text-top;">&nbsp;<?php echo odbc_result($result2, 'ATROBS')!=''?odbc_result($result2, 'ATROBS'):'&nbsp;';?></td>-->
                                 <?php 			}else{	?>
                                					<td >&nbsp;-</td>
                                                    <!--<td >&nbsp;</td>-->
                                                    <td >&nbsp;-</td>
                                                    <td >&nbsp;-</td>
                                                    <!--<td >&nbsp;</td>-->
                                                    <!--<td >&nbsp;</td>-->
                              <?php 
                                            	 }
											 	if($rowpos <= $canSal){	?>                      
                                                    <!-- salida -->
                                                    <td style="text-align:center;vertical-align:text-top;">&nbsp;<?php echo odbc_result($result3, 'ADSNRO')!=''?odbc_result($result3, 'ADSNRO'):'&nbsp;';?></td>
                                                    <!--<td >&nbsp;<?php echo odbc_result($result3, 'ATSDES')!=''?odbc_result($result3, 'ATSDES'):'&nbsp;';?></td>-->
                                                    <td style="text-align:center;vertical-align:text-top;">&nbsp;<?php echo odbc_result($result3, 'ATRFEC')!=''?formatDate(odbc_result($result3, 'ATRFEC'),'aaaa-mm-dd','dd/mm/aaaa' ):'&nbsp;';?></td>
                                                    <td style="text-align:center;vertical-align:text-top;">&nbsp;<?php echo odbc_result($result3, 'ATRCAN')!=''?number_format(odbc_result($result3, 'ATRCAN'),2,',','.'):'&nbsp;';?></td>
                                                    <!--<td style="text-align:center;vertical-align:text-top;">&nbsp;<?php echo odbc_result($result3, 'AISDES')!=''?odbc_result($result3, 'AISDES'):'&nbsp;';?></td>
                                                    <td style="text-align:center;vertical-align:text-top;">&nbsp;<?php echo odbc_result($result3, 'ATROBS')!=''?odbc_result($result3, 'ATROBS'):'&nbsp;';?></td>-->
                                <?php 			}else{ ?>
                                					<td >&nbsp;-</td>
                                                    <!--<td >&nbsp;</td>-->
                                                    <td >&nbsp;-</td>
                                                    <!--<td >&nbsp;</td>
                                                    <td >&nbsp;</td>-->
                                                    <td >&nbsp;-</td>
                              <?php 			}
                                            	//if($rowpos == 1)
												{	?> 
                                                   <td valign="middle" align="right" style="text-align:center;vertical-align:text-top;" rowspan="<?php //echo $numRows; ?>"><div><?php echo $paginat[$g]["ASLSAF"]!=''?number_format($paginat[$g]["ASLSAF"],2,',','.'):"--";?></div></td>
								<?php 			}//fin if  if($rowpos == 1)?>
                                            </tr>
                               <?php		}//fin while(odbc_fetch_row($resultES))
											}else{	?> 
                                                    <td >&nbsp;-</td>
                                                    <!--<td >&nbsp;</td>-->
                                                    <td >&nbsp;-</td>
                                                    <!--<td >&nbsp;</td>-->
                                                    <!--<td >&nbsp;</td>-->
                                                    <td >&nbsp;-</td>
                                                    
                                                    <td >&nbsp;-</td>
                                                    <!--<td >&nbsp;</td>-->
                                                    <td >&nbsp;-</td>
                                                    <td >&nbsp;-</td>
                                                    <!--<td >&nbsp;</td>
                                                    <td >&nbsp;</td>-->
                                                    <td valign="middle" align="right"  rowspan="<?php echo $numRows; ?>">&nbsp;-</td>
                                                </tr>
								<?php 		}
									}	?>                                            
                  </tbody>
                </table>
</div>
		  </div>
	  </div>
	</div>
		<!-- end #content -->
		<!-- end #sidebar -->
		<div style="clear: both;">&nbsp;</div>
	</div>
	<!-- end #page -->

	<div id="footer">
		<div id="opcionesmasusadas"><?php include("../opcionesmasusadasphp.php"); ?></div>
	</div>
 
	<!-- end #footer -->
</body>
</html>
