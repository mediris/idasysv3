<?php session_start();
include("../conectar.php");
$arqnro = trim($_GET["num"]);
header("Pragma: ");
header('Cache-control: ');
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Content-type: application/vnd.ms-excel");
header("Content-disposition: attachment; filename=Consulta_de_existencia_reservas.xls");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Documento sin título</title>
</head>
<style>

h1, h2, h3, h4, h5 {
	margin: 0;
	padding: 0;
	font-weight: normal;
	color: #32639A;
}

h1 {
	font-size: 2em;
}

h2 {
	font-size: 2.4em;	
}

h3 {
	font-size: 1.6em;
	font-style: italic;
}
h4 {
	font-size: 1.6em;
	font-style: italic;
	color: #FFF;
}
h5 {
	font-size: 1.0em;
	font-style: italic;
	color: #666;
}

#background-image
{
	font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
	font-size: 10px;
	margin: 0px;
	width: 100%;
	text-align: left;
	border-collapse: collapse;
}
#background-image th
{
	padding: 12px;
	font-weight: normal;
	font-size: 12px;
	color: #339;
	border-bottom-style: solid;
	border-left-style: none;
	text-align: center;
}
#background-image td
{
	color: #669;
	border-top: 1px solid #fff;
	padding-right: 4px;
	padding-left: 4px;
}
#background-image tfoot td
{
	font-size: 9px;
}
#background-image tbody 
{

	background-repeat: no-repeat;
	background-position: left top;
}
#background-image tbody td
{
	background-image: url(images/backn.png);
}
* html #background-image tbody td
{
	/* 
	   ----------------------------
		PUT THIS ON IE6 ONLY STYLE 
		AS THE RULE INVALIDATES
		YOUR STYLESHEET
	   ----------------------------
	*/
	filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='images/backn.png',sizingMethod='crop');
	background: none;
}	
</style>
<body>
<?php 
				
				if (!$_SESSION['fecsel']) {	$_SESSION['fecsel']=$Fechaactual;}
				$fecsel=$_SESSION['fecsel'];
							/*$sql=" SELECT t1.ACICOD, t1.AALCOD, t1.AARCOD, t3.AARDES, T1.AUMCOD, T4.AUMDES, t1.ALTCOD, t5.AARSTM, T6.ASLCTR,
									 sum(case when t1.aslfef=(SELECT max(t2.aslfef) FROM iv40fp t2 where t2.acicod=t1.acicod and t2.aalcod=t1.aalcod and t2.aarcod=t1.aarcod and t2.altcod=t1.altcod and t2.aslfef<'$Fechaactual' ) then t1.aslsaf else 0 end) as salant, 
									 sum(case when t1.aslfef between '$Fechaactual' and '$Fechaactual' then t1.ASLENT else 0 end) as ASLENT, 
									 sum(case when t1.aslfef between '$Fechaactual' and '$Fechaactual' then t1.ASLSAL else 0 end) as ASLSAL 
								FROM iv40fp t1 
									INNER JOIN IV05FP T3 ON(T1.ACICOD=T3.ACICOD AND T1.AARCOD=T3.AARCOD AND T3.AARSTS='01') 
									INNER JOIN IV13FP T4 ON(T1.ACICOD=T4.ACICOD AND T1.AUMCOD=T4.AUMCOD) 
									LEFT JOIN iv39fp t5 ON(T1.ACICOD=T5.ACICOD AND T1.AARCOD=T5.AARCOD AND T1.AALCOD=T5.AALCOD) 
									LEFT JOIN iv41fp T6 ON(T1.ACICOD=T6.ACICOD AND T1.AARCOD=T6.AARCOD AND T6.AALCOD=T5.AALCOD) 
								WHERE t1.acicod='$Compania' and t1.aalcod='$aalcod' 
								GROUP BY t1.ACICOD, t1.AALCOD, t1.AARCOD, t3.AARDES, t1.ALTCOD , T4.AUMDES,T1.AUMCOD, t5.aarstm,T6.ASLCTR 
								ORDER BY T3.AARDES ";*/

							//Variables SQL
							$select = "t1.ACICOD, t1.AALCOD, t1.AARCOD, t3.AARDES, T1.AUMCOD, T4.AUMDES, t1.ALTCOD, t5.AARSTM, T6.ASLCTR,
									 sum(case when t1.aslfef=(SELECT max(t2.aslfef) FROM iv40fp t2 where t2.acicod=t1.acicod and t2.aalcod=t1.aalcod and t2.aarcod=t1.aarcod and t2.altcod=t1.altcod and t2.aslfef<'$Fechaactual' ) then t1.aslsaf else 0 end) as salant, 
									 sum(case when t1.aslfef between '$Fechaactual' and '$Fechaactual' then t1.ASLENT else 0 end) as ASLENT, 
									 sum(case when t1.aslfef between '$Fechaactual' and '$Fechaactual' then t1.ASLSAL else 0 end) as ASLSAL";
							$tables = "iv40fp t1 
									INNER JOIN IV05FP T3 ON(T1.ACICOD=T3.ACICOD AND T1.AARCOD=T3.AARCOD AND T3.AARSTS='01') 
									INNER JOIN IV13FP T4 ON(T1.ACICOD=T4.ACICOD AND T1.AUMCOD=T4.AUMCOD) 
									LEFT JOIN iv39fp t5 ON(T1.ACICOD=T5.ACICOD AND T1.AARCOD=T5.AARCOD AND T1.AALCOD=T5.AALCOD) 
									LEFT JOIN iv41fp T6 ON(T1.ACICOD=T6.ACICOD AND T1.AARCOD=T6.AARCOD AND T6.AALCOD=T5.AALCOD)";
							$where = "t1.acicod='$Compania' and t1.aalcod='$aalcod'";
							$group = "t1.ACICOD, t1.AALCOD, t1.AARCOD, t3.AARDES, t1.ALTCOD , T4.AUMDES,T1.AUMCOD, t5.aarstm,T6.ASLCTR";
							$order = "T3.AARDES";

							$sql = $cid->select($tables, $select, $where, $group, $order);

							$resultt = $sql->fetchAll(PDO::FETCH_ASSOC);
							
								$z=0;
								
								foreach ($resultt as $fields){
									foreach ($fields as $val => $valorinterno)
									{
										$row[$z][$val] = trim($valorinterno);
									}
									$z++;
								}
								$_SESSION['solicitudarreglo']=$row;
								/*se muestra la cantidad de elementos segun la solicitada en $solicitudpagina*/
								$paginat=$_SESSION['solicitudarreglo'];
			?>      
<table width="100%" border="0">
<tr>
<td height="89"><h1>
<?php if($Compania=='14'){?>
	<img src="http://<?php echo $Direccionip; ?>/idasysv3/images/logoidacadef2005.png" width="280" height="68" />
<?php }else{ ?>
	<img src="http://<?php echo $Direccionip; ?>/idasysv3/images/logomeditronnuevo.png" width="280" height="68" />
<?php } ?>
   </h1>
  <h5>RIF:  <?php echo $Companiarif; ?></h5></td>
</tr>
<tr>
<td>

<table width="100%" id="background-image" >
	
  <thead>
  	<tr>
    	<th colspan="9" scope="col">
        	<h2>Consulta Existencia - Reservas(Detalles)</h2>
        </th>
    </tr>
  	<tr>
        <th colspan="9" scope="col"><h3>Almacén: <?php echo alamcen($aalcod, $Compania, $cid);?></h3></th>
    </tr>
    
  	<tr>
        <th width="30%"  scope="col">Art&oacute;culo</th>
        <th width="5%" scope="col">Stock Mínimo</th>
        <th width="10%" scope="col">Ubicaci&oacute;n</th>
        <th width="10%" scope="col">Unidad de Medida</th>
        <th width="10%" scope="col">Existencia</th>
        <th width="10%" scope="col">Reservas</th>
        <th width="10%" scope="col">Disponibilidad</th>
    </tr>
  </thead>
    <tbody>
				<?php 
                    $show = false;
                    //print_r($paginat);
                    $pagact=$solicitudpagina;
                    for($g=0; $g < (count($paginat)); $g++)
                    {
                ?>
                <?php
                	  $aslctr = floatval($paginat[$g]["ASLCTR"]);
                      $salTotal = $paginat[$g]["SALANT"]+$paginat[$g]["ASLENT"]+$paginat[$g]["ASLSAL"];
                      $salFinal = $paginat[$g]["SALANT"]+$paginat[$g]["ASLENT"]+$paginat[$g]["ASLSAL"]-$aslctr;
                      
                      /*validar si piden solo stm */
                      if($stm==1){//si 
                          if($salFinal<=$paginat[$g]["AARSTM"])	{$show = true;}
                          else									{$show = false;}
                      }else if($stm==2){
                         $show = true; 
                      }
                      
                      if($show){
                ?>
                        <tr>
                            <td scope="col"><div><?php echo $paginat[$g]["AARDES"]."<strong>(".$paginat[$g]["AARCOD"].")</strong>";?></div></td>                
                            <td scope="col"><div align="center"><?php echo number_format($paginat[$g]["AARSTM"],2,",",".");?></div></td>
                            <td scope="col" id="opciones"><div><strong>
                            <?php 
                                $list = list_ubiart($cid, $Compania, $paginat[$g]["AALCOD"], $paginat[$g]["AARCOD"],2);
                                if(trim($list)!=''){
                                    echo $list; } 
                                else { 
                                    echo 'Sin Ubicación';
                                }
                             ?>
                            </strong></div></td>
                            <td scope="col"><div align="center"><?php echo $paginat[$g]["AUMDES"];?></div></td>
                            <td scope="col"><div align="right"><?php echo number_format(($salTotal),2,",",".");?></div></td>
                            <td scope="col"><div align="right"><?php echo number_format($aslctr,2,",",".");?></div>  </td>
                            <td scope="col"><div align="right"><?php echo number_format(($salFinal),2,",",".");?></div>  </td>
                        </tr>
                <?php } }?>      
    </tbody>

</table>
</td>
</tr>
</table>
</body>
</html>
