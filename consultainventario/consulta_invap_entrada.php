<?php 
/*
 * jDavila
 * 03/02/2016
 */
session_start();
include("../conectar.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
<title>Idasys V3</title>
<link href="../<?php echo $css; ?>" rel="stylesheet" type="text/css" media="screen" />
<script language="JavaScript" src="../javascript/javascript.js"></script>
<script language="JavaScript" src="../calendario/javascripts.js"></script>
<script language="JavaScript" src="javascript.js"></script>
<script language="JavaScript" src="../javascript/jquery.js"></script>
<link href="../shadowbox.css"  rel="stylesheet" type="text/css"/>
<script src="../shadowbox.js" type="text/javascript"/> </script>
<script language="JavaScript" type="text/JavaScript">
Shadowbox.init({overlayOpacity: "0.5"});
</script>
<style type="text/css" title="currentStyle">
			@import "../DataTables-1.9.4/media/css/<?php echo $demo_page;?>";
			@import "../DataTables-1.9.4/media/css/<?php echo $demo_table;?>";
</style>
<script type="text/javascript" language="javascript" src="../DataTables-1.9.4/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf-8">
		
			
			
			 $(document).ready(function() 
			 {
			      document.getElementById('info').style.display="";

				 $('#info').dataTable( 
				  {
					"bStateSave": true,
			        "oLanguage": 
					{
			          	"sLengthMenu": "Mostrar _MENU_ registros por Pag.",
						"sZeroRecords": "No Existen Registros",
						"sInfo": "Mostrando desde _START_ hasta _END_ de _TOTAL_ registros",
						"sInfoEmpty": "Mostrando desde 0 hasta 0 de 0 registros",
						"sInfoFiltered": "(filtrando de _MAX_ de registros)",
						"sSearch": "Buscar",
						"oPaginate": 
						{
							"sNext": "Siguiente",
							"sPrevious": "Anterior"
						}
			        }
			      } );
			  } );
</script>

</head> 
<body background="../images/fondo idaca.jpg" >
<div id="wrapper">
  <?php include("../superior.php");?>
  <div id="page">
     <?php  include("../validar.php");?>
 		<?php 
				
					
					$wsolicitud=0;
					if ($solicitudpagina==0) 	
					{

						$sql="SELECT T3.ATRNUM, T3.ATRCOD, T4.AARCOD, T4.AARDES, T2.ATRDES, T3.ATRFEC, T1.ATRCAN, (T3.ATRDES) AS DESATR, T3.ATROBS, T1.ATRCUT, T6.AMCCOD,  T6.AMCDES,
								(SELECT T6.AAPVLA FROM IV38FP T6 WHERE T6.ACICOD=T1.ACICOD and T6.ATRNUM=T3.ATRNUM and T6.ATRCOD=T3.ATRCOD AND T6.APACOD IN ('1506') ) AS NROGIA,
								(SELECT T6.ASLSAF FROM IV40FP T6 WHERE T6.ACICOD=T1.ACICOD AND T6.AARCOD=T4.AARCOD AND T6.AALCOD='0001' ORDER BY T6.ASLFEF DESC FETCH FIRST 1 ROWS ONLY) AS ASLSAF
							FROM IV16FP T1 
								INNER JOIN IV05FP T4 ON (T1.ACICOD=T4.ACICOD AND T1.ATRART=T4.AARCOD )
								INNER JOIN IV06FP T5 ON (T1.ACICOD=T5.ACICOD AND T4.AARCOD=T5.AARCOD )
								INNER JOIN IV04FP T6 ON (T1.ACICOD=T6.ACICOD AND T5.AMCCOD=T6.AMCCOD )
								INNER JOIN IV15FP T3 ON (T1.ACICOD=T3.ACICOD AND T1.ATRCOD=T3.ATRCOD AND T1.ATRNUM=T3.ATRNUM AND T3.ATRSTS='02' ) 
								INNER JOIN iv12fp T2 ON (T1.ACICOD=T2.ACICOD AND T1.ATRCOD=T2.ATRCOD)
							WHERE T1.ACICOD='".$Compania."' AND T1.AALCOD='0001' AND T1.ATRCOD in ( '0001') 
							ORDER BY T3.ATRNUM";
						
						//echo $sql."<br/><br/>";
						$resultt=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111"));
						
						$z=0;
						$tothon=0;
						$totest=0;
						$lin=1;
						$limitep=$_SESSION['solicitudlineasporpaginat'];
						$pag=1;
						$primero='S';
						
						
						while(odbc_fetch_row($resultt))
						{
							
							$jml = odbc_num_fields($resultt);
							$row[$z]["pagina"] =  $pag;
							for($i=1;$i<=$jml;$i++)
							{	
								$row[$z][odbc_field_name($resultt,$i)] =  odbc_result($resultt,$i);
							}
							$z++;
							if ($lin>=$limitep) 
							{
								$limitep+=$_SESSION['solicitudlineasporpaginat'];
								$pag++;
							}
							$lin++;
						}
						$totsol=($lin-1);
						$_SESSION['totalsolicitudes']=$totsol;
						$_SESSION['solicitudarreglo']=$row;
						$solicitudpagina=1;
						$_SESSION['solicitudpaginas']=$pag;
					}//fin de solicitudpagina
					/*se muestra la cantidad de elementos segun la solicitada en $solicitudpagina*/
					$paginat=$_SESSION['solicitudarreglo'];
				
			?>
        <div id="content2" >   
        	<table width="100%"   border="0">
              <tr>
                <td width="65%" scope="col" colspan="2"><h1  class="title">Reporte de Entrada Detallado INVAP</h1>
                  <hr /></td>
                <td width="13%" scope="col"><div align="left">
                  <table width="100%"  border="0">
                    <tr>
                      <th width="30%" scope="col" >
                      <div class="flechas">
                      	 <a href="exportaraexcel_invap_entrada.php" target="_blank">
                        	<img src="../images/excel.jpg" alt="" width="30" height="30" title="Exportar a Excel Reporte Entradas INVAP"/>
                        </a>
                      </div></th>
                    </tr>
                  </table>
                </div></td>
              </tr>
            </table>   
		<div id="container">
            <div id="demo">
                <table width="100%" id="info" style="display:none">
                  <thead>
                    <tr>
                    	<th scope="col" style="background-color:rgb(204,204,204)"><strong>N�mero Referencia Entrada</strong></th>
                        <th scope="col" style="background-color:rgb(204,204,204)"><strong>C�digo</strong></th>
                   		<th scope="col" style="background-color:rgb(204,204,204)"><strong>Art�culo</strong></th>
                        <th scope="col" style="background-color:rgb(204,204,204)"><strong>Ubicaci�n</strong></th>
                        <th scope="col" style="background-color:rgb(204,204,204)"><strong>Marca</strong></th>
                        <th scope="col" style="background-color:rgb(204,204,204)"><strong>N�mero de Gu�a</strong></th>
                        <th scope="col" style="background-color:rgb(204,204,204)"><strong>Fecha</strong></th>
                        <th scope="col" style="background-color:rgb(204,204,204)"><strong>Cantidad</strong></th>
                        <th scope="col" style="background-color:rgb(204,204,204)"><strong>Descripci�n</strong></th>
                        <!--<th scope="col" style="background-color:rgb(204,204,204)"><strong>Observaciones</strong></th>-->
                    </tr>
                  </thead>
    				<tbody>
        						<?php
									//print_r($paginat);
									$pagact=$solicitudpagina;
									$part= 1;
									for($g=0; $g < (count($paginat)); $g++)
									{
											
											 
								?>
                                        <tr >
											<?php 
												/*numero de referencia entrada*/
												if($paginat[$g]["ATRCOD"]=='0003'){
                                            ?>
                                            		<td style="text-align:center;vertical-align:text-top;"><?php echo $paginat[$g]["ADSNRO"]!=''?add_ceros($paginat[$g]["ADSNRO"],6):'&nbsp;'; echo "<strong>(D)</strong>";?></td>
                                            <?php
                                            	}else{
                                            ?>
                                            		<td style="text-align:center;vertical-align:text-top;"><?php echo $paginat[$g]["ATRNUM"]!=''?add_ceros($paginat[$g]["ATRNUM"],6):'&nbsp;'; echo '(E)';?></td>
                                            <?php
                                            	}
                                            ?>
                                            
                                            <?php //codigo de articulo ?>
                                            <td valign="middle" style="text-align:left;vertical-align:text-top;" ><div>&nbsp;<?php echo $paginat[$g]["AARCOD"]; ?></div></td>
                                            
                                            <?php //descripcion de articulo ?>
                                            <td valign="middle" style="text-align:left;vertical-align:text-top;" ><div><?php echo $paginat[$g]["AARDES"]; ?></div></td>
                                            
                                            <?php //ubicacion ?>
                                            <td valign="middle" style="text-align:left;vertical-align:text-top;" >
                                                <div>
													<?php 
														$list = list_ubiart($cid, $Compania, '0001', $paginat[$g]["AARCOD"],2);
														if(trim($list)!=''){ echo $list; } 
														else { echo ' - Sin Ubicaci�n'; }
                                                    ?>
                                                </div>
                                            </td>
                                            
                                            <?php //marca articulo ?>
                                            <td valign="middle" style="text-align:left;vertical-align:text-top;" ><div><?php echo $paginat[$g]["AMCDES"]; ?></div></td>
                                            
                                            <!-- entradas -->
                                            
                                            <?php //numero de guias ?>
                                            <?php 
                                            	if($paginat[$g]["ATRCOD"]=='0003'){
                                            ?>
                                            		<td style="text-align:center;vertical-align:text-top;"><?php echo $paginat[$g]["NROGIA"]!=''?$paginat[$g]["NROGIA"]:'N/A';?></td>
                                            <?php
                                            	}else{
                                            ?>
                                            		<td style="text-align:center;vertical-align:text-top;"><?php echo $paginat[$g]["NROGIA"]!=''?$paginat[$g]["NROGIA"]:'---&nbsp;';?></td>
                                            <?php
                                            	}
                                            ?>
                                            <?php //fecha ?>
                                            <td style="text-align:center;vertical-align:text-top;">&nbsp;<?php echo $paginat[$g]["ATRFEC"]!=''?formatDate($paginat[$g]["ATRFEC"],'aaaa-mm-dd','dd/mm/aaaa' ):'&nbsp;';?></td>
                                            
                                            <?php //cantidad ?>
                                            <td style="text-align:center;vertical-align:text-top;">&nbsp;<?php echo $paginat[$g]["ATRCAN"]!=''?number_format($paginat[$g]["ATRCAN"],2,',',''):'&nbsp;';?></td>
                                            
                                            <?php //descripcion ?>
                                            <td style="text-align:center;vertical-align:text-top;">&nbsp;<?php echo $paginat[$g]["DESATR"]!=''?$paginat[$g]["DESATR"]:'&nbsp;';?></td>
                                            
                                            <?php //observaciones ?>
                                            <!--<td style="text-align:center;vertical-align:text-top;">&nbsp;<?php echo $paginat[$g]["ATROBS"]!=''?$paginat[$g]["ATROBS"]:'&nbsp;';?></td>-->
                                        </tr>
                                 <?php
									}	
								 ?>                                            
                  </tbody>
                </table>
</div>
		  </div>
	  </div>
	</div>
		<!-- end #content -->
		<!-- end #sidebar -->
		<div style="clear: both;">&nbsp;</div>
	</div>
	<!-- end #page -->

	<div id="footer">
		<div id="opcionesmasusadas"><?php include("../opcionesmasusadasphp.php"); ?></div>
	</div>
 
	<!-- end #footer -->
</body>
</html>
