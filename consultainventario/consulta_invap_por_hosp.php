<?php 
/*
 * jDavila
 * 12/01/2016
 */
session_start();
include("../conectar.php");
$tinva = $_GET['tinvan'];
//print_r($tinva);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
<title>Idasys V3</title>
<link href="../<?php echo $css; ?>" rel="stylesheet" type="text/css" media="screen" />
<script language="JavaScript" src="../javascript/javascript.js"></script>
<script language="JavaScript" src="../calendario/javascripts.js"></script>
<script language="JavaScript" src="javascript.js"></script>
<script language="JavaScript" src="../javascript/jquery.js"></script>
<link href="../shadowbox.css"  rel="stylesheet" type="text/css"/>
<script src="../shadowbox.js" type="text/javascript"/> </script>
<script language="JavaScript" type="text/JavaScript">
Shadowbox.init({overlayOpacity: "0.5"});
</script>
<style type="text/css" title="currentStyle">
			@import "../DataTables-1.9.4/media/css/<?php echo $demo_page;?>";
			@import "../DataTables-1.9.4/media/css/<?php echo $demo_table;?>";
</style>
<script type="text/javascript" language="javascript" src="../DataTables-1.9.4/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf-8">
		
			
			
			 $(document).ready(function() 
			 {
			      document.getElementById('info').style.display="";

				 $('#info').dataTable( 
				  {
					"bStateSave": true,
			        "oLanguage": 
					{
			          	"sLengthMenu": "Mostrar _MENU_ registros por Pag.",
						"sZeroRecords": "No Existen Registros",
						"sInfo": "Mostrando desde _START_ hasta _END_ de _TOTAL_ registros",
						"sInfoEmpty": "Mostrando desde 0 hasta 0 de 0 registros",
						"sInfoFiltered": "(filtrando de _MAX_ de registros)",
						"sSearch": "Buscar",
						"oPaginate": 
						{
							"sNext": "Siguiente",
							"sPrevious": "Anterior"
						}
			        }
			      } );
			  } );
</script>

</head> 
<body background="../images/fondo idaca.jpg" >
<div id="wrapper">
  <?php include("../superior.php");?>
  <div id="page">
     <?php  include("../validar.php");?>
 		<?php 
				
				if ($bandera==1) 
				{
					$wsolicitud=0;
					if ($solicitudpagina==0) 	
					{

						
						$tipoInve ='';
						if(!empty($tinva)){
							foreach($tinva as $key => $value){
								$tipoInve .= "'".$value."',";
							}
							$tipoInve = substr($tipoInve,0, (strripos($tipoInve,",")));
						}
						
						$sql="SELECT T2.ADSNRO, T3.ATSCOD, T3.ATSDES, T2.ATRFEC, T1.ATRCAN, T1.AARCOD, T5.AARDES, T2.ATROBS, T4.AISCOD, T4.AISDES,
								(SELECT T6.AAPVLA FROM IV46FP T6 WHERE T6.ACICOD=T1.ACICOD and T6.ADSNRO=T2.ADSNRO AND T6.APACOD IN ('1501') ) AS N1501,
								(SELECT T6.AAPVLA FROM IV46FP T6 WHERE T6.ACICOD=T1.ACICOD and T6.ADSNRO=T2.ADSNRO AND T6.APACOD IN ('1510') ) AS N1510,
								(SELECT T6.AAPVLA FROM IV46FP T6 WHERE T6.ACICOD=T1.ACICOD and T6.ADSNRO=T2.ADSNRO AND T6.APACOD IN ('1509') ) AS N1509 
							FROM IV36FP T1 
								INNER JOIN IV05FP T5 ON (T1.ACICOD=T5.ACICOD AND T1.AARCOD=T5.AARCOD)
								INNER JOIN IV35FP T2 ON (T1.ACICOD=T2.ACICOD AND T1.AALCOD= T2.AALCOD AND T1.ADPCOD=T2.ADPCOD AND T1.ATSCOD= T2.ATSCOD AND T1.ADSNRO=T2.ADSNRO) 
								INNER JOIN IS15FP T3 ON (T1.ACICOD=T3.ACICOD AND T2.ATSCOD=T3.ATSCOD) 
								INNER JOIN IV42FP T4 ON (T2.ACICOD=T4. ACICOD AND T2.AISCOD=T4.AISCOD) 
							WHERE T1.ACICOD='$Compania' AND 
								  T1.AALCOD not in ('0006', '0007', '0001') AND 
								  T3.ATSCOD in ('01','04') AND
								  T2.ATRFEC BETWEEN '$desde' AND '$hasta'
							";
						if(!empty($tipoInve)){
							$sql.="  AND T5.ATICOD IN (".$tipoInve.")";
						}
						$sql.=" ORDER BY T4.AISDES, T2.ATRFEC ";
						
						//echo $sql."<br/><br/>";
						$resultt=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111"));
						
						$z=0;
						$tothon=0;
						$totest=0;
						$lin=1;
						$limitep=$_SESSION['solicitudlineasporpaginat'];
						$pag=1;
						$primero='S';
						
						
						while(odbc_fetch_row($resultt))
						{
							
							$jml = odbc_num_fields($resultt);
							$row[$z]["pagina"] =  $pag;
							for($i=1;$i<=$jml;$i++)
							{	


								$row[$z][odbc_field_name($resultt,$i)] =  odbc_result($resultt,$i);
							}
							$z++;
							if ($lin>=$limitep) 
							{
								$limitep+=$_SESSION['solicitudlineasporpaginat'];
								$pag++;
							}
							$lin++;
						}
						$totsol=($lin-1);
						$_SESSION['totalsolicitudes']=$totsol;
						$_SESSION['solicitudarreglo']=$row;
						$solicitudpagina=1;
						$_SESSION['solicitudpaginas']=$pag;
					}//fin de solicitudpagina
					/*se muestra la cantidad de elementos segun la solicitada en $solicitudpagina*/
					$paginat=$_SESSION['solicitudarreglo'];
				}
			?>
        <div id="content2" >   
        	<table width="100%"   border="0">
              <tr>
                <td width="65%" scope="col" colspan="2"><h1  class="title">Reporte de Salidas por Hospital Meditron-INVAP</h1><hr /></td>
                <td width="13%" scope="col"><div align="left">
                  <table width="100%"  border="0">
                    <tr>
                      <th width="30%" scope="col" >
                      <div class="flechas">
                      	<a href="exportaraexcel_invap_por_hosp_invap.php?&desde=<?php echo $desde; ?>&hasta=<?php echo $hasta; ?>&tinvan=<?php echo $url222 = urlencode( serialize($tinva) );?>" target="_blank">
                        	<img src="../images/excel.jpg" alt="" width="30" height="30" title="Exportar a Excel Reporte INVAP"/>
                        </a>
                        <!--
                        <a href="exportaraexcel_invap_por_hosp_medi.php?&desde=<?php echo $desde; ?>&hasta=<?php echo $hasta; ?>&tinvan=<?php echo $url222 = urlencode( serialize($tinva) );?>" target="_blank">
                        	<img src="../images/excel.jpg" alt="" width="30" height="30" title="Exportar a Excel Reporte MEDI-INVAP"/>
                        </a>
                        -->
                      </div></th>
                    </tr>
                  </table>
                </div></td>
              </tr>
              <tr>
              	<td colspan="3">
                	<form name="form" id="form" method="post" on>
                    <input type="hidden" name="bandera" id="bandera" value="<?php echo $bandera;?>" />
                    <input type="hidden" name="aalcod" id="aalcod" value="0001" />                    
                	<table>
                    	<tr>
                        	<td>&nbsp;Desde:</td>
                            <td>&nbsp;<?php echo  escribe_formulario_fecha_vacio('desde','form',$desde); ?></td>
                            <td>&nbsp;</td>
                            <td>&nbsp;Hasta:</td>
                            <td>&nbsp;<?php echo  escribe_formulario_fecha_vacio('hasta','form',$hasta); ?></td>
                            <td rowspan="2" ><a href="javascript:busqueda5();"><img src="../images/precesarconsulta.png" width="50" height="50" alt="Realizar Consulta" style="border:none;"></a></td>
                        </tr>
                        <tr>
                        	<td colspan="5">
                                <table>
                                    <tr>
                                        <td >Tipo de Inventario</td>
                                        <td >
                                            <div id="table_tpinven">
                                            </div>
                                            <script>cargarSelectTpInven();</script>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    </form>
                </td>
              </tr>
            </table>   
		<div id="container">
            <div id="demo">
                <table width="100%" id="info"  border="0" style="display:none;">
                  <thead>
                    <tr>
                    	<th scope="col" style="background-color:rgb(204,204,204)"><strong>Hospital</strong></th>
                        <th scope="col" style="background-color:rgb(204,204,204)"><strong>Fecha</strong></th>
                        <th scope="col" style="background-color:rgb(204,204,204)"><strong>Referencia de Salida</strong></th>
                        <th scope="col" style="background-color:rgb(204,204,204)"><strong>Cantidad</strong></th>
                        <th scope="col" style="background-color:rgb(204,204,204)"><strong>C�digo Art�culo</strong></th>
                        <th scope="col" style="background-color:rgb(204,204,204)"><strong>Art�culo</strong></th>
                        <th scope="col" style="background-color:rgb(204,204,204)"><strong>Nro. Hoja de Servicio</strong></th>
                        <th scope="col" style="background-color:rgb(204,204,204)"><strong>N�mero de ITEM</strong></th>
                        <th scope="col" style="background-color:rgb(204,204,204)"><strong>ITEM</strong></th>
                    </tr>
                 </thead> 
                 <tbody>
        						<?php
									//print_r($paginat);
									$pagact=$solicitudpagina;
									$part= 1;
									for($g=0; $g < (count($paginat)); $g++)
									{
								   
								?>
                                        <tr >
                                            <td style="text-align:center;vertical-align:middle;"><?php echo $paginat[$g]['AISDES']!=''?$paginat[$g]['AISDES']:'&nbsp;';?></td>
                                            <td style="text-align:center;vertical-align:middle;"><?php echo $paginat[$g]['ATRFEC']!=''?formatDate($paginat[$g]['ATRFEC'],'aaaa-mm-dd','dd/mm/aaaa' ):'&nbsp;';?></td>
                                            <td style="text-align:center;vertical-align:middle;"><?php echo $paginat[$g]['ADSNRO']!=''?add_ceros($paginat[$g]['ADSNRO'],6):'&nbsp;'; echo "(S)";?></td>
                                            <td style="text-align:center;vertical-align:middle;"><?php echo $paginat[$g]['ATRCAN']!=''?number_format($paginat[$g]['ATRCAN'],0,',','.'):'&nbsp;';?></td>
                                            <td style="text-align:center;vertical-align:middle;"><?php echo $paginat[$g]['AARCOD']!=''?$paginat[$g]['AARCOD']:'&nbsp;';?></td>
                                            <td style="text-align:center;vertical-align:middle;"><?php echo $paginat[$g]['AARDES']!=''?$paginat[$g]['AARDES']:'&nbsp;';?></td>
                                            <td style="text-align:center;vertical-align:middle;"><?php echo $paginat[$g]['N1501']!=''?$paginat[$g]['N1501']:'--';?></td>
                                            <td style="text-align:center;vertical-align:middle;"><?php echo $paginat[$g]['N1510']!=''?$paginat[$g]['N1510']:'--';?></td>
                                            <td style="text-align:center;vertical-align:middle;"><?php echo $paginat[$g]['N1509']!=''?$paginat[$g]['N1509']:'--';?></td>
                                        </tr>
								<?php
									}	
								?>     
                       </tbody>                                         
                </table>
			</div>
		  </div>
	  </div>
	</div>
		<!-- end #content -->
		<!-- end #sidebar -->
		<div style="clear: both;">&nbsp;</div>
	</div>
	<!-- end #page -->

	<div id="footer">
		<div id="opcionesmasusadas"><?php include("../opcionesmasusadasphp.php"); ?></div>
	</div>
 	<?php 
		echo "<script>
				var list = [];
				var pos = 1;
				var lista = \"".$tipoInve."\";
				$('input[name=\"tinvan[]\"]').each(function() { 
					//alert($(this).val() );
					if( lista.indexOf($(this).val()) != -1 ){
						//alert('paso '+$(this).val()) ;
						 $(this).attr('checked', true); 
					}
					pos++;
				});
			  </script>";
	?>
	<!-- end #footer -->
</body>
</html>
