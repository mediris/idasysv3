<?php session_start();
include("../conectar.php");
$arqnro = trim($_GET["num"]);
header("Pragma: ");
header('Cache-control: ');
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Content-type: application/vnd.ms-excel");
header("Content-disposition: attachment; filename=Reporte_Salidas.xls");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Reporte Salidas</title>
</head>
<style>

h1, h2, h3, h4, h5 {
	margin: 0;
	padding: 0;
	font-weight: normal;
	color: #32639A;
}

h1 {
	font-size: 2em;
}

h2 {
	font-size: 2.4em;	
}

h3 {
	font-size: 1.6em;
	font-style: italic;
}
h4 {
	font-size: 1.6em;
	font-style: italic;
	color: #FFF;
}
h5 {
	font-size: 1.0em;
	font-style: italic;
	color: #666;
}

#background-image
{
	font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
	font-size: 10px;
	margin: 0px;
	width: 100%;
	text-align: left;
	border-collapse: collapse;
}
#background-image th
{
	padding: 12px;
	font-weight: normal;
	font-size: 12px;
	color: #339;
	border-bottom-style: solid;
	border-left-style: none;
	text-align: center;
}
#background-image td
{
	color: #669;
	border-top: 1px solid #fff;
	padding-right: 4px;
	padding-left: 4px;
}
#background-image tfoot td
{
	font-size: 9px;
}
#background-image tbody 
{

	background-repeat: no-repeat;
	background-position: left top;
}
#background-image tbody td
{
	background-image: url(images/backn.png);
}
* html #background-image tbody td
{
	/* 
	   ----------------------------
		PUT THIS ON IE6 ONLY STYLE 
		AS THE RULE INVALIDATES
		YOUR STYLESHEET
	   ----------------------------
	*/
	filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='images/backn.png',sizingMethod='crop');
	background: none;
}	
</style>
<body>
<?php 
				
				
		 		$wsolicitud=0;
				if ($solicitudpagina==0) 	
					{
						$sql="SELECT T3.AARUMB,T4.AUMDES, T1.ACICOD, T1.AARCOD, T1.AARNIV, T1.ATICOD, T1.ASICOD, T1.AARDES, T1.AARTDT, T1.AARSTS 
								FROM IV05FP T1, IV06FP T3, IV13FP T4 
								WHERE T1.ACICOD='".$Compania."' AND T1.ACICOD=T3.ACICOD AND T1.AARCOD=T3.AARCOD AND T1.ACICOD=T4.ACICOD AND T3.AARUMB=T4.AUMCOD AND 
								( T1.AARCOD IN (SELECT T2.AARCOD FROM IV40FP T2 WHERE T2.ACICOD=T1.ACICOD AND T2.AALCOD='".$aalcod."' GROUP BY   
								T2.AARCOD ORDER BY T2.AARCOD ) ) ORDER BY T1.AARDES ";
							
						$resultt=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111"));
						//echo $sql."<br/>";
						$z=0;
						$tothon=0;
						$totest=0;
						$lin=1;
						$limitep=$_SESSION['solicitudlineasporpaginat'];
						$pag=1;
						$primero='S';
						
						//$anio = date("Y");
						//echo "anio:".$anio;
						
						if($anio<date("Y")){
							$mes1 = '12';
						}else{
							$mes1 = date("m");
						}
						//echo "mes1:".$mes1;
						while(odbc_fetch_row($resultt))
						{
							$artcod = odbc_result($resultt,'AARCOD');
							$atrdes = odbc_result($resultt,'AARDES');
							$aumdes = odbc_result($resultt,'AUMDES');

							$sql2="SELECT ('".$artcod."') as AARCOD, ('".$atrdes."') as AARDES, ('".$aumdes."') as AUMDES, ";
							$canmese = '';
							for($k=1; $k<=(int)$mes1; $k++){
								$canmese .= "(SELECT SUM(T3.ATRCAN) AS ATRCAN 
									FROM IV16FP T3 
									INNER JOIN IV15FP T4 ON ( T3.ACICOD=T4.ACICOD AND T3.AALCOD=T4.AALCOD AND T3.ATRCOD=T4.ATRCOD AND T3.ATRNUM=T4.ATRNUM ) 
									INNER JOIN IV12FP T5 ON ( T4.ACICOD=T5.ACICOD AND T4.ATRCOD=T5.ATRCOD AND T5.ATRSIG='-') 
									WHERE  T4.ACICOD='".$Compania."' AND T4.AALCOD='".$aalcod."' AND month(T4.ATRFEC)='".$k."' AND YEAR(T4.ATRFEC)= '".$anio."' AND T3.ATRART=T6.ATRART 
									GROUP BY T3.ATRART ORDER BY T3.ATRART )AS CAN".$k." , ";
							}
							$canmese = substr($canmese,0, (strripos($canmese,",")));
							$canmese = $canmese." ";
									 
							$sql2.= $canmese."  FROM IV16FP T6 
								  WHERE T6.ACICOD='".$Compania."' AND T6.AALCOD='".$aalcod."' AND T6.ATRART='".$artcod."'  
								  GROUP BY T6.ATRART ";
							//echo $sql2."<br/><br/>";
							//echo "mese:".$meses;
							$resultt2=odbc_exec($cid,$sql2)or die(exit("Error en odbc_exec 11111 - ".odbc_error($resultt2) ));
							while(odbc_fetch_row($resultt2))
							{
								$jml = odbc_num_fields($resultt2);
								$row[$z]["pagina"] =  $pag;
								for($i=1;$i<=$jml;$i++)
								{	if (odbc_field_name($resultt2,$i)=='ARGTTE') {$totest+=odbc_result($resultt2,$i);}
									if (odbc_field_name($resultt2,$i)=='ARGTTM') {$tothon+=odbc_result($resultt2,$i);}
									$row[$z][odbc_field_name($resultt2,$i)] =  odbc_result($resultt2,$i);
								}
								$z++;
								if ($lin>=$limitep) 
								{
									$limitep+=$_SESSION['solicitudlineasporpaginat'];
									$pag++;
								}
								$lin++;
							}
						}
						$totsol=($lin-1);
						$_SESSION['totalsolicitudes']=$totsol;
						$_SESSION['solicitudarreglo']=$row;
						$solicitudpagina=1;
						$_SESSION['solicitudpaginas']=$pag;
					}//fin de solicitudpagina
					/*se muestra la cantidad de elementos segun la solicitada en $solicitudpagina*/
					$paginat=$_SESSION['solicitudarreglo'];
			?>      
<table width="100%" border="0">
<tr>
<td height="89"><h1>
<?php if($Compania=='14'){?>
	<img src="http://<?php echo $Direccionip; ?>/idasysv3/images/logoidacadef2005.png" width="280" height="68" />
<?php }else{ ?>
	<img src="http://<?php echo $Direccionip; ?>/idasysv3/images/logomeditronnuevo.png" width="280" height="68" />
<?php } ?>
   </h1>
  <h5>RIF:  <?php echo $Companiarif; ?></h5></td>
</tr>
<tr>
<td>

<table width="100%" id="background-image" >
	
  <thead>
  	<tr>
        <th colspan="<?php echo ($mes1+3);?>" scope="col"><h3>Reporte Movimiento Anual de Salidas</h3></th>
    </tr>
    <tr>
        <th colspan="<?php echo ($mes1+3);?>" scope="col"><h4>Elaborado el <?php echo $Fechaactual; ?> a las <?php echo $Horaactual2; ?></h4></th>
    </tr>
    <tr>
        <th colspan="<?php echo ($mes1+3);?>" scope="col"><h3>Reporte del A�o <?php echo $anio;?></h3></th>
    </tr>
  	<tr>
        <th colspan="<?php echo ($mes1+3);?>" scope="col"><h3>Almac�n: <?php echo alamcen($aalcod, $Compania);?></h3></th>
    </tr>
  	<tr>
       	<th scope="col">Cod. Art�culo</th>
        <th scope="col">Art�culo</th>
        <th scope="col">Unidad de Medida</th>
        <?php for($k=1; $k<=(int)$mes1; $k++){ ?>
        	<th scope="col"><?php echo mesescrito($k);?></th>
        <?php } ?>
    </tr>
  </thead>
    <tbody>
			<?php 
                $show = false;
                //print_r($paginat);
                $pagact=$solicitudpagina;
                for($g=0; $g < (count($paginat)); $g++)
                {
            ?>
                 <tr>
                        <td scope="col"><div>&nbsp;<?php echo $paginat[$g]["AARCOD"];?></div></td>
                        <td scope="col"><div><?php echo $paginat[$g]["AARDES"];?></div></td>
                        <td scope="col"><div align="center"><?php echo $paginat[$g]["AUMDES"];?></div></td>
                        <?php for($h=1; $h<=(int)$mes1; $h++){ ?>
                            <td scope="col"><div align="right">&nbsp;<?php $val= "CAN".$h; echo number_format($paginat[$g][$val],2,",",".");?></div></td>
                        <?php } ?>
                        
                    </tr>
            <?php }?>      
    </tbody>

</table>
</td>
</tr>
</table>
</body>
</html>
