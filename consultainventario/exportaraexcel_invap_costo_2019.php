<?php 
session_start();
include("../conectar.php");
$arqnro = trim($_GET["num"]);
//header("Pragma: ");
header("Pragma: no-cache");
header('Cache-control: ');
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Content-type: application/vnd.ms-excel");
//header("Content-type: application/octet-stream");
header("Content-disposition: attachment; filename=Reporte_Costos_INVAP_".formatDate($Fechaactual,'dd.mm.aaa','aaaa_mm_dd').".xls");

?>
 

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Reporte Cv</title>
	</head>
	<style>

		h1, h2, h3, h4, h5 {
			margin: 0;
			padding: 0;
			font-weight: normal;
			color: #32639A;
		}

		h1 {
			font-size: 2em;
		}

		h2 {
			font-size: 2.4em;	
		}

		h3 {
			font-size: 1.6em;
			font-style: italic;
		}
		h4 {
			font-size: 1.6em;
			font-style: italic;
			color: #FFF;
		}
		h5 {
			font-size: 1.0em;
			font-style: italic;
			color: #666;
		}

		#background-image{
			font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
			font-size: 10px;
			margin: 0px;
			width: 100%;
			text-align: left;
			border-collapse: collapse;
		}

		#background-image th{
			padding: 12px;
			font-weight: normal;
			font-size: 12px;
			color: #339;
			border-bottom-style: solid;
			border-left-style: none;
			text-align: center;
		}

		#background-image td{
			color: #669;
			border-top: 1px solid #fff;
			padding-right: 4px;
			padding-left: 4px;
		}

		#background-image tfoot td{
			font-size: 9px;
		}

		#background-image tbody {

			background-repeat: no-repeat;
			background-position: left top;
		}

		#background-image tbody td{
			background-image: url(images/backn.png);
		}

		* html #background-image tbody td{
			/* 
			   ----------------------------
				PUT THIS ON IE6 ONLY STYLE 
				AS THE RULE INVALIDATES
				YOUR STYLESHEET
			   ----------------------------
			*/
			filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='images/backn.png',sizingMethod='crop');
			background: none;
		}
	</style>
	<body>
		<?php 		
 		$wsolicitud=0;
		if ($solicitudpagina==0){
						
			$z=0;
			$tothon=0;
			$totest=0;
			$lin=1;
			$limitep=$_SESSION['solicitudlineasporpaginat'];
			$pag=1;
			$primero='S';

			$sql="SELECT T1.ATRNUM, T1.ATRDES, T1.ATRFEC, T1.ATRFLT, T2.ATRART, T3.AARCOD, T3.AARDES, T4.AMCCOD, T5.AMCDES, T6.ATIDES, T2.ATRCAN, T2.ATRCUT, T2.ATRFLP, T2.ATRFLE, T1.ATRCTT, (SELECT T6.AAPVLA FROM IV38FP T6 WHERE T6.ACICOD = T1.ACICOD AND T6.ATRNUM = T1.ATRNUM AND T6.ATRCOD = T1.ATRCOD AND T6.APACOD IN ('1506')) AS NROGIA

			FROM IV15FP T1
			INNER JOIN IV16FP T2 ON (T1.ACICOD = T2.ACICOD AND T1.ATRNUM = T2.ATRNUM AND T1.AALCOD = T2.AALCOD AND T1.ATRCOD = T2.ATRCOD)
			INNER JOIN IV05FP T3 ON (T1.ACICOD = T3.ACICOD AND T2.ATRART = T3.AARCOD)
			INNER JOIN IV06FP T4 ON (T1.ACICOD = T4.ACICOD AND T2.ATRART = T4.AARCOD)
			INNER JOIN IV04FP T5 ON (T1.ACICOD = T5.ACICOD AND T4.AMCCOD = T5.AMCCOD)
			INNER JOIN IV01FP T6 ON (T1.ACICOD = T6.ACICOD AND T3.ATICOD = T6.ATICOD)

			WHERE T1.ACICOD = '$Compania' AND T1.AALCOD = '0001' AND T1.ATRCOD = '0001'";
						
			//echo $sql."<br/><br/>";
			$resultt=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111"));
						
			while(odbc_fetch_row($resultt)){
				$jml = odbc_num_fields($resultt);
				$row[$z]["pagina"] =  $pag;
				for($i=1;$i<=$jml;$i++){	
					$row[$z][odbc_field_name($resultt,$i)] =  odbc_result($resultt,$i);
				}
				$z++;
				if ($lin>=$limitep){
					$limitep+=$_SESSION['solicitudlineasporpaginat'];
					$pag++;
				}
				$lin++;
			}

			$totsol=($lin-1);
			$_SESSION['totalsolicitudes']=$totsol;
			$_SESSION['solicitudarreglo']=$row;
			$solicitudpagina=1;
			$_SESSION['solicitudpaginas']=$pag;
		}//fin de solicitudpagina
		/*se muestra la cantidad de elementos segun la solicitada en $solicitudpagina*/
		$paginat = $_SESSION['solicitudarreglo']; ?>      
		<table width="100%" border="0">
	    	<tr>
	        	<td height="89">
	        		<h1>
						<?php if($Compania=='14'){?>
		                	<img src="http://<?php echo $Direccionip; ?>/idasysv3/images/logoidacadef2005.png" width="280" height="68" />
		            	<?php }else if($Compania=='40'){?>
			                <img src="http://<?php echo $Direccionip; ?>/idasysv3/images/MEDITRON_logo_rif.png" width="300" />
		            	<?php }else{ ?>
		                	<img src="http://<?php echo $Direccionip; ?>/idasysv3/images/logomeditronnuevo.png" width="280" height="68" />
		            	<?php } ?>
	               </h1>
	          		<h5>RIF:  <?php echo $Companiarif; ?></h5>
	        	</td>
	    	</tr>
			<table width="100%" id="background-image" >
				<thead>
	  				<tr>
	        			<th colspan="12" scope="col"><h2>Reporte de Inventario con Costos de Entrada Meditron-INVAP 2019</h2></th>
	    			</tr>
	    			<tr>
	        			<th colspan="12" scope="col">Elaborado el <?php echo $Fechaactual; ?> a las <?php echo $Horaactual2; ?></th>
	    			</tr>
	  				<tr>
	        			<th colspan="12" scope="col">Almacén Principal: INVAP</th>
	    			</tr>
					<tr style="border-bottom:solid;">
	                	<th scope="col" rowspan="2" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)">Código</th>
	               		<th scope="col" rowspan="2" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)">Artículo</th>
	                    <th scope="col" rowspan="2" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)">Ubicación</th>
	                    <th scope="col" rowspan="2" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)">Marca</th>
	                    <th scope="col" rowspan="2" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)">Tipo</th>
	                    <th scope="col" rowspan="2" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)"><strong>N° Referencia Entrada</strong></th>
	                    <th scope="col" rowspan="2" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)"><strong>N° Guía</strong></th>
	                	<th scope="col" colspan="9" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;border-right:solid;background-color:rgb(204,204,204); font-size:medium;">Entradas</th>
	                </tr>
	                <tr>
	                    <th scope="col" style="border-width:thin;border-bottom:solid;border-left:solid;solid;background-color:rgb(204,204,204)"><strong>Fecha</strong></th>
	                    <th scope="col" style="border-width:thin;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)"><strong>Cantidad</strong></th>
	                    <th scope="col" style="border-width:thin;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)"><strong>Descripción</strong></th>
	                    <th scope="col" style="border-width:thin;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)"><strong>Costo Unitario</strong></th>
	                    <th scope="col" style="border-width:thin;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)"><strong>Costo Total</strong></th>
	                    <th scope="col" style="border-width:thin;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)"><strong>Valor %</strong></th>
	                    <th scope="col" style="border-width:thin;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)"><strong>Flete y Seguro</strong></th>
	                    <th scope="col" style="border-width:thin;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)"><strong>Total General ITEM</strong></th>
	                    <th scope="col" style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;background-color:rgb(204,204,204)"><strong>Total Factura</strong></th>
	                </tr>
	          	</thead>
				<tbody>
					<?php 
					$pagact=$solicitudpagina;
					$part= 1;
					for($g=0; $g < (count($paginat)); $g++){ 
						// echo "//**".$paginat[$g]["ATRART"]."<br>";
						$rowpos = 0;

						/*entradas*/
						$sql2 = "SELECT *

						FROM IV16FP

						WHERE ACICOD = '".$Compania."' AND AALCOD = '0001' AND ATRCOD = '0001' AND ATRNUM = '".$paginat[$g]["ATRNUM"]."'";

						$result2 = odbc_exec($cid,$sql2)or die(exit("Error en odbc_exec 11111"));
						$result22 = odbc_exec($cid,$sql2)or die(exit("Error en odbc_exec 11111"));
						$canEnt = 0;

						while(odbc_fetch_row($result22)){
							$canEnt++;
						}

						$rowpos++;
						?>
				 		<tr>
				 			<!-- Código del artículo -->                            
				 			<td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:left;vertical-align:middle;" valign="middle" ><?php echo utf8_encode($paginat[$g]["ATRART"]); ?></td>

				 			<!-- Descripción del artículo -->
                            <td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:left;vertical-align:middle;" valign="middle" ><div><?php echo utf8_encode($paginat[$g]["AARDES"]); ?></div></td>

                            <!-- Ubicacion del articulo -->
                            <td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:left;vertical-align:middle;" valign="middle" >
                             	<?php 
                                    $list = list_ubiart($cid, $Compania, '0001', $paginat[$g]["AARCOD"],2);
                                    if(trim($list)!=''){
                                        echo $list; 
                                    }else{ 
                                        echo ' - Sin Ubicación';
                                    }
                             	?>
                            </td>

                            <!-- Marca del articulo -->
                            <td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:left;vertical-align:middle;" valign="middle" ><?php echo $paginat[$g]["AMCDES"]; ?></td>
							
							<?php if($paginat[$g - 1]["ATRNUM"] != $paginat[$g]["ATRNUM"]){ ?> 
	                            <!-- Tipo del articulo -->
	                            <td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:left;vertical-align:middle;" valign="middle" rowspan="<?php echo $canEnt; ?>"><?php echo utf8_encode($paginat[$g]["ATIDES"]); ?>
	                            </td>

		                        <!-- Nro de entrada de la transaccion -->
	                            <td style="border-width:thin;border-bottom:solid;border-left:solid;text-align:center;vertical-align:middle;" valign="middle" rowspan="<?php echo $canEnt; ?>"><?php echo $paginat[$g]["ATRNUM"] != '' ? add_ceros($paginat[$g]["ATRNUM"],6) : '&nbsp;'; echo ' (E)';?></td>

	                            <!-- Nro de guia de la transaccion -->
	                            <td style="border-width:thin;border-bottom:solid;border-left:solid;text-align:center;vertical-align:middle;" valign="middle" rowspan="<?php echo $canEnt; ?>"><?php echo $paginat[$g]["NROGIA"] != '' ? utf8_encode($paginat[$g]["NROGIA"]) : '---&nbsp;';?></td>

	                            <!-- Fecha de la transaccion -->
	                            <td style="border-width:thin;border-bottom:solid;border-left:solid;text-align:center;vertical-align:middle;" valign="middle" rowspan="<?php echo $canEnt; ?>"><?php echo $paginat[$g]["ATRFEC"] != '' ? formatDate($paginat[$g]["ATRFEC"], 'aaaa-mm-dd','dd/mm/aaaa' ) : '&nbsp;';?></td>
	                        <?php } ?>

                       		<!-- Cantidad del articulo -->         
                        	<td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:center;vertical-align:middle;"><?php echo $paginat[$g]["ATRCAN"] != '' ? number_format($paginat[$g]["ATRCAN"],2,',','') : '&nbsp;';?></td>

                        	<?php if($paginat[$g - 1]["ATRNUM"] != $paginat[$g]["ATRNUM"]){ ?>
	                        	<!-- Descripcion de la transaccion -->
                            	<td style="border-width:thin;border-bottom:solid;border-left:solid;text-align:center;vertical-align:middle;" valign="middle" rowspan="<?php echo $canEnt; ?>"><?php echo $paginat[$g]["ATRDES"] != '' ? utf8_encode($paginat[$g]["ATRDES"]) : '&nbsp;';?></td>
	                        <?php } ?>

                            <!-- Costo unitario del articulo -->
                            <td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:center;vertical-align:middle;"><?php echo $paginat[$g]["ATRCUT"] != '' ? number_format($paginat[$g]["ATRCUT"],2,',','') : '&nbsp;';?></td>

                            <!-- Costo total del articulo -->
                            <td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:center;vertical-align:middle;"><?php echo  $paginat[$g]["ATRCUT"] != '' ? number_format(($paginat[$g]["ATRCUT"] * $paginat[$g]["ATRCAN"]),2,',','') : '&nbsp;';?></td>

                            <!-- Valor % -->
                            <td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:center;vertical-align:middle;"><?php echo $paginat[$g]["ATRFLP"] != '' ? number_format($paginat[$g]["ATRFLP"],2,',','') : number_format(0,2,',','');?></td>

                            <!-- Flete y seguro -->
                            <td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:center;vertical-align:middle;"><?php echo $paginat[$g]["ATRFLE"] != '' ? number_format($paginat[$g]["ATRFLE"],2,',','') : number_format(0,2,',','');?></td>

                            <!-- Total general item -->
                            <td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:center;vertical-align:middle;"><?php echo $paginat[$g]["ATRCUT"] != '' && $paginat[$g]["ATRFLE"] != '' ? number_format(($paginat[$g]["ATRCUT"] * $paginat[$g]["ATRCAN"]) + $paginat[$g]["ATRFLE"],2,',','') : number_format(0,2,',','');?></td>
	                       
                            <?php if($paginat[$g - 1]["ATRNUM"] != $paginat[$g]["ATRNUM"]){ ?>

	                        	<!-- Total general factura -->
                        		<td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:center;vertical-align:middle;" valign="middle" rowspan="<?php echo $canEnt; ?>"><?php echo $paginat[$g]["ATRCTT"] ? number_format($paginat[$g]["ATRCTT"],2,',','') : number_format(0,2,',','');?></td>
	                        <?php } ?>
                        </tr>
	                <?php } ?>                                            
	            </tbody>
			</table>
		</table>
	</body>
</html>