<?php 
/*
 * Bonzalez
 * 22/03/2015
 */
session_start();
include("../conectar.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>Idasys V3</title>
<link href="../<?php echo $css; ?>" rel="stylesheet" type="text/css" media="screen" />
<script language="JavaScript" src="../javascript/javascript.js"></script>
<script language="JavaScript" src="../calendario/javascripts.js"></script>
<script language="JavaScript" src="javascript.js"></script>
<script language="JavaScript" src="../javascript/jquery.js"></script>
<link href="../shadowbox.css"  rel="stylesheet" type="text/css"/>
<script src="../shadowbox.js" type="text/javascript"/> </script>
<script language="JavaScript" type="text/JavaScript">
Shadowbox.init({overlayOpacity: "0.5"});
</script>
<style type="text/css" title="currentStyle">
			@import "../DataTables-1.9.4/media/css/<?php echo $demo_page;?>";
			@import "../DataTables-1.9.4/media/css/<?php echo $demo_table;?>";
</style>
<script type="text/javascript" language="javascript" src="../DataTables-1.9.4/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf-8">
		
			
			
			 $(document).ready(function() 
			 {
			      document.getElementById('info').style.display="";

				 $('#info').dataTable( 
				  {
					"bStateSave": true,
			        "oLanguage": 
					{
			          	"sLengthMenu": "Mostrar _MENU_ registros por Pag.",
						"sZeroRecords": "No Existen Registros",
						"sInfo": "Mostrando desde _START_ hasta _END_ de _TOTAL_ registros",
						"sInfoEmpty": "Mostrando desde 0 hasta 0 de 0 registros",
						"sInfoFiltered": "(filtrando de _MAX_ de registros)",
						"sSearch": "Buscar",
						"oPaginate": 
						{
							"sNext": "Siguiente",
							"sPrevious": "Anterior"
						}
			        }
			      } );
			  } );
</script>

</head> 
<body background="../images/fondo idaca.jpg" >
<div id="wrapper">
  <?php include("../superior.php");?>
  <div id="page">
     <?php  include("../validar.php");?>
 		<?php 
				
					
					$wsolicitud=0;
					if ($solicitudpagina==0) 	
					{

						$sql="SELECT T3.AARUMB, T4.AUMDES, T1.ACICOD, T1.AARCOD, T1.AARNIV, T1.ATICOD, T1.ASICOD, T1.AARDES, T1.AARTDT, T1.AARSTS, T5.AMCCOD,  T5.AMCDES, T7.ATIDES,
								(SELECT T6.ASLSAF FROM IV40FP T6 WHERE T6.ACICOD=T1.ACICOD AND T6.AARCOD=T1.AARCOD AND T6.AALCOD='0001' ORDER BY T6.ASLFEF DESC FETCH FIRST 1 ROWS ONLY) AS ASLSAF
							FROM IV05FP T1 
								INNER JOIN IV01FP T7 ON ( T1.ACICOD=T7.ACICOD AND T1.ATICOD=T7.ATICOD ) 
								INNER JOIN IV06FP T3 ON ( T1.ACICOD=T3.ACICOD AND T1.AARCOD=T3.AARCOD ) 
								INNER JOIN IV04FP T5 ON ( T1.ACICOD=T5.ACICOD AND T3.AMCCOD=T5.AMCCOD )
								INNER JOIN IV13FP T4 ON ( T1.ACICOD=T4.ACICOD AND T3.AARUMB=T4.AUMCOD ) 
							WHERE T1.ACICOD='$Compania' AND ( T1.AARCOD IN (SELECT T2.AARCOD FROM IV40FP T2 WHERE T2.ACICOD=T1.ACICOD AND T2.AALCOD='0001' GROUP BY T2.AARCOD ORDER BY T2.AARCOD ) ) 
							ORDER BY T1.AARDES";
						
						//echo $sql."<br/><br/>";
						$resultt=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111"));
						
						$z=0;
						$tothon=0;
						$totest=0;
						$lin=1;
						$limitep=$_SESSION['solicitudlineasporpaginat'];
						$pag=1;
						$primero='S';
						
						
						while(odbc_fetch_row($resultt))
						{
							
							$jml = odbc_num_fields($resultt);
							$row[$z]["pagina"] =  $pag;
							for($i=1;$i<=$jml;$i++)
							{	
								$row[$z][odbc_field_name($resultt,$i)] =  odbc_result($resultt,$i);
							}
							$z++;
							if ($lin>=$limitep) 
							{
								$limitep+=$_SESSION['solicitudlineasporpaginat'];
								$pag++;
							}
							$lin++;
						}
						$totsol=($lin-1);
						$_SESSION['totalsolicitudes']=$totsol;
						$_SESSION['solicitudarreglo']=$row;
						$solicitudpagina=1;
						$_SESSION['solicitudpaginas']=$pag;
					}//fin de solicitudpagina
					/*se muestra la cantidad de elementos segun la solicitada en $solicitudpagina*/
					$paginat=$_SESSION['solicitudarreglo'];
				
			?>
        <div id="content2" >   
        	<table width="100%"   border="0">
              <tr>
                <td width="65%" scope="col" colspan="2"><h1  class="title">Reporte de Inventario con Costos de Entrada Meditron-INVAP 2019</h1>
                  <hr /></td>
                <td width="13%" scope="col"><div align="left">
                  <table width="100%"  border="0">
                    <tr>
                      <th width="30%" scope="col" >
                      <div class="flechas">
                      	 <a href="exportaraexcel_invap_costo_2019.php" target="_blank">
                        	<img src="../images/excel.jpg" alt="" width="30" height="30" title="Exportar a Excel Reporte Costos INVAP"/>
                        </a>
                      </div></th>
                    </tr>
                  </table>
                </div></td>
              </tr>
            </table>   
		<div id="container">
            <div id="demo">
                <table width="100%" id="info" >
                  <thead>
                    <tr>
                    	<th scope="col" rowspan="2" style="background-color:rgb(204,204,204)">Código</th>
                   		<th scope="col" rowspan="2" style="background-color:rgb(204,204,204)">Artículo</th>
                        <th scope="col" rowspan="2" style="background-color:rgb(204,204,204)">Ubicación</th>
                        <th scope="col" rowspan="2" style="background-color:rgb(204,204,204)">Marca</th>
                        <th scope="col" rowspan="2" style="background-color:rgb(204,204,204)">Tipo</th>
                    	<th scope="col" colspan="7" style="background-color:rgb(204,204,204)">Entradas</th>
                        <!--<th scope="col" rowspan="2" style="background-color:rgb(204,204,204)">Existencia Actual</th>-->
                        
                    </tr>
                    <tr>
                        <th scope="col" style="background-color:rgb(204,204,204)"><strong>Nro. de Entrada</strong></th>
                        <th scope="col" style="background-color:rgb(204,204,204)"><strong>Nro. de Guía</strong></th>
                        <!--<th scope="col" ><strong>Tipo</strong></th>-->
                        <th scope="col" style="background-color:rgb(204,204,204)"><strong>Fecha</strong></th>
                        <th scope="col" style="background-color:rgb(204,204,204)"><strong>Cantidad</strong></th>
                        <th scope="col" style="background-color:rgb(204,204,204)"><strong>Descripción</strong></th>
                        <!--<th scope="col" style="background-color:rgb(204,204,204)"><strong>Observaciones</strong></th>-->
                        <th scope="col" style="background-color:rgb(204,204,204)"><strong>Costo Unitario $</strong></th>
                        <th scope="col" style="background-color:rgb(204,204,204)"><strong>Costo Total $</strong></th>
                        
                        
                    </tr>
                  </thead>
    				<tbody>
        						<?php
									//print_r($paginat);
									$pagact=$solicitudpagina;
									$part= 1;
									for($g=0; $g < (count($paginat)); $g++)
									{
											//echo "//**".$paginat[$g]["AARCOD"]."<br>";
											/*entradas*/
											 $sql2="SELECT T3.ATRNUM, T3.ATRCOD, T2.ATRDES, T3.ATRFEC, T1.ATRCAN, (T3.ATRDES) AS DESATR, T3.ATROBS, T1.ATRCUT,
											 				(SELECT T6.AAPVLA FROM IV38FP T6 WHERE T6.ACICOD=T1.ACICOD and T6.ATRNUM=T3.ATRNUM and T6.ATRCOD=T3.ATRCOD AND T6.APACOD IN ('1506') ) AS NROGIA
														FROM IV16FP T1 
															INNER JOIN IV15FP T3 ON (T1.ACICOD=T3.ACICOD AND T1.ATRCOD=T3.ATRCOD AND T1.ATRNUM=T3.ATRNUM ) 
															INNER JOIN iv12fp T2 ON(T1.ACICOD= T2.ACICOD AND T1.ATRCOD=T2.ATRCOD) 
														WHERE T1.ACICOD='$Compania' AND T1.AALCOD='0001' AND T1.ATRCOD in ( '0001') AND  T1.ATRART='".$paginat[$g]["AARCOD"]."' ORDER BY T3.ATRNUM";
											//echo $sql2."<br/><br/>";
											$result2=odbc_exec($cid,$sql2)or die(exit("Error en odbc_exec 11111"));
											$result22=odbc_exec($cid,$sql2)or die(exit("Error en odbc_exec 11111"));
											$canEnt=0;
											//$canEnt = odbc_num_rows($result22);
											while(odbc_fetch_row($result22)){
												$canEnt++;
											}
											//echo "<br>"."*".$canEnt;
											
											
											$resultES = '';
											$numRows = '';
											
											$resultES = $result2;
											$numRows = $canEnt;
											
											$rowpos = 0;
											//echo "numRows:".$numRows;
											//echo "rowpos:".$rowpos;				
                                 ?>           
                                                <!--<tr >
	                                                <td valign="middle" style="text-align:left;vertical-align:text-top;" rowspan="<?php echo $numRows; ?>"><div>&nbsp;<?php echo $paginat[$g]["AARCOD"]; ?></div></td>
                                                    <td valign="middle" style="text-align:left;vertical-align:text-top;" rowspan="<?php echo $numRows; ?>"><div><?php echo $paginat[$g]["AARDES"]; ?></div></td>
                                                    <td valign="middle" style="text-align:left;vertical-align:text-top;" rowspan="<?php echo $numRows; ?>"><div><?php echo $paginat[$g]["AMCDES"]; ?></div></td>
                                                -->
                                 <?php
                                             //while(odbc_fetch_row($resultES)){   
											 if($numRows>$rowpos){
											 while($numRows>$rowpos){   
											 
								?>
                                             	<tr >
	                                                <td valign="middle" style="text-align:left;vertical-align:text-top;" rowspan="<?php //echo $numRows; ?>"><div>&nbsp;<?php echo $paginat[$g]["AARCOD"]; ?></div></td>
                                                    <td valign="middle" style="text-align:left;vertical-align:text-top;" rowspan="<?php //echo $numRows; ?>"><div><?php echo utf8_encode($paginat[$g]["AARDES"]);?></div></td>
                                                    <td valign="middle" style="text-align:left;vertical-align:text-top;" rowspan="<?php //echo $numRows; ?>">
                                                    	<div>
															<?php 
                                                                $list = list_ubiart($cid, $Compania, '0001', $paginat[$g]["AARCOD"],2);
                                                                //echo "*".trim($paginat[$g]["AUBCOD"])."*";
                                                                //echo '<a rel="shadowbox;width=400;height=300" href="editarfubicacion.php?&aarcod='.trim($paginat[$g]["AARCOD"]).'&aalcod='.$aalcod.'" title="Editar" >';
                                                                
                                                                if(trim($list)!=''){
                                                                    echo $list; } 
                                                                else { 
                                                                    echo ' - Sin Ubicación';
                                                                }
                                                                //echo '</a>';
                                                             ?>
                                                        </div>
                                                    </td>
                                                    <td valign="middle" style="text-align:left;vertical-align:text-top;" rowspan="<?php //echo $numRows; ?>"><div><?php echo $paginat[$g]["AMCDES"]; ?></div></td>
                                                    <td valign="middle" style="text-align:left;vertical-align:text-top;" rowspan="<?php //echo $numRows; ?>"><div><?php echo utf8_encode($paginat[$g]["ATIDES"]); ?></div></td>
                                             <?PHP 
											//echo "numRows:".$numRows;
											//echo "rowpos:".$rowpos;
											          $rowpos++;       
													  odbc_fetch_row($result2);
												if($rowpos<=$canEnt){	
									?>
                                                    <!-- entradas -->
                                                    <!--
                                                    <td style="text-align:center;vertical-align:text-top;">&nbsp;<?php echo odbc_result($result2, 'ATRNUM')!=''?add_ceros(odbc_result($result2, 'ATRNUM'),6):'&nbsp;'; echo odbc_result($result2, 'ATRCOD')=='0003'?"<strong>(D)</strong>":'(E)';?></td>
                                                    <td style="text-align:center;vertical-align:text-top;">&nbsp;<?php echo odbc_result($result2, 'NROGIA')!=''?odbc_result($result2, 'ATRNUM'):'&nbsp;'; echo odbc_result($result2, 'ATRCOD')=='0003'?"N/A":'';?></td>
                                                    -->
                                                    
                                     <?php 
													if(odbc_result($result2, 'ATRCOD')=='0003'){
														?>
                                                        <td style="text-align:center;vertical-align:middle;"><?php echo odbc_result($result2, 'ADSNRO')!=''?add_ceros(odbc_result($result2, 'ADSNRO'),6):'&nbsp;'; echo "<strong>(D)</strong>";?></td>
                                                        <?php
													}else{
														?>
                                                        <td style="text-align:center;vertical-align:middle;"><?php echo odbc_result($result2, 'ATRNUM')!=''?add_ceros(odbc_result($result2, 'ATRNUM'),6):'&nbsp;'; echo '(E)';?></td>
                                                        <?php
													}
													if(odbc_result($result2, 'ATRCOD')=='0003'){
														?>
                                                        <td style="text-align:center;vertical-align:middle;"><?php echo odbc_result($result2, 'NROGIA')!=''?odbc_result($result2, 'NROGIA'):'N/A';?></td>
                                                        <?php
													}else{
														?>
                                                        <td style="text-align:center;vertical-align:middle;"><?php echo odbc_result($result2, 'NROGIA')!=''?odbc_result($result2, 'NROGIA'):'---&nbsp;';?></td>
                                                        <?php
													}
												
									?>
                                                    <!--<td >&nbsp;<?php echo odbc_result($result2, 'ATRDES')!=''?odbc_result($result2, 'ATRDES'):'&nbsp;';?></td>-->
                                                    <td style="text-align:center;vertical-align:text-top;">&nbsp;<?php echo odbc_result($result2, 'ATRFEC')!=''?formatDate(odbc_result($result2, 'ATRFEC'),'aaaa-mm-dd','dd/mm/aaaa' ):'&nbsp;';?></td>
                                                    <td style="text-align:center;vertical-align:text-top;">&nbsp;<?php echo odbc_result($result2, 'ATRCAN')!=''?number_format(odbc_result($result2, 'ATRCAN'),2,',',''):'&nbsp;';?></td>
                                                    <td style="text-align:center;vertical-align:text-top;">&nbsp;<?php echo odbc_result($result2, 'DESATR')!=''?utf8_encode(odbc_result($result2, 'DESATR')):'&nbsp;';?></td>
                                                    <!--<td style="text-align:center;vertical-align:text-top;">&nbsp;<?php echo odbc_result($result2, 'ATROBS')!=''?odbc_result($result2, 'ATROBS'):'&nbsp;';?></td>-->
                                                    <td style="text-align:center;vertical-align:text-top;">&nbsp;<?php echo odbc_result($result2, 'ATRCUT')!=''?number_format(odbc_result($result2, 'ATRCUT'),2,',',''):'&nbsp;';?></td>
                                                    <td style="text-align:center;vertical-align:text-top;">&nbsp;<?php echo odbc_result($result2, 'ATRCUT')!=''?number_format((odbc_result($result2, 'ATRCUT')*odbc_result($result2, 'ATRCAN')),2,',',''):'&nbsp;';?></td>
                                 <?php 			}else{	?>
                                					<td >&nbsp;</td>
                                                    <!--<td >&nbsp;</td>-->
                                                    <td >&nbsp;</td>
                                                    <td >&nbsp;</td>
                                                    <td >&nbsp;</td>
                                                    <!--<td >&nbsp;</td>-->
                                                    <td >&nbsp;</td>
                                                    <td >&nbsp;</td>
                              <?php 
                                            	 }
											 	
                                            	if($rowpos == 1){	?> 
                                                   <!--<td valign="middle" align="right" style="text-align:center;vertical-align:text-top;" rowspan="<?php echo $numRows; ?>"><div><?php echo $paginat[$g]["ASLSAF"]!=''?number_format($paginat[$g]["ASLSAF"],0):"--";?></div></td>-->
								<?php 			}//fin if  if($rowpos == 1)?>
                                            </tr>
                               <?php		}//fin while(odbc_fetch_row($resultES))
											}else{	?> 
                                                    <td >&nbsp;</td>
                                                    <!--<td >&nbsp;</td>-->
                                                    <td >&nbsp;</td>
                                                    <td >&nbsp;</td>
                                                    <!--<td >&nbsp;</td>-->
                                                    <td >&nbsp;</td>
                                                    <td >&nbsp;</td>
                                                    <td >&nbsp;</td>
                                                    
                                                    <!--<td valign="middle" align="right"  rowspan="<?php echo $numRows; ?>">&nbsp;</td>-->
                                                </tr>
								<?php 		}
									}	?>                                            
                  </tbody>
                </table>
</div>
		  </div>
	  </div>
	</div>
		<!-- end #content -->
		<!-- end #sidebar -->
		<div style="clear: both;">&nbsp;</div>
	</div>
	<!-- end #page -->

	<div id="footer">
		<div id="opcionesmasusadas"><?php include("../opcionesmasusadasphp.php"); ?></div>
	</div>
 
	<!-- end #footer -->
</body>
</html>
