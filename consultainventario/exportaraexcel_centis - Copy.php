<?php session_start();
include("../conectar.php");
$arqnro = trim($_GET["num"]);
//header("Pragma: ");

header("Pragma: no-cache");
header('Cache-control: ');
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Content-type: application/vnd.ms-excel");
//header("Content-type: application/octet-stream");
header("Content-disposition: attachment; filename=Reporte_CV.xls");


?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Reporte Cv</title>
</head>
<style>

h1, h2, h3, h4, h5 {
	margin: 0;
	padding: 0;
	font-weight: normal;
	color: #32639A;
}

h1 {
	font-size: 2em;
}

h2 {
	font-size: 2.4em;	
}

h3 {
	font-size: 1.6em;
	font-style: italic;
}
h4 {
	font-size: 1.6em;
	font-style: italic;
	color: #FFF;
}
h5 {
	font-size: 1.0em;
	font-style: italic;
	color: #666;
}

#background-image
{
	font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
	font-size: 10px;
	margin: 0px;
	width: 100%;
	text-align: left;
	border-collapse: collapse;
}
#background-image th
{
	padding: 12px;
	font-weight: normal;
	font-size: 12px;
	color: #339;
	border-bottom-style: solid;
	border-left-style: none;
	text-align: center;
}
#background-image td
{
	color: #669;
	border-top: 1px solid #fff;
	padding-right: 4px;
	padding-left: 4px;
}
#background-image tfoot td
{
	font-size: 9px;
}
#background-image tbody 
{

	background-repeat: no-repeat;
	background-position: left top;
}
#background-image tbody td
{
	background-image: url(images/backn.png);
}
* html #background-image tbody td
{
	/* 
	   ----------------------------
		PUT THIS ON IE6 ONLY STYLE 
		AS THE RULE INVALIDATES
		YOUR STYLESHEET
	   ----------------------------
	*/
	filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='images/backn.png',sizingMethod='crop');
	background: none;
}	
</style>
<body>
<?php 
				
				
		 		$wsolicitud=0;
				if ($solicitudpagina==0) 	
					{
						$sql="SELECT T5.AAPVLA, T5.APACOD, T3.AARUMB, T4.AUMDES, T1.ACICOD, T1.AARCOD, T1.AARNIV, T1.ATICOD, T1.ASICOD, T1.AARDES, T1.AARTDT, T1.AARSTS 
							FROM IV05FP T1 
								INNER JOIN IV06FP T3 ON ( T1.ACICOD=T3.ACICOD AND T1.AARCOD=T3.AARCOD ) 
								INNER JOIN IV13FP T4 ON ( T1.ACICOD=T4.ACICOD AND T3.AARUMB=T4.AUMCOD ) 
								LEFT JOIN IV17FP T5 ON ( T1.ACICOD=T5.ACICOD and T1.AARCOD=T5.AARCOD AND T5.APACOD = '0135' ) 
							WHERE T1.ACICOD='".$Compania."' AND 
								( T1.AARCOD IN (SELECT T2.AARCOD  FROM IV40FP T2 WHERE T2.ACICOD=T1.ACICOD AND T2.AALCOD='".$aalcod."' GROUP BY T2.AARCOD ORDER BY T2.AARCOD ) ) 
							ORDER BY T1.AARCOD";
							
						$resultt=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111"));
						//echo $sql."<br/>";
						$z=0;
						$tothon=0;
						$totest=0;
						$lin=1;
						$limitep=$_SESSION['solicitudlineasporpaginat'];
						$pag=1;
						$primero='S';
						
						$anio = date("Y");
						$mes1 = date("m");
						while(odbc_fetch_row($resultt))
						{
							$artcod = odbc_result($resultt,'AARCOD');
							$atrdes = odbc_result($resultt,'AARDES');
							$exp = substr($atrdes ,(strripos($atrdes,"(")) );
							$aumdes = odbc_result($resultt,'AUMDES');
							
							$listapacod = array('0133','0134','0135','0136','0137','0138','0139','0140','0141','0142','0143','0144','0145','0146','0147','0148','0149','0159','0160','0161','0162','0163','0164');
							//$listapacod = explode(',', $listapacod);
							$sql2="";
							$sql3="";
							$sql2="
								SELECT T4.ACICOD, T4.AALCOD, T4.ADPCOD, T4.ATRCOD, (T5.ATRDES) as ATRNOM, T5.ATRSIG, T4.ATRNUM, T4.ATRDES, 
										T4.ATRFEC, T4.AUSCOD, T4.ATROBS, T3.ATRSEC, T3.ATRART,('".$atrdes."') AS AARDES, T3.ATRCAN, T3.ATRUMB, ('".$aumdes."') AS AUMDES, ";
							foreach($listapacod as $key=>$value){			
								$sql3.="
										(SELECT T6.AAPVLA FROM iv17fp T6 WHERE T6.ACICOD=T3.ACICOD and T6.AARCOD=T3.ATRART AND T6.APACOD IN (".$value.") ) AS N".$value." , ";
							}
							$sql2.= substr($sql3,0, (strripos($sql3,",")));
							$sql2.="	
								FROM IV16FP T3 
									INNER JOIN IV15FP T4 ON ( T3.ACICOD=T4.ACICOD AND T3.AALCOD=T4.AALCOD AND T3.ATRCOD=T4.ATRCOD AND T3.ATRNUM=T4.ATRNUM )                                                                   
									INNER JOIN IV12FP T5 ON ( T4.ACICOD=T5.ACICOD AND T4.ATRCOD=T5.ATRCOD AND T5.ATRSIG='+')
								WHERE T4.ACICOD='".$Compania."' AND T4.AALCOD='".$aalcod."' AND T3.ATRART = '".$artcod."' AND T4.ATRCOD IN ('01', '02') 
								ORDER BY T3.ATRART
								";
							//echo $sql2."<br/><br/>";
							//DIE();
							$resultt2=odbc_exec($cid,$sql2)or die(exit("Error en odbc_exec 11111 - ".odbc_error($resultt2) ));
							while(odbc_fetch_row($resultt2))
							{
								$jml = odbc_num_fields($resultt2);
								$row[$z]["pagina"] =  $pag;
								for($i=1;$i<=$jml;$i++)
								{	
									$row[$z][odbc_field_name($resultt2,$i)] =  odbc_result($resultt2,$i);
								}
								$z++;
								if ($lin>=$limitep) 
								{
									$limitep+=$_SESSION['solicitudlineasporpaginat'];
									$pag++;
								}
								$lin++;
							}
						}
						$totsol=($lin-1);
						$_SESSION['totalsolicitudes']=$totsol;
						$_SESSION['solicitudarreglo']=$row;
						$solicitudpagina=1;
						$_SESSION['solicitudpaginas']=$pag;
					}//fin de solicitudpagina
					/*se muestra la cantidad de elementos segun la solicitada en $solicitudpagina*/
					$paginat=$_SESSION['solicitudarreglo'];
			?>      
<table width="100%" border="0">
<tr>
<td height="89"><h1>
<?php if($Compania=='14'){?>
	<img src="http://<?php echo $Direccionip; ?>/idasysv3/images/logoidacadef2005.png" width="280" height="68" />
<?php }else{ ?>
	<img src="http://<?php echo $Direccionip; ?>/idasysv3/images/logomeditronnuevo.png" width="280" height="68" />
<?php } ?>
   </h1>
  <h5>RIF:  <?php echo $Companiarif; ?></h5></td>
</tr>
<tr>
<td>

<table width="100%" id="background-image" >
	
  <thead>
  	<tr>
        <th colspan="9" scope="col"><h3>Reporte de CV</h3></th>
    </tr>
    <tr>
        <th colspan="9" scope="col"><h4>Elaborado el <?php echo $Fechaactual; ?> a las <?php echo $Horaactual2; ?></h4></th>
    </tr>
  	<tr>
        <th colspan="9" scope="col"><h3>Almac�n: <?php echo alamcen($aalcod, $Compania, $cid);?></h3></th>
    </tr>
  	<tr>
        <th scope="col">Nro Transacci�n </th>
        <th scope="col">Fecha Llegada a Meditron</th>
        <th scope="col">Nro de env�o - C�digo Art.</th>
        <th scope="col">Art�culo</th>
        <th scope="col">Descripci�n Larga</th>
        <th scope="col">Nro Contenedor</th>
        <th scope="col">Nro Lote</th>
        <th scope="col">Gu�a</th>
        <th scope="col">Fecha Gu�a</th>
        <th scope="col">Cantidad</th>
        <th scope="col">Unidad</th>
        <th scope="col">Nro Factura</th>
        <th scope="col">Fecha Factura</th>
        <th scope="col">Nro Empaque</th>
        <th scope="col">Fecha Empaque</th>
        <th scope="col">Fecha Llegada al Pa�s</th>
        <th scope="col">Dimensiones</th>
        <th scope="col">Peso Neto</th>
        <th scope="col">Peso Bruto</th>
        <th scope="col">transacci�n</th>
        <th scope="col">Nro Entrega</th>
        <th scope="col">Fecha Entrega</th>
        <th scope="col">Transportistas</th>
        <th scope="col">Destino</th>
        <th scope="col">Fecha de Rcepci�n</th>
        <th scope="col">Recibido por</th>
        <th scope="col">Observaciones</th>
    </tr>
  </thead>
   <tbody>
			<?php 
                
                //print_r($paginat);
                $pagact=$solicitudpagina;
                $part= 1;
				$num_rows =0;
                for($g=0; $g < (count($paginat)); $g++)
                {
					$sql4=" SELECT T4.ACICOD, T4.AALCOD, T4.ADPCOD, T4.ATRCOD, T5.ATRDES, T5.ATRSIG, T4.ATRNUM, T4.ATRDES as REPOR,                           
									T4.ATRFEC, T4.AUSCOD, T4.ATROBS, T3.ATRSEC, T3.ATRART,('".$paginat[$g]["AARDES"]."') AS AARDES,                   
									T3.ATRCAN, T3.ATRUMB, ('".$paginat[$g]["AUMDES"]."') AS AUMDES, T7.AUBCOD, T8.AISDES, T8.AISCOD, T9.ADSNRO                                                
								FROM IDASYSW.IV16FP T3                                                      
									INNER JOIN IDASYSW.IV15FP T4 ON ( T3.ACICOD=T4.ACICOD AND T3.AALCOD=T4.AALCOD AND T3.ATRCOD=T4.ATRCOD AND T3.ATRNUM=T4.ATRNUM )                                                                   
									INNER JOIN IDASYSW.IV12FP T5 ON ( T4.ACICOD=T5.ACICOD AND T4.ATRCOD=T5.ATRCOD AND T5.ATRSIG IN ('-','+') )                    
									LEFT JOIN IDASYSW.IV47FP T7 ON (T3.ACICOD=T7.ACICOD AND T3.AALCOD=T7.AALCOD AND T3.ATRART=T7.AARCOD)                                            
									INNER JOIN IDASYSW.IV35FP T9 ON (T3.ACICOD=T9.ACICOD AND T3.AALCOD=T9.AALCOD AND T4.ATRCOD=T9.ATRCOD AND T4.ATRNUM=T9.ATRNUM )                  
									INNER JOIN IDASYSW.IV42FP T8 ON (T3.ACICOD=T8.ACICOD AND T9.AISCOD=T8.AISCOD )                                                       
								WHERE T4.ACICOD='".$Compania."' AND T4.AALCOD='".$aalcod."' AND T3.ATRART = '".trim($paginat[$g]["ATRART"])."' AND                                       
									T4.ATRCOD IN ('01', '02')                                
								ORDER BY T3.ATRART , T4.ATRFEC ";
					$resultt4=odbc_exec($cid,$sql4)or die(exit("Error en odbc_exec 11111 - ".odbc_error($resultt4) ));					
              
                    /*
					if(strripos($paginat[$g]["AARDES"],"(") > 0){
                        $exp = substr($paginat[$g]["AARDES"] ,(strripos($paginat[$g]["AARDES"],"(")+1), (strripos($paginat[$g]["AARDES"],")")-strripos($paginat[$g]["AARDES"],"(")-1)  );
                    }else{
                        $exp = '-';
                    }
					*/
                    
                   // $num_rows = odbc_num_rows($resultt2);
					
					echo "<br>*".trim($paginat[$g]["ATRART"])."*".$num_rows."<br>";
                     if($num_rows>0)
					 //if(1==1)
					 {
                        
                        //for($i=0; $i<$num_rows;$i++){
                        $i=0;
                        while(odbc_fetch_row($resultt4)){
                            if($i==0)
                            {		
             ?>           
                            <tr>
                            	<!--<td scope="col" rowspan="<?php echo $num_rows;?>" valign="top"><div>&nbsp;<?php //echo $paginat[$g]["FLLE"]!=''?formatDate($paginat[$g]["FLLE"], 'dd.mm.aaaa', 'aaaa/mm/dd'):formatDate($paginat[$g]["ATRFEC"], 'aaaa-mm-dd', 'aaaa/mm/dd');?></div></td>-->
                                <td scope="col" valign="middle" rowspan="<?php echo $num_rows;?>"><div><?php echo $paginat[$g]["ATRNUM"]!=''?$paginat[$g]["ATRNUM"]:"--";?></div></td>
                                <td scope="col" valign="middle" rowspan="<?php echo $num_rows;?>"><div><?php echo $paginat[$g]["N0140"]!=''?formatDate($paginat[$g]["N0140"], 'dd.mm.aaaa', 'aaaa/mm/dd'):"--";?></div></td>
                                <td scope="col" valign="middle" rowspan="<?php echo $num_rows;?>"><div><?php echo $paginat[$g]["ATRART"]!=''?$paginat[$g]["ATRART"]:"--";?></div></td>
                                <td scope="col" valign="middle" rowspan="<?php echo $num_rows;?>"><div><?php echo $paginat[$g]["AARDES"]!=''?$paginat[$g]["AARDES"]:"--";?></div></td>
                                <td scope="col" valign="middle" rowspan="<?php echo $num_rows;?>"><div><?php echo $paginat[$g]["N0142"]!=''?$paginat[$g]["N0142"]:"--";?></div></td>
                                <td scope="col" valign="middle" rowspan="<?php echo $num_rows;?>"><div><?php echo $paginat[$g]["N0133"]!=''?$paginat[$g]["N0133"]:"--";?></div></td>
                                <td scope="col" valign="middle" rowspan="<?php echo $num_rows;?>"><div><?php echo $paginat[$g]["N0134"]!=''?$paginat[$g]["N0134"]:"--";?></div></td>
                                <td scope="col" valign="middle" rowspan="<?php echo $num_rows;?>"><div><?php echo $paginat[$g]["N0137"]!=''?$paginat[$g]["N0137"]:"--";?></div></td>
                                <td scope="col" valign="middle" rowspan="<?php echo $num_rows;?>"><div><?php echo $paginat[$g]["N0138"]!=''?formatDate($paginat[$g]["N0138"], 'dd.mm.aaaa', 'aaaa/mm/dd'):"--";?></div></td>
                                <td scope="col" valign="middle" rowspan="<?php echo $num_rows;?>"><div><?php echo $paginat[$g]["ATRCAN"]!=''?number_format($paginat[$g]["ATRCAN"],2,",","."):number_format(0,2,",",".");?></div></td>
                                <td scope="col" valign="middle" rowspan="<?php echo $num_rows;?>"><div><?php echo $paginat[$g]["AUMDES"]!=''?$paginat[$g]["AUMDES"]:"--";?></div></td>
                                <td scope="col" valign="middle" rowspan="<?php echo $num_rows;?>"><div><?php echo $paginat[$g]["N0135"]!=''?$paginat[$g]["N0135"]:"--";?></div></td>
                                <td scope="col" valign="middle" rowspan="<?php echo $num_rows;?>"><div><?php echo $paginat[$g]["N0136"]!=''?formatDate($paginat[$g]["N0136"], 'dd.mm.aaaa', 'aaaa/mm/dd'):"--";?></div></td>
                                <td scope="col" valign="middle" rowspan="<?php echo $num_rows;?>"><div><?php echo $paginat[$g]["N0164"]!=''?$paginat[$g]["N0164"]:"--";?></div></td>
                                <td scope="col" valign="middle" rowspan="<?php echo $num_rows;?>"><div><?php echo $paginat[$g]["N0141"]!=''?formatDate($paginat[$g]["N0141"], 'dd.mm.aaaa', 'aaaa/mm/dd'):"--";?></div></td>
                                <td scope="col" valign="middle" rowspan="<?php echo $num_rows;?>"><div><?php echo $paginat[$g]["N0139"]!=''?$paginat[$g]["N0139"]:"--";?></div></td>
                                <td scope="col" valign="middle" rowspan="<?php echo $num_rows;?>"><div><?php echo $paginat[$g]["N0143"]!=''?$paginat[$g]["N0143"]:"--";?></div></td>
                                <td scope="col" valign="middle" rowspan="<?php echo $num_rows;?>"><div><?php echo $paginat[$g]["N0144"]!=''?$paginat[$g]["N0144"]." Kg.":"--";?></div></td>
                                <td scope="col" valign="middle" rowspan="<?php echo $num_rows;?>"><div><?php echo $paginat[$g]["N0145"]!=''?$paginat[$g]["N0145"]." Kg.":"--";?></div></td> 
                       <?php }  ?>
                        
                        <td scope="col" valign="top"><div>&nbsp;<?php echo formatDate(odbc_result($resultt4,"1"), 'aaaa-mm-dd', 'dd/mm/aaaa');?></div></td>
                        <td scope="col" valign="top"><div>&nbsp;<?php echo odbc_result($resultt4,"2")." (".odbc_result($resultt2,"ADS2NRO").")";?></div></td>
                        <td scope="col" valign="top"><div>&nbsp;<?php echo number_format(odbc_result($resultt2,"ATRCAN"),2,",",".");?></div></td>
                        <td scope="col" valign="top"><div>&nbsp;<?php echo odbc_result($resultt4,"2");?></div></td>
                        <td scope="col" valign="top"><div>&nbsp;<?php echo odbc_result($resultt4,"2");?></div></td>
                        <td scope="col" valign="top"><div>&nbsp;<?php echo odbc_result($resultt4,"2");?></div></td>
                    </tr>
                    
            <?php 
                        $i++;
                        }
                     }else{
            ?>	
                        <tr>
                        	<!--<td scope="col" valign="top" ><div>&nbsp;<?php //echo $paginat[$g]["FLLE"]!=''?formatDate($paginat[$g]["FLLE"], 'dd.mm.aaaa', 'aaaa/mm/dd'):formatDate($paginat[$g]["ATRFEC"], 'aaaa-mm-dd', 'aaaa/mm/dd');?></div></td>-->
                            <td scope="col" valign="middle" rowspan="<?php echo $num_rows;?>"><div><?php echo $paginat[$g]["ATRNUM"]!=''?$paginat[$g]["ATRNUM"]:"--";?></div></td>
                            <td scope="col" valign="middle" rowspan="<?php echo $num_rows;?>"><div><?php echo $paginat[$g]["N0140"]!=''?formatDate($paginat[$g]["N0140"], 'dd.mm.aaaa', 'aaaa/mm/dd'):"--";?></div></td>
                            <td scope="col" valign="middle" rowspan="<?php echo $num_rows;?>"><div><?php echo $paginat[$g]["ATRART"]!=''?$paginat[$g]["ATRART"]:"--";?></div></td>
                            <td scope="col" valign="middle" rowspan="<?php echo $num_rows;?>"><div><?php echo $paginat[$g]["AARDES"]!=''?$paginat[$g]["AARDES"]:"--";?></div></td>
                            <td scope="col" valign="middle" rowspan="<?php echo $num_rows;?>"><div><?php echo $paginat[$g]["N0142"]!=''?$paginat[$g]["N0142"]:"--";?></div></td>
                            <td scope="col" valign="middle" rowspan="<?php echo $num_rows;?>"><div><?php echo $paginat[$g]["N0133"]!=''?$paginat[$g]["N0133"]:"--";?></div></td>
                            <td scope="col" valign="middle" rowspan="<?php echo $num_rows;?>"><div><?php echo $paginat[$g]["N0134"]!=''?$paginat[$g]["N0134"]:"--";?></div></td>
                            <td scope="col" valign="middle" rowspan="<?php echo $num_rows;?>"><div><?php echo $paginat[$g]["N0137"]!=''?$paginat[$g]["N0137"]:"--";?></div></td>
                            <td scope="col" valign="middle" rowspan="<?php echo $num_rows;?>"><div><?php echo $paginat[$g]["N0138"]!=''?formatDate($paginat[$g]["N0138"], 'dd.mm.aaaa', 'aaaa/mm/dd'):"--";?></div></td>
                            <td scope="col" valign="middle" rowspan="<?php echo $num_rows;?>"><div><?php echo $paginat[$g]["ATRCAN"]!=''?number_format($paginat[$g]["ATRCAN"],2,",","."):number_format(0,2,",",".");?></div></td>
                            <td scope="col" valign="middle" rowspan="<?php echo $num_rows;?>"><div><?php echo $paginat[$g]["AUMDES"]!=''?$paginat[$g]["AUMDES"]:"--";?></div></td>
                            <td scope="col" valign="middle" rowspan="<?php echo $num_rows;?>"><div><?php echo $paginat[$g]["N0135"]!=''?$paginat[$g]["N0135"]:"--";?></div></td>
                            <td scope="col" valign="middle" rowspan="<?php echo $num_rows;?>"><div><?php echo $paginat[$g]["N0136"]!=''?formatDate($paginat[$g]["N0136"], 'dd.mm.aaaa', 'aaaa/mm/dd'):"--";?></div></td>
                            <td scope="col" valign="middle" rowspan="<?php echo $num_rows;?>"><div><?php echo $paginat[$g]["N0164"]!=''?$paginat[$g]["N0164"]:"--";?></div></td>
                            <td scope="col" valign="middle" rowspan="<?php echo $num_rows;?>"><div><?php echo $paginat[$g]["N0141"]!=''?formatDate($paginat[$g]["N0141"], 'dd.mm.aaaa', 'aaaa/mm/dd'):"--";?></div></td>
                            <td scope="col" valign="middle" rowspan="<?php echo $num_rows;?>"><div><?php echo $paginat[$g]["N0139"]!=''?$paginat[$g]["N0139"]:"--";?></div></td>
                            <td scope="col" valign="middle" rowspan="<?php echo $num_rows;?>"><div><?php echo $paginat[$g]["N0143"]!=''?$paginat[$g]["N0143"]:"--";?></div></td>
                            <td scope="col" valign="middle" rowspan="<?php echo $num_rows;?>"><div><?php echo $paginat[$g]["N0144"]!=''?$paginat[$g]["N0144"]." Kg.":"--";?></div></td>
                            <td scope="col" valign="middle" rowspan="<?php echo $num_rows;?>"><div><?php echo $paginat[$g]["N0145"]!=''?$paginat[$g]["N0145"]." Kg.":"--";?></div></td>
                            
                            <td scope="col" valign="top" align="center"><div>-</div></td>
                            <td scope="col" valign="top" align="center"><div>-</div></td>
                            <td scope="col" valign="top" align="center"><div>-</div></td>
                            <td scope="col" valign="top" align="center"><div>-</div></td>
                            <td scope="col" valign="top" align="center"><div>-</div></td>
                            <td scope="col" valign="top" align="center"><div>-</div></td>
                        </tr>
            <?php 
                    }
                }

            ?>      
    </tbody>

</table>
</td>
</tr>
</table>
</body>
</html>
