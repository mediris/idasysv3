<?php 
/*
 * Luis Ramos (lRamos)
 * 02/11/2016 
 */
session_start();
include("../../conectar.php");

$tinva = $_GET['tinvan'];
$aticod = $_GET['aticod'];

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
<title>Idasys V3</title>
<link href="../../<?php echo $css; ?>" rel="stylesheet" type="text/css" media="screen" />
<script language="JavaScript" src="../../javascript/javascript.js"></script>
<script language="JavaScript" src="../../calendario/javascripts.js"></script>
<script language="JavaScript" src="../javascript.js"></script>
<script language="JavaScript" src="../../javascript/jquery.js"></script>
<link href="../../shadowbox.css"  rel="stylesheet" type="text/css"/>
<script src="../../shadowbox.js" type="text/javascript"/> </script>
<script language="JavaScript" type="text/JavaScript">
Shadowbox.init({overlayOpacity: "0.5"});
</script>
<style type="text/css" title="currentStyle">
			@import "../../DataTables-1.9.4/media/css/<?php echo $demo_page;?>";
			@import "../../DataTables-1.9.4/media/css/<?php echo $demo_table;?>";
</style>
<script type="text/javascript" language="javascript" src="../../DataTables-1.9.4/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf-8">
			
			 $(document).ready(function() 
			 {
			      document.getElementById('info').style.display="";
				  $('#info').dataTable( 
				  {
					"bStateSave": true,
			        "oLanguage": 
					{
			          	"sLengthMenu": "Mostrar _MENU_ registros por Pag.",
						"sZeroRecords": "No Existen Registros",
						"sInfo": "Mostrando desde _START_ hasta _END_ de _TOTAL_ registros",
						"sInfoEmpty": "Mostrando desde 0 hasta 0 de 0 registros",
						"sInfoFiltered": "(filtrando de _MAX_ de registros)",
						"sSearch": "Buscar",
						"oPaginate": 
						{
							"sNext": "Siguiente",
							"sPrevious": "Anterior"
						}
			        }
					/*,
					"aoColumnDefs": [
						{ "bVisible": false, "aTargets": [ 0 ] }
					]*/
			      } );
			  } );
</script>

</head>
<body background="../../images/fondo idaca.jpg" >
<div id="wrapper">
  <?php include("../../superior.php");?>
  <div id="page">
     <?php include("../../validar.php");?>

	  
 		<?php 
				
				if ($bandera==1) 
				{	
					$wsolicitud=0;
					if ($solicitudpagina==0) 	
					{

						$tipoAlm = '';
						$tipoInve ='';
						if(!empty($aticod)){
							if($aticod=='01'){
								$tipoInve = '0001'; //EQUIPOS
								$tipoAlm = '0002'; //ALMACEN EQUIPOS
								$tipoDot = "'01'";//TIPO DE DOTACION EN SALIDA
							} elseif($aticod=='02'){
								$tipoInve = '0002'; //CONSUMIBLES
								$tipoAlm = '0001'; //ALMACEN CONSUMIBLE BI
								$tipoDot = "'03'";//TIPO DE DOTACION EN SALIDA
							} elseif($aticod=='03'){
								$tipoInve = '0002'; //CONSUMIBLES
								$tipoAlm = '0002'; //ALMACEN CONSUMIBLE NV
								$tipoDot = "'02'";//TIPO DE DOTACION EN SALIDA
							} elseif($aticod=='04'){
								$tipoInve = '0003'; //REPUESTOS
								$tipoAlm = '0003'; //ALMACEN REPUESTOS
								$tipoDot = "'04'";//TIPO DE DOTACION EN SALIDA
							}
						}

						//(SELECT T6.ASLSAF FROM IV40FP T6 WHERE T6.ACICOD=T1.ACICOD AND T6.AARCOD=T1.AARCOD AND T6.AALCOD IN ('0004', '0005') ORDER BY T6.ASLFEF DESC FETCH FIRST 1 ROWS ONLY) AS ASLSAF
						$sql="SELECT T3.AARUMB, T4.AUMDES, T1.ACICOD, T1.AARCOD, T1.AARNIV, T1.ATICOD, T1.ASICOD, T1.AARDES, T1.AARTDT, T1.AARSTS, T1.ATICOD, T6.ATIDES, T7.AMCDES,
								(SELECT (SUM(T8.ASLENT)+SUM(T8.ASLSAL)) FROM IV40FP T8 WHERE T8.ACICOD=T1.ACICOD AND T8.AARCOD=T1.AARCOD AND T8.AALCOD in ('$tipoAlm')) AS SALFIN
							FROM IV05FP T1 
								INNER JOIN IV06FP T3 ON ( T1.ACICOD=T3.ACICOD AND T1.AARCOD=T3.AARCOD ) 
								INNER JOIN IV13FP T4 ON ( T1.ACICOD=T4.ACICOD AND T3.AARUMB=T4.AUMCOD ) 
								INNER JOIN IV01FP T6 ON (T1.ACICOD=T6.ACICOD  AND T1.ATICOD=T6.ATICOD)
								INNER JOIN IV04FP T7 ON (T3.ACICOD=T7.ACICOD AND T3.AMCCOD=T7.AMCCOD)
							WHERE T1.ACICOD='$Compania' AND 
								( T1.AARCOD IN (SELECT T2.AARCOD FROM IV40FP T2 WHERE T2.ACICOD=T1.ACICOD AND T2.AALCOD in ('$tipoAlm') GROUP BY T2.AARCOD ORDER BY T2.AARCOD ) ) 
								 ";
						if(!empty($tipoInve)){
							$sql.="  AND T1.ATICOD IN (".$tipoInve.")";
						}
						$sql.="  
							ORDER BY T1.AARDES";
								//T1.ALTCOD ,
								//echo $sql;
								
								$resultt=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111"));
								
								
								$z=0;
								$tothon=0;
								$totest=0;
								$lin=1;
								$limitep=$_SESSION['solicitudlineasporpaginat'];
								$pag=1;
								$primero='S';
								
								while(odbc_fetch_row($resultt))
								{
									$jml = odbc_num_fields($resultt);
									$row[$z]["pagina"] =  $pag;
									for($i=1;$i<=$jml;$i++)
									{	if (odbc_field_name($resultt,$i)=='ARGTTE') {$totest+=odbc_result($resultt,$i);}
										if (odbc_field_name($resultt,$i)=='ARGTTM') {$tothon+=odbc_result($resultt,$i);}
										$row[$z][odbc_field_name($resultt,$i)] =  odbc_result($resultt,$i);
									}
									$z++;
									if ($lin>=$limitep) 
									{
										$limitep+=$_SESSION['solicitudlineasporpaginat'];
										$pag++;
									}
									$lin++;
								}
								$totsol=($lin-1);
								$_SESSION['totalsolicitudes']=$totsol;
								$_SESSION['solicitudarreglo']=$row;
								$solicitudpagina=1;
								$_SESSION['solicitudpaginas']=$pag;
					}//fin de solicitudpagina
					/*se muestra la cantidad de elementos segun la solicitada en $solicitudpagina*/
					$paginat=$_SESSION['solicitudarreglo'];
				}
			?>
        <div id="content3" >   
        	<table width="100%"   border="0">
              <tr>
                <td width="65%" scope="col" colspan="2"><h1  class="title">Consulta Existencia - MEDIX</h1>
                  <hr /></td>
                <td width="13%" scope="col"><div align="left">
                  <table width="100%"  border="0">
                    <tr>
                      <th width="30%" scope="col" >
                      <div class="flechas">
                      	<a href="exportaraexcel_medix_<?php echo $aticod; ?>.php?&aticod=<?php echo $aticod; ?>&stm=<?php echo $stm; ?>&tinvan=<?php echo $url222 = urlencode( serialize($tinva) );?>" target="_blank">
                        	<img src="../../images/excel.jpg" alt="" width="30" height="30" title="Exportar a Excel"/>
                        </a>
                      </div></th>
                    </tr>
                  </table>
                </div></td>
              </tr>
              <tr>
              	<td colspan="3">
                	<form name="form" id="form" method="post" on>
                    <input type="hidden" name="bandera" id="bandera" value="<?php echo $bandera;?>" />
                    <input type="hidden" name="tipoAlm" id="tipoAlm" value="<?php echo $tipoAlm;?>" />
                   <!-- <input type="hidden" name="aalcod" id="aalcod" value="0004" /> -->                    
                	<table>
                	<tr>
                        	<td>&nbsp;Tipo de Inventario:</td>
                            <td>&nbsp;<?php //echo $aticod; ?>
                            	<input type="radio" name="aticod" id="aticod0" value="01" <?php if($aticod=='01'){echo 'checked="checked"';} ?>> EQUIPOS (0001)&nbsp;
                            	<input type="radio" name="aticod" id="aticod1" value="02" <?php if($aticod=='02'){echo 'checked="checked"';} ?>> CONSUMIBLES BI (0002)&nbsp;
                            	<input type="radio" name="aticod" id="aticod2" value="03" <?php if($aticod=='03'){echo 'checked="checked"';} ?>> CONSUMIBLES NV (0002)&nbsp;
                            	<input type="radio" name="aticod" id="aticod3" value="04" <?php if($aticod=='04'){echo 'checked="checked"';} ?>> REPUESTOS (0003)&nbsp;
                            </td>
                            <td >&nbsp;<a href="javascript:busqueda6();"><img src="../../images/precesarconsulta.png" width="50" height="50" alt="Realizar Consulta" style="border:none;"></a></td>
                        </tr>
                    </table>
                    </form>
                </td>
              </tr>
            </table>   
		<div id="container">
            <div id="demo">
                <table width="100%" id="info"  border="0" style="display:none;">
                  <thead>
                    <tr>
                    	<th scope="col" rowspan="2" style="background-color:rgb(204,204,204)">C&oacute;digo</th>
                   		<th scope="col" rowspan="2" style="background-color:rgb(204,204,204)">Art&iacute;culo</th>
                        <th scope="col" rowspan="2" style="background-color:rgb(204,204,204)">Ubicaci&oacute;n</th>
                    	<th scope="col" colspan="3" style="background-color:rgb(204,204,204)">Entradas</th>
                        <th scope="col" colspan="3" style="background-color:rgb(204,204,204)">Salidas</th>
                        <th scope="col" rowspan="2" style="background-color:rgb(204,204,204)">Existencia Actual</th>
                    </tr>
                    <tr>
                        <th scope="col" style="background-color:rgb(204,204,204)"><strong>Nro. de Entrada</strong></th>
                        <!--<th scope="col" ><strong>Tipo</strong></th>-->
                        <th scope="col" style="background-color:rgb(204,204,204)"><strong>Fecha</strong></th>
                        <th scope="col" style="background-color:rgb(204,204,204)"><strong>Cantidad</strong></th>
                        <!--<th scope="col" style="background-color:rgb(204,204,204)"><strong>Descripción</strong></th>-->
                        <!--<th scope="col" style="background-color:rgb(204,204,204)"><strong>Observaciones</strong></th>-->
                        
                        <th scope="col" style="background-color:rgb(204,204,204)"><strong>Nro. de Salida</strong></th>
                        <!--<th scope="col" ><strong>Tipo</strong></th>-->
                        <th scope="col" style="background-color:rgb(204,204,204)"><strong>Fecha</strong></th>
                        <th scope="col" style="background-color:rgb(204,204,204)"><strong>Cantidad</strong></th>
                        <!--<th scope="col" style="background-color:rgb(204,204,204)"><strong>Destino</strong></th>
                        <th scope="col" style="background-color:rgb(204,204,204)"><strong>Observaciones</strong></th>-->
                    </tr>
                 </thead> 
                 <tbody>
        						<?php
									//print_r($paginat);
									$pagact=$solicitudpagina;
									$part= 1;
									for($g=0; $g < (count($paginat)); $g++)
									{
											//echo "//**".$paginat[$g]["AARCOD"]."<br>";
											/*entradas*/
											/*$sql2="SELECT T3.ATRNUM, T3.ATRCOD, T2.ATRDES, T3.ATRFEC, T1.ATRCAN, (T3.ATRDES) AS DESATR, T3.ATROBS 
														FROM IV16FP T1 
															INNER JOIN IV15FP T3 ON (T1.ACICOD=T3.ACICOD AND T1.ATRCOD=T3.ATRCOD AND T1.ATRNUM=T3.ATRNUM ) 
															INNER JOIN iv12fp T2 ON(T1.ACICOD= T2.ACICOD AND T1.ATRCOD=T2.ATRCOD) 
														WHERE T1.ACICOD='$Compania' AND T1.AALCOD='0001' AND T1.ATRCOD in ( '0001', '0003', '0004', '0006', '0101' ) AND  T1.ATRART='".$paginat[$g]["AARCOD"]."'
														ORDER BY T3.ATRFEC ";
											*/
											/*entradas*/
											$sql2="( SELECT T3.ATRNUM, T3.ATRCOD, T2.ATRDES, T3.ATRFEC, (SUM(T1.ATRCAN)) AS ATRCAN, (T3.ATRDES) AS DESATR, T3.ATROBS, T5.ADSNRO, T5.ATSCOD 
													FROM IV16FP T1 
														INNER JOIN IV15FP T3 ON (T1.ACICOD=T3.ACICOD AND T1.ATRCOD=T3.ATRCOD AND T1.ATRNUM=T3.ATRNUM AND T1.ADPCOD=T3.ADPCOD AND T3.ATRSTS='02' ) 
														INNER JOIN iv12fp T2 ON(T1.ACICOD= T2.ACICOD AND T1.ATRCOD=T2.ATRCOD) 
														LEFT JOIN IV35FP T5 ON (T1.ACICOD=T5.ACICOD AND T1.AALCOD=T5.AALCOD AND T3.ADPCOD=T5.ADPCOD AND T1.ATRCOD=T5.ATRCOD AND T1.ATRNUM=T5.ATRNUM) 
													WHERE T1.ACICOD='42' AND T1.AALCOD IN ('$tipoAlm') AND T1.ATRCOD in ( '0101', '0102','0210' ) AND T1.ATRART='".$paginat[$g]["AARCOD"]."'
													GROUP BY T3.ATRNUM, T3.ATRCOD, T2.ATRDES, T3.ATRFEC,T3.ATRDES, T3.ATROBS, T5.ADSNRO, T5.ATSCOD   
													ORDER BY T3.ATRFEC 
													)UNION( 
													SELECT T3.ATRNUM, T3.ATRCOD, T2.ATRDES, T3.ATRFEC, (SUM(T1.ATRCAN)) AS ATRCAN, (T3.ATRDES) AS DESATR, T3.ATROBS, T5.ADSNRO, T5.ATSCOD 
													FROM IV16FP T1
													 INNER JOIN IV15FP T3 ON (T1.ACICOD=T3.ACICOD AND T1.ATRCOD=T3.ATRCOD AND T1.ATRNUM=T3.ATRNUM AND T1.ADPCOD=T3.ADPCOD ) 
													 INNER JOIN iv12fp T2 ON(T1.ACICOD= T2.ACICOD AND T1.ATRCOD=T2.ATRCOD) 
													 LEFT JOIN IV35FP T5 ON (T1.ACICOD=T5.ACICOD AND T1.AALCOD=T5.AALCOD AND T3.ADPCOD=T5.ADPCOD AND T1.ATRCOD=T5.ATRCOD AND T1.ATRNUM=T5.ATRNUM) 
													 WHERE T1.ACICOD='42' AND T1.AALCOD in ('0004', '0005', '0006') AND T1.ATRCOD in ( '0210' ) AND T1.ATRART='".$paginat[$g]["AARCOD"]."'
													 GROUP BY T3.ATRNUM, T3.ATRCOD, T2.ATRDES, T3.ATRFEC,T3.ATRDES, T3.ATROBS, T5.ADSNRO, T5.ATSCOD   
													 ORDER BY T3.ATRFEC )";
											//echo $sql2."<br/><br/>";
											$result2=odbc_exec($cid,$sql2)or die(exit("Error en odbc_exec 11111"));
											$result22=odbc_exec($cid,$sql2)or die(exit("Error en odbc_exec 11111"));
											$canEnt=0;

											//echo odbc_result($result2, 'ATRCOD');

											//die();
											//$canEnt = odbc_num_rows($result22);
											while(odbc_fetch_row($result22)){
												$canEnt++;
											}
											//echo "<br>"."*".$canEnt;
											
											/*salidas*/
											$sql3="SELECT T2.ADSNRO, T3.ATSCOD, T3.ATSDES, T2.ATRFEC, (SUM(T1.ATRCAN)) AS ATRCAN, T2.ATROBS, T4.AISCOD, T4.AISDES, T5.AUNDES, T1.ACICOD, T2.ADSNRO, T1.ATSCOD,
													(SELECT T6.AAPVLA FROM IV46FP T6 WHERE T6.ACICOD=T1.ACICOD and T6.ADSNRO=T2.ADSNRO AND T6.ATSCOD=T1.ATSCOD AND T6.APACOD IN ('2002') ) AS N2002,
													(SELECT T6.AAPVLA FROM IV46FP T6 WHERE T6.ACICOD=T1.ACICOD and T6.ADSNRO=T2.ADSNRO AND T6.ATSCOD=T1.ATSCOD AND T6.APACOD IN ('2003') ) AS N2003,
													(SELECT T6.AAPVLA FROM IV46FP T6 WHERE T6.ACICOD=T1.ACICOD and T6.ADSNRO=T2.ADSNRO AND T6.ATSCOD=T1.ATSCOD AND T6.APACOD IN ('2004') ) AS N2004,
													(SELECT T6.AAPVLA FROM IV46FP T6 WHERE T6.ACICOD=T1.ACICOD and T6.ADSNRO=T2.ADSNRO AND T6.ATSCOD=T1.ATSCOD AND T6.APACOD IN ('2005') ) AS N2005,
													(SELECT T6.AAPVLA FROM IV46FP T6 WHERE T6.ACICOD=T1.ACICOD and T6.ADSNRO=T2.ADSNRO AND T6.ATSCOD=T1.ATSCOD AND T6.APACOD IN ('2006') ) AS N2006,
													(SELECT T6.AAPVLA FROM IV46FP T6 WHERE T6.ACICOD=T1.ACICOD and T6.ADSNRO=T2.ADSNRO AND T6.ATSCOD=T1.ATSCOD AND T6.APACOD IN ('2007') ) AS N2007,
													(SELECT T6.AAPVLA FROM IV46FP T6 WHERE T6.ACICOD=T1.ACICOD and T6.ADSNRO=T2.ADSNRO AND T6.ATSCOD=T1.ATSCOD AND T6.APACOD IN ('2008') ) AS N2008,
													(SELECT T6.AAPVLA FROM IV46FP T6 WHERE T6.ACICOD=T1.ACICOD and T6.ADSNRO=T2.ADSNRO AND T6.ATSCOD=T1.ATSCOD AND T6.APACOD IN ('2009') ) AS N2009,
													(SELECT T6.AAPVLA FROM IV46FP T6 WHERE T6.ACICOD=T1.ACICOD and T6.ADSNRO=T2.ADSNRO AND T6.ATSCOD=T1.ATSCOD AND T6.APACOD IN ('2010') ) AS N2010,
													(SELECT T6.AAPVLA FROM IV46FP T6 WHERE T6.ACICOD=T1.ACICOD and T6.ADSNRO=T2.ADSNRO AND T6.ATSCOD=T1.ATSCOD AND T6.APACOD IN ('2011') ) AS N2011,
													( SELECT TRIM(PRAPIN) || ' ' || TRIM(SEAPIN) || ' ' || TRIM(NOMBIN) FROM INTRAMED.INEMP WHERE CIASIN='01' AND CEDUIN=(SELECT T6.AAPVLA FROM IV46FP T6 WHERE T6.ACICOD=T1.ACICOD and T6.ADSNRO=T2.ADSNRO AND T6.ATSCOD=T1.ATSCOD AND T6.APACOD IN (2012)) and STTRIN ='A' ) AS N2012, 
													(SELECT T6.AAPVLA FROM IV46FP T6 WHERE T6.ACICOD=T1.ACICOD and T6.ADSNRO=T2.ADSNRO AND T6.ATSCOD=T1.ATSCOD AND T6.APACOD IN ('2016') ) AS N2016,
													(SELECT T6.AAPVLA FROM IV46FP T6 WHERE T6.ACICOD=T1.ACICOD and T6.ADSNRO=T2.ADSNRO AND T6.ATSCOD=T1.ATSCOD AND T6.APACOD IN ('2017') ) AS N2017,
													(SELECT T6.AAPVLA FROM IV46FP T6 WHERE T6.ACICOD=T1.ACICOD and T6.ADSNRO=T2.ADSNRO AND T6.ATSCOD=T1.ATSCOD AND T6.APACOD IN ('2018') ) AS N2018,
													(SELECT T6.AAPVLA FROM IV46FP T6 WHERE T6.ACICOD=T1.ACICOD and T6.ADSNRO=T2.ADSNRO AND T6.ATSCOD=T1.ATSCOD AND T6.APACOD IN ('2020') ) AS N2020,
													(SELECT T6.AAPVLA FROM IV46FP T6 WHERE T6.ACICOD=T1.ACICOD and T6.ADSNRO=T2.ADSNRO AND T6.ATSCOD=T1.ATSCOD AND T6.APACOD IN ('2021') ) AS N2021,
													(SELECT T6.AAPVLA FROM IV46FP T6 WHERE T6.ACICOD=T1.ACICOD and T6.ADSNRO=T2.ADSNRO AND T6.ATSCOD=T1.ATSCOD AND T6.APACOD IN ('2022') ) AS N2022,
													( SELECT AITCOD FROM IS22FP WHERE ACICOD='42' AND AITCOD=(SELECT T6.AAPVLA FROM IV17FP T6 WHERE T6.ACICOD=42 AND T6.AARCOD='".$paginat[$g]["AARCOD"]."' AND T6.APACOD='1204') ) AS N2023, 
													( SELECT AITDES FROM IS22FP WHERE ACICOD='42' AND AITCOD=(SELECT T6.AAPVLA FROM IV17FP T6 WHERE T6.ACICOD=42 AND T6.AARCOD='".$paginat[$g]["AARCOD"]."' AND T6.APACOD='1204') ) AS N2024
														FROM IV36FP T1 
															INNER JOIN IV35FP T2 ON ( T1.ACICOD = T2.ACICOD AND 
																					  T1.AALCOD = T2.AALCOD AND 
																					  T1.ADPCOD = T2.ADPCOD AND 
																					  T1.ATSCOD = T2.ATSCOD AND 
																					  T1.ADSNRO = T2.ADSNRO 
																					) 
															INNER JOIN IS15FP T3 ON (T1.ACICOD=T3.ACICOD AND T2.ATSCOD=T3.ATSCOD) 
															INNER JOIN IV42FP T4 ON (T2.ACICOD=T4.ACICOD AND T2.AISCOD=T4.AISCOD)
															INNER JOIN IS01FP T5 ON (T1.ACICOD=T5.ACICOD AND T1.AALCOD=T5.AALCOD) 
														WHERE T1.ACICOD='$Compania' AND 
															  T1.AARCOD= '".$paginat[$g]["AARCOD"]."' AND 
															  T1.AALCOD in ('0004', '0005', '0006') AND 
															  T3.ATSCOD in ($tipoDot)
													    GROUP BY T2.ADSNRO, T3.ATSCOD, T3.ATSDES, T2.ATRFEC, T2.ATROBS, T4.AISCOD, T4.AISDES, T5.AUNDES, T1.ACICOD, T2.ADSNRO, T1.ATSCOD
														ORDER BY T2.ATRFEC ";

											
											//echo $sql3."<br/><br/>";
											$result3=odbc_exec($cid,$sql3)or die(exit("Error en odbc_exec 11111"));
											$result33=odbc_exec($cid,$sql3)or die(exit("Error en odbc_exec 11111"));
											//echo "<br>"."+".$canSal = odbc_num_rows($result3);
											$canSal=0;
											//$canEnt = odbc_num_rows($result22);
											while(odbc_fetch_row($result33)){
												$canSal++;
											}
											//echo "<br>"."*".$canSal;
											
											
											$resultES = '';
											$numRows = '';
											if($canSal>$canEnt){
												$resultES = $result3;
												$numRows = $canSal;
												//echo "<br>"."* salida";
											}else if($canSal<=$canEnt){
												$resultES = $result2;
												$numRows = $canEnt;
												//echo "<br>"."* entrada";
											}
											$rowpos = 0;

											//echo "numRows:".$numRows;
											//echo "rowpos:".$rowpos;				
                                 ?>     
                                        <!--<tr >
	                                                <td style="text-align:left;vertical-align:middle;" rowspan="<?php //echo $numRows; ?>"><?php echo $paginat[$g]["AARCOD"]; ?></td>
                                                    <td style="text-align:left;vertical-align:middle;" rowspan="<?php //echo $numRows; ?>"><?php echo $paginat[$g]["AARDES"]; ?></td>
                                         -->
                                                
                                 <?php
                                             //while(odbc_fetch_row($resultES)){   
											 if($numRows>$rowpos)
											 {
											 while($numRows>$rowpos){   
											 
											//echo "numRows:".$numRows;
											//echo "rowpos:".$rowpos;
											?>
                                            <tr >
	                                                <td style="text-align:left;vertical-align:middle;" rowspan="<?php //echo $numRows; ?>"><?php echo $paginat[$g]["AARCOD"]; ?></td>
                                                    <td style="text-align:left;vertical-align:middle;" rowspan="<?php //echo $numRows; ?>"><?php echo $paginat[$g]["AARDES"]; ?></td>
                                                    <td style="text-align:left;vertical-align:middle;" rowspan="<?php //echo $numRows; ?>">
														<div>
															<?php 
                                                                $list = list_ubiart($cid, $Compania, $tipoAlm, $paginat[$g]["AARCOD"],2);
                                                                //echo "*".trim($paginat[$g]["AUBCOD"])."*";
                                                                //echo '<a rel="shadowbox;width=400;height=300" href="editarfubicacion.php?&aarcod='.trim($paginat[$g]["AARCOD"]).'&aalcod='.$aalcod.'" title="Editar" >';
                                                                
                                                                if(trim($list)!=''){
                                                                    echo $list; } 
                                                                else { 
                                                                    echo ' - Sin Ubicaci&oacute;n';
                                                                }
                                                                //echo '</a>';
                                                             ?>
                                                        </div>
                                                    </td>
                                            <?php
											
											          $rowpos++;       
													  odbc_fetch_row($result2);
													  odbc_fetch_row($result3);                                     
												if($rowpos<=$canEnt){	
												?>
                                                <!-- entradas -->
                                                <?php 
													
													if(odbc_result($result2, 'ATRCOD')=='0210'){
														?>
                                                        <td style="text-align:center;vertical-align:middle;"><?php echo odbc_result($result2, 'ADSNRO')!=''?add_ceros(odbc_result($result2, 'ADSNRO'),6):'&nbsp;'; echo "<strong>(D)</strong>";?></td>
                                                        <?php
													}else{
														?>
                                                        <td style="text-align:center;vertical-align:middle;"><?php echo odbc_result($result2, 'ATRNUM')!=''?add_ceros(odbc_result($result2, 'ATRNUM'),6):'&nbsp;'; echo '(E)';?></td>
                                                        <?php
													}
												
									?>
                                                    
                                                    <!--<td ><?php echo odbc_result($result2, 'ATRDES')!=''?odbc_result($result2, 'ATRDES'):'&nbsp;';?></td>-->
                                                    <td style="text-align:center;vertical-align:middle;"><?php echo odbc_result($result2, 'ATRFEC')!=''?formatDate(odbc_result($result2, 'ATRFEC'),'aaaa-mm-dd','dd/mm/aaaa' ):'&nbsp;';?></td>
                                                    <td style="text-align:center;vertical-align:middle;"><?php echo odbc_result($result2, 'ATRCAN')!=''?number_format(odbc_result($result2, 'ATRCAN'),2,',','.'):'&nbsp;';?></td>
                                                    <!--<td style="text-align:center;vertical-align:middle;"><?php echo odbc_result($result2, 'DESATR')!=''?odbc_result($result2, 'DESATR'):'&nbsp;';?></td>-->
                                                    <!--<td style="text-align:center;vertical-align:middle;"><?php echo odbc_result($result2, 'ATROBS')!=''?odbc_result($result2, 'ATROBS'):'&nbsp;';?></td>-->
                                 <?php 			}else{	//if($rowpos<=$canEnt)?>
                                					<td style="text-align:center;vertical-align:middle;">--</td>
                                                    <td style="text-align:center;vertical-align:middle;">--</td>
                                                    <!--<td ></td>-->
                                                    <td style="text-align:center;vertical-align:middle;">--</td>
                                                    <!--<td ></td>-->
                                                    <!--<td ></td>-->
                              <?php 
                                            	 }
												 //echo odbc_result($result3, 'ATRCAN').'|'.$contador++;
												 //if($rowpos<=$canSal)
											 	if(  ($rowpos <= $canSal) && (number_format(odbc_result($result3, 'ATRCAN'),0,',','.') >= 0)  ){	
												
												?>                      
                                                    <!-- salida -->
                                                    <td style="text-align:center;vertical-align:middle;"><?php echo odbc_result($result3, 'ADSNRO')!=''?add_ceros(odbc_result($result3, 'ADSNRO'),6):'&nbsp;'; echo "(S)";?></td>
                                                    <!--<td ><?php echo odbc_result($result3, 'ATSDES')!=''?odbc_result($result3, 'ATSDES'):'&nbsp;';?></td>-->
                                                    <td style="text-align:center;vertical-align:middle;"><?php echo odbc_result($result3, 'ATRFEC')!=''?formatDate(odbc_result($result3, 'ATRFEC'),'aaaa-mm-dd','dd/mm/aaaa' ):'&nbsp;';?></td>
                                                    <td style="text-align:center;vertical-align:middle;"><?php echo odbc_result($result3, 'ATRCAN')!=''?number_format(odbc_result($result3, 'ATRCAN'),2,',','.'):'&nbsp;';?></td>
                                                    <!--<td style="text-align:center;vertical-align:middle;"><?php echo odbc_result($result3, 'AISDES')!=''?odbc_result($result3, 'AISDES'):'&nbsp;';?></td>
                                                    <td style="text-align:center;vertical-align:middle;"><?php echo odbc_result($result3, 'ATROBS')!=''?odbc_result($result3, 'ATROBS'):'&nbsp;';?></td>-->
                                                    <!--<td style="text-align:center;vertical-align:middle;"><?php echo odbc_result($result3, 'N2002')!=''?odbc_result($result3, 'N2002'):'--';?></td>
                                                    <td style="text-align:center;vertical-align:middle;"><?php echo odbc_result($result3, 'N2003')!=''?odbc_result($result3, 'N2003'):'--';?></td>
                                                    <td style="text-align:center;vertical-align:middle;"><?php echo odbc_result($result3, 'N2005')!=''?odbc_result($result3, 'N2005'):'--';?></td>
                                                    <td style="text-align:center;vertical-align:middle;"><?php echo odbc_result($result3, 'N2006')!=''?odbc_result($result3, 'N2006'):'--';?></td>
                                                    <td style="text-align:center;vertical-align:middle;"><?php echo odbc_result($result3, 'N2007')!=''?odbc_result($result3, 'N2007'):'--';?></td>
                                                    <td style="text-align:center;vertical-align:middle;"><?php echo odbc_result($result3, 'N2008')!=''?odbc_result($result3, 'N2008'):'--';?></td>
                                                    <td style="text-align:center;vertical-align:middle;"><?php echo odbc_result($result3, 'N2009')!=''?odbc_result($result3, 'N2009'):'--';?></td>
                                                    <td style="text-align:center;vertical-align:middle;"><?php echo odbc_result($result3, 'N2010')!=''?odbc_result($result3, 'N2010'):'--';?></td>
                                                    <td style="text-align:center;vertical-align:middle;"><?php echo odbc_result($result3, 'N2011')!=''?odbc_result($result3, 'N2011'):'--';?></td>
                                                    <td style="text-align:center;vertical-align:middle;"><?php echo odbc_result($result3, 'N2012')!=''?odbc_result($result3, 'N2012'):'--';?></td>
                                                    <td style="text-align:center;vertical-align:middle;"><?php echo odbc_result($result3, 'N2016')!=''?odbc_result($result3, 'N2016'):'--';?></td>
                                                    <td style="text-align:center;vertical-align:middle;"><?php echo odbc_result($result3, 'N2017')!=''?odbc_result($result3, 'N2017'):'--';?></td>
                                                    <td style="text-align:center;vertical-align:middle;"><?php echo odbc_result($result3, 'N2018')!=''?odbc_result($result3, 'N2018'):'--';?></td>
                                                    <td style="text-align:center;vertical-align:middle;"><?php echo odbc_result($result3, 'N2020')!=''?odbc_result($result3, 'N2020'):'--';?></td>
                                                    <td style="text-align:center;vertical-align:middle;"><?php echo odbc_result($result3, 'N2021')!=''?odbc_result($result3, 'N2021'):'--';?></td>
                                                    <td style="text-align:center;vertical-align:middle;"><?php echo odbc_result($result3, 'N2022')!=''?odbc_result($result3, 'N2022'):'--';?></td>-->
                                <?php 			}else{ //if($rowpos <= $canSal)?>
                                					<td style="text-align:center;vertical-align:middle;">--</td>
                                                    <!--<td ></td>-->
                                                    <td style="text-align:center;vertical-align:middle;">--</td>
                                                    <td style="text-align:center;vertical-align:middle;">--</td>
                                                    <!--<td ></td>
                                                    <td ></td>-->
                              <?php 			}//if($rowpos <= $canSal)
                                            	//if($rowpos == 1)
												{	?> 
                                                   <td style="text-align:center;vertical-align:middle;" rowspan="<?php //echo $numRows; ?>"><?php echo round($paginat[$g]["SALFIN"]); ?></td>
								<?php 			}//fin if  if($rowpos == 1)?>
                                            </tr>
                               <?php		}//fin while($numRows>$rowpos){
											}else{	?> 
                                                    <td style="text-align:center;vertical-align:middle;">--</td>
                                                    <!--<td ></td>-->
                                                    <td style="text-align:center;vertical-align:middle;">--</td>
                                                    <td style="text-align:center;vertical-align:middle;">--</td>
                                                    <!--<td ></td>-->
                                                    <!--<td ></td>-->
                                                    <td style="text-align:center;vertical-align:middle;">--</td>
                                                    
                                                    <td style="text-align:center;vertical-align:middle;">--</td>
                                                    <!--<td ></td>-->
                                                    <td style="text-align:center;vertical-align:middle;">--</td>
                                                    <td style="text-align:center;vertical-align:middle;">--</td>
                                                    <!--<td ></td>
                                                    <td ></td>-->
                                                    <td style="text-align:center;vertical-align:middle;" rowspan="<?php //echo $numRows; ?>">-&nbsp;</td>
                                                </tr>
								<?php 		}
								echo "";
									}	?>     
                       </tbody>                                         
                </table>
</div>
		  </div>
	  </div>
	</div>
		<!-- end #content -->
		<!-- end #sidebar -->
		<div style="clear: both;">&nbsp;</div>
	</div>
	<!-- end #page -->

	<div id="footer">
		<div id="opcionesmasusadas"><?php include("../../opcionesmasusadasphp.php"); ?></div>
	</div>
 	<?php 
		echo "<script>
				var list = [];
				var pos = 1;
				var lista = \"".$tipoInve."\";
				$('input[name=\"tinvan[]\"]').each(function() { 
					//alert($(this).val() );
					if( lista.indexOf($(this).val()) != -1 ){
						//alert('paso '+$(this).val()) ;
						 $(this).attr('checked', true); 
					}
					pos++;
				});
			  </script>";
	?>
	<!-- end #footer -->
</body>
</html>
