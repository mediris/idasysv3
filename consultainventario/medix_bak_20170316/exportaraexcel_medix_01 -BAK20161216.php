<?php session_start();
include("../../conectar.php");
$arqnro = trim($_GET["num"]);
$tinva = unserialize( urldecode( $_GET['tinvan'] ) ) ;
$tipoAlm = $_GET['aticod'];
$tipoInve = '0001'; // TIPO INVENTARIO - EQUIPOS
include("../../PHPExcel/PHPExcel.php");
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header("Content-disposition: attachment; filename=Reporte_MEDIX".formatDate($Fechaactual,'dd.mm.aaa','aaaa_mm_dd').".xlsx");
header('Cache-Control: max-age=0');
?>
<?php 
				
				
		 		$wsolicitud=0;
				if ($solicitudpagina==0) 	
				{
						
						$z=0;
						$tothon=0;
						$totest=0;
						$lin=1;
						$limitep=$_SESSION['solicitudlineasporpaginat'];
						$pag=1;
						$primero='S';

						/*
						//Almacen segun Tipo de Inventario
						$cod = '';
						if($tipoInve=='0001'){ // Equipos
							$cod = '0002'; // ALMACEN -> NV MDT - MEDIX 
						} elseif($tipoInve=='0002'){ //Consumibles
							$cod = '0001,0002'; //ALMACEN -> BI y NV MDT - MEDIX
						} elseif ($tipoInve=='0003') { //Repuestos
							$cod = '0003'; //ALMACEN -> Repuestos
						}
	*/
						//echo $cod.'<br>';

						$sql="SELECT T3.AARUMB, T4.AUMDES, T1.ACICOD, T1.AARCOD, T1.AARNIV, T1.ATICOD, T1.ASICOD, T1.AARDES, T1.AARTDT, T1.AARSTS, T1.ATICOD, T6.ATIDES, T7.AMCDES,
								(SELECT (SUM(T8.ASLENT)+SUM(T8.ASLSAL)) FROM IV40FP T8 WHERE T8.ACICOD=T1.ACICOD AND T8.AARCOD=T1.AARCOD AND T8.AALCOD in ('0002')) AS SALFIN
							FROM IV05FP T1 
								INNER JOIN IV06FP T3 ON ( T1.ACICOD=T3.ACICOD AND T1.AARCOD=T3.AARCOD ) 
								INNER JOIN IV13FP T4 ON ( T1.ACICOD=T4.ACICOD AND T3.AARUMB=T4.AUMCOD ) 
								INNER JOIN IV01FP T6 ON (T1.ACICOD=T6.ACICOD  AND T1.ATICOD=T6.ATICOD)
								INNER JOIN IV04FP T7 ON (T3.ACICOD=T7.ACICOD AND T3.AMCCOD=T7.AMCCOD)
							WHERE T1.ACICOD='$Compania' AND 
								( T1.AARCOD IN (SELECT T2.AARCOD FROM IV40FP T2 WHERE T2.ACICOD=T1.ACICOD AND T2.AALCOD in ('0002') GROUP BY T2.AARCOD ORDER BY T2.AARCOD ) ) ";
						if(!empty($tipoInve)){
							$sql.="  AND T1.ATICOD IN (".$tipoInve.")";
						}
						$sql.="  
							ORDER BY T1.AARDES";
						
						//echo $sql."<br/><br/>";
						$resultt=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111"));
						
							while(odbc_fetch_row($resultt))
							{
								$jml = odbc_num_fields($resultt);
								$row[$z]["pagina"] =  $pag;
								for($i=1;$i<=$jml;$i++)
								{	
									$row[$z][odbc_field_name($resultt,$i)] =  odbc_result($resultt,$i);
								}
								$z++;
								if ($lin>=$limitep) 
								{
									$limitep+=$_SESSION['solicitudlineasporpaginat'];
									$pag++;
								}
								$lin++;
							}

						$totsol=($lin-1);
						$_SESSION['totalsolicitudes']=$totsol;
						$_SESSION['solicitudarreglo']=$row;
						$solicitudpagina=1;
						$_SESSION['solicitudpaginas']=$pag;
					}//fin de solicitudpagina
					/*se muestra la cantidad de elementos segun la solicitada en $solicitudpagina*/
					$paginat=$_SESSION['solicitudarreglo'];

					//var_dump($paginat);

					$excel = new PHPExcel();

						//Propiedades del Excel
						$excel->getProperties()
						   ->setCreator('IDASYS WEB')
						   ->setTitle('Reporte MEDIX / Entrada y Salida')
						   ->setLastModifiedBy('IDASYS WEB')
						   ->setDescription('A demo to show how to use PHPExcel to manipulate an Excel file')
						   ->setSubject('Reporte de Entrada y Salida MEDIX')
						   ->setKeywords('excel php office phpexcel')
						   ->setCategory('inventario')
						   ;

						$exceldata = $excel->getSheet(0);
						$exceldata->setTitle('Reporte_MEDIX_Entrada_Salida');

						//Titulo
						$styleArray = array(
							    'font'  => array(
							        'bold'  => true
							    ),
							    'fill' => array(
					            'type' => PHPExcel_Style_Fill::FILL_SOLID,
					            'color' => array('rgb' => 'BDBDBD')
					            ),
					            'alignment' => array(
	            				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
	            				),
	            				'borders' => array(
					            'allborders' => array(
					                'style' => PHPExcel_Style_Border::BORDER_THIN,
					            	)
					        	)
						    );

						//Head Logo
					    $objDrawing = new PHPExcel_Worksheet_Drawing();
				        $objDrawing->setName('Logo');
				        $objDrawing->setDescription('Logo');
				        if($Compania == '14'){ 
					        $objDrawing->setPath('../../images/logoidacadef2005.png');
					        $objDrawing->setHeight(70);
					        $objDrawing->setWidth(200);
					    } else {
					    	$objDrawing->setPath('../../images/MEDITRON_logo_rif.png');
					        $objDrawing->setHeight(70);
				        	$objDrawing->setWidth(330);
					    }

				        $objDrawing->setCoordinates('A1');
				        $objDrawing->setWorksheet($excel->getActiveSheet());
				        //

						$excel->getActiveSheet()->getCell('A4')->setValue('RIF: '.$Companiarif);

						//Titulo 
						$excel->getActiveSheet()->getCell('A7')->setValue('REPORTE MEDIX');
						$exceldata->mergeCells('A7:O7');
						$excel->getActiveSheet()->getStyle('A7')->getFont()->setBold(true);
						$excel->getActiveSheet()->getStyle('A7')->getFont()->setSize(25);
						$excel->getActiveSheet()->getStyle('A7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
						//

						//Tipo de Inventario	
						$sql2="SELECT T6.ATIDES
						FROM IV01FP T6 
						WHERE T6.ACICOD='$Compania'  AND T6.ATICOD in (".$tipoInve.") ";
							
						$result2=odbc_exec($cid,$sql2)or die(exit("Error en odbc_exec 11111"));
						$tInventario = ''; 
						while(odbc_fetch_row($result2))
						{
							$tInventario .= trim(odbc_result($result2,'ATIDES'));
						}

						if(empty($tInventario)){$tInventario='N/A';}

						//Sub titulo 
						$excel->getActiveSheet()->getCell('A8')->setValue('Entrada y Salida de ' . $tInventario);
						$exceldata->mergeCells('A8:O8');
						$excel->getActiveSheet()->getStyle('A8')->getFont()->setBold(true);
						$excel->getActiveSheet()->getStyle('A8')->getFont()->setSize(20);
						$excel->getActiveSheet()->getStyle('A8')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

						$excel->getActiveSheet()->getCell('A9')->setValue('Elaborado el '.$Fechaactual.' a las '.$Horaactual2);
						$exceldata->mergeCells('A9:O9');
						$excel->getActiveSheet()->getStyle('A9')->getFont()->setBold(true);
						$excel->getActiveSheet()->getStyle('A9')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

						$excel->getActiveSheet()->getCell('A10')->setValue('Almacén Principal: MEDIX');
						$exceldata->mergeCells('A10:O10');
						$excel->getActiveSheet()->getStyle('A4')->getFont()->setBold(true);
						$excel->getActiveSheet()->getStyle('A10')->getFont()->setBold(true);
						$excel->getActiveSheet()->getStyle('A10')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

						$excel->getActiveSheet()->getCell('A11')->setValue('Filtrado por: '.$tInventario);
						$exceldata->mergeCells('A11:O11');
						$excel->getActiveSheet()->getStyle('A11')->getFont()->setBold(true);
						$excel->getActiveSheet()->getStyle('A11')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

						$exceldata->setCellValue('A12', 'Código');
						$exceldata->setCellValue('B12', 'Artículo');
						$exceldata->setCellValue('C12', 'Marca');
						$exceldata->setCellValue('D12', 'Tipo Inventario');
						$exceldata->setCellValue('E12', 'Ubicación');
						$exceldata->setCellValue('F12', 'Número Referencia Entrada');
						$exceldata->mergeCells('A12:A13');
						$exceldata->mergeCells('B12:B13');
						$exceldata->mergeCells('C12:C13');
						$exceldata->mergeCells('D12:D13');
						$exceldata->mergeCells('E12:E13');
						$exceldata->mergeCells('F12:F13');
						$excel->getActiveSheet()->getStyle('A12:V12')->applyFromArray($styleArray);

						//Entradas
						$exceldata->setCellValue('G12', 'Entradas'); // Sets cell 'a1' to value 'ID
						$exceldata->mergeCells('G12:I12'); 
					    $exceldata->setCellValue('G13', 'Fecha');
					    $exceldata->setCellValue('H13', 'Cantidad');
					    $exceldata->setCellValue('I13', 'Observaciones');

					    $exceldata->setCellValue('J12', 'Número Referencia Salidas');
					    $exceldata->mergeCells('J12:J13');

					    $exceldata->setCellValue('K12', 'Tipo de Salida');
					    $exceldata->mergeCells('K12:K13');

					    $exceldata->setCellValue('L12', 'Almacén');
					    $exceldata->mergeCells('L12:L13');

					    //Salidas
					    $exceldata->setCellValue('M12', 'Salidas'); // Sets cell 'a1' to value 'ID
						$exceldata->mergeCells('M12:U12'); 
					    $exceldata->setCellValue('M13', 'Fecha');
					    $exceldata->setCellValue('N13', 'Cantidad');
					    $exceldata->setCellValue('O13', 'Destino');
					    $exceldata->setCellValue('P13', 'Observaciones');
					    $exceldata->setCellValue('Q13', 'Fecha de Recepción');
					    $exceldata->setCellValue('R13', 'Recibido por:');
					    $exceldata->setCellValue('S13', 'Recibido por 2:');
					    $exceldata->setCellValue('T13', 'Nro. ITEM');
					    $exceldata->setCellValue('U13', 'Descripción del ITEM');

					    $exceldata->setCellValue('V12', 'Existencia Actual');
					    $exceldata->mergeCells('V12:V13');
					    $excel->getActiveSheet()->getStyle('A13:V13')->applyFromArray($styleArray);
					    				   		
					    //Genera Data
					    $pos = 14;
					    for($g=0; $g < (count($paginat)); $g++)
									{
											/*entradas*/
											$sql2="( SELECT T3.ATRNUM, T3.ATRCOD, T2.ATRDES, T3.ATRFEC, (SUM(T1.ATRCAN)) AS ATRCAN, (T3.ATRDES) AS DESATR, T3.ATROBS, T5.ADSNRO, T5.ATSCOD 
													FROM IV16FP T1 
														INNER JOIN IV15FP T3 ON (T1.ACICOD=T3.ACICOD AND T1.ATRCOD=T3.ATRCOD AND T1.ATRNUM=T3.ATRNUM AND T1.ADPCOD=T3.ADPCOD AND T3.ATRSTS='02' ) 
														INNER JOIN iv12fp T2 ON(T1.ACICOD= T2.ACICOD AND T1.ATRCOD=T2.ATRCOD) 
														LEFT JOIN IV35FP T5 ON (T1.ACICOD=T5.ACICOD AND T1.AALCOD=T5.AALCOD AND T3.ADPCOD=T5.ADPCOD AND T1.ATRCOD=T5.ATRCOD AND T1.ATRNUM=T5.ATRNUM) 
													WHERE T1.ACICOD='42' AND T1.AALCOD IN ('0002') AND T1.ATRCOD in ( '0101', '0102','0210' ) AND T1.ATRART='".$paginat[$g]["AARCOD"]."'
													GROUP BY T3.ATRNUM, T3.ATRCOD, T2.ATRDES, T3.ATRFEC,T3.ATRDES, T3.ATROBS, T5.ADSNRO, T5.ATSCOD   
													ORDER BY T3.ATRFEC 
													)UNION( 
													SELECT T3.ATRNUM, T3.ATRCOD, T2.ATRDES, T3.ATRFEC, (SUM(T1.ATRCAN)) AS ATRCAN, (T3.ATRDES) AS DESATR, T3.ATROBS, T5.ADSNRO, T5.ATSCOD 
													FROM IV16FP T1
													 INNER JOIN IV15FP T3 ON (T1.ACICOD=T3.ACICOD AND T1.ATRCOD=T3.ATRCOD AND T1.ATRNUM=T3.ATRNUM AND T1.ADPCOD=T3.ADPCOD ) 
													 INNER JOIN iv12fp T2 ON(T1.ACICOD= T2.ACICOD AND T1.ATRCOD=T2.ATRCOD) 
													 LEFT JOIN IV35FP T5 ON (T1.ACICOD=T5.ACICOD AND T1.AALCOD=T5.AALCOD AND T3.ADPCOD=T5.ADPCOD AND T1.ATRCOD=T5.ATRCOD AND T1.ATRNUM=T5.ATRNUM) 
													 WHERE T1.ACICOD='42' AND T1.AALCOD not in ('0004', '0005', '0006') AND T1.ATRCOD in ( '0210' ) AND T1.ATRART='".$paginat[$g]["AARCOD"]."'
													 GROUP BY T3.ATRNUM, T3.ATRCOD, T2.ATRDES, T3.ATRFEC,T3.ATRDES, T3.ATROBS, T5.ADSNRO, T5.ATSCOD   
													 ORDER BY T3.ATRFEC )";
											$result2=odbc_exec($cid,$sql2)or die(exit("Error en odbc_exec 11111"));
											$result22=odbc_exec($cid,$sql2)or die(exit("Error en odbc_exec 11111"));
											$canEnt=0;
											while(odbc_fetch_row($result22)){
												$canEnt++;
											}
											//echo $canEnt;
											//die();
											//echo $sql2.'<br>';
											//die();

											/*salidas*/
											$sql3="SELECT T2.ADSNRO, T3.ATSCOD, T3.ATSDES, T2.ATRFEC, (SUM(T1.ATRCAN)) AS ATRCAN, T2.ATROBS, T4.AISCOD, T4.AISDES, T5.AUNDES, T1.ACICOD, T2.ADSNRO, T1.ATSCOD,
													(SELECT T6.AAPVLA FROM IV46FP T6 WHERE T6.ACICOD=T1.ACICOD and T6.ADSNRO=T2.ADSNRO AND T6.ATSCOD=T1.ATSCOD AND T6.APACOD IN ('2002') ) AS N2002,
													(SELECT T6.AAPVLA FROM IV46FP T6 WHERE T6.ACICOD=T1.ACICOD and T6.ADSNRO=T2.ADSNRO AND T6.ATSCOD=T1.ATSCOD AND T6.APACOD IN ('2003') ) AS N2003,
													(SELECT T6.AAPVLA FROM IV46FP T6 WHERE T6.ACICOD=T1.ACICOD and T6.ADSNRO=T2.ADSNRO AND T6.ATSCOD=T1.ATSCOD AND T6.APACOD IN ('2004') ) AS N2004,
													(SELECT T6.AAPVLA FROM IV46FP T6 WHERE T6.ACICOD=T1.ACICOD and T6.ADSNRO=T2.ADSNRO AND T6.ATSCOD=T1.ATSCOD AND T6.APACOD IN ('2005') ) AS N2005,
													(SELECT T6.AAPVLA FROM IV46FP T6 WHERE T6.ACICOD=T1.ACICOD and T6.ADSNRO=T2.ADSNRO AND T6.ATSCOD=T1.ATSCOD AND T6.APACOD IN ('2006') ) AS N2006,
													(SELECT T6.AAPVLA FROM IV46FP T6 WHERE T6.ACICOD=T1.ACICOD and T6.ADSNRO=T2.ADSNRO AND T6.ATSCOD=T1.ATSCOD AND T6.APACOD IN ('2007') ) AS N2007,
													(SELECT T6.AAPVLA FROM IV46FP T6 WHERE T6.ACICOD=T1.ACICOD and T6.ADSNRO=T2.ADSNRO AND T6.ATSCOD=T1.ATSCOD AND T6.APACOD IN ('2008') ) AS N2008,
													(SELECT T6.AAPVLA FROM IV46FP T6 WHERE T6.ACICOD=T1.ACICOD and T6.ADSNRO=T2.ADSNRO AND T6.ATSCOD=T1.ATSCOD AND T6.APACOD IN ('2009') ) AS N2009,
													(SELECT T6.AAPVLA FROM IV46FP T6 WHERE T6.ACICOD=T1.ACICOD and T6.ADSNRO=T2.ADSNRO AND T6.ATSCOD=T1.ATSCOD AND T6.APACOD IN ('2010') ) AS N2010,
													(SELECT T6.AAPVLA FROM IV46FP T6 WHERE T6.ACICOD=T1.ACICOD and T6.ADSNRO=T2.ADSNRO AND T6.ATSCOD=T1.ATSCOD AND T6.APACOD IN ('2011') ) AS N2011,
													( SELECT TRIM(PRAPIN) || ' ' || TRIM(SEAPIN) || ' ' || TRIM(NOMBIN) FROM INTRAMED.INEMP WHERE CIASIN='01' AND CEDUIN=(SELECT T6.AAPVLA FROM IV46FP T6 WHERE T6.ACICOD=T1.ACICOD and T6.ADSNRO=T2.ADSNRO AND T6.ATSCOD=T1.ATSCOD AND T6.APACOD IN (2012)) and STTRIN ='A' ) AS N2012, 
													(SELECT T6.AAPVLA FROM IV46FP T6 WHERE T6.ACICOD=T1.ACICOD and T6.ADSNRO=T2.ADSNRO AND T6.ATSCOD=T1.ATSCOD AND T6.APACOD IN ('2016') ) AS N2016,
													(SELECT T6.AAPVLA FROM IV46FP T6 WHERE T6.ACICOD=T1.ACICOD and T6.ADSNRO=T2.ADSNRO AND T6.ATSCOD=T1.ATSCOD AND T6.APACOD IN ('2017') ) AS N2017,
													(SELECT T6.AAPVLA FROM IV46FP T6 WHERE T6.ACICOD=T1.ACICOD and T6.ADSNRO=T2.ADSNRO AND T6.ATSCOD=T1.ATSCOD AND T6.APACOD IN ('2018') ) AS N2018,
													(SELECT T6.AAPVLA FROM IV46FP T6 WHERE T6.ACICOD=T1.ACICOD and T6.ADSNRO=T2.ADSNRO AND T6.ATSCOD=T1.ATSCOD AND T6.APACOD IN ('2020') ) AS N2020,
													(SELECT T6.AAPVLA FROM IV46FP T6 WHERE T6.ACICOD=T1.ACICOD and T6.ADSNRO=T2.ADSNRO AND T6.ATSCOD=T1.ATSCOD AND T6.APACOD IN ('2021') ) AS N2021,
													(SELECT T6.AAPVLA FROM IV46FP T6 WHERE T6.ACICOD=T1.ACICOD and T6.ADSNRO=T2.ADSNRO AND T6.ATSCOD=T1.ATSCOD AND T6.APACOD IN ('2022') ) AS N2022,
													( SELECT AITCOD FROM IS22FP WHERE ACICOD='42' AND AITCOD=(SELECT T6.AAPVLA FROM IV17FP T6 WHERE T6.ACICOD=42 AND T6.AARCOD='".$paginat[$g]["AARCOD"]."' AND T6.APACOD='1204') ) AS N2023, 
													( SELECT AITDES FROM IS22FP WHERE ACICOD='42' AND AITCOD=(SELECT T6.AAPVLA FROM IV17FP T6 WHERE T6.ACICOD=42 AND T6.AARCOD='".$paginat[$g]["AARCOD"]."' AND T6.APACOD='1204') ) AS N2024
														FROM IV36FP T1 
															INNER JOIN IV35FP T2 ON ( T1.ACICOD = T2.ACICOD AND 
																					  T1.AALCOD = T2.AALCOD AND 
																					  T1.ADPCOD = T2.ADPCOD AND 
																					  T1.ATSCOD = T2.ATSCOD AND 
																					  T1.ADSNRO = T2.ADSNRO 
																					) 
															INNER JOIN IS15FP T3 ON (T1.ACICOD=T3.ACICOD AND T2.ATSCOD=T3.ATSCOD) 
															INNER JOIN IV42FP T4 ON (T2.ACICOD=T4.ACICOD AND T2.AISCOD=T4.AISCOD)
															INNER JOIN IS01FP T5 ON (T1.ACICOD=T5.ACICOD AND T1.AALCOD=T5.AALCOD) 
														WHERE T1.ACICOD='$Compania' AND 
															  T1.AARCOD= '".$paginat[$g]["AARCOD"]."' AND 
															  T1.AALCOD in ('0004', '0005', '0006') AND 
															  T3.ATSCOD in ('01','02','03','04')
													    GROUP BY T2.ADSNRO, T3.ATSCOD, T3.ATSDES, T2.ATRFEC, T2.ATROBS, T4.AISCOD, T4.AISDES, T5.AUNDES, T1.ACICOD, T2.ADSNRO, T1.ATSCOD
														ORDER BY T2.ATRFEC ";

											//echo $sql3.'<br>';
											
											//(SELECT * FROM INTRAMED.INEMP WHERE ACICOD='42' AND CI='')
											
											$result3=odbc_exec($cid,$sql3)or die(exit("Error en odbc_exec 11111"));
											$result33=odbc_exec($cid,$sql3)or die(exit("Error en odbc_exec 11111"));

											$canSal=0;
											while(odbc_fetch_row($result33)){
												$canSal++;
											}

											$numRows = '';

											if($canSal>$canEnt){
												$numRows = $canSal;
											}else if($canSal<=$canEnt){
												$numRows = $canEnt;
											}

											$ent = 0;
											$sal = 0;

											$rowpos = 0;
											$ent = '';

											//Codigo
											$exceldata->setCellValue('A' . $pos, trim($paginat[$g]["AARCOD"])); 

											//Articulo
											$exceldata->setCellValue('B' . $pos, utf8_encode(trim($paginat[$g]["AARDES"])));

											//Marca
											$cellc = $paginat[$g]['AMCDES']!=''?$paginat[$g]['AMCDES']:'N/A';
											$exceldata->setCellValue('C' . $pos, trim($cellc)); 

											//Tipo Inventario
											$exceldata->setCellValue('D' . $pos, trim($paginat[$g]["ATIDES"]));

											//Ubicacion 
                                            $list = list_ubiart($cid, $Compania, '0002', $paginat[$g]["AARCOD"],3);
                                                                
                                            if(trim($list)!=''){
                                                $exceldata->setCellValue('E' . $pos, trim($list)); 
                                            } else { 
                                                $exceldata->setCellValue('E' . $pos, ' - Sin Ubicación'); 
                                            }

                                            //Merge Celdas a la cantidad de NumRows
                                           if($numRows > 1)
											{
												$lineas = $pos+$numRows-1;
												$exceldata->mergeCells('A'.$pos.':A'.$lineas);
										    	$exceldata->mergeCells('B'.$pos.':B'.$lineas);
										    	$exceldata->mergeCells('C'.$pos.':C'.$lineas);
										    	$exceldata->mergeCells('D'.$pos.':D'.$lineas);
										    	$exceldata->mergeCells('E'.$pos.':E'.$lineas);
										    	$exceldata->mergeCells('V'.$pos.':V'.$lineas);
											}

											if($numRows>$rowpos){
												while($numRows>$rowpos){   
											 
											 $rowpos++;
											 odbc_fetch_row($result2);
											 odbc_fetch_row($result3);       

											 //	$ent = odbc_result($result2, 'ATRCAN');
												if($rowpos<=$canEnt){
													//Numero Referencia Entrada
													if(odbc_result($result2, 'ATRCOD')=='0210') { 
														$cellf = odbc_result($result2, 'ADSNRO')!=''?add_ceros(odbc_result($result2, 'ADSNRO'),6):' ';
														$exceldata->setCellValue('F' . $pos, $cellf.'(D)');
													}else{
                                                       $cellf = odbc_result($result2, 'ATRNUM')!=''?add_ceros(odbc_result($result2, 'ATRNUM'),6):'N/A';
                                                       $exceldata->setCellValue('F' . $pos, $cellf.'(E)');
													}	

														//Fecha
														$cellg = odbc_result($result2, 'ATRFEC')!=''?formatDate(odbc_result($result2, 'ATRFEC'),'aaaa-mm-dd','dd/mm/aaaa' ):'--/--/----';
														$exceldata->setCellValue('G' . $pos, $cellg);

														//Cantidad
														$cellh = odbc_result($result2, 'ATRCAN')!=''?odbc_result($result2, 'ATRCAN'):' ';
														$exceldata->setCellValue('H' . $pos, $cellh);

														//Observaciones
														$celli = odbc_result($result2, 'ATROBS')!=''?odbc_result($result2, 'ATROBS'):'';
														$exceldata->setCellValue('I' . $pos, utf8_encode($celli));	
                                             	}
                                             	//echo $rowpos.'<br>';
                                             	
                                             	if($rowpos>=1){
                                             		if(!empty($ent)){
                                             			$ent = $ent+odbc_result($result2, 'ATRCAN');
                                             		} else {
                                             			$ent = odbc_result($result2, 'ATRCAN');
                                             		}
                                             	}
                                             	//echo $ent.'<br>';

                                             	//echo $totalEnt.'<br>';

												if(  ($rowpos <= $canSal) && (number_format(odbc_result($result3, 'ATRCAN'),0,',','.') >= 0)  ){
                                                      
                                                   //Numero Referencia Salidas         
                                                   $cellj = odbc_result($result3, 'ADSNRO')!=''?add_ceros(odbc_result($result3, 'ADSNRO'),6):' ';
                                                   $exceldata->setCellValue('J' . $pos, $cellj.'(S)');

                                                   //Tipo de Salida       
                                                   $exceldata->setCellValue('K' . $pos, trim(odbc_result($result3, 'ATSDES')));

                                                   //Almacen         
                                                   $exceldata->setCellValue('L' . $pos, utf8_encode(trim(odbc_result($result3, 'AUNDES'))));

                                                   //Fecha
                                                   $cellm = odbc_result($result3, 'ATRFEC')!=''?formatDate(odbc_result($result3, 'ATRFEC'),'aaaa-mm-dd','dd/mm/aaaa' ):'&nbsp;';
                                                   $exceldata->setCellValue('M' . $pos, $cellm);

                                                   //Cantidad
                                                   $celln = odbc_result($result3, 'ATRCAN')!=''?odbc_result($result3, 'ATRCAN'):' ';
                                                   $exceldata->setCellValue('N' . $pos, $celln);

                                                   //Hospital
                                                   $exceldata->setCellValue('O' . $pos, 'HOSPITAL '.utf8_encode(trim(odbc_result($result3, 'AISDES'))));

                                                   //Observaciones
                                                   $exceldata->setCellValue('P' . $pos, utf8_encode(trim(odbc_result($result3, 'ATROBS'))));

                                                   //Instalado en el Equipo
                                                   /*$cellq =  odbc_result($result3, 'N2017')!=''?odbc_result($result3, 'N2017'):'';
                                                   $exceldata->setCellValue('Q' . $pos, utf8_encode($cellq));

                                                   //Serial
                                                   $cellr =  odbc_result($result3, 'N2018')!=''?odbc_result($result3, 'N2003'):'';
                                                   $exceldata->setCellValue('R' . $pos, utf8_encode($cellr));*/

                                                   //Fecha de Recepcion
                                                   $cellq =  odbc_result($result3, 'N2003')!=''?formatDate(odbc_result($result3, 'N2003'),'dd.mm.aaaa','dd/mm/aaaa' ):'';
                                                   $exceldata->setCellValue('Q' . $pos, utf8_encode($cellq)); 

                                                   //Recibido por 1: Nombre y Apellido . C.I. - Cargo
                                                   $cellr =  odbc_result($result3, 'N2004')!=''?odbc_result($result3, 'N2004').' / '.odbc_result($result3, 'N2005').' / '.odbc_result($result3, 'N2006'):'';
                                                   $exceldata->setCellValue('R' . $pos, utf8_encode($cellr));

                                                   //Recibido por 2: Nombre y Apellido . C.I. - Cargo
                                                   $cells =  odbc_result($result3, 'N2007')!=''?odbc_result($result3, 'N2007').' / '.odbc_result($result3, 'N2008').' / '.odbc_result($result3, 'N2009'):'';
                                                   $exceldata->setCellValue('S' . $pos, utf8_encode($cells));

                                                   //Nro. ITEM
                                                   $cellt =  odbc_result($result3, 'N2023')!=''?trim(odbc_result($result3, 'N2023')):'';
                                                   $exceldata->setCellValue('T' . $pos, utf8_encode($cellt));

                                                   //Descripcion del ITEM
                                                   $cellu =  odbc_result($result3, 'N2024')!=''?trim(odbc_result($result3, 'N2024')):'';
                                                   $exceldata->setCellValue('U' . $pos, utf8_encode($cellu));

                                                   //$sal = number_format(odbc_result($result3, 'ATRCAN'),2,',','.');  
	                                                   if($rowpos>=1){
	                                             		if(!empty($sal)){
	                                             			$sal = $sal+odbc_result($result3, 'ATRCAN');
	                                             		} else {
	                                             			$sal = odbc_result($result3, 'ATRCAN');
	                                             		}
	                                             	}
 												} 
                                            	if($rowpos == 1){
                                            	  // echo $ent.'<br>'.$sal.'<br>'.$salFin.'FIN <br>';
                                            		//$salFin = $ent-$sal;
                                                   /*$celly = $paginat[$g]["ASLSAF"]!=''?number_format($paginat[$g]["ASLSAF"],2,',','.'):"--";*/
                                                   $exceldata->setCellValue('V' . $pos, $paginat[$g]["SALFIN"]);
                                            	}//fin if  if($rowpos == 1)			
	                               				$pos++;
											}//fin while(odbc_fetch_row($resultES))
										}											
									}                                  

						//Obtiene la cantidad de rows del excel (-13 rows del titulo) 			
						$rowCount = $excel->getActiveSheet()->getHighestRow();
						$rowCount = $rowCount-13;

						//Bordes y ajuste de anchura para la data generada
						$lin = 14;
				   		$prevpos = $lin-2;
						$highestColumn = $excel->getActiveSheet()->getHighestColumn();
						for($col = 'A'; $col <= $highestColumn; $col++)
					    {
					    	$value = $excel->getActiveSheet()->getCell($col.$prevpos)->getValue();
				   			$width = mb_strwidth($value);
				   			if($width == 0){
				   				$prevpos = $lin-1;
				   				$value = $excel->getActiveSheet()->getCell($col.$prevpos)->getValue();
				   				$width = mb_strwidth($value);
				   			}
					    	for($i = 0; $i < $rowCount; $i++){
					    		$value1 = $excel->getActiveSheet()->getCell($col.$lin)->getValue();
				   				$width1 = mb_strwidth($value1);
								if($width1 > $width){
									$width = $width1;
								}
								$excel->getActiveSheet()->getStyle($col.$lin)->applyFromArray(array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN,))));
								$excel->getActiveSheet()->getStyle($col.$lin)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
								//Texto centrado a Columnas especificas
								$excel->getActiveSheet()->getStyle('A'.$lin)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
								$excel->getActiveSheet()->getStyle('B'.$lin)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
								$excel->getActiveSheet()->getStyle('C'.$lin)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
								$excel->getActiveSheet()->getStyle('D'.$lin)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
								$excel->getActiveSheet()->getStyle('E'.$lin)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
								$excel->getActiveSheet()->getStyle($col.$lin)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
								//Background color de celdas
								if($col == 'H' || $col == 'N' || $col == 'V'){ 
									if($col == 'H'){
										$excel->getActiveSheet()->getStyle($col.$lin)->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => '3399FF'))));
									} elseif($col == 'N') {
										$excel->getActiveSheet()->getStyle($col.$lin)->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => 'FFFF99'))));
									} elseif($col == 'V') {
										$excel->getActiveSheet()->getStyle($col.$lin)->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => '00FF00'))));
									}
								}
								//Wrap Text para la Columna 'D'
								$excel->getActiveSheet()->getStyle($col.$lin)->getAlignment()->setWrapText(true);
								$lin++;
								$prevpos++;
							}
							$width = $width+2;
							// IF para colocar un width estatico a la Columna y se aplique el WrapText
							if($col == 'A' || $col == 'B' || $col == 'I' || $col == 'T' || $col == 'U' || $col == 'V'){
								$width = 45;
								$excel->getActiveSheet()->getColumnDimension($col)->setAutoSize(false);
								$excel->getActiveSheet()->getColumnDimension($col)->setWidth($width);
							} else {
								$excel->getActiveSheet()->getColumnDimension($col)->setAutoSize(false);
								$excel->getActiveSheet()->getColumnDimension($col)->setWidth($width);
							}
							$width = 0;
							$lin = 14;
							$prevpos = $lin-2;
						}

						//Ocultar Columnas
						$excel->getActiveSheet()->getColumnDimension('D')->setVisible(false);
						$excel->getActiveSheet()->getColumnDimension('K')->setVisible(false);
						$excel->getActiveSheet()->getColumnDimension('S')->setVisible(false);

						//
						//Se genera el excel
				        $writer = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
						$writer->save('php://output');
						exit();
						?>