<?php
/*
 * Luis Ramos (lRamos)
 * 10/11/2016 
 */ 
session_start();
include("../../conectar.php");
include("../../PHPExcel/PHPExcel.php");
$arqnro = trim($_GET["num"]);
$tinva = unserialize( urldecode( $_GET['tinvan'] ) ) ;
$aticod = $_GET['aticod'];
$tipoAlm = '';
$tipoInve ='';
if(!empty($aticod)){
	if($aticod=='01'){
		$tipoInve = '0001'; //EQUIPOS
		$tipoAlm = '0002'; //ALMACEN EQUIPOS
		$tipoSal = '0203'; //TIPO DE SALIDA
		$filename = 'Reporte_MEDIX_CostosMOV'.formatDate($Fechaactual,'dd.mm.aaa','aaaa_mm_dd').".xlsx";
	} elseif($aticod=='02'){
		$tipoInve = '0002'; //CONSUMIBLES
		$tipoAlm = '0001'; //ALMACEN CONSUMIBLE BI
		$tipoSal = '0209'; //TIPO DE SALIDA
		$filename = 'Reporte_MEDIX_CostosMOV'.formatDate($Fechaactual,'dd.mm.aaa','aaaa_mm_dd').".xlsx";
	} elseif($aticod=='03'){
		$tipoInve = '0002'; //CONSUMIBLES
		$tipoAlm = '0002'; //ALMACEN CONSUMIBLE NV
		$tipoSal = '0206'; //TIPO DE SALIDA
		$filename = 'Reporte_MEDIX_CostosMOV'.formatDate($Fechaactual,'dd.mm.aaa','aaaa_mm_dd').".xlsx";
	} elseif($aticod=='04'){
		$tipoInve = '0003'; //REPUESTOS
		$tipoAlm = '0003'; //ALMACEN REPUESTOS
		$tipoSal = '0213'; //TIPO DE SALIDA
		$filename = 'Reporte_MEDIX_CostosMOV'.formatDate($Fechaactual,'dd.mm.aaa','aaaa_mm_dd').".xlsx";
	}
}
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header("Content-disposition: attachment; filename=".$filename."");
header('Cache-Control: max-age=0');
?>
<?php 

					$sql="SELECT T1.AARCOD, T1.AARDES,
							(SELECT SUM(T2.ATRCAN) FROM IV16FP T2 WHERE T2.ACICOD='42' AND T2.AALCOD IN ('$tipoAlm') AND T2.ATRCOD='0101' AND T2.ATRART=T1.AARCOD) as ENTRADAS,
							(SELECT SUM(T3.ATRCAN) FROM IV16FP T3 WHERE T3.ACICOD='42' AND T3.AALCOD IN ('0004', '0005') AND T3.ATRCOD='$tipoSal' AND T3.ATRART=T1.AARCOD) as SALIDAS, 
							(SELECT MAX(T4.ATRCUT) FROM IV16FP T4 WHERE T4.ACICOD='42' AND T4.AALCOD IN ('$tipoAlm') AND T4.ATRCOD='0101' AND T4.ATRART=T1.AARCOD) as COSTO,
							(SELECT SUM(T3.ATRCAN) FROM IV16FP T3 WHERE T3.ACICOD='42' AND T3.AALCOD IN ('$tipoAlm','0004', '0005') AND T3.ATRCOD='0210' AND T3.ATRART=T1.AARCOD) as DEVOLUCIONES
							FROM IV05FP T1 WHERE T1.ACICOD='42' AND (SELECT SUM(T2.ATRCAN) FROM IV16FP T2 WHERE T2.ACICOD='42' AND T2.AALCOD IN ('$tipoAlm') AND T2.ATRCOD='0101' AND T2.ATRART=T1.AARCOD) > 0 AND T1.ATICOD IN (".$tipoInve.")";
					//echo $sql.'<br>';
					$result = odbc_exec($cid, $sql)or die(exit("Error en odbc_exec 11111"));
					$resultrow = odbc_exec($cid, $sql)or die(exit("Error en odbc_exec 11111"));

					$excel = new PHPExcel();

						//Propiedades del Excel
						$excel->getProperties()
						   ->setCreator('IDASYS WEB')
						   ->setTitle('Reporte MEDIX / Costos con Movimiento')
						   ->setLastModifiedBy('IDASYS WEB')
						   ->setDescription('A demo to show how to use PHPExcel to manipulate an Excel file')
						   ->setSubject('Reporte de Costos con Movimientos')
						   ->setKeywords('excel php office phpexcel')
						   ->setCategory('inventario')
						   ;

						$exceldata = $excel->getSheet(0);
						$exceldata->setTitle('Reporte_CostosMOV_MEDIX');

						//Titulo
						$styleArray = array(
							    'font'  => array(
							        'bold'  => true
							    ),
							    'fill' => array(
					            'type' => PHPExcel_Style_Fill::FILL_SOLID,
					            'color' => array('rgb' => 'BDBDBD')
					            ),
					            'alignment' => array(
	            				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
	            				),
	            				'borders' => array(
					            'allborders' => array(
					                'style' => PHPExcel_Style_Border::BORDER_THIN,
					            	)
					        	)
						    );

						//Head Logo
					    $objDrawing = new PHPExcel_Worksheet_Drawing();
				        $objDrawing->setName('Logo');
				        $objDrawing->setDescription('Logo');
				        if($Compania == '14'){ 
					        $objDrawing->setPath('../../images/logoidacadef2005.png');
					        $objDrawing->setHeight(70);
					        $objDrawing->setWidth(200);
					    } else {
					    	$objDrawing->setPath('../../images/MEDITRON_logo_rif.png');
					        $objDrawing->setHeight(70);
				        	$objDrawing->setWidth(330);
					    }

				        $objDrawing->setCoordinates('A1');
				        $objDrawing->setWorksheet($excel->getActiveSheet());
				        //

						$excel->getActiveSheet()->getCell('A4')->setValue('RIF: '.$Companiarif);

						//Titulo 
						$excel->getActiveSheet()->getCell('A7')->setValue('REPORTE MEDIX');
						$exceldata->mergeCells('A7:I7');
						$excel->getActiveSheet()->getStyle('A7')->getFont()->setBold(true);
						$excel->getActiveSheet()->getStyle('A7')->getFont()->setSize(25);
						$excel->getActiveSheet()->getStyle('A7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
						//

						//Tipo de Inventario	
						$sql2="SELECT T6.ATIDES
						FROM IV01FP T6 
						WHERE T6.ACICOD='$Compania'  AND T6.ATICOD in (".$tipoInve.") ";
							
						$result2=odbc_exec($cid,$sql2)or die(exit("Error en odbc_exec 11111"));
						$tInventario = ''; 
						while(odbc_fetch_row($result2))
						{
							$tInventario .= trim(odbc_result($result2,'ATIDES'));
						}

						if(empty($tInventario)){$tInventario='N/A';}

						//Sub titulo 
						$excel->getActiveSheet()->getCell('A8')->setValue('Costos con Movimientos');
						$exceldata->mergeCells('A8:I8');
						$excel->getActiveSheet()->getStyle('A8')->getFont()->setBold(true);
						$excel->getActiveSheet()->getStyle('A8')->getFont()->setSize(20);
						$excel->getActiveSheet()->getStyle('A8')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

						$excel->getActiveSheet()->getCell('A9')->setValue('Elaborado el '.$Fechaactual.' a las '.$Horaactual2);
						$exceldata->mergeCells('A9:I9');
						$excel->getActiveSheet()->getStyle('A9')->getFont()->setBold(true);
						$excel->getActiveSheet()->getStyle('A9')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

						$excel->getActiveSheet()->getCell('A10')->setValue('Almacén Principal: MEDIX');
						$exceldata->mergeCells('A10:I10');
						$excel->getActiveSheet()->getStyle('A4')->getFont()->setBold(true);
						$excel->getActiveSheet()->getStyle('A10')->getFont()->setBold(true);
						$excel->getActiveSheet()->getStyle('A10')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

						$excel->getActiveSheet()->getCell('A11')->setValue('Filtrado por: '.$tInventario);
						$exceldata->mergeCells('A11:I11');
						$excel->getActiveSheet()->getStyle('A11')->getFont()->setBold(true);
						$excel->getActiveSheet()->getStyle('A11')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

						$exceldata->setCellValue('A12', 'Código');
						$exceldata->setCellValue('B12', 'Artículo');
						$exceldata->setCellValue('C12', 'Costo');
						$exceldata->mergeCells('A12:A13');
						$exceldata->mergeCells('B12:B13');
						$exceldata->mergeCells('C12:C13');
						$excel->getActiveSheet()->getStyle('A12:I12')->applyFromArray($styleArray);

						//Entradas
						$exceldata->setCellValue('D12', 'Entradas'); // Sets cell 'a1' to value 'ID
						$exceldata->mergeCells('D12:E12'); 
					    $exceldata->setCellValue('D13', 'Cantidad');
					    $exceldata->setCellValue('E13', 'Costo Total $');
					    $exceldata->setCellValue('F12', 'Salidas');
					    $exceldata->mergeCells('F12:G12');
					    $exceldata->setCellValue('F13', 'Cantidad');
					    $exceldata->setCellValue('G13', 'Costo Total $');
					    $exceldata->setCellValue('H12', 'Saldo Final');
					    $exceldata->mergeCells('H12:I12');
					    $exceldata->setCellValue('H13', 'Existencia');
					    $exceldata->setCellValue('I13', 'Costo Total $');
					    $excel->getActiveSheet()->getStyle('A13:I13')->applyFromArray($styleArray);
					    				   		
					    //Genera Data
					    $pos = 14;
					    $startline = 14;
					    $count = 1;
					    $rowCount = 0;
					   	while(odbc_fetch_row($resultrow)){
					   		$rowCount++;
					    }

					    while(odbc_fetch_row($result))
						{
	                        $costoEn =  odbc_result($result, 'COSTO')*odbc_result($result, 'ENTRADAS');

	                        //Salidas restando devoluciones
	                        $salidas = odbc_result($result, 'SALIDAS') - odbc_result($result, 'DEVOLUCIONES');
	                        
	                        //Costo de Salidas
	                        $costoSal = round(odbc_result($result, 'COSTO'))*$salidas;

	                        //Existencia Actual
	                        $exist = round(odbc_result($result, 'ENTRADAS')) - $salidas;

	                        //Costo de Existencia Actual
	                        $costoFin = $exist*round(odbc_result($result, 'COSTO'));	

							//Codigo
							$exceldata->setCellValue('A' . $pos, trim(odbc_result($result, 'AARCOD'))); 

							//Articulo
							$exceldata->setCellValue('B' . $pos, utf8_encode(trim(odbc_result($result, 'AARDES'))));

							//Costo
							$cellc = odbc_result($result, 'COSTO')!=''?odbc_result($result, 'COSTO'):'Sin Costo';
							$exceldata->setCellValue('C' . $pos, trim($cellc)); 

							//Entradas - Cantidad
							$exceldata->setCellValue('D' . $pos, round(odbc_result($result, 'ENTRADAS')));

	                        //Merge Celdas a la cantidad de NumRows
	                       /* if($prevcod==odbc_result($result, 'AARCOD') || is_null($prevcod)){
	                        	$mergepos = $pos;
	                        } else {
	                        	$exceldata->mergeCells('A'.$startline.':A'.$mergepos);
						    	$exceldata->mergeCells('B'.$startline.':B'.$mergepos);
						    	$exceldata->mergeCells('C'.$startline.':C'.$mergepos);
						    	$exceldata->mergeCells('D'.$startline.':D'.$mergepos);
						    	$exceldata->mergeCells('E'.$startline.':E'.$mergepos);
						    	$startline = $mergepos+1;
						    	$mergepos++;
	                        }

	                        //IF Especifico para hacer merge en caso de que en la ultima linea del result sea $prevcod==odbc_result($result, 'AARCOD')
	                        if($count >= $rowCount){
	                        	$exceldata->mergeCells('A'.$startline.':A'.$mergepos);
						    	$exceldata->mergeCells('B'.$startline.':B'.$mergepos);
						    	$exceldata->mergeCells('C'.$startline.':C'.$mergepos);
						    	$exceldata->mergeCells('D'.$startline.':D'.$mergepos);
						    	$exceldata->mergeCells('E'.$startline.':E'.$mergepos);
						    	$startline = $mergepos+1;
						    	$mergepos++;
	                        }*/
			
							//Costo Total
							$exceldata->setCellValue('E' . $pos, $costoEn);

							//Fecha
							$exceldata->setCellValue('F' . $pos, $salidas);

							//Cantidad
							$exceldata->setCellValue('G' . $pos, $costoSal);

							//Observaciones
							$exceldata->setCellValue('H' . $pos, $exist);

							//Costo Unitario $
							$exceldata->setCellValue('I' . $pos, $costoFin);

               				$pos++;
						}//fin while                        

						//Obtiene la cantidad de rows del excel (-13 rows del titulo) 			
						$rowCount = $excel->getActiveSheet()->getHighestRow();
						$rowCount = $rowCount-13;

						//Bordes y ajuste de anchura para la data generada
						$lin = 14;
				   		$prevpos = $lin-2;
						$highestColumn = $excel->getActiveSheet()->getHighestColumn();
						for($col = 'A'; $col <= $highestColumn; $col++)
					    {
					    	$value = $excel->getActiveSheet()->getCell($col.$prevpos)->getValue();
				   			$width = mb_strwidth($value);
				   			if($width == 0){
				   				$prevpos = $lin-1;
				   				$value = $excel->getActiveSheet()->getCell($col.$prevpos)->getValue();
				   				$width = mb_strwidth($value);
				   			}
					    	for($i = 0; $i < $rowCount; $i++){
					    		$value1 = $excel->getActiveSheet()->getCell($col.$lin)->getValue();
				   				$width1 = mb_strwidth($value1);
								if($width1 > $width){
									$width = $width1;
								}
								$excel->getActiveSheet()->getStyle($col.$lin)->applyFromArray(array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN,))));
								$excel->getActiveSheet()->getStyle($col.$lin)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
								//Texto centrado a Columnas especificas
								$excel->getActiveSheet()->getStyle('A'.$lin)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
								$excel->getActiveSheet()->getStyle('B'.$lin)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
								$excel->getActiveSheet()->getStyle($col.$lin)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
								//Background color de celdas
								if($col == 'E' || $col == 'G' || $col == 'I'){ 
									if($col == 'E'){
										$excel->getActiveSheet()->getStyle($col.$lin)->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => '3399FF'))));
									} elseif($col == 'G') {
										$excel->getActiveSheet()->getStyle($col.$lin)->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => 'FFFF99'))));
									}elseif($col == 'I') {
										$excel->getActiveSheet()->getStyle($col.$lin)->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => '00FF00'))));
									}
								}
								//Wrap Text para la Columna 'D'
								$excel->getActiveSheet()->getStyle($col.$lin)->getAlignment()->setWrapText(true);
								$lin++;
								$prevpos++;
							}
							$width = $width+2;
							// IF para colocar un width estatico a la Columna y se aplique el WrapText
							if($col == 'A' || $col == 'B'){
								$width = 45;
								$excel->getActiveSheet()->getColumnDimension($col)->setAutoSize(false);
								$excel->getActiveSheet()->getColumnDimension($col)->setWidth($width);
							} else {
								$excel->getActiveSheet()->getColumnDimension($col)->setAutoSize(false);
								$excel->getActiveSheet()->getColumnDimension($col)->setWidth($width);
							}
							$width = 0;
							$lin = 14;
							$prevpos = $lin-2;
						}
						//Ocultar Columnas
						/*$excel->getActiveSheet()->getColumnDimension('D')->setVisible(false);
						$excel->getActiveSheet()->getColumnDimension('K')->setVisible(false);
						$excel->getActiveSheet()->getColumnDimension('U')->setVisible(false);*/

						//
						//Se genera el excel
				        $writer = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
						$writer->save('php://output');
						exit();
						?>