<?php 
/*
 * jDavila
 * 23/05/2012 
 * modificado: 24/05/2012
 */
session_start();
include("../conectar.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
<title>Idasys V3</title>
<link href="../<?php echo $css; ?>" rel="stylesheet" type="text/css" media="screen" />
<script language="JavaScript" src="../javascript/javascript.js"></script>
<script language="JavaScript" src="../calendario/javascripts.js"></script>
<script language="JavaScript" src="javascript.js"></script>
<script language="JavaScript" src="../javascript/jquery.js"></script>
<link href="../shadowbox.css"  rel="stylesheet" type="text/css"/>
<script src="../shadowbox.js" type="text/javascript"/> </script>
<script language="JavaScript" type="text/JavaScript">
Shadowbox.init({overlayOpacity: "0.5"});
</script>
<style type="text/css" title="currentStyle">
			@import "../DataTables-1.9.4/media/css/<?php echo $demo_page;?>";
			@import "../DataTables-1.9.4/media/css/<?php echo $demo_table;?>";
</style>
<script type="text/javascript" language="javascript" src="../DataTables-1.9.4/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf-8">
			
			 $(document).ready(function() 
			 {
			      document.getElementById('info').style.display="";
				  $('#info').dataTable( 
				  {
					"bStateSave": true,
			        "oLanguage": 
					{
			          	"sLengthMenu": "Mostrar _MENU_ registros por Pag.",
						"sZeroRecords": "No Existen Registros",
						"sInfo": "Mostrando desde _START_ hasta _END_ de _TOTAL_ registros",
						"sInfoEmpty": "Mostrando desde 0 hasta 0 de 0 registros",
						"sInfoFiltered": "(filtrando de _MAX_ de registros)",
						"sSearch": "Buscar",
						"oPaginate": 
						{
							"sNext": "Siguiente",
							"sPrevious": "Anterior"
						}
			        }
					/*,
					"aoColumnDefs": [
						{ "bVisible": false, "aTargets": [ 0 ] }
					]*/
			      } );
			  } );
</script>

</head>
<body background="../images/fondo idaca.jpg" >
<div id="wrapper">
  <?php include("../superior.php");?>
  <div id="page">
     <?php include("../validar.php");?>

	  
 		<?php 
				
				if ($bandera==1) 
				{	
					$wsolicitud=0;
					if ($solicitudpagina==0) 	
					{
						$fecha = date('Y-m-d');
						//$fecha = '2015-02-01';
						if( date('m',strtotime('0 months',strtotime(date($fecha)))) < date('m',strtotime('-1 months',strtotime(date($fecha)))) ){
							$anio1 = "'".date('Y',strtotime('-1 year',strtotime(date($fecha))))."'";
						}else{
							$anio1 = "'".date('Y',strtotime('0 year',strtotime(date($fecha))))."'";
						}
						
						
						if( date('m',strtotime('0 months',strtotime(date($fecha)))) < date('m',strtotime('-2 months',strtotime(date($fecha)))) ){
							$anio2 = "'".date('Y',strtotime('-1 year',strtotime(date($fecha))))."'";
						}else{
							$anio2 = "'".date('Y',strtotime('0 year',strtotime(date($fecha))))."'";
						}
						
						if( date('m',strtotime('0 months',strtotime(date($fecha)))) < date('m',strtotime('-3 months',strtotime(date($fecha)))) ){
							$anio3 = "'".date('Y',strtotime('-1 year',strtotime(date($fecha))))."'";
						}else{
							$anio3 = "'".date('Y',strtotime('0 year',strtotime(date($fecha))))."'";
						}
							$sql= "
								SELECT T1.ACICOD, T1.AALCOD, T1.AARCOD, T3.AARDES, T1.AUMCOD, T4.AUMDES, T1.ALTCOD, T5.AARSTM, 
									sum(case when T1.ASLFEF=(SELECT max(T2.ASLFEF) FROM iv40fp T2 where T2.ACICOD=T1.ACICOD AND T2.AALCOD=T1.AALCOD AND T2.AARCOD=T1.AARCOD AND T2.ALTCOD=T1.ALTCOD AND T2.ASLFEF<'01.10.1990' ) then T1.aslsaf else 0 end) as SALANT, 
									sum(case when T1.ASLFEF < '".$fecha."' then T1.ASLENT else 0 end) as ASLENT, 
									sum(case when T1.ASLFEF < '".$fecha."' then T1.ASLSAL else 0 end) as ASLSAL,
									(SELECT SUM(T6.ATRCAN) AS ATRCAN FROM IV16FP T6 INNER JOIN IV15FP T7 ON ( T6.ACICOD=T7.ACICOD AND T6.AALCOD=T7.AALCOD AND T6.ATRCOD=T7.ATRCOD AND T6.ATRNUM=T7.ATRNUM ) INNER JOIN IV12FP T8 ON ( T7.ACICOD=T8.ACICOD AND T7.ATRCOD=T8.ATRCOD AND T8.ATRSIG='-') WHERE T7.ACICOD=T1.ACICOD AND T7.AALCOD=T1.AALCOD AND month(T7.ATRFEC)='".date("m")."' AND YEAR(T7.ATRFEC)= '".date('Y')."' AND T6.ATRART=T1.AARCOD GROUP BY T6.ATRART ORDER BY T6.ATRART)AS CAN1 ,
									(SELECT SUM(T9.ATRCAN) AS ATRCAN FROM IV16FP T9 INNER JOIN IV15FP T10 ON ( T9.ACICOD=T10.ACICOD AND T9.AALCOD=T10.AALCOD AND T9.ATRCOD=T10.ATRCOD AND T9.ATRNUM=T10.ATRNUM ) INNER JOIN IV12FP T11 ON ( T10.ACICOD=T11.ACICOD AND T10.ATRCOD=T11.ATRCOD AND T11.ATRSIG='-') WHERE T10.ACICOD=T1.ACICOD AND T10.AALCOD=T1.AALCOD AND month(T10.ATRFEC)='".date("m",strtotime("-1 month", strtotime($fecha)))."' AND YEAR(T10.ATRFEC)= ".$anio1." AND T9.ATRART=T1.AARCOD GROUP BY T9.ATRART ORDER BY T9.ATRART)AS CAN2 ,
									(SELECT SUM(T12.ATRCAN) AS ATRCAN FROM IV16FP T12 INNER JOIN IV15FP T13 ON ( T12.ACICOD=T13.ACICOD AND T12.AALCOD=T13.AALCOD AND T12.ATRCOD=T13.ATRCOD AND T12.ATRNUM=T13.ATRNUM ) INNER JOIN IV12FP T14 ON ( T13.ACICOD=T14.ACICOD AND T13.ATRCOD=T14.ATRCOD AND T14.ATRSIG='-') WHERE T13.ACICOD=T1.ACICOD AND T13.AALCOD=T1.AALCOD AND month(T13.ATRFEC)='".date("m",strtotime("-2 month", strtotime($fecha)))."' AND YEAR(T13.ATRFEC)= ".$anio2." AND T12.ATRART=T1.AARCOD GROUP BY T12.ATRART ORDER BY T12.ATRART  )AS CAN3, 
									(SELECT SUM(T15.ATRCAN) AS ATRCAN FROM IV16FP T15 INNER JOIN IV15FP T16 ON ( T15.ACICOD=T16.ACICOD AND T15.AALCOD=T16.AALCOD AND T15.ATRCOD=T16.ATRCOD AND T15.ATRNUM=T16.ATRNUM ) INNER JOIN IV12FP T17 ON ( T16.ACICOD=T17.ACICOD AND T16.ATRCOD=T17.ATRCOD AND T17.ATRSIG='-') WHERE T16.ACICOD=T1.ACICOD AND T16.AALCOD=T1.AALCOD AND month(T16.ATRFEC)='".date("m",strtotime("-3 month", strtotime($fecha)))."' AND YEAR(T16.ATRFEC)= ".$anio3." AND T15.ATRART=T1.AARCOD GROUP BY T15.ATRART ORDER BY T15.ATRART )AS CAN4 
								FROM iv40fp T1 
									INNER JOIN IV05FP T3 ON(T1.ACICOD=T3.ACICOD AND T1.AARCOD=T3.AARCOD AND T3.AARSTS='01') 
									INNER JOIN IV13FP T4 ON(T1.ACICOD=T4.ACICOD AND T1.AUMCOD=T4.AUMCOD) 
									left join iv39fp T5 ON(T1.ACICOD=T5.ACICOD AND T1.AARCOD=T5.AARCOD AND T1.AALCOD=T5.AALCOD) 
								WHERE T1.ACICOD='".$Compania."' AND T1.AALCOD='".$aalcod."'
								GROUP BY T1.ACICOD, T1.AALCOD, T1.AARCOD, T3.AARDES, T1.ALTCOD , T4.AUMDES,T1.AUMCOD, T5.aarstm 
								ORDER BY T3.AARDES 
							";
							
								$resultt=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111"));
								//echo $sql;
								$z=0;
								$tothon=0;
								$totest=0;
								$lin=1;
								$limitep=$_SESSION['solicitudlineasporpaginat'];
								$pag=1;
								$primero='S';
								
								while(odbc_fetch_row($resultt))
								{
									$jml = odbc_num_fields($resultt);
									$row[$z]["pagina"] =  $pag;
									for($i=1;$i<=$jml;$i++)
									{	
										$row[$z][odbc_field_name($resultt,$i)] =  odbc_result($resultt,$i);
									}
									$z++;
									if ($lin>=$limitep) 
									{
										$limitep+=$_SESSION['solicitudlineasporpaginat'];
										$pag++;
									}
									$lin++;
								}
								$totsol=($lin-1);
								$_SESSION['totalsolicitudes']=$totsol;
								$_SESSION['solicitudarreglo']=$row;
								$solicitudpagina=1;
								$_SESSION['solicitudpaginas']=$pag;
					}//fin de solicitudpagina
					/*se muestra la cantidad de elementos segun la solicitada en $solicitudpagina*/
					$paginat=$_SESSION['solicitudarreglo'];
				}
			?>
        <div id="content3" >   
        	<table width="100%"   border="0">
              <tr>
                <td width="65%" scope="col" colspan="2"><h1  class="title">Resultado del c�lculo obtenido del Stock M�nimo en base al consumo del los �ltimos 3 Meses </h1><hr /></td>
                <td width="13%" scope="col"><div align="left">
                  <table width="100%"  border="0">
                    <tr>
                      <th width="30%" scope="col" >
                      <div class="flechas">
                      	<!---
                            <a href="exportarapdf.php?&desde=<?php echo $desde; ?>&hasta=<?php echo $hasta; ?>&aalcod=<?php echo $aalcod; ?>&stm=<?php echo $stm; ?>" target="_blank">
                                <img src="../images/pdf.jpg" alt="" width="30" height="30" border="0" class="flechas" title="Exportar a PDF"/>
                            </a>
                            &nbsp;&nbsp;
                        -->
                        <a href="exportaraexcelesultstockminimo.php?&aalcod=<?php echo $aalcod; ?>&stm=<?php echo $stm; ?>" target="_blank">
                        	<img src="../images/excel.jpg" alt="" width="30" height="30" title="Exportar a Excel"/>
                        </a>
                      </div></th>
                    </tr>
                  </table>
                </div></td>
              </tr>
              <tr>
              	<td colspan="3">
                	<form name="form" id="form" method="post">
                    <input type="hidden" name="bandera" id="bandera" value="<?php echo $bandera;?>" />
                	<table>
                    	<tr>
                        	<td>&nbsp;Almac�n:</td>
                            <td>&nbsp;<?php //echo $aalcod; ?>
                            	<select name="aalcod" id="aalcod">
									<?php 
                                    //$sql="SELECT AALCOD, AALDES FROM iv07fp where acicod ='$Compania' order by AALDES ";
									if(accesotodasunisol_alma('4')=='S')
									{
										$sql="SELECT AALCOD, AALDES FROM iv07fp where acicod ='$Compania'  order by AALDES ";//and AALCOD <> '0017'
									}
									else
									{
										$sql = "SELECT T1.AALCOD, T2.AALDES FROM iv44fp T1, IV07FP T2 WHERE T1.acicod ='$Compania' AND T1.ACICOD=T2.ACICOD AND T1.auscod='$Usuario' AND T1.AALCOD= T2.AALCOD order by t2.AALDES";
									}
                                    $result1=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111")); 
									$select = '';
                                    while(odbc_fetch_row($result1)){
                                        $cod=trim(odbc_result($result1,1));
                                        $des=trim(odbc_result($result1,2));
										if(!empty($aalcod))
										{
											if($cod == $aalcod){
												$select = ' selected="selected" ';
											}
											else{
												$select = '';
											}
										}											
                                    ?>
                                        <option value= "<?php echo $cod; ?>" <?php echo $select?> ><?php echo $des."(".$cod.")"; ?></option>
                                    <?php } ?>
                                </select>
                            </td>
                            <td>&nbsp;</td>
                            <td>Solo Stock<br /> M�nimo</td>
                            <td>
                            	<?php if($stm == 1 ){
										$check = 'checked="checked"';
									  }
									  else{
										  $check = '';
									  }
								?>
                            	<input name="stm" id="stm" type="checkbox" value="" <?php echo $check; ?>/>
                            </td>
                            <td><a href="javascript:busquedaresultadostockminimo();"><img src="../images/precesarconsulta.png" width="50" height="50" alt="Realizar Consulta" style="border:none;"></a></td>
                        </tr>
                    </table>
                    </form>
                    <hr />
                </td>
                
              </tr>
              <!--<tr>
                <td colspan="2" scope="col"><h5 align="left"><?php echo $paginat[0]["ALCSED"];?>wwww</h5></td>
                <td scope="col"> Pagina: <?php if (!$solicitudpagina) {$paginaarriba=1;} else {$paginaarriba=$solicitudpagina;}; echo $paginaarriba."/".$_SESSION['solicitudpaginas']; ?></td>
              </tr>-->
            </table>   
		<div id="container">
            <div id="demo">
                <table width="100%" id="info" style="display:none">
                  <thead>
                    <tr>
	                    <th width="5%"  scope="col">Cod. Art&iacute;culo</th>
                        <th width="30%"  scope="col">Descripci&oacute;n</th>
                        <th width="5%" scope="col">Stock M&iacute;nimo</th>
                        <th width="5%" scope="col">Ubicaci&oacute;n</th>
                        <th width="5%" scope="col">Unidad de Medida</th>
                        <th width="5%" scope="col"><?php echo mesescrito( date("m",strtotime("-3 month", strtotime(date("d-m-Y")))) ); ?></th>
                        <th width="5%" scope="col"><?php echo mesescrito( date("m",strtotime("-2 month", strtotime(date("d-m-Y")))) ); ?></th>
                        <th width="5%" scope="col"><?php echo mesescrito( date("m",strtotime("-1 month", strtotime(date("d-m-Y")))) ); ?></th>
                        <th width="5%" scope="col"><?php echo mesescrito( date("m") ); ?></th>
                        <th width="5%" scope="col">Saldo Final</th>
                        <th width="5%" scope="col">Opciones</th>
                    </tr>
                  </thead>
    			  <tbody>
        						<?php 
									$show = false;
									//print_r($paginat);
									$pagact=$solicitudpagina;
									for($g=0; $g < (count($paginat)); $g++)
									{
                                ?>
                                <?php 
									  $salFinal = $paginat[$g]["SALANT"]+$paginat[$g]["ASLENT"]+$paginat[$g]["ASLSAL"];
									  
									  /*validar si piden solo stm */
									  if($stm==1){//si 
										  if($salFinal<=$paginat[$g]["AARSTM"])	{$show = true;}
										  else									{$show = false;}
									  }else if($stm==2){
										 $show = true; 
									  }
									  
									  if($show){
								?>
                                        <tr>
                                        	<!--<td><?php if($salFinal<=$paginat[$g]["AARSTM"]){echo "M&Iacute;NIMO";}else{echo "";} ?></td>-->
                                        	<td scope="col"><div><?php echo "<strong>".$paginat[$g]["AARCOD"]."</strong>";?></div></td>
                                            <td scope="col"><div><?php echo $paginat[$g]["AARDES"];?></div></td>                
                                            <td scope="col">
                                            	<div align="center">
                                                    <a rel="shadowbox;width=400;height=200" href="editarfstm.php?&aarcod=<?php echo trim($paginat[$g]["AARCOD"]);?>&aalcod=<?php echo $aalcod; ?>" title="Editar" >
                                                        <?php echo number_format($paginat[$g]["AARSTM"],2,",",".");?>
                                                    </a>
                                                </div>
                                            </td>
                                            <td scope="col" id="opciones">
                                            	<div><strong>
													<?php 
                                                        $list = list_ubiart($cid, $Compania, $paginat[$g]["AALCOD"], $paginat[$g]["AARCOD"],2);
                                                        echo '<a rel="shadowbox;width=400;height=300" href="editarfubicacion.php?&aarcod='.trim($paginat[$g]["AARCOD"]).'&aalcod='.$aalcod.'" title="Editar" >';
                                                        
                                                        if(trim($list)!=''){	echo $list;	}
                                                        else {	echo ' - Sin Ubicaci�n';	}
                                                        echo '</a>';
                                                     ?>
                                                </strong></div>
                                            </td>
                                            <td scope="col"><div align="center"><?php echo $paginat[$g]["AUMDES"];?></div></td>
                                            
                                            <td scope="col"><div align="right"><?php echo number_format($paginat[$g]["CAN4"],2,",",".");?></div></td>
                                            <td scope="col"><div align="right"><?php echo number_format($paginat[$g]["CAN3"],2,",",".");?></div></td>
                                            <td scope="col"><div align="right"><?php echo number_format($paginat[$g]["CAN2"],2,",",".");?></div></td>
                                            <td scope="col"><div align="right"><?php echo number_format($paginat[$g]["CAN1"],2,",",".");?></div></td>
                                            
                                            <td scope="col"><div align="right"><?php echo number_format(($salFinal),2,",",".");?></div>  </td>
                                            <td scope="col">
		                                            &nbsp;
                                            		<!--<a rel="shadowbox;width=650;height=345" title="Detalle" href="detalle.php?&id=<?php //echo trim($paginat[$g]["AARCOD"]); ?>&fdesde=<?php// echo $desde; ?>&fhasta=<?php //echo $hasta; ?>">Detalle</a>-->
                                            		<!--<a href="javascript:verdetalle('<?php //echo trim($paginat[$g]["AARCOD"]); ?>','<?php //echo $desde; ?>','<?php //echo $hasta; ?>', '<?php //echo $aalcod; ?>', <?php //echo $paginat[$g]["SALANT"]; ?>,'<?php //echo trim($paginat[$g]["AUMDES"]);?>' )"><img src="../images/ver.png" title="Ver Detalle" width="31" height="28" border="0"></a>		-->
                                            </td>
                                        </tr>
                            	<?php } }?>      
                  </tbody>
                </table>
</div>
		  </div>
	  </div>
	</div>
		<!-- end #content -->
		<!-- end #sidebar -->
		<div style="clear: both;">&nbsp;</div>
	</div>
	<!-- end #page -->

	<div id="footer">
		<div id="opcionesmasusadas"><?php include("../opcionesmasusadasphp.php"); ?></div>
	</div>
 
	<!-- end #footer -->
</body>
</html>
