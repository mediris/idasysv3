<?php 
/*
 * Luis Ramos (lRamos)
 * 10/11/2016  
 * Johan Davila
 * 28/03/2017
 */
session_start();
include("../conectar.php");
$tinva = $_GET['tinvan'];
$aalcod = $_GET['aalcod'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
<title>Idasys V3</title>
<link href="../<?php echo $css; ?>" rel="stylesheet" type="text/css" media="screen" />
<script language="JavaScript" src="../javascript/javascript.js"></script>
<script language="JavaScript" src="../calendario/javascripts.js"></script>
<script language="JavaScript" src="javascript.js"></script>
<script language="JavaScript" src="../javascript/jquery.js"></script>
<link href="../shadowbox.css"  rel="stylesheet" type="text/css"/>
<script src="../shadowbox.js" type="text/javascript"/> </script>
<script language="JavaScript" type="text/JavaScript">
Shadowbox.init({overlayOpacity: "0.5"});
</script>
<style type="text/css" title="currentStyle">
			@import "../DataTables-1.9.4/media/css/<?php echo $demo_page;?>";
			@import "../DataTables-1.9.4/media/css/<?php echo $demo_table;?>";
</style>
<script type="text/javascript" language="javascript" src="../DataTables-1.9.4/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf-8">
			
			 $(document).ready(function() 
			 {
			      document.getElementById('info').style.display="";
				  $('#info').dataTable( 
				  {
					"bStateSave": true,
			        "oLanguage": 
					{
			          	"sLengthMenu": "Mostrar _MENU_ registros por Pag.",
						"sZeroRecords": "No Existen Registros",
						"sInfo": "Mostrando desde _START_ hasta _END_ de _TOTAL_ registros",
						"sInfoEmpty": "Mostrando desde 0 hasta 0 de 0 registros",
						"sInfoFiltered": "(filtrando de _MAX_ de registros)",
						"sSearch": "Buscar",
						"oPaginate": 
						{
							"sNext": "Siguiente",
							"sPrevious": "Anterior"
						}
			        }
					/*,
					"aoColumnDefs": [
						{ "bVisible": false, "aTargets": [ 0 ] }
					]*/
			      } );
			  } );
</script>
</head>
<body background="../images/fondo idaca.jpg" >
<div id="wrapper">
  <?php include("../superior.php");?>
  <div id="page">
     <?php include("../validar.php");?>

	  
 		<?php 
				
				if ($bandera==1) 
				{	
					$tipoInve ='';
					if(!empty($tinva)){
						foreach($tinva as $key => $value){
							$tipoInve .= "'".$value."',";
							/*
							echo '<script>
									$("#tinvan").prop("checked", "checked");
								  </script>';
								  */
						}
						$tipoInve = substr($tipoInve,0, (strripos($tipoInve,",")));
					}

					$sql="SELECT T1.ATRART, T1.ATRNUM, T1.ATRCOD, T1.ATRCUT, (SUM(T1.ATRCAN)) AS ATRCAN, T2.ATRDES, T3.ATRFEC, T3.ATROBS, T5.AARCOD, T5.AARDES, T7.AMCDES, T8.ATIDES
						FROM IV16FP T1
							INNER JOIN IV12FP T2 ON (T1.ACICOD=T2.ACICOD AND T1.ATRCOD=T2.ATRCOD)
							INNER JOIN IV15FP T3 ON (T1.ACICOD=T3.ACICOD AND T1.ATRCOD=T3.ATRCOD AND T1.ATRNUM=T3.ATRNUM AND T3.ATRSTS='02')
							LEFT JOIN IV35FP T4 ON (T1.ACICOD=T4.ACICOD AND T1.AALCOD=T4.AALCOD AND T1.ATRCOD=T4.ATRCOD AND T1.ATRNUM=T4.ATRNUM)
							INNER JOIN IV05FP T5 ON (T1.ACICOD=T5.ACICOD AND T1.ATRART=T5.AARCOD)
							INNER JOIN IV06FP T6 ON (T1.ACICOD=T6.ACICOD AND T5.AARCOD=T6.AARCOD)
							INNER JOIN IV04FP T7 ON (T1.ACICOD=T7.ACICOD AND T6.AMCCOD=T7.AMCCOD)
							INNER JOIN IV01FP T8 ON (T5.ACICOD=T8.ACICOD AND T5.ATICOD=T8.ATICOD)
						WHERE T1.ACICOD='$Compania' AND T1.AALCOD IN ('$aalcod') AND T1.ATRCOD='0103' AND T3.ATRFEC BETWEEN '$desde' AND '$hasta'";
					if(!empty($tipoInve)){
						$sql.="  AND T5.ATICOD IN (".$tipoInve.")";
					}
					$sql.="  
						GROUP BY T1.ATRART, T1.ATRNUM, T1.ATRCOD, T1.ATRCUT, T2.ATRDES, T3.ATRFEC, T3.ATROBS, T5.AARCOD, T5.AARDES, T7.AMCDES, T8.ATIDES
						ORDER BY T5.AARDES, T1.ATRNUM, T1.ATRART";

					$result=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111"));
				}
			?>
        <div id="content3" >   
        	<table width="100%"   border="0">
              <tr>
                <td width="65%" scope="col" colspan="2"><h1  class="title">Reporte de Inventario con Costos - IDACA</h1>
                  <hr /></td>
                <td width="13%" scope="col"><div align="left">
                  <table width="100%"  border="0">
                    <tr>
                      <th width="30%" scope="col" >
                      <div class="flechas">
                      	<a href="exportaraexcel_idaca_costos.php?&desde=<?php echo $desde; ?>&hasta=<?php echo $hasta; ?>&aalcod=<?php echo $aalcod; ?>&tinvan=<?php echo $url222 = urlencode( serialize($tinva) );?>" target="_blank">
                        	<img src="../images/excel.jpg" alt="" width="30" height="30" title="Exportar a Excel"/>
                        </a>
                      </div></th>
                    </tr>
                  </table>
                </div></td>
              </tr>
              <tr>
              	<td colspan="3">
                	<form name="form" id="form" method="post" on>
                    <input type="hidden" name="bandera" id="bandera" value="<?php echo $bandera;?>" />
                	<table>
                    	<tr>
                        	<td>&nbsp;Desde:</td>
                            <td>&nbsp;<?php echo  escribe_formulario_fecha_vacio('desde','form',$desde); ?></td>
                            <td>&nbsp;</td>
                            <td>&nbsp;Hasta:</td>
                            <td>&nbsp;<?php echo  escribe_formulario_fecha_vacio('hasta','form',$hasta); ?></td>
                            <td rowspan="3"><a href="javascript:busqueda5();"><img src="../images/precesarconsulta.png" width="50" height="50" alt="Realizar Consulta" style="border:none;"></a></td>
                        </tr>
                        <tr>
                        	<td>&nbsp;Almac&eacute;n:</td>
                            <td>&nbsp;<?php //echo $aalcod; ?>
                            	<select name="aalcod" id="aalcod" onchange="cargarSelectTpInven(<?php ?>)" >
									<?php 
                                    //$sql="SELECT AALCOD, AALDES FROM iv07fp where acicod ='$Compania' order by AALDES ";
									if(accesotodasunisol_alma('4')=='S')
									{
										$sql="SELECT AALCOD, AALDES FROM iv07fp where acicod ='$Compania'  order by AALDES ";//and AALCOD <> '0017'
									}
									else
									{
										$sql = "SELECT T1.AALCOD, T2.AALDES FROM iv44fp T1, IV07FP T2 WHERE T1.acicod ='$Compania' AND T1.ACICOD=T2.ACICOD AND T1.auscod='$Usuario' AND T1.AALCOD= T2.AALCOD order by t2.AALDES";
									}
                                    $result1=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111")); 
									$select = '';
                                    while(odbc_fetch_row($result1)){
                                        $cod=trim(odbc_result($result1,1));
                                        $des=trim(odbc_result($result1,2));
										if(!empty($aalcod))
										{
											if($cod == $aalcod){
												$select = ' selected="selected" ';
											}
											else{
												$select = '';
											}
										}											
                                    ?>
                                        <option value= "<?php echo $cod; ?>" <?php echo $select?> ><?php echo $des."(".$cod.")"; ?></option>
                                    <?php } ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                        	<td>Tipo de Inventario</td>
                            <td colspan="4">
                            	<div id="table_tpinven">
                                </div>
                                <script>cargarSelectTpInven();</script>
                            </td>
                        </tr>
                    </table>
                    </form>
                    <hr />
                </td>
              </tr>
            </table>   
		<div id="container">
            <div id="demo">
                <table width="100%" id="info"  border="0" style="display:none;">
                  <thead>
                    <tr>
                    	<th scope="col" rowspan="2" style="background-color:rgb(204,204,204)">C&oacute;digo</th>
                   		<th scope="col" rowspan="2" style="background-color:rgb(204,204,204)">Art&iacute;culo</th>
                        <th scope="col" rowspan="2" style="background-color:rgb(204,204,204)">Ubicaci&oacute;n</th>
                    	<th scope="col" colspan="5" style="background-color:rgb(204,204,204)">Entradas</th>
                    </tr>
                    <tr>
                        <th scope="col" style="background-color:rgb(204,204,204)"><strong>Nro. de Entrada</strong></th>
                        <th scope="col" style="background-color:rgb(204,204,204)"><strong>Fecha</strong></th>
                        <th scope="col" style="background-color:rgb(204,204,204)"><strong>Cantidad</strong></th>
                        <th scope="col" style="background-color:rgb(204,204,204)"><strong>Costo Unitario $</strong></th>
                        <th scope="col" style="background-color:rgb(204,204,204)"><strong>Costo Total $</strong></th>
                    </tr>
                 </thead> 
                 <tbody>
        						<?php
        						if($bandera==1){
									while(odbc_fetch_row($result))
									{	
											?>
                                            <tr >
	                                                <td style="text-align:left;vertical-align:middle;"><?php echo trim(odbc_result($result, 'AARCOD')); ?></td>
                                                    <td style="text-align:left;vertical-align:middle;"><?php echo trim(odbc_result($result, 'AARDES')); ?></td>
                                                    <td style="text-align:left;vertical-align:middle;">
														<div>
															<?php 
                                                                $list = list_ubiart($cid, $Compania, $tipoAlm, trim(odbc_result($result, 'AARCOD')),2);
                                                                
                                                                if(trim($list)!=''){
                                                                    echo $list; } 
                                                                else { 
                                                                    echo ' - Sin Ubicaci&oacute;n';
                                                                }
                                                             ?>
                                                        </div>
                                                    </td>
                                                <?php 
													
													if(odbc_result($result, 'ATRCOD')=='0210'){
														?>
                                                        <td style="text-align:center;vertical-align:middle;"><?php echo odbc_result($result, 'ADSNRO')!=''?add_ceros(odbc_result($result, 'ADSNRO'),6):'&nbsp;'; echo "<strong>(D)</strong>";?></td>
                                                        <?php
													}else{
														?>
                                                        <td style="text-align:center;vertical-align:middle;"><?php echo odbc_result($result, 'ATRNUM')!=''?add_ceros(odbc_result($result, 'ATRNUM'),6):'&nbsp;'; echo '(E)';?></td>
                                                        <?php
													}		
									?>
                                                    <td style="text-align:center;vertical-align:middle;"><?php echo odbc_result($result, 'ATRFEC')!=''?formatDate(odbc_result($result, 'ATRFEC'),'aaaa-mm-dd','dd/mm/aaaa' ):'&nbsp;';?></td>
                                                    <td style="text-align:center;vertical-align:middle;"><?php echo odbc_result($result, 'ATRCAN')!=''?number_format(odbc_result($result, 'ATRCAN'),2,',','.'):'&nbsp;';?></td>
                                                    <td style="text-align:center;vertical-align:text-top;">&nbsp;<?php echo odbc_result($result, 'ATRCUT')!=''?number_format(odbc_result($result, 'ATRCUT'),2,',',''):'&nbsp;';?></td>
                                                    <td style="text-align:center;vertical-align:text-top;">&nbsp;<?php echo odbc_result($result, 'ATRCUT')!=''?number_format((odbc_result($result, 'ATRCUT')*odbc_result($result, 'ATRCAN')),2,',',''):'&nbsp;';?></td>
                                            </tr>
								<?php 
									}//fin while
								}//fin if
							    ?>
                       </tbody>                                         
                </table>
</div>

		  </div>
	  </div>
	</div>
		<!-- end #content -->
		<!-- end #sidebar -->
		<div style="clear: both;">&nbsp;</div>
	</div>
	<!-- end #page -->

	<div id="footer">
		<div id="opcionesmasusadas"><?php include("../opcionesmasusadasphp.php"); ?></div>
	</div>
 	<?php 
		echo "<script>
				var list = [];
				var pos = 1;
				var lista = \"".$tipoInve."\";
				$('input[name=\"tinvan[]\"]').each(function() { 
					//alert($(this).val() );
					if( lista.indexOf($(this).val()) != -1 ){
						//alert('paso '+$(this).val()) ;
						 $(this).attr('checked', true); 
					}
					pos++;
				});
			  </script>";
	?>
	<!-- end #footer -->
</body>
</html>
