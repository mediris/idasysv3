<?php session_start();
include("../conectar.php");
$arqnro = trim($_GET["num"]);
$tinva = unserialize( urldecode( $_GET['tinvan'] ) ) ;

//header("Pragma: ");
header("Pragma: no-cache");
header('Cache-control: ');
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Content-type: application/vnd.ms-excel");
//header("Content-type: application/octet-stream");
header("Content-disposition: attachment; filename=Reporte_Consumo_Detalle_".formatDate($Fechaactual,'dd.mm.aaa','aaaa_mm_dd').".xls");

?>
 

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Reporte Consumo Detalle</title>
</head>
<style>

h1, h2, h3, h4, h5 {
	margin: 0;
	padding: 0;
	font-weight: normal;
	color: #32639A;
}

h1 {
	font-size: 2em;
}

h2 {
	font-size: 2.4em;	
}

h3 {
	font-size: 1.6em;
	font-style: italic;
}
h4 {
	font-size: 1.6em;
	font-style: italic;
	color: #FFF;
}
h5 {
	font-size: 1.0em;
	font-style: italic;
	color: #666;
}

#background-image
{
	font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
	font-size: 10px;
	margin: 0px;
	width: 100%;
	text-align: left;
	border-collapse: collapse;
}
#background-image th
{
	padding: 12px;
	font-weight: normal;
	font-size: 12px;
	color: #339;
	border-bottom-style: solid;
	border-left-style: none;
	text-align: center;
}
#background-image td
{
	color: #669;
	border-top: 1px solid #fff;
	padding-right: 4px;
	padding-left: 4px;
}
#background-image tfoot td
{
	font-size: 9px;
}
#background-image tbody 
{

	background-repeat: no-repeat;
	background-position: left top;
}
#background-image tbody td
{
	background-image: url(images/backn.png);
}
* html #background-image tbody td
{
	/* 
	   ----------------------------
		PUT THIS ON IE6 ONLY STYLE 
		AS THE RULE INVALIDATES
		YOUR STYLESHEET
	   ----------------------------
	*/
	filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='images/backn.png',sizingMethod='crop');
	background: none;
}	
</style>
<body>
<?php 
				
				
		 		$wsolicitud=0;
				if ($solicitudpagina==0) 	
				{
						
						$z=0;
						$tothon=0;
						$totest=0;
						$lin=1;
						$limitep=$_SESSION['solicitudlineasporpaginat'];
						$pag=1;
						$primero='S';
						
						$tipoInve ='';
						if(!empty($tinva)){
							foreach($tinva as $key => $value){
								$tipoInve .= "'".$value."',";
							}
							$tipoInve = substr($tipoInve,0, (strripos($tipoInve,",")));
						}
						
						$sql="SELECT
							(
								SELECT count(*)                                                   
								FROM IDASYSW.IS21FP A212                                          
									INNER JOIN  \"SF.APDATA\".RH009L3 A092 ON UBIC09=APRUBI                
									INNER JOIN (IDASYSW.IV46FP A462 INNER JOIN (IDASYSW.IV35FP A352   
									INNER JOIN IDASYSW.IV36FP A362 ON A362.ACICOD=A352.ACICOD AND     
										A352.ADSNRO=A362.ADSNRO)                                          
										 ON A462.ACICOD=A352.ACICOD AND A352.ATSCOD=13 AND A352.ADSSTS=03 
										AND A352.ADSNRO=A462.ADSNRO)                                      
										ON A212.ACICOD=A462.ACICOD AND A462.AAPVLA=A212.APRCOD            
								WHERE A212.ACICOD=A21.ACICOD and A212.APRCOD=A21.APRCOD AND A352.ATRFEC BETWEEN '".$desde."' AND '".$hasta."'            
							) AS CANTIDAD,
							
							A21.ACICOD, A21.APRCOD, A21.APRDES, A21.APRMOD,         
							A21.APRUBI,A09.DESM09, A35.ADSNRO, A35.ATRDES, A35.ATRFEC,     
							A36.AARCOD, A05.AARDES, A36.ATRCAN, A36.ATRUMB, A13.AUMDES 
							FROM IDASYSW.IS21FP A21 
								INNER JOIN  \"SF.APDATA\".RH009L3 A09 ON UBIC09=APRUBI 
								
								INNER JOIN (IDASYSW.IV46FP A46 INNER JOIN (IDASYSW.IV35FP A35 INNER JOIN IDASYSW.IV36FP A36 ON A36.ACICOD=A35.ACICOD AND A35.ADSNRO=A36.ADSNRO) 
									ON A46.ACICOD=A35.ACICOD AND A35.ATSCOD=13 AND A35.ADSSTS=03 AND A35.ADSNRO=A46.ADSNRO) 
								ON A21.ACICOD=A46.ACICOD AND A46.AAPVLA=A21.APRCOD 
							
								INNER JOIN IDASYSW.IV05FP A05 ON (A05.ACICOD=A21.ACICOD AND A05.AARCOD=A36.AARCOD) 
								INNER JOIN IDASYSW.IV06FP A06 ON (A05.ACICOD=A06.ACICOD AND A05.AARCOD=A06.AARCOD) 
								INNER JOIN IDASYSW.IV13FP A13 ON (A05.ACICOD=A13.ACICOD AND A06.AARUMB=A13.AUMCOD) 
							WHERE A21.ACICOD='".$Compania."' AND 
								  A35.ATRFEC BETWEEN '".$desde."' AND '".$hasta."'
							ORDER BY A21.APRDES, A21.APRCOD, A35.ATRFEC, A05.AARDES ";
						
						//echo $sql."<br/><br/>";
						$resultt=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111"));
						
							while(odbc_fetch_row($resultt))
							{
								$jml = odbc_num_fields($resultt);
								$row[$z]["pagina"] =  $pag;
								for($i=1;$i<=$jml;$i++)
								{	
									$row[$z][odbc_field_name($resultt,$i)] =  odbc_result($resultt,$i);
								}
								$z++;
								if ($lin>=$limitep) 
								{
									$limitep+=$_SESSION['solicitudlineasporpaginat'];
									$pag++;
								}
								$lin++;
							}

						$totsol=($lin-1);
						$_SESSION['totalsolicitudes']=$totsol;
						$_SESSION['solicitudarreglo']=$row;
						$solicitudpagina=1;
						$_SESSION['solicitudpaginas']=$pag;
					}//fin de solicitudpagina
					/*se muestra la cantidad de elementos segun la solicitada en $solicitudpagina*/
					$paginat=$_SESSION['solicitudarreglo'];
			?>      
<table width="100%" border="0">
<tr>
<td height="89"><h1>
<?php if($Compania=='14'){?>
	<img src="http://<?php echo $Direccionip; ?>/idasysv3/images/logoidacadef2005.png" width="280" height="68" />
<?php }else if($Compania=='40'){?>
	<img src="http://<?php echo $Direccionip; ?>/idasysv3/images/MEDITRON_logo_rif.png" width="300" />
<?php }else{ ?>
	<img src="http://<?php echo $Direccionip; ?>/idasysv3/images/logomeditronnuevo.png" width="280" height="68" />
<?php } ?>
   </h1>
  <h5>RIF:  <?php echo $Companiarif; ?></h5></td>
</tr>
<tr>
<td>

<table width="100%" id="background-image" >
	
  <thead>
  	<tr>
        <th colspan="9" scope="col"><h1>Consumo de Tonner MEDITRON</h1></th>
    </tr>
    <tr>
        <th colspan="9" scope="col"><strong>Desde:</strong> <?php echo $desde;?> ; <strong>Hasta:</strong>  <?php echo $hasta;?></th>
    </tr>
    <tr>
        <th colspan="9" scope="col">Elaborado el <?php echo $Fechaactual; ?> a las <?php echo $Horaactual2; ?></th>
    </tr>
    <tr style="border-bottom:solid;">
        <th scope="col" colspan="2" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)"><strong>Información de Impresora</strong></th>
        <th scope="col" colspan="3" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)"><strong>Información de Dotación</strong></th>
        <th scope="col" colspan="4" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)"><strong>Información de Detalle</strong></th>

    </tr>
    <tr>
        <th scope="col" style="border-width:thin;border-bottom:solid;border-left:solid;solid;background-color:rgb(204,204,204);"><strong>Impresora</strong></th>
        <th scope="col" style="border-width:thin;border-bottom:solid;border-left:solid;solid;background-color:rgb(204,204,204);"><strong>Gerencia Encargada</strong></th>
        
        <th scope="col" style="border-width:thin;border-bottom:solid;border-left:solid;solid;background-color:rgb(204,204,204);"><strong>Nro. Dotación Salida</strong></th>
        <th scope="col" style="border-width:thin;border-bottom:solid;border-left:solid;solid;background-color:rgb(204,204,204);"><strong>Persona Solicitante</strong></th>
        <th scope="col" style="border-width:thin;border-bottom:solid;border-left:solid;solid;background-color:rgb(204,204,204);"><strong>Fecha de Solicitud</strong></th>
        
        <th scope="col" style="border-width:thin;border-bottom:solid;border-left:solid;solid;background-color:rgb(204,204,204);"><strong>Cod. Articulo</strong></th>
        <th scope="col" style="border-width:thin;border-bottom:solid;border-left:solid;solid;background-color:rgb(204,204,204);"><strong>Articulo</strong></th>
        <th scope="col" style="border-width:thin;border-bottom:solid;border-left:solid;solid;background-color:rgb(204,204,204);"><strong>Cantidad</strong></th>
        <th scope="col" style="border-width:thin;border-bottom:solid;border-left:solid;solid;background-color:rgb(204,204,204);"><strong>Uni. Medida</strong></th>
    </tr>
  </thead>
    				<tbody>
        						<?php
									//print_r($paginat);
									$pagact=$solicitudpagina;
									$part= 1;
									$numRowsww = 0;
									$lin = 1;
									for($g=0; $g < (count($paginat)); $g++)
									{
									
									$ante = $paginat[$g-1]["APRCOD"];
									
									
									
									
								 	if($paginat[$g]["APRCOD"]!=$ante)
									{
										
										if($lin % 2 == 0){ $color = 'background-color:#add6ff;';	}
										else			 { $color = '';								}
										
										$html.='
										<tr style="'.$color.'">
											<td style="border-width:thin;border-bottom:solid;border-left:solid;text-align:left;vertical-align:top;" rowspan="'.$paginat[$g]["CANTIDAD"].'" >'.$paginat[$g]["APRDES"].'('.$paginat[$g]["APRCOD"].')</td>
											<td style="border-width:thin;border-bottom:solid;border-left:solid;text-align:left;vertical-align:top;" rowspan="'.$paginat[$g]["CANTIDAD"].'" >'.$paginat[$g]["DESM09"].'</td>';
											$lin++;
									}
									
									$html.='	 
                                        <td style="border-width:thin;border-bottom:solid;border-left:solid;'.$color.'">'.$paginat[$g]["ADSNRO"].'</td>
                                        <td style="border-width:thin;border-bottom:solid;border-left:solid;'.$color.'">'.$paginat[$g]["ATRDES"].'</td>
                                        <td style="border-width:thin;border-bottom:solid;border-left:solid;'.$color.'">'.$paginat[$g]["ATRFEC"].'</td>
                                        
                                        <td style="border-width:thin;border-bottom:solid;border-left:solid;'.$color.'">'.$paginat[$g]["AARCOD"].'</td>
                                        <td style="border-width:thin;border-bottom:solid;border-left:solid;'.$color.'">'.$paginat[$g]["AARDES"].'</td>
                                        <td style="border-width:thin;border-bottom:solid;border-left:solid;'.$color.'">'.number_format($paginat[$g]["ATRCAN"],2,',','').'</td>
                                        <td style="border-width:thin;border-bottom:solid;border-left:solid;'.$color.'">'.$paginat[$g]["AUMDES"].'</td>
                                    </tr>
									';

									
									
									}
									echo $html;
									
								?>                                            
                  </tbody>
</table>
</td>
</tr>
</table>
</body>
</html>
