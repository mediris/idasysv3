<?php session_start();
include("../conectar.php");
$arqnro = trim($_GET["num"]);
$tinva = unserialize( urldecode( $_GET['tinvan'] ) ) ;
include("../Librerias/PHPExcel/PHPExcel.php");

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header("Content-disposition: attachment; filename=Reporte_Consumo_Detalle_".formatDate($Fechaactual,'dd.mm.aaa','aaaa_mm_dd'));
header('Cache-Control: max-age=0');


						$z=0;
			
						$tipoInve ='';
						if(!empty($tinva)){
							foreach($tinva as $key => $value){
								$tipoInve .= "'".$value."',";
							}
							$tipoInve = substr($tipoInve,0, (strripos($tipoInve,",")));
						}
						//\"SF.APDATA\".RH009L1 -> SFAPDATAV.RH009L3
						$sql="SELECT
							(
								SELECT count(*)                                                   
								FROM IDASYSW.IS21FP A212                                          
									INNER JOIN \"SF.APDATA\".RH009L1 A092 ON UBIC09=APRUBI                
									INNER JOIN (IDASYSW.IV46FP A462 INNER JOIN (IDASYSW.IV35FP A352   
									INNER JOIN IDASYSW.IV36FP A362 ON A362.ACICOD=A352.ACICOD AND     
										A352.ADSNRO=A362.ADSNRO)                                          
										 ON A462.ACICOD=A352.ACICOD AND A352.ATSCOD=13 AND A352.ADSSTS=03 
										AND A352.ADSNRO=A462.ADSNRO)                                      
										ON A212.ACICOD=A462.ACICOD AND A462.AAPVLA=A212.APRCOD            
								WHERE A212.ACICOD=A21.ACICOD and A212.APRCOD=A21.APRCOD AND A352.ATRFEC BETWEEN '".$desde."' AND '".$hasta."'            
							) AS CANTIDAD,
							
							A21.ACICOD, A21.APRCOD, A21.APRDES, A21.APRMOD,         
							A21.APRUBI,A09.DESM09, A35.ADSNRO, A35.ATRDES, A35.ATRFEC,     
							A36.AARCOD, A05.AARDES, A36.ATRCAN, A36.ATRUMB, A13.AUMDES 
							FROM IDASYSW.IS21FP A21 
								INNER JOIN \"SF.APDATA\".RH009L1 A09 ON UBIC09=APRUBI 
								
								INNER JOIN (IDASYSW.IV46FP A46 INNER JOIN (IDASYSW.IV35FP A35 INNER JOIN IDASYSW.IV36FP A36 ON A36.ACICOD=A35.ACICOD AND A35.ADSNRO=A36.ADSNRO) 
									ON A46.ACICOD=A35.ACICOD AND A35.ATSCOD=13 AND A35.ADSSTS=03 AND A35.ADSNRO=A46.ADSNRO) 
								ON A21.ACICOD=A46.ACICOD AND A46.AAPVLA=A21.APRCOD 
							
								INNER JOIN IDASYSW.IV05FP A05 ON (A05.ACICOD=A21.ACICOD AND A05.AARCOD=A36.AARCOD) 
								INNER JOIN IDASYSW.IV06FP A06 ON (A05.ACICOD=A06.ACICOD AND A05.AARCOD=A06.AARCOD) 
								INNER JOIN IDASYSW.IV13FP A13 ON (A05.ACICOD=A13.ACICOD AND A06.AARUMB=A13.AUMCOD) 
							WHERE A21.ACICOD='".$Compania."' AND 
								  A35.ATRFEC BETWEEN '".$desde."' AND '".$hasta."'
							ORDER BY A21.APRDES, A21.APRCOD, A35.ATRFEC, A05.AARDES ";
						
						$query = $cid->queryAll($sql);
						$resultt = $query->fetchAll(PDO::FETCH_ASSOC);

						foreach ($resultt as $fields){
							foreach ($fields as $val => $valorinterno)
							{
								$row[$z][$val] = ICONV( "ISO-8859-1", "UTF-8", trim($valorinterno));
							}
							$z++;
						}

						$_SESSION['solicitudarreglo']=$row;
						$paginat=$_SESSION['solicitudarreglo'];
						
						$excel = new PHPExcel();

						//Propiedades del Excel
						$excel->getProperties()
						   ->setCreator('IDASYS WEB')
						   ->setTitle('Reporte Consumible Detalle')
						   ->setLastModifiedBy('IDASYS WEB')
						   ->setDescription('A demo to show how to use PHPExcel to manipulate an Excel file')
						   ->setSubject('Reporte de Entradas')
						   ->setKeywords('excel php office phpexcel')
						   ->setCategory('inventario')
						   ;

						$exceldata = $excel->getSheet(0);
						$exceldata->setTitle('Reporte_Consumo_Detalle');

						//Titulo
						$styleArray = array(
							    'font'  => array(
							        'bold'  => true
							    ),
							    'fill' => array(
					            'type' => PHPExcel_Style_Fill::FILL_SOLID,
					            'color' => array('rgb' => 'BDBDBD')
					            ),
					            'alignment' => array(
	            				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
	            				),
	            				'borders' => array(
					            'allborders' => array(
					                'style' => PHPExcel_Style_Border::BORDER_THIN,
					            	)
					        	)
						    );

						//Head Logo
					    $objDrawing = new PHPExcel_Worksheet_Drawing();
				        $objDrawing->setName('Logo');
				        $objDrawing->setDescription('Logo');
				        $objDrawing->setPath('C:\xampp\htdocs\idasysv3\images\MEDITRON_logo_rif.png');
				        $objDrawing->setHeight(70);
				        $objDrawing->setWidth(330);

				        $objDrawing->setCoordinates('A1');
				        $objDrawing->setWorksheet($excel->getActiveSheet());
				        //

						$excel->getActiveSheet()->getCell('A4')->setValue('RIF: '.$Companiarif);

						//Titulo 
						$excel->getActiveSheet()->getCell('A7')->setValue('Consumo de Tonner MEDITRON');
						$exceldata->mergeCells('A7:I7');
						$excel->getActiveSheet()->getStyle('A7')->getFont()->setBold(true);
						$excel->getActiveSheet()->getStyle('A7')->getFont()->setSize(25);
						$excel->getActiveSheet()->getStyle('A7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
						//


						$excel->getActiveSheet()->getCell('D9')->setValue('Desde: '.$desde.' - Hasta: '.$hasta);
						$excel->getActiveSheet()->getCell('D10')->setValue('Elaborado el '.$Fechaactual.' a las '.$Horaactual2);
						$excel->getActiveSheet()->getStyle('A4')->getFont()->setBold(true);
						$excel->getActiveSheet()->getStyle('D9:D10')->getFont()->setBold(true);
						$excel->getActiveSheet()->getStyle('D9:D10')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);


						$exceldata->setCellValue('A11', 'Información de Impresora');
						$exceldata->setCellValue('C11', 'Información de Dotación');
						$exceldata->setCellValue('F11', 'Información Detalle');
						$exceldata->mergeCells('A11:B11');
						$exceldata->mergeCells('C11:E11');
						$exceldata->mergeCells('F11:I11');
						$excel->getActiveSheet()->getStyle('A11:I11')->applyFromArray($styleArray);


						$exceldata->setCellValue('a12', 'Impresoras'); // Sets cell 'a1' to value 'ID 
					    $exceldata->setCellValue('b12', 'Gerencia Encargada');
					    $exceldata->setCellValue('c12', 'Nro. Dotacion Salida');
					    $exceldata->setCellValue('d12', 'Persona Solicitante');
					    $exceldata->setCellValue('e12', 'Fecha de Solicitud');
					    $exceldata->setCellValue('f12', 'Cod. Articulo');
					    $exceldata->setCellValue('g12', 'Articulo');
					    $exceldata->setCellValue('h12', 'Cantidad');
					    $exceldata->setCellValue('i12', 'Uni. Medida');
					    $excel->getActiveSheet()->getStyle('A12:I12')->applyFromArray($styleArray);
					    				   		
					    //Genera Data
					    $rowCant = 0;

					    foreach($paginat as $rows) {
					    	$rowCant++;
					    }

					    $g = 0;
					    $pos = 13;
					   // $color = 1;
					    for ($i = 0; $i < $rowCant; $i++){
					    	$ante = $paginat[$g-1]["APRCOD"];
					    	//echo $ante;
					    	//break;
					    	//die();
							if($paginat[$g]["APRCOD"]!=$ante)
							{
						    	$lineas = $pos+$paginat[$g]["CANTIDAD"]-1;
						    	if(($lineas+$pos)>$pos){
						    		$exceldata->mergeCells('A'.$pos.':A'.$lineas);
						    		$exceldata->mergeCells('B'.$pos.':B'.$lineas);
						    	}
						        $exceldata->setCellValue('A' . $pos, $paginat[$g]["APRDES"].'('.$paginat[$g]["APRCOD"].')');
						        $exceldata->setCellValue('B' . $pos, $paginat[$g]["DESM09"]);
					    	} else {
					    		$exceldata->setCellValue('A' . $pos, $paginat[$g]["APRDES"].'('.$paginat[$g]["APRCOD"].')'); 
						        $exceldata->setCellValue('B' . $pos, $paginat[$g]["DESM09"]);
					    	}
					        $exceldata->setCellValue('C' . $pos, $paginat[$g]["ADSNRO"]);
					        $exceldata->setCellValue('D' . $pos, $paginat[$g]["ATRDES"]);
					        $exceldata->setCellValue('E' . $pos, $paginat[$g]["ATRFEC"]);
					        $exceldata->setCellValue('F' . $pos, $paginat[$g]["AARCOD"]);
					        $exceldata->setCellValue('G' . $pos, $paginat[$g]["AARDES"]);
					        $exceldata->setCellValue('H' . $pos, $paginat[$g]["ATRCAN"]);
					        $exceldata->setCellValue('I' . $pos, $paginat[$g]["AUMDES"]);
					       /* if($color % 2 == 0) {
					        	$excel->getActiveSheet()->getStyle('A'.$pos.':I'.$pos)->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => 'add6ff'))));
					        }
					        $color++;*/
					        $g++;	
					        $pos++;			        
				   		}


						//Bordes y ajuste de anchura para la data generada
						$lin = 13;
				   		$prevpos = $lin-1;
						for($col = 'A'; $col !== 'J'; $col++) 
					    {
					    	$value = $excel->getActiveSheet()->getCell($col.$prevpos)->getValue();
				   			$width = mb_strwidth($value);
					    	for($i = 0; $i <$rowCant; $i++){
					    		$value1 = $excel->getActiveSheet()->getCell($col.$lin)->getValue();
				   				$width1 = mb_strwidth($value1);
								if($width1 > $width){
									$width = $width1;
								}
								$excel->getActiveSheet()->getStyle($col.$lin)->applyFromArray(array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN,))));
								$lin++;
								$prevpos++;
							}
							$width = $width+5;
							$excel->getActiveSheet()->getColumnDimension($col)->setAutoSize(false);
							$excel->getActiveSheet()->getColumnDimension($col)->setWidth($width);
							$width = 0;
							$lin = 13;
							$prevpos = $lin-1;
						}
						//
						//Se genera el excel
				        $writer = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
						$writer->save('php://output');
						exit();
			?>  