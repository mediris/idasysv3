<?php 
/*
 * jDavila
 * 23/05/2012 
 * modificado: 24/05/2012
 */
session_start();
include("../../conectar.php");

$tinva = $_GET['tinvan'];

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
<title>Idasys V3</title>
<link href="../../<?php echo $css; ?>" rel="stylesheet" type="text/css" media="screen" />
<script language="JavaScript" src="../../javascript/javascript.js"></script>
<script language="JavaScript" src="../../calendario/javascripts.js"></script>
<script language="JavaScript" src="javascript.js"></script>
<script language="JavaScript" src="../../javascript/jquery.js"></script>
<link href="../../shadowbox.css"  rel="stylesheet" type="text/css"/>
<script src="../../shadowbox.js" type="text/javascript"/> </script>
<script language="JavaScript" type="text/JavaScript">
Shadowbox.init({overlayOpacity: "0.5"});
</script>
<style type="text/css" title="currentStyle">
			@import "../../DataTables-1.9.4/media/css/<?php echo $demo_page;?>";
			@import "../../DataTables-1.9.4/media/css/<?php echo $demo_table;?>";
</style>
<script type="text/javascript" language="javascript" src="../../DataTables-1.9.4/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf-8">
			
			 $(document).ready(function() 
			 {
			      document.getElementById('info').style.display="";
				  $('#info').dataTable( 
				  {
					"bStateSave": true,
			        "oLanguage": 
					{
			          	"sLengthMenu": "Mostrar _MENU_ registros por Pag.",
						"sZeroRecords": "No Existen Registros",
						"sInfo": "Mostrando desde _START_ hasta _END_ de _TOTAL_ registros",
						"sInfoEmpty": "Mostrando desde 0 hasta 0 de 0 registros",
						"sInfoFiltered": "(filtrando de _MAX_ de registros)",
						"sSearch": "Buscar",
						"oPaginate": 
						{
							"sNext": "Siguiente",
							"sPrevious": "Anterior"
						}
			        }
					/*,
					"aoColumnDefs": [
						{ "bVisible": false, "aTargets": [ 0 ] }
					]*/
			      } );
			  } );
</script>

</head>
<body background="../../images/fondo idaca.jpg" >
<div id="wrapper">
  <?php include("../../superior.php");?>
  <div id="page">
     <?php include("../../validar.php");?>

	  
 		<?php 
				
				if ($bandera==1) 
				{	
					$wsolicitud=0;
					if ($solicitudpagina==0) 	
					{
						$tipoInve ='';
						if(!empty($tinva)){
							foreach($tinva as $key => $value){
								$tipoInve .= "'".$value."',";
								/*
								echo '<script>
										$("#tinvan").prop("checked", "checked");
									  </script>';
									  */
							}
							$tipoInve = substr($tipoInve,0, (strripos($tipoInve,",")));
						}
						
						
						$sql="SELECT T1.ACICOD, T1.AALCOD, T1.AARCOD, T3.AARDES, T1.AUMCOD, T4.AUMDES, T1.ALTCOD, T5.AARSTM, T3.ATICOD, T6.ATIDES,
									SUM(CASE WHEN T1.ASLFEF=(SELECT MAX(T2.ASLFEF) FROM IV40FP T2 WHERE T2.ACICOD=T1.ACICOD AND T2.AALCOD=T1.AALCOD AND T2.AARCOD=T1.AARCOD AND T2.ALTCOD=T1.ALTCOD AND T2.ASLFEF<'$desde' ) THEN T1.ASLSAF ELSE 0 END) AS SALANT, 
									SUM(CASE WHEN T1.ASLFEF BETWEEN '$desde' AND '$hasta' THEN T1.ASLENT ELSE 0 END) AS ASLENT, 
									SUM(CASE WHEN T1.ASLFEF BETWEEN '$desde' AND '$hasta' THEN T1.ASLSAL ELSE 0 END) AS ASLSAL 
								FROM IV40FP T1 
									INNER JOIN IV05FP T3 ON(T1.ACICOD=T3.ACICOD AND T1.AARCOD=T3.AARCOD) 
									INNER JOIN IV13FP T4 ON(T1.ACICOD=T4.ACICOD AND T1.AUMCOD=T4.AUMCOD) 
									LEFT JOIN IV39FP T5 ON(T1.ACICOD=T5.ACICOD AND T1.AARCOD=T5.AARCOD AND T1.AALCOD=T5.AALCOD) 
									INNER JOIN IV01FP T6 ON (T1.ACICOD=T6.ACICOD  AND T3.ATICOD=T6.ATICOD)
								WHERE T1.ACICOD='$Compania' AND T1.AALCOD='$aalcod' ";
						if(!empty($tipoInve)){
							$sql.="  AND T3.ATICOD IN (".$tipoInve.")";
						}
						$sql.="  GROUP BY T1.ACICOD, T1.AALCOD, T1.AARCOD, T3.AARDES, T1.ALTCOD , T4.AUMDES,T1.AUMCOD, T5.AARSTM, T3.ATICOD, T6.ATIDES
								ORDER BY T3.AARDES";
								
								//echo $sql;
								
								$resultt=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111"));
								
								
								$z=0;
								$tothon=0;
								$totest=0;
								$lin=1;
								$limitep=$_SESSION['solicitudlineasporpaginat'];
								$pag=1;
								$primero='S';
								
								while(odbc_fetch_row($resultt))
								{
									$jml = odbc_num_fields($resultt);
									$row[$z]["pagina"] =  $pag;
									for($i=1;$i<=$jml;$i++)
									{	if (odbc_field_name($resultt,$i)=='ARGTTE') {$totest+=odbc_result($resultt,$i);}
										if (odbc_field_name($resultt,$i)=='ARGTTM') {$tothon+=odbc_result($resultt,$i);}
										$row[$z][odbc_field_name($resultt,$i)] =  odbc_result($resultt,$i);
									}
									$z++;
									if ($lin>=$limitep) 
									{
										$limitep+=$_SESSION['solicitudlineasporpaginat'];
										$pag++;
									}
									$lin++;
								}
								$totsol=($lin-1);
								$_SESSION['totalsolicitudes']=$totsol;
								$_SESSION['solicitudarreglo']=$row;
								$solicitudpagina=1;
								$_SESSION['solicitudpaginas']=$pag;
					}//fin de solicitudpagina
					/*se muestra la cantidad de elementos segun la solicitada en $solicitudpagina*/
					$paginat=$_SESSION['solicitudarreglo'];
				}
			?>
        <div id="content3" >   
        	<table width="100%"   border="0">
              <tr>
                <td width="65%" scope="col" colspan="2"><h1  class="title">Consulta Existencia</h1><hr /></td>
                <td width="13%" scope="col"><div align="left">
                  <table width="100%"  border="0">
                    <tr>
                      <th width="30%" scope="col" >
                      <div class="flechas">
                      	<a href="exportarapdf.php?&desde=<?php echo $desde; ?>&hasta=<?php echo $hasta; ?>&aalcod=<?php echo $aalcod; ?>&stm=<?php echo $stm; ?>" target="_blank">
                        	<img src="../../images/pdf.jpg" alt="" width="30" height="30" border="0" class="flechas" title="Exportar a PDF"/>
                        </a>
                        &nbsp;&nbsp;
                      	<a href="exportaraexcel.php?&desde=<?php echo $desde; ?>&hasta=<?php echo $hasta; ?>&aalcod=<?php echo $aalcod; ?>&stm=<?php echo $stm; ?>&tinvan=<?php echo $url222 = urlencode( serialize($tinva) );?>" target="_blank">
                        	<img src="../../images/excel.jpg" alt="" width="30" height="30" title="Exportar a Excel"/>
                        </a>
                      </div></th>
                    </tr>
                  </table>
                </div></td>
              </tr>
              <tr>
              	<td colspan="3">
                	<form name="form" id="form" method="post" on>
                    <input type="hidden" name="bandera" id="bandera" value="<?php echo $bandera;?>" />
                	<table>
                    	<tr>
                        	<td>&nbsp;Desde:</td>
                            <td>&nbsp;<?php echo  escribe_formulario_fecha_vacio('desde','form',$desde); ?></td>
                            <td>&nbsp;</td>
                            <td>&nbsp;Hasta:</td>
                            <td>&nbsp;<?php echo  escribe_formulario_fecha_vacio('hasta','form',$hasta); ?></td>
                            <td rowspan="3"><a href="javascript:busqueda();"><img src="../../images/precesarconsulta.png" width="50" height="50" alt="Realizar Consulta" style="border:none;"></a></td>
                        </tr>
                        <tr>
                        	<td>&nbsp;Almac�n:</td>
                            <td>&nbsp;<?php //echo $aalcod; ?>
                            	<select name="aalcod" id="aalcod" onchange="cargarSelectTpInven(<?php ?>)" >
									<?php 
                                    //$sql="SELECT AALCOD, AALDES FROM iv07fp where acicod ='$Compania' order by AALDES ";
									if(accesotodasunisol_alma('4')=='S')
									{
										$sql="SELECT AALCOD, AALDES FROM iv07fp where acicod ='$Compania'  order by AALDES ";//and AALCOD <> '0017'
									}
									else
									{
										$sql = "SELECT T1.AALCOD, T2.AALDES FROM iv44fp T1, IV07FP T2 WHERE T1.acicod ='$Compania' AND T1.ACICOD=T2.ACICOD AND T1.auscod='$Usuario' AND T1.AALCOD= T2.AALCOD order by t2.AALDES";
									}
                                    $result1=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111")); 
									$select = '';
                                    while(odbc_fetch_row($result1)){
                                        $cod=trim(odbc_result($result1,1));
                                        $des=trim(odbc_result($result1,2));
										if(!empty($aalcod))
										{
											if($cod == $aalcod){
												$select = ' selected="selected" ';
											}
											else{
												$select = '';
											}
										}											
                                    ?>
                                        <option value= "<?php echo $cod; ?>" <?php echo $select?> ><?php echo $des."(".$cod.")"; ?></option>
                                    <?php } ?>
                                </select>
                            </td>
                            <td>&nbsp;</td>
                            <td>Solo Stock<br /> M&iacute;nimo</td>
                            <td>
                            	<?php if($stm == 1 ){
										$check = 'checked="checked"';
									  }
									  else{
										  $check = '';
									  }
								?>
                            	<input name="stm" id="stm" type="checkbox" value="" <?php echo $check; ?>/>
                                
                             </td>
                        </tr>
                        <tr>
                        	<td>Tipo de Inventario</td>
                            <td colspan="4">
                            	<div id="table_tpinven">
                                </div>
                                <script>cargarSelectTpInven();</script>
                            </td>
                        </tr>
                    </table>
                    </form>
                    <hr />
                </td>
                
              </tr>
              <!--<tr>
                <td colspan="2" scope="col"><h5 align="left"><?php echo $paginat[0]["ALCSED"];?>wwww</h5></td>
                <td scope="col"> Pagina: <?php if (!$solicitudpagina) {$paginaarriba=1;} else {$paginaarriba=$solicitudpagina;}; echo $paginaarriba."/".$_SESSION['solicitudpaginas']; ?></td>
              </tr>-->
            </table>   
		<div id="container">
            <div id="demo">
                <table width="100%" id="info" style="display:none">
                  <thead>
                    <tr>
                        <!--<th scope="col">B1</th>-->
                        <th scope="col">Foto</th>
                        <th width="30%"  scope="col">Art&iacute;culo</th>
                        <th width="5%" scope="col">Cant. M&iacute;nima</th>
                        <!--<th width="5%" scope="col">Tipo Inventario</th>-->
                        <th width="10%" scope="col">Ubicaci&oacute;n</th>
                        <th width="10%" scope="col">Unidad de Medida</th>
                        <th width="10%" scope="col">Saldo Anterior</th>
                        <th width="10%" scope="col">Entrada</th>
                        <th width="10%" scope="col">Salida</th>
                        <th width="10%" scope="col">Saldo Final</th>
                        <th width="5%" scope="col">Opciones</th>
                    </tr>
                  </thead>
    				<tbody>
        						<?php 
									$show = false;
									//print_r($paginat);
									$pagact=$solicitudpagina;
									for($g=0; $g < (count($paginat)); $g++)
									{
                                ?>
                                <?php $sql="SELECT AMDCOD, AMMCOD, AMMDES, AMMTIP, AMMSTS FROM MB09FP WHERE AMDCOD='$modulo' AND AMMTIP='F' AND AMMCOD='1' ORDER BY AMMCOD";
                                      $resultt2=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111")); 
									  $salFinal = $paginat[$g]["SALANT"]+$paginat[$g]["ASLENT"]+$paginat[$g]["ASLSAL"];
									  
									  /*validar si piden solo stm */
									  if($stm==1){//si 
										  if($salFinal<=$paginat[$g]["AARSTM"])	{$show = true;}
										  else									{$show = false;}
									  }else if($stm==2){
										 $show = true; 
									  }
									  
									  if($show){
								?>
                                        <tr>
                                        	<!--<td><?php if($salFinal<=$paginat[$g]["AARSTM"]){echo "M&Iacute;NIMO";}else{echo "";} ?></td>-->
                                        	<td scope="col"><div>
													<?php 
														$mostrar = 0;
														$foto = "../../fotosarticulos/A_".trim($paginat[$g]["AARCOD"])."_". odbc_result($resultt2,2).'.jpg';
														if(!file_exists($foto))
														{$foto ='../../images/nodisponible.png'; $mostrar = 0;}else{$mostrar = 1;}
															if($mostrar)
															{															
													?>
                                                    			<a target='_blank' href='<?php echo trim($foto); ?>'>
                                                    <?php   } ?>
                                                    <img src='<?php echo trim($foto); ?>' width="50" height="50" border='0'/>
                                                    <?php 
															if(file_exists($foto))
															{															
													?>
                                                   	</a>
                                                    <?php } ?></div></td>
                                            <td scope="col"><div><?php echo $paginat[$g]["AARDES"]."<strong>(".$paginat[$g]["AARCOD"].")</strong>";?></div></td>                
                                            <td scope="col">
                                                <div align="center">
                                                    <a rel="shadowbox;width=400;height=200" href="editarfstm.php?&aarcod=<?php echo trim($paginat[$g]["AARCOD"]);?>&aalcod=<?php echo $aalcod; ?>" title="Editar" >
                                                    	<?php echo number_format($paginat[$g]["AARSTM"],2,",",".");?>
                                                    </a>
                                                </div>
                                            </td>
                                            <!--
                                            <td scope="col">
                                                <div align="center">
                                                    <?php echo $paginat[$g]["ATIDES"];?>
                                                </div>
                                            </td>
                                            -->
                                            <td scope="col" id="opciones"><div><strong>
											<?php 
												$list = list_ubiart($cid, $Compania, $paginat[$g]["AALCOD"], $paginat[$g]["AARCOD"],2);
												//echo "*".trim($paginat[$g]["AUBCOD"])."*";
												echo '<a rel="shadowbox;width=400;height=300" href="editarfubicacion.php?&aarcod='.trim($paginat[$g]["AARCOD"]).'&aalcod='.$aalcod.'" title="Editar" >';
												
												if(trim($list)!=''){
													echo $list; } 
												else { 
													echo ' - Sin Ubicaci�n';
												}
												echo '</a>';
											 ?>
                                            </strong></div></td>
                                            <td scope="col"><div align="center"><?php echo $paginat[$g]["AUMDES"];?></div></td>
                                            <td scope="col"><div align="right"><?php echo number_format($paginat[$g]["SALANT"],2,",",".");?></div></td>
                                            <td scope="col"><div align="right"><?php echo number_format($paginat[$g]["ASLENT"],2,",",".");?></div></td>
                                            <td scope="col"><div align="right"><?php echo number_format($paginat[$g]["ASLSAL"],2,",",".");?></div></td>
                                            
                                            <td scope="col"><div align="right"><?php echo number_format(($salFinal),2,",",".");?></div>  </td>
                                            <td scope="col">
                                            		<!--<a rel="shadowbox;width=650;height=345" title="Detalle" href="detalle.php?&id=<?php echo trim($paginat[$g]["AARCOD"]); ?>&fdesde=<?php echo $desde; ?>&fhasta=<?php echo $hasta; ?>">Detalle</a>-->
                                            		<a href="javascript:verdetalle('<?php echo trim($paginat[$g]["AARCOD"]); ?>','<?php echo $desde; ?>','<?php echo $hasta; ?>', '<?php echo $aalcod; ?>', <?php echo $paginat[$g]["SALANT"]; ?>,'<?php echo trim($paginat[$g]["AUMDES"]);?>' )"><img src="../../images/ver.png" title="Ver Detalle" width="31" height="28" border="0"></a>		
                                                    <?php if($Compania == '40') { ?>
                                                    	<a href="javascript:abriretiquetas('<?php echo $aalcod; ?>','<?php echo trim($paginat[$g]["AARCOD"]); ?>','','' )"><img src="../../images/etiqueta.png" title="Ver Detalle" width="31"  border="0"></a>		
                                                    <?php } ?>
                                            </td>
                                        </tr>
                            	<?php } }?>      
                  </tbody>
                </table>
</div>
		  </div>
	  </div>
	</div>
		<!-- end #content -->
		<!-- end #sidebar -->
		<div style="clear: both;">&nbsp;</div>
	</div>
	<!-- end #page -->

	<div id="footer">
		<div id="opcionesmasusadas"><?php include("../../opcionesmasusadasphp.php"); ?></div>
	</div>
    <?php 
		echo "<script>
				var list = [];
				var pos = 1;
				var lista = \"".$tipoInve."\";
				$('input[name=\"tinvan[]\"]').each(function() { 
					//alert($(this).val() );
					if( lista.indexOf($(this).val()) != -1 ){
						alert('paso '+$(this).val()) ;
						 $(this).attr('checked', true); 
					}
					pos++;
				});
			  </script>";
	?>
 
	<!-- end #footer -->
</body>
</html>
