<?php 
/*
 * Luis Ramos (lRamos)
 * 10/11/2016  
 * Johan Davila
 * 28/03/2017
 */
session_start();
include("../conectar.php");
$tinva = $_GET['tinvan'];
$aalcod = $_GET['aalcod'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
<title>Idasys V3</title>
<link href="../<?php echo $css; ?>" rel="stylesheet" type="text/css" media="screen" />
<script language="JavaScript" src="../javascript/javascript.js"></script>
<script language="JavaScript" src="../calendario/javascripts.js"></script>
<script language="JavaScript" src="javascript.js"></script>
<script language="JavaScript" src="../javascript/jquery.js"></script>
<link href="../shadowbox.css"  rel="stylesheet" type="text/css"/>
<script src="../shadowbox.js" type="text/javascript"/> </script>
<script language="JavaScript" type="text/JavaScript">
Shadowbox.init({overlayOpacity: "0.5"});
</script>
<style type="text/css" title="currentStyle">
			@import "../DataTables-1.9.4/media/css/<?php echo $demo_page;?>";
			@import "../DataTables-1.9.4/media/css/<?php echo $demo_table;?>";
</style>
<script type="text/javascript" language="javascript" src="../DataTables-1.9.4/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf-8">
			
			 $(document).ready(function() 
			 {
			      document.getElementById('info').style.display="";
				  $('#info').dataTable( 
				  {
					"bStateSave": true,
			        "oLanguage": 
					{
			          	"sLengthMenu": "Mostrar _MENU_ registros por Pag.",
						"sZeroRecords": "No Existen Registros",
						"sInfo": "Mostrando desde _START_ hasta _END_ de _TOTAL_ registros",
						"sInfoEmpty": "Mostrando desde 0 hasta 0 de 0 registros",
						"sInfoFiltered": "(filtrando de _MAX_ de registros)",
						"sSearch": "Buscar",
						"oPaginate": 
						{
							"sNext": "Siguiente",
							"sPrevious": "Anterior"
						}
			        }
					/*,
					"aoColumnDefs": [
						{ "bVisible": false, "aTargets": [ 0 ] }
					]*/
			      } );
			  } );
</script>
</head>
<body background="../images/fondo idaca.jpg" >
<div id="wrapper">
  <?php include("../superior.php");?>
  <div id="page">
     <?php include("../validar.php");?>

	  
 		<?php 
				
				if ($bandera==1) 
				{	
					$tipoInve ='';
					if(!empty($tinva)){
						foreach($tinva as $key => $value){
							$tipoInve .= "'".$value."',";
						}
						$tipoInve = substr($tipoInve,0, (strripos($tipoInve,",")));
					}

					/*
					$sql="SELECT T1.AARCOD, T1.AARDES,
							(SELECT SUM(T2.ATRCAN) FROM IV16FP T2 WHERE T2.ACICOD='42' AND T2.AALCOD IN ('$tipoAlm') AND T2.ATRCOD IN ( '0101', '0102') AND T2.ATRART=T1.AARCOD) as ENTRADAS,
							(SELECT SUM(T3.ATRCAN) FROM IV16FP T3 WHERE T3.ACICOD='42' AND T3.AALCOD IN ('$tipoAlm','0004', '0005', '0006') AND T3.ATRCOD IN ('$tipoSal','0103') AND T3.ATRART=T1.AARCOD) as SALIDAS, 
							(SELECT MAX(T4.ATRCUT) FROM IV16FP T4 WHERE T4.ACICOD='42' AND T4.AALCOD IN ('$tipoAlm') AND T4.ATRCOD='0101' AND T4.ATRART=T1.AARCOD) as COSTO,
							(SELECT SUM(T3.ATRCAN) FROM IV16FP T3 WHERE T3.ACICOD='42' AND T3.AALCOD IN ('$tipoAlm','0004', '0005', '0006') AND T3.ATRCOD='$tipoDevT' AND T3.ATRART=T1.AARCOD) as DEVOLUCIONES
							FROM IV05FP T1 WHERE T1.ACICOD='42' AND (SELECT SUM(T2.ATRCAN) FROM IV16FP T2 WHERE T2.ACICOD='42' AND T2.AALCOD IN ('$tipoAlm','0004', '0005', '0006') AND T2.ATRCOD='0101' AND T2.ATRART=T1.AARCOD) > 0";
					if(!empty($tipoInve)){
						$sql.="  AND T1.ATICOD IN (".$tipoInve.")";
					}
					*/
					$sql ="SELECT T1.AARCOD, T1.AARDES,
					( (SELECT SUM(T2.ATRCAN) FROM IDASYSW.IV16FP T2 WHERE T2.ACICOD='14' AND T2.AALCOD IN ('0001') AND T2.ATRCOD IN ( '0103', '3215') AND T2.ATRART=T1.AARCOD) - 
								 CASE 
									WHEN (SELECT SUM(T5.ATRCAN) FROM IDASYSW.IV16FP T5 WHERE T5.ACICOD='14' AND T5.AALCOD IN ('0001') AND T5.ATRCOD IN ('0102', '0105', '0055', '0058', '0059', '0062', '0063', '0065') AND T5.ATRART=T1.AARCOD )>0 
									THEN (SELECT SUM(T5.ATRCAN) FROM IDASYSW.IV16FP T5 WHERE T5.ACICOD='14' AND T5.AALCOD IN ('0001') AND T5.ATRCOD IN ('0102', '0105', '0055', '0058', '0059', '0062', '0063', '0065') AND T5.ATRART=T1.AARCOD )
									ELSE 0
								  END +
					             CASE 
									WHEN (SELECT SUM(T5.ATRCAN) FROM IDASYSW.IV16FP T5 WHERE T5.ACICOD='14' AND T5.AALCOD IN ('0001') AND T5.ATRCOD IN ('0101', '0104', '0003', '0056', '0057', '0060', '0061', '0064', '0066') AND T5.ATRART=T1.AARCOD )>0 
									THEN (SELECT SUM(T5.ATRCAN) FROM IDASYSW.IV16FP T5 WHERE T5.ACICOD='14' AND T5.AALCOD IN ('0001') AND T5.ATRCOD IN ('0101', '0104', '0003', '0056', '0057', '0060', '0061', '0064', '0066') AND T5.ATRART=T1.AARCOD )
									ELSE 0
								  END
								) as ENTRADAS,
					( CASE WHEN (SELECT SUM(T2.ATRCAN) FROM IDASYSW.IV16FP T2 WHERE T2.ACICOD='14' AND T2.AALCOD IN ('0001') AND T2.ATRCOD IN ('0051', '0052') AND T2.ATRART=T1.AARCOD)>0
					THEN (SELECT SUM(T2.ATRCAN) FROM IDASYSW.IV16FP T2 WHERE T2.ACICOD='14' AND T2.AALCOD IN ('0001') AND T2.ATRCOD IN ('0051', '0052') AND T2.ATRART=T1.AARCOD)  - 
								 CASE 
									WHEN (SELECT SUM(T5.ATRCAN) FROM IDASYSW.IV16FP T5 WHERE T5.ACICOD='14' AND T5.AALCOD IN ('0001') AND T5.ATRCOD IN ('0102', '0105', '0055', '0058', '0059', '0062', '0063', '0065') AND T5.ATRART=T1.AARCOD )>0 
									THEN (SELECT SUM(T5.ATRCAN) FROM IDASYSW.IV16FP T5 WHERE T5.ACICOD='14' AND T5.AALCOD IN ('0001') AND T5.ATRCOD IN ('0102', '0105', '0055', '0058', '0059', '0062', '0063', '0065') AND T5.ATRART=T1.AARCOD )
									ELSE 0
								  END +
					             CASE 
									WHEN (SELECT SUM(T5.ATRCAN) FROM IDASYSW.IV16FP T5 WHERE T5.ACICOD='14' AND T5.AALCOD IN ('0001') AND T5.ATRCOD IN ('0101', '0104', '0003', '0056', '0057', '0060', '0061', '0064', '0066') AND T5.ATRART=T1.AARCOD )>0 
									THEN (SELECT SUM(T5.ATRCAN) FROM IDASYSW.IV16FP T5 WHERE T5.ACICOD='14' AND T5.AALCOD IN ('0001') AND T5.ATRCOD IN ('0101', '0104', '0003', '0056', '0057', '0060', '0061', '0064', '0066') AND T5.ATRART=T1.AARCOD )
									ELSE 0
								  END
								ELSE 0 
								END
								) as SALIDAS
					FROM IDASYSW.IV05FP T1 
					WHERE T1.ACICOD='14'
					ORDER BY T1.AARDES";

					//echo $sql;

					$result=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111"));
				}
			?>
        <div id="content3" >   
        	<table width="100%"   border="0">
              <tr>
                <td width="65%" scope="col" colspan="2"><h1  class="title">Reporte de Inventario con Costos de Entrada y Salida - MEDIX</h1>
                  <hr /></td>
                <td width="13%" scope="col"><div align="left">
                  <table width="100%"  border="0">
                    <tr>
                      <th width="30%" scope="col" >
                      <div class="flechas">
                      	<a href="exportaraexcel_medix_costo_ent_sal.php?&aticod=<?php echo $aticod; ?>&stm=<?php echo $stm; ?>&tinvan=<?php echo $url222 = urlencode( serialize($tinva) );?>" target="_blank">
                        	<img src="../images/excel.jpg" alt="" width="30" height="30" title="Exportar a Excel"/>
                        </a>
                      </div></th>
                    </tr>
                  </table>
                </div></td>
              </tr>
              <tr>
              	<td colspan="3">
                	<form name="form" id="form" method="post" on>
                    <input type="hidden" name="bandera" id="bandera" value="<?php echo $bandera;?>" />
                	<table>
                    	<tr>
                        	<td>&nbsp;Desde:</td>
                            <td>&nbsp;<?php echo  escribe_formulario_fecha_vacio('desde','form',$desde); ?></td>
                            <td>&nbsp;</td>
                            <td>&nbsp;Hasta:</td>
                            <td>&nbsp;<?php echo  escribe_formulario_fecha_vacio('hasta','form',$hasta); ?></td>
                            <td rowspan="3"><a href="javascript:busqueda5();"><img src="../images/precesarconsulta.png" width="50" height="50" alt="Realizar Consulta" style="border:none;"></a></td>
                        </tr>
                        <tr>
                        	<td>&nbsp;Almac&eacute;n:</td>
                            <td>&nbsp;<?php //echo $aalcod; ?>
                            	<select name="aalcod" id="aalcod" onchange="cargarSelectTpInven(<?php ?>)" >
									<?php 
                                    //$sql="SELECT AALCOD, AALDES FROM iv07fp where acicod ='$Compania' order by AALDES ";
									if(accesotodasunisol_alma('4')=='S')
									{
										$sql="SELECT AALCOD, AALDES FROM iv07fp where acicod ='$Compania'  order by AALDES ";//and AALCOD <> '0017'
									}
									else
									{
										$sql = "SELECT T1.AALCOD, T2.AALDES FROM iv44fp T1, IV07FP T2 WHERE T1.acicod ='$Compania' AND T1.ACICOD=T2.ACICOD AND T1.auscod='$Usuario' AND T1.AALCOD= T2.AALCOD order by t2.AALDES";
									}
                                    $result1=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111")); 
									$select = '';
                                    while(odbc_fetch_row($result1)){
                                        $cod=trim(odbc_result($result1,1));
                                        $des=trim(odbc_result($result1,2));
										if(!empty($aalcod))
										{
											if($cod == $aalcod){
												$select = ' selected="selected" ';
											}
											else{
												$select = '';
											}
										}											
                                    ?>
                                        <option value= "<?php echo $cod; ?>" <?php echo $select?> ><?php echo $des."(".$cod.")"; ?></option>
                                    <?php } ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                        	<td>Tipo de Inventario</td>
                            <td colspan="4">
                            	<div id="table_tpinven">
                                </div>
                                <script>cargarSelectTpInven();</script>
                            </td>
                        </tr>
                    </table>
                    </form>
                    <hr />
                </td>
              </tr>
            </table>   
		<div id="container">
            <div id="demo">
                <table width="100%" id="info"  border="0" style="display:none;">
                  <thead>
                    <tr>
                    	<th scope="col" rowspan="2" style="background-color:rgb(204,204,204)">C&oacute;digo</th>
                   		<th scope="col" rowspan="2" style="background-color:rgb(204,204,204)">Art&iacute;culo</th>
                        <th scope="col" rowspan="2" style="background-color:rgb(204,204,204)">Costo</th>
                    	<th scope="col" colspan="2" style="background-color:rgb(204,204,204)">Entradas</th>
                    	<th scope="col" colspan="2" style="background-color:rgb(204,204,204)">Salidas</th>
                    	<th scope="col" colspan="2" style="background-color:rgb(204,204,204)">Saldo Final</th>
                    </tr>
                    <tr>
                        <th scope="col" style="background-color:rgb(204,204,204)"><strong>Cantidad</strong></th>
                        <th scope="col" style="background-color:rgb(204,204,204)"><strong>Costo Total $</strong></th>
                        <th scope="col" style="background-color:rgb(204,204,204)"><strong>Cantidad</strong></th>
                        <th scope="col" style="background-color:rgb(204,204,204)"><strong>Costo Total $</strong></th>
                        <th scope="col" style="background-color:rgb(204,204,204)"><strong>Existencia</strong></th>
                        <th scope="col" style="background-color:rgb(204,204,204)"><strong>Costo Total $</strong></th>
                    </tr>
                 </thead> 
                 <tbody>
        						<?php
        						if($bandera==1){
									while(odbc_fetch_row($result))
									{	
											?>
                                            <tr >
	                                                <td style="text-align:left;vertical-align:middle;"><?php echo trim(odbc_result($result, 'AARCOD')); ?></td>
                                                    <td style="text-align:left;vertical-align:middle;"><?php echo trim(odbc_result($result, 'AARDES')); ?></td>
                                                    <td style="text-align:center;vertical-align:middle;"><?php echo number_format(odbc_result($result, 'COSTO'),3,',','');?></td>	
                                                    <?php 
                                                    //Costo de Entradas
                                                    $costoEn =  odbc_result($result, 'COSTO')*odbc_result($result, 'ENTRADAS');

                                                    //Salidas restando devoluciones
                                                    $salidas = odbc_result($result, 'SALIDAS') - odbc_result($result, 'DEVOLUCIONES');

                                                    //Costo de Salidas
                                                    $costoSal = round(odbc_result($result, 'COSTO'))*$salidas;
                                                    ?>
                                                    <td style="text-align:center;vertical-align:text-top;">&nbsp;<?php echo number_format(odbc_result($result, 'ENTRADAS'),2,',','');?></td>
                                                    <td style="text-align:center;vertical-align:text-top;">&nbsp;<?php echo number_format($costoEn,3,',','');?></td>
                                                    <td style="text-align:center;vertical-align:text-top;">&nbsp;<?php echo number_format($salidas,2,',','');?></td>
                                                    <td style="text-align:center;vertical-align:text-top;">&nbsp;<?php echo number_format($costoSal,3,',','');?></td>
                                                    <?php 
                                                     //Existencia Actual
                                                     $exist = round(odbc_result($result, 'ENTRADAS')) - $salidas;

                                                     //Costo de Existencia Actual
                                                     $costoFin = $exist*number_format(odbc_result($result, 'COSTO'),2,',','');		
                                                    ?>
                                                    <td style="text-align:center;vertical-align:text-top;">&nbsp;<?php echo number_format($exist,2,',','');?></td>
                                                    <td style="text-align:center;vertical-align:text-top;">&nbsp;<?php echo number_format($costoFin,3,',','');?></td>
                                            </tr>
								<?php 
									}//fin while
								}//fin if
							    ?>
                       </tbody>                                         
                </table>
</div>

		  </div>
	  </div>
	</div>
		<!-- end #content -->
		<!-- end #sidebar -->
		<div style="clear: both;">&nbsp;</div>
	</div>
	<!-- end #page -->

	<div id="footer">
		<div id="opcionesmasusadas"><?php include("../opcionesmasusadasphp.php"); ?></div>
	</div>
 	<?php 
		echo "<script>
				var list = [];
				var pos = 1;
				var lista = \"".$tipoInve."\";
				$('input[name=\"tinvan[]\"]').each(function() { 
					//alert($(this).val() );
					if( lista.indexOf($(this).val()) != -1 ){
						//alert('paso '+$(this).val()) ;
						 $(this).attr('checked', true); 
					}
					pos++;
				});
			  </script>";
	?>
	<!-- end #footer -->
</body>
</html>
