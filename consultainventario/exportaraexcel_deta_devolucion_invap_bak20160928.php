<?php session_start();
include("../conectar.php");
$arqnro = trim($_GET["num"]);
//header("Pragma: ");
header("Pragma: no-cache");
header('Cache-control: ');
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Content-type: application/vnd.ms-excel");
//header("Content-type: application/octet-stream");
header("Content-disposition: attachment; filename=Reporte_devoluciones_INVAP_".formatDate($Fechaactual,'dd.mm.aaa','aaaa_mm_dd').".xls");

?>
 

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Reporte Cv</title>
</head>
<style>

h1, h2, h3, h4, h5 {
	margin: 0;
	padding: 0;
	font-weight: normal;
	color: #32639A;
}

h1 {
	font-size: 2em;
}

h2 {
	font-size: 2.4em;	
}

h3 {
	font-size: 1.6em;
	font-style: italic;
}
h4 {
	font-size: 1.6em;
	font-style: italic;
	color: #FFF;
}
h5 {
	font-size: 1.0em;
	font-style: italic;
	color: #666;
}

#background-image
{
	font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
	font-size: 10px;
	margin: 0px;
	width: 100%;
	text-align: left;
	border-collapse: collapse;
}
#background-image th
{
	padding: 12px;
	font-weight: normal;
	font-size: 12px;
	color: #339;
	border-bottom-style: solid;
	border-left-style: none;
	text-align: center;
}
#background-image td
{
	color: #669;
	border-top: 1px solid #fff;
	padding-right: 4px;
	padding-left: 4px;
}
#background-image tfoot td
{
	font-size: 9px;
}
#background-image tbody 
{

	background-repeat: no-repeat;
	background-position: left top;
}
#background-image tbody td
{
	background-image: url(images/backn.png);
}
* html #background-image tbody td
{
	/* 
	   ----------------------------
		PUT THIS ON IE6 ONLY STYLE 
		AS THE RULE INVALIDATES
		YOUR STYLESHEET
	   ----------------------------
	*/
	filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='images/backn.png',sizingMethod='crop');
	background: none;
}	
</style>
<body>
<?php 
						/*$sql="SELECT T3.AARUMB, T4.AUMDES, T1.ACICOD, T1.AARCOD, T1.AARNIV, T1.ATICOD, T1.ASICOD, T1.AARDES, T1.AARTDT, T1.AARSTS,
						(SELECT T6.ASLSAF FROM IV40FP T6 WHERE T6.ACICOD=T1.ACICOD AND T6.AARCOD=T1.AARCOD AND T6.AALCOD='0001' ORDER BY T6.ASLFEF DESC FETCH FIRST 1 ROWS ONLY) AS ASLSAF
							FROM IV05FP T1 
								INNER JOIN IV06FP T3 ON ( T1.ACICOD=T3.ACICOD AND T1.AARCOD=T3.AARCOD ) 
								INNER JOIN IV13FP T4 ON ( T1.ACICOD=T4.ACICOD AND T3.AARUMB=T4.AUMCOD ) 
							WHERE T1.ACICOD='$Compania' AND ( T1.AARCOD IN (SELECT T2.AARCOD FROM IV40FP T2 WHERE T2.ACICOD=T1.ACICOD AND T2.AALCOD='0001' GROUP BY T2.AARCOD ORDER BY T2.AARCOD ) ) 
							ORDER BY T1.AARDES";*/
						
						$sql = $cid->select("IV05FP T1 
								INNER JOIN IV06FP T3 ON ( T1.ACICOD=T3.ACICOD AND T1.AARCOD=T3.AARCOD ) 
								INNER JOIN IV13FP T4 ON ( T1.ACICOD=T4.ACICOD AND T3.AARUMB=T4.AUMCOD )", "T3.AARUMB, T4.AUMDES, T1.ACICOD, T1.AARCOD, T1.AARNIV, T1.ATICOD, T1.ASICOD, T1.AARDES, T1.AARTDT, T1.AARSTS,
						(SELECT T6.ASLSAF FROM IV40FP T6 WHERE T6.ACICOD=T1.ACICOD AND T6.AARCOD=T1.AARCOD AND T6.AALCOD='0001' ORDER BY T6.ASLFEF DESC FETCH FIRST 1 ROWS ONLY) AS ASLSAF", "T1.ACICOD='$Compania' AND ( T1.AARCOD IN (SELECT T2.AARCOD FROM IV40FP T2 WHERE T2.ACICOD=T1.ACICOD AND T2.AALCOD='0001' GROUP BY T2.AARCOD ORDER BY T2.AARCOD ) ) ", "", "T1.AARDES");


						$resultt = $sql->fetchAll(PDO::FETCH_ASSOC);
						
						$z=0;
			
						foreach ($resultt as $fields){
							foreach ($fields as $val => $valorinterno) {
								$row[$z][$val] = ICONV("ISO-8859-1", "UTF-8", trim($valorinterno));
							}
							$z++;
						}
						$_SESSION['solicitudarreglo']=$row;
					$paginat=$_SESSION['solicitudarreglo'];
			?>      
<table width="100%" border="0">
<tr>
<td height="89"><h1>
<?php if($Compania=='14'){?>
	<img src="http://<?php echo $Direccionip; ?>/idasysv3/images/logoidacadef2005.png" width="280" height="68" />
<?php }else if($Compania=='40'){?>
	<img src="http://<?php echo $Direccionip; ?>/idasysv3/images/MEDITRON_logo_rif_2.png" width="300" />
<?php }else{ ?>
	<img src="http://<?php echo $Direccionip; ?>/idasysv3/images/logomeditronnuevo.png" width="280" height="68" />
<?php } ?>
   </h1>
  <h5>RIF:  <?php echo $Companiarif; ?></h5></td>
</tr>
<tr>
<td>

<table width="100%" id="background-image" >
	
  <thead>
  	<tr>
        <th colspan="7" scope="col"><h1>REPORTE - INVAP</h1></th>
    </tr>
  	<tr>
        <th colspan="7" scope="col"><h2>Detalle de Devolución</h2></th>
    </tr>
    <tr>
        <th colspan="7" scope="col">Elaborado el <?php echo $Fechaactual; ?> a las <?php echo $Horaactual2; ?></th>
    </tr>
  	<tr>
        <th colspan="7" scope="col">Almacén Principal: INVAP</th>
    </tr>

    				<tr style="border-bottom:solid;">
                    	<th scope="col" rowspan="2" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)">Código</th>
                   		<th scope="col" rowspan="2" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)">Artículo</th>
                        <th scope="col" rowspan="2" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)"><strong>Número Referencia Entrada</strong></th>
                    	<th scope="col" colspan="3" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204); font-size:medium;">Entradas</th>
                        <th scope="col" rowspan="2" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;border-right:solid;background-color:rgb(204,204,204)">Existencia Actual</th>
                    </tr>
                    <tr>
                        <th scope="col" style="border-width:thin;border-bottom:solid;border-left:solid;solid;background-color:rgb(204,204,204)"><strong>Fecha</strong></th>
                        <th scope="col" style="border-width:thin;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)"><strong>Cantidad</strong></th>
                        <th scope="col" style="border-width:thin;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)"><strong>Observaciones</strong></th>
                    </tr>
                  </thead>
    			  <tbody>
        						<?php
									$pagact=$solicitudpagina;
									$part= 1;
									$paso = 0;
									for($g=0; $g < (count($paginat)); $g++)
									{
											$fondo ="background-color:#E0F8F7;";
											
											/*entradas*/
											 $sql2="SELECT T3.ATRNUM, T3.ATRCOD, T2.ATRDES, T3.ATRFEC, T1.ATRCAN, (T3.ATRDES) AS DESATR, T3.ATROBS, T5.ADSNRO, T5.ATSCOD
													FROM IV16FP T1 
														INNER JOIN IV15FP T3 ON (T1.ACICOD=T3.ACICOD AND T1.ATRCOD=T3.ATRCOD AND T1.ATRNUM=T3.ATRNUM AND T1.ADPCOD=T3.ADPCOD ) 
														INNER JOIN iv12fp T2 ON(T1.ACICOD= T2.ACICOD AND T1.ATRCOD=T2.ATRCOD) 
														LEFT JOIN IV35FP T5 ON (T1.ACICOD=T5.ACICOD AND T1.AALCOD=T5.AALCOD AND T3.ADPCOD=T5.ADPCOD AND T1.ATRCOD=T5.ATRCOD AND T1.ATRNUM=T5.ATRNUM)
													WHERE T1.ACICOD='$Compania' AND T1.AALCOD='0001' AND T1.ATRCOD in ( '0003') AND  T1.ATRART='".$paginat[$g]["AARCOD"]."' 
													ORDER BY T3.ATRFEC";
											
											 $query = $cid->queryAll($sql2);
											$result2 = $query->fetchAll(PDO::FETCH_ASSOC);
											$canEnt=0;
											foreach($result2 as $entradas){
												$canEnt++;
											}
											
											$numRows = '';
											$numRows = $canEnt;
											
											$rowpos = 0;
											$count2 = 0;

											if($numRows>$rowpos){
												$paso++;
												if( ($paso%2) == 0){$fondo ="background-color:#E0F8F7;";}
												else{$fondo ="";}
                                 ?>     
                                        <tr >
	                                                <td style=" <?php echo $fondo;?>text-align:left;vertical-align:middle;" rowspan="<?php echo $numRows; ?>"><?php echo "&nbsp;".$paginat[$g]["AARCOD"]; ?></td>
                                                    <td style=" <?php echo $fondo;?>text-align:left;vertical-align:middle;" rowspan="<?php echo $numRows; ?>"><?php echo $paginat[$g]["AARDES"]; ?></td>
    
                                 <?php
											}
											 if($numRows>$rowpos){
											 while($numRows>$rowpos){   
											 
											?>
                                            <?php
											
											     $rowpos++;       
												if($rowpos<=$canEnt){	
												?>
                                                <!-- entradas -->
                                                <?php 
													if($result2[$count2]['ATRCOD']=='0003'){
														?>
                                                        <td style=" <?php echo $fondo;?>text-align:center;vertical-align:middle;"><?php echo $result2[$count2]['ADSNRO']!=''?add_ceros($result2[$count2]['ADSNRO'],6):'&nbsp;'; echo "<strong>(D)</strong>";?></td>
                                                        <?php
													}else{
														?>
                                                        <td style=" <?php echo $fondo;?>text-align:center;vertical-align:middle;"><?php echo $result2[$count2]['ATRNUM']!=''?add_ceros($result2[$count2]['ATRNUM'],6):'&nbsp;'; echo '(E)';?></td>
                                                        <?php
													}
												
									?>
                                                    <td style=" <?php echo $fondo;?>text-align:center;vertical-align:middle;"><?php echo $result2[$count2]['ATRFEC']!=''?formatDate($result2[$count2]['ATRFEC'],'aaaa-mm-dd','dd/mm/aaaa' ):'&nbsp;';?></td>
                                                    <td style=" <?php echo $fondo;?>text-align:center;vertical-align:middle;"><?php echo $result2[$count2]['ATRCAN']!=''?number_format($result2[$count2]['ATRCAN'],0):'&nbsp;';?></td>
                                                    <td style=" <?php echo $fondo;?>text-align:center;vertical-align:middle;"><?php echo $result2[$count2]['ATROBS']!=''?$result2[$count2]['ATROBS']:'&nbsp;';?></td>
                                 <?php 			}else{	?>
                                					<td style=" <?php echo $fondo;?>text-align:center;vertical-align:middle;">--</td>
                                                    <td style=" <?php echo $fondo;?>text-align:center;vertical-align:middle;">--</td>
                                                    <td style=" <?php echo $fondo;?>text-align:center;vertical-align:middle;">--</td>
                              <?php 
                                            	 }
											 	 if($rowpos == 1){
							?> 
                                                   <td style=" <?php echo $fondo;?>text-align:center;vertical-align:middle;" rowspan="<?php echo $numRows; ?>"><?php echo $paginat[$g]["ASLSAF"]!=''?number_format($paginat[$g]["ASLSAF"],0):"--";?></td>
                              <?php } ?>           
 		                         
                                            </tr>
                               <?php		$count2++;
                               				}//fin while(odbc_fetch_row($resultES))
											}//if($numRows>$rowpos){
									}	?>     
                       </tbody>  

</table>
</td>
</tr>
</table>
</body>
</html>
