<?php session_start();
include("../conectar.php");
$arqnro = trim($_GET["num"]);
$tinva = unserialize( urldecode( $_GET['tinvan'] ) ) ;
//header("Pragma: ");
header("Pragma: no-cache");
header('Cache-control: ');
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Content-type: application/vnd.ms-excel");
//header("Content-type: application/octet-stream");
header("Content-disposition: attachment; filename=Reporte_INVAP_Hospital_".formatDate($Fechaactual,'dd.mm.aaa','aaaa_mm_dd').".xls");

?>
 

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Reporte</title>
</head>
<style>

h1, h2, h3, h4, h5 {
	margin: 0;
	padding: 0;
	font-weight: normal;
	color: #32639A;
}

h1 {
	font-size: 2em;
}

h2 {
	font-size: 2.4em;	
}

h3 {
	font-size: 1.6em;
	font-style: italic;
}
h4 {
	font-size: 1.6em;
	font-style: italic;
	color: #FFF;
}
h5 {
	font-size: 1.0em;
	font-style: italic;
	color: #666;
}

#background-image
{
	font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
	font-size: 10px;
	margin: 0px;
	width: 100%;
	text-align: left;
	border-collapse: collapse;
}
#background-image th
{
	padding: 12px;
	font-weight: normal;
	font-size: 12px;
	color: #339;
	border-bottom-style: solid;
	border-left-style: none;
	text-align: center;
}
#background-image td
{
	color: #669;
	border-top: 1px solid #fff;
	padding-right: 4px;
	padding-left: 4px;
}
#background-image tfoot td
{
	font-size: 9px;
}
#background-image tbody 
{

	background-repeat: no-repeat;
	background-position: left top;
}
#background-image tbody td
{
	background-image: url(images/backn.png);
}
* html #background-image tbody td
{
	/* 
	   ----------------------------
		PUT THIS ON IE6 ONLY STYLE 
		AS THE RULE INVALIDATES
		YOUR STYLESHEET
	   ----------------------------
	*/
	filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='images/backn.png',sizingMethod='crop');
	background: none;
}	
</style>
<body>
<?php 
				
				
		 		$wsolicitud=0;
				if ($solicitudpagina==0) 	
				{
						
						$z=0;
						$tothon=0;
						$totest=0;
						$lin=1;
						$limitep=$_SESSION['solicitudlineasporpaginat'];
						$pag=1;
						$primero='S';
						
						$tipoInve ='';
						if(!empty($tinva)){
							foreach($tinva as $key => $value){
								$tipoInve .= "'".$value."',";
							}
							$tipoInve = substr($tipoInve,0, (strripos($tipoInve,",")));
						}
						
						$sql="SELECT T2.ADSNRO, T3.ATSCOD, T3.ATSDES, T2.ATRFEC, T1.ATRCAN, T1.AARCOD, T5.AARDES, T2.ATROBS, T4.AISCOD, T4.AISDES,
								(SELECT T6.AAPVLA FROM IV46FP T6 WHERE T6.ACICOD=T1.ACICOD and T6.ADSNRO=T2.ADSNRO AND T6.APACOD IN ('1501') ) AS N1501,
								(SELECT T6.AAPVLA FROM IV46FP T6 WHERE T6.ACICOD=T1.ACICOD and T6.ADSNRO=T2.ADSNRO AND T6.APACOD IN ('1510') ) AS N1510,
								(SELECT T6.AAPVLA FROM IV46FP T6 WHERE T6.ACICOD=T1.ACICOD and T6.ADSNRO=T2.ADSNRO AND T6.APACOD IN ('1509') ) AS N1509 
							FROM IV36FP T1 
								INNER JOIN IV05FP T5 ON (T1.ACICOD=T5.ACICOD AND T1.AARCOD=T5.AARCOD)
								INNER JOIN IV35FP T2 ON (T1.ACICOD=T2.ACICOD AND T1.AALCOD= T2.AALCOD AND T1.ADPCOD=T2.ADPCOD AND T1.ATSCOD= T2.ATSCOD AND T1.ADSNRO=T2.ADSNRO) 
								INNER JOIN IS15FP T3 ON (T1.ACICOD=T3.ACICOD AND T2.ATSCOD=T3.ATSCOD) 
								INNER JOIN IV42FP T4 ON (T2.ACICOD=T4. ACICOD AND T2.AISCOD=T4.AISCOD) 
							WHERE T1.ACICOD='$Compania' AND 
								  T1.AALCOD not in ('0006', '0007', '0001') AND 
								  T3.ATSCOD in ('01','04') AND
								  T2.ATRFEC BETWEEN '$desde' AND '$hasta'
							";
						if(!empty($tipoInve)){
							$sql.="  AND T5.ATICOD IN (".$tipoInve.")";
						}
						$sql.=" ORDER BY T4.AISDES, T2.ATRFEC ";
						
						//echo $sql."<br/><br/>";
						$resultt=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111"));
						
							while(odbc_fetch_row($resultt))
							{
								$jml = odbc_num_fields($resultt);
								$row[$z]["pagina"] =  $pag;
								for($i=1;$i<=$jml;$i++)
								{	
									$row[$z][odbc_field_name($resultt,$i)] =  odbc_result($resultt,$i);
								}
								$z++;
								if ($lin>=$limitep) 
								{
									$limitep+=$_SESSION['solicitudlineasporpaginat'];
									$pag++;
								}
								$lin++;
							}

						$totsol=($lin-1);
						$_SESSION['totalsolicitudes']=$totsol;
						$_SESSION['solicitudarreglo']=$row;
						$solicitudpagina=1;
						$_SESSION['solicitudpaginas']=$pag;
					}//fin de solicitudpagina
					/*se muestra la cantidad de elementos segun la solicitada en $solicitudpagina*/
					$paginat=$_SESSION['solicitudarreglo'];
			?>      
<table width="100%" border="0">
<tr>
<td height="89"><h1>
<?php if($Compania=='14'){?>
	<img src="http://<?php echo $Direccionip; ?>/idasysv3/images/logoidacadef2005.png" width="280" height="68" />
<?php }else if($Compania=='40'){?>
	<img src="http://<?php echo $Direccionip; ?>/idasysv3/images/MEDITRON_logo_rif.png" width="300" />
<?php }else{ ?>
	<img src="http://<?php echo $Direccionip; ?>/idasysv3/images/logomeditronnuevo.png" width="280" height="68" />
<?php } ?>
   </h1>
  <h5>RIF:  <?php echo $Companiarif; ?></h5></td>
</tr>
<tr>
<td>

<table width="100%" id="background-image" >
	
  <thead>
  	<tr>
        <th colspan="9" scope="col"><h1>REPORTE - INVAP</h1></th>
    </tr>
  	<tr>
        <th colspan="9" scope="col"><h2>Salida por Hospital</h2></th>
    </tr>
    <tr>
        <th colspan="9" scope="col">Elaborado el <?php echo $Fechaactual; ?> a las <?php echo $Horaactual2; ?></th>
    </tr>
    <tr>
        <th colspan="9" scope="col"><strong>Desde:</strong> <?php echo $desde;?> ; <strong>Hasta:</strong>  <?php echo $hasta;?></th>
    </tr>

    <?php if(!empty($tinva)){ ?>
        <tr>
            <th colspan="14" scope="col">
                <strong>Filtrado por:</strong>
                <?php 
							$tipoInve ='';
							if(!empty($tinva)){
								foreach($tinva as $key => $value){
									$tipoInve .= "'".$value."',";
								}
								$tipoInve = substr($tipoInve,0, (strripos($tipoInve,",")));
							}
							
							$sql2="SELECT T6.ATIDES
							FROM IV01FP T6 
							WHERE T6.ACICOD='$Compania'  AND T6.ATICOD in (".$tipoInve.") ";
							
							$result2=odbc_exec($cid,$sql2)or die(exit("Error en odbc_exec 11111")); 
							while(odbc_fetch_row($result2))
							{
								echo odbc_result($result2,'ATIDES').", ";
							}
				
				;?>
            </th>
        </tr>
    <?php } ?>

    				<tr style="border-bottom:solid;">
                   		<th scope="col" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)">Hospital</th>
                        <th scope="col" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)">Fecha</th>
                        <th scope="col" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)"><strong>Referencia de Salida</strong></th>
                        <th scope="col" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)"><strong>Cantidad</strong></th>
                    	<th scope="col" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204);">C�digo Art�culo</th>
                        <th scope="col" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204);">Art�culo</th>
                        <th scope="col" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)"><strong>Nro. Hoja de Servicio</strong></th>
                        <th scope="col" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204);">N�mero de ITEM</th>
                        <th scope="col" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;border-right:solid;background-color:rgb(204,204,204)">ITEM</th>
                    </tr>
                  </thead>
    				<tbody>
        						<?php
									//print_r($paginat);
									$pagact=$solicitudpagina;
									$part= 1;
									for($g=0; $g < (count($paginat)); $g++)
									{					
                                 ?>
										<tr >
                                            <td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:left;vertical-align:middle;"><?php echo $paginat[$g]['AISDES']!=''?$paginat[$g]['AISDES']:'&nbsp;';?></td>
                                            <td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:left;vertical-align:middle;"><?php echo $paginat[$g]['ATRFEC']!=''?formatDate($paginat[$g]['ATRFEC'],'aaaa-mm-dd','dd/mm/aaaa' ):'&nbsp;';?></td>
                                            <td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:left;vertical-align:middle;"><?php echo $paginat[$g]['ADSNRO']!=''?add_ceros($paginat[$g]['ADSNRO'],6):'&nbsp;'; echo "(S)";?></td>
                                            <td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:right;vertical-align:middle;"><?php echo $paginat[$g]['ATRCAN']!=''?number_format($paginat[$g]['ATRCAN'],0,',','.'):'&nbsp;';?></td>
                                            <td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:left;vertical-align:middle;">&nbsp;<?php echo $paginat[$g]['AARCOD']!=''?$paginat[$g]['AARCOD']:'&nbsp;';?></td>
                                            <td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:left;vertical-align:middle;"><?php echo $paginat[$g]['AARDES']!=''?$paginat[$g]['AARDES']:'&nbsp;';?></td>
                                            <td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:right;vertical-align:middle;"><?php echo $paginat[$g]['N1501']!=''?$paginat[$g]['N1501']:'--';?></td>
                                            <td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:right;vertical-align:middle;"><?php echo $paginat[$g]['N1510']!=''?$paginat[$g]['N1510']:'--';?></td>
                                            <td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:left;vertical-align:middle;"><?php echo $paginat[$g]['N1509']!=''?$paginat[$g]['N1509']:'--';?></td>
                                        </tr>	
								<?php																						
									}
								?>                                            
                  </tbody>

</table>
</td>
</tr>
</table>
</body>
</html>
