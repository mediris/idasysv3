<?php 

include("../conectar.php");
$arqnro = trim($_GET["num"]);
$tinva = unserialize( urldecode( $_GET['tinvan'] ) ) ;
//header("Pragma: ");
header("Pragma: no-cache");
header('Cache-control: ');
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Content-type: application/vnd.ms-excel");
header("Content-type: application/octet-stream");
header("Content-disposition: attachment; filename=Reporte_MEDI_INVAP_".formatDate($Fechaactual,'dd.mm.aaa','aaaa_mm_dd').".xls");


session_start();
include_once("../conectar.php");

$dsn="idasysv3";
$Usuario = "CORREO";
$Contrasena = "CORREO";
$Compania = 40;

$cid=odbc_connect($dsn,$Usuario,$Contrasena);

date_default_timezone_set("America/Caracas");
$Fechaactual=date("d").".".date("m").".".date("Y");
$Horaactual2=date(H).":".date(i).":".date(s)." ".date(a);
$Direccionip = '192.168.1.107';
$Companiarif= "J-000814660";

//header("Pragma: ");

		 		$wsolicitud=0;
				if ($solicitudpagina==0) 	
				{
						
						$z=0;
						$tothon=0;
						$totest=0;
						$lin=1;
						$limitep=$_SESSION['solicitudlineasporpaginat'];
						$pag=1;
						$primero='S';
						
						$sql="SELECT T3.AARUMB, T4.AUMDES, T1.ACICOD, T1.AARCOD, T1.AARNIV, T1.ATICOD, T1.ASICOD, T1.AARDES, T1.AARTDT, T1.AARSTS,
						(SELECT T6.ASLSAF FROM IV40FP T6 WHERE T6.ACICOD=T1.ACICOD AND T6.AARCOD=T1.AARCOD AND T6.AALCOD='0001' ORDER BY T6.ASLFEF DESC FETCH FIRST 1 ROWS ONLY) AS ASLSAF
							FROM IV05FP T1 
								INNER JOIN IV06FP T3 ON ( T1.ACICOD=T3.ACICOD AND T1.AARCOD=T3.AARCOD ) 
								INNER JOIN IV13FP T4 ON ( T1.ACICOD=T4.ACICOD AND T3.AARUMB=T4.AUMCOD ) 
							WHERE T1.ACICOD='$Compania' AND ( T1.AARCOD IN (SELECT T2.AARCOD FROM IV40FP T2 WHERE T2.ACICOD=T1.ACICOD AND T2.AALCOD='0001' GROUP BY T2.AARCOD ORDER BY T2.AARCOD ) ) 
							ORDER BY T1.AARDES";
						
						//echo $sql."<br/><br/>";
						$resultt=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111"));
						
							while(odbc_fetch_row($resultt))
							{
								$jml = odbc_num_fields($resultt);
								$row[$z]["pagina"] =  $pag;
								for($i=1;$i<=$jml;$i++)
								{	
									$row[$z][odbc_field_name($resultt,$i)] =  odbc_result($resultt,$i);
								}
								$z++;
								if ($lin>=$limitep) 
								{
									$limitep+=$_SESSION['solicitudlineasporpaginat'];
									$pag++;
								}
								$lin++;
							}
							$totsol=($lin-1);
							$_SESSION['totalsolicitudes']=$totsol;
							$_SESSION['solicitudarreglo']=$row;
							$solicitudpagina=1;
							$_SESSION['solicitudpaginas']=$pag;
					}//fin de solicitudpagina
					$paginat=$_SESSION['solicitudarreglo'];
				
			?>      
			<table>
					
                    <tr>
						<th >
							<img src="http://<?php echo $Direccionip; ?>/idasysv3/images/MEDITRON_logo_rif.png" width="300"/>
							<h5>RIF: <?php echo $Companiarif;?> </h5>
						 </th>
					</tr>
           </table>
           <table align="center" >
					<tr>
						<th style="text-align:center">
                        	<h1>REPORTE - INVAP</h1>
                            <h2>Entrada y Salida de Repuesto y Equipos</h2>
                            Elaborado el <?php echo $Fechaactual; ?> a las <?php echo $Horaactual2; ?><br />
                            Almac&eacute;n Principal: INVAP
                        </th>
					</tr>
            </table>
			<table style="font-size:9px;" border="0.5px" cellpadding="0" cellspacing="0" align="center">
                    <thead>
    				<tr style="border-bottom:solid;border-width:thick;border-color:#CCCCCC;">
                   		<th rowspan="2" style="background-color:rgb(204,204,204);text-align:center;">Art&iacute;culo</th>
                        <th rowspan="2" style="background-color:rgb(204,204,204);text-align:center;"><strong>N&uacute;mero<br />Referencia<br />Entrada</strong></th>
                    	<th colspan="3" style="background-color:rgb(204,204,204);text-align:center;font-size:medium;">Entradas</th>
                        <th rowspan="2" style="background-color:rgb(204,204,204);text-align:center;"><strong>N&uacute;mero<br />Referencia<br />Salidas</strong></th>
                        <th colspan="5" style="background-color:rgb(204,204,204);text-align:center;font-size:medium;">Salidas</th>
                        <th rowspan="2" style="background-color:rgb(204,204,204)">Existencia<br />Actual</th>
                    </tr>
                    <tr>
                        <th style="background-color:rgb(204,204,204);text-align:center;"><strong>Fecha</strong></th>
                        <th style="background-color:rgb(204,204,204);text-align:center;"><strong>Cantidad</strong></th>
                        <th style="background-color:rgb(204,204,204);text-align:center;"><strong>Observaciones</strong></th>
                        
                        <th style="background-color:rgb(204,204,204);text-align:center;"><strong>Fecha</strong></th>
                        <th style="background-color:rgb(204,204,204);text-align:center;"><strong>Cantidad</strong></th>
                        <th style="background-color:rgb(204,204,204);text-align:center;"><strong>Destino</strong></th>
                        <th style="background-color:rgb(204,204,204);text-align:center;"><strong>Observaciones</strong></th>
                        <th style="background-color:rgb(204,204,204);text-align:center;"><strong>Nro. Hoja<br />de Servicio</strong></th>
                    </tr>
					</thead>
                    
                    
        						<?php
									//print_r($paginat);
									$pagact=$solicitudpagina;
									$part= 1;
									for($g=0; $g < (count($paginat)); $g++)
									{
											
											if( ($g%2) == 0){$fondo ="background-color:#E0F8F7;";}
											else{$fondo ="";}
											
											
											//echo "//**".$paginat[$g]["AARCOD"]."<br>";
											/*entradas*/
											/* $sql2="SELECT T3.ATRNUM, T3.ATRCOD, T2.ATRDES, T3.ATRFEC, T1.ATRCAN, (T3.ATRDES) AS DESATR, T3.ATROBS 
														FROM IV16FP T1 
															INNER JOIN IV15FP T3 ON (T1.ACICOD=T3.ACICOD AND T1.ATRCOD=T3.ATRCOD AND T1.ATRNUM=T3.ATRNUM ) 
															INNER JOIN iv12fp T2 ON(T1.ACICOD= T2.ACICOD AND T1.ATRCOD=T2.ATRCOD) 
														WHERE T1.ACICOD='$Compania' AND T1.AALCOD='0001' AND T1.ATRCOD in ( '0001', '0003', '0004', '0006', '0101' ) AND  T1.ATRART='".$paginat[$g]["AARCOD"]."'
														ORDER BY T3.ATRFEC ";
											*/
											/*entradas*/
											 $sql2="SELECT T3.ATRNUM, T3.ATRCOD, T2.ATRDES, T3.ATRFEC, T1.ATRCAN, (T3.ATRDES) AS DESATR, T3.ATROBS, T5.ADSNRO, T5.ATSCOD
													FROM IV16FP T1 
														INNER JOIN IV15FP T3 ON (T1.ACICOD=T3.ACICOD AND T1.ATRCOD=T3.ATRCOD AND T1.ATRNUM=T3.ATRNUM AND T1.ADPCOD=T3.ADPCOD ) 
														INNER JOIN iv12fp T2 ON(T1.ACICOD= T2.ACICOD AND T1.ATRCOD=T2.ATRCOD) 
														LEFT JOIN IV35FP T5 ON (T1.ACICOD=T5.ACICOD AND T1.AALCOD=T5.AALCOD AND T3.ADPCOD=T5.ADPCOD AND T1.ATRCOD=T5.ATRCOD AND T1.ATRNUM=T5.ATRNUM)
													WHERE T1.ACICOD='$Compania' AND T1.AALCOD='0001' AND T1.ATRCOD in ( '0001', '0003', '0004', '0006', '0101' ) AND  T1.ATRART='".$paginat[$g]["AARCOD"]."' 
													ORDER BY T3.ATRFEC";
											//echo $sql2."<br/><br/>";
											$result2=odbc_exec($cid,$sql2)or die(exit("Error en odbc_exec 11111"));
											$result22=odbc_exec($cid,$sql2)or die(exit("Error en odbc_exec 11111"));
											$canEnt=0;
											//$canEnt = odbc_num_rows($result22);
											while(odbc_fetch_row($result22)){
												$canEnt++;
											}
											//echo "<br>"."*".$canEnt;
											
											/*salidas*/
											$sql3="SELECT T2.ADSNRO, T3.ATSCOD, T3.ATSDES, T2.ATRFEC, T1.ATRCAN, T2.ATROBS, T4.AISCOD, T4.AISDES,
													(SELECT T6.AAPVLA FROM IV46FP T6 WHERE T6.ACICOD=T1.ACICOD and T6.ADSNRO=T2.ADSNRO AND T6.APACOD IN ('1501') ) AS N1501 
														FROM IV36FP T1 
															INNER JOIN IV35FP T2 ON (T1.ACICOD=T2.ACICOD AND T1.AALCOD= T2.AALCOD AND T1.ADPCOD=T2.ADPCOD AND T1.ATSCOD= T2.ATSCOD AND T1.ADSNRO=T2.ADSNRO) 
															INNER JOIN IS15FP T3 ON (T1.ACICOD=T3.ACICOD AND T2.ATSCOD=T3.ATSCOD) 
															INNER JOIN IV42FP T4 ON (T2.ACICOD=T4.ACICOD AND T2.AISCOD=T4.AISCOD) 
														WHERE T1.ACICOD='$Compania' AND T1.AARCOD= '".$paginat[$g]["AARCOD"]."' AND T1.AALCOD not in ('0006') AND T3.ATSCOD in ('01','04') ORDER BY T2.ATRFEC ";
											
											//echo $sql3."<br/><br/>";
											$result3=odbc_exec($cid,$sql3)or die(exit("Error en odbc_exec 11111"));
											$result33=odbc_exec($cid,$sql3)or die(exit("Error en odbc_exec 11111"));
											//echo "<br>"."+".$canSal = odbc_num_rows($result3);
											$canSal=0;
											//$canEnt = odbc_num_rows($result22);
											while(odbc_fetch_row($result33)){
												$canSal++;
											}
											//echo "<br>"."*".$canSal;
											
											
											$resultES = '';
											$numRows = '';
											if($canSal>$canEnt){
												$resultES = $result3;
												$numRows = $canSal;
												//echo "<br>"."* salida";
											}else if($canSal<=$canEnt){
												$resultES = $result2;
												$numRows = $canEnt;
												//echo "<br>"."* entrada";
											}
											$rowpos = 0;
											//echo "numRows:".$numRows;
											//echo "rowpos:".$rowpos;				
                                 ?>           
                                               
                                 <?php
                                             //while(odbc_fetch_row($resultES)){   
											 if($numRows>$rowpos){
												 $canpas = 0;
											 while($numRows>$rowpos){   
											 	$canpas++;
											//echo "numRows:".$numRows;
											//echo "rowpos:".$rowpos;
												
											          $rowpos++;       
													  odbc_fetch_row($result2);
													  odbc_fetch_row($result3);      
													  
													if($rowpos>'1')
													{
														echo "<tr>";
													}else{
														$lineaGruesa = 'border-top-width:2px;';
														echo ' <tr>
														<td style="'.$lineaGruesa.$fondo.';text-align:left;vertical-align:middle;" valign="middle" rowspan="'.$numRows.'">'.wordwrap($paginat[$g]["AARDES"],20,"<br />", true).'</td>';
													}                               
												if($rowpos<=$canEnt){
													if($rowpos==1){$lineaGruesa = 'border-top-width:2px;';}
													else{$lineaGruesa = '';}
													?>
                                                <!-- entradas -->
                                                <?php 
													if(odbc_result($result2, 'ATRCOD')=='0003'){
														?>
                                                        <td style=" <?php echo $lineaGruesa.$fondo; ?> ;text-align:center;vertical-align:middle;" valign="middle">&nbsp;<?php echo odbc_result($result2, 'ADSNRO')!=''?add_ceros(odbc_result($result2, 'ADSNRO'),6):'&nbsp;'; echo "<strong>(D)</strong>";?></td>
                                                        <?php
													}else{
														?>
                                                        <td style=" <?php echo $lineaGruesa.$fondo; ?> ;text-align:center;vertical-align:middle;" valign="middle">&nbsp;<?php echo odbc_result($result2, 'ATRNUM')!=''?add_ceros(odbc_result($result2, 'ATRNUM'),6):'&nbsp;'; echo '(E)';?></td>
                                                        <?php
													}	
								?>
                                                    <td style=" <?php echo $lineaGruesa.$fondo; ?>text-align:center;vertical-align:middle;" valign="middle"><?php echo odbc_result($result2, 'ATRFEC')!=''?formatDate(odbc_result($result2, 'ATRFEC'),'aaaa-mm-dd','dd/mm/aaaa' ):'&nbsp;';?></td>
                                                    <td style=" <?php echo $lineaGruesa.$fondo; ?>text-align:center;vertical-align:middle; background-color:rgb(0,153,255)" valign="middle"><?php echo odbc_result($result2, 'ATRCAN')!=''?number_format(odbc_result($result2, 'ATRCAN'),0):'&nbsp;';?></td>
                                                    <td style=" <?php echo $lineaGruesa.$fondo; ?>text-align:center;vertical-align:middle;" valign="middle"><?php echo wordwrap(trim(odbc_result($result2, 'ATROBS')!=''?odbc_result($result2, 'ATROBS'):'&nbsp;'),30,"<br />", true);?></td>
                                 <?php 
                                             	}else{
                                            // while(odbc_fetch_row($resultES)){                     
								?>
                                					<!-- entradas -->
                                                    <td style=" <?php echo $lineaGruesa.$fondo; ?>border-left:solid;">&nbsp;</td>
                                                    <td style=" <?php echo $lineaGruesa.$fondo; ?>">&nbsp;</td>
                                                    <td style=" <?php echo $lineaGruesa.$fondo; ?>background-color:rgb(0,153,255)">&nbsp;</td>
                                                    <td style=" <?php echo $lineaGruesa.$fondo; ?>">&nbsp;</td>
                              <?php 
                                            	 }
											 
											 	if($rowpos <= $canSal){
 								?>                      
                                                    <!-- salida -->
                                                    <td style=" <?php echo $lineaGruesa.$fondo; ?>text-align:center;vertical-align:middle;" valign="middle">&nbsp;<?php echo odbc_result($result3, 'ADSNRO')!=''?add_ceros(odbc_result($result3, 'ADSNRO'),6):'&nbsp;'; echo "(S)";?></td>
                                                    <td style=" <?php echo $lineaGruesa.$fondo; ?>text-align:center;vertical-align:middle;" valign="middle"><?php echo odbc_result($result3, 'ATRFEC')!=''?formatDate(odbc_result($result3, 'ATRFEC'),'aaaa-mm-dd','dd/mm/aaaa' ):'&nbsp;';?></td>
                                                    <td style=" <?php echo $lineaGruesa.$fondo; ?>text-align:center;vertical-align:middle;background-color:rgb(255,255,153)" valign="middle"><?php echo odbc_result($result3, 'ATRCAN')!=''?number_format(odbc_result($result3, 'ATRCAN'),0):'&nbsp;';?></td>
                                                    <td style=" <?php echo $lineaGruesa.$fondo; ?>text-align:center;vertical-align:middle;" valign="middle"><?php echo wordwrap(odbc_result($result3, 'AISDES')!=''?odbc_result($result3, 'AISDES'):'&nbsp;',25,"<br />", true);?></td>
                                                    <td style=" <?php echo $lineaGruesa.$fondo; ?>text-align:center;vertical-align:middle;" valign="middle"><?php echo wordwrap(odbc_result($result3, 'ATROBS')!=''?odbc_result($result3, 'ATROBS'):'&nbsp;',30,"<br />", true);?></td>
                                                    <td style=" <?php echo $lineaGruesa.$fondo; ?>text-align:center;vertical-align:middle;" valign="middle"><?php echo odbc_result($result3, 'N1501')!=''?odbc_result($result3, 'N1501'):'--';?></td>
                                <?php 
												}else{
								?>
                                					<!-- salida -->
                                                    <td style=" <?php echo $lineaGruesa.$fondo; ?>">&nbsp;</td>
                                                    <td style=" <?php echo $lineaGruesa.$fondo; ?>">&nbsp;</td>
                                                    <td style=" <?php echo $lineaGruesa.$fondo; ?>background-color:rgb(255,255,153)">&nbsp;</td>
                                                    <td style=" <?php echo $lineaGruesa.$fondo; ?>">&nbsp;</td>
                                                    <td style=" <?php echo $lineaGruesa.$fondo; ?>">&nbsp;</td>
                                                    <td style=" <?php echo $lineaGruesa.$fondo; ?>">&nbsp;</td>
                              <?php 
                                             	}
												
												
											 
                                            	if(  trim($rowpos)!= '1' )
												{
													echo '</tr>';
													
                                            	}//fin if  if($rowpos == 1)
												
												else 
												{
													echo '<td style="'.$lineaGruesa.$fondo.'text-align:center;vertical-align:middle;background-color:rgb(0,255,0)" rowspan="'.$numRows.'">';echo $paginat[$g]["ASLSAF"]!=''?number_format($paginat[$g]["ASLSAF"],0):"--"; echo '</td>
                                             		</tr>';
													

												}
												
											}//fin while(odbc_fetch_row($resultES))
											}else{
								?> 
                                                    <td style="">&nbsp;</td>
                                                    <td style="">&nbsp;</td>
                                                    <td style=" background-color:rgb(0,153,255)">&nbsp;</td>
                                                    <td style="">&nbsp;</td>
                                                    
                                                    <td style="">&nbsp;</td>
                                                    <td style="">&nbsp;</td>
                                                    <td style="background-color:rgb(255,255,153)">&nbsp;</td>
                                                    <td style="">&nbsp;</td>
                                                    <td style="">&nbsp;</td>
                                                    <td style="">&nbsp;</td>
                                                    <td style="background-color:rgb(0,255,0)" rowspan="<?php echo $numRows; ?>">&nbsp;</td>
                                                </tr>
								<?php 				
												
											}
																				
									}
								?>                                            


</table>