<?php session_start();
include("../conectar.php");
$arqnro = trim($_GET["num"]);
$tinva = unserialize( urldecode( $_GET['tinvan'] ) ) ;
//header("Pragma: ");
header("Pragma: no-cache");
header('Cache-control: ');
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Content-type: application/vnd.ms-excel");
//header("Content-type: application/octet-stream");
header("Content-disposition: attachment; filename=Reporte_MDT_Entrada_Salida".formatDate($Fechaactual,'dd.mm.aaa','aaaa_mm_dd').".xls");

?>
 

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Reporte Sistemas</title>
</head>
<style>

h1, h2, h3, h4, h5 {
	margin: 0;
	padding: 0;
	font-weight: normal;
	color: #32639A;
}

h1 {
	font-size: 2em;
}

h2 {
	font-size: 2.4em;	
}

h3 {
	font-size: 1.6em;
	font-style: italic;
}
h4 {
	font-size: 1.6em;
	font-style: italic;
	color: #FFF;
}
h5 {
	font-size: 1.0em;
	font-style: italic;
	color: #666;
}

#background-image
{
	font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
	font-size: 10px;
	margin: 0px;
	width: 100%;
	text-align: left;
	border-collapse: collapse;
}
#background-image th
{
	padding: 12px;
	font-weight: normal;
	font-size: 12px;
	color: #339;
	border-bottom-style: solid;
	border-left-style: none;
	text-align: center;
}
#background-image td
{
	color: #669;
	border-top: 1px solid #fff;
	padding-right: 4px;
	padding-left: 4px;
}
#background-image tfoot td
{
	font-size: 9px;
}
#background-image tbody 
{

	background-repeat: no-repeat;
	background-position: left top;
}
#background-image tbody td
{
	background-image: url(images/backn.png);
}
* html #background-image tbody td
{
	/* 
	   ----------------------------
		PUT THIS ON IE6 ONLY STYLE 
		AS THE RULE INVALIDATES
		YOUR STYLESHEET
	   ----------------------------
	*/
	filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='images/backn.png',sizingMethod='crop');
	background: none;
}	
</style>
<body>
<?php 
						$z=0;

						$tipoInve ='';
						if(!empty($tinva)){
							foreach($tinva as $key => $value){
								$tipoInve .= "'".$value."',";
							}
							$tipoInve = substr($tipoInve,0, (strripos($tipoInve,",")));
						}
						
						$sql="SELECT T3.AARUMB, T4.AUMDES, T1.ACICOD, T1.AARCOD, T1.AARNIV, T1.ATICOD, T1.ASICOD, T1.AARDES, T1.AARTDT, T1.AARSTS, T1.ATICOD, T6.ATIDES,
						(SELECT T6.ASLSAF FROM IV40FP T6 WHERE T6.ACICOD=T1.ACICOD AND T6.AARCOD=T1.AARCOD AND T6.AALCOD='0013' ORDER BY T6.ASLFEF DESC FETCH FIRST 1 ROWS ONLY) AS ASLSAF
							FROM IV05FP T1 
								INNER JOIN IV06FP T3 ON ( T1.ACICOD=T3.ACICOD AND T1.AARCOD=T3.AARCOD ) 
								INNER JOIN IV13FP T4 ON ( T1.ACICOD=T4.ACICOD AND T3.AARUMB=T4.AUMCOD ) 
								INNER JOIN IV01FP T6 ON (T1.ACICOD=T6.ACICOD  AND T1.ATICOD=T6.ATICOD)
							WHERE T1.ACICOD='$Compania' AND 
								( T1.AARCOD IN (SELECT T2.AARCOD FROM IV40FP T2 WHERE T2.ACICOD=T1.ACICOD AND T2.AALCOD='0013' GROUP BY T2.AARCOD ORDER BY T2.AARCOD ) ) 
								 ";
						if(!empty($tipoInve)){
							$sql.="  AND T1.ATICOD IN (".$tipoInve.")";
						}
						$sql.="  
							ORDER BY T1.AARDES";
						
						$query = $cid->queryAll($sql);

						$resultt = $query->fetchAll(PDO::FETCH_ASSOC);

						foreach ($resultt as $fields){
							foreach ($fields as $val => $valorinterno)
							{
								$row[$z][$val] = ICONV( "ISO-8859-1", "UTF-8", trim($valorinterno));
							}
							$z++;
						}
					$_SESSION['solicitudarreglo']=$row;
					$paginat=$_SESSION['solicitudarreglo'];
			?>      
<table width="100%" border="0">
<tr>
<td height="89"><h1>
<?php if($Compania=='14'){?>
	<img src="http://<?php echo $Direccionip; ?>/idasysv3/images/logoidacadef2005.png" width="280" height="68" />
<?php }else if($Compania=='40'){?>
	<img src="http://<?php echo $Direccionip; ?>/idasysv3/images/MEDITRON_logo_rif.png" width="300" />
<?php }else{ ?>
	<img src="http://<?php echo $Direccionip; ?>/idasysv3/images/logomeditronnuevo.png" width="280" height="68" />
<?php } ?>
   </h1>
  <h5>RIF:  <?php echo $Companiarif; ?></h5></td>
</tr>
<tr>
<td>

<table width="100%" id="background-image" >
	
  <thead>
  	<tr>
        <th colspan="18" scope="col"><h1>REPORTE - MDT / TI</h1></th>
    </tr>
  	<tr>
        <th colspan="18" scope="col"><h2>Entrada y Salida</h2></th>
    </tr>
    <tr>
        <th colspan="18" scope="col">Elaborado el <?php echo $Fechaactual; ?> a las <?php echo $Horaactual2; ?></th>
    </tr>
  	<tr>
        <th colspan="18" scope="col">Almacen Meditron Gerencia de Tecnologia de Información</th>
    </tr>
    <?php if(!empty($tinva)){ ?>
        <tr>
            <th colspan="18" scope="col">
                <strong>Filtrado por:</strong>
                <?php 
							$tipoInve ='';
							if(!empty($tinva)){
								foreach($tinva as $key => $value){
									$tipoInve .= "'".$value."',";
								}
								$tipoInve = substr($tipoInve,0, (strripos($tipoInve,",")));
							}
							
							/*$sql2="SELECT T6.ATIDES
							FROM IV01FP T6 
							WHERE T6.ACICOD='$Compania'  AND T6.ATICOD in (".$tipoInve.") ";*/
							
							$sql2 = $cid->select("IV01FP T6", "T6.ATIDES", "T6.ACICOD='$Compania'  AND T6.ATICOD in (".$tipoInve.")", "", "");

							$result2 = $sql2->fetchAll(PDO::FETCH_ASSOC);
							$count = 0;
							foreach($result2 as $values)
							{
								echo $result2[$count]['ATIDES'].", ";
								$count++;
							}
				
				;?>
            </th>
        </tr>
    <?php } ?>

    				<tr style="border-bottom:solid;">
                    	<th scope="col" rowspan="2" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)">C&oacute;digo</th>
                   		<th scope="col" rowspan="2" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)">Art&iacute;culo</th>
                        <th scope="col" rowspan="2" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)">Marca</th>
                        <th scope="col" rowspan="2" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)">Tipo Inventario</th>
                        <th scope="col" rowspan="2" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)">Ubicaci&oacute;n</th>
                        
                        <th scope="col" rowspan="2" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)"><strong>N&uacute;mero Referencia Entrada</strong></th>
                    	<th scope="col" colspan="5" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204); font-size:medium;">Entradas</th>
                        
                        <th scope="col" rowspan="2" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)"><strong>N&uacute;mero Referencia Salidas</strong></th>
                        <th scope="col" colspan="6" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204); font-size:medium;">Salidas</th>
                        
                        <th scope="col" rowspan="2" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;border-right:solid;background-color:rgb(204,204,204)">Existencia Actual</th>
                    </tr>
                    <tr>
                        <th scope="col" style="border-width:thin;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)"><strong>Tipo</strong></th>
                        <th scope="col" style="border-width:thin;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)"><strong>Fecha</strong></th>
                        <th scope="col" style="border-width:thin;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)"><strong>Cantidad</strong></th>
                        <th scope="col" style="border-width:thin;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)"><strong>Descripci&oacute;n</strong></th>
                        <th scope="col" style="border-width:thin;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)"><strong>Observaciones</strong></th>
                        
                        <th scope="col" style="border-width:thin;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)"><strong>Tipo</strong></th>
                        <th scope="col" style="border-width:thin;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)"><strong>Fecha</strong></th>
                        <th scope="col" style="border-width:thin;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)"><strong>Cantidad</strong></th>
                        <th scope="col" style="border-width:thin;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)"><strong>Solicitante</strong></th>
                        <th scope="col" style="border-width:thin;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)"><strong>Cargo de Solicitante</strong></th>
                        <th scope="col" style="border-width:thin;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)"><strong>Observaciones</strong></th>
                        
                    </tr>
                  </thead>
    				<tbody>
        						<?php
									for($g=0; $g < (count($paginat)); $g++)
									{
											/*entradas*/
											 $sql2="SELECT T3.ATRNUM, T3.ATRCOD, T2.ATRDES, T3.ATRFEC, T1.ATRCAN, (T3.ATRDES) AS DESATR, T3.ATROBS, T5.ADSNRO, T5.ATSCOD 
													FROM IV16FP T1 
														INNER JOIN IV15FP T3 ON (T1.ACICOD=T3.ACICOD AND T1.ATRCOD=T3.ATRCOD AND T1.ATRNUM=T3.ATRNUM AND T1.ADPCOD=T3.ADPCOD AND T3.ATRSTS='02' ) 
														INNER JOIN iv12fp T2 ON(T1.ACICOD= T2.ACICOD AND T1.ATRCOD=T2.ATRCOD) 
														LEFT JOIN IV35FP T5 ON (T1.ACICOD=T5.ACICOD AND T1.AALCOD=T5.AALCOD AND T3.ADPCOD=T5.ADPCOD AND T1.ATRCOD=T5.ATRCOD AND T1.ATRNUM=T5.ATRNUM) 
													WHERE T1.ACICOD='$Compania' AND T1.AALCOD='0013' AND T1.ATRCOD in ( '3215', '0103', '0006' ) AND T1.ATRART='".$paginat[$g]["AARCOD"]."' ORDER BY T3.ATRFEC 
													";

											$query2 = $cid->queryAll($sql2);

											$result2 = $query2->fetchAll(PDO::FETCH_ASSOC);
											$canEnt=0;

											foreach($result2 as $entradas){
												$canEnt++;
											}
											
											/*salidas*/
											$sql3="SELECT T2.ADSNRO, T3.ATSCOD, T3.ATSDES, T2.ATRFEC, T1.ATRCAN, T2.ATROBS, T2.ATRDES, T4.AISCOD, T4.AISDES
													FROM IV36FP T1 
													INNER JOIN IV35FP T2 ON ( T1.ACICOD = T2.ACICOD AND T1.AALCOD = T2.AALCOD AND T1.ADPCOD = T2.ADPCOD AND T1.ATSCOD = T2.ATSCOD AND T1.ADSNRO = T2.ADSNRO ) 
													INNER JOIN IS15FP T3 ON (T1.ACICOD=T3.ACICOD AND T2.ATSCOD=T3.ATSCOD) 
													INNER JOIN IV42FP T4 ON (T2.ACICOD=T4.ACICOD AND T2.AISCOD=T4.AISCOD) 
													WHERE T1.ACICOD='$Compania' AND 
													T1.AARCOD= '".$paginat[$g]["AARCOD"]."' AND 
													T1.AALCOD='0013' AND 
													T3.ATSCOD in ('13', '14', '15', '18', '19' , '20', '23', '24', '26', '27' )
													ORDER BY T2.ATRFEC 
													";

											$query3 = $cid->queryAll($sql3);

											$result3 = $query3->fetchAll(PDO::FETCH_ASSOC);

											$canSal=0;
											foreach($result3 as $salidas){
												$canSal++;
											}

											$numRows = '';

											if($canSal>$canEnt){
												$numRows = $canSal;
											}else if($canSal<=$canEnt){
												$numRows = $canEnt;
											}

											$rowpos = 0;
											$count2 = 0;
											$count3 = 0;			
                                 ?>           
                                                <tr >
	                                                <td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:left;vertical-align:middle;" valign="middle" rowspan="<?php echo $numRows; ?>"><div>&nbsp;<?php echo $paginat[$g]["AARCOD"]; ?></div></td>
                                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:left;vertical-align:middle;" valign="middle" rowspan="<?php echo $numRows; ?>"><div><?php echo $paginat[$g]["AARDES"]; ?></div></td>
                                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:left;vertical-align:middle;" valign="middle" rowspan="<?php echo $numRows; ?>"><div>&nbsp;<?php  echo $paginat[$g]['AMCDES']!=''?$paginat[$g]['AMCDES']:'N/A'; ?></div></td>
                                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:left;vertical-align:middle;" valign="middle" rowspan="<?php echo $numRows; ?>"><div><?php echo $paginat[$g]["ATIDES"]; ?></div></td>
                                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:left;vertical-align:middle;" valign="middle" rowspan="<?php echo $numRows; ?>">
                                                    	<div>
															<?php 
                                                                $list = list_ubiart($cid, $Compania, '0001', $paginat[$g]["AARCOD"],2);
                                                                
                                                                if(trim($list)!=''){
                                                                    echo $list; } 
                                                                else { 
                                                                    echo ' - Sin Ubicaci&oacute;n';
                                                                }
                                                             ?>
                                                        </div>
                                                    </td>
                                 <?php
											 if($numRows>$rowpos){
											 while($numRows>$rowpos){   
											 
											          $rowpos++;                                      
												if($rowpos<=$canEnt){
													?>
                                                <!-- entradas -->
                                                <?php 
													if($result2[$count2]['ATRCOD']=='0006'){
														?>
                                                        <td style="border-width:thin;border-bottom:solid;border-left:solid;text-align:center;vertical-align:middle;" valign="middle">&nbsp;<?php echo $result2[$count2]['ADSNRO']!=''?add_ceros($result2[$count2]['ADSNRO'],6):'&nbsp;'; echo "<strong>(D)</strong>";?></td>
                                                        <?php
													}else{
														?>
                                                        <td style="border-width:thin;border-bottom:solid;border-left:solid;text-align:center;vertical-align:middle;" valign="middle">&nbsp;<?php echo $result2[$count2]['ATRNUM']!=''?add_ceros($result2[$count2]['ATRNUM'],6):'&nbsp;'; echo '(E)';?></td>
                                                        <?php
													}	
													
								?>
                                                    
                                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;text-align:center;vertical-align:middle;" valign="middle">&nbsp;<?php echo $result2[$count2]['ATRDES']!=''?$result2[$count2]['ATRDES']:'&nbsp;';?></td>
                                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;text-align:center;vertical-align:middle;" valign="middle"><?php echo $result2[$count2]['ATRFEC']!=''?formatDate($result2[$count2]['ATRFEC'],'aaaa-mm-dd','dd/mm/aaaa' ):'&nbsp;';?></td>
                                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;text-align:center;vertical-align:middle; background-color:rgb(0,153,255)" valign="middle"><?php echo $result2[$count2]['ATRCAN']!=''?number_format($result2[$count2]['ATRCAN'],2,',','.'):'&nbsp;';?></td>
                                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;text-align:left;vertical-align:text-top;" valign="top">&nbsp;<?php echo $result2[$count2]['DESATR']!=''?$result2[$count2]['DESATR']:'&nbsp;';?></td>
                                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;text-align:left;vertical-align:text-top;" valign="top"><?php echo $result2[$count2]['ATROBS']!=''?$result2[$count2]['ATROBS']:'&nbsp;';?></td>
                                                    	
                                                      
                                 <?php 
								 				
                                             	}else{                  
								?>
                                					<td style="border-width:thin;border-bottom:solid;border-left:solid;">&nbsp;</td>
                                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;">&nbsp;</td>
                                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;">&nbsp;</td>
                                                    <td style="border-width:thin;border-bottom:solid;border-left:solid; background-color:rgb(0,153,255)">&nbsp;</td>
                                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;">&nbsp;</td>
                                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;">&nbsp;</td>
                              <?php 
                                            	 }
										
												if(  ($rowpos <= $canSal) && (number_format($result3[$count3]['ATRCAN'],0,',','.') >= 0)  ){
                                            
								?>                      
                                                    <!-- salida -->
                                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;text-align:center;vertical-align:middle;" valign="middle">&nbsp;<?php echo $result3[$count3]['ADSNRO']!=''?add_ceros($result3[$count3]['ADSNRO'],6):'&nbsp;'; echo "(S)";?></td>
                                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;text-align:center;vertical-align:middle;" valign="middle">&nbsp;<?php echo $result3[$count3]['ATSDES']!=''?$result3[$count3]['ATSDES']:'&nbsp;';?></td>
                                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;text-align:center;vertical-align:middle;" valign="middle"><?php echo $result3[$count3]['ATRFEC']!=''?formatDate($result3[$count3]['ATRFEC'],'aaaa-mm-dd','dd/mm/aaaa' ):'&nbsp;';?></td>
                                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;text-align:center;vertical-align:middle;background-color:rgb(255,255,153)" valign="middle"><?php echo $result3[$count3]['ATRCAN']!=''?number_format($result3[$count3]['ATRCAN'],2,',','.'):'&nbsp;';?></td>
                                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;text-align:left;vertical-align:text-top;" valign="top"><?php echo $result3[$count3]['ATRDES']!=''?$result3[$count3]['ATRDES']:'&nbsp;';?></td>
                                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;text-align:center;vertical-align:middle;" valign="middle"><?php echo $result3[$count3]['AISDES']!=''?$result3[$count3]['AISDES']:'&nbsp;';?></td>
                                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;text-align:left;vertical-align:text-top;" valign="top"><?php echo $result3[$count3]['ATROBS']!=''?$result3[$count3]['ATROBS']:'&nbsp;';?></td>
                                                    
                                <?php 
												}else{					
								?>
                                					<td style="border-width:thin;border-bottom:solid;border-left:solid;">&nbsp;</td>
                                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;">&nbsp;</td>
                                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;">&nbsp;</td>
                                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;background-color:rgb(255,255,153)">&nbsp;</td>
                                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;">&nbsp;</td>
                                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;">&nbsp;</td>
                                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;">&nbsp;</td>
                              <?php 
                                             	}
											 
                                            	if($rowpos == 1){
								?> 
                                                   <td align="right" style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:center;vertical-align:middle;background-color:rgb(0,255,0)" valign="middle" rowspan="<?php echo $numRows; ?>"><div><?php echo $paginat[$g]["ASLSAF"]!=''?number_format($paginat[$g]["ASLSAF"],2,',','.'):"--";?></div></td>
								<?php 
                                            	}//fin if  if($rowpos == 1)
								?>
                                            </tr>
                               <?php				
											$count2++;
											$count3++;
											}//fin while(odbc_fetch_row($resultES))
											}else{
								?> 
                                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;border-left:solid;">&nbsp;</td>
                                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;">&nbsp;</td>
                                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;">&nbsp;</td>
                                                    <td style="border-width:thin;border-bottom:solid;border-left:solid; background-color:rgb(0,153,255)">&nbsp;</td>
                                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;">&nbsp;</td>
                                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;">&nbsp;</td>
                                                    
                                                    
                                					<td style="border-width:thin;border-bottom:solid;border-left:solid;">&nbsp;</td>
                                                    <td style="border-width:thin;border-bottom:solid;">&nbsp;</td>
                                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;">&nbsp;</td>
                                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;background-color:rgb(255,255,153)">&nbsp;</td>
                                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;">&nbsp;</td>
                                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;">&nbsp;</td>
                                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;">&nbsp;</td>
                                                    
                                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;background-color:rgb(0,255,0)" rowspan="<?php echo $numRows; ?>">&nbsp;</td>
                                                </tr>
								<?php 				
												
											}
																				
									}
								?>                                            
                  </tbody>

</table>
</td>
</tr>
</table>
</body>
</html>
