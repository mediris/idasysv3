<?php 
/*
 * BGONZALEZ
 * 04/09/2020
 */
session_start();
include("../conectar.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="keywords" content="" />
		<meta name="description" content="" />
		<title>Idasys V3</title>
		<link href="../<?php echo $css; ?>" rel="stylesheet" type="text/css" media="screen" />
		<script language="JavaScript" src="../javascript/javascript.js"></script>
		<script language="JavaScript" src="../calendario/javascripts.js"></script>
		<script language="JavaScript" src="javascript.js"></script>
		<script language="JavaScript" src="../javascript/jquery.js"></script>
		<link href="../shadowbox.css"  rel="stylesheet" type="text/css"/>
		<script src="../shadowbox.js" type="text/javascript"/> </script>
		<script language="JavaScript" type="text/JavaScript">
			Shadowbox.init({overlayOpacity: "0.5"});
		</script>
		<style type="text/css" title="currentStyle">
			@import "../DataTables-1.9.4/media/css/<?php echo $demo_page;?>";
			@import "../DataTables-1.9.4/media/css/<?php echo $demo_table;?>";
		</style>
		<script type="text/javascript" language="javascript" src="../DataTables-1.9.4/media/js/jquery.dataTables.js"></script>
		<script type="text/javascript" charset="utf-8">
			$(document).ready(function(){
	      		document.getElementById('info').style.display="";

				$('#info').dataTable({
					"bStateSave": true,
			        "oLanguage": {
			          	"sLengthMenu": "Mostrar _MENU_ registros por Pag.",
						"sZeroRecords": "No Existen Registros",
						"sInfo": "Mostrando desde _START_ hasta _END_ de _TOTAL_ registros",
						"sInfoEmpty": "Mostrando desde 0 hasta 0 de 0 registros",
						"sInfoFiltered": "(filtrando de _MAX_ de registros)",
						"sSearch": "Buscar",
						"oPaginate": {
							"sNext": "Siguiente",
							"sPrevious": "Anterior"
						}
			        }
		      	});
		  	});
		</script>
	</head>

	<body background="../images/fondo idaca.jpg" >
		<div id="wrapper">
  			<?php include("../superior.php");?>
  			<div id="page">
     			<?php  include("../validar.php");?>
 				<?php 
				
				if($bandera == 1){	
					$tipo = $_GET['tipo'];
					$fecha = $_GET['fecha'];
					$fechaIni = trim($_GET['fecha']) ? trim($_GET['fecha']) . "-01-01" : date("Y").'-01-01';
					$fechaFin = trim($_GET['fecha']) ? trim($_GET['fecha']) . "-12-31" : date("Y").'-12-31';
					$wsolicitud = 0;
					if($solicitudpagina == 0){

						$sql = "SELECT T5.AAPVLA, T5.APACOD, T3.AARUMB, T4.AUMDES, T1.ACICOD, T1.AARCOD, T1.AARNIV, T1.ATICOD, T1.ASICOD, T1.AARDES, T1.AARTDT, T1.AARSTS, (SELECT T6.ASLSAF FROM IV40FP T6 WHERE T6.ACICOD=T1.ACICOD AND T6.AARCOD=T1.AARCOD AND T6.AALCOD='".$aalcod."' ORDER BY T6.ASLFEF DESC FETCH FIRST 1 ROWS ONLY) AS ASLSAF

						FROM IV05FP T1 
						INNER JOIN IV06FP T3 ON (T1.ACICOD = T3.ACICOD AND T1.AARCOD = T3.AARCOD) 
						INNER JOIN IV13FP T4 ON (T1.ACICOD = T4.ACICOD AND T3.AARUMB = T4.AUMCOD) 
						LEFT JOIN IV17FP T5 ON (T1.ACICOD = T5.ACICOD AND T1.AARCOD = T5.AARCOD AND T5.APACOD IN ('0102','0108', '1101', '1127')) 
						
						WHERE T1.ACICOD = '".$Compania."' AND (T1.AARCOD IN (SELECT T2.AARCOD FROM IV40FP T2 WHERE T2.ACICOD = T1.ACICOD AND T2.AALCOD = '".$aalcod."' GROUP BY T2.AARCOD ORDER BY T2.AARCOD) AND T1.AARSTS = '01' AND T1.ATICOD = '".$tipo."') 
							
						ORDER BY T1.AARCOD";
						
						//echo $sql."<br/><br/>";
						$resultt = odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111"));
						
						$z = 0;
						// $tothon = 0;
						// $totest = 0;
						$lin = 1;
						$limitep = $_SESSION['solicitudlineasporpaginat'];
						$pag = 1;
						// $primero = 'S';

						while(odbc_fetch_row($resultt)){ 

							$artcod = odbc_result($resultt,'AARCOD');
							$atrdes = odbc_result($resultt,'AARDES');
							$exp = substr($atrdes ,(strripos($atrdes,"(")) );
							$aumdes = odbc_result($resultt,'AUMDES');
							$existencia = odbc_result($resultt,'ASLSAF');
							
							/*CARGA DE ENTRADAS*/
							$listapacod = array('0102','0108', '1101', '1127');

							//$listapacod = explode(',', $listapacod);
							$sql2="";
							$sql3="";
							$sql2.="
								SELECT T4.ACICOD, T4.AALCOD, T4.ADPCOD, T4.ATRCOD, (T5.ATRDES) as ATRNOM, T5.ATRSIG, T4.ATRNUM, T4.ATRDES, 
										T4.ATRFEC, T4.AUSCOD, T4.ATROBS, T3.ATRSEC, T3.ATRART,('".$atrdes."') AS AARDES, T3.ATRCAN, T3.ATRUMB, ('".$aumdes."') AS AUMDES, ('".$existencia."') AS ASLSAF, ";
							foreach($listapacod as $key=>$value){			
								$sql3.="
										(SELECT T6.AAPVLA FROM iv17fp T6 WHERE T6.ACICOD=T3.ACICOD and T6.AARCOD=T3.ATRART AND T6.APACOD IN (".$value.") ) AS N".$value." , ";
							}
							$sql2.= substr($sql3,0, (strripos($sql3,",")));
							$sql2.="	
								FROM IV16FP T3 
									INNER JOIN IV15FP T4 ON ( T3.ACICOD=T4.ACICOD AND T3.AALCOD=T4.AALCOD AND T3.ATRCOD=T4.ATRCOD AND T3.ATRNUM=T4.ATRNUM )                                                                   
									INNER JOIN IV12FP T5 ON ( T4.ACICOD=T5.ACICOD AND T4.ATRCOD=T5.ATRCOD AND T5.ATRSIG='+')
								WHERE T4.ACICOD='".$Compania."' AND T4.AALCOD='".$aalcod."' AND T3.ATRART = '".$artcod."'
								ORDER BY T4.ATRNUM , T3.ATRART
								";
								
							// echo $sql2."<br/><br/>";
							// DIE();
							$resultt2=odbc_exec($cid,$sql2)or die(exit("Error en odbc_exec 11111 - ".odbc_error($resultt2) ));
							while(odbc_fetch_row($resultt2))
							{
								$jml = odbc_num_fields($resultt2);
								$row[$z]["pagina"] =  $pag;
								for($i=1;$i<=$jml;$i++)
								{	
									$row[$z][odbc_field_name($resultt2,$i)] =  odbc_result($resultt2,$i);
								}
								$z++;
								if ($lin>=$limitep) 
								{
									$limitep+=$_SESSION['solicitudlineasporpaginat'];
									$pag++;
								}
								$lin++;
							}
						}

						$totsol = ($lin-1);
						$_SESSION['totalsolicitudes'] = $totsol;
						$_SESSION['solicitudarreglo'] = $row;
						$solicitudpagina = 1;
						$_SESSION['solicitudpaginas'] = $pag;
					}//fin de solicitudpagina
					/*se muestra la cantidad de elementos segun la solicitada en $solicitudpagina*/
					$paginat = $_SESSION['solicitudarreglo'];
				}?>
   	 			<div id="content3" >   
        			<table width="100%"   border="0">
              			<tr>
                			<td width="65%" scope="col" colspan="2"><h1  class="title">Reporte por Tipo de Artículo</h1><hr /></td>
                			<td width="13%" scope="col">
                				<div align="left">
                  					<table width="100%"  border="0">
                    					<tr>
                      						<th width="30%" scope="col" >
                      							<div class="flechas">
                      								<a href="exportaraexcel_meditron_tipo_inventario.php?&aalcod=<?php echo $aalcod; ?>&tipo=<?php echo $tipo?>&fecha=<?php echo $fecha?>" target="_blank">
                        								<img src="../images/excel.jpg" alt="" width="30" height="30" title="Exportar a Excel Salida"/>
                        							</a>
                      							</div>
                      						</th>
                    					</tr>
                  					</table>
                				</div>
                			</td>
              			</tr>
              			<tr>
              				<td colspan="3">
                				<form name="form" id="form" method="post">
                    				<input type="hidden" name="bandera" id="bandera" value="<?php echo $bandera;?>" />
                    				<input type="hidden" name="tipo" id="tipo" value="<?php echo $tipo;?>" />
                					<table>
                    					<tr>
                        					<td>&nbsp;</td>
                            				<td>&nbsp;</td>
                            				<td>&nbsp;</td>
                            				<td>&nbsp;</td>
                            				<td>&nbsp;</td>
                            				<td rowspan="2"><a href="javascript:busquedatipinv();"><img src="../images/precesarconsulta.png" width="50" height="50" alt="Realizar Consulta" style="border:none;"></a></td>
                        				</tr>
                        				<tr>
                        					<td>&nbsp;Almac&eacute;n:</td>
                            				<td>&nbsp;<?php //echo $fecha; ?>
                                				<select name="aalcod" id="aalcod">
	                                    			<option value= "0001">del Departamento de Servicio Generales y Mantenimiento(0001)</option>
                                				</select>
	                            			</td>
	                            			<td>&nbsp;</td>
	                            			<td>&nbsp;</td>
	                            			<td>&nbsp;</td>
                        				</tr>
                        				<tr>
                        					<td>&nbsp;Año:</td>
                        					<td>&nbsp;
                        						<select name="fecha" id="fecha">
													<?php 
													$i = 0;
													while($i <= 10){ ?>
							                			<option value="<?php echo date('Y') - $i;?>"><?php echo date('Y') - $i;?></option>
							                			<?php $i++;  
							                		} ?>	
							                    </select>
                        					</td>
                        				</tr>
                        				<tr>
				                        	<td>&nbsp;Tipo de Inventario</td>
				                            <td colspan="4">
				                            	<table>
					                            	<tr>
					                            		<td>
					                            			<input type="radio" name="tipInv" value="0008">
															<label for="invofc">Inventario Oficinas MEDITRON</label>
														</td>
					                            		<td>
					                            			<input type="radio" name="tipInv" value="0009">
															<label for="invlmp">Inventario Limpieza MEDITRON</label>
														</td>
														<td>
					                            			<input type="radio" name="tipInv" value="0010">
															<label for="invcft">Inventario Cafetín MEDITRON</label>
														</td>
					                            	</tr>
				                            	</table>

				                            	<!-- <div id="table_tpinven">
				                                </div>
				                                <script>cargarSelectTpInven();</script> -->

				                            </td>
				                        </tr>
				                        <?php if($bandera == 1){ ?>
					                        <tr>
					                        	<td>
	                        						<?php 
                        							if($tipo == '0005'){
                        								$inv = "Oficinas";
                        							}else if($tipo == '0006'){
                        								$inv = "Limpieza";
                        							}else{
                        								$inv = "Cafet&iacute;n";
                        							}
                        								?>
                        							<b>Inventario: <?php echo $inv?>. Año: <?php echo $fecha?></b>
	                        					</td>
					                        </tr>
					                    <?php } ?>
                					</table>
                    			</form>
	                    		<hr />
	                		</td>
              			</tr>
            		</table>   
					<div id="container">
            			<div id="demo">
                			<table width="100%" id="info" style="display:">
                  				<thead>
                    				<tr>
				                        <th scope="col" rowspan="2" style="background-color:rgb(204,204,204)">C&oacute;d Art.</th>
				                        <th scope="col" rowspan="2" style="background-color:rgb(204,204,204)">Art&iacute;culo</th>
				                        <th scope="col" rowspan="2" style="background-color:rgb(204,204,204)">Nro referencia entrada</th>
				                    	
				                        <th scope="col" colspan="3" style="background-color:rgb(204,204,204)">Entradas</th>

				                        <th scope="col" rowspan="2" style="background-color:rgb(204,204,204)">Nro referencia salida</th>
				                        
				                        <th scope="col" colspan="3" style="background-color:rgb(204,204,204)">Salidas</th>
				                        
				                        <th scope="col" rowspan="2" style="background-color:rgb(204,204,204)">Existencia actual</th>
				                    </tr>
				                    <tr>
				                        <th scope="col" style="background-color:rgb(204,204,204)"><strong>Fecha</strong></th>
				                        <th scope="col" style="background-color:rgb(204,204,204)"><strong>Cant.</strong></th>
				                        <th scope="col" style="background-color:rgb(204,204,204)"><strong>Observaciones</strong></th>

				                        <th scope="col" style="background-color:rgb(204,204,204)"><strong>Fecha</strong></th>
				                        <th scope="col" style="background-color:rgb(204,204,204)"><strong>Cant.</strong></th>
				                        <th scope="col" style="background-color:rgb(204,204,204)"><strong>Observaciones</strong></th>
				                    </tr>
                 				</thead> 
    							<tbody>

        							<?php
									$pagact = $solicitudpagina;
									$part = 1;
									for($g = 0; $g < (count($paginat)); $g++){
										// echo "//**".$paginat[$g]["ATRART"]."<br>";

										if($paginat[$g - 1]["ATRART"] != $paginat[$g]["ATRART"]){

											/*entradas*/
											$sql2 = "SELECT T3.ATRNUM, T3.ATRCOD, T2.ATRDES, T3.ATRFEC, T1.ATRCAN, (T3.ATRDES) AS DESATR, T3.ATROBS, T5.ADSNRO, T5.ATSCOD 

											FROM IV16FP T1 
											INNER JOIN IV15FP T3 ON (T1.ACICOD = T3.ACICOD AND T1.ATRCOD = T3.ATRCOD AND T1.ATRNUM = T3.ATRNUM AND T1.ADPCOD = T3.ADPCOD AND T3.ATRSTS = '02' AND T3.ATRFEC BETWEEN '".$fechaIni."' AND '".$fechaFin."') 
											INNER JOIN IV12FP T2 ON (T1.ACICOD = T2.ACICOD AND T1.ATRCOD = T2.ATRCOD) 
											LEFT JOIN IV35FP T5 ON (T1.ACICOD = T5.ACICOD AND T1.AALCOD = T5.AALCOD AND T3.ADPCOD = T5.ADPCOD AND T1.ATRCOD = T5.ATRCOD AND T1.ATRNUM = T5.ATRNUM) 

											WHERE T1.ACICOD = '".$Compania."' AND T1.AALCOD = '".$aalcod."' AND T1.ATRCOD in ('0101', '0103', '0104', '0106', '0108', '3215', '0006', '0018', '0401') AND T1.ATRART='".$paginat[$g]["ATRART"]."' 

											ORDER BY T3.ATRFEC";

											// echo $sql2."<br/><br/>";
											$result2 = odbc_exec($cid,$sql2)or die(exit("Error en odbc_exec 11111"));
											$result22 = odbc_exec($cid,$sql2)or die(exit("Error en odbc_exec 11111"));
											$canEnt = 0;
											//$canEnt = odbc_num_rows($result22);
											while(odbc_fetch_row($result22)){
												$canEnt++;
											}
											//echo "<br>"."*".$canEnt;

											/*salidas*/
											$sql3 = "SELECT (T3.ATRNUM) AS ADSNRO, T3.ATRCOD, T2.ATRDES, T3.ATRFEC, T1.ATRCAN, (T3.ATRDES) AS DESATR, T3.ATROBS, T5.ADSNRO, T5.ATSCOD

											FROM IV16FP T1 
											INNER JOIN IV15FP T3 ON (T1.ACICOD = T3.ACICOD AND T1.ATRCOD = T3.ATRCOD AND T1.ATRNUM = T3.ATRNUM AND T1.ADPCOD = T3.ADPCOD AND T3.ATRSTS = '02' AND T3.ATRFEC BETWEEN '".$fechaIni."' AND '".$fechaFin."') 
											INNER JOIN IV12FP T2 ON (T1.ACICOD = T2.ACICOD AND T1.ATRCOD = T2.ATRCOD) 
											LEFT JOIN IV35FP T5 ON (T1.ACICOD = T5.ACICOD AND T1.AALCOD = T5.AALCOD AND T3.ADPCOD = T5.ADPCOD AND T1.ATRCOD = T5.ATRCOD AND T1.ATRNUM = T5.ATRNUM) 

											WHERE T1.ACICOD = '$Compania' AND T1.AALCOD = '$aalcod' AND T1.ATRCOD in ('0102', '0105', '0107', '0005', '0007', '0008', '0009', '0010', '0011', '0012', '0013', '0014', '0015', '0016', '0017', '0019', '0020', '0402') AND T1.ATRART='".$paginat[$g]["ATRART"]."' 

											ORDER BY T3.ATRFEC";

											// echo $sql3."<br/><br/>";
											$result3 = odbc_exec($cid,$sql3)or die(exit("Error en odbc_exec 11111"));
											$result33 = odbc_exec($cid,$sql3)or die(exit("Error en odbc_exec 11111"));
											//echo "<br>"."+".$canSal = odbc_num_rows($result3);
											$canSal = 0;
											//$canEnt = odbc_num_rows($result22);
											while(odbc_fetch_row($result33)){
												$canSal++;
											}
											//echo "<br>"."*".$canSal;

											$resultES = '';
											$numRows = '';
											if($canSal > $canEnt){
												$resultES = $result3;
												$numRows = $canSal;
												//echo "<br>"."* salida";
											}else if($canSal <= $canEnt){
												$resultES = $result2;
												$numRows = $canEnt;
												//echo "<br>"."* entrada";
											}
											$rowpos = 0;
											//echo "numRows:".$numRows;
											//echo "rowpos:".$rowpos;				
	                                 		?>
	                                                
	                                 		<?php  
										 	if($numRows > $rowpos){
											 	while($numRows > $rowpos){   
												 
												//echo "numRows:".$numRows;
												//echo "rowpos:".$rowpos;
											 		$rowpos++;       
												  	odbc_fetch_row($result2);
												  	odbc_fetch_row($result3);
												?>
	                                            	<tr>

		                                                 <!-- Código del artículo -->
		                                                <td style="text-align: left; vertical-align: middle;">
		                                                	<?php echo $paginat[$g]["ATRART"] != '' ? $paginat[$g]["ATRART"] : '--';?>
		                                                </td>

		                                                <!-- Artículo -->
		                                                <td style="text-align: center; vertical-align: middle;">
		                                                	<?php echo $paginat[$g]['AARDES'] != '' ? $paginat[$g]['AARDES'] : '--';?>
		                                                </td> 

		                                            	<?php

		                                                /* <!-- entradas --> */
														if($rowpos <= $canEnt){?>

															<!-- N° referencia entrada -->
		                                                	<td style="text-align: center; vertical-align: middle;">
		                                                		<?php echo odbc_result($result2, 'ATRNUM') != '' ? odbc_result($result2, 'ATRNUM') : '--'; echo ' (E)';?>
		                                                	</td>

		                                                	<!-- Fecha entrada -->
		                                                	<td style="text-align: center; vertical-align: middle;">
		                                                		<?php echo odbc_result($result2, 'ATRFEC') != '' ? formatDate(odbc_result($result2, 'ATRFEC'),'aaaa-mm-dd', 'dd/mm/aaaa' ) : '--';?>
		                                                	</td>

		                                                	<!-- Cantidad entrada -->
		                                                	<td style="text-align: center; vertical-align: middle;">
		                                                		<?php echo odbc_result($result2, 'ATRCAN') != '' ? number_format(odbc_result($result2, 'ATRCAN'),2,',','.') : '--';?>
		                                                	</td>

		                                                	<!-- Observacion entrada -->
		                                                	<td style="text-align: center; vertical-align: middle;">
		                                                		<?php echo odbc_result($result2, 'ATROBS') != '' ? odbc_result($result2, 'ATROBS') : '--';?>
		                                                	</td>
		                                 				
		                                 				<?php }else{ ?>

		                                					<td style="text-align: center; vertical-align: middle;">--</td>
		                                                    <td style="text-align: center; vertical-align: middle;">--</td>
		                                                    <td style="text-align: center; vertical-align: middle;">--</td>
		                                                    <td style="text-align: center; vertical-align: middle;">--</td>

		                              					<?php } 


		                              					/* <!-- salidas --> */
													 	if(($rowpos <= $canSal) && (number_format(odbc_result($result3, 'ATRCAN'),2,',','.') >= 0)) { ?>     

													 		<!-- N° referencia salida -->
		                                                    <td style="text-align: center; vertical-align: middle;">
		                                                    	<?php echo odbc_result($result3, 'ADSNRO') != '' ? odbc_result($result3, 'ADSNRO') : '--'; echo " (S)";?>
		                                                    </td>	
																
		   													<!-- Fecha salida -->
		                                                    <td style="text-align: center; vertical-align: middle;">
		                                                    	<?php echo odbc_result($result3, 'ATRFEC') != '' ? formatDate(odbc_result($result3, 'ATRFEC'),'aaaa-mm-dd', 'dd/mm/aaaa' ) : '--';?>
		                                                    </td>

		                                                    <!-- Cantidad salida -->
		                                                    <td style="text-align: center; vertical-align: middle;">
		                                                    	<?php echo odbc_result($result3, 'ATRCAN') != '' ? number_format(odbc_result($result3, 'ATRCAN'),2,',','.') : '--';?>
		                                                    </td>

		                                                    <!-- Observacion salida -->
		                                                    <td style="text-align: center; vertical-align: middle;">
		                                                    	<?php echo odbc_result($result3, 'ATROBS') != '' ? odbc_result($result3, 'ATROBS') : '--';?>
		                                                    </td>

		                               		 			<?php }else{ ?>

		                                					<td style="text-align: center; vertical-align: middle;">--</td>
		                                                    <td style="text-align: center; vertical-align: middle;">--</td>
		                                                    <td style="text-align: center; vertical-align: middle;">--</td>
		                                                    <td style="text-align: center; vertical-align: middle;">--</td>

		                              					<?php } ?>

														<!-- Existencia -->
	                                                   	<td style="text-align: center; vertical-align: middle;">
	                                                   		<?php echo $paginat[$g]["ASLSAF"] != '' ? number_format($paginat[$g]["ASLSAF"],2,',','.') : '--';?>
	                                                   	</td>

		                                            </tr>

	                               				<?php }
											}
											echo "";
										}
									}?>    
                  				</tbody>
                			</table>
						</div>
		  			</div>
	  			</div>
			</div>
			<div style="clear: both;">&nbsp;</div>
		</div>
		<div id="footer">
			<div id="opcionesmasusadas"><?php include("../opcionesmasusadasphp.php"); ?></div>
		</div>
	</body>
</html>
