<?php session_start();
include("../conectar.php");
$arqnro = trim($_GET["num"]);
header("Pragma: ");
header('Cache-control: ');
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Content-type: application/vnd.ms-excel");
header("Content-disposition: attachment; filename=Consulta de existencia.xls");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Documento sin t�tulo</title>
</head>
<style>

h1, h2, h3, h4, h5 {
	margin: 0;
	padding: 0;
	font-weight: normal;
	color: #32639A;
}

h1 {
	font-size: 2em;
}

h2 {
	font-size: 2.4em;	
}

h3 {
	font-size: 1.6em;
	font-style: italic;
}
h4 {
	font-size: 1.6em;
	font-style: italic;
	color: #FFF;
}
h5 {
	font-size: 1.0em;
	font-style: italic;
	color: #666;
}

#background-image
{
	font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
	font-size: 10px;
	margin: 0px;
	width: 100%;
	text-align: left;
	border-collapse: collapse;
}
#background-image th
{
	padding: 12px;
	font-weight: normal;
	font-size: 12px;
	color: #339;
	border-bottom-style: solid;
	border-left-style: none;
	text-align: center;
}
#background-image td
{
	color: #669;
	border-top: 1px solid #fff;
	padding-right: 4px;
	padding-left: 4px;
}
#background-image tfoot td
{
	font-size: 9px;
}
#background-image tbody 
{

	background-repeat: no-repeat;
	background-position: left top;
}
#background-image tbody td
{
	background-image: url(images/backn.png);
}
* html #background-image tbody td
{
	/* 
	   ----------------------------
		PUT THIS ON IE6 ONLY STYLE 
		AS THE RULE INVALIDATES
		YOUR STYLESHEET
	   ----------------------------
	*/
	filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='images/backn.png',sizingMethod='crop');
	background: none;
}	
</style>
<body>
<?php 

		 		$wsolicitud=0;
				if ($solicitudpagina==0) 	
				{
							$fecha = date('Y-m-d');
							//$fecha = '2015-02-01';
							if( date('m',strtotime('0 months',strtotime(date($fecha)))) < date('m',strtotime('-1 months',strtotime(date($fecha)))) ){
								$anio1 = "'".date('Y',strtotime('-1 year',strtotime(date($fecha))))."'";
							}else{
								$anio1 = "'".date('Y',strtotime('0 year',strtotime(date($fecha))))."'";
							}
							
							
							if( date('m',strtotime('0 months',strtotime(date($fecha)))) < date('m',strtotime('-2 months',strtotime(date($fecha)))) ){
								$anio2 = "'".date('Y',strtotime('-1 year',strtotime(date($fecha))))."'";
							}else{
								$anio2 = "'".date('Y',strtotime('0 year',strtotime(date($fecha))))."'";
							}
							
							if( date('m',strtotime('0 months',strtotime(date($fecha)))) < date('m',strtotime('-3 months',strtotime(date($fecha)))) ){
								$anio3 = "'".date('Y',strtotime('-1 year',strtotime(date($fecha))))."'";
							}else{
								$anio3 = "'".date('Y',strtotime('0 year',strtotime(date($fecha))))."'";
							}
							
							$sql= "
								SELECT T1.ACICOD, T1.AALCOD, T1.AARCOD, T3.AARDES, T1.AUMCOD, T4.AUMDES, T1.ALTCOD, T5.AARSTM, 
									sum(case when T1.ASLFEF=(SELECT max(T2.ASLFEF) FROM iv40fp T2 where T2.ACICOD=T1.ACICOD AND T2.AALCOD=T1.AALCOD AND T2.AARCOD=T1.AARCOD AND T2.ALTCOD=T1.ALTCOD AND T2.ASLFEF<'01.10.1990' ) then T1.aslsaf else 0 end) as SALANT, 
									sum(case when T1.ASLFEF < '".$fecha."' then T1.ASLENT else 0 end) as ASLENT, 
									sum(case when T1.ASLFEF < '".$fecha."' then T1.ASLSAL else 0 end) as ASLSAL,
									(SELECT SUM(T6.ATRCAN) AS ATRCAN FROM IV16FP T6 INNER JOIN IV15FP T7 ON ( T6.ACICOD=T7.ACICOD AND T6.AALCOD=T7.AALCOD AND T6.ATRCOD=T7.ATRCOD AND T6.ATRNUM=T7.ATRNUM ) INNER JOIN IV12FP T8 ON ( T7.ACICOD=T8.ACICOD AND T7.ATRCOD=T8.ATRCOD AND T8.ATRSIG='-') WHERE T7.ACICOD=T1.ACICOD AND T7.AALCOD=T1.AALCOD AND month(T7.ATRFEC)='".date("m")."' AND YEAR(T7.ATRFEC)= '".date('Y')."' AND T6.ATRART=T1.AARCOD GROUP BY T6.ATRART ORDER BY T6.ATRART)AS CAN1 ,
									(SELECT SUM(T9.ATRCAN) AS ATRCAN FROM IV16FP T9 INNER JOIN IV15FP T10 ON ( T9.ACICOD=T10.ACICOD AND T9.AALCOD=T10.AALCOD AND T9.ATRCOD=T10.ATRCOD AND T9.ATRNUM=T10.ATRNUM ) INNER JOIN IV12FP T11 ON ( T10.ACICOD=T11.ACICOD AND T10.ATRCOD=T11.ATRCOD AND T11.ATRSIG='-') WHERE T10.ACICOD=T1.ACICOD AND T10.AALCOD=T1.AALCOD AND month(T10.ATRFEC)='".date("m",strtotime("-1 month", strtotime($fecha)))."' AND YEAR(T10.ATRFEC)= ".$anio1." AND T9.ATRART=T1.AARCOD GROUP BY T9.ATRART ORDER BY T9.ATRART)AS CAN2 ,
									(SELECT SUM(T12.ATRCAN) AS ATRCAN FROM IV16FP T12 INNER JOIN IV15FP T13 ON ( T12.ACICOD=T13.ACICOD AND T12.AALCOD=T13.AALCOD AND T12.ATRCOD=T13.ATRCOD AND T12.ATRNUM=T13.ATRNUM ) INNER JOIN IV12FP T14 ON ( T13.ACICOD=T14.ACICOD AND T13.ATRCOD=T14.ATRCOD AND T14.ATRSIG='-') WHERE T13.ACICOD=T1.ACICOD AND T13.AALCOD=T1.AALCOD AND month(T13.ATRFEC)='".date("m",strtotime("-2 month", strtotime($fecha)))."' AND YEAR(T13.ATRFEC)= ".$anio2." AND T12.ATRART=T1.AARCOD GROUP BY T12.ATRART ORDER BY T12.ATRART  )AS CAN3, 
									(SELECT SUM(T15.ATRCAN) AS ATRCAN FROM IV16FP T15 INNER JOIN IV15FP T16 ON ( T15.ACICOD=T16.ACICOD AND T15.AALCOD=T16.AALCOD AND T15.ATRCOD=T16.ATRCOD AND T15.ATRNUM=T16.ATRNUM ) INNER JOIN IV12FP T17 ON ( T16.ACICOD=T17.ACICOD AND T16.ATRCOD=T17.ATRCOD AND T17.ATRSIG='-') WHERE T16.ACICOD=T1.ACICOD AND T16.AALCOD=T1.AALCOD AND month(T16.ATRFEC)='".date("m",strtotime("-3 month", strtotime($fecha)))."' AND YEAR(T16.ATRFEC)= ".$anio3." AND T15.ATRART=T1.AARCOD GROUP BY T15.ATRART ORDER BY T15.ATRART )AS CAN4 
								FROM iv40fp T1 
									INNER JOIN IV05FP T3 ON(T1.ACICOD=T3.ACICOD AND T1.AARCOD=T3.AARCOD AND T3.AARSTS='01') 
									INNER JOIN IV13FP T4 ON(T1.ACICOD=T4.ACICOD AND T1.AUMCOD=T4.AUMCOD) 
									left join iv39fp T5 ON(T1.ACICOD=T5.ACICOD AND T1.AARCOD=T5.AARCOD AND T1.AALCOD=T5.AALCOD) 
								WHERE T1.ACICOD='".$Compania."' AND T1.AALCOD='".$aalcod."'
								GROUP BY T1.ACICOD, T1.AALCOD, T1.AARCOD, T3.AARDES, T1.ALTCOD , T4.AUMDES,T1.AUMCOD, T5.aarstm 
								ORDER BY T3.AARDES 
								";
							
							$resultt=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111"));
								//echo $sql;
								$z=0;
								$tothon=0;
								$totest=0;
								$lin=1;
								$limitep=$_SESSION['solicitudlineasporpaginat'];
								$pag=1;
								$primero='S';
								
								while(odbc_fetch_row($resultt))
								{
									$jml = odbc_num_fields($resultt);
									$row[$z]["pagina"] =  $pag;
									for($i=1;$i<=$jml;$i++)
									{	
										$row[$z][odbc_field_name($resultt,$i)] =  odbc_result($resultt,$i);
									}
									$z++;
									if ($lin>=$limitep) 
									{
										$limitep+=$_SESSION['solicitudlineasporpaginat'];
										$pag++;
									}
									$lin++;
								}
								$totsol=($lin-1);
								$_SESSION['totalsolicitudes']=$totsol;
								$_SESSION['solicitudarreglo']=$row;
								$solicitudpagina=1;
								$_SESSION['solicitudpaginas']=$pag;
					}//fin de solicitudpagina
					/*se muestra la cantidad de elementos segun la solicitada en $solicitudpagina*/
					$paginat=$_SESSION['solicitudarreglo'];
			?>
<table width="100%" border="0">
<tr>
<td height="89"><h1>
<?php if($Compania=='14'){?>
	<img src="http://<?php echo $Direccionip; ?>/idasysv3/images/logoidacadef2005.png" width="280" height="68" />
<?php }else{ ?>
	<img src="http://<?php echo $Direccionip; ?>/idasysv3/images/logomeditronnuevo.png" width="280" height="68" />
<?php } ?>
   </h1>
  <h5>RIF:  <?php echo $Companiarif; ?></h5></td>
</tr>
<tr>
<td>

<table width="100%" id="background-image" >
	
    <thead>
    	<tr>
            <th colspan="9" scope="col"><h3>�	Resultado del c&aacute;lculo obtenido del Stock M&iacute;nimo en base al consumo del los &uacute;ltimos 3 Meses</h3></th>
        </tr>
        <tr>
            <th colspan="9" scope="col"><h3>Almac&eacute;n: <?php echo alamcen($aalcod, $Compania);?></h3></th>
        </tr>
        <?php 
			/*validar si piden solo stm */
			if($stm==1){//si 
				echo '  <tr>
							<th colspan="9" scope="col"><h3>Muestra Art&iacute;culos en Existencia Bajo Stock M&iacute;nimo</h3></th>
						</tr>';
			}else if($stm==2){
				echo '';
			}
		?>
        <tr>
            <th colspan="9" scope="col"><strong>Fecha:</strong> <?php echo $Fechaactual;?> ; <strong>Hora:</strong>  <?php echo $Horaactual;?></th>
        </tr>
        <tr>
            <th width="5%"  scope="col">Cod. Art&iacute;culo</th>
            <th width="30%"  scope="col">Descripci&oacute;n</th>
            <th width="5%" scope="col">Stock. M&iacute;nimo</th>
            <th width="5%" scope="col">Ubicaci&oacute;n</th>
            <th width="5%" scope="col">Unidad de Medida</th>
            <th width="5%" scope="col"><?php echo mesescrito( date("m",strtotime("-3 month", strtotime(date("d-m-Y")))) ); ?></th>
            <th width="5%" scope="col"><?php echo mesescrito( date("m",strtotime("-2 month", strtotime(date("d-m-Y")))) ); ?></th>
            <th width="5%" scope="col"><?php echo mesescrito( date("m",strtotime("-1 month", strtotime(date("d-m-Y")))) ); ?></th>
            <th width="5%" scope="col"><?php echo mesescrito( date("m") ); ?></th>
            <th width="5%" scope="col">Saldo Final</th>
            <th width="5%" scope="col">Nro. Orden de Compra</th>
        </tr>
    </thead>
   <tbody>
				<?php 
                    $show = false;
                    //print_r($paginat);
                    $pagact=$solicitudpagina;
                    for($g=0; $g < (count($paginat)); $g++)
                    {
                ?>
                <?php 
                      $salFinal = $paginat[$g]["SALANT"]+$paginat[$g]["ASLENT"]+$paginat[$g]["ASLSAL"];
                      
                      /*validar si piden solo stm */
                      if($stm==1){//si 
                          if($salFinal<=$paginat[$g]["AARSTM"])	{$show = true;}
                          else									{$show = false;}
                      }else if($stm==2){
                         $show = true; 
                      }
                      
                      if($show){
                ?>
                        <tr>
                            <!--<td><?php if($salFinal<=$paginat[$g]["AARSTM"]){echo "MINIMO";}else{echo "";} ?></td>-->
                            <td scope="col"><div><?php echo "<strong>".$paginat[$g]["AARCOD"]."</strong>";?></div></td>
                            <td scope="col"><div><?php echo $paginat[$g]["AARDES"];?></div></td>                
                            <td scope="col">
                                <div align="center">
                                    <?php echo number_format($paginat[$g]["AARSTM"],2,",",".");?>
                                </div>
                            </td>
                            <td scope="col" id="opciones">
                                <div><strong>
                                    <?php 
                                        $list = list_ubiart($cid, $Compania, $paginat[$g]["AALCOD"], $paginat[$g]["AARCOD"],2);
                                        if(trim($list)!=''){	echo $list;	}
                                        else {	echo ' - Sin Ubicaci�n';	}
                                     ?>
                                </strong></div>
                            </td>
                            <td scope="col"><div align="center"><?php echo $paginat[$g]["AUMDES"];?></div></td>
                            
                            <td scope="col"><div align="right"><?php echo number_format($paginat[$g]["CAN4"],2,",",".");?></div></td>
                            <td scope="col"><div align="right"><?php echo number_format($paginat[$g]["CAN3"],2,",",".");?></div></td>
                            <td scope="col"><div align="right"><?php echo number_format($paginat[$g]["CAN2"],2,",",".");?></div></td>
                            <td scope="col"><div align="right"><?php echo number_format($paginat[$g]["CAN1"],2,",",".");?></div></td>
                            
                            <td scope="col"><div align="right"><?php echo number_format(($salFinal),2,",",".");?></div>  </td>
                            <td scope="col">
                                    &nbsp;
                                    <!--<a rel="shadowbox;width=650;height=345" title="Detalle" href="detalle.php?&id=<?php //echo trim($paginat[$g]["AARCOD"]); ?>&fdesde=<?php// echo $desde; ?>&fhasta=<?php //echo $hasta; ?>">Detalle</a>-->
                                    <!--<a href="javascript:verdetalle('<?php //echo trim($paginat[$g]["AARCOD"]); ?>','<?php //echo $desde; ?>','<?php //echo $hasta; ?>', '<?php //echo $aalcod; ?>', <?php //echo $paginat[$g]["SALANT"]; ?>,'<?php //echo trim($paginat[$g]["AUMDES"]);?>' )"><img src="../images/ver.png" title="Ver Detalle" width="31" height="28" border="0"></a>		-->
                            </td>
                        </tr>
                <?php } }?>      
    </tbody>

</table>
</td>
</tr>
</table>
</body>
</html>
