<?php session_start();
include("../conectar.php");
include("../JSON.php");
$user=$_REQUEST["usuario"];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>agregar usuario</title>
<link href="../<?php echo $css; ?>" rel="stylesheet" type="text/css" media="screen" />
<script language="JavaScript" src="../javascript/jquery.js"></script>
<script language="JavaScript" src="../javascript/javascript.js"></script>
<script language="JavaScript" src="javascript.js"></script>
<script language="JavaScript" src="../calendario/javascripts.js"></script>
</head>
<body bgcolor="#FFFFFF" class="sinbody" onload="javascript:usuarioeditar('<?php echo $user;?>');cargarparametros(document.getElementById('auscod').value)">
<div id="editarusuariodiv">
  <form id="usuarioeditarform" name="usuarioeditarform"  class="form" method="post" action="">  
    <table width="100%"  border="0">
      <tr>
        <th width="20%" scope="col"><label>Usuario</label></th>
        <th width="36%" id="wsauscod" align="left" scope="col"></th>
        <th colspan="2"  class="Estilo5" scope="col"><div align="left">
          <input name="auscod" type="hidden" id="auscod"></div></th>
      </tr>
      <tr>
        <th scope="col"><label>Primer Apellido </label></th>
        <th scope="col" id="wsausap1" align="left" ><div align="left"></div></th>
        <th colspan="2" id="erreausap1" class="Estilo5" scope="col">&nbsp;</th>
      </tr>
      <tr>
        <th scope="col"><label>Segundo Apellido </label></th>
        <th scope="col" id="wsausap2" align="left" ><div align="left"></div></th>
        <th colspan="2" id="erreausap2" class="Estilo5" scope="col">&nbsp;</th>
      </tr>
      <tr>
        <th scope="col"><label>Primer Nombre </label></th>
        <th scope="col" id="wsausno1" align="left" ><div align="left"></div></th>
        <th colspan="2" id="erreausno1" class="Estilo5" scope="col">&nbsp;</th>
      </tr>
      <tr>
        <th scope="col"><label>Segundo Nombre </label></th>
        <th scope="col" id="wsausno2" align="left" ><div align="left"></div></th>
        <th colspan="2" id="erreausno2" class="Estilo5" scope="col">&nbsp;</th>
      </tr>
	  <tr>
        <th scope="col"><label>Usuario Supervisor </label></th>
        <th scope="col" id="wsaussup" align="left" ><div align="left"></div></th>
        <th colspan="2" id="erreaussup" class="Estilo5" scope="col">&nbsp;</th>
      </tr>
	  <tr>
        <th scope="col"><label>Email</label></th>
        <th scope="col"  id="wsaemmai" align="left" ><div align="left"></div></th>
        <th colspan="2" id="erraemmai" class="Estilo5" scope="col">&nbsp;</th>
      </tr>
      <tr>
      	<td colspan="4">
        
            <table width="100%"  border="0" >
                <tr>
                    <td width="100%" colspan="3" scope="col"><h3>Parámetros Adicionales: </h3>
                    <span class="header"></span></td>
                </tr>
                <tr>
                    <td>
                    <div id="parametros"></div>
                    </td>
                </tr>
            
            </table>
        
        </td>
      </tr>
      <tr>
        <th scope="col">&nbsp;</th>
        <th scope="col"></th>
        <th width="23%"  scope="col">
          <input name="cancelar" type="button" onclick="parent.location.reload();parent.Shadowbox.close();"" " value="Salir" id="cancelar" />
        </th>
        <th width="21%"  scope="col">
          <input type="button" name="submit" id="submit" value="Cambiar" onclick="usuarioeditarvalidar()" />
        </th>
      </tr>
      
    </table>
  </form>
  </div>
  <div align="center" id="usuarioeditaraftsav">
  <br />
  <br />
  <br />
  <br />
  <br />
  <br />
  Registro Actualizado con exito !
  <br />
  <br />
  <br />
  <input name="Submit3" type="button" onClick="parent.location.reload();parent.Shadowbox.close();"" " value="Cerrar">
  </div>
</body>
</html>