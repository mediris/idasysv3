<?php 
/*
 *jDavila
 *14/02/2012
 */
session_start();
include("../conectar.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
<title>Idasys V3</title>
<link href="../<?php echo $css; ?>" rel="stylesheet" type="text/css" media="screen" />
<script language="JavaScript" src="../javascript/javascript.js"></script>
<script language="JavaScript" src="javascript.js"></script>
<script language="JavaScript" src="../javascript/jquery.js"></script>
<link href="../shadowbox.css"  rel="stylesheet" type="text/css"/>
<script src="../shadowbox.js" type="text/javascript"/> </script>
<script language="JavaScript" type="text/JavaScript">
Shadowbox.init({
overlayOpacity: "0.5"
});
</script>
</head>
<body>
<div id="wrapper">
  <?php 
  		include("../superior.php");
  		?>
  <div id="page">
      <?php include("../validar.php");  		?>

<div id="sidebar">
	    <ul>
	      <li>
	        <h2>Estad&iacute;sticas</h2>
	        <p>Aqu&iacute; se reflejar&aacute;n estad&iacute;sticas particulares seg&uacute;n en la opci&oacute;n del sistema que se encuentre</p>
          </li>
	      <li>
	        <h2>Categories</h2>
	        <ul>
	          <li><a href="#">Aliquam libero</a></li>
	          <li><a href="#">Consectetuer adipiscing elit</a></li>
	          <li><a href="#">Metus aliquam pellentesque</a></li>
	          <li><a href="#">Suspendisse iaculis mauris</a></li>
	          <li><a href="#">Urnanet non molestie semper</a></li>
	          <li><a href="#">Proin gravida orci porttitor</a></li>
            </ul>
          </li>
	      <li>
	        <h2>Blogroll</h2>
	        <ul>
	          <li><a href="#">Aliquam libero</a></li>
	          <li><a href="#">Consectetuer adipiscing elit</a></li>
	          <li><a href="#">Metus aliquam pellentesque</a></li>
	          <li><a href="#">Suspendisse iaculis mauris</a></li>
	          <li><a href="#">Urnanet non molestie semper</a></li>
	          <li><a href="#">Proin gravida orci porttitor</a></li>
            </ul>
          </li>
	      <li>
	        <h2>Archives</h2>
	        <ul>
	          <li><a href="#">Aliquam libero</a></li>
	          <li><a href="#">Consectetuer adipiscing elit</a></li>
	          <li><a href="#">Metus aliquam pellentesque</a></li>
	          <li><a href="#">Suspendisse iaculis mauris</a></li>
	          <li><a href="#">Urnanet non molestie semper</a></li>
	          <li><a href="#">Proin gravida orci porttitor</a></li>
            </ul>
          </li>
      </ul>
	    </div>
<!--	  <div id="content" style="background-image: url(fondoimagen.png);">-->
   	  <div id="content" >
		<div class="post">
			<table width="100%"   border="0">
              <tr>
                <td width="84%" scope="col"><h1 align="center" class="title">MENUES</h1></td>
                <td width="16%" scope="col"><div align="left">
                  <table width="100%"  border="0">
                    <tr>
                      <th width="36%" scope="col"><a rel="shadowbox;width=650;height=345" title="Agregar Menu" href="../menues/menuesfagregar.php"><img src="../images/agregar.gif" alt="Agregar" width="25" height="25" border="0" /></a></th>
                      <th width="30%" scope="col"><img src="../images/excel.jpg" alt="" width="25" height="25" /></th>
                      <th width="16%" scope="col"><img src="../images/pdf.jpg" alt="" width="25" height="25" /></th>
                      <th width="18%" scope="col"><img src="../images/impresora.gif" alt="" width="25" height="25" /></th>
                    </tr>
                  </table>
                </div></td>
              </tr>
            </table>
			
			<div class="entry">
<table width="96%" id="background-image" >
	
  <thead>
  	<tr>
    	<th width="15%" height="32" scope="col">Opci&oacute;n </th>
    	<th width="32%" scope="col">Descripci&oacute;n </th>
    	<th width="15%" scope="col">Sec </th>
    	<th width="14%" scope="col">Opci&oacute;n Precedente </th>
    	<th width="10%" scope="col">Link </th>
        <th width="10%" scope="col">Status </th>
		<th width="14%" colspan="4" scope="col">opciones</th>
  	</tr>
  </thead>
    <tbody >
	          <?php $sql="SELECT AOPCOD, AOPDES, AOPSEC, AOPPRE, AOPLNK, AOPSTS, AMDCOD FROM mb22fp WHERE AMDCOD ='$modulo' ORDER BY AOPPRE, AOPSEC ";
				$result=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111")); 
  				while(odbc_fetch_row($result)){			?>
                <tr>
                    <td scope="col"><div  ><strong><?php echo odbc_result($result,1);?></strong></div></td>                
                    <td scope="col"><div  ><strong><?php echo odbc_result($result,2);?></strong></div></td>
                    <td scope="col"><div  ><?php echo odbc_result($result,3);?></div></td>
                    <td scope="col"><div  ><?php echo odbc_result($result,4);?></div></td>
                    <td scope="col"><div  ><?php echo odbc_result($result,5);?></div></td>
                    <td scope="col"><div  ><?php echo status('AOPSTS',odbc_result($result,6));?></div></td>      
                    <td scope="col">
                    
                    <ul id="opciones">
                        <li><a rel="shadowbox;width=650;height=345" title="Editar Menu" href="../menues/menuesfeditar.php?menu=<?php echo trim(odbc_result($result,1));?>&modulo=<?php echo trim(odbc_result($result,7)); ?>"><img src="../images/editar.gif" alt="Editar" width="15" height="15" border="0" /></a>
                        <!--<td width="34" scope="col"><a href="javascript:menueseditar('<?php echo trim(odbc_result($result,7))."','".trim(odbc_result($result,1));?>')"><img src="../imagenes/editar.gif" alt="Editar informaci&oacute;n" width="15" height="15" border="0"></a></td>-->
                        </li>
                        <!--<li><a rel="shadowbox;width=650;height=345" title="Autorizar Menu" href="../Copia de usuarios/usuariofautorizar.php?usuario=<?php echo trim(odbc_result($result,1));?>"><img src="../images/autorizaciones.gif" alt="Autorizar" width="15" height="15" border="0" /></a>
                        </li>-->
                        <!--<li>
                    <a href="javascript:usuarioverhistorial('<?php echo trim(odbc_result($result,1));?>')"><img src="../images/historial.gif" alt="Ver Historial" width="15" height="15" border="0" /></a>
                        </li>-->
                        <li>
		                    <a href="javascript:eliminar('<?php echo trim($modulo)."','".trim(odbc_result($result,1));?>')"><img src="../images/eliminar.gif" alt="Eliminar" width="15" height="15" border="0" /></a>
                        </li>
                      </ul>
                    </td>
                    
                      </tr>
			  <?php } ?>
  </tbody>

</table>
		  </div>
	  </div>
	</div>
		<!-- end #content -->
		<!-- end #sidebar -->
	<div style="clear: both;">&nbsp;</div>
  </div>
	<!-- end #page -->
</div>
	<div id="footer">
		<div id="opcionesmasusadas"><?php include("../opcionesmasusadasphp.php");?></div>
	</div>
	<!-- end #footer -->
</body>
</html>
