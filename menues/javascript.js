/*
jDavila
13/02/12
*/
function agregar()
{
	document.menuesagregarform.amdcod.value="";
	document.menuesagregarform.aopcod.value="";
	document.menuesagregarform.aopdes.value="";
	document.menuesagregarform.aoplnk.value="";
	document.menuesagregarform.aopsec.value="";
	document.menuesagregarform.aoppre.value="";
	$("#menugregaraftsav").hide(1);
}

/*
jDavila
13/02/12
*/
function agregarvalidar(){	
	var contenido= "";
	var elemento;
	document.body.style.cursor="wait";

$.ajax({
	   	async: false,
        type: "POST",
		dataType: "JSON",
        url: "menuesagregarvalidarphp.php",
        data: $("#menuesagregarform").serialize(),
        success: function(datos){
			vdata = parseJSONx(datos);}							
});			
		sierror='N';
			for(i in vdata)	{
			if(vdata[i].campo) 	{
			document.getElementById(vdata[i].campo).innerHTML=vdata[i].msg;
			sierror='S';
							}
												}
	document.body.style.cursor="default";
	if(sierror=='N') 		
	{
		$("#agregarmenuesdiv").hide(1000);
		$("#menugregaraftsav").show(1000);
	}


}
/*
jDavila
13/02/12
*/
function editar(modulo, opcion) {

	var param = [];
	param['aopcod']=opcion;
	param['amdcod']=modulo;
	/*cual es la diferencia*/
	ejecutasqlp("menuesinformacionphp.php",param);

	for(i in gdata){
		document.menueseditarform.aopcod.value=gdata[i].AOPCOD;
		document.menueseditarform.amdcod.value=gdata[i].AMDCOD;
		document.getElementById("wsaopcod").innerHTML=gdata[i].AOPCOD;
		document.getElementById("wsamdcod").innerHTML=gdata[i].AMDCOD+' '+gdata[i].AMDDES;
		document.menueseditarform.aopdes.value=gdata[i].AOPDES;
		document.menueseditarform.aoplnk.value=gdata[i].AOPLNK;
		document.menueseditarform.aopsec.value=gdata[i].AOPSEC;
		var supervisor=gdata[i].AOPPRE;
		for(i=0;i<document.menueseditarform.aoppre.length;i++)
		{
			var supervisors=document.menueseditarform.aoppre.options[i].value;
			if(supervisors==supervisor)
			{
				document.menueseditarform.aoppre.options[i].selected=true;
			}
		} 
	};
	$("#menuesditaraftsav").hide(1);

}

/*
 *jDavila
 *14/02/2012
 */
function editarvalidar(){	
	var contenido= "";
	var elemento;
	document.body.style.cursor="wait";
	$.ajax({
			async: false,
			type: "POST",
			dataType: "JSON",
			url: "menueseditarvalidarphp.php",
			data: $("#menueseditarform").serialize(),
			success: function(datos){
						vdata = parseJSONx(datos);}							
	});			
	var sierror='N';
	for(i in vdata)	{
		if(vdata[i].campo) 	{
			document.getElementById(vdata[i].campo).innerHTML=vdata[i].msg;
			sierror='S';
		}
	}
	document.body.style.cursor="default";
	if(sierror=='N') 		
	{
		$("#editarmenudiv").hide(1000);
		$("#menuesditaraftsav").show(1000);
	}
}

/*
 *jDavila
 *14/02/2012
 */
function eliminar(modulo, opcion) {
	if (confirm('Seguro que desea borrar el menu ' + opcion + '?'))
	{
		var param = [];
		param['opcion']=opcion;
		param['modulo']=modulo;
		ejecutasqld("menueseliminar.php",param);
		location.reload();
	}
}