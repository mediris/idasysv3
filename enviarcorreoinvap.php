<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Envio de Correos</title>

<script language="JavaScript" src="javascript/javascript.js"></script>
<script language="JavaScript" src="javascript/jquery.js"></script>

<script>

	$.ajax({
		url: "consultainventario/exportarapdf_invap.php",
		beforeSend: function () {
                        $("#pdf").html("Iniciando proceso de Creacion de PDF");
        },
		success: function(datos) {
			vdata = parseJSONx(datos);
			if(vdata.datos==1){
				$("#pdf").html("PDF creado de forma correcta");
				$.ajax({
					url: "procesocorreoinvap.php",
					 beforeSend: function () {
                        $("#correo").html("Iniciando proceso de envio de correos");
                	},
					complete: function() {
						$("#correo").html("Envio de correo finalizado");
						window.close();
					}
				});
			}
			//window.close();
		}
	});
	
	/*
	$.ajax({
		url: "procesocorreoinvap.php",
		complete: function() {
			window.close();
		}
	});
	*/
</script>
</head>

<body>
<div id="pdf"></div>
<div id="correo"></div>
</body>
</html>
