<?php

class confirmar	{
/***********************************/
/*INICIO AREA CONFIRMAR TRANSACCION*/
/*
 * jDavila
 * 23/05/12
 * entradas:
 *  $acicod, COMPAÑIA
 *  $cid, CONEXION A BD
 *  $aalcod, ALMACEN
 *  $atrnum, NUMERO TRANSACCION
 *  $atrcod, CODIGO TRANSACION
 *  $aarcod, CODIGO ARTICULO
 *  $aardes, DESCRIPCION ARTICULO
 * @ACTUALIZA DETALLE DE TRANSACCION (iv16fp) A '2'
 */
 function updatestatusdetalletransaccion($acicod, $cid, $aalcod, $atrnum, $atrcod, $aarcod, $aardes){
	/*include_once("../conectar.php");*/
	$sql5 ="update iv16fp set atdsts='02' WHERE ACICOD ='$acicod' and AALCOD ='$aalcod' and ATRNUM='$atrnum' and ATRCOD ='atrcod' and atrart='$aarcod'";
	echo "&nbsp;&nbsp;Actualizando estatus de transaccion($atrcod-$atrnum-$aalcod); art&iacute;culo <strong>$aardes</strong> ($aarcod)...<br /><br />";
	$result5=odbc_exec($cid,$sql5)or die(exit("Error en odbc_exec 5"));			
	auditoriagrabar($modulo,"iv16fp","$Compania;$aalcod;$atrcod;$atrnum;$aarcod","cambio IV16FP $aarcod;$aalcod;$atrcod;$atrnum;$fechatrans");
 }
 
/*
 * jDavila
 * $Compania, COMPAÑIA
 * $cid, CONEXION
 * $Usuario, CODIGO USUARIO
 * $atrcod, CODIGO TRANSACCION
 * $aalcod, CODIGO ALMACEN
 * $atrnum, NUMERO TRANSACCION
 * $imprimir='1' ; OPCIONAL, 1=MUESTRA MENSAJE, 0=NO MUESTRA MENSAJES
 * @CONFIRMACION Y ACTUALIZACION DE STATUS DE LAS TRANSACCIONES
 */

 function confirmartransaccion($Compania, $cid, $Usuario, $atrcod, $aalcod, $atrnum, $imprimir='1', $modulo='IDAS')
 {
	/*busco el detalle*/
	if($imprimir=='1') echo "Cargando...<br />";
	$sql1 ="SELECT T1.ACICOD, T1.AALCOD, T1.ADPCOD, T1.ATRCOD, T1.ATRNUM, T1.ATRDES, T1.ATRFEC, T1.ATRHOR, T1.ATRMOR, T1.AUSCOD, T1.ATRSTS, 
	T2.ADPCOD, T2.ATRSEC,T2.ATRART, T2.ATRLOT, T2.ATRSER, T2.ATRCAN, T2.ATRUMB, T2.ATRUMH, T2.ATRFAC, T2.ATRCUT, T2.ATRCUS, T2.ATREAC, T2.ATREAA, 
	T2.ATREAL, T2.ATDSTS, T3.ATRDES AS ATRTDES , T4.AARDES, T5.AUMDES, T6.AALDES, T3.ATRCOD, T3.ATRSIG
			FROM  IV15FP T1 
			left join (IV16FP T2 
						inner join IV05FP T4 on (T2.ATRART=T4.AARCOD AND T4.ACICOD=T2.ACICOD) 
						INNER JOIN IV13FP T5 ON T5.ACICOD=T2.ACICOD AND T5.AUMCOD=T2.ATRUMH) on T1.AALCOD= T2.AALCOD AND T1.ATRNUM=T2.ATRNUM AND T1.ATRCOD=T2.ATRCOD  AND T1.ACICOD=T2.ACICOD 
						LEFT JOIN IV07FP T6 ON (T6.AALCOD=T1.AALCOD AND T6.ACICOD=T2.ACICOD), 
			IV12FP T3 WHERE
			 T1.ACICOD='$Compania' AND T1.ATRCOD=$atrcod AND T1.ATRNUM=$atrnum AND
			 T1.AUSCOD='$Usuario' AND T1.AALCOD='$aalcod' AND T1.ATRCOD=T3.ATRCOD AND T1.ACICOD=T3.ACICOD";

	$result1=odbc_exec($cid,$sql1)or die(exit("Error en odbc_exec 1")); 
	if($imprimir=='1') echo "Fin de Carga...<br />";
	$i=0;
	/*POR CADA ARTICULO...*/
	if($result1)
	{
		if($imprimir=='1')echo "<hr /><br />";
		while(odbc_fetch_row($result1))
		{
				$bandera1=1;
				$aarcodtem = odbc_result($result1, 'ATRART');
				$aardestem = odbc_result($result1, 'AARDES');
				$fechatrans =  odbc_result($result1, 'ATRFEC');
				//$fechatrans = "25.05.2012";
				$signo =  odbc_result($result1, 'ATRSIG');
				
				/*UNIDAD DE MEDIDAS*/
				$atrumb =  odbc_result($result1, 'ATRUMB');
				$atrumh =  odbc_result($result1, 'ATRUMH');
				$atrfac =  odbc_result($result1, 'ATRFAC');
				
				
				$salida = '0';
				$entrada = '0';
				if(trim($signo)=='-'){
					$salida =  (odbc_result($result1, 'ATRCAN')*-1)/$atrfac;
					$entrada = '0';
				}else if(trim($signo)=='+'){
					$entrada =  (odbc_result($result1, 'ATRCAN'))/$atrfac;
					$salida = '0';
				}
	
				
				if($imprimir=='1')echo "Buscar art <strong>". $aardestem." </strong>(".$aarcodtem.") en listado del dia..<br />";
			/*busco existencia de cabecera*/
				$sql2 =" SELECT ACICOD, AALCOD, AARCOD, ALTCOD, ASLFEF, ASLSAA, ASLENT,   
						ASLSAL, ASLSAF FROM iv40fp WHERE ACICOD ='$Compania' and AALCOD ='$aalcod' 
						and AARCOD='$aarcodtem' and ALTCOD ='' and ASLFEF ='$fechatrans'   ";
			
				$result2=odbc_exec($cid,$sql2)or die(exit("Error en odbc_exec 1"));
			/*si*/
				//if(odbc_num_rows($result2)>0)
				if(odbc_result($result2,1))
				{
	
					if($imprimir=='1')echo "&nbsp;&nbsp;Encontrado...<br />";
					$salFin=0;
					/*actualizo cambiando el saldo final*/	
					$salAnt=odbc_result($result2, 'ASLSAF');
					if($imprimir=='1')echo "&nbsp;&nbsp;Saldo anterior: $salAnt <br />";
					if(!empty($entrada))
					{
						$salFin=$salAnt+$entrada;
						if($imprimir=='1')echo "&nbsp;&nbsp;Entrada: $entrada <br />";
						if($imprimir=='1')echo "&nbsp;&nbsp;Saldo fin: $salFin <br />";
						$sql2 =" UPDATE IV40FP SET  ASLENT=ASLENT+$entrada, ASLSAF=$salFin 
						where
						 ACICOD ='$Compania' and AALCOD ='$aalcod' and 
						 AARCOD='$aarcodtem' and ALTCOD ='' and 
						 ASLFEF ='$fechatrans'   ";
						 if($imprimir=='1')echo "&nbsp;&nbsp;Actualizando entrada...<br />";
					}
					else if(!empty($salida))
					{
						$salFin=$salAnt+$salida;
						if($imprimir=='1')echo "&nbsp;&nbsp;Salida: $salida <br />";
						if($imprimir=='1')echo "&nbsp;&nbsp;Saldo fin: $salFin <br />";
						$sql2 =" UPDATE IV40FP SET ASLSAL=ASLSAL+$salida, ASLSAF=$salFin 
						where
						 ACICOD ='$Compania' and AALCOD ='$aalcod' and 
						 AARCOD='$aarcodtem' and ALTCOD ='' and 
						 ASLFEF ='$fechatrans'   ";
						 if($imprimir=='1')echo "&nbsp;&nbsp;Actualizando salida...<br />";
					}
					$result2=odbc_exec($cid,$sql2)or die(exit("Error en odbc_exec 2"));			
					auditoriagrabar($modulo,"IV40FP","$Compania;$aalcod;$aarcodtem;$fechatrans","cambio IV40FP $salFin;$aalcod;$aarcodtem;$fechatrans");
					/*cambiando estatus detalle*/
					updatestatusdetalletransaccion($Compania, $cid, $aalcod,$atrnum,$atrcod,$aarcodtem,$aardestem);
				}
			/*no*/
				else{
					if($imprimir=='1')echo "&nbsp;&nbsp;**No Encontrado...<br />";
					if($imprimir=='1')echo "&nbsp;&nbsp;Buscando saldo anterior...<br />";
					/*busco saldo anterior*/
					$sql3 ="SELECT ASLSAF FROM iv40fp WHERE ACICOD ='$Compania' and AALCOD ='$aalcod' 
						and AARCOD='$aarcodtem' and ALTCOD ='' ORDER BY ASLFEF desc  ";
					$result3=odbc_exec($cid,$sql3)or die(exit("Error en odbc_exec 3"));			
					if(odbc_result($result3,'ASLSAF'))
					{
						$salAnt = odbc_result($result3, 'ASLSAF');
						if($imprimir=='1')echo "&nbsp;&nbsp;Saldo anterior: $salAnt <br />";
					}
					else
					{
						$salAnt=0;
						if($imprimir=='1')echo "&nbsp;&nbsp;Saldo anterior: $salAnt <br />";
					}
					/*guardo*/
					$salFin = $salAnt + $entrada + $salida;/*calculo saldo final*/
					if($imprimir=='1')echo "&nbsp;&nbsp;entrada: $entrada <br />";
					if($imprimir=='1')echo "&nbsp;&nbsp;salida: $salida <br />";
					if($imprimir=='1')echo "&nbsp;&nbsp;Saldo fin: $salFin <br />";
					if($imprimir=='1')echo "Guardando...<br />";
					$sql4="INSERT INTO IV40FP (ACICOD, AALCOD, AARCOD, ALTCOD, ASLFEF, ASLSAA, ASLENT,ASLSAL, ASLSAF, AUMCOD)VALUES('$Compania', '$aalcod', '$aarcodtem', '$lote', '$fechatrans', $salAnt, $entrada, $salida, $salFin, '$atrumb')";
					$result4=odbc_exec($cid,$sql4)or die(exit("Error en odbc_exec 4"));			
					auditoriagrabar($modulo,"IV40FP","$Compania;$aalcod;$aarcodtem;$fechatrans","agregando IV40FP $salFin;$aalcod;$aarcodtem;$fechatrans");
	
					/*cambiando estatus detalle*/
					updatestatusdetalletransaccion($Compania,$cid,$aalcod,$atrnum,$atrcod,$aarcodtem,$aardestem);
				}
				if($imprimir=='1')echo "<hr /><br />";
		}
	}
	else
	{
		if($imprimir=='1')echo "No se puede procesar....<br />";
		if($imprimir=='1')echo "<hr /><br />";
	}
	/*actualizando status de la transaccion....*/
	if($bandera1==1)
	{
		$sql5 ="UPDATE IV15FP SET ATRSTS='02' WHERE ACICOD='$Compania' and AALCOD='$aalcod' and ATRCOD ='$atrcod' and ATRNUM ='$atrnum'";
		if($imprimir=='1')echo "&nbsp;&nbsp;Actualizando estatus de transaccion($atrcod-$atrnum-$aalcod)...<br />";
		$result5=odbc_exec($cid,$sql5)or die(exit("Error en odbc_exec 5"));			
		auditoriagrabar($modulo,"IV40FP","$Compania;$aalcod;$atrcod;$atrnum","cambio IV15FP $aalcod;$atrcod;$atrnum;$fechatrans");
		if($imprimir=='1')echo "<hr /><br />";
	}
 }
 
 
 /*
  * jDavila
  * 24/05/2012 
  * $aslctr, reserva; se debe enviar con signo (+ ó -)
  * $compania, COMPANIA
  * $aalcod, CODIGO ALMACEN
  * $aarcod, CODIGO ARTICULO
  * $altcod, LOTE
  * $aslfef, FEHCA
  * $cid, CONEXION
  * $modulo MODULO(VARIABLE GLOBAL)
  */
 function reservadetalle($aslctr, $compania, $aalcod, $aarcod, $altcod, $aslfef, $atrumb, $cid, $modulo)
 {
	 	
	/*busco existencia de cabecera*/
	$sql1 =" SELECT ACICOD, AALCOD, AARCOD, ALTCOD, ASLFEF, ASLSAA, ASLENT,   
			ASLSAL, ASLSAF FROM iv40fp WHERE ACICOD ='$compania' and AALCOD ='$aalcod' 
			and AARCOD='$aarcod' and ALTCOD ='$altcod' and ASLFEF ='$aslfef'";
	$result1=odbc_exec($cid,$sql1)or die(exit("Error en odbc_exec 1"));
	
	/*si*/
	if(odbc_result($result1,1))
	{
		 echo $sql2 ="UPDATE IV40FP SET ASLCTR=ASLCTR+$aslctr
				where
				 ACICOD='$compania' and AALCOD ='$aalcod' and 
				 AARCOD='$aarcod' and ALTCOD ='$altcod' and 
				 ASLFEF='$aslfef' ";
		$result2=odbc_exec($cid,$sql2)or die(exit("Error en odbc_exec 2"));			
		auditoriagrabar($modulo,"IV40FP","$Compania;$aalcod;$aarcod;$aslfef","cambio IV40FP $salFin;$aalcod;$aarcod;$aslfef");
		
		$sql3 =" SELECT ASLSAA, ASLENT, ASLSAL, ASLSAF, ASLCTR 
			FROM iv40fp WHERE ACICOD ='$compania' and AALCOD ='$aalcod' 
			and AARCOD='$aarcod' and ALTCOD ='$altcod' and ASLFEF ='$aslfef'";
		$result3=odbc_exec($cid,$sql3)or die(exit("Error en odbc_exec 3"));
		$aslsaa = odbc_result($result3,'ASLSAA');
		$aslctr = odbc_result($result3,'ASLENT');
		$aslctr = odbc_result($result3,'ASLSAL');
		$aslctr = odbc_result($result3,'ASLSAF');
		$aslctr = odbc_result($result3,'ASLCTR');
		//if(){}
		
	}
	/*no*/
	else
	{
		/*aqui*** */
		 echo $sql4="INSERT INTO IV40FP (ACICOD, AALCOD, AARCOD, ALTCOD, ASLFEF, ASLSAA, ASLENT, ASLSAL, ASLSAF, ASLCTR, AUMCOD)VALUES
							('$compania', '$aalcod', '$aarcod', '$lote', '$aslfef', 0, 0, 0, 0, '$aslctr', '$atrumb')";
		$result4=odbc_exec($cid,$sql4)or die(exit("Error en odbc_exec 4"));			
		auditoriagrabar($modulo,"IV40FP","$Compania;$aalcod;$aarcodtem;$fechatrans","agregando IV40FP $salFin;$aalcod;$aarcodtem;$fechatrans");
	}
 }
 
 /*FIN AREA CONFIRMAR TRANSACCION*/
 /********************************/
	
}/*fin de clase*/

?>