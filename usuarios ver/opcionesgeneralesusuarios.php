<?php session_start();
include("../conectar.php");
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="nl" lang="nl">
  <head>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  <link href="../css/estilos1.css" rel="stylesheet" type="text/css">
  <script language="JavaScript" src="../javascript/javascript.js"></script>
  <script language="JavaScript" src="../javascript/jquery.js"></script>
  <script language="JavaScript" src="../calendario/javascripts.js"></script>
  <title>GABC, Tu Administraci&oacute;n</title>
  <script language="JavaScript" type="text/JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
//-->
  </script>
  </head>

<body id="index" onload="menuizquierdo('0900000')">
  <div>
<!-- start header -->
<?php include("../superior.php");?>
<?php include("../validar.php");?>

<!-- start page -->
<div id="container" style="position:absolute; width:919px; height:262px; z-index:1; left: 1px; top: 124px;">
	<!-- start content -->
	<div id="content" style="position:absolute; width:719px; height:368px; z-index:1; left: 217px; top: 1px;">
		<div class="post">
			<table width="100%"  border="0">
              <tr>
                <td width="76%" scope="col"><h1 align="center" class="title">USUARIOS</h1></td>
                <td width="24%" scope="col"><div align="left">
                  <table width="100%"  border="0">
                    <tr>
                      <th width="36%" scope="col"><span class="iconos"><img src="../imagenes/agregar.gif" alt="Agregar" width="25" height="25" onclick="usuarioagregar()"></span></th>
                      <th width="30%" scope="col">&nbsp;</th>
                      <th width="16%" scope="col">&nbsp;</th>
                      <th width="18%" scope="col">&nbsp;</th>
                    </tr>
                  </table>
                </div></td>
              </tr>
            </table>
			
			<div class="entry">
<table id="tablausuario" class="tablapasoscss" width="100%"  border="0">
	
  <thead>
  	<tr>
    	<th width="9%" height="32" scope="col">Usuario</th>
    	<th width="37%" scope="col">Apellidos y Nombres </th>
    	<th width="20%" scope="col">Ultima Conexi&oacute;n </th>
    	<th width="12%" scope="col">Supervisor</th>
    	<th width="12%" scope="col">Status</th>
		<th width="10%" scope="col">opciones</th>
  	</tr>
  </thead>
    <tbody>
	          <?php $sql="Select * from mb20fp order by auscod ";
				$result=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111")); 
  				while(odbc_fetch_row($result)){			?>
  	<tr>
    	<td  scope="col"><div ><strong>
    	  <?php echo odbc_result($result,1);?>
  	  </strong></div>    	  <div align="center">
		<?php if (odbc_result($result,21)=='F') { echo '<img src="../imagenes/femenino.gif" width="20" height="20">' ;}
		else {echo '<img src="../imagenes/masculino.gif" width="20" height="20"> '; }?>
              </div></td>                
    	<td scope="col"><?php echo odbc_result($result,2)." ".odbc_result($result,3).",".odbc_result($result,4);?></td>
    	<td scope="col"><?php echo fecdma(odbc_result($result,18),'amd','.')." - ".odbc_result($result,19);?></td>
    	<td scope="col"><?php echo odbc_result($result,13);?></td>
    	<td scope="col"><?php echo status('AUSSTS',odbc_result($result,20));?></td>
		<td valign="top" scope="col">
          <table width="100%" height="22%"  border="0">
            <tr>
              <th width="34" scope="col"><a href="javascript:usuarioeditar('<?php echo trim(odbc_result($result,1));?>')"><img src="../imagenes/editar.gif" alt="Editar informaci&oacute;n" width="15" height="15" border="0"></a></th>
              <th width="35" scope="col"><a href="javascript:usuarioeditaraut('<?php echo trim(odbc_result($result,1));?>')"><img src="../imagenes/autorizaciones.gif" alt="Editar Autorizaci&oacute;n" width="15" height="15" border="0"></a></th>
            </tr>
            <tr>
              <td height="28"><a href="javascript:usuarioverhistorial('<?php echo trim(odbc_result($result,1));?>')"><img src="../imagenes/historial.gif" alt="Ver Historial" width="15" height="15" border="0"></a></td>
              <td><a href="javascript:usuarioeliminar('<?php echo trim(odbc_result($result,1));?>')"><img src="../imagenes/eliminar.gif" alt="Eliminar" width="15" height="15" border="0"></a></td>
            </tr>
          </table>  	</td>
          </tr>
			  <?php } ?>
  </tbody>

</table>
		  </div>
	  </div>
	</div>
	<!-- end content -->
	<!-- start sidebar -->
	<!-- end sidebar -->

	<div style="clear: both;">&nbsp;
	  <div id="izquierdo" style="position:absolute; width:200px; height:368px; z-index:1; left: 1px; top: 1px;">
	  	<div id="sidebar">
	  <ul>
			
			<li>
				<h2>Opciones Generales:</h2>
				<div id="opciones">
				</div>
			</li>
      </ul>
	</div>

	  		<?php if ($Usuario!="") { ?> 
<div id="noticiayeventos" >
<iframe src="../noticias/noticiasyeventos.php" width=200px height=117px scrolling="NO" frameborder="0"></iframe>
</div>
 <?php }  ?>
	  </div>
	</div></div>
	<div id="agregarusuariodiv" class="white_content">
	  <div>

   
  <form id="usuarioagregarform" name="usuarioagregarform" method="post" action="">
  <fieldset id="form">
<legend>Agregar Usuario</legend>
  
   
    <table width="100%"  border="0">
	
      <tr>
        <th width="20%" scope="col"><label>Usuario</label></th>
        <th width="36%" scope="col"><div align="left">
          <input name="auscod" type="text" id="auscod" size="10" maxlength="10" onKeyUp="usuarioagregarform.auscod.value=usuarioagregarform.auscod.value.toUpperCase()">
        </div></th>
        <th colspan="2" id="errauscod" class="Estilo5" scope="col">&nbsp;</th>
      </tr>
      <tr>
        <th scope="col"><label>Primer Apellido </label></th>
        <th scope="col"><div align="left">
          <input name="ausap1" type="text" id="ausap1" size="25" maxlength="25">
        </div></th>
        <th colspan="2" id="errausap1" class="Estilo5" scope="col">&nbsp;</th>
      </tr>
      <tr>
        <th scope="col"><label>Segundo Apellido </label></th>
        <th scope="col"><div align="left">
          <input name="ausap2" type="text" id="ausap2" size="25" maxlength="25">
        </div></th>
        <th colspan="2" id="errausap2" class="Estilo5" scope="col">&nbsp;</th>
      </tr>
      <tr>
        <th scope="col"><label>Primer Nombre </label></th>
        <th scope="col"><div align="left">
          <input name="ausno1" type="text" id="ausno1" size="25" maxlength="25">
        </div></th>
        <th colspan="2" id="errausno1" class="Estilo5" scope="col">&nbsp;</th>
      </tr>
      <tr>
        <th scope="col"><label>Segundo Nombre </label></th>
        <th scope="col"><div align="left">
          <input name="ausno2" type="text" id="ausno2" size="25" maxlength="25">
        </div></th>
        <th colspan="2" id="errausno2" class="Estilo5" scope="col">&nbsp;</th>
      </tr>
      <tr>
        <th scope="col"><label>Fecha Nacimiento </label></th>
        <th scope="col">

          <div align="left">
   <?php echo escribe_formulario_fecha_vacio("ausfen","usuarioagregarform","0"); ?>
        </div>
          </th>
        <th colspan="2" scope="col"><div align="left">(DD.MM.AAAA)</div></th>
      </tr>
      <tr>
        <th scope="col"><label>Fecha de Ingreso </label></th>
        <th scope="col">          <div align="left">
    <?php echo escribe_formulario_fecha_vacio("ausfei","usuarioagregarform","0"); ?>
   </div>
</th>
        <th colspan="2" scope="col"><div align="left">(DD.MM.AAAA)</div></th>
      </tr>
      <tr>
        <th scope="col"><label>Ficha en N&oacute;mina</label></th>
        <th scope="col"><div align="left">
          <input name="ausfic" type="text" id="ausfic" size="10" maxlength="10">
        </div></th>
        <th colspan="2" id="errausfic" class="Estilo5" scope="col">&nbsp;</th>
      </tr>
      <tr>
        <th scope="col"><label>Sexo</label></th>
        <th scope="col"><div align="left">
          <p>
          
            <input name="aussex" type="radio" value="M" checked>
  Masculino

           
            <input type="radio" name="aussex" value="F">
  Femenino
            <br>
          </p>
</div></th>
        <th colspan="2" id="erraussex" class="Estilo5" scope="col">&nbsp;</th>
      </tr>
	  <tr>
        <th scope="col"><label>Usuario Supervisor </label></th>
        <th scope="col"><div align="left">
          <input type="checkbox" name="aussup" id="aussup"value="S">
</div></th>
        <th colspan="2" id="erraussup" class="Estilo5" scope="col">&nbsp;</th>
      </tr>
	  <tr>
        <th scope="col"><label>Tel&eacute;fonos</label></th>
        <th scope="col"><div align="left">
          <input name="austls" type="text" id="austls" size="25" maxlength="25">
        </div></th>
        <th colspan="2" id="erraustls" class="Estilo5" scope="col">&nbsp;</th>
      </tr>
      <tr>
        <th scope="col"><label>Supervisor</label></th>
        <th scope="col"><div align="left"><select name="aususr" id="select">
      <option value= " " >*** No tiene supervisor ***</option>
      <?php $sql="Select auscod, ausap1, ausap2, ausno1, ausno2 from mb20fp t1 where aussup='S' order by ausap1, ausap2 , ausno1";
				$result1=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111")); 
  				while(odbc_fetch_row($result1)){			
										$auscod=trim(odbc_result($result1,1));
										$nombre=odbc_result($result1,5)." ".odbc_result($result1,4).", ".odbc_result($result1,3)." ".odbc_result($result1,2);?>
      <option value= "<?php echo $auscod; ?>" ><?php echo $nombre ?></option>
      <?php } ?>
    </select></div></th>
        <th colspan="2" id="erraususr" class="Estilo5" scope="col">&nbsp;</th>
      </tr>
	  <tr>
        <th scope="col"><label>Email</label></th>
        <th scope="col"><div align="left">
          <input name="aemmai" type="text" id="austls" size="50" maxlength="50">
        </div></th>
        <th colspan="2" id="erraemmai" class="Estilo5" scope="col">&nbsp;</th>
      </tr>
      <tr>
        <th scope="col">&nbsp;</th>
        <th scope="col"></th>
        <th width="23%" id="erraususr" scope="col"><p onclick="usuarioagregarcerrar()" class="subir" align="right">salir</p></th>
        <th width="21%" id="erraususr" scope="col"><p align="right" class="subir" onclick="usuarioagregarvalidar()">Grabar</p></th>
      </tr>
    </table>
    </fieldset>
  </form>
</div>
</div>
<div id="editarusuariodiv" class="white_content">
	  <div>

   
  <form id="usuarioeditarform" name="usuarioeditarform" method="post" action="">
   <fieldset id="form">
<legend>Editar Usuario</legend>
   
    <table width="100%"  border="0">
	
      <tr>
        <th width="20%" scope="col"><label>Usuario</label></th>
        <th width="36%" id="wsauscod" align="left" scope="col">
        </th>
        <th colspan="2"  class="Estilo5" scope="col"><div align="left">
          <input name="auscod" type="hidden" id="auscod"></div></th>
      </tr>
      <tr>
        <th scope="col"><label>Primer Apellido </label></th>
        <th scope="col"><div align="left">
          <input name="ausap1" type="text" id="ausap1" size="25" maxlength="25">
        </div></th>
        <th colspan="2" id="erreausap1" class="Estilo5" scope="col">&nbsp;</th>
      </tr>
      <tr>
        <th scope="col"><label>Segundo Apellido </label></th>
        <th scope="col"><div align="left">
          <input name="ausap2" type="text" id="ausap2" size="25" maxlength="25">
        </div></th>
        <th colspan="2" id="erreausap2" class="Estilo5" scope="col">&nbsp;</th>
      </tr>
      <tr>
        <th scope="col"><label>Primer Nombre </label></th>
        <th scope="col"><div align="left">
          <input name="ausno1" type="text" id="ausno1" size="25" maxlength="25">
        </div></th>
        <th colspan="2" id="erreausno1" class="Estilo5" scope="col">&nbsp;</th>
      </tr>
      <tr>
        <th scope="col"><label>Segundo Nombre </label></th>
        <th scope="col"><div align="left">
          <input name="ausno2" type="text" id="ausno2" size="25" maxlength="25">
        </div></th>
        <th colspan="2" id="erreausno2" class="Estilo5" scope="col">&nbsp;</th>
      </tr>
      <tr>
        <th scope="col"><label>Fecha Nacimiento </label></th>
        <th scope="col">

          <div align="left">
   <?php echo escribe_formulario_fecha_vacio("ausfen","usuarioeditarform","0"); ?>
        </div>
          </th>
        <th colspan="2" scope="col"><div align="left">(DD.MM.AAAA)</div></th>
      </tr>
      <tr>
        <th scope="col"><label>Fecha de Ingreso </label></th>
        <th scope="col">          <div align="left">
    <?php echo escribe_formulario_fecha_vacio("ausfei","usuarioeditarform","0"); ?>
   </div>
</th>
        <th colspan="2" scope="col"><div align="left">(DD.MM.AAAA)</div></th>
      </tr>
      <tr>
        <th scope="col"><label>Ficha en N&oacute;mina</label></th>
        <th scope="col"><div align="left">
          <input name="ausfic" type="text" id="ausfic" size="10" maxlength="10">
        </div></th>
        <th colspan="2" id="erreausfic" class="Estilo5" scope="col">&nbsp;</th>
      </tr>
      <tr>
        <th scope="col"><label>Sexo</label></th>
        <th scope="col"><div align="left">
          <p>
           
            <input name="aussex" type="radio" value="M" checked>
  Masculino

          
            <input type="radio" name="aussex" value="F">
  Femenino
            <br>
          </p>
</div></th>
        <th colspan="2" id="erreaussex" class="Estilo5" scope="col">&nbsp;</th>
      </tr>
	  <tr>
        <th scope="col"><label>Usuario Supervisor </label></th>
        <th scope="col"><div align="left">
          <input type="checkbox" name="aussup" id="aussup"value="S">
</div></th>
        <th colspan="2" id="erreaussup" class="Estilo5" scope="col">&nbsp;</th>
      </tr>
	  <tr>
        <th scope="col"><label>Tel&eacute;fonos</label></th>
        <th scope="col"><div align="left">
          <input name="austls" type="text" id="austls" size="25" maxlength="25">
        </div></th>
        <th colspan="2" id="erreaustls" class="Estilo5" scope="col">&nbsp;</th>
      </tr>
      <tr>
        <th scope="col"><label>Supervisor</label></th>
        <th scope="col"><div align="left"><select name="aususr" id="select">
      <option value= " " >*** No tiene supervisor ***</option>
      <?php $sql="Select auscod, ausap1, ausap2, ausno1, ausno2 from mb20fp t1 where aussup='S' order by ausap1, ausap2 , ausno1";
				$result1=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111")); 
  				while(odbc_fetch_row($result1)){			
										$auscod=trim(odbc_result($result1,1));
										$nombre=odbc_result($result1,5)." ".odbc_result($result1,4).", ".odbc_result($result1,3)." ".odbc_result($result1,2);?>
      <option value= "<?php echo $auscod; ?>" ><?php echo $nombre ?></option>
      <?php } ?>
    </select></div></th>
        <th colspan="2" id="erreaususr" class="Estilo5" scope="col">&nbsp;</th>
      </tr>
	  <tr>
        <th scope="col"><label>Email</label></th>
        <th scope="col"><div align="left">
          <input name="aemmai" type="text" id="austls" size="50" maxlength="50">
        </div></th>
        <th colspan="2" id="erraemmai" class="Estilo5" scope="col">&nbsp;</th>
      </tr>
      <tr>
        <th scope="col">&nbsp;</th>
        <th scope="col"></th>
        <th width="23%"  scope="col"><p onclick="usuarioeditarcerrar()" class="subir" align="right">salir</p></th>
        <th width="21%"  scope="col"><p align="right" class="subir" onclick="usuarioeditarvalidar()">Modificar</p></th>
      </tr>
    </table>
    </fieldset>
  </form>
</div>
</div>
<div id="editarusuarioautdiv" class="white_content">
	  <div>  
  <form id="usuarioeditarautform" name="usuarioeditarautform" method="post" action="">
    <h4>Autorizar Opciones</h4>
    <table width="60%"  border="0" align="center" class="Estilo3">
      <tr>
        <th width="30%" scope="col"><div align="left">Usuario</div></th>
        <th width="70%" id="wsauscodaut" align="left" scope="col">
        </th>
        
      </tr>
      <tr>
        <th scope="col"><div align="left">Opciones Autorizada: </div></th>
        <th  align="left" scope="col"><input name="auscod" type="hidden" id="auscod">
          </th>
      </tr>
      <tr>
        <th colspan="2" scope="col"vwidth="40%"><div id="menuh" class="cuerporec" align="left" >
					<ul id="ul">
					
					</ul>
		</div></th>
        </tr>
			<tr>
        <th width="23%"  scope="col"><p onclick="usuarioeditarautcerrar()" class="subir" align="right">Salir</p></th>
        <th width="21%"  scope="col"><p align="right" class="subir" onclick="usuarioeditarautvalidar()">Autorizar</p></th>
      </tr>

    </table>
  </form>
</div>
</div>


<div id="fade" class="black_overlay"></div>
</div>
<!-- end page -->


  <div id="navigation">
	<div id="opcionesmasusadas"><?php include("../opcionesmasusadasphp.php");?></div>
  </div>
</body>
</html>
