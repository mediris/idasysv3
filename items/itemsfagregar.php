<?php 
/*
* jdavila 
* 08/09/2015
*/
session_start();
include("../conectar.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="sinhead">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>agregar tipo inventario</title>
<link href="../<?php echo $css; ?>" rel="stylesheet" type="text/css" media="screen" />
<script language="JavaScript" src="../javascript/jquery.js"></script>
<script language="JavaScript" src="../javascript/javascript.js"></script>
<script language="JavaScript" src="javascript.js"></script>
</head>
<body class="sinbody" bgcolor="#FFFFFF" onload="javascript:agregar()">

<div id="aediv" class="white_content">

        <form id="form" name="form" method="post" action="" class="form">
		        <input name="compcod" id="compcod" type="hidden" value="<?php echo $Compania;?>">
                <table width="100%"  border="0">
                  <tr>
                    <th scope="col"><label>C&oacute;digo de ITEM</label></th>
                    <th scope="col"><div align="left">
                    <input name="aitcod" id="aitcod" type="text" size="50" maxlength="4"></input>
                    </div></th>
                    <th colspan="2" id="erraitcod" class="Estilo5" scope="col">&nbsp;</th>
                  </tr>
                  <tr>
                    <th scope="col"><label>Categor&iacute;a de ITEM<br />(*Solo N&uacute;mero)</label></th>
                    <th scope="col"><div align="left">
                    <!--<input name="aitcat" id="aitcat" type="text" size="50" maxlength="3"></input>-->
                    <select name="aitcat" id="aitcat">
                    	<option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                    </select>
                    </div></th>
                    <th colspan="2" id="erraitcat" class="Estilo5" scope="col">&nbsp;</th>
                  </tr>
                  <tr>
                    <th scope="col"><label>Descripci&oacute;n</label></th>
                    <th scope="col"><div align="left">
                    <input name="aitdes" id="aitdes" type="text" size="50" maxlength="200"></input>
                    </div></th>
                    <th colspan="2" id="erraitdes" class="Estilo5" scope="col">&nbsp;</th>
                  </tr>
                  <tr>
                    <th scope="col"><label>Observaci&oacute;n</label></th>
                    <th scope="col"><div align="left">
                      <input name="aitobs" id="aitobs" type="text" size="50" maxlength="150"></input>
                    </div></th>
                    <th colspan="2" id="erraitobs" class="Estilo5" scope="col">&nbsp;</th>
                  </tr>
                  
                  <tr>
                    <th scope="col">&nbsp;</th>
                    <th scope="col"></th>
                    <!--<th width="23%"  scope="col"><p onclick="marcaseditarcerrar()" class="subir" align="right">salir</p></th>
                    <th width="21%"  scope="col"><p align="right" class="subir" onclick="marcaseditarvalidar()">Grabar</p></th>-->
    				<th width="23%"  scope="col"><input name="cancelar" type="button" onClick="parent.location.reload();parent.Shadowbox.close();"  value="Salir" id="cancelar"></th>
	                <th width="21%"  scope="col"><input type="button" name="submit" id="submit" value="Grabar" onClick="agregarvalidar()" /></th>
                  </tr>
                </table>
    	</form>
</div>
<div align="center" id="aftsav">
  <br />
  <br />
  <br />
  <br />
  <br />
  <br />
  Registro Agregado con exito !
  <br />
  <br />
  <br />
  <input name="Submit3" type="button" onClick="parent.location.reload();parent.Shadowbox.close();"" " value="Cerrar"></div>
</body>
</html>