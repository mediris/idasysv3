<script>
<!--
/*
 * jdavila
 * metodo utilizado para colocar la imagen de fondo en los modulos deseados
 * solo hace falta colocar la imagen con nombre "fondoimagen.png" en la carpeta del modulo
 * se coloca com fondo de div id='content'
 */
function fondocontent()
{
	<?php $archivo = "fondoimagen.png"?>
	var sino = <?php echo file_exists($archivo)==true?1:2;?>;
	if(window.document.getElementById("content") && sino == 1)
	{
		window.document.getElementById("content").style.backgroundImage = "url(fondoimagen.png)";
	}	
}
fondocontent();
	-->
</script>

<script language="JavaScript" type="text/JavaScript">
<!--
function MM_jumpMenu(targ,selObj,restore){ //v3.0
dat = selObj.options[selObj.selectedIndex].value;
opcion = dat.substring(0,9);
pagina = dat.substring(10,89);
  irapagina(pagina , opcion);
  if (restore) selObj.selectedIndex=0;
}
//-->
</script>

<script language="JavaScript" type="text/JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
//-->
</script>


<table width="100%"  border="0">
  <tr>
    <th width="62%" scope="col"><div id="legal">
      <div align="left">(c) 2011   <a href="http://www.gabc.com.ve">GABC Sistemas,C.A.</a></div>
    </div></th><?php if ($Usuario!="") { ?>
        <th width="6%" scope="col"><div><a href="javascript:usuariosconectados();"><img src="/idasysv3/images/usuarios.gif" width="24" height="24" border="0"></a>
		<div id="usuariosdelsistema" style="position:absolute;  width:350px; height:400px; z-index:100; left: 420px; top:  -417px; overflow:auto; display:none">
<table id="background-image"  width="100%" class="usuariosconectados">
	
  <thead>
  	<tr>
    	<th height="20" colspan="3" scope="col"><div align="center">USUARIOS DEL SISTEMA</div></th>
    	</tr>
	  	<tr>
    	<th width="28%" height="20" scope="col">Usuario</th>
    	<th width="50%" scope="col">Apellidos y Nombres </th>
    	<th width="22%" scope="col">Status</th>
  	</tr>

  </thead>
    <tbody>
	          <?php $sql="Select * from mb20fp order by auscod ";
				$result=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111")); 
  				while(odbc_fetch_row($result)){			?>
  	<tr>
    	<td height="20"  scope="col">
    	  	<?php if (odbc_result($result,21)=='F') { echo '<img src="/idasysv3/images/femenino.gif" width="10" height="10">' ;}
				else {echo '<img src="/idasysv3/images/masculino.gif" width="10" height="10"> '; }
				echo odbc_result($result,1);
			?>
        </td>                
    	<td scope="col"><?php echo odbc_result($result,2)." ".odbc_result($result,3).",".odbc_result($result,4);?></td>
    	<td scope="col"><?php echo status('AUSSTS',odbc_result($result,20));?></td>
     </tr>
			  <?php } ?>
  </tbody>

</table>
</div></div>
		
		</th>
        <th width="6%" scope="col"><a href="javascript:noticiaagregar();"><img src="/idasysv3/images/news.gif" alt="Agregar Noticia o Evento" width="38" height="23" border="0"></a></th>
		<th width="6%" scope="col"><a href="javascript:soporteausuario();"><img src="/idasysv3/images/helpdesk.gif" ALT="Soporte a Usuario" width="23" height="24" border="0"></a></th>
    <th width="26%" scope="col">
	
     <?php $sql="SELECT T1.AOPCOD, T3.AOPDES, T3.AOPLNK, T1.AUOEST FROM mb23fp t1, mb22fp t3 WHERE T1.AUSCOD='$Usuario' and T1.AMDCOD='$modulo' and not exists (select * from  mb22fp t2 where t1.amdcod= t2.amdcod and t1.aopcod=t2.aoppre) and t1.amdcod=t3.amdcod and t1.aopcod=t3.aopcod and t1.auoest<>0 ORDER BY T1.AUOEST desc FETCH FIRST 10 ROWS ONLY ";
		$result1=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111")); 
  		while(odbc_fetch_row($result1)){			
		
	if ($privez!='S') 	{?>
		<select name="opcionesr" id="opcionesr" onChange="MM_jumpMenu('parent',this,0)">
      <option value= " " selected="selected" >*** Acceso R&aacute;pido ***</option>
	  					<?php $privez='S'; }	?>
      <option value= "<?php echo odbc_result($result1,1)."/idasysv3/".odbc_result($result1,3); ?>" ><?php echo odbc_result($result1,2); ?></option>				
      	<?php } ?>
		</select>
	</th>
  <?php
}
?>
</tr>
</table>
<?php if ($Usuario!="") { ?>

<div id="agregarnoticiadiv" style="position:absolute; width:658px; height:164px; z-index:101; left: 83px; top: -217px; display:none;">
  <form action="" method="post" name="noticiaagregarform" id="noticiaagregarform">
<fieldset id="form">
<legend>Agregar una Noticia o Evento </legend>
  
    <table width="100%"  border="0"  bgcolor="#CDCDCD">
      <tr>
        <td width="21%"><label>Tipo Noticia/Evento</label></td>
        <td width="50%"><select name="atncod" id="atncod">
      <?php $sql="Select * from mb35fp t1  order by atncod";
				$result1=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111")); 
  				while(odbc_fetch_row($result1)){			
										$atncod=trim(odbc_result($result1,1));
										$atndes=odbc_result($result1,2);?>
      <option value= "<?php echo $atncod; ?>" ><?php echo $atndes ?></option>
      <?php } ?>
    </select></td>
        <td width="29%">&nbsp;</td>
      </tr>
      <tr>
        <td><label>Noticia o Eveno</label></td>
        <td><textarea name="anedes" cols="50" rows="2" id="anedes"></textarea></td>
        <td id="erranedes" class="Estilo5">&nbsp;</td>
      </tr>
      <tr>
        <td><label>Duraci&oacute;n:</label></td>
        <td><select name="tiempo" id="tiempo">
          <option value="12">12 Horas</option>
          <option value="24">24 Horas</option>
          <option value="48">48 Horas</option>
          <option value="72">72 Horas</option>
        </select></td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td><label>a Usuario: </label></td>
        <td><select name="anerec" id="select">
      <option value= " " >*** Todos los Usuarios ***</option>
      <?php $sql="Select auscod, ausap1, ausap2, ausno1, ausno2 from mb20fp t1 order by ausap1, ausap2 , ausno1";
				$result1=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111")); 
  				while(odbc_fetch_row($result1)){			
										$auscod=trim(odbc_result($result1,1));
										$nombre=odbc_result($result1,5)." ".odbc_result($result1,4).", ".odbc_result($result1,3)." ".odbc_result($result1,2);?>
      <option value= "<?php echo $auscod; ?>" ><?php echo $nombre ?></option>
      <?php } ?>
    </select></td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td></td>
        <td><p onclick="noticiaagregarcerrar()" class="subir" align="right">salir</p></td>
        <td><p align="left" class="subir" onclick="noticiaagregarvalidar()">Grabar</p></td>
      </tr>
    </table>
    </fieldset>
  </form>
</div>
  <?php
}
?>