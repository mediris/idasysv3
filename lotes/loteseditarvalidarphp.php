<?php
/*
 *jDavila	
 *21/03/2013
 */
	
	session_start();
	header("Content-type: text/javascript; charset=iso-8859-1"); 
	include("../conectar.php");
	include("../JSON.php");
	if ($Usuario) { 
		$aarcod=$cadena["aarcod"];
		$altcod=$cadena["altcod"];
		$altltr=$cadena["altltr"];
		$altfvf=$cadena["altfvf"];
		$altfef=$cadena["altfef"];
		$altfra=$cadena["altfra"];

		$i=0;
		$json = new Services_JSON();
		/* Validar Error */
		
		/* articulo */ 
		if (strlen($aarcod)==0) {$i++;$i++;$mensaje[$i]["campo"]="erraarcod";
								$mensaje[$i]["msg"]=ICONV( "ISO-8859-1", "UTF-8", trim(buscaerror($idioma,"lot_10001")));}
		
		/* validacio de codigo lote */
		if (strlen($altcod)==0) {$i++;$mensaje[$i]["campo"]="erraltcod";
								$mensaje[$i]["msg"]=ICONV( "ISO-8859-1", "UTF-8", trim(buscaerror($idioma,"lot_20001")));}
		
		
		/* fecha vencimiento proveedor */ 
		if (strlen($altfvf)==0) {$i++;$i++;$mensaje[$i]["campo"]="erraltfvf";
								$mensaje[$i]["msg"]=ICONV( "ISO-8859-1", "UTF-8", trim(buscaerror($idioma,"lot_3001")));}
		
		/* fecha emision proveedor */ 
		if (strlen($altfef)==0) {$i++;$i++;$mensaje[$i]["campo"]="erraltfef";
								$mensaje[$i]["msg"]=ICONV( "ISO-8859-1", "UTF-8", trim(buscaerror($idioma,"lot_40001")));}
		
		/* fecha pre-vencimiento (fecha reanalicis) */ 
		if (strlen($altfra)==0) {$i++;$i++;$mensaje[$i]["campo"]="erraltfra";
								$mensaje[$i]["msg"]=ICONV( "ISO-8859-1", "UTF-8", trim(buscaerror($idioma,"lot_50001")));
		}else{
			$lote1 = new lote($cid,$Compania, $modulo);/* instancia de lote */
			$valialtfra = $lote1->validarfechareanalisis(formatDate($altfvf  ,'dd.mm.aaaa','aaaammdd'), formatDate($altfra,'dd.mm.aaaa','aaaammdd'), $DiasValidacion);
			if ($valialtfra==-1) {$i++;$i++;$mensaje[$i]["campo"]="erraltfra";
								$mensaje[$i]["msg"]=ICONV( "ISO-8859-1", "UTF-8", trim(buscaerror($idioma,"lot_50002")));}
		}
		
		
		/* si hay error */
		if ($i>0) 	{
					echo $json->encode($mensaje);
					}			
		/* si no hubo error */			
		
		else		{
					/* guardar lote */
					$lote = new lote($cid,$Compania, $modulo);/* instancia de lote */
					//$lote->editarlote($aarcod, $altcod, $altfef, $altfvf, '', '', '', '', '', '', '', '', '', '', '', $altfra ); /* edtar lote */
					$lote->editarlote($aarcod, $altcod, $altltr, $altfef, $altfvf, '', '', '', '', '', '', '', '', '', '', '', $altfra );
		}
	}
?>