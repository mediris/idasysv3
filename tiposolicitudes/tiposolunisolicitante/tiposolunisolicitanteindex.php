<?php 
/*
 Luis Ramos
 15/09/2016
 */
session_start();
include("../../conectar.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
<title>Idasys V3</title>
<link href="../../<?php echo $css; ?>" rel="stylesheet" type="text/css" media="screen" />
<script language="JavaScript" src="../../javascript/javascript.js"></script>
<script language="JavaScript" src="javascript.js"></script>
<script language="JavaScript" src="../../javascript/jquery.js"></script>
<link href="../../shadowbox.css"  rel="stylesheet" type="text/css"/>
<script src="../../shadowbox.js" type="text/javascript"/> </script>
<script language="JavaScript" type="text/JavaScript">
Shadowbox.init({overlayOpacity: "0.5"});
</script>
<style type="text/css" title="currentStyle">
			@import "../../DataTables-1.9.4/media/css/<?php echo $demo_page;?>";
			@import "../../DataTables-1.9.4/media/css/<?php echo $demo_table;?>";
</style>
<script type="text/javascript" language="javascript" src="../../DataTables-1.9.4/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf-8">
			
			 $(document).ready(function() 
			 {
			      document.getElementById('info').style.display="";
				  $('#info').dataTable( 
				  {
			        "bStateSave": true,
					"oLanguage": 
					{
			          	"sLengthMenu": "Mostrar _MENU_ registros por Pag.",
						"sZeroRecords": "No Existen Registros",
						"sInfo": "Mostrando desde _START_ hasta _END_ de _TOTAL_ registros",
						"sInfoEmpty": "Mostrando desde 0 hasta 0 de 0 registros",
						"sInfoFiltered": "(filtrando de _MAX_ de registros)",
						"sSearch": "Buscar",
						"oPaginate": 
						{
							"sNext": "Siguiente",
							"sPrevious": "Anterior"
						}
			        }
			      } );
			  } );
</script>
</head>
<body>
<div id="wrapper">
  <?php 
  		include("../../superior.php");
  		?>
  <div id="page">
      <?php include("../../validar.php");  		?>
   	  
 		<?php 
				
				//$sql="SELECT t1.ACICOD, t1.ATRCOD, t1.ATRDES, t1.ATRSIG, t1.ATRMOR, t1.ATTCOD, t2.ATTCOD, t2.ATTDES FROM IV12FP t1, IV27FP t2 WHERE t1.ACICOD = '$Compania' AND t1.ATTCOD = t2.ATTCOD and t1.ACICOD = t2.ACICOD ORDER BY t2.ATTCOD";

				$sql = "SELECT T1.ACICOD, T1.ATLCOD, T1.AUNCOD, T1.ASUSTS, T3.AUNDES, T1.ASUSTS
									FROM SL06FP T1 
									INNER JOIN SL01FP T2 ON (T1.ACICOD=T2.ACICOD AND T1.ATLCOD=T2.ATLCOD) 
									INNER JOIN IS01FP T3 ON (T1.ACICOD=T3.ACICOD AND T1.AUNCOD=T3.AUNCOD) WHERE T1.ACICOD='$Compania' AND T1.ATLCOD='$atlcod' AND T2.ATLDES='$atldes' ORDER BY T3.AUNDES";

				$resultt=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111"));
				$z=0;
				
				while(odbc_fetch_row($resultt))
				{
					$jml = odbc_num_fields($resultt);
					for($i=1;$i<=$jml;$i++)
					{
						$row[$z][odbc_field_name($resultt,$i)] =  odbc_result($resultt,$i);
					}
					$z++;
				}
				/*guarla informacion en un arreglo*/
				$_SESSION['solicitudarreglo']=$row;			
			?> 
        <div id="content3" > 
        	<table width="100%"   border="0">
              <tr>
                <td width="84%" scope="col"><h1 align="center" class="title">UNIDADES SOLICITANTES</h1>
                <div align="center"><a href="../tiposolucitudindex.php">Tipo de Solicitud: </a><?php echo $atldes . ' ('.$atlcod.')'; ?></div><hr /></td>
                <td width="16%" scope="col"><div align="left">
                  <table width="100%"  border="0">
                    <tr>
                      <th width="36%" scope="col"><a rel="shadowbox;width=650;height=345" title="Agregar Unidad Solicitante" href="tiposolunisolicitantefagregar.php?atlcod=<?php echo $atlcod; ?>&atldes=<?php echo $atldes; ?>"><img src="../../images/agregar.gif" alt="Agregar" width="25" height="25" border="0" /></a></th>
                      <th width="30%" scope="col"><img src="../../images/excel.jpg" alt="" width="25" height="25" /></th>
                      <th width="16%" scope="col"><img src="../../images/pdf.jpg" alt="" width="25" height="25" /></th>
                      <th width="18%" scope="col"><img src="../../images/impresora.gif" alt="" width="25" height="25" /></th>
                    </tr>
                  </table>
                </div></td>
              </tr>
            </table>        
			<div id="container">
            <div id="demo">
                <table width="100%" id="info" style="display:none">
                    <thead>
                        <tr>
                       		<th>C&oacute;digo</th>
                            <th>Descripci&oacute;n Solicitud</th>
                            <th>Status</th>
                            <th>Opciones</th>
                        </tr> 
                    </thead>
                    <tbody >
                        <?php 
                            $paginat=$_SESSION['solicitudarreglo'];
                            for($g=0; $g < (count($paginat)); $g++)
                            {
                                
                        ?>
                                <tr>
                                <td scope="col"><div align="center"><?php echo $paginat[$g]["AUNCOD"];?></div></td>      
                                <td scope="col"><div align="left"><?php echo $paginat[$g]["AUNDES"];?></div></td>
                                <td scope="col"><div align="center"><?php echo $paginat[$g]["ASUSTS"];?></div></td>        
                                    <?php /* seccion de opciones */ ?>
                                    <td scope="col">
                                        <ul id="opciones">
                                            <li><a title="Eliminar" href="javascript:eliminar('<?php echo trim($paginat[$g]["ATLCOD"]);?>', '<?php echo trim($paginat[$g]["AUNCOD"]);?>')"><img src="../../images/eliminar.gif" alt="Eliminar" width="15" height="15" border="0" /></a></li>
                                        </ul>
                                    </td>             
                                </tr>
                        <?php } ?> 
                    
                    </tbody>
                
                </table>
            </div>
	  </div>
	</div>
		<!-- end #content -->
	<div style="clear: both;">&nbsp;</div>
  </div>
	<!-- end #page -->
</div>
	<div id="footer">
		<div id="opcionesmasusadas"><?php include("../../opcionesmasusadasphp.php");?></div>
	</div>
	<!-- end #footer -->
</body>
</html>
