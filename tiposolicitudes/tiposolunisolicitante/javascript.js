/*
jDavila
20/03/12
*/
function agregar()
{
	document.form.auncod.value="";
	$("#agregaraftsav").hide(1);
}

/*
jDavila
20/03/12
*/
function agregarvalidar(){	
	var contenido= "";
	var elemento;
	document.body.style.cursor="wait";

	$.ajax({
	   	async: false,
        type: "POST",
		dataType: "JSON",
        url: "tiposolunisolicitanteagregarvalidar.php",
        data: $("#form").serialize(),
        success: function(datos){
			vdata = parseJSONx(datos);}							
	});			
	sierror='N';
	for(i in vdata)	{
		if(vdata[i].campo) 	{
			document.getElementById(vdata[i].campo).innerHTML=vdata[i].msg;
			sierror='S';
		}
	}
	document.body.style.cursor="default";
	if(sierror=='N') 		
	{
		$("#agregardiv").hide(1000);
		$("#agregaraftsav").show(1000);
	}
}

/*
jDavila
20/03/12
*/
function eliminar(atlcod, auncod) {
	if (confirm('Seguro que desea borrar la Unidad Solicitante: ' + auncod + '?'))
	{
		var param = [];
		param['atlcod']=atlcod;
		param['auncod']=auncod;
		ejecutasqld("tiposolunisolicitanteeliminar.php",param);
		location.reload();
	}
}
