<?php
session_start();

include_once("../conectar.php");

if($Compania =='43'){
	$img = 'MEDITRON_logo_rif_2.png';
}else{
	$img = 'logomeditronnuevo2.png';
}

$direccion = !empty($Direccionip)?$Direccionip:'administracionweb';
$Companiarif = $_SESSION['Companiarif'];
			
$html.='
	<style>
		.principal{
			width:100%; 
			margin-left:4px; 
			border-bottom: 1px solid;
			padding: 13px 0px 13px 0px;
		}
		.item{
			text-align: left;
			margin-left: 14px;
			margin-top: 0px;
		}
		.item-10{
			text-align: left;
			margin-left: 10px;
			margin-top: 0px;
		}
		.punto-inspeccionar{
			text-align: left;
			margin-left: 45px;
			margin-top: -15px;
		}
		.conforme{
			margin-left: 437px;
			margin-top:-17px;
			text-align:justify;
		}
		.no-conforme{
			text-align: right;
			margin-right:225px;
			margin-top: -32px;
		}
		.cuadro{
			border:1px solid; 
			width:15px;
			height:15px;
		}
	</style>
	<page backtop="70mm" backbottom="80mm" >
		<page_header>
    		<table style="width:100%;align:center;" border="0" >
            	<tr>
                	<td>
                    	<img src="http://'.$direccion.'/idasysv3/images/'.$img.'"/><h5>R.I.F.: '.$Companiarif.'</h5>
                	</td>
                	<td>
                    	<table style="width: 50%;margin-left:350px;font-size:10;">
                        	<tr align="left" style="width: 30%;"><td>P&Aacute;G. N&deg; [[page_cu]] DE [[page_nb]]</td></tr>
                        	<tr align="left" style="width: 30%;"><td>FECHA: '.date('d/m/Y').'</td></tr>
                    	</table>
                	</td>
            	</tr>
            	<tr>
             		<td colspan="2" style="text-align: center"> 
						<h3>CERTIFICADO DE LIBERACIÓN DE PRODUCTO TERMINADO</h3>
					</td>
            	</tr>
				<tr>
                	<td colspan="3" style="width: 100%;">
						<table style="width: 100%;">
							<tr>
								<td align="left" style="width: 20%;"><strong>Nombre del producto:</strong> </td>
								<td align="left" style="width: 81%;">'.trim($nombreArticulo).' <strong>('.trim($codigoArticulo).')</strong></td>
							</tr>
							<tr>
								<td align="left" style="width: 20%;"><strong>Fecha de fabricación:</strong> </td>
								<td align="left" style="width: 81%;">'.trim($paramAdicional[1]).'</td>
							</tr>
							<tr>
								<td align="left" style="width: 20%;"><strong>Fecha de vencimiento:</strong> </td>
								<td align="left" style="width: 81%;">'.trim($paramAdicional[2]).'</td>
							</tr>
							
							<tr>
								<td align="left" style="width: 20%;"><strong>N° de lote:</strong> </td>
								<td align="left" style="width: 81%;">'.trim($lote).'</td>
							</tr>	
						</table>
					</td>
            	</tr>
				<tr>
					<td colspan="3" style="width: 100%;" style="vertical-align:top;height:502px;"  >
						<table style="vertical-align:top;width: 100%; margin-top:25px;" cellpadding="0" cellspacing="0"> 
							<tr>
								<td style="text-align:center;vertical-align:middle;width:5%;font-size:11;border-top:1px solid;border-bottom:1px solid;border-left:1px solid;padding:5px 0px 5px 0px;" >
									<strong>IT.</strong>
								</td>
								<td style="text-align:center;width:45%;vertical-align:middle;font-size:11;border-top:1px solid;border-bottom:1px solid;border-left:1px solid;padding:5px 0px 5px 0px;" >
									<strong>PUNTO A INSPECCIONAR</strong>
								</td>
								<td style="text-align:center;width:15%;vertical-align:middle;font-size:11;border-top:1px solid;border-bottom:1px solid;border-left:1px solid;padding:5px 0px 5px 0px;" >
									<strong>CONFORME</strong>
								</td>
								<td style="text-align:center;width:15%;vertical-align:middle;font-size:11;border-top:1px solid;border-bottom:1px solid;border-left:1px solid;padding:5px 0px 5px 0px;" >
									<strong>NO CONFORME</strong>
								</td>
								<td style="text-align:center;width:20%;vertical-align:middle;font-size:11;border-top:1px solid;border-bottom:1px solid;border-left:1px solid;border-right:1px solid;padding:5px 0px 5px 0px;" >
									<strong>OBSERVACIÓN</strong>
								</td>
							</tr>
							<tr>
								<td style="height:414px;text-align:center;border-left:1px solid;" >
									&nbsp;
								</td>
								<td style="text-align:center;border-left:1px solid;" >
									&nbsp;
								</td>
								<td style="text-align:center;border-left:1px solid;" >
									&nbsp;
								</td>
								<td style="text-align:center;border-left:1px solid;" >
									&nbsp;
								</td>
								<td style="text-align:center;border-left:1px solid; border-right:1px solid;" >
									&nbsp;
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</page_header>
		<page_footer>
		<div style="position:absolute; bottom: 2px; margin-left:3px;">
        	<table style="width: 776px;border_top: none; border-bottom:none;border-right:none; border-left:none;" cellpadding="0" cellspacing="0">
				<tr>
					<td style="border-right:1px solid; border-left:1px solid; text-align:left; padding-top:10px;" colspan="3">
						<strong>OBSERVACIONES:</strong>
					</td>
				</tr>
				<tr>
					<td style="height:90px;border-right:1px solid; border-left:1px solid; text-align:left; border-bottom:1px solid;" colspan="3">
						<div style="text-align:justify;"></div>
					</td>
				</tr>
				<tr>
					<td style="border-bottom:1px solid; border-right:1px solid; border-left:1px solid; text-align:center; padding: 5px 0px 5px 0px;">
						<strong>Aprobado</strong> <div class="cuadro" style="margin-left: 5px;"></div>
					</td>
					<td style="border-bottom:1px solid; text-align:center;border-right:1px solid; padding: 5px 0px 5px 0px;">
						<strong>Rechazado</strong> <div class="cuadro" style="margin-left: 5px;"></div>
					</td>
				</tr>
			</table>
			<div style="text-align: center; border-left: 1px solid;border-right: 1px solid;border-bottom: 1px solid; padding: 5px 0px 5px 0px;">
				<strong>Realizado y aprobado por: Farmaceutica Regente</strong>
			</div>
			<table style="border-right:1px solid; border-left:1px solid; border-bottom: 1px solid; margin-left: 0px">
        		<tr>
            		<td>
						<table style="font-size:10px;" cellspacing="10px">

							<tr>
								<td><strong>NOMBRE Y APELLIDO:</strong></td>
								<td>_________________________________________________________________________________________________________________</td>
							</tr>
							<tr>
								<td><strong>C.I.:</strong></td>
								<td>_________________________________________________________________________________________________________________</td>
							</tr>
							<tr>
								<td><strong>FIRMA:</strong></td>
								<td >_________________________________________________________________________________________________________________</td>
							</tr>
							<tr>
								<td><strong>FECHA:</strong></td>
								<td>_____/_____/_______</td>
							</tr>
							<tr>
								<td><strong>HORA:</strong></td>
								<td>____:____  ____</td>
							</tr>
						</table>
					</td>
        		</tr>
			</table>
			<table>
				<tr style="font-size:10px;">
					<td style="text-align:left;" colspan="3">
						F-G-LAB-001-11-17
					</td>
				</tr>
			</table>
		</div>
		</page_footer>
		<div class="principal">
			<div class="item">1</div>
			<div class="punto-inspeccionar">Revisión del histórico del lote</div>
			<div class="conforme">
				<div class="cuadro"></div>
			</div>
			<div class="no-conforme">
				<div class="cuadro"></div>
			</div>
		</div>
		<div class="principal">
			<div class="item">2</div>
			<div class="punto-inspeccionar">Revisión de la documentación  de producción del lote</div>
			<div class="conforme">
				<div class="cuadro"></div>
			</div>
			<div class="no-conforme">
				<div class="cuadro"></div>
			</div>
		</div>
		<div class="principal">
			<div class="item">3</div>
			<div class="punto-inspeccionar">Buenas practicas de documentación del lote</div>
			<div class="conforme">
				<div class="cuadro"></div>
			</div>
			<div class="no-conforme">
				<div class="cuadro"></div>
			</div>
		</div>
		<div class="principal">
			<div class="item">4</div>
			<div class="punto-inspeccionar">Verificación del proceso de manufactura</div>
			<div class="conforme">
				<div class="cuadro"></div>
			</div>
			<div class="no-conforme">
				<div class="cuadro"></div>
			</div>
		</div>
		<div class="principal">
			<div class="item">5</div>
			<div class="punto-inspeccionar"></div>
			<div class="conforme">
				<div class="cuadro"></div>
			</div>
			<div class="no-conforme">
				<div class="cuadro"></div>
			</div>
		</div>
		<div class="principal">
			<div class="item">6</div>
			<div class="punto-inspeccionar"></div>
			<div class="conforme">
				<div class="cuadro"></div>
			</div>
			<div class="no-conforme">
				<div class="cuadro"></div>
			</div>
		</div>
		<div class="principal">
			<div class="item">7</div>
			<div class="punto-inspeccionar"></div>
			<div class="conforme">
				<div class="cuadro"></div>
			</div>
			<div class="no-conforme">
				<div class="cuadro"></div>
			</div>
		</div>
		<div class="principal">
			<div class="item">8</div>
			<div class="punto-inspeccionar"></div>
			<div class="conforme">
				<div class="cuadro"></div>
			</div>
			<div class="no-conforme">
				<div class="cuadro"></div>
			</div>
		</div>
		<div class="principal">
			<div class="item">9</div>
			<div class="punto-inspeccionar"></div>
			<div class="conforme">
				<div class="cuadro"></div>
			</div>
			<div class="no-conforme">
				<div class="cuadro"></div>
			</div>
		</div>
		<div class="principal">
			<div class="item-10">10</div>
			<div class="punto-inspeccionar"></div>
			<div class="conforme">
				<div class="cuadro"></div>
			</div>
			<div class="no-conforme">
				<div class="cuadro"></div>
			</div>
		</div>
	</page>';
?>