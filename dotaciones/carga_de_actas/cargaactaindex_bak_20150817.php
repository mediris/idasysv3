<?php session_start();
include("../../conectar.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
<title>Idasys V3</title>
<link href="../../<?php echo $css; ?>" rel="stylesheet" type="text/css" media="screen" />
<script language="JavaScript" src="../../javascript/javascript.js"></script>
<script language="JavaScript" src="../javascript.js"></script>
<script language="JavaScript" src="../../javascript/jquery.js"></script>
<link href="../../shadowbox.css"  rel="stylesheet" type="text/css"/>
<script src="../../shadowbox.js" type="text/javascript"/> </script>
<script language="JavaScript" type="text/JavaScript">
Shadowbox.init({
overlayOpacity: "0.5"
});
</script>
<style type="text/css" title="currentStyle">
			@import "../../DataTables-1.9.4/media/css/<?php echo $demo_page;?>";
			@import "../../DataTables-1.9.4/media/css/<?php echo $demo_table;?>";
</style>
<script type="text/javascript" language="javascript" src="../../DataTables-1.9.4/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf-8">
			
			 $(document).ready(function() 
			 {
				  document.getElementById('info').style.display ="";
				  oTable = $('#info').dataTable({
				  	"iDisplayLength": 25,/*cantidad de pag iniciales*/
					"aLengthMenu": [[25, 50,100, -1], [25, 50,100, "All"]],/*lista de lineas*/
					"fnDrawCallback": function ( oSettings ) {
						if ( oSettings.aiDisplay.length == 0 ){return;}
						 
						var nTrs = $('#info tbody tr');

						var iColspan = nTrs[0].getElementsByTagName('td').length;
						var sLastGroup = "";
						var sLastGroup2 = "";
						var sLastGroup4 = "";
						for ( var i=0 ; i<nTrs.length ; i++ )
						{
							
							var iDisplayIndex = oSettings._iDisplayStart + i;
							var sGroup = oSettings.aoData[ oSettings.aiDisplay[iDisplayIndex] ]._aData[0];
							if ( sGroup != sLastGroup )
							{
								var nGroup = document.createElement( 'tr' );
								var nCell = document.createElement( 'td' );
								nCell.colSpan = iColspan;
								nCell.className = "STS";
								nCell.innerHTML = sGroup;
								nGroup.appendChild( nCell );
								nTrs[i].parentNode.insertBefore( nGroup, nTrs[i] );
								sLastGroup = sGroup;
							}
							
							var iDisplayIndex = oSettings._iDisplayStart + i;
							var sGroup2 = oSettings.aoData[ oSettings.aiDisplay[iDisplayIndex] ]._aData[1];
							if ( sGroup2 != sLastGroup2 )
							{
								var nGroup2 = document.createElement( 'tr' );
								var nCell2 = document.createElement( 'td' );
								nCell2.colSpan = iColspan;
								nCell2.className = "ADSSTS";
								nCell2.innerHTML = sGroup2;
								nGroup2.appendChild( nCell2 );
								nTrs[i].parentNode.insertBefore( nGroup2, nTrs[i] );
								sLastGroup2 = sGroup2;
							}
							
							var iDisplayIndex = oSettings._iDisplayStart + i;
							var sGroup4 = oSettings.aoData[ oSettings.aiDisplay[iDisplayIndex] ]._aData[2];
							if ( sGroup4 != sLastGroup4 )
							{
								var nGroup4 = document.createElement( 'tr' );
								var nCell4 = document.createElement( 'td' );
								nCell4.colSpan = iColspan;
								nCell4.className = "AISDES";
								nCell4.innerHTML = sGroup4;
								nGroup4.appendChild( nCell4 );
								nTrs[i].parentNode.insertBefore( nGroup4, nTrs[i] );
								sLastGroup4 = sGroup4;
							}
						}
						
					},
					"columnDefs": [
						{
							"targets": [ 0 ],
							"visible": false,
							"searchable": false 
						}
					],
					"aoColumnDefs": [
						{ "bVisible": false, "aTargets": [ 0 ] }, 
						{ "bVisible": false, "aTargets": [ 1 ] }, 
						{ "bVisible": false, "aTargets": [ 2 ] } 
					],
					"aaSortingFixed": [
						[ 0, 'asc' ], 
						[ 1, 'asc' ], 
						[ 2, 'desc' ]
					],
					"aaSorting": [[ 3, 'desc' ]],
					"sDom": 'lfr<"giveHeight"t>ip',
					"bStateSave": true,
					"oLanguage": 
					{
			          "sLengthMenu": "Mostrar _MENU_ registros por Pag.",
						"sZeroRecords": "No Existen Registros",
						"sInfo": "Mostrando desde _START_ hasta _END_ de _TOTAL_ registros",
						"sInfoEmpty": "Mostrando desde 0 hasta 0 de 0 registros",
						"sInfoFiltered": "(filtrando de _MAX_ de registros)",
						"sSearch": "Buscar",
					  "oPaginate": 
					  {
			            "sNext": "Siguiente",
						"sPrevious": "Anterior"
			          },
					  
			        }
				});
			  } 
			  );
</script>
</head>
<body>
<div id="wrapper">
  <?php include("../../superior.php"); ?>
  <div id="page">
      <?php include("../../validar.php");?>

<!--<div id="sidebar">
	    <ul>
	      <li>
	        <h2>Estad&iacute;sticas</h2>
	        <p>Aqu&iacute; se reflejar&aacute;n estad&iacute;sticas particulares seg&uacute;n en la opci&oacute;n del sistema que se encuentre</p>
          </li>
	      <li>
	        <h2>Categories</h2>
	        <ul>
	          <li><a href="#">Aliquam libero</a></li>
	          <li><a href="#">Consectetuer adipiscing elit</a></li>
	          <li><a href="#">Metus aliquam pellentesque</a></li>
	          <li><a href="#">Suspendisse iaculis mauris</a></li>
	          <li><a href="#">Urnanet non molestie semper</a></li>
	          <li><a href="#">Proin gravida orci porttitor</a></li>
            </ul>
          </li>
	      <li>
	        <h2>Blogroll</h2>
	        <ul>
	          <li><a href="#">Aliquam libero</a></li>
	          <li><a href="#">Consectetuer adipiscing elit</a></li>
	          <li><a href="#">Metus aliquam pellentesque</a></li>
	          <li><a href="#">Suspendisse iaculis mauris</a></li>
	          <li><a href="#">Urnanet non molestie semper</a></li>
	          <li><a href="#">Proin gravida orci porttitor</a></li>
            </ul>
          </li>
	      <li>
	        <h2>Archives</h2>
	        <ul>
	          <li><a href="#">Aliquam libero</a></li>
	          <li><a href="#">Consectetuer adipiscing elit</a></li>
	          <li><a href="#">Metus aliquam pellentesque</a></li>
	          <li><a href="#">Suspendisse iaculis mauris</a></li>
	          <li><a href="#">Urnanet non molestie semper</a></li>
	          <li><a href="#">Proin gravida orci porttitor</a></li>
            </ul>
          </li>
      </ul>
    </div>-->
	  
         <?php 
		 	$wsolicitud=0;
			
						/*nuevo*/
			//if ($solicitudpagina==0){
						
							
					$sql="SELECT T1.ACICOD, T1.AALCOD, T1.ADPCOD, T1.ATSCOD, T1.ADSNRO, T1.ATRDES, T1.ATROBS, 
								   T1.ATRFEC, T1.ATRMOR, T1.AUSCOD, T1.ADSSTS, T2.ATSDES, T2.ATSAPR, T1.ATSCOD, 
								   T1.ADSNRO, T3.AISDES
							FROM IV35FP T1 
								INNER JOIN IS15FP T2 ON ( T1.ACICOD=T2.ACICOD AND T1.ATSCOD=T2.ATSCOD )
								INNER JOIN IV42FP T3 ON ( T1.ACICOD=T3.ACICOD AND T1.AISCOD=T3.AISCOD )
							WHERE   T1.ACICOD='40' AND T1.AALCOD NOT IN ('0001') AND T1.ATSCOD IN ('04') AND 
									T1.ADSSTS NOT IN ('01', '02', '04')
							ORDER BY T1.AUSCOD, T1.AALCOD, T1.ATSCOD, T1.ADSSTS DESC ";
						//echo $sql;
						
						$resultt=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111"));
						$z=0;
						$lin=1;
						$limitep=$_SESSION['solicitudlineasporpaginat'];
						$pag=1;
						$primero='S';
						
						while(odbc_fetch_row($resultt))
						{
							$jml = odbc_num_fields($resultt);
							$row[$z]["pagina"] =  $pag;
							for($i=1;$i<=$jml;$i++)
							{
								$row[$z][odbc_field_name($resultt,$i)] =  odbc_result($resultt,$i);
							}
							$z++;
							if ($lin>=$limitep) 
							{
								$limitep+=$_SESSION['solicitudlineasporpaginat'];
								$pag++;
							}
							$lin++;
						}
						/*guarla informacion en un arreglo*/
						$_SESSION['solicitudarreglo']=$row;
						
						/*permite mostrar pag seleccionada*/
						if ($solicitudpagina==0) 	
						{
							$totsol=($lin-1);
							$_SESSION['totalsolicitudes']=$totsol;
							$solicitudpagina=1;
							$_SESSION['solicitudpaginas']=$pag;
						}
			//}
?>
		<div id="content3" >
        	<table width="100%"   border="0">
              <tr>
                <td width="84%" >
                	<h1 align="center" class="title">Manejo de Actas</h1>
                 <hr />
                 </td>
                <td width="16%" ><div align="left">
                  <table width="100%"  border="0">
                    <tr>
					  <th width="36%" scope="col">&nbsp;</th>
	                  <th width="30%" >
                          <a href="exportaraexcel.php" target="_blank">
	                          <img src="../../images/excel.jpg" alt="" width="25" height="25" />
                          </a>
                      </th>
                    </tr>
                  </table>
                </div></td>
              </tr>
            </table>
		<div id="container">
            <div id="demo">
                <table width="100%" id="info" style="display:">
                    <thead>
                      <tr>
                      	<th><div align="left"><strong>STS</strong></div></th>
                        <th><div align="left"><strong>ADSSTS</strong></div></th>
                        <th><div align="left"><strong>AISDES</strong></div></th>
                        <th>Nro. Nota Entrega</th>
                        <th>Art�culo</th>
                        <th>Opciones</th>
                      </tr>
                    </thead>
                    <tbody>
              <?php 
			  
			  	$tipopar="";
			  	$tipotec="";
				
				$paginat=$_SESSION['solicitudarreglo'];
                            
				$pagact=$solicitudpagina;
				for($g=0; $g < (count($paginat)); $g++)
				{
				  	
 			?>

              <tr>
	            <td><div align="left"><strong><?php echo trim($paginat[$g]["ADSSTS"]);?></strong></div></td>
                <td><div align="left"><strong><?php echo status('ADSSTS',trim($paginat[$g]["ADSSTS"]));?></strong></div></td>
              	<td><div align="left"><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $paginat[$g]["AISDES"];?></strong></div></td>
                
                <td><div align="center"><strong><?php echo add_ceros($paginat[$g]["ADSNRO"],6);?></strong></div></td>
                <td>
                
                	<div align="center">
                		<?php 
						//echo "**".$paginat[$g]["ADSSTS"]."**";
							$sql="SELECT T2.AARDES, T1.AARCOD, T1.ATRCAN 
									FROM IV36FP T1 
										INNER JOIN IV05FP T2 ON (T1.ACICOD=T2.ACICOD AND T1.AARCOD=T2.AARCOD) 
									WHERE T1.ACICOD='".$Compania."' AND T1.ADSNRO=".$paginat[$g]["ADSNRO"]." ";
										
							//echo $sql;
						
							$resultDETA=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111"));
							$can=0;
							while(odbc_fetch_row($resultDETA))
							{
								echo $can>0? "<br />":"";
								echo odbc_result($resultDETA, 'AARDES')." (".odbc_result($resultDETA, 'AARCOD').") Cantidad-".number_format(odbc_result($resultDETA, 'ATRCAN'),0)." ";
								$can++;
							}
								
						?>
                	</div>
                </td>
                <td valign="top" style="vertical-align:middle;">
                	<ul id="opciones">
                        <li>
                            <a href="javascript:openActa('I','<?php echo trim($paginat[$g]["ATSCOD"]);?>','<?php echo $paginat[$g]["ADSNRO"] ?>','<?php echo $Compania; ?>','<?php echo $paginat[$g]["AALCOD"] ?>')">
                            	<img src="../../images/pdf_I.jpg" title="Acta Instalaci�n" alt="Acta Instalaci�n" width="22" border="0">
                            </a>
                        </li>
                                                
                        <?php 
                        if( ( $paginat[$g]["ADSSTS"]=='06' ) ){
                        ?>
                            <li>	
                            	<a href="javascript:cambiarStatusActa('<?php echo trim($paginat[$g]["AALCOD"]); ?>','<?php echo trim(alamcen(trim($paginat[$g]["AALCOD"]), $Compania)); ?>','<?php echo trim($paginat[$g]["ADSNRO"]);?>','<?php echo trim($paginat[$g]["ATSCOD"]);?>')">
                        			<img src="../../images/instalcion.png" title="Instalaci�n" width="22" border="0">
                        		</a>
                        	</li>
                        <?php 
                        }
                        ?>
                        
						
						
                        
                        <li>
                            <a href="javascript:openActa('R','<?php echo trim($paginat[$g]["ATSCOD"]);?>','<?php echo $paginat[$g]["ADSNRO"] ?>','<?php echo $Compania; ?>','<?php echo $paginat[$g]["AALCOD"] ?>')">
                                <img src="../../images/pdf_R.jpg" title="Acta Recepci�n" alt="Acta Recepci�n" width="22" border="0">
                            </a>
                        </li> 
                        
                        <?php 
                        if( ( $paginat[$g]["ADSSTS"]=='08' ) ){
                        ?>
                            <li>	
                            	<a href="javascript:cambiarStatusActa('<?php echo trim($paginat[$g]["AALCOD"]); ?>','<?php echo trim(alamcen(trim($paginat[$g]["AALCOD"]), $Compania)); ?>','<?php echo trim($paginat[$g]["ADSNRO"]);?>','<?php echo trim($paginat[$g]["ATSCOD"]);?>')">
                        			<img src="../../images/recepcion.png" title="Recepci�n" height="19" border="0">
                        		</a>
                        	</li>
                        <?php 
                        }
                        ?>
                        
                        <?php 
                        if( ( $paginat[$g]["ADSSTS"]=='06' ) || ( $paginat[$g]["ADSSTS"]=='08' ) || ( $paginat[$g]["ADSSTS"]=='09' ) ){
                        ?>
                            <li>
                                <a rel="shadowbox;width=1000;height=500" title="Carga Param. Adicionales" href="dotacionesfparamAdicionales.php?&pedid=<?php echo trim($paginat[$g]["ADSNRO"]);?>&almacen=<?php echo trim($paginat[$g]["AALCOD"]);;?>&tipo=<?php echo trim($paginat[$g]["ATSCOD"]);?>">
                                    <img src="../../images/recepcion_documentos_envio.png" title="Carga Param. Adicionales" width="22" border="0">
                                </a>
                            </li>
                        <?php 
                        }
                        ?>
                    	
                        <?php 
                        if( ( $paginat[$g]["ADSSTS"]=='09' ) ){
                        ?>
                            <li>	
                            	<a href="javascript:cambiarStatusActa('<?php echo trim($paginat[$g]["AALCOD"]); ?>','<?php echo trim(alamcen(trim($paginat[$g]["AALCOD"]), $Compania)); ?>','<?php echo trim($paginat[$g]["ADSNRO"]);?>','<?php echo trim($paginat[$g]["ATSCOD"]);?>')">
                        			<img src="../../images/aprobado ok.gif" title="Cerrar" width="22" border="0">
                        		</a>
                        	</li>
                        <?php 
                        }
                        ?>
                        
                	</ul>
              	</td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
          </div>
			
          </td>
          </tr>
          </table>
	    </div>
	  </div>
  </div>
		<!-- end #content -->
		<!-- end #sidebar -->
	<div style="clear: both;">&nbsp;</div>
  </div>
	<!-- end #page -->
	<div id="footer">
		<div id="opcionesmasusadas"><?php include("../../opcionesmasusadasphp.php");?></div>
	</div>
	<!-- end #footer -->
</body>
</html>
