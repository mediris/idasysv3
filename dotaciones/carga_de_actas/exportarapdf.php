<?php 
	session_start();
	include("../../conectar.php");
	require_once('../html2pdf/html2pdf.class.php');
 	$acta = trim($_REQUEST["acta"]);
	//$Compania = trim($_REQUEST["comp"]);

	ob_start();
	if($acta=='R'){
   		include('actarecepcion_individual_INVAP.php');
	}else if($acta=='I'){
		if($Compania=='42'){
			$atscod = trim($_REQUEST["atscod"]);
			$adsnro = trim($_REQUEST["adsnro"]);
			if($atscod =='01' && $adsnro ==93 ){
				include('actainstalacion_bienes_MEDIX_extra.php');
			}else if($atscod =='01' && $adsnro ==127 ){
				include('actainstalacion_bienes_MEDIX_extra_127.php');
			}else{
				include('actainstalacion_bienes_MEDIX.php');
			}
		} else { 
   		 	include('actainstalacion_bienes_INVAP.php');
   		}
	}
	
	
	
	try
	{
		$content = ob_get_clean();
		$html2pdf = new HTML2PDF('Portrait','letter','es');
		$html2pdf->setTestTdInOnePage(false);
		$html2pdf->pdf->SetDisplayMode('fullpage');
		$html2pdf = new HTML2PDF('P','letter','en', true, 'UTF-8', array(17, 10, 15, 17));
		$html2pdf->WriteHTML($content);
		//$html2pdf->Output('Listado de Estudios.pdf', 'D');//para forzar la descarga
		if($acta=='R'){
   			$html2pdf->Output('Acta_de_Recepcion_Individual_'.add_ceros($adsnro,6).'_'.date('Ymd').'.pdf');
		}else if($acta=='I'){
			$html2pdf->Output('Acta_de_Instalacion_Bienes_'.add_ceros($adsnro,6).'_'.date('Ymd').'.pdf');
		}
		
		
		//auditoriagrabar($modulo,"*PDF","$atrnum","Se ha generado Dotaci�n");
	}catch(HTML2PDF_exception $e) {
		echo $e;
		exit;
	}

?>