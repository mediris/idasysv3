<?php session_start();
include("../../conectar.php");
$arqnro = trim($_GET["num"]);
include("../../PHPExcel/PHPExcel.php");
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header("Content-disposition: attachment; filename=Manejo_Actas_".formatDate($Fechaactual,'dd.mm.aaa','aaaa_mm_dd').".xlsx");
header('Cache-Control: max-age=0');
?>
<?php 
		$wsolicitud=0;

					if($Compania=='42'){
						$atscodLis = "'01'";
						$notin ='';
						$almacen = '0001, 0002, 0003';
					}else{
						$atscodLis = "'04'";
						$notin=" and 
								T1.ADSNRO <> '325' AND
								T1.ADSNRO <> '491' AND
								T1.ADSNRO <> '492' ";
						$almacen = '0001';
					}
			
					$sql="SELECT T1.ACICOD, T1.AALCOD, T1.ADPCOD, T1.ATSCOD, T1.ADSNRO, T1.ATRDES, T1.ATROBS, 
								   T1.ATRFEC, T1.ATRMOR, T1.AUSCOD,
								   (case when T1.ADSSTS in ('03', '06', '07') then '07' else T1.ADSSTS end ) as ADSSTS,  
								   T2.ATSDES, T2.ATSAPR, T1.ATSCOD, 
								   T1.ADSNRO, T3.AISDES,
								   (SELECT T6.AAPVLA FROM iv46fp T6 WHERE T6.ACICOD=T1.ACICOD AND T6.ATSCOD=T1.ATSCOD AND T6.ADSNRO=T1.ADSNRO and T6.APACOD IN ('1504') ) AS N1504 ,
								   (SELECT T6.AAPVLA FROM iv46fp T6 WHERE T6.ACICOD=T1.ACICOD AND T6.ATSCOD=T1.ATSCOD AND T6.ADSNRO=T1.ADSNRO and T6.APACOD IN ('1507') ) AS N1507 ,
								   (SELECT T6.AAPVLA FROM iv46fp T6 WHERE T6.ACICOD=T1.ACICOD AND T6.ATSCOD=T1.ATSCOD AND T6.ADSNRO=T1.ADSNRO and T6.APACOD IN ('1508') ) AS N1508 ,
								   (SELECT T6.AAPVLA FROM IV46FP T6 WHERE T6.ACICOD=T1.ACICOD AND T6.ATSCOD=T1.ATSCOD AND T6.ADSNRO=T1.ADSNRO and T6.APACOD IN ('2003') ) AS N2003 ,
								   (SELECT T6.AAPVLA FROM IV46FP T6 WHERE T6.ACICOD=T1.ACICOD AND T6.ATSCOD=T1.ATSCOD AND T6.ADSNRO=T1.ADSNRO and T6.APACOD IN ('2010') ) AS N2010 ,
								   (SELECT T6.AAPVLA FROM IV46FP T6 WHERE T6.ACICOD=T1.ACICOD AND T6.ATSCOD=T1.ATSCOD AND T6.ADSNRO=T1.ADSNRO and T6.APACOD IN ('2011') ) AS N2011
							FROM IV35FP T1 
								INNER JOIN IS15FP T2 ON ( T1.ACICOD=T2.ACICOD AND T1.ATSCOD=T2.ATSCOD )
								INNER JOIN IV42FP T3 ON ( T1.ACICOD=T3.ACICOD AND T1.AISCOD=T3.AISCOD )
							WHERE   T1.ACICOD='$Compania' AND T1.AALCOD NOT IN ('$almacen') AND T1.ATSCOD IN ($atscodLis) AND 
									T1.ADSSTS NOT IN ('01', '02', '04') ".$notin."
							ORDER BY (case when T1.ADSSTS in ('03', '06', '07') then '01' else T1.ADSSTS end ), T3.AISDES, T1.ADSNRO ";
						//echo $sql;
						
						$resultt=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111"));
						$z=0;
						$lin=1;
						$limitep=$_SESSION['solicitudlineasporpaginat'];
						$pag=1;
						$primero='S';
						
						while(odbc_fetch_row($resultt))
						{
							$jml = odbc_num_fields($resultt);
							$row[$z]["pagina"] =  $pag;
							for($i=1;$i<=$jml;$i++)
							{
								$row[$z][odbc_field_name($resultt,$i)] =  odbc_result($resultt,$i);
							}
							$z++;
							if ($lin>=$limitep) 
							{
								$limitep+=$_SESSION['solicitudlineasporpaginat'];
								$pag++;
							}
							$lin++;
						}
						/*guarla informacion en un arreglo*/
						$_SESSION['solicitudarreglo']=$row;
						
						/*permite mostrar pag seleccionada*/
						if ($solicitudpagina==0) 	
						{
							$totsol=($lin-1);
							$_SESSION['totalsolicitudes']=$totsol;
							$solicitudpagina=1;
							$_SESSION['solicitudpaginas']=$pag;
						}

						$paginat=$_SESSION['solicitudarreglo'];
						//var_dump($paginat);

						$excel = new PHPExcel();

						//Propiedades del Excel
						$excel->getProperties()
						   ->setCreator('IDASYS WEB')
						   ->setTitle('Manejo de Actas')
						   ->setLastModifiedBy('IDASYS WEB')
						   ->setDescription('A demo to show how to use PHPExcel to manipulate an Excel file')
						   ->setSubject('Manejo de Actas')
						   ->setKeywords('excel php office phpexcel')
						   ->setCategory('inventario')
						   ;

						$exceldata = $excel->getSheet(0);
						$exceldata->setTitle('Manejo_de_Actas');

						//Titulo
						$styleArray = array(
							    'font'  => array(
							        'bold'  => true
							    ),
							    'fill' => array(
					            'type' => PHPExcel_Style_Fill::FILL_SOLID,
					            'color' => array('rgb' => 'BDBDBD')
					            ),
					            'alignment' => array(
	            				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
	            				),
	            				'borders' => array(
					            'allborders' => array(
					                'style' => PHPExcel_Style_Border::BORDER_THIN,
					            	)
					        	)
						    );

						//Head Logo
					    $objDrawing = new PHPExcel_Worksheet_Drawing();
				        $objDrawing->setName('Logo');
				        $objDrawing->setDescription('Logo');
				        if($Compania == '14'){ 
					        $objDrawing->setPath('../../images/logoidacadef2005.png');
					        $objDrawing->setHeight(70);
					        $objDrawing->setWidth(200);
					    } else {
					    	$objDrawing->setPath('../../images/MEDITRON_logo_rif.png');
					        $objDrawing->setHeight(70);
				        	$objDrawing->setWidth(330);
					    }

				        $objDrawing->setCoordinates('A1');
				        $objDrawing->setWorksheet($excel->getActiveSheet());
				        //
				        
				        //Sub Logo
				        $objDrawing2 = new PHPExcel_Worksheet_Drawing();
				        $objDrawing2->setName('SubLogo');
				        $objDrawing2->setDescription('SubLogo');
				        if($Compania=='42'){
					        $objDrawing2->setPath('../../images/logo_medix.png');
					        $objDrawing2->setHeight(200);
					        $objDrawing2->setWidth(190);
				    	} else {
				    		$objDrawing2->setPath('../../images/logo_invap.png');
					        $objDrawing2->setHeight(70);
					        $objDrawing2->setWidth(170);
				    	}
				    	$objDrawing2->setCoordinates('E1');
					    $objDrawing2->setWorksheet($excel->getActiveSheet());
				        //

						$excel->getActiveSheet()->getCell('A4')->setValue('RIF: '.$Companiarif);

						//Titulo
						if($Compania=='42'){ 
							$excel->getActiveSheet()->getCell('A7')->setValue('Control de Actas de Instalación de Bienes');
						} else {
							$excel->getActiveSheet()->getCell('A7')->setValue('Manejo de Actas');
						}
						$exceldata->mergeCells('A7:G7');
						$excel->getActiveSheet()->getStyle('A7')->getFont()->setBold(true);
						$excel->getActiveSheet()->getStyle('A7')->getFont()->setSize(25);
						$excel->getActiveSheet()->getStyle('A7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
						//

						//Sub titulo 
						/*$excel->getActiveSheet()->getCell('A8')->setValue('Entrada y Salida de ' . $tInventario);
						$exceldata->mergeCells('A8:G8');
						$excel->getActiveSheet()->getStyle('A8')->getFont()->setBold(true);
						$excel->getActiveSheet()->getStyle('A8')->getFont()->setSize(20);
						$excel->getActiveSheet()->getStyle('A8')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);*/

						$excel->getActiveSheet()->getCell('A8')->setValue('Elaborado el '.$Fechaactual.' a las '.$Horaactual2);
						$exceldata->mergeCells('A8:G8');
						$excel->getActiveSheet()->getStyle('A8')->getFont()->setBold(true);
						$excel->getActiveSheet()->getStyle('A8')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

						/*$excel->getActiveSheet()->getCell('A10')->setValue('Almacén Principal: MEDIX');
						$exceldata->mergeCells('A10:G10');
						$excel->getActiveSheet()->getStyle('A4')->getFont()->setBold(true);
						$excel->getActiveSheet()->getStyle('A10')->getFont()->setBold(true);
						$excel->getActiveSheet()->getStyle('A10')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

						$excel->getActiveSheet()->getCell('A11')->setValue('Filtrado por: '.$tInventario);
						$exceldata->mergeCells('A11:G11');
						$excel->getActiveSheet()->getStyle('A11')->getFont()->setBold(true);
						$excel->getActiveSheet()->getStyle('A11')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);*/

						$exceldata->setCellValue('A9', 'Status');
						$exceldata->setCellValue('B9', 'Nro. Nota de Entrega');
						if($Compania=='42'){
							$exceldata->setCellValue('C9', 'Fecha de Recepción - Nota de Entrega');
						} else {
							$exceldata->setCellValue('C9', 'Fecha Nota de Entrega');
						}
						$exceldata->setCellValue('D9', 'Hospital');
						$exceldata->setCellValue('E9', 'Artículo');
						$exceldata->setCellValue('F9', 'Fecha de Instalación');
						$exceldata->setCellValue('G9', 'Fecha de Recepción');
						$excel->getActiveSheet()->getStyle('A9:G9')->applyFromArray($styleArray);
			   		
					    //Genera Data
					    $pos = 10;
					    for($g=0; $g < (count($paginat)); $g++)
						{
							//Status
							$cella = trim($paginat[$g]["ADSSTS"])=='07'?"Pendiente por Imprimir":status('ADSSTS',trim($paginat[$g]["ADSSTS"])); 
							$exceldata->setCellValue('A' . $pos, trim($paginat[$g]["ADSSTS"])." - ".utf8_encode($cella)); 

							//Nro. Nota Entrega
							$exceldata->setCellValue('B' . $pos, add_ceros($paginat[$g]["ADSNRO"],6));

							//Fecha de Nota Entrega
							if($Compania=='42'){
								$cellc = trim($paginat[$g]["N2003"])!=''?formatDate($paginat[$g]["N2003"],'dd.mm.aaaa','dd/mm/aaaa'):'--';
								$exceldata->setCellValue('C' . $pos, trim($cellc)); 
							} else {
								$cellc = trim($paginat[$g]["N1504"])!=''?formatDate($paginat[$g]["N1504"],'dd.mm.aaaa','dd/mm/aaaa'):'--';
								$exceldata->setCellValue('C' . $pos, trim($cellc)); 
							}
							//Cliente
							$exceldata->setCellValue('D' . $pos, utf8_encode(trim($paginat[$g]["AISDES"])));

							//Articulo
							$sql="SELECT T2.AARDES, T1.AARCOD, T1.ATRCAN 
									FROM IV36FP T1 
										INNER JOIN IV05FP T2 ON (T1.ACICOD=T2.ACICOD AND T1.AARCOD=T2.AARCOD) 
									WHERE T1.ACICOD='".$Compania."' AND T1.ADSNRO=".$paginat[$g]["ADSNRO"]." ";
							$resultDETA=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111"));
							$can=0;
							$listArt = Array();
							while(odbc_fetch_row($resultDETA))
							{
								$art = trim(odbc_result($resultDETA, 'AARDES'))." (".trim(odbc_result($resultDETA, 'AARCOD')).") Cantidad - ".number_format(odbc_result($resultDETA, 'ATRCAN'),0)."";
								$listArt[] = $art;
								$can++;
							}

							$exceldata->setCellValue('E' . $pos, utf8_encode(implode(", ", $listArt)));
							
							if($Compania == '42') {
								//Fecha de Instalacion
		                        $cellf = trim($paginat[$g]["N2011"])!=''?formatDate($paginat[$g]["N2011"],'dd.mm.aaaa','dd/mm/aaaa'):'--'; 
		                        $exceldata->setCellValue('F' . $pos, $cellf);

		                        //Fecha de Recepcion 
		                        $cellg = trim($paginat[$g]["N2010"])!=''?formatDate($paginat[$g]["N2003"],'dd.mm.aaaa','dd/mm/aaaa'):'--';
		                        $exceldata->setCellValue('G' . $pos, $cellg);
							} else {
								//Fecha de Instalacion
		                        $cellf = trim($paginat[$g]["N1507"])!=''?formatDate($paginat[$g]["N1507"],'dd.mm.aaaa','dd/mm/aaaa'):'--'; 
		                        $exceldata->setCellValue('F' . $pos, $cellf);

		                        //Fecha de Recepcion 
		                        $cellg = trim($paginat[$g]["N1508"])!=''?formatDate($paginat[$g]["N1508"],'dd.mm.aaaa','dd/mm/aaaa'):'--';
		                        $exceldata->setCellValue('G' . $pos, $cellg);
							}
	                        $pos++;   
						}                                  

						//Obtiene la cantidad de rows del excel (-13 rows del titulo) 			
						$rowCount = $excel->getActiveSheet()->getHighestRow();
						$rowCount = $rowCount-9;

						//Bordes y ajuste de anchura para la data generada
						$lin = 10;
				   		$prevpos = $lin-1;
						$highestColumn = $excel->getActiveSheet()->getHighestColumn();
						for($col = 'A'; $col <= $highestColumn; $col++)
					    {
					    	$value = $excel->getActiveSheet()->getCell($col.$prevpos)->getValue();
				   			$width = mb_strwidth($value);
				   			if($width == 0){
				   				$prevpos = $lin-1;
				   				$value = $excel->getActiveSheet()->getCell($col.$prevpos)->getValue();
				   				$width = mb_strwidth($value);
				   			}
					    	for($i = 0; $i < $rowCount; $i++){
					    		$value1 = $excel->getActiveSheet()->getCell($col.$lin)->getValue();
				   				$width1 = mb_strwidth($value1);
								if($width1 > $width){
									$width = $width1;
								}
								$excel->getActiveSheet()->getStyle($col.$lin)->applyFromArray(array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN,))));
								$excel->getActiveSheet()->getStyle($col.$lin)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
								//Texto centrado a Columnas especificas
								$excel->getActiveSheet()->getStyle('A'.$lin)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
								$excel->getActiveSheet()->getStyle('B'.$lin)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
								$excel->getActiveSheet()->getStyle('C'.$lin)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
								$excel->getActiveSheet()->getStyle('D'.$lin)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
								$excel->getActiveSheet()->getStyle('E'.$lin)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
								$excel->getActiveSheet()->getStyle('F'.$lin)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
								$excel->getActiveSheet()->getStyle('G'.$lin)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
								$excel->getActiveSheet()->getStyle($col.$lin)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
								//Wrap Text
								$excel->getActiveSheet()->getStyle($col.$lin)->getAlignment()->setWrapText(true);
								$lin++;
								$prevpos++;
							}
							$width = $width+2;
							// IF para colocar un width estatico a la Columna y se aplique el WrapText
							if($col == 'D' || $col == 'E'){
								$width = 45;
								$excel->getActiveSheet()->getColumnDimension($col)->setAutoSize(false);
								$excel->getActiveSheet()->getColumnDimension($col)->setWidth($width);
							} else {
								$excel->getActiveSheet()->getColumnDimension($col)->setAutoSize(false);
								$excel->getActiveSheet()->getColumnDimension($col)->setWidth($width);
							}
							$width = 0;
							$lin = 10;
							$prevpos = $lin-1;
						}

						//Ocultar Columnas
						if ($Compania=='42') {
							$excel->getActiveSheet()->getColumnDimension('G')->setVisible(false);
						}

						//
						//Se genera el excel
				        $writer = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
						$writer->save('php://output');
						exit();
			?>
