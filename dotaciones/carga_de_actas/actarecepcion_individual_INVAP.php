<?php 

 $atscod = trim($_REQUEST["atscod"]);
 $adsnro = trim($_REQUEST["adsnro"]);
 $acicod = trim($_REQUEST["acicod"]);
 $aalcod = trim($_REQUEST["aalcod"]);
 
 

 $sql="SELECT T1.ADSNRO, T2.AISDES, T2.AISOBS,
			(SELECT T9.AAPVLA FROM IV46FP T9 WHERE T9.ACICOD=T2.ACICOD and T9.ADSNRO=T1.ADSNRO AND T9.APACOD IN ('1509') ) AS ITEM, 
			(SELECT T9.AAPVLA FROM IV46FP T9 WHERE T9.ACICOD=T2.ACICOD and T9.ADSNRO=T1.ADSNRO AND T9.APACOD IN ('1510') ) AS NRITEM 
		FROM IV35FP T1 
			INNER JOIN  IV42FP T2 ON (T1.ACICOD=T2.ACICOD AND T1.AISCOD=T2.AISCOD) 
		 WHERE T1.ACICOD='".$Compania."' AND T1.ADSNRO =".$adsnro."  AND T1.ATSCOD='".$atscod."' AND T1.AALCOD='".$aalcod."'";
	 
	 $result=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111"));

?>

<page backtop="30mm" > 

<page_header>
	<table border="0" style="text-align:center;" align="center">
    	<tr>
        	<td >
            	<img src="http://<?php echo $Direccionip;?>/idasysv3/images/logo_mpps.png" style="height:90px"/><br />
                <!--<img src="http://<?php echo $Direccionip;?>/idasysv3/images/pueblo_victorioso.png" style="height:40px"/>-->
            </td>
            <td style="width:210px">
            	<img src="http://<?php echo $Direccionip;?>/idasysv3/images/convenio_argentina_venezuela.png" style="height:80px"/>
            </td>
            <td style="width:210px">
            	<img src="http://<?php echo $Direccionip;?>/idasysv3/images/logo_invap.png" style="height:40px"/>
            </td>
        </tr>
    </table>
</page_header>


<div class=WordSection1>

<p class=MsoNormal style="margin-top:6.0pt;margin-right:0cm;margin-bottom:6.0pt;margin-left:0cm;text-align:center">
    <b>
        <u>
            <span lang=ES-TRAD style="font-size:13px;;letter-spacing:1.0pt;text-align:center">
            	ACTA INDIVIDUAL DE RECEPCI&Oacute;N DE BIENES: 
            </span>
        </u>
    </b>
</p>

<p style="text-align:justify;line-height:125%;">
    <span lang=ES-TRAD style="font-size:13px;line-height:125%;">Yo, ___________________, venezolano, mayor de edad, titular de la C&eacute;dula de 
        Identidad N� __________, actuando en mi car&aacute;cter de _________________________, designado 
        por la Direcci&oacute;n Estatal de Salud del Estado ___________________, para recibir bienes destinados al 
        <?php echo utf8_encode(odbc_result($result,'AISDES')); ?>, adscrito al 
        Ministerio del Poder Popular Para la Salud, ubicado en <?php echo utf8_encode(trim(odbc_result($result,'AISOBS'))); ?>, por 
        el presente documento declaro: Recibo en este acto los bienes identificados en
        <?php  if(odbc_result($result,'ADSNRO')=='712' || odbc_result($result,'ADSNRO')=='1129'){//712 y 1129
					echo ' las Notas de Entregas No. '.add_ceros('712',6)."/".date('Y')." y No. ".add_ceros('1129',6)."/".date('Y');
					
				}else if(odbc_result($result,'ADSNRO')=='620' || odbc_result($result,'ADSNRO')=='1130'){//620 y 1130
					echo ' las Notas de Entregas No. '.add_ceros('620',6)."/".date('Y')." y No. ".add_ceros('1130',6)."/".date('Y');
					
				}else if(odbc_result($result,'ADSNRO')=='1461' || odbc_result($result,'ADSNRO')=='1478'){//1461 y 1478
					echo ' las Notas de Entregas No. '.add_ceros('1461',6)."/".date('Y')." y No. ".add_ceros('1478',6)."/".date('Y');
					
				}else if(odbc_result($result,'ADSNRO')=='812' || odbc_result($result,'ADSNRO')=='1324'){//812 / 1324
					echo ' las Notas de Entregas No. '.add_ceros('812',6)."/".date('Y')." y No. ".add_ceros('1324',6)."/".date('Y');
					
				}else if(odbc_result($result,'ADSNRO')=='811' || odbc_result($result,'ADSNRO')=='1322'){//811 / 1322 - MODIFICADO 06-01-2017
                    echo ' las Notas de Entregas No. '.add_ceros('811',6).", ".add_ceros('812',6)." y No. ".add_ceros('1322',6);
                    
                }else if(odbc_result($result,'ADSNRO')=='1744' || odbc_result($result,'ADSNRO')=='366'){//1744 y 366
                    echo 'NOTAS DE ENTREGAS No. '.add_ceros('1744',6)."/".date('Y')." y No. ".add_ceros('366',6)."/".date('Y');
                    
                }else if(odbc_result($result,'ADSNRO')=='1745' || odbc_result($result,'ADSNRO')=='728'){//1745 y 728
                    echo 'NOTAS DE ENTREGAS No. '.add_ceros('1745',6)."/".date('Y')." y No. ".add_ceros('728',6)."/".date('Y');
				}else if(odbc_result($result,'ADSNRO')=='267' || odbc_result($result,'ADSNRO')=='918' || odbc_result($result,'ADSNRO')=='276' || odbc_result($result,'ADSNRO')=='916'){//Condiciones sin A�O
                    echo 'NOTAS DE ENTREGAS No. '.add_ceros(trim(odbc_result($result, 'ADSNRO')),6);	
                }else{
					echo ' la Nota de Entrega No. '.add_ceros( odbc_result($result,'ADSNRO'),6)."/".date('Y');
				}
		?>, en las cantidades all&iacute; indicadas, los cuales fueron debidamente verificados y probados, 
        encontr&aacute;ndose que son nuevos y est&aacute;n en perfectas condiciones de operatividad y funcionamiento
		<?php if(odbc_result($result,'NRITEM')!='49' && odbc_result($result,'NRITEM')!='50' && 
				 odbc_result($result,'NRITEM')!='51' && odbc_result($result,'NRITEM')!='52' &&
				 odbc_result($result,'NRITEM')!='53' && odbc_result($result,'NRITEM')!='54' &&
				 odbc_result($result,'NRITEM')!='55' && odbc_result($result,'NRITEM')!='56' &&
				 odbc_result($result,'NRITEM')!='57' && odbc_result($result,'NRITEM')!='58' &&
				 odbc_result($result,'NRITEM')!='59' && odbc_result($result,'NRITEM')!='60' ){ 
				 
					 if( odbc_result($result,'ADSNRO')!='1193' && odbc_result($result,'ADSNRO')!='1195' && 
					 	 odbc_result($result,'ADSNRO')!='1205' && odbc_result($result,'ADSNRO')!='1213' && 
						 odbc_result($result,'ADSNRO')!='1215' && odbc_result($result,'ADSNRO')!='1225' && 
						 odbc_result($result,'ADSNRO')!='1227' && odbc_result($result,'ADSNRO')!='1237' && 
						 odbc_result($result,'ADSNRO')!='1238' && odbc_result($result,'ADSNRO')!='1285' && 
						 odbc_result($result,'ADSNRO')!='1315' && odbc_result($result,'ADSNRO')!='1316' && 
						 odbc_result($result,'ADSNRO')!='1302' ){
					
						echo ', asimismo se deja constancia de la inducci&oacute;n y 
						entrenamiento dado al personal seg&uacute;n listado anexo.';
					}else{
						echo '.';
					}
				}else{ //ups item 49,50,51,52,53,54,55,56,57,58,59,60 
					echo '.';
				} 
		?>
    </span>
<br /><br />
    <span lang=ES-TRAD style="font-size:13px;line-height:125%;">A partir de la presente fecha asumo en nombre y representaci&oacute;n del Ministerio del
        Poder Popular Para la Salud la guarda y custodia de los bienes entregados, as&iacute;
        como las obligaciones de cuido y mantenimiento de los mismos.
    </span>
<br /><br />
    <span lang=ES-TRAD style="font-size:13px;line-height:125%;">Los bienes aqu&iacute; entregados fueron adquiridos por el Convenio Integral de
        Cooperaci&oacute;n entre la Rep&uacute;blica Bolivariana de Venezuela y la
         Rep&uacute;blica Argentina, seg&uacute;n contrato No. <b>122/2014</b> 
         suscrito en fecha <b>02/12/2014</b>,
        para el "<?php echo utf8_encode('SUMINISTRO DE EQUIPOS PARA EL PROGRAMA DE ONCOLOG�A'); ?>".
    </span>
<br />
    <span lang=ES-TRAD style="font-size:13px;line-height:125%;"><br />OBSERVACIONES:________________________________________________________________________________
        ________________________________________________________________________________________________
        ________________________________________________________________________________________________
    </span>
<br />
	<span lang=ES-TRAD style="font-size:13px;line-height:125%;">En testimonio de lo expuesto, la entrega de los bienes se verifica con la firma del presente
        documento, en se&ntilde;al de aceptaci&oacute;n y conformidad. Se hacen Cinco (5) ejemplares
        de un mismo tenor y a un s&oacute;lo efecto, en ___________, Estado
        _______________, a los ___ d&iacute;as del mes de _________ de _______.
    </span>
</p>
<table  style="width:100%; text-align:center">
	<tr>
    	<td style="width:50%">
        	<p class=MsoBodyText2 align=center style="text-align:center;line-height:125%"><b><span
            lang=ES-TRAD style="">Por: Hospital o Instituto</span></b></p>
            
            <p class=MsoBodyText2 align=center style="text-align:center;line-height:125%"><span
            lang=ES-TRAD style="">Nombre:
            _______________________</span></p>
            
            <p class=MsoBodyText2 align=center style="text-align:center;line-height:125%"><span
            lang=ES-TRAD style="">C.I. No.:
            ________________________</span></p>
            
            <p class=MsoBodyText2 align=center style="text-align:center;line-height:125%"><span
            lang=ES-TRAD style="">Firma y Sello:
            ___________________</span></p>
            
            <p class=MsoNormal align=center style="text-align:center;line-height:125%"><span
            lang=ES-TRAD style="">Cargo:_________________________</span></p>
        </td>
        <td style="width:50%">
        	<p class=MsoBodyText2 align=center style="text-align:center;line-height:125%"><b><span
            lang=ES-TRAD style="">Por: el MPPS</span></b></p>
            
            <p class=MsoBodyText2 align=center style="text-align:center;line-height:125%"><span
            lang=ES-TRAD style="">Nombre:
            _______________________</span></p>
            
            <p class=MsoBodyText2 align=center style="text-align:center;line-height:125%"><span
            lang=ES-TRAD style="">C.I. No.:
            ________________________</span></p>
            
            <p class=MsoBodyText2 align=center style="text-align:center;line-height:125%"><span
            lang=ES-TRAD style="">Firma y Sello:
            ___________________</span></p>
            
            <p class=MsoNormal align=center style="text-align:center;line-height:125%"><span
            lang=ES-TRAD style="">Cargo:_________________________</span></p>
        </td>
    </tr>
    <tr>
    	<td colspan="2">
            <p class=MsoBodyText2 align=center style="text-align:center;line-height:125%"><b><span
            lang=ES-TRAD style="">Por: Meditron, C.A.</span></b></p>
            
            <p class=MsoBodyText2 align=center style="text-align:center;line-height:125%"><span
            lang=ES-TRAD style="">Nombre:
            _______________________</span></p>
            
            <p class=MsoBodyText2 align=center style="text-align:center;line-height:125%"><span
            lang=ES-TRAD style="">C.I. No.:
            ________________________</span></p>
            
            <p class=MsoBodyText2 align=center style="text-align:center;line-height:125%"><span
            lang=ES-TRAD style="">Firma y Sello:
            ___________________</span></p>
            
            <p class=MsoNormal align=center style="text-align:center;line-height:125%"><span
            lang=ES-TRAD style="">Cargo:_________________________</span></p>
        </td>
    </tr>
</table>

</div>

</page>