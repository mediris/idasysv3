<?php 
	session_start();
	include("../../conectar.php");
	include("../../JSON.php");
	$json = new Services_JSON();
	
	$acicod = trim($_REQUEST['acicod']);
	$adsnro = trim($_REQUEST['adsnro']);
	$aalcod = trim($_REQUEST['aalcod']);
	$atscod = trim($_REQUEST['atscod']);
	$doctip = trim($_REQUEST['doctip']);
	
	
	
	$carpeta1 = "D:/actas/".$acicod."_".trim($aalcod)."_".trim($atscod)."_".add_ceros(trim($adsnro),6)."/";
	$carpeta2= "".$acicod."_".trim($aalcod)."_".trim($atscod)."_".add_ceros(trim($adsnro),6)."";
	$return=array();
	
	if(file_exists($carpeta1)){
		if($doctip=='pdfI'){
			$nombre = 'acta_de_instalacion_bienes_'.add_ceros(trim($adsnro),6).'.pdf';
			if(file_exists($carpeta1.$nombre)){	
				$modi 	= date ("d/m/Y H:i:s", filemtime($carpeta1.$nombre));//ultima modificacion
				$tama 	= round(filesize($carpeta1.$nombre)/1024,2,PHP_ROUND_HALF_UP);//tamaño
				//$tipo 	= filetype($carpeta.$nombre);//tipo
				$tipo 	= explode('.', $nombre);
				
				$return = array('val'=>true,'modi'=>$modi, 'tama'=>$tama, 'tipo'=>$tipo[1], 'nomb'=>$nombre, 'carp'=>$carpeta2);
				//echo "salida";
				echo $json->encode($return);		
			}else{
				//echo "no paso ";
				$return = array('val'=>false);
				echo $json->encode($return);		
			}
		}else if($doctip=='pdfR'){
			$nombre = 'acta_de_recepcion_individual_'.add_ceros(trim($adsnro),6).'.pdf';
			if(file_exists($carpeta1.$nombre)){	
				$modi 	= date ("d/m/Y H:i:s", filemtime($carpeta1.$nombre));//ultima modificacion
				$tama 	= round(filesize($carpeta1.$nombre)/1024,2,PHP_ROUND_HALF_UP);//tamaño
				//$tipo 	= filetype($carpeta.$nombre);//tipo
				$tipo 	= explode('.', $nombre);
				
				$return = array('val'=>true,'modi'=>$modi, 'tama'=>$tama, 'tipo'=>$tipo[1], 'nomb'=>$nombre, 'carp'=>$carpeta2);
				//echo "salida";
				echo $json->encode($return);		
			}else{
				//echo "no paso ";
				$return = array('val'=>false);
				echo $json->encode($return);		
			}
		}else if($doctip=='pdfN'){
			$nombre = 'nota_de_entrega_'.add_ceros(trim($adsnro),6).'.pdf';
			if(file_exists($carpeta1.$nombre)){	
				$modi 	= date ("d/m/Y H:i:s", filemtime($carpeta1.$nombre));//ultima modificacion
				$tama 	= round(filesize($carpeta1.$nombre)/1024,2,PHP_ROUND_HALF_UP);//tamaño
				//$tipo 	= filetype($carpeta.$nombre);//tipo
				$tipo 	= explode('.', $nombre);
				
				$return = array('val'=>true,'modi'=>$modi, 'tama'=>$tama, 'tipo'=>$tipo[1], 'nomb'=>$nombre, 'carp'=>$carpeta2);
				//echo "salida";
				echo $json->encode($return);		
			}else{
				//echo "no paso ";
				$return = array('val'=>false);
				echo $json->encode($return);		
			}
		}else{
			$return = array('val'=>false);
			echo $json->encode($return);
		}
	}else{
		//echo "no paso ";
		$return = array('val'=>false);
		echo $json->encode($return);		
	}

?>