<?php 

 $atscod = trim($_REQUEST["atscod"]);
 $adsnro = trim($_REQUEST["adsnro"]);
 $acicod = trim($_REQUEST["acicod"]);
 $aalcod = trim($_REQUEST["aalcod"]);
 
 

 $sql="SELECT T1.ADSNRO, T2.AISDES, T2.AISOBS FROM IV35FP T1 
	 	INNER JOIN  IV42FP T2 ON (T1.ACICOD=T2.ACICOD AND T1.AISCOD=T2.AISCOD) 
	 WHERE T1.ACICOD='".$Compania."' AND T1.ADSNRO =".$adsnro."  AND T1.ATSCOD='".$atscod."' AND T1.AALCOD='".$aalcod."'";
	 
	 $result=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111"));

?>

<page backtop="30mm" > 

<page_header>
	<table border="0" style="text-align:center;" align="center">
    	<tr>
        	<td >
            	<img src="http://<?php echo $Direccionip;?>/idasysv3/images/logo_mpps.png" style="width:200px;height:70px"/><br />
                <img src="http://<?php echo $Direccionip;?>/idasysv3/images/pueblo_victorioso.png" style="height:40px"/>
            </td>
            <td style="width:210px">
            	<img src="http://<?php echo $Direccionip;?>/idasysv3/images/convenio_argentina_venezuela.png" style="height:80px"/>
            </td>
            <td style="width:210px">
            	<img src="http://<?php echo $Direccionip;?>/idasysv3/images/logo_invap.png" style="height:40px"/>
            </td>
        </tr>
    </table>
</page_header>


<div class=WordSection1>

<p class=MsoNormal style="margin-top:6.0pt;margin-right:0cm;margin-bottom:6.0pt;margin-left:0cm;text-align:center">
    <b>
        <u>
            <span lang=ES-TRAD style="font-size:11.0pt;;letter-spacing:1.0pt;text-align:center">
            	ACTA INDIVIDUAL DE RECEPCI�N DE BIENES: 
            </span>
        </u>
    </b>
</p>

<p class=MsoBodyText style="text-align:justify;line-height:115%">
    <span lang=ES-TRAD style="font-size:11.0pt;line-height:115%;">
        Yo, ___________________, venezolano, mayor de edad, titular de la C�dula de Identidad N� __________, 
        actuando en mi car�cter de _________________________, designado por la Direcci�n Estatal de Salud del 
        Estado ___________________, para recibir bienes destinados al <b><u><?php echo odbc_result($result,'AISDES'); ?></u></b>, 
        adscrito al Ministerio del Poder Popular Para la Salud, ubicado en <b><u><?php echo trim(odbc_result($result,'AISOBS')); ?></u></b>, 
        por el presente documento declaro: Recibo en este acto los bienes identificados en 
        la <b>Nota de Entrega <u>No. <?php echo add_ceros( odbc_result($result,'ADSNRO'),6); ?>/2015</u></b> y en las cantidades all� indicadas, 
        los cuales fueron debidamente verificados y probados, encontr�ndose que son nuevos y est�n en perfectas 
        condiciones de operatividad y funcionamiento, asimismo se deja constancia de la inducci�n y 
        entrenamiento dado al personal seg�n listado anexo.
    </span>
<br /><br />
    <span lang=ES-TRAD style="font-size:11.0pt;line-height:115%;">
        A partir de la presente fecha asumo en nombre y representaci�n del Ministerio del
        Poder Popular Para la Salud la guarda y custodia de los bienes entregados, as�
        como las obligaciones de cuido y mantenimiento de los mismos.
    </span>
<br /><br />
    <span lang=ES-TRAD style="font-size:11.0pt;line-height:115%;">
        Los bienes aqu� entregados fueron adquiridos por el Convenio Integral de
        Cooperaci�n entre la
          Rep�blica Bolivariana de Venezuela y la
         Rep�blica Argentina, seg�n contrato<b> No. 122/2014</b> suscrito en fecha <b>02/12/2014</b>,
        para el Suministro de Equipos para el Programa de Oncolog�a.
    </span>
<br />
    <span lang=ES-TRAD style="font-size:11.0pt;line-height:115%;">
    	OBSERVACIONES:______________________________________________________________________
        ______________________________________________________________________________________
        ______________________________________________________________________________________
    </span>
<br />
	<span lang=ES-TRAD style="font-size:11.0pt;line-height:115%;">
		En testimonio de lo expuesto, la entrega de los bienes se verifica con la firma del presente
        documento, en se�al de aceptaci�n y conformidad. Se hacen Cinco (5) ejemplares
        de un mismo tenor y a un s�lo efecto, en ___________, Estado
        _______________, a los ___ d�as del mes de _________ de _______.
    </span>
</p>
<table  style="width:100%; text-align:center">
	<tr>
    	<td style="width:50%">
        	<p class=MsoBodyText2 align=center style="text-align:center;line-height:115%"><b><span
            lang=ES-TRAD style="">Por: Hospital o Instituto</span></b></p>
            
            <p class=MsoBodyText2 align=center style="text-align:center;line-height:115%"><span
            lang=ES-TRAD style="">Nombre:
            _______________________</span></p>
            
            <p class=MsoBodyText2 align=center style="text-align:center;line-height:115%"><span
            lang=ES-TRAD style="">C.I. No.:
            ________________________</span></p>
            
            <p class=MsoBodyText2 align=center style="text-align:center;line-height:115%"><span
            lang=ES-TRAD style="">Firma y Sello:
            ___________________</span></p>
            
            <p class=MsoNormal align=center style="text-align:center;line-height:115%"><span
            lang=ES-TRAD style="">Cargo:_________________________</span></p>
        </td>
        <td style="width:50%">
        	<p class=MsoBodyText2 align=center style="text-align:center;line-height:115%"><b><span
            lang=ES-TRAD style="">Por: el MPPS</span></b></p>
            
            <p class=MsoBodyText2 align=center style="text-align:center;line-height:115%"><span
            lang=ES-TRAD style="">Nombre:
            _______________________</span></p>
            
            <p class=MsoBodyText2 align=center style="text-align:center;line-height:115%"><span
            lang=ES-TRAD style="">C.I. No.:
            ________________________</span></p>
            
            <p class=MsoBodyText2 align=center style="text-align:center;line-height:115%"><span
            lang=ES-TRAD style="">Firma y Sello:
            ___________________</span></p>
            
            <p class=MsoNormal align=center style="text-align:center;line-height:115%"><span
            lang=ES-TRAD style="">Cargo:_________________________</span></p>
        </td>
    </tr>
    <tr>
    	<td colspan="2">
            <p class=MsoBodyText2 align=center style="text-align:center;line-height:115%"><b><span
            lang=ES-TRAD style="">Por: Meditron, C.A.</span></b></p>
            
            <p class=MsoBodyText2 align=center style="text-align:center;line-height:115%"><span
            lang=ES-TRAD style="">Nombre:
            _______________________</span></p>
            
            <p class=MsoBodyText2 align=center style="text-align:center;line-height:115%"><span
            lang=ES-TRAD style="">C.I. No.:
            ________________________</span></p>
            
            <p class=MsoBodyText2 align=center style="text-align:center;line-height:115%"><span
            lang=ES-TRAD style="">Firma y Sello:
            ___________________</span></p>
            
            <p class=MsoNormal align=center style="text-align:center;line-height:115%"><span
            lang=ES-TRAD style="">Cargo:_________________________</span></p>
        </td>
    </tr>
</table>

</div>

</page>