<?php 

 $atscod = trim($_REQUEST["atscod"]);
 $adsnro = trim($_REQUEST["adsnro"]);
 $acicod = trim($_REQUEST["acicod"]);
 $aalcod = trim($_REQUEST["aalcod"]);
 
 

 /*
 $sql="SELECT T2.ADSNRO, T3.AISDES, T3.AISOBS, T4.AARDES 
		FROM IV36FP T1 
			INNER JOIN IV35FP T2 ON (T1.ACICOD=T2.ACICOD AND T1.AALCOD=T2.AALCOD AND T1.ADPCOD=T2.ADPCOD AND T1.ATSCOD=T2.ATSCOD AND T1.ADSNRO=T2.ADSNRO) 
			INNER JOIN IV42FP T3 ON (T2.ACICOD=T3.ACICOD AND T2.AISCOD=T3.AISCOD) 
			INNER JOIN IV05FP T4 ON (T1.ACICOD=T4.ACICOD AND T1.AARCOD =T4.AARCOD) 
		WHERE T1.ACICOD='".$Compania."' AND T1.ADSNRO =".$adsnro."  AND T1.ATSCOD='".$atscod."' AND T1.AALCOD='".$aalcod."' ";
*/
$sql="SELECT T2.ADSNRO, T3.AISDES, T3.AISOBS, T4.AARDES, T6.AMCDES,
			(SELECT T6.AAPVLA FROM IV17FP T6 WHERE T6.ACICOD=T1.ACICOD AND T6.AARCOD=T1.AARCOD AND T6.APACOD IN ('0112') ) AS SERIAL,
			(SELECT T6.AAPVLA FROM IV17FP T6 WHERE T6.ACICOD=T1.ACICOD AND T6.AARCOD=T1.AARCOD AND T6.APACOD IN ('0116') ) AS MODELO,
			(SELECT T9.AAPVLA FROM IV46FP T9 WHERE T9.ACICOD=T1.ACICOD and T9.ADSNRO=T1.ADSNRO AND T9.APACOD IN ('1509') ) AS ITEM,
			(SELECT T9.AAPVLA FROM IV46FP T9 WHERE T9.ACICOD=T1.ACICOD and T9.ADSNRO=T1.ADSNRO AND T9.APACOD IN ('1510') ) AS NRITEM 
		FROM IV36FP T1 
			INNER JOIN IV35FP T2 ON (T1.ACICOD=T2.ACICOD AND T1.AALCOD=T2.AALCOD AND T1.ADPCOD=T2.ADPCOD AND T1.ATSCOD=T2.ATSCOD AND T1.ADSNRO=T2.ADSNRO) 
			INNER JOIN IV42FP T3 ON (T2.ACICOD=T3.ACICOD AND T2.AISCOD=T3.AISCOD) 
			INNER JOIN IV05FP T4 ON (T1.ACICOD=T4.ACICOD AND T1.AARCOD=T4.AARCOD) 
			INNER JOIN IV06FP T5 ON (T1.ACICOD=T5.ACICOD AND T1.AARCOD=T5.AARCOD) 
			INNER JOIN IV04FP T6 ON (T1.ACICOD=T6.ACICOD AND T5.AMCCOD=T6.AMCCOD)
		WHERE T1.ACICOD='".$Compania."' AND T1.ADSNRO =".$adsnro."  AND T1.ATSCOD='".$atscod."' AND T1.AALCOD='".$aalcod."'  ";

	 $result=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111"));
while(odbc_fetch_row($result))
{
?>

<page backtop="30mm" > 

<page_header>
	<table border="0" style="text-align:center;" align="center">
    	<tr>
        	<td >
            	<img src="http://<?php echo $Direccionip;?>/idasysv3/images/logo_mpps.png" style="width:200px;height:70px"/><br />
                <img src="http://<?php echo $Direccionip;?>/idasysv3/images/pueblo_victorioso.png" style="height:40px"/>
            </td>
            <td style="width:210px">
            	<img src="http://<?php echo $Direccionip;?>/idasysv3/images/convenio_argentina_venezuela.png" style="height:80px"/>
            </td>
            <td style="width:210px">
            	<img src="http://<?php echo $Direccionip;?>/idasysv3/images/logo_invap.png" style="height:40px"/>
            </td>
        </tr>
    </table>
</page_header>


<div class=WordSection1>

<p class=MsoNormal style="margin-top:6.0pt;margin-right:0cm;margin-bottom:6.0pt;margin-left:0cm;text-align:center">
    <b>
        <u>
            <span lang=ES-TRAD style="font-size:11.0pt;;letter-spacing:1.0pt;text-align:center">
            	ACTA DE INSTALACI�N DE BIENES
            </span>
        </u>
    </b>
</p>

<p class=MsoBodyText style="text-align:justify;line-height:115%">
    <span lang=ES-TRAD style="font-size:11.0pt;line-height:115%;">
    	Hoy, ___ del mes de __________________del a�o _______, en el: <b><u><?php echo odbc_result($result,'AISDES'); ?></u></b> 
        adscrito al Ministerio del Poder Popular para la Salud, ubicado en <b><u><?php echo odbc_result($result,'AISOBS'); ?></u></b>, 
        se procedi� a la Instalaci�n del Equipo: <b><u><?php echo odbc_result($result,'AARDES')." / Marca: ".odbc_result($result,'AMCDES')." / Modelo: ".odbc_result($result,'MODELO')." / Serial: ".odbc_result($result,'SERIAL'); ?></u></b> Seg�n 
        NOTA DE ENTREGA	<b><u>No. <?php echo add_ceros( odbc_result($result,'ADSNRO'),6); ?>/2015</u></b> en presencia de:
    </span>
<br /><br />
    <span lang=ES-TRAD style="font-size:11.0pt;line-height:115%;">
       <table style="width:100%;" border="1" cellspacing="0">
       		<tr style="width:100%;">
            	<th style="width:25%;text-align:center;vertical-align:middle;">Apellido y <br />Nombre</th>
                <th style="width:25%;text-align:center;vertical-align:middle;">C.I. No.</th>
                <th style="width:25%;text-align:center;vertical-align:middle;">Cargo</th>
                <th style="width:25%;text-align:center;vertical-align:middle;">Firma</th>
            </tr>
			<tr>
            	<td style="height:4mm"> </td>
                <td> </td>
                <td> </td>
                <td> </td>
            </tr>
            <tr>
            	<td style="height:4mm"> </td>
                <td> </td>
                <td> </td>
                <td> </td>
            </tr>
            <tr>
            	<td style="height:4mm"> </td>
                <td> </td>
                <td> </td>
                <td> </td>
            </tr>
            
       </table>
    </span>
<br /><br />
    <span lang=ES-TRAD style="font-size:11.0pt;line-height:115%;">
        El equipo instalado fue adquirido mediante el convenio Integral de Cooperaci�n Internacional suscrito 
        entre la Rep�blica Bolivariana de Venezuela y la Rep�blica de Argentina seg�n Contrato No. <b>122/2014</b> 
        suscrito en fecha <b>02/12/2014</b> para <b>"Prestaci�n del Servicio de Mantenimiento Preventivo y Correctivo, 
        Actualizaci�n Tecnol�gica, Suministros Especiales y Capacitaci�n del Personal en los Servicios de 
        Radioterapia Con o Sin Medicina Nuclear del Sistema P�blico Nacional de Salud"</b>
    </span>
<br /><br />
	<span lang=ES-TRAD style="font-size:11.0pt;line-height:115%;">
		En testimonio de lo expuesto, se deja constancia que el Equipo se encuentra en perfecto estado de 
        funcionamiento y operatividad, se verifica con la firma del presente documento, en se�al de aceptaci�n y 
        conformidad. Se hacen Cinco (5) ejemplares de un mismo tenor y a un s�lo efecto.
    </span>
</p>
<table  style="width:100%; text-align:center">
	<tr>
    	<td style="width:50%">
        	<p class=MsoBodyText2 align=center style="text-align:center;line-height:115%"><b><span
            lang=ES-TRAD style="">Por: Hospital o Instituto</span></b></p>
            
            <p class=MsoBodyText2 align=center style="text-align:center;line-height:115%"><span
            lang=ES-TRAD style="">Nombre:
            _______________________</span></p>
            
            <p class=MsoBodyText2 align=center style="text-align:center;line-height:115%"><span
            lang=ES-TRAD style="">C.I. No.:
            ________________________</span></p>
            
            <p class=MsoBodyText2 align=center style="text-align:center;line-height:115%"><span
            lang=ES-TRAD style="">Firma y Sello:
            ___________________</span></p>
            
            <p class=MsoNormal align=center style="text-align:center;line-height:115%"><span
            lang=ES-TRAD style="">Cargo:_________________________</span></p>
        </td>
        <td style="width:50%">
        	<p class=MsoBodyText2 align=center style="text-align:center;line-height:115%"><b><span
            lang=ES-TRAD style="">Por: Hospital o Instituto</span></b></p>
            
            <p class=MsoBodyText2 align=center style="text-align:center;line-height:115%"><span
            lang=ES-TRAD style="">Nombre:
            _______________________</span></p>
            
            <p class=MsoBodyText2 align=center style="text-align:center;line-height:115%"><span
            lang=ES-TRAD style="">C.I. No.:
            ________________________</span></p>
            
            <p class=MsoBodyText2 align=center style="text-align:center;line-height:115%"><span
            lang=ES-TRAD style="">Firma y Sello:
            ___________________</span></p>
            
            <p class=MsoNormal align=center style="text-align:center;line-height:115%"><span
            lang=ES-TRAD style="">Cargo:_________________________</span></p>
        </td>
    </tr>
    <tr>
    	<td colspan="2">
            <p class=MsoBodyText2 align=center style="text-align:center;line-height:115%"><b><span
            lang=ES-TRAD style="">Firmado por la Empresa</span></b></p>
            
            <p class=MsoBodyText2 align=center style="text-align:center;line-height:115%"><span
            lang=ES-TRAD style="">Nombre:
            _______________________</span></p>
            
            <p class=MsoBodyText2 align=center style="text-align:center;line-height:115%"><span
            lang=ES-TRAD style="">C.I. No.:
            ________________________</span></p>
            
            <p class=MsoBodyText2 align=center style="text-align:center;line-height:115%"><span
            lang=ES-TRAD style="">Firma y Sello:
            ___________________</span></p>
            
            <p class=MsoNormal align=center style="text-align:center;line-height:115%"><span
            lang=ES-TRAD style="">Cargo:_________________________</span></p>
        </td>
    </tr>
</table>

</div>

</page>

<?php 
}
?>