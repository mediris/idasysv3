//Requerimientos
function agregar() {
	document.agregarform.atrobs.value="";
	document.agregarform.atrdes.value="";
	document.agregarform.atscod.value="";
	document.agregarform.aiscod.value="";
}

function agregarvalidar(){	
	var contenido= "";
	var elemento;
	var nodetalle= "";
	//alert(document.agregarform.aiscod.options[document.agregarform.aiscod.selectedIndex].text );
	document.body.style.cursor="wait";

	var indice1 = document.agregarform.atscod.selectedIndex;
	var val1 = document.agregarform.atscod.options[indice1].value;
	var val2 = document.agregarform.aalcod.value;
	capturararticulo(val1, val2);
	
	
	$.ajax({
			async: false,
			type: "POST",
			dataType: "JSON",
			url: "dotacionesagregarvalidarphp.php",
			data: $("#agregarform").serialize(),
			success: function(datos){
				vdata = parseJSONx(datos);}							
	});			
	sierror='N';
	for(i in vdata)	{ 
		if(vdata[i].campo){
			document.getElementById(vdata[i].campo).innerHTML=vdata[i].msg;
			sierror='S';		
		}
		if(vdata[i].ATRNUM){
			document.agregarform.atrnum.value=vdata[i].ATRNUM;
			document.solicitudes.atrnum.value=vdata[i].ATRNUM;
		}
	}
	
	if(sierror=='N')
	{
		document.body.style.cursor="default";
		if(nodetalle=="S"){
			finalizarreq();
		}else{
			$("#agregarc").css("display", "none");
			$("#reqst06fp").css("display", "block");
			document.getElementById("ord1").innerHTML=document.agregarform.atrdes.value;
			document.getElementById("ord3").innerHTML=document.agregarform.atrobs.value;
			if(document.agregarform.aiscod.value!=''){
				document.getElementById("ord4").innerHTML=document.agregarform.aiscod.options[document.agregarform.aiscod.selectedIndex].text;
			}else{
				document.getElementById("ord4").innerHTML="-";
			}
			document.getElementById("ord5").innerHTML=document.agregarform.atscod.options[document.agregarform.atscod.selectedIndex].text;
			document.solicitudes.atscod.value=document.agregarform.atscod.value;
			//document.getElementById("ord6").innerHTML=document.agregarform.aalcod.value;
			document.solicitudes.aalcod.value=document.agregarform.aalcod.value;
			document.getElementById("aarcod").focus();
			$("#ocultarform").css("display", "none");
			
		}//fin del else
	}
}//fin del primer if

function choiceart(targ,selObj,restore,e){ //v3.0

	dat = selObj.options[selObj.selectedIndex].value;
	var datos = dat.split("@");
	/*document.getElementById("aarumb").value=datos[1];*/
	combo_uni(datos[0], datos[1]);
	activaimagengrabar(1);
}


function finalizarreq(){
			$("#agregaraftsav").hide(1000);		
			parent.location.reload();
			parent.Shadowbox.close();
	}	

function enviarpedido(nrord){

	if (confirm('Seguro que desea enviar a aprobaci\u00f3n el pedido ' + nrord + '?'))
	{
			var param = [];
			param['ATRNUM']=nrord;
			ejecutasqld("enviarrequerimiento.php",param);

			var param = [];
			param['ATRNUM']=nrord;
			
			var windowprops = "toolbar=0,location=0,directories=0,status=0, " + "menubar=0,scrollbars=0,resizable=0,width=600,height=300";

			OpenWindow = window.open("correo.php?&ATRNUM="+nrord, "enviodecorreo", windowprops);
			
			//ejecutasqla("correo.php",param);
			
			$("#agregaraftsav").hide(1000);		
			parent.location.reload();
			parent.Shadowbox.close();
	}	
}

function activaimagengrabar(num){ 
	document.getElementById("aslcan").focus();
	if ((document.getElementById("aslcan").value !==0) && (document.solicitudes.aarcod.selectedIndex!=='') && (validarCantidad()==1)) {
		document.getElementById("imagengrabar").style.display='block';
	}
	if ((document.getElementById("aslcan").value == 0) || (document.solicitudes.aarcod.selectedIndex=='') || (validarCantidad()==2)) {
		if( (validarCantidad()==2) && (document.solicitudes.aarcod.selectedIndex!='') && (document.getElementById("aslcan").value != 0) ){
			alert("La cantidad registrada para la dotaci\u00f3n es Mayor \n a la cantida Existente en el Almac\u00e9n.\n");
		}
		document.getElementById("imagengrabar").style.display='none'
	}
}

function vertransaccion(){
	var code = $("#atocod").val(); 
	//alert(code);
	var param = [];
			param['atocod']=code;
			ejecutasqlp("buscarinventariotransaccion.php",param);
			for(i in gdata){
			document.agregarform.atrcod.value=gdata[i].ATRCOD;
			//var str=document.requerimagregarform.atrcod.value;
			//alert(str.length);
			};
	}
	
function cargarinfopa(cod, nrord , alma) {
			var param = [];
			param['atrnum']=nrord;
			param['atrcod']=cod;
			param['aalcod']=alma;
			ejecutasqlp("informacionphp.php",param);
			for(i in gdata)
			{
				document.getElementById("nroreq").innerHTML=gdata[i].ADSNRO;
				document.agregarform.atrnum.value=gdata[i].ADSNRO;
				
				
				document.getElementById("watscod").innerHTML=gdata[i].ATSDES;
				document.agregarform.atscod.value=gdata[i].ATSCOD;

				document.agregarform.aalcod.value=gdata[i].AALCOD;
				document.getElementById("waalcod").innerHTML=gdata[i].AALDES;

				document.getElementById("waiscod").innerHTML=gdata[i].AISDES;
				document.agregarform.aiscod.value=gdata[i].AISCOD;

				document.getElementById("watrdes").innerHTML=gdata[i].ATRDES;
				document.agregarform.atrdes.value=gdata[i].ATRDES;

				document.getElementById("watrobs").innerHTML=gdata[i].ATROBS;
				document.agregarform.atrobs.value=gdata[i].ATROBS;
			};
}

function cargarinfopaacta(cod, nrord , alma) {
			var param = [];
			param['atrnum']=nrord;
			param['atrcod']=cod;
			param['aalcod']=alma;
			ejecutasqlp("../informacionphp.php",param);
			for(i in gdata)
			{
				document.getElementById("nroreq").innerHTML=gdata[i].ADSNRO;
				document.agregarform.atrnum.value=gdata[i].ADSNRO;
				
				
				document.getElementById("watscod").innerHTML=gdata[i].ATSDES;
				document.agregarform.atscod.value=gdata[i].ATSCOD;

				document.agregarform.aalcod.value=gdata[i].AALCOD;
				document.getElementById("waalcod").innerHTML=gdata[i].AALDES;

				document.getElementById("waiscod").innerHTML=gdata[i].AISDES;
				document.agregarform.aiscod.value=gdata[i].AISCOD;

				document.getElementById("watrdes").innerHTML=gdata[i].ATRDES;
				document.agregarform.atrdes.value=gdata[i].ATRDES;

				document.getElementById("watrobs").innerHTML=gdata[i].ATROBS;
				document.agregarform.atrobs.value=gdata[i].ATROBS;
			};
}

function editar(cod, nrord , alma) {
			var param = [];
			param['atrnum']=nrord;
			param['atrcod']=cod;
			param['aalcod']=alma;
			ejecutasqlp("informacionphp.php",param);
			for(i in gdata)
			{
				document.agregarform.atrobs.value=gdata[i].ATROBS;
				document.getElementById("nroreq").innerHTML=gdata[i].ADSNRO;
				document.agregarform.atrnum.value=gdata[i].ADSNRO;
				document.agregarform.atrdes.value=gdata[i].ATRDES;
				document.agregarform.aiscod.value=gdata[i].AISCOD;
				
				document.getElementById("watscod").innerHTML=gdata[i].ATSDES;
				document.agregarform.atscod.value=gdata[i].ATSCOD;
				document.getElementById("waalcod").innerHTML=gdata[i].AALDES;
				document.agregarform.aalcod.value=gdata[i].AALCOD;
				
				if (gdata[i].AARCOD!='') 
				{
					//alert(gdata[i].ATRART);
					var value4 = gdata[i].AARCOD+'@'+gdata[i].ATRUMB;
					var texto4 = gdata[i].AARDES;
					var cantidad = gdata[i].ATRCAN;
					//var texto5 = gdata[i].ATRUMB;
					var texto5 = gdata[i].AUMDES;
					var valueatr = gdata[i].ATSCOD;
					
					/*FORMATEO DE CANTIDAD*/
					cantidad = cantidad!=''?NumFormat(cantidad, '2', '10', '.', ''):'';
					
					cadena = "<tr>";
					//cadena = cadena + "<td>" + $("#adpcod2").val() + "</td>";
					cadena = cadena + "<td>" + texto4 + " (" + gdata[i].AARCOD + ")</td>";
					cadena = cadena + "<td ><div align='right'>" + cantidad + "</div></td>";
					cadena = cadena + "<td>" + texto5 + "</td>";
					//cadena = cadena + "<td>" + $("#atrfac").val() + "</td>";
					//cadena = cadena + "<td>" + $("#aslcos").val() + "</td>";
					
					cadena = cadena + "<td><a class='elimina'  id='"+value4+"' name='"+value4+"'  href='javascript:fn_dar_eliminar(\"" + value4 + "\" , "+gdata[i].ADSNRO+", \""+valueatr+"\");'><img src='../images/eliminar.gif' alt='Eliminar' width='15' height='15' border='0'></a></td>";
					cadena =  cadena + "</tr>";
					$("#grilla").append(cadena);
				};
			};
}

function editarcerrar() {
	document.getElementById('agregardiv').style.display='none';
	document.getElementById('fade').style.display='none';
}

function editarvalidarpa(){	
	var contenido= "";
	var elemento;
	var nodetalle= "";
	
	document.body.style.cursor="wait";

	$.ajax({
	   	async: false,
        type: "POST",
		dataType: "JSON",
        url: "dotacioneseditarvalidarparamadiphp.php",
        data: $("#agregarform").serialize(),
        success: function(datos){//alert(datos);
			vdata = parseJSONx(datos);}							
	});			
		sierror='N';
		for(i in vdata)	{
			if(vdata[i].campo) 	{
				document.getElementById(vdata[i].campo).innerHTML=vdata[i].msg;
				sierror='S';		
			}
		}
	
	if(sierror=='N'){
	document.body.style.cursor="default";
		if(nodetalle=="S"){
			finalizarreq();
		}else{		
    		
			$("#ocultarform").css("display", "none");
			$("#agregaraftsav").css("display", "block");
		}
}//fin del primer if
}

function editarvalidarpaacta(){	
	var contenido= "";
	var elemento;
	var nodetalle= "";
	
	document.body.style.cursor="wait";

	$.ajax({
	   	async: false,
        type: "POST",
		dataType: "JSON",
        url: "dotacioneseditarvalidarparamadiphp.php",
        data: $("#agregarform").serialize(),
        success: function(datos){//alert(datos);
			vdata = parseJSONx(datos);}							
	});			
		sierror='N';
		for(i in vdata)	{
			if(vdata[i].campo) 	{
				document.getElementById(vdata[i].campo).innerHTML=vdata[i].msg;
				sierror='S';		
			}
		}
	
	if(sierror=='N'){
	document.body.style.cursor="default";
		if(nodetalle=="S"){
			finalizarreq();
		}else{		
    		
			$("#ocultarform").css("display", "none");
			$("#agregaraftsav").css("display", "block");
		}
}//fin del primer if
}

function editarvalidar(){	
	var contenido= "";
	var elemento;
	var nodetalle= "";
	
	document.body.style.cursor="wait";

	$.ajax({
	   	async: false,
        type: "POST",
		dataType: "JSON",
        url: "dotacioneseditarvalidarphp.php",
        data: $("#agregarform").serialize(),
        success: function(datos){//alert(datos);
			vdata = parseJSONx(datos);}							
	});	
			
	sierror='N';
	for(i in vdata)	{
		if(vdata[i].campo) 	{
			document.getElementById(vdata[i].campo).innerHTML=vdata[i].msg;
			sierror='S';		
		}
	}
	
	if(sierror=='N'){
	document.body.style.cursor="default";
		if(nodetalle=="S"){
			finalizarreq();}
		else{
			document.getElementById("ord1").innerHTML=document.agregarform.atrdes.value;
			document.getElementById("ord3").innerHTML=document.agregarform.atrobs.value;
			if(document.agregarform.aiscod.value!=''){
				document.getElementById("ord4").innerHTML=document.agregarform.aiscod.options[document.agregarform.aiscod.selectedIndex].text;
			}else{
				document.getElementById("ord4").innerHTML="-";
			}
			document.getElementById("ord5").innerHTML=document.getElementById("watscod").innerHTML;
			document.solicitudes.atscod.value=document.agregarform.atscod.value;
			//document.getElementById("ord6").innerHTML=document.agregarform.aalcod.value;
			document.getElementById("aarcod").focus();

			capturararticulo(document.agregarform.atscod.value, $("#aalcod").val());
			
			$("#agregarc").css("display", "none");
			$("#reqst06fp").css("display", "block");
			document.getElementById("ord3").innerHTML=document.agregarform.atrobs.value;
			document.solicitudes.atrnum.value=document.agregarform.atrnum.value;
			document.getElementById("aarcod").focus();
			$("#ocultarform").css("display", "none");}//fin del else
	}
}//fin del primer if

function eliminar(nrord, alma, atrcod) {
	
	if (confirm('Seguro que desea borrar la dotaci\u00f3n ' + nrord + '?'))
	{
		var param = [];
		param['atrnum']=nrord;
		param['aalcod']=alma;
		param['atrcod']=atrcod;
		ejecutasqld("procesareliminar.php",param);
		location.reload();
	}
}

//Despachar , rechazar orden de servicio pendiente
function procesardespacho(nrord) {
if (confirm('Una vez despachado el requerimiento ' + nrord + ' no se podr\u00e1 hacer modificaciones. Desea continuar?'))
{
var param = [];
			guardardespacho(nrord);
			param['ATRNUM']=nrord;
			ejecutasqld("despacharrequerimiento.php",param);
			
			//var windowprops = "toolbar=0,location=0,directories=0,status=0, " + "menubar=0,scrollbars=0,resizable=0,width=400,height=200";
//OpenWindow = window.open("correoaprobado.php?&ATRNUM="+nrord, "enviodecorreo", windowprops);
			window.opener.document.location.reload()
			window.close();
}
}
function recibirdespacho(nrord) {
if (confirm('Desea recibir el requerimiento ' + nrord + ' ?'))
{
var param = [];
			$.ajax({
	   	async: false,
        type: "POST",
		dataType: "JSON",
        url: "recibirrequerimiento.php",
        data: $("#despachoform").serialize(),
        success: function(datos){//alert(datos);
			vdata = parseJSONx(datos);}							
});						

			//var windowprops = "toolbar=0,location=0,directories=0,status=0, " + "menubar=0,scrollbars=0,resizable=0,width=400,height=200";
			//OpenWindow = window.open("correoaprobado.php?&ATRNUM="+nrord, "enviodecorreo", windowprops);
			window.opener.document.location.reload()
			window.close();
}
}


function guardardespacho(nrord) {
var param = [];
			$.ajax({
	   	async: false,
        type: "POST",
		dataType: "JSON",
        url: "guardarrequerimiento.php",
        data: $("#despachoform").serialize(),
        success: function(datos){//alert(datos);
			vdata = parseJSONx(datos);}							
});						
			//var windowprops = "toolbar=0,location=0,directories=0,status=0, " + "menubar=0,scrollbars=0,resizable=0,width=400,height=200";
//OpenWindow = window.open("correoaprobado.php?&ATRNUM="+nrord, "enviodecorreo", windowprops);

location.reload();
}


//Requerimiento grabar en is05fp

			function fn_cantidad(){
				cantidad = $("#grilla tbody").find("tr").length;
				$("#span_cantidad").html(cantidad);
			};
            
            function grabardetallereq(){
				var indice4 = document.solicitudes.aarcod.selectedIndex;
    			var texto4 = document.solicitudes.aarcod.options[indice4].text;
				var value4 = document.solicitudes.aarcod.options[indice4].value;
    			//var texto5 = document.solicitudes.aarumb.value;
				var indice5 = document.solicitudes.aarumb.selectedIndex;
    			var texto5 = document.solicitudes.aarumb.options[indice5].text;
				var value5 = document.solicitudes.aarumb.options[indice5].value;
				dat = value4;
				var datos = dat.split("@");
				
				
				
				var valueatr = document.solicitudes.atscod.value; 
				
				/*CORTANDO NOMBRE*/
				var pos =texto4.indexOf(".");
				if(pos!="-1") { var nombre = texto4.substring(0,pos); }
				else		  { var nombre = texto4 				  }
				
				/*FORMATEO DE CANTIDAD*/
               	var cantidad = cantidad!=''?NumFormat($("#aslcan").val(), '2', '10', '.', ''):'';
											
				
			    cadena = "<tr>";
                //cadena = cadena + "<td>" + $("#adpcod2").val() + "</td>";
				cadena = cadena + "<td>" + nombre + " (" + datos[0] + ")</td>";
                cadena = cadena + "<td> <div align='right'>" + cantidad + "</div></td>";
                cadena = cadena + "<td>" + texto5 + "</td>";
				//cadena = cadena + "<td>" + $("#atrfac").val() + "</td>";
				//cadena = cadena + "<td>" + $("#aslcos").val() + "</td>";
                cadena = cadena + "<td><a class='elimina'  id='"+value4+"' name='"+value4+"'  href='javascript:fn_dar_eliminar(\"" + value4 + "\",this,\""+valueatr+"\");'><img src='../images/eliminar.gif' alt='Eliminar' width='15' height='15' border='0'></a></td>";
				cadena =  cadena + "</tr>";
				
				$.ajax({
					async: false,
					type: "POST",
					dataType: "JSON",
					url: "guardardetalles.php",
					data: $("#solicitudes").serialize(),
					success: function(datos){//alert(datos);
						vdata = parseJSONx(datos);}							
				});			
                sierror='N';
					for(i in vdata)	{
						if(vdata[i].campo) 	{
							//document.getElementById(vdata[i].campo).innerHTML=vdata[i].msg;
							alert(vdata[i].msg);
							sierror='S';
						}
					}
												
				if (sierror=='S')	{
					alert("Producto ya se encuentra en la dotaci\u00f3n.")}								
				else				{
					$("#grilla").append(cadena);
					//alert("Articulo agregado");
									}
									
				document.getElementById("aarcod").value="";
				document.getElementById("aslcan").value="";
				document.getElementById("aarumb").value="";
				combo_uni(document.getElementById("aarcod").value, document.getElementById("aarumb").value);
				capturararticulo(document.agregarform.atscod.value, document.solicitudes.aalcod.value);
				activaimagengrabar();
				document.getElementById("aarcod").focus();
            };
            
			
            function fn_dar_eliminar(cod, hcd, atrcod){
				//alert(cod);
                $("a.elimina").click(function(){
					atscod = $("#atscod").val();
					hcd = document.solicitudes.atrnum.value;
                    id = $(this).parents("tr").find("td").eq(0).html();
                    respuesta = confirm("Desea eliminar el Art\u00edculo: " + id);
                    if (respuesta){//alert($(this));
                        //$(this).parents("tr").fadeOut("normal", function(){		
                            $(this).parents("#grilla tr").remove();
							var param = [];
							param['atrnum']=hcd;
							param['aarcod']=this.id;
							param['aalcod']=document.getElementById('aalcod').value;
							param['atrcod']=atrcod;
							$.ajax({
								async: false,
								type: "POST",
								dataType: "JSON",
								url: "eliminardetalle.php",
								data: convertToJson(param),
								success: function(datos){//alert(datos);
									//alert("Art\u00edculo " + id + " eliminado");
									capturararticulo(atscod,document.getElementById('aalcod').value);
								}							
						//	});	
								
                        })
                    }
                });
            };

function requerimverdetalle(num, alm, atrcod) {
	self.name = "main"; // names current window as "main"
	
	var windowprops ="toolbar=0,location=0,directories=0,status=0,"+"menubar=0,scrollbars=1,resizable=0,width=990,height=800";
	
	var ventana="dotacionverdetalle.php?&num="+num+"&alm="+alm+"&atrcod="+atrcod;
	
	window.open(ventana,"", windowprops); // opens remote control

}

function requerimdespachar(num) {
self.name = "main"; // names current window as "main"

var windowprops ="toolbar=0,location=0,directories=0,status=0,"+"menubar=0,scrollbars=1,resizable=0,width=990,height=800";

var ventana="requerimdespachardetalle.php?&num="+num;

window.open(ventana,"", windowprops); // opens remote control

}
function requerimrecibir(num, sts) {
self.name = "main"; // names current window as "main"

var windowprops ="toolbar=0,location=0,directories=0,status=0,"+"menubar=0,scrollbars=1,resizable=0,width=990,height=800";
if (sts=='05')	{var ventana="requerimrecibirdetalle.php?&num="+num;}
else			{var ventana="requerimrecibirdetalle2.php?&num="+num;}


window.open(ventana,"", windowprops); // opens remote control

}

function esenter(e) {
	var tecla;
	if (window.event) {tecla = window.event.keyCode;}
	else if (e) tecla = e.which;
	else return false;
	if (tecla == 13)   	{
	if ((document.getElementById("aslcan").value !=0) && (document.solicitudes.aarcod.selectedIndex!='')) {
	grabardetallereq(); 	}}
	else   				{return false;  }

}
function esenter1(e) {
	var tecla;
	if (window.event) {tecla = window.event.keyCode;}
	else 			if (e) {tecla = e.which;}
	else 			return false;
	if (tecla == 13)   	{return true; 	}
	else   				{return false;  }

}
function nada() {
}

function validarcantrec(candes, observ,cantrec) {alert("Debe llenar el campo de observaci\u00f3n, colocando motivo");
        if(cantrec.value != candes)	{	document.getElementById(observ).disabled=false;
											document.getElementById(observ).focus();		}								
		else 						{	document.getElementById(observ).disabled=true;	}
}
function validarobserv(observ) {
        if (observ.value=="")  {		observ.style.border = "1px solid #F00";
										alert("Se debe colocar una observaci\u00f3n v\u00e1lida");                               
										observ.focus();}
}
function validarobservon(observ) {
        if (observ.value=="")  {		observ.style.border = "1px solid #F00";
										alert("Se debe colocar una observaci\u00f3n v\u00e1lida");                               
										observ.focus();}
}
/*
 * @jDavila
 *13/04/2012
 */
function combo_uni(art, medi)
{
	var param = [];
	param['medi']=medi;
	param['art']=art;
	$.ajax({
		async: false,
		type: 'POST',
		url: 'cargarunimedida.php',
		beforeSend:function(){
			$('#div_aarnum').html('<span class="input-notification attention png_bg">Cargando...</span>');
		},
		data: convertToJson(param),
		success: function(html){                
			$('#div_aarnum').html(html);
		}
	});
}

function confirmadotacion(alma, desalma, tipo, atrcod)
{
	if(confirm("Desea Confirmar la Dotaci\u00f3n " + tipo + " del Almac\u00e9n "+desalma)){
		var param = [];var vdata;
		param['aalcod']=alma;
		param['atrnum']=tipo;
		param['atrcod']=atrcod;

		$('#loader').css("display", "block");
		$("#backdrop").css("display", "block");

		$.ajax({
			async: false,
			type: 'POST',
			url: 'confirmardotacion.php',
			data: convertToJson(param),
			success: function(datos){

				$("#loader").css("display", "none");
            	$("#backdrop").css("display", "none");

				if(datos == 0){
					alert("La dotaci\u00f3n no tiene art\u00edculos");
				}
				if(datos == 1) {
					alert("La dotaci\u00f3n se confirm\u00f3 exitosamente");
					requerimimprimir(tipo, alma, atrcod);///falta atrcod
					location.reload();
				}

				// vdata = parseJSONx(datos);
				// for(i in vdata)	{
				// 	//alert(vdata[i].FIN);
				// 	if(vdata[i].FIN==0) 	{
				// 		alert("La dotaci\u00f3n no tiene art\u00edculos");
				// 	}
				// 	if(vdata[i].FIN==1) 	{
				// 		alert("La dotaci\u00f3n se confirm\u00f3 exitosamente");
				// 		requerimimprimir(tipo, alma, atrcod);///falta atrcod
				// 		location.reload();
				// 	}
				// }
			}
		});	
	}
}


function requerimimprimir(tipo, alma, atrcod)
{
	var windowprops = "toolbar=0,location=0,directories=0,status=0, " + "menubar=0,scrollbars=0,resizable=0,width=600,height=300";
	OpenWindow = window.open("exportarapdf.php?&atrnum="+tipo+"&aalcod="+alma+"&atrcod="+atrcod, "enviodecorreo", windowprops);
}

function requerimimprimir_ne(tipo, alma, atrcod)
{
	var windowprops = "toolbar=0,location=0,directories=0,status=0, " + "menubar=0,scrollbars=0,resizable=0,width=600,height=300";
	OpenWindow = window.open("exportarapdf_ne.php?&atrnum="+tipo+"&aalcod="+alma+"&atrcod="+atrcod, "enviodecorreo", windowprops);
}

function requerimimprimir_ne_costo(tipo, alma, atrcod)
{
	var windowprops = "toolbar=0,location=0,directories=0,status=0, " + "menubar=0,scrollbars=0,resizable=0,width=600,height=300";
	OpenWindow = window.open("exportarapdf_costos.php?&atrnum="+tipo+"&aalcod="+alma+"&atrcod="+atrcod, "enviodecorreo", windowprops);
}

function aprovacion(alma, desalma, tipo, atrcod)
{
	if(confirm("Desea Solicitar aprobaci\u00f3n de la Dotaci\u00f3n " + tipo + " del Almac\u00e9n "+desalma)){
		var param = [];var vdata;
		param['aalcod']=alma;
		param['atrnum']=tipo;
		param['atrcod']=atrcod;
		ejecutasqlp("enviarrequerimiento.php",param);
		
		for(i in gdata){
			if(gdata[i].EJEC == 1)//bien
			{
				var windowprops = "toolbar=0,location=0,directories=0,status=0, " + "menubar=0,scrollbars=0,resizable=0,width=600,height=300";
				OpenWindow = window.open("correo.php?&atrnum="+tipo+"&aalcod="+alma+"&atrcod="+atrcod, "enviodecorreo", windowprops);

				$("#agregaraftsav").hide(1000);

				parent.location.reload();
				parent.Shadowbox.close();
			}
			else if(gdata[i].EJEC == 0)//mal
			{
				alert("No se puede enviar aprobaci\u00f3n de la dotaci\u00f3n \n Se debe ingresar Detalle.");
			}
		}
		
	}
}

function finalizar(alma, desalma, tipo, atrcod)
{
	if(confirm("Desea Finalizar la Dotaci\u00f3n " + tipo + " del Almac\u00e9n "+desalma)){
		var param = [];var vdata;
		param['aalcod']=alma;
		param['atrnum']=tipo;
		param['atrcod']=atrcod;
		ejecutasqlp("finalizarrequerimiento.php",param);
		
		for(i in gdata){
			if(gdata[i].EJEC == 1)//bien
			{
				alert("La Dotaci\u00f3n " + tipo + " Finalizo de forma exitosa.");
				parent.location.reload();
			}
			else if(gdata[i].EJEC == 0)//mal
			{
				alert("No se pudo finalizar la dotaci\u00f3n.");
			}
		}
		
	}
}

function cambiarStatus(alma, desalma, adsnro, atscod)
{
	if(confirm("Desea Cambiar el Estado de la Dotaci\u00f3n " + adsnro + " del Almac\u00e9n "+desalma)){
		var param = [];var vdata;
		param['aalcod']=alma;
		param['adsnro']=adsnro;
		param['atscod']=atscod;
		ejecutasqlp("actualizarstatusrequerimiento.php",param);
		
		for(i in gdata){
			if(gdata[i].EJEC == 1)//bien
			{
				alert("La Dotaci\u00f3n " + adsnro + " cambio de estado de forma exitosa.");
				parent.location.reload();
			}
			else if(gdata[i].EJEC == 0)//mal
			{
				alert("No se pudo cambiar de estado la Dotaci\u00f3n.");
			}
		}
		
	}
}

function cambiarStatusActa(alma, desalma, adsnro, atscod)
{
	if(confirm("Desea Cambiar el Estado de la Dotaci\u00f3n " + adsnro + " del Almac\u00e9n "+desalma)){
		var param = [];var vdata;
		param['aalcod']=alma;
		param['adsnro']=adsnro;
		param['atscod']=atscod;
		ejecutasqlp("../actualizarstatusrequerimiento.php",param);
		
		for(i in gdata){
			if(gdata[i].EJEC == 1)//bien
			{
				alert("La Dotaci\u00f3n " + adsnro + " cambio de estado de forma exitosa.");
				parent.location.reload();
			}
			else if(gdata[i].EJEC == 0)//mal
			{
				alert("No se pudo cambiar de estado la Dotaci\u00f3n.");
			}
		}
		
	}
}


function cargarparametros(obj,num)
{
	//if(num=='')num=0;
	var param = [];
	var atrnum = num;
	var value = $('#'+obj.id).val();
	if (value==0){
		value = 99999999;
	}
	param['code']=value;
	param['num']=atrnum;
	$.ajax({
		async: false,
		type: 'POST',
		url: 'cargarparametros.php',
		beforeSend:function(){
			$('#parametros').html('<span class="input-notification attention png_bg">Cargando...</span>');
		},
		data: convertToJson(param),
		success: function(html){                
			$('#parametros').html(html);
		}
	});
}

function cargarparametrosacta(obj,num)
{
	//if(num=='')num=0;
	var param = [];
	var atrnum = num;
	var value = $('#'+obj.id).val();
	if (value==0){
		value = 99999999;
	}
	param['code']=value;
	param['num']=atrnum;
	$.ajax({
		async: false,
		type: 'POST',
		url: 'cargarparametros.php',
		beforeSend:function(){
			$('#parametros').html('<span class="input-notification attention png_bg">Cargando...</span>');
		},
		data: convertToJson(param),
		success: function(html){                
			$('#parametros').html(html);
		}
	});
}


//aprobar , rechazar orden de servicio pendiente

function aprobar(abc, num, cia, cod, alm) {
	if (confirm('Seguro que desea aprobar la Dotaci\u00f3n ' + num + '?'))
	{
		var param = [];
		param['abc']=abc;
		param['num']=num;
		param['cia']=cia;
		param['cod']=cod;
		
		ejecutasqlp("aprobardotacion.php",param);
		for(i in gdata)	{
			if(gdata[i].ERROR){
				//alert(gdata[i].ERROR);
				if(gdata[i].ERROR == 1)alert("C\u00f3digo de autorizaci\u00f3n ya utilizado");
				if(gdata[i].ERROR == 2)alert("Dotaci\u00f3n NO está pendiente de aprobaci\u00f3n o ya aprobado");
				if(gdata[i].ERROR == 3)alert("Usuario NO autorizado a aporobar esta dotaci\u00f3n");
				if(gdata[i].ERROR == 4){
					/*
					var windowprops = "toolbar=0,location=0,directories=0,status=0, " + "menubar=0,scrollbars=0,resizable=0,width=600,height=300";
					OpenWindow = window.open("correo.php?&atrnum="+num+"&aalcod="+alm+"&atrcod="+cod, "enviodecorreo", windowprops);
					alert("Dotaci\u00f3n Nro: "+num+" aprobado con \u00e9xito");
					*/
					location.reload();
				}
				
			}
		}
	}
}

function capturararticulo(code, aalcod)
{
	
	var param = [];
	param['code']=code;
	param['aalcod']=aalcod;
	$.ajax({
		async: false,
		type: 'GET',
		url: 'buscararticulo.php',
		beforeSend:function(){
			//$('#aarcod').html('<span class="input-notification attention png_bg">Cargando...</span>');
			//document.body.className = 'waiting';
			//$("#reqst06fp").css("display", "none");
			$("#ocultarform").hide(1000);
			$("#reqst06fp").hide(1000);
			//$("#espera").css("display", "block");
			$("#espera").show();
			
		},
		data: convertToJson(param),
		success: function(resultado){
			if(resultado == false)
			{
				alert("No existe ningun art\u00edculo para tipo de requisici\u00f3n");
				document.getElementById("aarcod").value=0;
				$("#aarcod").attr("disabled",true);
			}
			else
			{
				$("#aarcod").attr("disabled",false);
				document.getElementById("aarcod").options.length=1;
				$('#aarcod').append(resultado);
			}
			//document.body.className = '';
			//$("#espera").css("display", "none");
			$("#espera").hide(10);
			//$("#reqst06fp").css("display", "block");
			$("#reqst06fp").show(10);
		}
	});
}


function rechazar(abc, num, cia, cod, alm) {
	if (confirm('Seguro que desea no aprobar la Dotaci\u00f3n ' + num + '?'))
	{
		var param = [];
		param['abc']=abc;
		param['num']=num;
		param['cia']=cia;
		param['cod']=cod;
		ejecutasqlp("negardotacion.php",param);
		for(i in gdata)	{
			if(gdata[i].ERROR){
				//alert(gdata[i].ERROR);
				if(gdata[i].ERROR == 1)alert("C\u00f3digo de autorizaci\u00f3n ya utilizado");
				if(gdata[i].ERROR == 2)alert("Dotaci\u00f3n ya fue procesada");
				if(gdata[i].ERROR == 3)alert("Usuario NO autorizado para porcesar esta dotaci\u00f3n como No aprobada");
				if(gdata[i].ERROR == 4){
					var windowprops = "toolbar=0,location=0,directories=0,status=0, " + "menubar=0,scrollbars=0,resizable=0,width=600,height=300";
					OpenWindow = window.open("correo.php?&atrnum="+num+"&aalcod="+alm+"&atrcod="+cod, "enviodecorreo", windowprops);
					alert("Dotaci\u00f3n Nro: "+num+" rechazada con \u00e9xito");
					location.reload();
				}
			}
		}
	}
}

function reverzar(alma, desalma, tipo, atrcod)
{
	if(confirm("Desea Revertir la Dotaci\u00f3n " + tipo + " del Almac\u00e9n "+desalma)){
		var param = [];var vdata;
		param['aalcod']=alma;
		param['atrnum']=tipo;
		param['atrcod']=atrcod;
		
		$.ajax({
			async: false,
			type: 'POST',
			url: 'test.php',
			data: convertToJson(param),
			success: function(datos){                
				vdata = parseJSONx(datos);
				for(i in vdata)	{
					//alert(vdata[i].FIN);
					if(vdata[i].FIN==1) 	{
						alert("La dotaci\u00f3n se confirm\u00f3 exitosamente");
						requerimimprimir(tipo, alma, atrcod);///falta atrcod
						location.reload();
					}
					else{
						alert("La dotaci\u00f3n tiene problemas para realizar la reversa");
					}
				}
			}
		});	
	}
}
/*
function validarCantidad()
{
	var indice4 = document.solicitudes.aarcod.selectedIndex;
	var texto4 = document.solicitudes.aarcod.options[indice4].text;	
	var cantidad =  document.solicitudes.aslcan.value;
	cantidad = cantidad.replace(",",".");
	
	//BUSCAR CANTIDAD
	var pos1 =texto4.indexOf(">");
	var pos2 =texto4.indexOf("*");
	if(pos1!="-1" && pos2!="-1") { var exist = texto4.substring((pos1+1),pos2); }
	else		  { var exist = 0		    				}
	if(exist.length>1){
		exist = exist.replace(",","");
	}
	
	//alert(cantidad+"**"+exist);
	// VALIDAR SI CANTIDAD > EXISTENCIA
	if( Number(cantidad) <= Number(exist) )
	{
		return 1;//bien 
	}
	else if( Number(cantidad) > Number(exist) ){
		return 2;//mal
	}
}
*/
function validarCantidad()
{
	var indice4 = document.solicitudes.aarcod.selectedIndex;
	var texto4 = document.solicitudes.aarcod.options[indice4].text;	
	var cantidad =  document.solicitudes.aslcan.value;
	cantidad = cantidad.replace(",",".");
	var atrnum = document.solicitudes.atrnum.value;
	var aalcod = document.solicitudes.aalcod.value;
	var param = [];
	
	var atscod = document.solicitudes.atscod.value;

	if(atrnum){
		param['aalcod']=aalcod;
		param['adsnro']=atrnum;
		param['atscod']=atscod;
		ejecutasqlp("validarDotDev.php",param);
		for(i in gdata){
			if(gdata[i].SIG == '-'){
				/* BUSCAR CANTIDAD */
				var pos1 =texto4.indexOf(">");
				var pos2 =texto4.indexOf("|");
				if(pos1!="-1" && pos2!="-1") { var exist = texto4.substring((pos1+1),pos2); }
				else		  { var exist = 0		    				}
				if(exist.length>1){
					exist = exist.replace(",","");
				}
				
				//alert(cantidad+"**"+exist);
				/* VALIDAR SI CANTIDAD > EXISTENCIA*/
				if( Number(cantidad) <= Number(exist) ){
					return 1;//bien 
				}
				else if( Number(cantidad) > Number(exist) ){
					return 2;//mal
				}
			}
			else if(gdata[i].SIG == '+'){
				return 1;//bien 
			}
		}
	}
	else
	{
		return 2;
	}
}

function openActa(acta,atscod,adsnro,comp,aalcod)
{
	
	var windowprops = "toolbar=0,location=0,directories=0,status=0, " + "menubar=0,scrollbars=0,resizable=0,width=600,height=300";
	OpenWindow = window.open("exportarapdf.php?&atscod="+atscod+"&adsnro="+adsnro+"&acicod="+comp+"&aalcod="+aalcod+"&acta="+acta, "", windowprops);
		
}

function etiquetasDotaciones(num, atrcod){
	self.name = "main"; // names current window as "main"
	
	var windowprops ="toolbar=0,location=0,directories=0,status=0,"+"menubar=0,scrollbars=1,resizable=0,width=990,height=800";
	
	var ventana="../etiquetas2/exportpdf_dotaciones.php?&num="+num+"&atrcod="+atrcod;
	
	window.open(ventana,"", windowprops); // opens remote control
}

function certificadoLiberacion(nrodot, alma, tipdot){
	var windowprops = "toolbar=0,location=0,directories=0,status=0,"+"menubar=0,scrollbars=0,resizable=0,width=600,height=300";
	OpenWindow = window.open("exportar_cert_lib_prod.php?&nrodot="+nrodot+"&alma="+alma+"&tipdot="+tipdot, "", windowprops);
}

function verificacionFuentes(nrodot, alma, tipdot){
	var windowprops = "toolbar=0,location=0,directories=0,status=0,"+"menubar=0,scrollbars=0,resizable=0,width=600,height=300";
	OpenWindow = window.open("exportar_verificacion_fuentes.php?&nrodot="+nrodot+"&alma="+alma+"&tipdot="+tipdot, "", windowprops);
}