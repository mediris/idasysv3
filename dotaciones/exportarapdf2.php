<?php 
session_start();
include_once("../conectar.php");
require_once('tcpdf/config/lang/eng.php');
require_once('tcpdf/tcpdf.php');

$atrnum = trim($_REQUEST["atrnum"]);
$aalcod = trim($_REQUEST["aalcod"]);

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor($Usuarionombre);
$pdf->SetTitle('Dotacion '.$atrnum.'');
$pdf->SetSubject('Dotacion '.$atrnum.'');
//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, 'Despachar Pedido', PDF_HEADER_STRING);
$pdf->SetHeaderData('../../../images/logoidacadef20052.png', PDF_HEADER_LOGO_WIDTH, 'Despachar Pedido', 'R.I.F.: '.$Companiarif);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

// add a page
$pdf->AddPage();
// set font
$pdf->SetFont('helvetica', '', 10);

$sql2="SELECT T2.ATSCOD, T2.ADPCOD, T2.ADSNRO, T2.Atrfec, T2.Atrhor, T5.DESC82, 
				 			   T2.ATROBS, T2.AUSCOD, T3.AUSAP1, T3.AUSAP2, T3.AUSNO1, T3.AUSNO2, 
							   T4.AEMMAI, T2.ADSSTS , T2.ATRDES, T2.AISCOD, T6.AISDES
						FROM iv35fp T2, kn082lg1 T5 , mb20fp T3 
						left join mb21fp T4 on (T3.auscod=t4.auscod and T4.aemprd='S') 
						, IV42FP T6
						WHERE T2.AUSCOD = T3.AUSCOD 
							and T2.AALCOD = T5.ALMA82 
							and T2.acicod = '$Compania' 
							AND T2.ACICOD = T6.ACICOD
							and T2.ATSCOD = '0052'  
							and T2.ADSNRO = '$atrnum' 
							and T2.AALCOD = '$aalcod'
							AND T2.AISCOD = T6.AISCOD";
/*
	0-T1.ATSCOD, 1-T1.ADPCOD, 2-T1.ADSNRO, 3-T1.Atrfec, 4-T1.Atrhor,  
	5-T5.DESC82, 6-T1.ATROBS, 7-T1.AUSCOD, 8-T3.AUSAP1, 9-T3.AUSAP2, 
	10-T3.AUSNO1,  11-T3.AUSNO2, 12-T4.AEMMAI, 13-T1.ATRSTS , 14-T2.ATRDES
*/
			$result2=odbc_exec($cid,$sql2)or die(exit("Error en odbc_exec 11111"));
			$atrcod=trim(odbc_result($result2,1));//cod transaccion
			$adpcod=trim(odbc_result($result2,2));//departamento
			if(!empty($adpcod))
			{
				$aunsup = unisolicitante($adpcod, $Compania);
			}else
			{
				$aunsup ='-';
			}
			
			$atrnum=trim(odbc_result($result2,3));//transaccion
			$atrfec=trim(odbc_result($result2,4));//fecha de transaccion
			$atrhor=trim(odbc_result($result2,5));//hora de transaccion
			$desc82=trim(odbc_result($result2,6));//descripcion de almacen
			$atrobs=trim(odbc_result($result2,7));//obser
			$auscod=trim(odbc_result($result2,8));//usuario quien realizo la ord
			$ausap1=trim(odbc_result($result2,9));//ususario apellido 1
			$ausap2=trim(odbc_result($result2,10));//ususario apellido 2
			$ausno1=trim(odbc_result($result2,11));//usuario nombre 1
			$ausno2=trim(odbc_result($result2,12));//usuario nombre 2
			$aemmai=trim(odbc_result($result2,13));//email.. desti 
			$atrsts=trim(odbc_result($result2,14));//status transaccion
			$atrdes=trim(odbc_result($result2,15));//status transaccion
			$aiscod=trim(odbc_result($result2,16));// cod iden servicio
			$aisdes=trim(odbc_result($result2,17));// descripcion iden servicio
			
			//detalle del Requerimiento			
			$sqla="SELECT t1.AtrCAN, t1.AtrUMb, t2.AARDES,t1.aarcod, t1.atdsts 
					FROM iv36FP t1 left join IV05FP t2 on (t1.aarcod=t2.aarcod) 
					WHERE t1.ACICOD='$Compania' and t1.acicod=t2.acicod and t1.ADSNRO=$atrnum and 
						  t1.ATSCDO='0052' and t1.aalcod='$aalcod'";

			$resulta=odbc_exec($cid,$sqla)or die(exit("Error en odbc_exec 11111"));
			$html.='
			 	<table width="168" cellpadding="0" cellspacing="0">
                  <thead>
						<tr>
							<th colspan="1" valign="middle" rowspan="2"><img src="http://'.$Direccionip.'/idasysv3/images/logoidacadef20052.png"/><h5>R.I.F.: '.$Companiarif.'</h5></th>
							<th colspan="5" valign="middle" scope="col" align="center"><h3>Dotaci&oacute;n Nro '.$atrnum.'</h3></th>
						</tr>
						<tr>
						  <th valign="middle" colspan="6"><div>Almac&eacute;n:<strong>'.almacen_viejo($aalcod).'</strong></div></th>
					   </tr>
				  </thead>
                  <tbody>
		            <!--<tr>
					  
					  <td valign="middle" colspan="2" align="right"><strong>C&oacute;digo de Transacci&oacute;n: '.$atrcod.'</strong></td>
		              <td colspan="2" valign="middle" align="left">Fecha:<strong>'.date('d.m.y').'</strong></td>
		            </tr>-->
		            <tr>
		              <td valign="middle"><strong>Fecha / Hora:</strong></td>
		              <td valign="middle"><strong>Servicio:</strong></td>
		              <td valign="middle" colspan="2"><strong>Usuario:</strong></td>
		              <!--<td valign="middle"><strong>Departamento</strong></td>-->
		              <td valign="middle"><strong>Status</strong></td>
	                </tr>
		            <tr >
		              <td height="24" valign="middle"><div >'.$atrfec.' / '.$atrhor.'</div></td>
		              <td valign="middle"><div>'.$aisdes." - ".utf8_encode($atrdes).'</div></td>
		              <td valign="middle" colspan="2"><div>'.utf8_encode(usuario($auscod)).'</div></td>
		              <!--<td valign="middle"><div>'.utf8_encode($aunsup).'</div></td>-->
		              <td valign="middle"><div>'.utf8_encode(status('ADSSTS',$atrsts)).'</div></td>
	                </tr>
					<tr>
						<td colspan="5"><hr /></td>
					</tr>
		            <tr>
		              
		              <th valign="middle"><strong>C&oacute;digo Art&iacute;culo:</strong></th>
		              <th valign="middle"><strong>Descripci&oacute;n</strong></th>
		              <th valign="middle"><strong>Cantidad</strong></th>
		              <th valign="middle"><strong>Unidad de Medida</strong></th>
                      <th valign="middle"><strong>Status</strong></th>
 					</tr>';  

                    while(odbc_fetch_row($resulta)){
						$aarcod=odbc_result($resulta,4);
						$descripcion=odbc_result($resulta,3);
						$stsdet=odbc_result($resulta,5);
						$cantidad=number_format(odbc_result($resulta,1),2,",",".");
						$unidad=odbc_result($resulta,2);
	
						$html.='
						<tr>
							<td valign="middle"><div>'.$aarcod.'</div></td>
							<td valign="middle"><div>'.utf8_encode($descripcion).'</div></td>
							<td valign="middle" align="right">'.$cantidad.'</td>
							<td valign="middle"><div>'.unidad_medidad($unidad,$Compania, $cantidad).'</div></td>
							<td valign="middle"><div>'.utf8_encode(status('ATDSTS',$stsdet)).'</div></td>
						</tr>';
                     } 
			$html.='
					  <tr>
						<td colspan="5"><hr /></td>
					</tr>
					<tr>
		              <td valign="middle"><div ><strong>Observaciones</strong> :</div></td>
		              <td colspan="4" valign="middle"><div>'.utf8_encode($atrobs).'</div></td>
	                </tr>
					 <tr>
						<td colspan="5"><hr /></td>
					</tr>
					<tr>
						 <td align="center" colspan="2">Despachado Por:</td>
						 <td align="center" >&nbsp;</td>
						 <td align="center" colspan="2">Recibido Por:</td>
					 </tr> 
					 <tr>
						 <td align="center" colspan="2">'.utf8_encode(usuario($Usuario)).'</td>
						 <td align="center" >&nbsp;</td>
						 <td align="center" colspan="2" rowspan="2">
							 <table><tr><td>Nombre:</td><td>_______________</td></tr>
							 <tr><td>C.I.:</td><td>_______________</td></tr>
							 <tr><td>Firma:</td><td>_______________</td></tr></table>
						</td>
					 </tr>
					  <tr>
						 <td align="center" colspan="2">_________________</td>
						 <td align="center" >&nbsp;</td>
					 </tr>
					</tbody>
</table>';
	//echo $html;
$tbl = <<<EOD
	$html
EOD;

	$pdf->writeHTML($tbl, true, false, false, false, '');
	$pdf->Output('Despacho_'.$atrnum.'.pdf', 'I');

?>