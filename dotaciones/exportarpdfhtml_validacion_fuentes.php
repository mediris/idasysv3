<?php
session_start();

include_once("../conectar.php");

if($Compania =='43'){
	$img = 'MEDITRON_logo_rif_2.png';
}else{
	$img = 'logomeditronnuevo2.png';
}

$direccion = !empty($Direccionip)?$Direccionip:'administracionweb';
$Companiarif = $_SESSION['Companiarif'];
			
$html.='
	<style>
		.principal{
			width:100%; 
			margin-left:4px; 
			border-bottom: 1px solid;
			padding: 5px 0px 5px 0px;
		}
		.item{
			text-align: left;
			margin-left: 14px;
			margin-top: 0px;
			font-size: 11;
		}
		.conforme{
			margin-left: 655px;
			margin-top: -17px;
			text-align:justify;
		}
		.no-conforme{
			text-align: right;
			margin-right: 48px;
			margin-top: -32px;
		}
		.cuadro{
			border:1px solid; 
			width:15px;
			height:15px;
		}
	</style>
	<page backtop="70mm" backbottom="80mm" >
		<page_header>
			<table style="width:100%;align:center;" border="0" >
            	<tr>
                	<td>
                    	<img src="http://'.$direccion.'/idasysv3/images/'.$img.'"/><h5>R.I.F.: '.$Companiarif.'</h5>
                	</td>
                	<td>
                    	<table style="width: 50%;margin-left:350px;font-size:10;">
                        	<tr align="left" style="width: 30%;"><td>P&Aacute;G. N&deg; [[page_cu]] DE [[page_nb]]</td></tr>
                        	<tr align="left" style="width: 30%;"><td>FECHA: '.date('d/m/Y').'</td></tr>
                        	<tr align="left" style="width: 30%;"><td>N° de control: '.add_ceros($nrodot,6).'</td></tr>
                    	</table>
                	</td>
            	</tr>
            	<tr>
             		<td colspan="2" style="text-align: center"> 
						<h3>VERIFICACIÓN DE LAS FUENTES</h3>
					</td>
            	</tr>
            	<tr>
                	<td colspan="3" style="width: 100%;">
						<table style="width: 100%;">
							<tr>
								<td align="left" style="width: 20%;"><strong>Cliente:</strong> </td>
								<td align="left" style="width: 81%;">'.trim(utf8_encode($cliente)).'</td>
							</tr>
							<tr>
								<td align="left" style="width: 20%;"><strong>Dirección:</strong> </td>
								<td align="left" style="width: 81%;">'.trim(utf8_encode($direccionServicio)).'</td>
							</tr>
							<tr>
								<td align="left" style="width: 20%;"><strong>Paciente:</strong> </td>
								<td align="left" style="width: 81%;">'.trim(utf8_encode($paciente)).'</td>
							</tr>
							<tr>
								<td align="left" style="width: 20%;"><strong>Tipo de fuente:</strong> </td>
								<td align="left" style="width: 81%;"><strong>NO SELLADA</strong></td>
							</tr>
						</table>
					</td>
            	</tr>
            	<tr>
					<td colspan="3" style="width: 100%;" style="vertical-align:top;height: 502px;">
						<table style="vertical-align:top;width: 100%; margin-top:25px;" cellpadding="0" cellspacing="0"> 
							<tr>
								<td style="vertical-align:middle; width:80%; font-size:11; border-top:1px solid; border-bottom:1px solid; border-left:1px solid; padding:5px 0px 5px 5px;">
									<strong>VERIFICACIÓN DEL CORRECTO ETIQUETADO</strong>
								</td>
								<td style="text-align:center; width:10%; vertical-align:middle; font-size:11; border-top:1px solid; border-bottom:1px solid; border-left:1px solid; padding:5px 0px 5px 0px;">
									<strong>SI</strong>
								</td>
								<td style="text-align:center; width:10%; vertical-align:middle; font-size:11; border-top:1px solid; border-bottom:1px solid; border-left:1px solid; border-right:1px solid; padding:5px 0px 5px 0px;">
									<strong>NO</strong>
								</td>
							</tr>
							<tr>
								<td style="height:151px; text-align:center; border-left:1px solid;">
									&nbsp;
								</td>
								<td style="text-align:center; border-left:1px solid;">
									&nbsp;
								</td>
								<td style="text-align:center; border-left:1px solid; border-right:1px solid;">
									&nbsp;
								</td>
							</tr>
						</table>
					</td>
				</tr>
            </table>
		</page_header>
		<page_footer>
			<table style="width: 101%; border: 1px solid; border-bottom:none;border-right:none; border-left:none; margin-left: 3px;" cellpadding="2px" cellspacing="0">
				<tr>
					<td style="border-right:1px solid; border-left:1px solid; text-align:left; border-bottom:1px solid;" colspan="3">
						<strong>OBSERVACIONES:</strong>
					</td>
				</tr>
				<tr>
					<td style="height:130px;border-right:1px solid; border-left:1px solid; text-align:left; border-bottom:1px solid;" colspan="3"></td>
				</tr>
				<tr>
					<td style=" border-bottom:1px solid; border-right:1px solid; border-left:1px solid; text-align:center; padding: 5px 0px 5px 0px;">
						<strong>Elaborado por:</strong>
					</td>
					<td style=" border-bottom:1px solid; text-align:center;border-right:1px solid; padding: 5px 0px 5px 0px;">
						<strong>Verificado por:</strong>
					</td>
					<td style=" border-bottom:1px solid; border-right:1px solid;text-align:center; padding: 5px 0px 5px 0px;">
						<strong>Recibido por:</strong>
					</td>
				</tr>
        		<tr>
            		<td style="border-right:1px solid; border-left:1px solid; width: 33%;">
						<table style="font-size:10px; width: 100%;" cellspacing="3px">
							<tr>
								<td style="padding: 3px 0px 3px 0px;"><strong>Nombre y Apellido:</strong></td>
								<td>___________________________</td>
							</tr>
							<tr>
								<td style="padding: 3px 0px 3px 0px;"><strong>Cargo:</strong></td>
								<td>___________________________</td>
							</tr>
							<tr>
								<td style="padding: 3px 0px 3px 0px;"><strong>Unidad:</strong></td>
								<td >___________________________</td>
							</tr>
							<tr>
								<td style="padding: 3px 0px 3px 0px;"><strong>Firma:</strong></td>
								<td >___________________________</td>
							</tr>
							<tr>
								<td style="padding: 3px 0px 3px 0px;"><strong>Fecha:</strong></td>
								<td><strong>Hora:</strong></td>
							</tr>
						</table>
					</td>
            		<td style="width: 33%;border-right:1px solid;">
						<table style="font-size:10px; width: 100%; padding: 5px 0px 5px 0px;" cellspacing="3px">
							<tr>
								<td style="padding: 3px 0px 3px 0px;"><strong>Nombre y Apellido:</strong></td>
								<td>___________________________</td>
							</tr>
							<tr>
								<td style="padding: 3px 0px 3px 0px;"><strong>Cargo:</strong></td>
								<td>___________________________</td>
							</tr>
							<tr>
								<td style="padding: 3px 0px 3px 0px;"><strong>Unidad:</strong></td>
								<td>___________________________</td>
							</tr>
							<tr>
								<td style="padding: 3px 0px 3px 0px;"><strong>Firma:</strong></td>
								<td >___________________________</td>
							</tr>
							<tr>
								<td style="padding: 3px 0px 3px 0px;"><strong>Fecha:</strong></td>
								<td><strong>Hora:</strong></td>
							</tr>
						</table>
					</td>
					<td style="width: 33%; border-right:1px solid;">
						<table style="font-size:10px; width: 100%;" cellspacing="3px">
							<tr>
								<td style="padding: 3px 0px 3px 0px;"><strong>Nombre y Apellido:</strong></td>
								<td>___________________________</td>
							</tr>
							<tr>
								<td style="padding: 3px 0px 3px 0px;"><strong>Cargo:</strong></td>
								<td>___________________________</td>
							</tr>
							<tr>
								<td style="padding: 3px 0px 3px 0px;"><strong>Unidad:</strong></td>
								<td >___________________________</td>
							</tr>
							<tr>
								<td style="padding: 3px 0px 3px 0px;"><strong>Firma:</strong></td>
								<td >___________________________</td>
							</tr>
							<tr>
								<td style="padding: 3px 0px 3px 0px;"><strong>Fecha:</strong></td>
								<td><strong>Hora:</strong></td>
							</tr>
						</table>
					</td>
        		</tr>
        		<tr style="font-size:10px;">
					<td style="border-top: 1px solid;text-align:left;" colspan="3">
						F-LAB-041-06-18
					</td>
				</tr>
			</table>
		</page_footer>
		<div class="principal">
			<div class="item">CONTENIDO: <span style="margin-left: 180px;"><strong>Contenedor de Vial con 131I</strong></span></div>
			<div class="conforme">
				<div class="cuadro"></div>
			</div>
			<div class="no-conforme">
				<div class="cuadro"></div>
			</div>
		</div>
		<div class="principal">
			<div class="item">CLASIFICACIÓN Y CATEGORÍA: <span style="margin-left: 140px;"><strong>CLASE 7</strong></span></div>
			<div class="conforme">
				<div class="cuadro"></div>
			</div>
			<div class="no-conforme">
				<div class="cuadro"></div>
			</div>
		</div>
		<div class="principal">
			<div class="item">NÚMERO DE LAS NACIONES UNIDAS: <span style="margin-left: 107px;"><strong>UN-2916</strong></span></div>
			<div class="conforme">
				<div class="cuadro"></div>
			</div>
			<div class="no-conforme">
				<div class="cuadro"></div>
			</div>
		</div>
		<div class="principal">
			<div class="item">ACTIVIDAD: </div>
			<div class="conforme">
				<div class="cuadro"></div>
			</div>
			<div class="no-conforme">
				<div class="cuadro"></div>
			</div>
		</div>
		<div class="principal">
			<div class="item">ÍNDICE DE TRANSPORTE (IT): </div>
			<div class="conforme">
				<div class="cuadro"></div>
			</div>
			<div class="no-conforme">
				<div class="cuadro"></div>
			</div>
		</div>
		<div class="principal">
			<div class="item">ETIQUETADO DE BULTO: <span style="margin-left: 165px;"><strong>AMARILLO III</strong></span></div>
			<div class="conforme">
				<div class="cuadro"></div>
			</div>
			<div class="no-conforme">
				<div class="cuadro"></div>
			</div>
		</div>
		<table style="vertical-align:top; width: 100%; margin-top:5px; margin-left: 3px;" cellpadding="0" cellspacing="0"> 
			<tr>
				<td style="vertical-align:middle; width:100%; font-size:11; border-top:1px solid; border-bottom:1px solid; border-right:1px solid; border-left:1px solid; padding:5px 0px 5px 5px;">
					<strong>VERIFICACIÓN EN EL DESPACHO DE LABORATORIO</strong>
				</td>
			</tr>
			<tr>
				<td style="font-size:11; border-left:1px solid; border-right:1px solid; border-bottom:1px solid; padding: 5px 0px 5px 5px;">
					1. La Tasa de Dosis (TD) a un (1) metro de distancia del bulto, es igual o menor que: TD &lt;= IT/100 &nbsp;&nbsp;&nbsp;____ mSv/h&nbsp;&nbsp;&nbsp; ____ µSv/h
					<p style="text-align: center">SI __________ &nbsp;&nbsp;&nbsp; NO __________ &nbsp;&nbsp;&nbsp; Tasa de Dosis __________ µSv/h</p>
				</td>
			</tr>
			<tr>
				<td style="font-size:11; border-left:1px solid; border-right:1px solid; border-bottom:1px solid; padding: 5px 0px 5px 5px;">
					2. La Tasa de Dosis (TD) en la superficie de las cuatro (4) caras del bulto es:  Mayor qué 5 µSv/h y Menor qué 500 µSv/h
					<p style="text-align: center">SI __________ &nbsp;&nbsp;&nbsp; NO __________ &nbsp;&nbsp;&nbsp; Tasa de Dosis &lt;__________ µSv/h</p>

				</td>
			</tr>
		</table>
		<table style="vertical-align:top; width: 100%; margin-top:5px; margin-left: 3px;" cellpadding="0" cellspacing="0"> 
			<tr>
				<td style="vertical-align:middle; width:100%; font-size:11; border-top:1px solid; border-bottom:1px solid; border-right:1px solid; border-left:1px solid; padding:5px 0px 5px 5px;">
					<strong>VERIFICACIÓN EN EL VEHÍCULO</strong>
				</td>
			</tr>
			<tr>
				<td style="font-size:11; border-left:1px solid; border-right:1px solid; border-bottom:1px solid; padding: 5px 0px 5px 5px;">
					1. La Tasa de Dosis (TD) en la cabina del vehículo no es superior a: 20 µSv/h
					<p style="text-align: center">SI __________ &nbsp;&nbsp;&nbsp; NO __________ &nbsp;&nbsp;&nbsp; Tasa de Dosis __________ µSv/h</p>
				</td>
			</tr>
		</table>
		<table style="vertical-align:top; width: 100%; margin-top:5px; margin-left: 3px;" cellpadding="0" cellspacing="0"> 
			<tr>
				<td style="vertical-align:middle; width:100%; font-size:11; border-top:1px solid; border-bottom:1px solid; border-right:1px solid; border-left:1px solid; padding:5px 0px 5px 5px;">
					<strong>VERIFICACIÓN EN LA ENTREGA DE LA FUENTE EN EL HOSPITAL O INSTITUTO</strong>
				</td>
			</tr>
			<tr>
				<td style="font-size:11; border-left:1px solid; border-right:1px solid; border-bottom:1px solid; padding: 5px 0px 5px 5px;">
					1. La Tasa de Dosis (TD) en la superficie de las cuatro (4) caras del bulto es: ______________________________________
					<p style="text-align: center">SI __________ &nbsp;&nbsp;&nbsp; NO __________ &nbsp;&nbsp;&nbsp; Tasa de Dosis &lt;__________ µSv/h</p>
				</td>
			</tr>
		</table>
	</page>';
?>