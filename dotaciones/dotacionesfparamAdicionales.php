<?php session_start();
include("../conectar.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="sinhead">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Cargar Parametros Adicionales de Dotaciones</title>
<link href="../<?php echo $css; ?>" rel="stylesheet" type="text/css" media="screen" />
<script language="JavaScript" src="../javascript/jquery.js"></script>
<script language="JavaScript" src="../javascript/javascript.js"></script>
<script language="JavaScript" src="javascript.js"></script>
<script language="JavaScript" src="../calendario/javascripts.js"></script>
</head>
<body class="sinbody" bgcolor="#FFFFFF" onload="javascript:cargarinfopa('<?php echo trim($atrcod);?>','<?php echo trim($pedid);?>','<?php echo trim($almacen);?>'); cargarparametros(document.getElementById('atscod'),document.getElementById('atrnum').value )">
<div id="agregardiv">

<div>
<div id="ocultarform">
	<form id="agregarform" name="agregarform" method="post" class="form" action="">
    <input name="atrcod" id="atrcod" type="hidden" value="0052" />
	<table class="tabla1">
      <tr>
        <td width="29%"  scope="col"><label>C&oacute;digo y N&uacute;mero de Transacci&oacute;n</label></td>
        <td width="32%" scope="col">
        	<input name="atrnum" id="atrnum" type="hidden" value="" />
			<input name="aalcod" type="hidden" id="aalcod" value="<?php echo $almacen;?>" /> 
			<div align="left" id="nroreq"></div>
        </td>
        <td colspan="2" id="errarqtip"  scope="col">&nbsp;</td>
      </tr>
       <tr>
        <td  scope="col">Tipo de Dotaci&oacute;n: </td>
        <td width="32%" scope="col"><div align="left" id="watscod"></div>
        <input name="atscod" id="atscod" type="hidden" value="" /></td>
        <td colspan="2" id="erratscod"  scope="col">&nbsp;</td>
      </tr>
      <tr>
        <td  scope="col">Almacén: </td>
        <td width="32%" scope="col"> 
        	<div align="left" id="waalcod"></div>
        </td>
        <td colspan="2" id="erraalcod"  scope="col">&nbsp;</td>
      </tr>
      <tr>
        <td  scope="col">Servicio:</td>
        <td width="32%" scope="col">
        <input name="aiscod" type="hidden" id="aiscod" value="" />
        <div align="left" id="waiscod"></div>
        </td>
        <td colspan="2" id="errarqtip"  scope="col">&nbsp;</td>
      </tr>
      <tr>
        <td  scope="col">Nombre: </td>
        <td width="32%" scope="col">
        	<input name="atrdes" type="hidden" id="atrdes" value="" />
        	<div align="left" id="watrdes"></div>
        </td>
        <td colspan="2" id="erratrdes"  scope="col">&nbsp;</td>
      </tr>
      <tr>
        <td scope="col"><label>Observación</label></td>
        <td scope="col">
        	<input name="atrobs" type="hidden" id="atrobs" value="" />
            <div align="left" id="watrobs"></div>
        </td>
        <td colspan="2" id="erratrobs" scope="col">&nbsp;</td>
      </tr>
      <tr>
            <td colspan="4">
                <table width="100%"  border="0" >
                  <tr>
                    <td width="100%" colspan="3" scope="col"><h3>Parámetros Adicionales: </h3>
                      <span class="header">
                      </span></td>
                    </tr>
                    <tr>
                        <td>
                        <div id="parametros">
                        </div>
                        </td>
                    </tr>
                  </table>
            </td>
      </tr>
      <tr>
        <th scope="col">&nbsp;</th>
        <th scope="col"></th>
        <th width="18%" scope="col"><input name="cancelar" type="button" onClick="parent.location.reload();parent.Shadowbox.close();"" " value="Salir" id="cancelar"></th>
        <th width="21%"  scope="col"><input type="button" name="submit" id="submit" value="Grabar" onClick="editarvalidarpa()" /></th>
        
      </tr>
    </table>

  </form></div>    
</div>

</div>
<div align="center" id="agregaraftsav" style="display:none">
  <br />
  <br />
  <br />
  <br />
  <br />
  <br />
  Registro Agregado con exito !
  <br />
  <br />
  <br />
  <input name="Submit3" type="button" onClick="parent.location.reload();parent.Shadowbox.close();"" " value="Cerrar"></div>
</body>
</html>