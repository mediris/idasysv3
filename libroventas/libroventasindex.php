<?php 
/*
 * jDavila
 * 23/05/2012 
 * modificado: 24/05/2012
 */
session_start();
include("../conectar.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
<title>Idasys V3</title>
<link href="../<?php echo $css; ?>" rel="stylesheet" type="text/css" media="screen" />
<script language="JavaScript" src="../javascript/javascript.js"></script>
<script language="JavaScript" src="../calendario/javascripts.js"></script>
<script language="JavaScript" src="javascript.js"></script>
<script language="JavaScript" src="../javascript/jquery.js"></script>
<link href="../shadowbox.css"  rel="stylesheet" type="text/css"/>
<script src="../shadowbox.js" type="text/javascript"/> </script>
<script language="JavaScript" type="text/JavaScript">
Shadowbox.init({overlayOpacity: "0.5"});
</script>
<style type="text/css" title="currentStyle">
			@import "../DataTables-1.9.4/media/css/<?php echo $demo_page;?>";
			@import "../DataTables-1.9.4/media/css/<?php echo $demo_table;?>";
</style>
<script type="text/javascript" language="javascript" src="../DataTables-1.9.4/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf-8">
			
			 $(document).ready(function() 
			 {
			      document.getElementById('info').style.display="";
				  $('#info').dataTable( 
				  {
			        "oLanguage": 
					{
			          	"sLengthMenu": "Mostrar _MENU_ registros por Pag.",
						"sZeroRecords": "No Existen Registros",
						"sInfo": "Mostrando desde _START_ hasta _END_ de _TOTAL_ registros",
						"sInfoEmpty": "Mostrando desde 0 hasta 0 de 0 registros",
						"sInfoFiltered": "(filtrando de _MAX_ de registros)",
						"sSearch": "Buscar",
						"oPaginate": 
						{
							"sNext": "Siguiente",
							"sPrevious": "Anterior"
						}
			        }
			      } );
			  } );
</script>

</head>
<body background="../images/fondo idaca.jpg" >
<div id="wrapper">
  <?php include("../superior.php");?>
  <div id="page">
     <?php include("../validar.php");  		?>

	  
 		<?php 
				
				if ($bandera==1) 
				{	
					$wsolicitud=0;
					if ($solicitudpagina==0) 	
					{
								
								$sql ="SELECT ACCCIA, ADDLOF, ADDFEF, (case when addnrr=0 then ADDTPF else addtir end)as tipo,min( case when addnrr=0 then addnrf else addnrr end) as minimo, max ( case when addnrr=0 then addnrf else addnrr end) as maximo ,  sum( ADDTED+ ADDTEN+ ADDTMI ) as sum
										FROM id72fp 
										WHERE ACCCIA ='$Compania' and ADDFEF between '$desde' and '$hasta' and ADDLOF <> 'PR' and addsbs='F' and addsts>'01' 
										GROUP BY ACCCIA, ADDLOF, ADDFEF, ADDTPF, (case when addnrr=0 then ADDTPF else addtir end) ORDER BY ACCCIA, ADDLOF, ADDFEF, ADDTPF";
								$resultt=odbc_exec($cidv2,$sql)or die(exit("Error en odbc_exec 11111"));
								//echo $sql;
								$z=0;
								$tothon=0;
								$totest=0;
								$lin=1;
								$limitep=$_SESSION['solicitudlineasporpaginat'];
								$pag=1;
								$primero='S';
								
								while(odbc_fetch_row($resultt))
								{
									$jml = odbc_num_fields($resultt);
									$row[$z]["pagina"] =  $pag;
									for($i=1;$i<=$jml;$i++)
									{	if (odbc_field_name($resultt,$i)=='ARGTTE') {$totest+=odbc_result($resultt,$i);}
										if (odbc_field_name($resultt,$i)=='ARGTTM') {$tothon+=odbc_result($resultt,$i);}
										$row[$z][odbc_field_name($resultt,$i)] =  odbc_result($resultt,$i);
									}
									$z++;
									if ($lin>=$limitep) 
									{
										$limitep+=$_SESSION['solicitudlineasporpaginat'];
										$pag++;
									}
									$lin++;
								}
								$totsol=($lin-1);
								$_SESSION['totalsolicitudes']=$totsol;
								$_SESSION['solicitudarreglo']=$row;
								$solicitudpagina=1;
								$_SESSION['solicitudpaginas']=$pag;
					}//fin de solicitudpagina
					/*se muestra la cantidad de elementos segun la solicitada en $solicitudpagina*/
										$paginat=$_SESSION['solicitudarreglo'];
				}
			?>
        <div id="content3" >   
        	<table width="100%"   border="0">
              <tr>
                <td width="65%" scope="col" colspan="2"><h1  class="title">Libro de Ventas</h1><hr /></td>
                <td width="13%" scope="col"><div align="left">
                  <table width="100%"  border="0">
                    <tr>
                      <th width="30%" scope="col" >
                      <div class="flechas">
                      	<a href="exportarapdf.php?&desde=<?php echo $desde; ?>&hasta=<?php echo $hasta; ?>" target="_blank">
                        	<img src="../images/pdf.jpg" alt="" width="30" height="30" border="0" class="flechas" title="Exportar a PDF"/>
                        </a>
                        <a href="exportaraexcel.php?&desde=<?php echo $desde; ?>&hasta=<?php echo $hasta; ?>" target="_blank">
                        	<img src="../images/excel.jpg" alt="" width="30" height="30" title="Exportar a Excel"/>
                        </a>
                      </div></th>
                    </tr>
                  </table>
                </div></td>
              </tr>
              <tr>
              	<td colspan="3">
                	<form name="form" id="form" method="post">
                    <input type="hidden" name="bandera" id="bandera" value="<?php echo $bandera;?>" />
                	<table>
                    	<tr>
                        	<td>&nbsp;Desde:</td>
                            <td>&nbsp;<?php echo  escribe_formulario_fecha_vacio('desde','form',$desde); ?></td>
                            <td>&nbsp;</td>
                            <td>&nbsp;Hasta:</td>
                            <td>&nbsp;<?php echo  escribe_formulario_fecha_vacio('hasta','form',$hasta); ?></td>
                            <td rowspan="2"><a href="javascript:busqueda();"><img src="../images/precesarconsulta.png" width="50" height="50" alt="Realizar Consulta" style="border:none;"></a></td>
                        </tr>
                    </table>
                    </form>
                    <hr />
                </td>
              </tr>
            </table>   
		<div id="container">
            <div id="demo">
                <table width="100%" id="info" style="display:none">
	
  <thead>
  	<tr>
    	<th width="10%" scope="col">Localidad</th>
    	<th width="10%" scope="col">Fecha</th>
    	<th width="10%" scope="col">Tipo</th>
    	<th width="10%" scope="col">Nro. Control Min</th>
        <th width="10%" scope="col">Nro. Control Max</th>
		<th width="10%" scope="col">Total</th>
 	</tr>
  </thead>
    <tbody>
         <?php 
									
									//print_r($paginat);
									$pagact=$solicitudpagina;
									for($g=0; $g < (count($paginat)); $g++)
									{
										
                                ?>
                                		<tr>
                               		 		<td scope="col"><div align="center"><strong><?php echo $paginat[$g]["ADDLOF"];?></strong></div></td>
                                            <td scope="col"><div align="center"><?php echo fecdma($paginat[$g]["ADDFEF"], 'amd','/' );?></div></td>
                                            <td scope="col"><div align="center"><?php echo $paginat[$g]["TIPO"];?></div></td>
                                            <td scope="col"><div align="right"><?php echo $paginat[$g]["MINIMO"];?></div></td>
                                            <td scope="col"><div align="right"><?php echo $paginat[$g]["MAXIMO"];?></div></td>
                                            <td scope="col"><div align="right"><?php echo number_format($paginat[$g]["SUM"],2,",",".");?></div>  </td>
                                        </tr>
                            	<?php } ?>      
  </tbody>

</table>
</div>
		  </div>
	  </div>
	</div>
		<!-- end #content -->
		<!-- end #sidebar -->
		<div style="clear: both;">&nbsp;</div>
	</div>
	<!-- end #page -->

	<div id="footer">
		<div id="opcionesmasusadas"><?php include("../opcionesmasusadasphp.php"); ?></div>
	</div>
 
	<!-- end #footer -->
</body>
</html>
