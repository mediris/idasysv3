/*
jDavila
15/02/12
*/
function agregar()
{
	document.agregarform.astfld.value="";
	document.agregarform.astcod.value="";
	document.agregarform.astdes.value="";
	document.agregarform.astdcr.value="";
	$("#agregaraftsav").hide(1);
}

/*
jDavila
15/02/12
*/
function agregarvalidar(){	
	var contenido= "";
	var elemento;
	document.body.style.cursor="wait";

	$.ajax({
	   	async: false,
        type: "POST",
		dataType: "JSON",
        url: "statusagregarvalidarphp.php",
        data: $("#agregarform").serialize(),
        success: function(datos){
			vdata = parseJSONx(datos);}							
	});			
	sierror='N';
	for(i in vdata)	{
		if(vdata[i].campo) 	{
			document.getElementById(vdata[i].campo).innerHTML=vdata[i].msg;
			sierror='S';
		}
	}
	document.body.style.cursor="default";
	if(sierror=='N') 		
	{
		$("#agregardiv").hide(1000);
		$("#agregaraftsav").show(1000);
	}
}

/*
jDavila
15/02/12
*/
function editar(status, valor) {

	var param = [];
	param['astcod']=status;
	param['astfld']=valor;
	ejecutasqlp("statusinformacionphp.php",param);

	for(i in gdata){
		document.editarform.hastfld.value=gdata[i].ASTFLD;
		document.getElementById("wsastfld").innerHTML=gdata[i].ASTFLD;
		document.editarform.hastcod.value=gdata[i].ASTCOD;
		document.getElementById("wsastcod").innerHTML=gdata[i].ASTCOD;
		document.editarform.astdes.value=gdata[i].ASTDES;
		document.editarform.astdcr.value=gdata[i].ASTDCR;
	};
	$("#agregaraftsav").hide(1);
}

/*
jDavila
15/02/12
*/
function editarvalidar(){	
	var contenido= "";
	var elemento;
	document.body.style.cursor="wait";
	$.ajax({
			async: false,
			type: "POST",
			dataType: "JSON",
			url: "statuseditarvalidarphp.php",
			data: $("#editarform").serialize(),
			success: function(datos){
						vdata = parseJSONx(datos);}							
	});			
	var sierror='N';
	for(i in vdata)	{
		if(vdata[i].campo) 	{
			document.getElementById(vdata[i].campo).innerHTML=vdata[i].msg;
			sierror='S';
		}
	}
	document.body.style.cursor="default";
	if(sierror=='N') 		
	{
		$("#editarform").hide(1000);
		$("#agregaraftsav").show(1000);	
	}

}

/*
jDavila
15/02/12
*/
function eliminar(status, valor) {
	if (confirm('Seguro que desea borrar el status ' + valor + '-' + status + '?'))
	{
		var param = [];
		param['astcod']=status;
		param['astfld']=valor;
		ejecutasqld("statuseliminar.php",param);
		location.reload();
	}
}
