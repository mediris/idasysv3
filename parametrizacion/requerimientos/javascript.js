/*
 * jDavila
 * 16/02/12
 * modificado: 22/02/2012
 */
function agregar()
{
	document.agregarform.atqcod.value="";
	document.agregarform.atqdes.value="";
	//document.agregarform.aticod.value="";
	/*
	for(var i = 0; i < document.agregarform.aticod.length; i++){
		document.agregarform.aticod[i].checked = true;
	}
	*/
	document.agregarform.atrent.value="";
	document.agregarform.atrsal.value="";
	$("#agregaraftsav").hide(1);
}

/*
jDavila
16/02/12
*/
function agregarvalidar(){	
	var contenido= "";
	var elemento;
	document.body.style.cursor="wait";

	$.ajax({
	   	async: false,
        type: "POST",
		dataType: "JSON",
        url: "tiporequerimientoagregarvalidarphp.php",
        data: $("#agregarform").serialize(),
        success: function(datos){
			vdata = parseJSONx(datos);}							
	});			
	sierror='N';
	for(i in vdata)	{
		if(vdata[i].campo) 	{
			document.getElementById(vdata[i].campo).innerHTML=vdata[i].msg;
			sierror='S';
		}
	}
	document.body.style.cursor="default";
	if(sierror=='N') 		
	{
		$("#agregardiv").hide(1000);
		$("#agregaraftsav").show(1000);
	}
}

/*
 * jDavila
 * 13/02/12
 * modificado: 22/02/2012
 */
function editar(astcod) {

	var param = [];
	param['astcod']=astcod;

	/*cual es la diferencia*/
	ejecutasqlp("tiporequerimientoinformacionphp.php",param);

	for(i in gdata){
		document.editarform.hatqcod.value=gdata[i].ATQCOD;
		document.getElementById("wsatqcod").innerHTML=gdata[i].ATQCOD;
		document.editarform.atqdes.value=gdata[i].ATQDES;
		/*utilizada en el select */
		/*document.editarform.aticod.value=gdata[i].ATICOD;*/
		var num = gdata[i].ATICOD
		document.getElementById("aticod["+num+"]").checked = 1;

		document.editarform.atrent.value=gdata[i].ATRENT;
		document.editarform.atrsal.value=gdata[i].ATRSAL;
	};
	$("#editaraftsav").hide(1);
}

/*
 *jDavila
 *15/02/2012
 */
function editarvalidar(){	
	var contenido= "";
	var elemento;
	document.body.style.cursor="wait";
	$.ajax({
			async: false,
			type: "POST",
			dataType: "JSON",
			url: "tiporequerimientoeditarvalidarphp.php",
			data: $("#editarform").serialize(),
			success: function(datos){
						vdata = parseJSONx(datos);}							
	});			
	var sierror='N';
	for(i in vdata)	{
		if(vdata[i].campo) 	{
			document.getElementById(vdata[i].campo).innerHTML=vdata[i].msg;
			sierror='S';
		}
	}
	document.body.style.cursor="default";
	if(sierror=='N') 		
	{
		$("#editarform").hide(1000);
		$("#editaraftsav").show(1000);	
	}
}


/*
 *jDavila
 *15/02/2012
 */
function eliminar(tiporeq) {
	if (confirm('Seguro que desea borrar el tipo de requerimiento ' + tiporeq + '?'))
	{
		var param = [];
		param['atqcod']=tiporeq;
		ejecutasqld("tiporequerimientoeliminar.php",param);
		location.reload();
	}
}

/*
 *jDavila
 *16/04/2015
 */
function agregarusu()
{
	document.agregarform.auscod.value="";
	$("#agregaraftsav").hide(1);
}

/*
 *jDavila
 *16/04/2015
 */
function agregarvalidarsus(){	
	var contenido= "";
	var elemento;
	document.body.style.cursor="wait";

	$.ajax({
	   	async: false,
        type: "POST",
		dataType: "JSON",
        url: "tiporequeriusuarioagregarvalidar.php",
        data: $("#agregarform").serialize(),
        success: function(datos){
			vdata = parseJSONx(datos);}							
	});			
	sierror='N';
	for(i in vdata)	{
		if(vdata[i].campo) 	{
			document.getElementById(vdata[i].campo).innerHTML=vdata[i].msg;
			sierror='S';
		}
	}
	document.body.style.cursor="default";
	if(sierror=='N') 		
	{
		$("#agregardiv").hide(1000);
		$("#agregaraftsav").show(1000);
	}
}

/*
 *jDavila
 *16/04/2015
 */
function eliminarsus(id, atqcod, atqdes) {
	if (confirm('Seguro que desea borrar el usuario ' + id + '?'))
	{
		var param = [];
		param['auscod']=id;
		param['atqcod']=atqcod;
		param['atqdes']=atqdes;
		
		ejecutasqld("tiporequeriusuarioeliminar.php",param);
		location.reload();
	}
}