<?php
/*
 * Luis Ramos (lRamos)
 * 29/11/2016 
 */ 
session_start();
include("../conectar.php");
include("../PHPExcel/PHPExcel.php");
$arqnro = trim($_GET["num"]);
$tinva = unserialize( urldecode( $_GET['tinvan'] ) ) ;
$Usuario = trim($_GET["Usuario"]);
$coddpto = trim($_GET["coddpto"]);
$auncod = trim($_GET["auncod"]);
//$aticod = $_GET['aticod'];
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header("Content-disposition: attachment; filename=Reporte_Costos_MEDIX_".formatDate($Fechaactual,'dd.mm.aaa','aaaa_mm_dd').".xlsx");
header('Cache-Control: max-age=0');
?>
<?php 

					$sql="SELECT AUSCOD, AUSSUP FROM MB20FP WHERE AUSCOD='$Usuario' AND AUSSUP='S'";
						$result9=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111"));
						$super=trim(odbc_result($result9,2));
						if($super == 'S')
						{
							$auncod ='';
							$auncod = trim($_GET['auncod']);
							if(!empty($auncod))
							{
								$sql="SELECT T1.ARQNRO, T1.AFESOL, T1.AHRSOL, T1.ARQTIP, t2.atqdes, T1.ARQOBS, T1.ARQSTS, T1.AUSCOD 
								FROM is04fp t1, is02fp t2 
								WHERE T1.ACICOD= T2.ACICOD and T1.ARQTIP= T2.ATQCOD and t1.ACICOD = '$Compania' AND t1.AUNCOD='$auncod' 
								ORDER BY t1.arqsts, t1.auscod, T1.AFESOL desc, T1.AHRSOL desc";
							}else{
								$sql="SELECT T1.ARQNRO, T1.AFESOL, T1.AHRSOL, T1.ARQTIP, t2.atqdes, T1.ARQOBS, T1.ARQSTS, T1.AUSCOD 
								FROM is04fp t1, is02fp t2 
								WHERE T1.ACICOD= T2.ACICOD and T1.ARQTIP= T2.ATQCOD and t1.ACICOD = '$Compania' AND t1.AUNCOD='$coddpto' 
								ORDER BY t1.arqsts, t1.auscod, T1.AFESOL desc, T1.AHRSOL desc";
							}
						}
						// colocar en despues de '$Compania'  AND t1.AUNCOD='$coddpto'
						else
						{
							$sql3="SELECT AAPVLA FROM IV43FP WHERE ACICOD ='$Compania' AND AUSCOD = '$Usuario' ";
							$result3=odbc_exec($cid,$sql3)or die(exit("Error en odbc_exec 11111"));
							$permiso = 0;
							$TEM =@ odbc_result($result3, 1);
							if(trim($TEM) == 'N') $permiso = 0;
							else $permiso = 1;
							if($permiso>0)
							{
								$sql="SELECT T1.ARQNRO, T1.AFESOL, T1.AHRSOL, T1.ARQTIP, t2.atqdes, T1.ARQOBS, T1.ARQSTS, T1.AUSCOD 
								FROM is04fp t1, is02fp t2 
								WHERE T1.ACICOD= T2.ACICOD and T1.ARQTIP= T2.ATQCOD and t1.ACICOD = '$Compania' AND t1.AUNCOD='$coddpto' 
								order by t1.arqsts, t1.auscod, T1.AFESOL desc, T1.AHRSOL desc";
							}
							else
							{
								$sql="SELECT T1.ARQNRO, T1.AFESOL, T1.AHRSOL, T1.ARQTIP, t2.atqdes, T1.ARQOBS, T1.ARQSTS, T1.AUSCOD 
								FROM is04fp t1, is02fp t2
								WHERE T1.ACICOD= T2.ACICOD and T1.ARQTIP= T2.ATQCOD and t1.ACICOD = '$Compania' AND t1.AUSCOD='$Usuario' 
								order by t1.arqsts, t1.auscod, T1.AFESOL desc, T1.AHRSOL desc";
							}
						}
					//echo $sql.'<br>';
					//die();

					$result = odbc_exec($cid, $sql)or die(exit("Error en odbc_exec 11111"));
					//$resultrow = odbc_exec($cid, $sql)or die(exit("Error en odbc_exec 11111"));

					$excel = new PHPExcel();

						//Propiedades del Excel
						$excel->getProperties()
						   ->setCreator('IDASYS WEB')
						   ->setTitle('Reporte de Pedidos')
						   ->setLastModifiedBy('IDASYS WEB')
						   ->setDescription('A demo to show how to use PHPExcel to manipulate an Excel file')
						   ->setSubject('Reporte de Pedidos')
						   ->setKeywords('excel php office phpexcel')
						   ->setCategory('inventario')
						   ;

						$exceldata = $excel->getSheet(0);
						$exceldata->setTitle('Reporte_Pedidos');

						//Titulo
						$styleArray = array(
							    'font'  => array(
							        'bold'  => true
							    ),
							    'fill' => array(
					            'type' => PHPExcel_Style_Fill::FILL_SOLID,
					            'color' => array('rgb' => 'BDBDBD')
					            ),
					            'alignment' => array(
	            				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
	            				),
	            				'borders' => array(
					            'allborders' => array(
					                'style' => PHPExcel_Style_Border::BORDER_THIN,
					            	)
					        	)
						    );

						//Head Logo
					    $objDrawing = new PHPExcel_Worksheet_Drawing();
				        $objDrawing->setName('Logo');
				        $objDrawing->setDescription('Logo');
				        if($Compania == '14'){ 
					        $objDrawing->setPath('../images/logoidacadef2005.png');
					        $objDrawing->setHeight(70);
					        $objDrawing->setWidth(200);
					    } else {
					    	$objDrawing->setPath('../images/MEDITRON_logo_rif.png');
					        $objDrawing->setHeight(70);
				        	$objDrawing->setWidth(330);
					    }

				        $objDrawing->setCoordinates('A1');
				        $objDrawing->setWorksheet($excel->getActiveSheet());
				        //

						$excel->getActiveSheet()->getCell('A4')->setValue('RIF: '.$Companiarif);

						//Titulo 
						$excel->getActiveSheet()->getCell('A7')->setValue('REPORTE MEDITRON');
						$exceldata->mergeCells('A7:K7');
						$excel->getActiveSheet()->getStyle('A7')->getFont()->setBold(true);
						$excel->getActiveSheet()->getStyle('A7')->getFont()->setSize(25);
						$excel->getActiveSheet()->getStyle('A7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
						//

						//Tipo de Inventario	
						/*$sql2="SELECT T6.ATIDES
						FROM IV01FP T6 
						WHERE T6.ACICOD='$Compania'  AND T6.ATICOD in (".$tipoInve.") ";
							
						$result2=odbc_exec($cid,$sql2)or die(exit("Error en odbc_exec 11111"));
						$tInventario = ''; 
						while(odbc_fetch_row($result2))
						{
							$tInventario .= trim(odbc_result($result2,'ATIDES'));
						}

						if(empty($tInventario)){$tInventario='N/A';}*/

						//Sub titulo 
						$excel->getActiveSheet()->getCell('A8')->setValue('Pedidos');
						$exceldata->mergeCells('A8:K8');
						$excel->getActiveSheet()->getStyle('A8')->getFont()->setBold(true);
						$excel->getActiveSheet()->getStyle('A8')->getFont()->setSize(20);
						$excel->getActiveSheet()->getStyle('A8')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

						$excel->getActiveSheet()->getCell('A9')->setValue('Elaborado el '.$Fechaactual.' a las '.$Horaactual2);
						$exceldata->mergeCells('A9:K9');
						$excel->getActiveSheet()->getStyle('A9')->getFont()->setBold(true);
						$excel->getActiveSheet()->getStyle('A9')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

						if(empty($auncod)){
							$auncod = $coddpto;
						}

						$excel->getActiveSheet()->getCell('A10')->setValue('Almacén: '.utf8_encode(alamcen($auncod, $Compania)));
						$exceldata->mergeCells('A10:K10');
						$excel->getActiveSheet()->getStyle('A4')->getFont()->setBold(true);
						$excel->getActiveSheet()->getStyle('A10')->getFont()->setBold(true);
						$excel->getActiveSheet()->getStyle('A10')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

						/*$excel->getActiveSheet()->getCell('A11')->setValue('Filtrado por: '.$tInventario);
						$exceldata->mergeCells('A11:K11');
						$excel->getActiveSheet()->getStyle('A11')->getFont()->setBold(true);
						$excel->getActiveSheet()->getStyle('A11')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);*/

						$exceldata->setCellValue('A11', 'Nro. Requerimiento');
						$exceldata->setCellValue('B11', 'Fecha de Solicitud');
						$exceldata->setCellValue('C11', 'Hora de Solicitud');
						$exceldata->setCellValue('D11', 'Tipo de Requerimiento');
						$exceldata->setCellValue('E11', 'Desc. Tip/Requerimiento');
						$exceldata->setCellValue('F11', 'Observación');
						$exceldata->setCellValue('G11', 'Status');
						$exceldata->setCellValue('H11', 'Usuarios');
						$excel->getActiveSheet()->getStyle('A11:H11')->applyFromArray($styleArray);
					    				   		
					    //Genera Data
					    $pos = 12;
					    /*$startline = 14;
					    $count = 1;
					    $rowCount = 0;
					   	while(odbc_fetch_row($resultrow)){
					   		$rowCount++;
					    }*/

					    while(odbc_fetch_row($result))
						{
							//NRO Requerimiento
							$exceldata->setCellValue('A' . $pos, odbc_result($result, 'ARQNRO')); 

							//Fecha Solicitud
							$exceldata->setCellValue('B' . $pos, odbc_result($result, 'AFESOL'));

							//Hora Solicitud
							$exceldata->setCellValue('C' . $pos, odbc_result($result, 'AHRSOL'));

							//Tipo Requerimiento
							$exceldata->setCellValue('D' . $pos, trim(odbc_result($result, 'ARQTIP')));

							//Desc. Tip/Requerim.
							$exceldata->setCellValue('E' . $pos, trim(odbc_result($result, 'ATQDES')));

							//ARQOBS
							$exceldata->setCellValue('F' . $pos, utf8_encode(trim(odbc_result($result, 'ARQOBS'))));

							//Sts/Requerimiento
							$exceldata->setCellValue('G' . $pos, odbc_result($result, 'ARQSTS'));
							$usuario = new inf_usuario($cid, odbc_result($result, 'AUSCOD'));

							//Usuarios
							$exceldata->setCellValue('H' . $pos, $usuario->getNombre());
               				$pos++;
						}//fin while                        

						//Obtiene la cantidad de rows del excel (-13 rows del titulo) 			
						$rowCount = $excel->getActiveSheet()->getHighestRow();
						$rowCount = $rowCount-11;

						//Bordes y ajuste de anchura para la data generada
						$lin = 12;
				   		$prevpos = $lin-1;
						$highestColumn = $excel->getActiveSheet()->getHighestColumn();
						for($col = 'A'; $col <= $highestColumn; $col++)
					    {
					    	$value = $excel->getActiveSheet()->getCell($col.$prevpos)->getValue();
				   			$width = mb_strwidth($value);
				   			if($width == 0){
				   				$prevpos = $lin-1;
				   				$value = $excel->getActiveSheet()->getCell($col.$prevpos)->getValue();
				   				$width = mb_strwidth($value);
				   			}
					    	for($i = 0; $i < $rowCount; $i++){
					    		$value1 = $excel->getActiveSheet()->getCell($col.$lin)->getValue();
				   				$width1 = mb_strwidth($value1);
								if($width1 > $width){
									$width = $width1;
								}
								$excel->getActiveSheet()->getStyle($col.$lin)->applyFromArray(array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN,))));
								$excel->getActiveSheet()->getStyle($col.$lin)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
								//Texto centrado a Columnas especificas
								$excel->getActiveSheet()->getStyle('A'.$lin)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
								$excel->getActiveSheet()->getStyle('B'.$lin)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
								$excel->getActiveSheet()->getStyle('C'.$lin)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
								$excel->getActiveSheet()->getStyle('D'.$lin)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
								$excel->getActiveSheet()->getStyle('E'.$lin)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
								$excel->getActiveSheet()->getStyle($col.$lin)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
								//Background color de celdas
								/*if($col == 'J' || $col == 'K' ){ 
									if($col == 'J'){
										$excel->getActiveSheet()->getStyle($col.$lin)->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => '3399FF'))));
									} elseif($col == 'K') {
										$excel->getActiveSheet()->getStyle($col.$lin)->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => 'FFFF99'))));
									}elseif($col == 'W') {
										$excel->getActiveSheet()->getStyle($col.$lin)->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => '00FF00'))));
									}
								}*/
								//Wrap Text para la Columna 'D'
								$excel->getActiveSheet()->getStyle($col.$lin)->getAlignment()->setWrapText(true);
								$lin++;
								$prevpos++;
							}
							$width = $width+2;
							// IF para colocar un width estatico a la Columna y se aplique el WrapText
							if($col == 'A' || $col == 'B' || $col == 'F'){
								$width = 45;
								$excel->getActiveSheet()->getColumnDimension($col)->setAutoSize(false);
								$excel->getActiveSheet()->getColumnDimension($col)->setWidth($width);
							} else {
								$excel->getActiveSheet()->getColumnDimension($col)->setAutoSize(false);
								$excel->getActiveSheet()->getColumnDimension($col)->setWidth($width);
							}
							$width = 0;
							$lin = 12;
							$prevpos = $lin-1;
						}
						//Ocultar Columnas
						/*$excel->getActiveSheet()->getColumnDimension('D')->setVisible(false);
						$excel->getActiveSheet()->getColumnDimension('K')->setVisible(false);
						$excel->getActiveSheet()->getColumnDimension('U')->setVisible(false);*/

						//
						//Se genera el excel
				        $writer = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
						$writer->save('php://output');
						exit();
						?>