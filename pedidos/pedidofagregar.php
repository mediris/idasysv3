<?php session_start();
include("../conectar.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="sinhead">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>agregar usuario</title>
<link href="../<?php echo $css; ?>" rel="stylesheet" type="text/css" media="screen" />
<script language="JavaScript" src="../javascript/jquery.js"></script>
<script language="JavaScript" src="../javascript/javascript.js"></script>
<script language="JavaScript" src="javascript.js"></script>
<script language="JavaScript" src="../calendario/javascripts.js"></script>
</head>
<body class="sinbody" bgcolor="#FFFFFF" onload="javascript:agregar();cargarparametros(document.getElementById('arqtip'),'');">
<div id="agregardiv">

<div>
<div id="ocultarform"><form id="agregarform" name="agregarform" method="post" class="form" action="">

    <table class="tabla1">
      <tr>
        <td  scope="col"><label>Nro de Pedido </label></td>
        <td width="23%" scope="col">
        	<input name="arqnro" type="hidden" value="" />
            <div align="left" id="nroreq"></div>
        	<input name="auncod" type="hidden" value="<?php echo trim($auncod);?>" />
        </td>
        <td colspan="2" id="errarqtip"  scope="col">&nbsp;</td>
      </tr>
      <tr>
        <td  scope="col"><label>Tipo de Pedido</label></td>
        <td width="23%" scope="col"><div align="left">
            <select name="arqtip" id="arqtip" onchange="cargarparametros(this,'');">
				<?php 
					/*$sql="SELECT ATQCOD, ATQDES FROM is02fp t1 WHERE t1.acicod='$Compania'";*/
					$sql = "SELECT T1.ATQCOD, T1.ATQDES 
					FROM is02fp t1 
					INNER JOIN IS20fp t2 on ( T1.ACICOD =T2.ACICOD AND T1.ATQCOD=T2.ATQCOD AND T2.AUSCOD='$Usuario' )                                                
					WHERE t1.acicod='$Compania'";
					
					$result1=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111"));//si no es supervisor debe aparecer un textbox en modo lectura del usuario logueado o conectado, de lo contrario si aparece esta lista
					
					while(odbc_fetch_row($result1)){			
						$atqcod=trim(odbc_result($result1,1));
						$atqnom=trim(odbc_result($result1,2));?>
						<option value= "<?php echo $atqcod; ?>" ><?php echo $atqnom ?></option>
                <?php } ?>
            </select>
        </div></td>
        <td colspan="2" id="errarqtip"  scope="col">&nbsp;</td>
      </tr>
      <?php 
	  //VALIDO  PARA IDACA / MEDIX / LABORATORIO
	  if(($Compania == '14' || $Compania == '42' || $Compania == '43') && accesotodasunisol_alma(7)== 'S' ){
	  ?>
          <tr>
            <td  scope="col"><label>Unidad que Entrega</label></td>
            <td width="23%" scope="col"><div align="left">
                <select name="auncod2" id="auncod2">
                    <?php
						$listAuncod = '';
						if($Compania=='14'){
							$listAuncod = "'001','007','009','010','011', '".$auncod."'";
						}else if($Compania=='42'){
							$listAuncod = "'0004','0006','0005', '".$auncod."'";
						}else if($Compania=='43'){
              $listAuncod = "'0003', '".$auncod."'";
            }else{
							$listAuncod = "";
						}
						//unidades solictantes
						$sql="SELECT AUNCOD, AUNDES 
						FROM is01fp 
						WHERE ACICOD='$Compania' AND AUNCOD NOT IN ($listAuncod)
						ORDER BY AUNDES";
						/* 
						//almacenes
                        $sql="SELECT AALCOD, AALDES 
                        FROM iv07fp 
                        WHERE acicod ='$Compania' and 
                        aalcod not in ('0003' , '0004', '0005', '0006', '0017', '0009', '".$auncod."' )";
						*/
                        $result1=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111"));//si no es supervisor debe aparecer un textbox en modo lectura del usuario logueado o conectado, de lo contrario si aparece esta lista
                        
                        while(odbc_fetch_row($result1)){			
                            $aalcod=trim(odbc_result($result1,1));
                            $aaldes=trim(odbc_result($result1,2));?>
                            <option value= "<?php echo $aalcod; ?>" ><?php echo $aaldes ?></option>
                    <?php } ?>
                </select>
            </div></td>
            <td colspan="2" id="errauncod2"  scope="col">&nbsp;</td>
          </tr>
      <?php
	  }else{//PARA LAS DEMAS COMPA�IAS
	  ?>
      	 <tr>
            <td  scope="col"><label>Unidad que Entrega</label></td>
            <td width="23%" scope="col">
				<div align="left">
                    <?php 
						if($Compania=='14'){//idaca
							echo unisolicitante('002',$Compania);
						}else if($Compania=='01'){//meditron
							echo unisolicitante('0001',$Compania);
						}else if($Compania=='40'){//invap
							echo unisolicitante('0001',$Compania);
						}else if($Compania=='41'){//centis
							echo unisolicitante('0001',$Compania);
						}else if($Compania=='42'){//medix
							echo unisolicitante('0001',$Compania);
						}
					?>
            	</div>
            	<div style="visibility:hidden">
            	  <select name="auncod2" id="auncod2">
            	    <option value= "" >Seleccione...</option>
            	    <?php
                            //unidades solictantes
                            $sql="SELECT AUNCOD, AUNDES 
                            FROM is01fp 
                            WHERE ACICOD='$Compania' ";
							if($Compania=='14'){//idaca
								$sql.="AND AUNCOD NOT IN ('001','007','009','010','011', '".$auncod."')";
							}else if($Compania=='01'){//meditron
								$sql.="AND AUNCOD NOT IN ('".$auncod."')";
							}else if($Compania=='40'){//invap
								$sql.="AND AUNCOD NOT IN ('".$auncod."')";
							}else if($Compania=='41'){//centis
								$sql.="AND AUNCOD IN ('".$auncod."')";
							}else if($Compania=='42'){//medix
								$sql.="AND AUNCOD NOT IN ('".$auncod."')";
							}
							//echo $sql;
							//$sql="AUNCOD NOT IN ('001','007','009','010','011', '".$auncod."')";
                            /* 
                            //almacenes
                            $sql="SELECT AALCOD, AALDES 
                            FROM iv07fp 
                            WHERE acicod ='$Compania' and 
                            aalcod not in ('0003' , '0004', '0005', '0006', '0017', '0009', '".$auncod."' )";
                            */
							
                            $result1=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111"));//si no es supervisor debe aparecer un textbox en modo lectura del usuario logueado o conectado, de lo contrario si aparece esta lista
                            
                            while(odbc_fetch_row($result1)){			
                                $aalcod=trim(odbc_result($result1,1));
                                $aaldes=trim(odbc_result($result1,2));
                                if($Compania=='14'){//idaca
									if($aalcod=='002'){
										$selecttt = 'selected="selected"';
									}else{
										$selecttt = '';
									}
								}else if($Compania=='01'){//meditron
									if($aalcod=='0001'){
										$selecttt = 'selected="selected"';
									}else{
										$selecttt = '';
									}
								}else if($Compania=='40'){//invap
									if($aalcod=='0001'){
										$selecttt = 'selected="selected"';
									}else{
										$selecttt = '';
									}
								}else if($Compania=='41'){//centis
									if($aalcod=='0001'){
										$selecttt = 'selected="selected"';
									}else{
										$selecttt = '';
									}
								}else  if($Compania=='42'){//medix
									if($aalcod=='0001'){
										$selecttt = 'selected="selected"';
									}else{
										$selecttt = '';
									}
								}else{
									$selecttt = '';
								}

                                ?>
            	    <option value= "<?php echo $aalcod; ?>" <?php echo $selecttt; ?>><?php echo $aaldes ?></option>
            	    <?php } ?>
          	    </select>
            	</div>
               
            </td>
            <td colspan="2" id="errauncod2"  scope="col">&nbsp;</td>
          </tr>
      	
      <?php
	  }
	  ?>
      <tr>
        <td scope="col"><label>Observaci�n</label></td>
        <td scope="col"><div align="left">
          <textarea name="arqobs"  id="arqobs" cols="50" rows="5"/></textarea>
         
        </div></td>
        <td colspan="2" id="erraosnro" scope="col">&nbsp;</td>
      </tr>
      
      <tr>
      	<td colspan="4">
            <table width="100%"  border="0" >
                <tr>
                    <td width="100%" colspan="3" scope="col">
                        <h3>Par�metros Adicionales: </h3>
                        <span class="header">
                        </span>
                    </td>
                </tr>
                <tr>
                    <td>
	                    <div id="parametros"></div>
                    </td>
                </tr>
            </table>
        </td>
      </tr>
      
      <tr>
        <th scope="col">&nbsp;</th>
        <th scope="col"></th>
        <th width="23%" scope="col"><input name="cancelar" type="button" onClick="parent.location.reload();parent.Shadowbox.close();"" " value="Salir" id="cancelar"></th>
        <th width="21%"  scope="col"><input type="button" name="submit" id="submit" value="Grabar" onClick="agregarvalidar()" /></th>
        
      </tr>
    </table>

  </form></div>
  <form id="solicitudes" name="solicitudes" class="form">
  <div id="reqst06fp" align="center" style="display:none">

  <legend></legend>
  <table width="100%" class="tabla1">
  	<tr>
  	<td>
  		<table width="90%" height="94" border="0" cellpadding="0" cellspacing="4" align="center">
            <tr>
                <td><strong>N�mero de Pedido:</strong></td>
                <td><input name="arqnro" type="text"  ReadOnly/></td>
            </tr>
            <tr>
                <td width="36%"><strong>Tipo de Pedido</strong></td>
                <td width="64%"><div id="ord1"></div> <input name="arqtip" type="hidden" value="<?php echo trim($auncod);?>" /></td>
            </tr>
            <tr>
                <td width="36%"><strong>Unidad Solicitante</strong></td>
                <td width="64%"><div id="ord2"></div> <input name="auncod" type="hidden" value="" /> </td>
            </tr>
            <tr>
                <td width="36%"><strong>Unidad que Entrega</strong></td>
                <td width="64%"><div id="ord3"></div> <input name="auncod2" type="hidden" value="" /></td>
            </tr>
            <tr>
                <td><strong>Observaci�n:</strong></td>
                <td><div id="ord4"></div></td>
            </tr>
  		</table>
 	</td>
 	</tr>
 <tr>
 <td>
  <table  id="grilla" width="100%"  border="0" >
    <thead>
      <tr>
        <th colspan="4" align="center" scope="col"><h3>Detalle del Pedido: </h3>
          </th>
        </tr>
      <tr>	
        <th width="27%">Art�culo</th>
        <th width="19%">Cant. Solicitada</th>
        <th width="20%">Un/Med </th>
        <th width="13%">Opciones</th>
        </tr>
      </thead>
    <tbody>
      <tr>
        <td>
          <div align="left" id="ocultararticulo">
            <select id="aarcod" name="aarcod" onchange="choiceart('parent',this,0,event)">
              
              </select>
            </div></td>
        
        <td><div ><input type="text" name="aslcan" id="aslcan" size="15" maxlength="15" onkeydown="esenter(event);" onchange="activaimagengrabar(1)"/></div></td>
        <td>
          <!--<div >
            <input name="aarumb" type="text" id="aarumb" size="6" maxlength="4"  ReadOnly>
            </div>-->
            <div id="div_aarnum">
            </div>
          </td>
        
        <td >
          <table width="100%">
            <tr>
              <td>
                <div id="imagengrabar" style="display:none"><a href="javascript:grabardetallereq();"><img src="../images/aprobar.png" width="16" height="16" border="0" /></a> </div>
                </td>
              <td>&nbsp;</td>
              <td><div id="imageneliminar" style="display:none"><a href="javascript:detallereqeliminar();"><img src="../images/rechazar.png" width="16" height="16" /></a> </div></td>
              </tr>
            </table>
          </td>
        </tr>
      </tbody>
  </table>
  
       </td>
       </tr>
                <tr>
                    	
        	
        	<td width="21%"  scope="col"><strong><p id='finalizarboton' onclick="finalizarreq('<?php echo trim($coddpto);?>')" class="subir" align="right">Finalizar</p></strong></td>
        	
   		  </tr>
        
          </table>

    </div>
    </form>  
</div>

</div>
<div align="center" id="agregaraftsav" style="display:none">
  <br />
  <br />
  <br />
  <br />
  <br />
  <br />
  Registro Agregado con exito !
  <br />
  <br />
  <br />
  <input name="Submit3" type="button" onClick="parent.location.reload();parent.Shadowbox.close();"" " value="Cerrar"></div>
</body>
</html>