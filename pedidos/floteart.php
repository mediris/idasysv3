<?php session_start();
include("../conectar.php");

$arqnro = $_GET['nro'];
$aarcod = $_GET['art'];
$aarumb = $_GET['uni'];
$cantid = $_GET['can'];
$aalcod = $_GET['alm'];

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="sinhead">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>editar pedido</title>
<link href="../style.css" rel="stylesheet" type="text/css" media="screen" />
<script language="JavaScript" src="../javascript/jquery.js"></script>
<script language="JavaScript" src="../javascript/javascript.js"></script>
<script language="JavaScript" src="javascript.js"></script>
<script language="JavaScript" src="../calendario/javascripts.js"></script>
</head>
<body class="sinbody" bgcolor="#FFFFFF" onload="javascript:cargar('<?php echo trim($arqnro);?>','<?php echo trim($aarcod);?>', '<?php echo trim($aarumb);?>', '<?php echo trim($cantid);?>', '<?php echo trim($aalcod);?>')">
<form name="solicitudes" id="solicitudes">
<table  id="grilla" width="100%"  border="0" class="tabla1">
	<input type="hidden" name="aarcod" id="aarcod" value="<?php echo $aarcod;?>" />
    <input type="hidden" name="arqnro" id="arqnro" value="<?php echo $arqnro;?>" />
    <input type="hidden" name="aarumb" id="aarumb" value="<?php echo $aarumb;?>" />
    <?php 
		$arti = new inf_articulo($Compania, $aarcod);
    ?>
    <input type="hidden" name="aarumbdes" id="aarumbdes" value="<?php echo unidad_medidad($aarumb, $Compania,1);?>" />
    <input type="hidden" name="aalcod" id="aalcod" value="<?php echo $aalcod;?>" />
    <input type="hidden" name="cantid" id="cantid" value="<?php echo $cantid;?>" />
    <thead>
      <tr>
        <th colspan="5" align="center" scope="col"><h3>Detalle del Articulo (Lotes): <?php echo $arti->baardes; echo " (".unidad_medidad($aarumb, $Compania, $cantid).") ";?></h3>
          </th>
        </tr>
      <tr>	
        <th width="19%">Cant. Solicitada</th>
        <th width="19%">Lote</th>
        <th width="13%">Opciones</th>
        </tr>
      </thead>
    <tbody>
      <tr>
        <td><div ><input type="text" name="aslcan" id="aslcan" size="15" maxlength="15" onkeypress="javascript:if(event.keyCode==13){return false;}" <? //onkeydown="esenter(event);" ?> onchange="activaimagengrabar2();"/></div></td>
        <td>
            <div id="div_atlcod"></div>
        </td>
        <td>
        	<div id="imagengrabar" style="display:none"><a href="javascript:grabardetallereqlote();"><img src="../images/aprobar.png" width="16" height="16" border="0" /></a> </div>
        </td>
        </tr>
       	<tr>
        	<td ><div id="div_total"></div></td>
            <td >&nbsp;</td>
            <td >&nbsp;</td>
        </tr>
      </tbody>
      <tfoot>
      	<tr>
        	<td colspan="3" align="right"> <input name="cancelar" type="button" onClick="parent.Shadowbox.close();"" " value="Salir" id="cancelar"></td>
        </tr>
      </tfoot>
  </table>
  </form>
</body>
</html>
