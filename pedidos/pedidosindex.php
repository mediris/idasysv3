<?php session_start();
include("../conectar.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
<title>Idasys V3</title>
<link href="../<?php echo $css; ?>" rel="stylesheet" type="text/css" media="screen" />
<script language="JavaScript" src="../javascript/javascript.js"></script>
<script language="JavaScript" src="javascript.js"></script>
<script language="JavaScript" src="../javascript/jquery.js"></script>
<link href="../shadowbox.css"  rel="stylesheet" type="text/css"/>
<script src="../shadowbox.js" type="text/javascript"/> </script>
<script language="JavaScript" type="text/JavaScript">
Shadowbox.init({
overlayOpacity: "0.5"
});
</script>
<style type="text/css" title="currentStyle">
			@import "../DataTables-1.9.4/media/css/<?php echo $demo_page;?>";
			@import "../DataTables-1.9.4/media/css/<?php echo $demo_table;?>";
</style>
<script type="text/javascript" language="javascript" src="../DataTables-1.9.4/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf-8">
			
			 $(document).ready(function() 
			 {
				  document.getElementById('info').style.display ="";
			      $('#info').dataTable( 
				  {
			        //inicio seccion de agrupacion
					"fnDrawCallback": function ( oSettings ) {
						if ( oSettings.aiDisplay.length == 0 )
						{
							return;
						}
				 
						var nTrs = $('#info tbody tr');
						var iColspan = nTrs[0].getElementsByTagName('td').length;
						var sLastGroup = "";
						var sLastGroup2 = "";
						/*grupo de status*/
						for ( var i=0 ; i<nTrs.length ; i++ )
						{
							var iDisplayIndex = oSettings._iDisplayStart + i;
							var sGroup = oSettings.aoData[ oSettings.aiDisplay[iDisplayIndex] ]._aData[1];
							if ( sGroup != sLastGroup2 )
							{
								var nGroup = document.createElement( 'tr' );
								var nCell = document.createElement( 'td' );
								nCell.colSpan = iColspan;
								nCell.className = "Usuario";//el nombre de la comlumna (debe estar de primera..)
								nCell.innerHTML = sGroup;
								nGroup.appendChild( nCell );
								nTrs[i].parentNode.insertBefore( nGroup, nTrs[i] );
								sLastGroup2 = sGroup;
							}
							
							var iDisplayIndex = oSettings._iDisplayStart + i;
							var sGroup = oSettings.aoData[ oSettings.aiDisplay[iDisplayIndex] ]._aData[0];
							if ( sGroup != sLastGroup )
							{
								var nGroup = document.createElement( 'tr' );
								var nCell = document.createElement( 'td' );
								nCell.colSpan = iColspan;
								nCell.className = "Status";//el nombre de la comlumna (debe estar de primera..)
								nCell.innerHTML = sGroup;
								nGroup.appendChild( nCell );
								nTrs[i].parentNode.insertBefore( nGroup, nTrs[i] );
								sLastGroup = sGroup;
							}
						}
					},
					"aoColumnDefs": [
						{ "bVisible": false, "aTargets": [ 0 ] },
						{ "bVisible": false, "aTargets": [ 1 ] }
					],
					"aaSortingFixed": [[ 0, 'asc' ]],
					"aaSorting": [[ 1, 'asc' ]],
					"sDom": 'lfr<"giveHeight"t>ip',
					//fin seccion de agrupacion
					
					"bStateSave": true,
					"oLanguage": 
					{
			          "sLengthMenu": "Mostrar _MENU_ registros por Pag.",
						"sZeroRecords": "No Existen Registros",
						"sInfo": "Mostrando desde _START_ hasta _END_ de _TOTAL_ registros",
						"sInfoEmpty": "Mostrando desde 0 hasta 0 de 0 registros",
						"sInfoFiltered": "(filtrando de _MAX_ de registros)",
						"sSearch": "Buscar",
					  "oPaginate": 
					  {
			            "sNext": "Siguiente",
						"sPrevious": "Anterior"
			          },
					  
			        }
			      } );
			  } );
</script>
</head>
<body>
<div id="wrapper">
  <?php 
  		include("../superior.php");
  		?>
  <div id="page">
      <?php include("../validar.php");  		?>

<!--
<div id="sidebar">
	    <ul>
	      <li>
	        <h2>Estad&iacute;sticas</h2>
	        <p>Aqu&iacute; se reflejar&aacute;n estad&iacute;sticas particulares seg&uacute;n en la opci&oacute;n del sistema que se encuentre</p>
          </li>
	      <li>
	        <h2>Categories</h2>
	        <ul>
	          <li><a href="#">Aliquam libero</a></li>
	          <li><a href="#">Consectetuer adipiscing elit</a></li>
	          <li><a href="#">Metus aliquam pellentesque</a></li>
	          <li><a href="#">Suspendisse iaculis mauris</a></li>
	          <li><a href="#">Urnanet non molestie semper</a></li>
	          <li><a href="#">Proin gravida orci porttitor</a></li>
            </ul>
          </li>
	      <li>
	        <h2>Blogroll</h2>
	        <ul>
	          <li><a href="#">Aliquam libero</a></li>
	          <li><a href="#">Consectetuer adipiscing elit</a></li>
	          <li><a href="#">Metus aliquam pellentesque</a></li>
	          <li><a href="#">Suspendisse iaculis mauris</a></li>
	          <li><a href="#">Urnanet non molestie semper</a></li>
	          <li><a href="#">Proin gravida orci porttitor</a></li></ul>
	      </li>
      </ul>
  </div>-->
	  
         <?php 
		 		$wsolicitud=0;
				if ($solicitudpagina==0) 	
				{
							$sql="SELECT AUNCOD FROM IS09FP WHERE ACICOD='$Compania' and AUSCOD='$Usuario'";
							$resultec=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111"));
							$coddpto=trim(odbc_result($resultec,1));
							if($coddpto == ''){
								echo "<br><br><h1><center>Ud no est� asociado a alguna unidad solicitante</center></h1><br><center><img src='../images/alert.png' alt='Alerta' width='40' height='40' border='0'></center>";
								echo "<br><br><center>Por favor comuniquese con el administrador del sistema</center>";
								echo "<br><h3><center><a href=http://".$Direccionip."/idasysv3/index.php>REGRESAR</a></center></h3>";
								exit;
							}
							else
							{
								$sql="SELECT AUSCOD, AUSSUP FROM MB20FP WHERE AUSCOD='$Usuario' AND AUSSUP='S'";
								$result9=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111"));
								$super=trim(odbc_result($result9,2));

								$fecha = trim($_GET['fecha']) ? trim($_GET['fecha']) . "-01-01" : date("Y").'-01-01';

								if($super == 'S')
								{
									$auncod ='';
									$auncod = trim($_GET['auncod']);
									if(!empty($auncod))
									{
										$sql="SELECT T1.ARQNRO, T1.AFESOL, T1.AHRSOL, T1.ARQTIP, t2.atqdes, T1.ARQOBS, T1.ARQSTS, T1.AUSCOD 
										FROM is04fp t1, is02fp t2 
										WHERE T1.ACICOD= T2.ACICOD and T1.ARQTIP= T2.ATQCOD and t1.ACICOD = '$Compania' AND t1.AUNCOD='$auncod' and T1.AFESOL >= '$fecha'
										ORDER BY t1.arqsts, t1.auscod, T1.AFESOL desc, T1.AHRSOL desc";
									}else{
										$sql="SELECT T1.ARQNRO, T1.AFESOL, T1.AHRSOL, T1.ARQTIP, t2.atqdes, T1.ARQOBS, T1.ARQSTS, T1.AUSCOD 
										FROM is04fp t1, is02fp t2 
										WHERE T1.ACICOD= T2.ACICOD and T1.ARQTIP= T2.ATQCOD and t1.ACICOD = '$Compania' AND t1.AUNCOD='$coddpto' and T1.AFESOL >= '$fecha'
										ORDER BY t1.arqsts, t1.auscod, T1.AFESOL desc, T1.AHRSOL desc";
									}
								}
								// colocar en despues de '$Compania'  AND t1.AUNCOD='$coddpto'
								else
								{
									$sql3="SELECT AAPVLA FROM IV43FP WHERE ACICOD ='$Compania' AND AUSCOD = '$Usuario' ";
									$result3=odbc_exec($cid,$sql3)or die(exit("Error en odbc_exec 11111"));
									$permiso = 0;
									$TEM =@ odbc_result($result3, 1);
									if(trim($TEM) == 'N') $permiso = 0;
									else $permiso = 1;
									if($permiso>0)
									{
										$sql="SELECT T1.ARQNRO, T1.AFESOL, T1.AHRSOL, T1.ARQTIP, t2.atqdes, T1.ARQOBS, T1.ARQSTS, T1.AUSCOD 
										FROM is04fp t1, is02fp t2 
										WHERE T1.ACICOD= T2.ACICOD and T1.ARQTIP= T2.ATQCOD and t1.ACICOD = '$Compania' AND t1.AUNCOD='$coddpto' and T1.AFESOL >= '$fecha'
										order by t1.arqsts, t1.auscod, T1.AFESOL desc, T1.AHRSOL desc";
									}
									else
									{
										$sql="SELECT T1.ARQNRO, T1.AFESOL, T1.AHRSOL, T1.ARQTIP, t2.atqdes, T1.ARQOBS, T1.ARQSTS, T1.AUSCOD 
										FROM is04fp t1, is02fp t2
										WHERE T1.ACICOD= T2.ACICOD and T1.ARQTIP= T2.ATQCOD and t1.ACICOD = '$Compania' AND t1.AUSCOD='$Usuario' and T1.AFESOL >= '$fecha'
										order by t1.arqsts, t1.auscod, T1.AFESOL desc, T1.AHRSOL desc";
									}
								}
							}
							//echo $sql;
							$resultt=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111"));
							$z=0;
							$lin=1;
							$limitep=$_SESSION['solicitudlineasporpaginat'];
							$pag=1;
							$primero='S';

  							while(odbc_fetch_row($resultt))
							{
								$jml = odbc_num_fields($resultt);
								$row[$z]["pagina"] =  $pag;
   			 					for($i=1;$i<=$jml;$i++)
        						{
									$row[$z][odbc_field_name($resultt,$i)] =  odbc_result($resultt,$i);
								}
								$z++;
								if ($lin>=$limitep) 
								{
									$limitep+=$_SESSION['solicitudlineasporpaginat'];
									$pag++;
								}
								$lin++;
							}
							$totsol=($lin-1);
							$_SESSION['totalsolicitudes']=$totsol;
							$_SESSION['solicitudarreglo']=$row;
							$solicitudpagina=1;
							$_SESSION['solicitudpaginas']=$pag;
				}//fin de solicitudpagina
?>
	<div id="content3" >
    	<table width="100%"   border="0">
              <tr>
                <td width="84%" scope="col"><h1 align="center" class="title">PEDIDOS desde la fecha <?php echo $fecha; ?></h1>
                <div align="center"><h3>
				<form>Unidad Solicitante:     
				<?PHP 
					$sqlsup = "SELECT AUSCIA, AUSCOD, AUSSUP, AUSUSR FROM mb20fp WHERE AUSCOD ='".$Usuario."' AND AUSCIA='".$Compania."'";
					$resultsup =@odbc_exec($cid,$sqlsup)or die(exit("Error en odbc_exec 11111"));
					$supervisor = odbc_result($resultsup, 'AUSSUP');
                    if($supervisor =='S') 
                    {
						?>
                        
                        <select name="auncod" id="auncod">
                        <?php 
						$sql="SELECT AUNCOD, AUNDES FROM IS01FP WHERE ACICOD='$Compania' order by aundes";
                        $result1=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111")); 
                        $select = '';
                        while(odbc_fetch_row($result1)){

                            $cod=trim(odbc_result($result1,1));
                            $des=trim(odbc_result($result1,2));
                            if(!empty($auncod))
                            {
                                if($cod == $auncod){
                                    $select = ' selected="selected" ';
                                }
                                else{
                                    $select = '';
                                }
                            }
							else
							{
								if($cod == $coddpto){
                                    $select = ' selected="selected" ';
                                }
                                else{
                                    $select = '';
                                }
							}
                        ?>
                            <option value= "<?php echo $cod; ?>" <?php echo $select?> ><?php echo $des." ( ".$cod." )"; ?></option>
                        <?php } ?>
                    </select>
                    <select name="fecha" id="fecha">
						<?php 
						$i = 0;
						while($i <= 10){ ?>
                			<option value="<?php echo date('Y') - $i;?>"><?php echo date('Y') - $i;?></option>
                			<?php $i++;  
                		} ?>	
                    </select>

                    <button type="submit">Buscar</button>

                    <?php 	
                    }
                    else
                    {
                        $sql="SELECT AUNDES FROM IS01FP WHERE ACICOD='$Compania' AND AUNCOD='$coddpto'";
						$result20=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111"));
						echo $descrp=trim(odbc_result($result20,1));
                    }
                ?>
                </form>
                 
                 </h3></div><hr /></td>
                <td width="16%" scope="col">
                    <div align="left">
                      <table width="100%"  border="0">
                        <tr>
                        <?php 
							if(!empty($auncod))
							{
						?>
	                          <th width="36%" scope="col"><a rel="shadowbox;width=950;height=450" title="Agregar Pedido" href="pedidofagregar.php?&tipo=<?php echo trim($auncod);?>"><img src="../images/agregar.gif" title="Agregar" width="29" height="30" border="0" /></a></th>
                        <?php 
							}else if(!empty($coddpto)){
						?>
    	                   	  <th width="36%" scope="col"><a rel="shadowbox;width=950;height=450" title="Agregar Pedido" href="pedidofagregar.php?&tipo=<?php echo trim($coddpto);?>"><img src="../images/agregar.gif" title="Agregar" width="29" height="30" border="0" /></a></th>
                        <?php 
							}
						?>
                        <th width="30%" scope="col">
                          	<a href="exportaraexcel.php?&Usuario=<?php echo $Usuario; ?>&coddpto=<?php echo trim($coddpto); ?>&auncod=<?php echo $auncod;?>" target="_blank">
                        		<img src="../images/excel.jpg" alt="" width="30" height="30" title="Exportar a Excel"/>
                        	</a>
                        </th>
                          <th width="16%" scope="col"><img src="../images/pdf.jpg" alt="" width="25" height="25" /></th>
                          <th width="18%" scope="col"><img src="../images/impresora.gif" alt="" width="25" height="25" /></th>
                        </tr>
                      </table>
                    </div>
                </td>
              </tr>
            </table>
		<div id="container">
            <div id="demo">
            <table width="100%" id="info" style="display:none">
            <thead>
              <tr>
              	<th ><div align="left"><strong>Status</strong></div></th>
                <th ><div align="left"><strong>Usuario</strong></div></th>
                <th width="17%" scope="col">N&uacute;mero Pedido</th>
                <th width="39%" scope="col">Tipo de Solicitud</th>
                <th width="23%" scope="col">Fecha Inicio</th>
                <th width="21%" scope="col">Opciones</th>
              </tr>
            </thead>
            <tbody>
              <?php $tipopar="";
			  		$tipotec="";
				
				$paginat=$_SESSION['solicitudarreglo'];
				//print_r($paginat);
				$pagact=$solicitudpagina;
				for($g=0; $g < (count($paginat)); $g++){
			?>
        
              <tr>
              	<td scope="col"><div align="left"><strong>Status : <?php echo status('ARQSTS',$paginat[$g]["ARQSTS"]);?></strong></div></td>
                <td scope="col"><div align="left">&nbsp;&nbsp;<strong>Usuario : <?php echo "(".$paginat[$g]["AUSCOD"].")".usuario($paginat[$g]["AUSCOD"]);?></strong></div></td>
                <td scope="col"><div align="center"><strong><?php echo $paginat[$g]["ARQNRO"];?></strong></div></td>
                <td scope="col"><div align="center"><?php echo $paginat[$g]["ATQDES"];?></div></td>
                <td scope="col"><div align="center"><?php echo $paginat[$g]["AFESOL"]." / ".$paginat[$g]["AHRSOL"];?></div></td>
                <td valign="top" scope="col">
                	<ul id="opciones">
						<?php if($paginat[$g]["ARQSTS"]=='01' || $Usuario == 'JDAVILA' || $Usuario == 'BGONZALEZ'){?>
                        	<?php if(trim($paginat[$g]["AUSCOD"]) == trim($Usuario) || $supervisor ==  'S' ){?>
                                <li><a rel="shadowbox;width=950;height=450" title="Editar Pedido" href="pedidofeditar.php?&pedid=<?php echo trim($paginat[$g]["ARQNRO"]);?>"><img src="../images/editar.gif" title="Editar informaci&oacute;n" width="25" height="25" border="0"></a></li>
                                <?php 
                                /*verificando si se puede enviar el correo*/
                                    $sqlc="Select count(*) from is05fp where acicod='$Compania' and arqnro='".$paginat[$g]["ARQNRO"]."' ";
                                    $resultc=odbc_exec($cid,$sqlc)or die(exit("Error en odbc_exec 11111"));
                                    if(odbc_result($resultc,1)>0){
                                ?>
                                        <li><a href="javascript:enviarpedido('<?php echo trim($paginat[$g]["ARQNRO"]);?>')"><img src="../images/enviaremail.png" title="Enviar a aprobar" width="36" height="28" border="0" /></a></li>
                                <?php }else{ ?>
                                        <li><img src="../images/enviaremailnone.png" title="No se puede realizar aprobaci�n" width="36" height="28" border="0"></li>
                                <?php } ?>
                            
                                <li><a href="javascript:eliminar('<?php echo trim($paginat[$g]["ARQNRO"]);?>')"><img src="../images/eliminar.gif" title="Eliminar" width="25" height="25" border="0"></a></li>
                        	<?php } ?>
						<?php } ?>
                        
                        <li><a href="javascript:requerimverdetalle('<?php echo trim($paginat[$g]["ARQNRO"]);?>')"><img src="../images/ver.png" title="Ver Detalle" width="31" height="28" border="0"></a></li>

						<?php /*if(($Compania == '43' && $auncod == '0003' && trim($paginat[$g]["ARQTIP"]) != '03') || ($Compania == '43' && $auncod == '' && $coddpto == '0003' && trim($paginat[$g]["ARQTIP"]) != '03')){ ?>
							<li><a href="javascript:etiquetasPedidos('<?php echo trim($paginat[$g]["ARQNRO"]);?>', '<?php echo trim($paginat[$g]["ARQTIP"]);?>')"><img src="../images/etiqueta.png" title="Generacion de Etiquetas" alt="Generacion de Etiquetas" width="30" height="25" border="0" /></a></li>
						<?php }*/?>

						<!-- NUEVO -->
						<?php if(($Compania == '43' && $auncod == '0003' && trim($paginat[$g]["ARQTIP"]) == '02') || ($Compania == '43' && $auncod == '' && $coddpto == '0003' && trim($paginat[$g]["ARQTIP"]) == '02')){ ?>
							<li><a href="javascript:etiquetasPedidos('<?php echo trim($paginat[$g]["ARQNRO"]);?>', '<?php echo trim($paginat[$g]["ARQTIP"]);?>')"><img src="../images/etiqueta.png" title="Generacion de Etiquetas C�psulas I-131" alt="Generacion de Etiquetas C�psulas I-131" width="30" height="25" border="0" /></a></li>
						<?php }?>

						<?php if(($Compania == '43' && $auncod == '0003' && trim($paginat[$g]["ARQTIP"]) == '04') || ($Compania == '43' && $auncod == '' && $coddpto == '0003' && trim($paginat[$g]["ARQTIP"]) == '04')){ ?>
							<li><a href="javascript:etiquetasPedidos('<?php echo trim($paginat[$g]["ARQNRO"]);?>', '<?php echo trim($paginat[$g]["ARQTIP"]);?>')"><img src="../images/etiqueta.png" title="Generacion de Etiquetas Viales Tecnecio" alt="Generacion de Etiquetas Viales Tecnecio" width="30" height="25" border="0" /></a></li>
						<?php }?>
						<!-- NUEVO -->
						
                  	</ul>
                 </td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
          </div>
	    </div>
	  </div>
  </div>
		<!-- end #content -->
		<!-- end #sidebar -->
	<div style="clear: both;">&nbsp;</div>
  </div>
	<!-- end #page -->
	<div id="footer">
		<div id="opcionesmasusadas"><?php include("../opcionesmasusadasphp.php");?></div>
	</div>
	<!-- end #footer -->
</body>
</html>
