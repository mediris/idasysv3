/*
jDavila
26/03/12
*/
function agregar()
{
	fecha=new Date(); 
	var a=fecha.getDate();
	if (a <= 9)	{	cad="0";
					a = cad+a;
				}
	var b=fecha.getMonth();
	var b=b+1;
	if (b <= 9)	{	cad="0";
					b = cad+b;
				}
	var c=fecha.getFullYear(); 
	var d = ".";
	var e = a+d+b+d+c;
	
	//document.getElementById("tituloop").innerHTML="Agregar Opci&oacute;n";
	document.form.atjfec.value= e;
	document.form.atjnri.value="";
	document.form.atjtxt.value="";
	$("#aftsav").hide(1);
}

/*
jDavila
26/03/12
*/
function agregarvalidar(){	
	var contenido= "";
	var elemento;
	document.body.style.cursor="wait";

	$.ajax({
	   	async: false,
        type: "POST",
		dataType: "JSON",
        url: "cfrgtagregarvalidarphp.php",
        data: $("#form").serialize(),
        success: function(datos){
			vdata = parseJSONx(datos);}							
	});			
	sierror='N';
	for(i in vdata)	{
		if(vdata[i].campo) 	{
			document.getElementById(vdata[i].campo).innerHTML=vdata[i].msg;
			sierror='S';
		}
	}
	document.body.style.cursor="default";
	if(sierror=='N') 		
	{
		$("#aediv").hide(1000);
		$("#aftsav").show(1000);
	}
}

/*
jDavila
26/03/12
*/
function editar(fecha, sec) {

	var param = [];
	param['atjfec']=fecha;
	param['atjcts']=sec;
	ejecutasqlp("cfrgtinformacionphp.php",param);

	for(i in gdata)
	{
		document.form.atjfec.value=gdata[i].ATJFEC;
		window.document.getElementById("watjfec").innerHTML=gdata[i].ATJFEC;//fecha
						
		document.form.atjnri.value=gdata[i].ATJNRI;
		window.document.getElementById("watjnri").innerHTML=gdata[i].ATJNRI;
						
						
		/*document.form.aalsel.disabled = true;
		var tem = gdata[i].AALSEL;
		var arr = tem.split(";");
		var datos = document.form.aalsel.options;
		for (var b = 0; b<datos.length; b++){
			for (var j = 0; j<arr.length; j++){
				if(datos[b].value == arr[j]){
					datos[b].selected = true;
				}
			}
		}*/
		document.getElementById("divaalsel").innerHTML=gdata[i].AALDES;
		document.form.aalsel.value=gdata[i].AALSEL;
		
		document.form.atisel.disabled = true;
		var tem = gdata[i].ATISEL;
		var arr = tem.split(";");
		var datos = document.form.atisel.options;
		for (var b = 0; b<datos.length; b++){
			for (var j = 0; j<arr.length; j++){
				if(datos[b].value == arr[j]){
					datos[b].selected = true;
				}
			}
		}
		
		document.form.atrcde.value=gdata[i].ATRCDE;
		//window.document.getElementById("atrcde").innerHTML=gdata[i].ATRCDE;
		
		document.form.atrcds.value=gdata[i].ATRCDS;
		//window.document.getElementById("atrcds").innerHTML=gdata[i].ATRCDS;
				
		document.form.atjtxt.value=gdata[i].ATJTXT;
		//window.document.getElementById("atjtxt").innerHTML=gdata[i].ATJTXT;

		document.getElementById('atjcts').value= sec;
	};
	$("#aftsav").hide(1);
}

/*
jDavila
26/03/12
*/
function editarvalidar(){	
	var contenido= "";
	var elemento;
	document.body.style.cursor="wait";
	$.ajax({
			async: false,
			type: "POST",
			dataType: "JSON",
			url: "cfrgteditarvalidarphp.php",
			data: $("#form").serialize(),
			success: function(datos){
						vdata = parseJSONx(datos);}							
	});			
	var sierror='N';
	for(i in vdata)	{
		if(vdata[i].campo) 	{
			document.getElementById(vdata[i].campo).innerHTML=vdata[i].msg;
			sierror='S';
		}
	}
	document.body.style.cursor="default";
	if(sierror=='N') 		
	{
		$("#form").hide(1000);
		$("#aftsav").show(1000);	
	}

}

/*
jDavila
26/03/12
*/
function eliminar(fecha, sec) {
	if (confirm('Seguro que desea borrar la tarjeta ?'))
	{
		document.body.style.cursor="wait";
		var param = [];
		param['atjfec']=fecha;
		param['atjcts']=sec;
		ejecutasqld("cfrgteliminar.php",param);
		document.body.style.cursor="default";	
		location.reload();
	}
}

function cargafisicarapido()
{
	var contenido= "";
	var elemento;
	document.body.style.cursor="wait";
	$.ajax({
			async: false,
			type: "POST",
			dataType: "JSON",
			url: "cfrcargarconteo.php",
			data: $("#form").serialize(),
			success: function(datos){
						vdata = parseJSONx(datos);}							
	});
	document.body.style.cursor="default";	
	//location.reload();
}

function chequeodetalleguardar()
{
	var contenido= "";
	var elemento;
	document.body.style.cursor="wait";
	$.ajax({
			async: false,
			type: "POST",
			dataType: "JSON",
			url: "cfrcamstatusdet.php",
			data: $("#form").serialize(),
			success: function(datos){
						vdata = parseJSONx(datos);}							
	});
	document.body.style.cursor="default";	
	//location.reload();
}

function finalizarcargacfrapido()
{
	cargafisicarapido(); //cfrcargarconteo
	chequeodetalleguardar(); //cfrcamstatusdet
	var contenido= "";
	var elemento;
	document.body.style.cursor="wait";
	$.ajax({
			async: false,
			type: "POST",
			dataType: "JSON",
			url: "cfrcargarconteofinalizar.php",
			data: $("#form").serialize(),
			success: function(datos){
						//alert("Conteo Finalizado y guardado satisfactoriamente...");
						vdata = parseJSONx(datos);}							
	});
	window.opener.document.location.reload();self.close();
}


function cargarconteofisico(cfgt,conteo, sec)
{
	self.name = "main"; // names current window as "main"
	var windowprops="toolbar=0,location=0,directories=0,status=0,"+"menubar=0,scrollbars=1,resizable=0,width=1000,height=600";
	//cfrcargaf.php?cfgt=<?php echo trim(fecdma($paginat[$g]["ATJFEC"],"amd","."))."&conteo=1"."&sec=$sec";?>
	var ventana="cfrcargaf.php?cfgt="+cfgt+"&conteo="+conteo+"&sec="+sec+"";
	window.open(ventana,"",windowprops); // opens remote control

}

function verconteofisico(cfgt,conteo, sec)
{
	self.name = "main"; // names current window as "main"
	var windowprops="toolbar=0,location=0,directories=0,status=0,"+"menubar=0,scrollbars=1,resizable=0,width=1000,height=600";
	//cfrcargaf.php?cfgt=<?php echo trim(fecdma($paginat[$g]["ATJFEC"],"amd","."))."&conteo=1"."&sec=$sec";?>
	var ventana="cfrverconteo.php?cfgt="+cfgt+"&conteo="+conteo+"&sec="+sec+"";
	window.open(ventana,"",windowprops); // opens remote control
}

function imprimirconteofisico(cfgt,conteo, sec)
{
	self.name = "main"; // names current window as "main"
	var windowprops="toolbar=0,location=0,directories=0,status=0,"+"menubar=0,scrollbars=1,resizable=0,width=1000,height=600";
	//cfrcargaf.php?cfgt=<?php echo trim(fecdma($paginat[$g]["ATJFEC"],"amd","."))."&conteo=1"."&sec=$sec";?>
	var ventana="cfrver.php?cfgt="+cfgt+"&conteo="+conteo+"&sec="+sec+"";
	window.open(ventana,"",windowprops); // opens remote control

}

function iniciarcarga(stsnuevo , sts, atjfec, atjcts, compania, cid, modulo)
{
	var param = [];
	param['stsnuevo']=stsnuevo;
	param['sts']=sts;
	param['atjfec']=atjfec;
	param['atjcts']=atjcts;
	param['compania']=compania;
	param['cid']=cid;
	param['modulo']=modulo;
	
	ejecutasqld("iniciarcaga.php",param);
}

function anularconteo(cfgt, sec)
{
	if(confirm("Va realizar la Anulacion del Conteo Fisico."))
	{
		var param = [];
		param['cfgt']=cfgt;
		param['sec']=sec;
		ejecutasqlp("../ajustesconteofisico/anularconteo.php",param);
		location.reload();
	}	
}